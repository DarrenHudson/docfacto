package com.docfacto.links.data;

/**
 * An enum to represent the type of response a servlet gives.
 *
 * @author damonli - created Jul 24, 2013
 * @since 2.4.5
 */
public enum LinksServletPostResponseType {

    REDIRECT_TO_PAGE("redirect_to_page"),
    ERROR_EDITING_PROJECT("error_editing_project"), 
    PROJECT_EDITED("project_edited"), 
    NO_PROJECTS_ADDED("no_projects_added"),
    LOGIN_SUCCESSFUL("is_logged_in"), 
    LOGIN_FAILED("login_successful"), 
    IS_LOGGED_IN("is_logged_in"), 
    PROJECT_ATTRIBUTE_ERROR("project_attribute_error"), 
    PROJECTS_ADDED("projects_added"), 
    REFRESH("refresh"),
    ADD_PROJECT_ERROR("add_project_error"),
    REDIRECT_TO_PROJECT_KEY("redirect_to_project_key"), 
    NO_MORE_NEW_PROJECTS_ALLOWED("no_more_new_projects_allowed"), 
    NO_LIMIT_ON_PROJECTS("no_limit_on_projects"),
    PROJECTS_LEFT("projects_left");
    
    private final String name;
    
    private LinksServletPostResponseType(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
}
