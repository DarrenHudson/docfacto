package com.docfacto.links.data;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * This class contains common methods that can be used by servlet classes.
 * 
 * @author damonli - created 4 Sep 2013
 * @since 2.4.5
 */
public class ServletUtils {

    /**
     * Writes a response in the form of a json string to a given response object.
     * <p>
     * This method creates a response object for a links servlet response type and the string data to be posted, 
     * then converts this response object to a json string and writes it to the given HttpServletRespone object.
     * </p>
     * @param response the response to write the response data to.
     * @param responseType the type of response.
     * @param data the data to be posted.
     * @throws IOException when there was a problem writing the data to the response.
     * @since 2.4.5
     */
    public static void sendPostResponse(HttpServletResponse response, LinksServletPostResponseType responseType, String data) throws IOException {

        LinksServletPostResponse postResponse = new LinksServletPostResponse(responseType,data);

        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(postResponse);

        PrintWriter out = response.getWriter();
        out.print(json);
    }
}
