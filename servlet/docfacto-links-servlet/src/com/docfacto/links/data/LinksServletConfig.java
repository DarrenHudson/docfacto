package com.docfacto.links.data;

/**
 * An interface to be implemented by classes the represent a configuration for this servlet.
 * <p>
 * The Links Servlet can have it's login username, password and session interval configured. A class implementing
 * this interface should be able to retrieve these details.
 * </p>
 * @author damonli - created Sep 13, 2013
 * @since 2.4.6
 */
public interface LinksServletConfig {
    
    /**
     * Returns username.
     *
     * @return the username
     * @since 2.4.6
     */
    public String getUsername();
    
    /**
     * Returns password.
     *
     * @return the password
     * @since 2.4.6
     */
    public String getPassword();
    
    /**
     * Returns sessionInterval.
     *
     * @return the sessionInterval
     * @since 2.4.6
     */
    public int getSessionInterval();
}
