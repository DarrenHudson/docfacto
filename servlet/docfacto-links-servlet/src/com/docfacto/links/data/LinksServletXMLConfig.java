package com.docfacto.links.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.docfacto.common.IOUtils;
import com.docfacto.common.DocfactoException;

/**
 * This class represents the configuration for this the servlet with attributes from the Links Servlet config.xml file.
 * <p>
 * This class reads the /WEB-INF/config/config.xml file to get the necessary attributes for a configuration for
 * the servlet.
 * </p>
 * @author damonli - created 13 Sep 2013
 * @since 2.4.6
 */
public class LinksServletXMLConfig  implements LinksServletConfig {

    private final static String CONFIG_FILE_PATH = "/WEB-INF/config/config.xml";
    
    private static final String DEFAULT_USERNAME = "admin";
    private static final String DEFAULT_PASSWORD = "admin";
    private static final int DEFAULT_SESSION_INTERVAL = 20*60; // Default session interval is 20 minutes
    
    private String username;
    private String password;
    private Integer sessionInterval;
    
    private ServletContext servletContext;
    
    /**
     * Constructor.
     * @param servletContext the servlet context to get the config.xml file from.
     * @since 2.4.6
     */
    public LinksServletXMLConfig(ServletContext servletContext) {
        this.servletContext = servletContext;
        
        this.username = DEFAULT_USERNAME;
        this.password = DEFAULT_PASSWORD;
        this.sessionInterval = DEFAULT_SESSION_INTERVAL;
        
        File configFile;
        try {
            configFile = getConfigFile(CONFIG_FILE_PATH);
        
            if (configFile != null) {
                setConfigAttributesFromConfigFile(configFile);
            }
        }
        catch (ConfigFileParsingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    /**
     * Gets a config file object from a config file path.
     * 
     * @param configFilePath the path to the config file.
     * @return the config file object read from the given path.
     * @throws ConfigFileParsingException when there was an error generating the file object from the given path.
     * @since 2.4.6
     */
    private File getConfigFile(String configFilePath) throws ConfigFileParsingException {

        InputStream inputStream = servletContext.getResourceAsStream(configFilePath);
        
        File configFile;
        try {
            configFile = File.createTempFile("tempConfigXML", "");
        }
        catch (IOException e2) {
            e2.printStackTrace();
            throw new ConfigFileParsingException(e2.getMessage());
        }
        
        OutputStream outputStream;
        try {
            outputStream = new FileOutputStream(configFile);
            
            try {
                IOUtils.copyStream(inputStream,outputStream);
            }
            catch (DocfactoException e1) {
                e1.printStackTrace();
                throw new ConfigFileParsingException(e1.getMessage());
            }
            
            try {
                outputStream.close();
            }
            catch (IOException e1) {
                e1.printStackTrace();
                throw new ConfigFileParsingException(e1.getMessage());
            }
        }
        catch (FileNotFoundException e2) {
            e2.printStackTrace();
            throw new ConfigFileParsingException(e2.getMessage());
        }
        
        return configFile;
    }
    
    /**
     * Set the attributes from the config file.
     * <p>
     * This method looks for and parses a config xml file. When it finds the first 'username', 'password' and 
     * 'session-interval'tags, it sets these attributes for the instance of this class.
     * </p>
     * @param configFile the config file to get the usernamd and password from.
     * @throws ConfigFileParsingException when there was an error getting and parsing the config file's values
     * @since 2.4.6
     */
    private void setConfigAttributesFromConfigFile(File configFile) throws ConfigFileParsingException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            
            try {
                Document doc = dBuilder.parse(configFile);
                
                NodeList usernameNodeList = doc.getElementsByTagName("username");
                NodeList passwordNodeList = doc.getElementsByTagName("password");
                NodeList sessionIntervalNodeList = doc.getElementsByTagName("session-interval");
                
                Node firstUsernameNode = usernameNodeList.item(0);
                
                if (firstUsernameNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element usernameElement = (Element)firstUsernameNode;
                    this.username = usernameElement.getTextContent();
                }
                
                Node firstPasswordNode = passwordNodeList.item(0);
                
                if (firstPasswordNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element passwordElement = (Element)firstPasswordNode;
                    this.password = passwordElement.getTextContent();
                }
                
                Node firstSessionIntervalNode = sessionIntervalNodeList.item(0);
                
                if (firstSessionIntervalNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element sessionIntervalElement = (Element)firstSessionIntervalNode;
                    
                    try{
                        this.sessionInterval = Integer.parseInt(sessionIntervalElement.getTextContent());
                    } catch(NumberFormatException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            catch (SAXException e) {
                e.printStackTrace();
                throw new ConfigFileParsingException(e.getMessage());
            }
            catch (IOException e) {
                e.printStackTrace();
                throw new ConfigFileParsingException(e.getMessage());
            }
        }
        catch (ParserConfigurationException e) {
            e.printStackTrace();
            throw new ConfigFileParsingException(e.getMessage());
        }
    }

    /**
     * @see com.docfacto.links.data.LinksServletConfig#getUsername()
     */
    @Override
    public String getUsername() {
        return this.username;
    }

    /**
     * @see com.docfacto.links.data.LinksServletConfig#getPassword()
     */
    @Override
    public String getPassword() {
        return this.password;
    }

    /**
     * @see com.docfacto.links.data.LinksServletConfig#getSessionInterval()
     */
    @Override
    public int getSessionInterval() {
        return this.sessionInterval;
    }
}
