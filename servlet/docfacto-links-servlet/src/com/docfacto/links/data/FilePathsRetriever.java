package com.docfacto.links.data;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class contains methods to retrieve the paths of all files in given directories.
 * 
 * @author damonli - created Jul 8, 2013
 * @since 2.4.5
 */
public class FilePathsRetriever {

    private List<String> fileExtensions;

    /**
     * Constructs the file paths retriever for a given list of extensions. The retriever will only consider 
     * files which have an extension which is given in this list to be a file.
     * 
     * @param extensions the list of extensions which the retriever will return file paths for.
     * @since 2.4.5
     */
    public FilePathsRetriever(List<String> extensions) {
        this.fileExtensions = extensions;
    }

    /**
     * Gets the paths of all the files for a given directory.
     * 
     * @param directory the directory to get the file paths for.
     * @return the list of paths of all files for the given directory.
     * @since 2.4.5
     */
    public List<String> getFilesFromDirectory(String directory) {

        File directoryFile = new File(directory);
        List<String> filePathsList = new ArrayList<String>();

        if (directoryFile.isDirectory()) {
            List<File> docFileList = getFilesFromDirs(Arrays.asList(directoryFile.listFiles()));
            
            for (File file:docFileList) {
                if (file.isFile())
                    filePathsList.add(file.getAbsolutePath());
            }
        }
        else {
            if (fileExtensionIsAnAcceptedExtension(directoryFile))
                filePathsList.add(directoryFile.getAbsolutePath());
        }

        return filePathsList;
    }

    /**
     * Returns the list of files from a list of directories
     * <p>
     * This method traverses each of the given directories and returns a list of
     * Files which have an extension which is an accepted documentation
     * extension.
     * </p>
     * 
     * @param dirs the list of directories to check for files
     * @return the list of documentation files
     * @since 2.4.5
     */
    private List<File> getFilesFromDirs(List<File> dirs) {
        List<File> files = new ArrayList<File>();
        for (File dir:dirs) {
            if (dir.isDirectory())
                files.addAll(getFilesFromDirs(Arrays.asList(dir.listFiles())));
            else {
                if (fileExtensionIsAnAcceptedExtension(dir))
                    files.add(dir);
            }
        }
        return files;
    }

    /**
     * Checks whether a file's extension is a valid extension
     * <p>
     * This method gets the list of accepted file extensions from
     * LinksConfig and then returns whether the given file extension matches
     * on the extensions in this list.
     * </p>
     * 
     * @param file the document file to check
     * @return whether the given file's extension is a valid documentation
     * extension
     * @since 2.4.5
     */
    private boolean fileExtensionIsAnAcceptedExtension(File file) {
        for (String acceptedDocExtension:fileExtensions) {
            if (file.getName().endsWith(acceptedDocExtension))
                return true;
        }
        return false;
    }
}
