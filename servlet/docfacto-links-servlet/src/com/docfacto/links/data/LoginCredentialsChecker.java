package com.docfacto.links.data;

/**
 * An interface implemented by classes which can verify whether a username and password combination
 * if correct for logging in.
 *
 * @author damonli - created 6 Sep 2013
 * @since 2.4.5
 */
public interface LoginCredentialsChecker {

    /**
     * Verifyies whether a username and password combination are correct for logging in.
     * 
     * @param username the username to verify
     * @param password the password to verify
     * @return whether the username and password combination are accepted
     * @since 2.4.5
     */
    public boolean verifyUsernameAndPassword(String username,String password);
    
}
