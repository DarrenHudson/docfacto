package com.docfacto.links.data;

public class NoNewProjectsAllowedException extends Exception {
	
	public NoNewProjectsAllowedException() {
		super("No more projects allowed");
	}
}
