package com.docfacto.links.data;

import com.docfacto.output.generated.Result;

/**
 * A data class to hold a result and a snapshot of the code/doc for that result.
 *
 * @author damonli - created Aug 2, 2013
 * @since 2.4.5
 */
public class ResultsData {

    private final Result result;
    private final Snapshot snapshot;
    
    /**
     * Constructor.
     * @param result the result this results data object is for
     * @param snapshot the snapshot of this result
     */
    public ResultsData(Result result, Snapshot snapshot) {
        this.result = result;
        this.snapshot = snapshot;
    }
}
