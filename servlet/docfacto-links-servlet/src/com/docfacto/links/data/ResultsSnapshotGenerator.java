package com.docfacto.links.data;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.output.generated.Position;
import com.docfacto.output.generated.Result;

/**
 * This class contains methods to generate a snapshot for a result. A snapshot is an extract of the source/doc file
 * associated with the result including the position the result is on. This acts as a preview to highlight the 
 * area of the file in question.
 *
 * @author damonli - created Aug 1, 2013
 * @since 2.4.5
 */
public class ResultsSnapshotGenerator {
    /*
     * The number of lines a snapshot is
     */
    private static final int SNAPSHOT_SIZE = 11;
    /*
     * The number of lines before the line the result is on to begin the snapshot
     */
    private static final int LINES_BEFORE_RESULT_LINE_TO_START_SNAPSHOT = 5;
    
    /**
     * This method generates a snapshot object for a given result.
     * <p>
     * This method reads the file the result is for, beginning from the result's line position and returns the
     * snapshot object created from this. <b />
     * </p>
     * @param result the result to generate a snapshot for.
     * @return the snapshot generated for the given result.
     * @throws IOException when there was a problem reading a line from the file associated with the given result.
     * @throws DocfactoException when there was a problem reading the file associated with the given result.
     * @since 2.4.5
     */
    public Snapshot getSnapshotForResult(Result result) throws IOException, DocfactoException {
        Position position = result.getPosition();
        String file = position.getFile();
        int resultLinePosition = position.getLine();
        LineNumberReader lineNumberReader = IOUtils.createFileReader(file);
        
        List<String> snapshotStringList = new ArrayList<String>();
        
        int noOfLinesRead = 0;
        int initialLineToBeRead = getInitialLineToBeRead(resultLinePosition);
        int resultLineInSnapshot = LINES_BEFORE_RESULT_LINE_TO_START_SNAPSHOT-1;
        
        if (initialLineToBeRead == 0) {
            resultLineInSnapshot = resultLinePosition-2;
        }
        
        lineNumberReader.setLineNumber(initialLineToBeRead);
        
        for (int i=0; i < initialLineToBeRead-1; i++) {
            lineNumberReader.readLine();
        }
        
        String stringBeingRead = "";
        
        while((stringBeingRead=lineNumberReader.readLine()) != null) {
            snapshotStringList.add(stringBeingRead);
            noOfLinesRead++;
            
            if (noOfLinesRead == SNAPSHOT_SIZE) 
                break;
        }
        
        return new Snapshot(resultLineInSnapshot, snapshotStringList.toArray(new String[snapshotStringList.size()]));
    }

    /**
     * Gets the line number a snapshot should begin from for a given result line position.
     * 
     * @param resultLinePosition the result's line position to calculate the starting line from.
     * @return the line to start the snapshot from.
     * @since 2.4.5
     */
    private int getInitialLineToBeRead(int resultLinePosition) {
        int initialLineToBeRead = resultLinePosition - LINES_BEFORE_RESULT_LINE_TO_START_SNAPSHOT;
        
        if (initialLineToBeRead <= 0)
            return 0;
        else 
            return initialLineToBeRead;
        
    }
}
