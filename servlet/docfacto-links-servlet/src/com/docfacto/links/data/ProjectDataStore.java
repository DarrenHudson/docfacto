package com.docfacto.links.data;

import java.util.HashMap;
import java.util.Map;

/**
 * An enum singleton to store all the projects. 
 * <p>
 * This class stores all the links projects into a map, which maps the project key
 * to the project, so a project can be retrieved by giving its key.
 * </p>
 * <p>
 * This class is also responsible for calculating whether there is a restriction on 
 * new projects, and if so, how many are there allowed to be added.
 * </p>
 * @author damonli - created Jul 22, 2013
 * @since 2.4.5
 */
public enum ProjectDataStore {

    INSTANCE;
    
    /**
     * The minimum number of projects that must be allowed if there is a restriction.
     */
    public final int MINIMUM_PROJECTS_ALLOWED = 3;
    /**
     * If ProjectDataStore returns the number of projects allowed for a given project is equal to this, it means there 
     * is no limit to the number of projects allowed for this project.
     */
    public final int INFINITE_PROJECTS_ALLOWED_VALUE = -1;
    /**
     * If the restriction value for links in the XML Config is equal to this, it means there is no restriction on 
     * the number of projects.
     */
    public final int INFINITE_PROJECTS_ALLOWED_RESTRICTION_VALUE = 0;
    
    private Map<String, LinksProject> linksProjects;
    private int projectsLeft;
    
    
    private ProjectDataStore() {
        linksProjects = new HashMap<String, LinksProject>();
        projectsLeft = MINIMUM_PROJECTS_ALLOWED;
    }
    
    /**
     * Add a links project to the projects store.
     * @param project the project to store.
     * @throws NoNewProjectsAllowedException no new projects are allowed.
     */
    public void addLinksProject(LinksProject project) throws NoNewProjectsAllowedException {
    	if (canAddMoreProjects()) {
    		linksProjects.put(project.getKey(), project);
        	recalculateProjectsRestriction();
    	}
    	else {
    		throw new NoNewProjectsAllowedException();
    	}
    }
    
    private int getProjectsAllowedFromProject(LinksProject linksProject) {
    	Integer restrictionValue = linksProject.getMaxFileLimit();
    	
    	if (restrictionValue == INFINITE_PROJECTS_ALLOWED_RESTRICTION_VALUE) {
    		return INFINITE_PROJECTS_ALLOWED_VALUE;
    	}
    	else {
    		return MINIMUM_PROJECTS_ALLOWED;
    	}
    }
    
    /**
     * Update the project store to recalculate how many projects are allowed still.
     */
    public void recalculateProjectsRestriction() {
    	for (LinksProject linksProject : linksProjects.values()) {
    		int projectsAllowedFromProject = getProjectsAllowedFromProject(linksProject);
    		if (projectsAllowedFromProject == INFINITE_PROJECTS_ALLOWED_VALUE) {
    			projectsLeft = INFINITE_PROJECTS_ALLOWED_VALUE;
    			return;
    		}
    	}
    	
    	// No projects with an unrestricted config, therefore, set the projects left
    	projectsLeft = MINIMUM_PROJECTS_ALLOWED - linksProjects.values().size();
    }
    
    private boolean canAddMoreProjects() {
    	return (projectsLeft > 0) || projectsLeft == INFINITE_PROJECTS_ALLOWED_VALUE;
    }
    
    /**
     * Get the project with the given key.
     * @param projectKey the key to get the project for.
     * @return the project with the given key.
     */
    public LinksProject getLinksProject(String projectKey) {
        return linksProjects.get(projectKey);
    }
    
    /**
     * Remove the project with the given key from the project store.
     * @param projectKey the project with the project key to remove.
     */
    public void removeLinksProject(String projectKey) {
        linksProjects.remove(projectKey);
        recalculateProjectsRestriction();
    }

    /**
     * Get the links projects map containing the projects stored, which maps a project key to the project 
     * with that project key.
     * @return the map containing the projects stored.
     */
    public Map<String, LinksProject> getLinksProjects() {
    	return linksProjects;
    }
    
    /**
     * Get the number of projects left which can be added.
     * @return the number of projects left which can be added.
     */
	public int getProjectsLeft() {
		return projectsLeft;
	}
}
