package com.docfacto.links.data;

import java.util.ArrayList;
import java.util.List;

/**
 * A data class to store the attributes for a project's configuration.
 * <p>
 * This data class stores the necessary attributes for a project's configuration. This includes: <b />
 * The name given to the project. <b />
 * The working directory for the project <b />
 * The directory of the source files. <b />
 * The directory of the documentation files. <b />
 * The directory of the xml config file. <b />
 * The type of the source files to be analysed. <b />
 * The type of the documentation files to be analysed. <b />
 * The possible extensions of the source files. <b />
 * The possible extensions of the documentation files.
 * </p>
 * @author damonli - created Jul 16, 2013
 * @since 2.4.5
 */
public class ProjectConfiguration {
    
    private String projectName;
    private String workingDirectory;
    private String sourceDirectory;
    private String docDirectory;
    private String xmlConfigDirectory;
    private List<String> sourceExtensions;
    private List<String> docExtensions;
    
    /**
     * Constructor.
     * @since 2.4.5
     */
    public ProjectConfiguration() {
        setProjectName("");
        setWorkingDirectory("");
        setSourceDirectory("");
        setDocDirectory("");
        setXmlConfigDirectory("");
        setSourceExtensions(new ArrayList<String>());
        setDocExtensions(new ArrayList<String>());
    }

    /**
     * Returns projectName.
     *
     * @return the projectName
     * @since 2.4.5
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * Sets projectName.
     *
     * @param projectName the projectName value
     * @since 2.4.5
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /**
     * Returns sourceDirectory.
     *
     * @return the sourceDirectory
     * @since 2.4.5
     */
    public String getSourceDirectory() {
        return sourceDirectory;
    }

    /**
     * Sets sourceDirectory.
     *
     * @param sourceDirectory the sourceDirectory value
     * @since 2.4.5
     */
    public void setSourceDirectory(String sourceDirectory) {
        this.sourceDirectory = sourceDirectory;
    }

    /**
     * Returns docDirectory.
     *
     * @return the docDirectory
     * @since 2.4.5
     */
    public String getDocDirectory() {
        return docDirectory;
    }

    /**
     * Sets docDirectory.
     *
     * @param docDirectory the docDirectory value
     * @since 2.4.5
     */
    public void setDocDirectory(String docDirectory) {
        this.docDirectory = docDirectory;
    }

    /**
     * Returns xmlConfigDirectory.
     *
     * @return the xmlConfigDirectory
     * @since 2.4.5
     */
    public String getXmlConfigDirectory() {
        return xmlConfigDirectory;
    }

    /**
     * Sets xmlConfigDirectory.
     *
     * @param xmlConfigDirectory the xmlConfigDirectory value
     * @since 2.4.5
     */
    public void setXmlConfigDirectory(String xmlConfigDirectory) {
        this.xmlConfigDirectory = xmlConfigDirectory;
    }

    /**
     * Returns sourceExtensions.
     *
     * @return the sourceExtensions
     * @since 2.4.5
     */
    public List<String> getSourceExtensions() {
        return sourceExtensions;
    }

    /**
     * Sets sourceExtensions.
     *
     * @param sourceExtensions the sourceExtensions value
     * @since 2.4.5
     */
    public void setSourceExtensions(List<String> sourceExtensions) {
        this.sourceExtensions = sourceExtensions;
    }

    /**
     * Returns docExtensions.
     *
     * @return the docExtensions
     * @since 2.4.5
     */
    public List<String> getDocExtensions() {
        return docExtensions;
    }

    /**
     * Sets docExtensions.
     *
     * @param docExtensions the docExtensions value
     * @since 2.4.5
     */
    public void setDocExtensions(List<String> docExtensions) {
        this.docExtensions = docExtensions;
    }

    /**
     * Returns workingDirectory.
     *
     * @return the workingDirectory
     * @since 2.4.5
     */
    public String getWorkingDirectory() {
        return workingDirectory;
    }

    /**
     * Sets workingDirectory.
     *
     * @param workingDirectory the workingDirectory value
     * @since 2.4.5
     */
    public void setWorkingDirectory(String workingDirectory) {
        this.workingDirectory = workingDirectory;
    }
    
}
