package com.docfacto.links.data;

/**
 * A data class to hold a snapshot of source/doc for a result.
 * <p>
 * This class holds the line of a result this snapshot is for, and an array of strings which represents each line
 * in the snapshot. <br />
 * 
 * A 'snapshot' is an extract of code/documentation which can be used as a preview to highlight the problem
 * area for a result.
 * </p>
 * @author damonli - created Aug 6, 2013
 * @since 2.4.5
 */
public class Snapshot {

    private int resultLine;
    private String[] snapshotLines;
    
    /**
     * Constructor.
     * @param resultLine the line the result is on
     * @param snapshotLines the lines in the snapshot
     */
    public Snapshot(int resultLine, String[] snapshotLines) {
        this.resultLine = resultLine;
        this.snapshotLines = snapshotLines;
    }
}
