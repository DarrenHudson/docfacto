package com.docfacto.links.data;

/**
 * This is an exception which is thrown where there was an error when parsing the Config File
 *
 * @author damonli - created 6 Sep 2013
 * @since 2.4.5
 */
public class ConfigFileParsingException extends Exception {

    /**
     * Constructor.
     * @since 2.4.5
     */
    public ConfigFileParsingException() {
        super("Error when parsing config file");
    }
    
    /**
     * Constructor.
     * @param message a specific detail message
     * @since 2.4.5
     */
    public ConfigFileParsingException(String message) {
        super(message);
    }
}
