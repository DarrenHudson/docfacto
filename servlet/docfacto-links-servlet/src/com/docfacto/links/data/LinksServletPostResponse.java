package com.docfacto.links.data;

/**
 * A data class to represent a get/post response from a servlet which contains
 * the response type and the string data it holds.
 *
 * @author damonli - created Jul 24, 2013
 * @since 2.4.5
 */
public class LinksServletPostResponse {

    private final LinksServletPostResponseType type;
    private final String data;
    
    /**
     * Constructor.
     * @param type the type of the response
     * @param data the message data for the response
     * @since 2.4.5
     */
    public LinksServletPostResponse(LinksServletPostResponseType type, String data) {
        this.type = type;
        this.data = data;
    }
}
