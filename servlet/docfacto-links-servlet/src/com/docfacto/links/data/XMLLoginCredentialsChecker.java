package com.docfacto.links.data;

/**
 * This class is responsible for verifying a username and password combination according to the
 * username and password set in the config file.
 * 
 * @author damonli - created 6 Sep 2013
 * @since 2.4.5
 */
public class XMLLoginCredentialsChecker implements LoginCredentialsChecker {
    private String username;
    private String password;

    /**
     * Constructor.
     * @param linksServletConfig the configuration to get the username and password from
     * @since 2.4.5
     */
    public XMLLoginCredentialsChecker(LinksServletConfig linksServletConfig) {
        setUsernameAndPasswordFromConfigFile(linksServletConfig);
    }

    /**
     * Set the username and password for this credentials checker from the given servlet config.
     * </p>
     * @param servletConfig the servlet configuration object to get the username and password from.
     * @since 2.4.5
     */
    private void setUsernameAndPasswordFromConfigFile(LinksServletConfig servletConfig) {
        this.username = servletConfig.getUsername();
        this.password = servletConfig.getPassword();
    }

    /**
     * @see com.docfacto.links.data.LoginCredentialsChecker#verifyUsernameAndPassword(java.lang.String,
     * java.lang.String)
     */
    @Override
    public boolean verifyUsernameAndPassword(String usernameInput,String passwordInput) {
        if (this.username != null && this.password != null) {
            return (usernameInput.equals(this.username) && passwordInput.equals(this.password));
        }
        else {
            return false;
        }
    }
}
