package com.docfacto.links.servlets;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.docfacto.config.XmlConfig;
import com.docfacto.exceptions.InvalidConfigException;
import com.docfacto.licensor.Products;
import com.docfacto.licensor.UDCProcessor;
import com.docfacto.links.data.LinksProject;
import com.docfacto.links.data.LinksRunner;
import com.docfacto.links.data.LinksServletPostResponseType;
import com.docfacto.links.data.NoNewProjectsAllowedException;
import com.docfacto.links.data.ProjectConfiguration;
import com.docfacto.links.data.ProjectDataStore;
import com.docfacto.links.data.ServletUtils;

/**
 * Servlet implementation class LinksServlet
 * @author damonli
 * @since 2.4.5
 */
public class LinksServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LinksServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
	    
	    RequestDispatcher requestDispatcher = request.getRequestDispatcher("index.jsp");
        try {
            requestDispatcher.forward(request,response);
        }
        catch (ServletException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
	    String action = request.getParameter("action");
	    
        if (action != null) {
            if (action.equals("addNewProject")) {
                String newProjectName = request.getParameter("newProjectName");
                
                System.out.println("Adding new links project: " + newProjectName);
                String newWorkingDirectory = request.getParameter("newWorkingDirectory");
                String newSourceDirectory = request.getParameter("newSourceDirectory");
                String newDocDirectory = request.getParameter("newDocDirectory");
                String newXMLConfigDirectory = request.getParameter("newXMLConfigDirectory");
                
                String sourceExtensionsString = request.getParameter("newSourceExtensions");
                List<String> newSourceExtensions = splitStringOnCommasAndSpaces(sourceExtensionsString);
                
                String docExtensionsString = request.getParameter("newDocExtensions");
                List<String> newDocExtensions = splitStringOnCommasAndSpaces(docExtensionsString);
                
                ProjectConfiguration projectConfiguration = new ProjectConfiguration();
                projectConfiguration.setWorkingDirectory(newWorkingDirectory);
                projectConfiguration.setProjectName(newProjectName);
                projectConfiguration.setSourceDirectory(newSourceDirectory);
                projectConfiguration.setDocDirectory(newDocDirectory);
                projectConfiguration.setXmlConfigDirectory(newXMLConfigDirectory);
                projectConfiguration.setSourceExtensions(newSourceExtensions);
                projectConfiguration.setDocExtensions(newDocExtensions);
                
                LinksProject linksProject = new LinksProject(projectConfiguration);

                try {
					ProjectDataStore.INSTANCE.addLinksProject(linksProject);
				} catch (NoNewProjectsAllowedException e) {
					ServletUtils.sendPostResponse(response, LinksServletPostResponseType.NO_MORE_NEW_PROJECTS_ALLOWED, e.getMessage());
					return;
				}
                
                runLinksOnProject(linksProject);
                
                ServletUtils.sendPostResponse(response, LinksServletPostResponseType.REDIRECT_TO_PROJECT_KEY, linksProject.getKey());
            }
            else if (action.equals("runLinksForProject")) {
                String projectKey = request.getParameter("projectKey");
                System.out.println("Running links for project: " + projectKey);
                LinksProject linksProject = ProjectDataStore.INSTANCE.getLinksProject(projectKey);
                runLinksOnProject(linksProject);
                ServletUtils.sendPostResponse(response, LinksServletPostResponseType.REFRESH, "");
            }
            else if (action.equals("deleteProject")) {
                String projectKey = request.getParameter("projectKey");
                System.out.println("Deleting project: " + projectKey);
                ProjectDataStore.INSTANCE.removeLinksProject(projectKey);
                ServletUtils.sendPostResponse(response, LinksServletPostResponseType.REDIRECT_TO_PAGE, "index.jsp");
            }
            else if (action.equals("EditProject")) {
                String projectKey = request.getParameter("projectKey");
                String newProjectName = request.getParameter("newProjectName");
                String newWorkingDirectory = request.getParameter("newWorkingDirectory");
                String newSourceDirectory = request.getParameter("newSourceDirectory");
                String newDocDirectory = request.getParameter("newDocDirectory");
                String newXMLConfigDirectory = request.getParameter("newXMLConfigDirectory");
                
                String sourceExtensionsString = request.getParameter("newSourceExtensions");
                List<String> newSourceExtensions = splitStringOnCommasAndSpaces(sourceExtensionsString);
                
                String docExtensionsString = request.getParameter("newDocExtensions");
                List<String> newDocExtensions = splitStringOnCommasAndSpaces(docExtensionsString);
                
                LinksProject project = ProjectDataStore.INSTANCE.getLinksProjects().get(projectKey);
                ProjectConfiguration projectConfiguration = project.getProjectConfiguration();
                projectConfiguration.setProjectName(newProjectName);
                projectConfiguration.setWorkingDirectory(newWorkingDirectory);
                projectConfiguration.setSourceDirectory(newSourceDirectory);
                projectConfiguration.setDocDirectory(newDocDirectory);
                projectConfiguration.setXmlConfigDirectory(newXMLConfigDirectory);
                projectConfiguration.setSourceExtensions(newSourceExtensions);
                projectConfiguration.setDocExtensions(newDocExtensions);
                
                ProjectDataStore.INSTANCE.recalculateProjectsRestriction();
                
                System.out.println("Edited project: " + projectKey);
                ServletUtils.sendPostResponse(response, LinksServletPostResponseType.PROJECT_EDITED, "edited project: " + newProjectName);
            }
            else if (action.equals("forwardToSourceAnalysis")) {
                ServletUtils.sendPostResponse(response, LinksServletPostResponseType.REDIRECT_TO_PAGE, "index.jsp");
            }
            else
                System.out.println(this.getServletName() + ": Don't know what to do with this post: " + action);
        }
	}
	
	/**
	 * Runs links on a given project.
	 * <p>
	 * This method runs links for a given project, and notifies the udc processor of this.
	 * </p>
	 * @param linksProject the project to run links for.
	 * @since 2.4.5
	 */
	private void runLinksOnProject(LinksProject linksProject) {
	    notifyUDCProcessorLinksIsBeingRun(linksProject);
	    LinksRunner.runLinksOnProject(linksProject);
	}
	
	/**
	 * Gets the docfacto xml config object from a given docfacto xml config directory
	 * 
	 * @param xmlConfigDirectory the directory to get the docfacto xml config from.
	 * @return the xml config object for the given docfacto xml config directory.
	 * @since 2.4.5
	 */
	private XmlConfig getXmlConfigForDirectory(String xmlConfigDirectory) {
        File file = new File(xmlConfigDirectory);
        try {
            return new XmlConfig(file);
        }
        catch (InvalidConfigException e) {
            e.printStackTrace();
        }
        
        return null;
	}
	
	/**
     * Notify the UDC Processor that Links is being run from Sonar
     * 
     * @since 2.4.5
     */
    private void notifyUDCProcessorLinksIsBeingRun(LinksProject project) {
        String xmlConfigFile = project.getProjectConfiguration().getXmlConfigDirectory();
        File file = new File(xmlConfigFile);
        try {
            XmlConfig xmlConfig = new XmlConfig(file);
            UDCProcessor.registerProduct("Docfacto Links Runner",xmlConfig.getProduct(Products.LINKS),xmlConfig);
        }
        catch (InvalidConfigException e) {
            System.out.println("Error notifying UDC Processor, could not create XmlConfig");
            e.printStackTrace();
        }
    }
    
    /**
     * Splits a given string on commas and spaces
     * 
     * @param stringToBeSplit the string to be split
     * @return the list of strings from the given string split on commas and spaces
     * @since 2.4.5
     */
    private List<String> splitStringOnCommasAndSpaces(String stringToBeSplit) {
        String regularExpression = ",\\s*";
        return Arrays.asList(stringToBeSplit.split(regularExpression));
    }
}
