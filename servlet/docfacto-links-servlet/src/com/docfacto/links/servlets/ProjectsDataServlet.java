package com.docfacto.links.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.docfacto.links.data.LinksProject;
import com.docfacto.links.data.LinksServletPostResponseType;
import com.docfacto.links.data.ProjectDataStore;
import com.docfacto.links.data.ServletUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class ProjectsDataServlet
 * @author damonli
 * @since 2.4.5
 */
public class ProjectsDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProjectsDataServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    
	    String action = request.getParameter("action");
        if (action != null) {
            if (action.equals("getProjectWithKey")) {
                String projectKey = request.getParameter("projectKey");
                
                LinksProject project = ProjectDataStore.INSTANCE.getLinksProject(projectKey);
                
                Gson gson = new GsonBuilder().create();
                String json = gson.toJson(project);
                
                PrintWriter out= response.getWriter();
                out.print(json);
            }
            else if (action.equals("getProjectsLeft")) {
            	int projectsLeft = ProjectDataStore.INSTANCE.getProjectsLeft();
            	
            	if (projectsLeft == ProjectDataStore.INSTANCE.INFINITE_PROJECTS_ALLOWED_VALUE) {
            		ServletUtils.sendPostResponse(response, LinksServletPostResponseType.NO_LIMIT_ON_PROJECTS, "There are no limits on the number of projects");
            	}
            	else {
            		ServletUtils.sendPostResponse(response, LinksServletPostResponseType.PROJECTS_LEFT, projectsLeft + "");
            	}
            }
            else {
                System.out.println(this.getServletName() + ": Don't know what to do with this action: " + action);
            }
        }
        else {
    	    if (ProjectDataStore.INSTANCE.getLinksProjects().size() == 0) {
    	        ServletUtils.sendPostResponse(response, LinksServletPostResponseType.NO_PROJECTS_ADDED, "No projects added yet");
    	    }
    	    else {
        	    Gson gson = new GsonBuilder().create();
        	    String json = gson.toJson(ProjectDataStore.INSTANCE.getLinksProjects());
        	    
        	    /*PrintWriter out= response.getWriter();
                out.print(json);*/
        	    ServletUtils.sendPostResponse(response, LinksServletPostResponseType.PROJECTS_ADDED, json);
    	    }
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("index.jsp");
        try {
            requestDispatcher.forward(request,response);
        }
        catch (ServletException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}

}
