package com.docfacto.links.servlets;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.docfacto.links.data.LinksServletConfig;
import com.docfacto.links.data.LinksServletPostResponseType;
import com.docfacto.links.data.LinksServletXMLConfig;
import com.docfacto.links.data.LoginCredentialsChecker;
import com.docfacto.links.data.ServletUtils;
import com.docfacto.links.data.XMLLoginCredentialsChecker;

/**
 * Servlet implementation class AdminServlet
 * @author damonli
 * @since 2.4.6
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private LinksServletConfig servletXMLConfig;
	private LoginCredentialsChecker loginCredentialsChecker;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
    }
    
    /**
     * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
     */
    public void init(ServletConfig config) throws ServletException {  
        super.init(config);
        servletXMLConfig = new LinksServletXMLConfig(this.getServletContext());
        loginCredentialsChecker = new XMLLoginCredentialsChecker(servletXMLConfig);
    }  

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String action = request.getParameter("action");

        if (action != null) {
            if (action.equals("getLoginStatus")) {
                System.out.println("Checking Login Status");
                HttpSession session = request.getSession();
                int sessionInterval = servletXMLConfig.getSessionInterval();
                session.setMaxInactiveInterval(sessionInterval); //Set the session interval by default to 20 minutes
                
                if (session != null && session.getAttribute("isLoggedIn") != null && (Boolean)session.getAttribute("isLoggedIn")) {
                    ServletUtils.sendPostResponse(response, LinksServletPostResponseType.IS_LOGGED_IN, "true");
                }
                else {
                    ServletUtils.sendPostResponse(response, LinksServletPostResponseType.IS_LOGGED_IN, "false");
                }
            }
        }
	}

    /**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
	    String action = request.getParameter("action");
	    HttpSession session = request.getSession(true);
	    
	    if (action == null) {
	        session.setAttribute("isLoggedIn", false);
	    }
	    else if (action.equals("login")) {
	        System.out.println("Attempting to log in");
	        if (loginCredentialsChecker == null) {
	            System.out.println("BUG: LoginCredentialsChecker was not initialized");
	            session.setAttribute("isLoggedIn", false);
	        }
	        else {
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                
                boolean usernameAndPasswordCorrect = loginCredentialsChecker.verifyUsernameAndPassword(username, password);
                
                if (usernameAndPasswordCorrect) {
                    System.out.println("Login successful");
                    session.setAttribute("isLoggedIn", true);
                    ServletUtils.sendPostResponse(response,LinksServletPostResponseType.LOGIN_SUCCESSFUL, "Login successful");
                    return;
                }
                else {
                    System.out.println("Login failed: username and password are incorrect");
                    ServletUtils.sendPostResponse(response,LinksServletPostResponseType.LOGIN_FAILED, "Username and password are incorrect");
                    return;
                }
	        }
	    }
	    else if (action.equals("logout")) {
	        System.out.println("Logging out");
	        session.setAttribute("isLoggedIn", false);
	    }
	    else {
	        System.out.println(this.getServletName() + ": Don't know what to do with this post: " + action);
	    }
        
        //redirectBackToPage(request, response);
	}
}
