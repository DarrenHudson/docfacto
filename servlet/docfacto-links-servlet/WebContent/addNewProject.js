function addProject(event) {
	var projectName=$('#newProjectName').val();
	var workingDirectory=$('#newWorkingDirectory').val();
	var sourceDirectory=$('#newSourceDirectory').val();
	var docDirectory=$('#newDocDirectory').val();
	var xmlConfigDirectory=$('#newXMLConfigDirectory').val();
	var sourceExtensions=$('#newSourceExtensions').val();
	var docExtensions=$('#newDocExtensions').val();
	
	if (stringIsEmpty(projectName)) {
		alert("Please set a project name");
		return;
	}
	
	$.post('/docfacto-links-servlet/LinksServlet',
		{ action:"addNewProject",
			newProjectName:projectName,
			newWorkingDirectory:workingDirectory,
			newSourceDirectory:sourceDirectory,
			newDocDirectory:docDirectory,
			newXMLConfigDirectory:xmlConfigDirectory,
			newSourceExtensions:sourceExtensions,
			newDocExtensions:docExtensions
		},
		function(data) {
			var linksServletResponse = $.parseJSON(data);
			var type = linksServletResponse.type;
			alert("type: " + type);
			if (type == "REDIRECT_TO_PAGE") {
				var pageToRedirectTo = linksServletResponse.data;
				window.location.href = pageToRedirectTo;
			}
			else if (type == "NO_MORE_NEW_PROJECTS_ALLOWED") {
				var numberOfProjectsAllowed = linksServletResponse.data;
				alert("You have reached the maximum number of projects allowed (" + numberOfProjectsAllowed + ") for your license");
			}
			else {
				alert("Don't know what to do");
			}
		}
	);
}

function stringIsEmpty(inputString) {
	// Check if the browser support the 'trim()' method, if not, do the trim
	if (!('trim' in String.prototype)) {
	    String.prototype.trim= function() {
	        inputString.replace(/^\s+/, '').replace(/\s+$/, '');
	    };
	}
	
	return (inputString.trim() === '');
}