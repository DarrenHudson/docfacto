function fillInProjectDetailsPageForProject(projectKey, project) {
	
	var projectConfiguration = project.projectConfiguration;
	var linksProcessingErrors = project.linksProcessingErrors;
	
	var projectConfigurationString = getProjectConfigurationHTMLString(projectKey, projectConfiguration);
	
	var editProjectButtonString = getEditProjectButtonHTMLString(projectKey);
	var cancelEditProjectButtonString = getCancelEditProjectButtonHTMLString(projectKey);
	var saveProjectChangesButtonString = getSaveProjectChangesButtonHTMLString(projectKey);
	var saveAndRunProjectChangesButtonString = getSaveAndRunProjectChangesButtonHTMLString(projectKey);
	var deleteProjectButtonString = getDeleteProjectButtonHTMLString(projectKey);
	
	var projectConfigurationButtonsString = editProjectButtonString + "  " 
												+ cancelEditProjectButtonString
												+ saveProjectChangesButtonString
												+ saveAndRunProjectChangesButtonString
												+ "<br />"
										+ deleteProjectButtonString + "<br />";
	
	$('#projectConfigurationForm').html(projectConfigurationString);
	$('#projectConfigurationButtons').html(projectConfigurationButtonsString);
	
	if (linksProcessingErrors.length > 0) {
		var projectErrorsHTMLString = getProjectErrorsHTMLString(linksProcessingErrors);
		$('#projectErrors').html(projectErrorsHTMLString);
	}
}

function getProjectConfigurationHTMLString(projectKey, projectConfiguration) {
	var htmlString = "";
	
	htmlString += '<div id="projectDetailsForm">';
	
	htmlString += '<label class="projectDetailsFormLabel">Project Name</label>';
	htmlString += '<input disabled="disabled" style="border:0px;" size="100" id="' + projectKey + 'ProjectNameField" class="' + projectKey + 'ProjectConfigurationField" projectKey="' + projectKey + '" value="';
	htmlString += projectConfiguration.projectName;
	htmlString += '" />';
	htmlString += '<br>';
	
	htmlString += '<label class="projectDetailsFormLabel">Source Directory</label>';
	htmlString += '<input disabled="disabled" style="border:0px;" size="100" id="' + projectKey + 'SourceDirectoryField" class="' + projectKey + 'ProjectConfigurationField" projectKey="' + projectKey + '" value="';
	htmlString += projectConfiguration.sourceDirectory;
	htmlString += '" />';
	htmlString += '<br>';
	
	htmlString += '<label class="projectDetailsFormLabel">Documentation Directory</label>';
	htmlString += '<input disabled="disabled" style="border:0px;" size="100" id="' + projectKey + 'DocDirectoryField" class="' + projectKey + 'ProjectConfigurationField" projectKey="' + projectKey + '" value="';
	htmlString += projectConfiguration.docDirectory;
	htmlString += '" />';
	htmlString += '<br>';
	
	htmlString += '<label class="projectDetailsFormLabel">XML Configuration Directory</label>';
	htmlString += '<input disabled="disabled" style="border:0px;" size="100" id="' + projectKey + 'XmlConfigDirectoryField" class="' + projectKey + 'ProjectConfigurationField" projectKey="' + projectKey + '" value="';
	htmlString += projectConfiguration.xmlConfigDirectory;
	htmlString += '" />';
	htmlString += '<br>';
	
	htmlString += '<label class="projectDetailsFormLabel">Source File Extensions</label>';
	htmlString += '<input disabled="disabled" style="border:0px;" size="100" id="' + projectKey + 'SourceExtensionsField" class="' + projectKey + 'ProjectConfigurationField" projectKey="' + projectKey + '" value="';
	htmlString += projectConfiguration.sourceExtensions.join();
	htmlString += '" />';
	htmlString += '<br>';
	
	htmlString += '<label class="projectDetailsFormLabel">Doc File Extensions</label>';
	htmlString += '<input disabled="disabled" style="border:0px;" size="100" id="' + projectKey + 'DocExtensionsField" class="' + projectKey + 'ProjectConfigurationField" projectKey="' + projectKey + '" value="';
	htmlString += projectConfiguration.docExtensions.join();
	htmlString += '" />';
	htmlString += '<br>';
	
	htmlString += '<label class="projectDetailsFormLabel">Base Directory</label>';
	htmlString += '<input disabled="disabled" style="border:0px;" size="100" id="' + projectKey + 'WorkingDirectoryField" class="' + projectKey + 'ProjectConfigurationField" projectKey="' + projectKey + '" value="';
	htmlString += projectConfiguration.workingDirectory;
	htmlString += '" />';
	htmlString += '<br>';
	
	htmlString += '</div>';
	
	return htmlString;
}

function getEditProjectButtonHTMLString(projectKey) {
	return '<button class="editProjectButton" projectKey="' + projectKey + '">Edit Project</button>';
}

function getCancelEditProjectButtonHTMLString(projectKey) {
	return '<button class="cancelEditProjectButton hidden" projectKey="' + projectKey + '">Cancel Edit</button>';
}

function getSaveProjectChangesButtonHTMLString(projectKey) {
	return '<button class="saveProjectChangesButton hidden" projectKey="' + projectKey + '">Save Changes</button>';
}

function getSaveAndRunProjectChangesButtonHTMLString(projectKey) {
	return '<button class="saveAndRunProjectChangesButton hidden" projectKey="' + projectKey + '">Save and Run</button>';
}

function getDeleteProjectButtonHTMLString(projectKey) {
	return '<button class="deleteProjectButton" projectKey="' + projectKey + '">Delete Project</button>';
}


function enableProjectConfigurationInputFieldsForProjectKey(projectKey) {
	var projectConfigurationClass = "." + projectKey + "ProjectConfigurationField";
	
	$(projectConfigurationClass).removeAttr("disabled");
	$(projectConfigurationClass).removeAttr("style");
	
	$('.cancelEditProjectButton').removeClass("hidden");
	$('.saveProjectChangesButton').removeClass("hidden");
	$('.saveAndRunProjectChangesButton').removeClass("hidden");
	$('.runLinksButton').addClass("hidden");
	$('.deleteProjectButton').addClass("hidden");
}

function disableProjectConfigurationInputFieldsForProjectKey(projectKey) {
	var projectConfigurationClass = "." + projectKey + "ProjectConfigurationField";
	
	$(projectConfigurationClass).attr("disabled", "disabled");
	$(projectConfigurationClass).attr("style", "border:0px;");
	
	$('.saveProjectChangesButton').addClass("hidden");
	$('.saveAndRunProjectChangesButton').addClass("hidden");
	$('.cancelEditProjectButton').addClass("hidden");
	$('.runLinksButton').removeClass("hidden");
	$('.deleteProjectButton').removeClass("hidden");
}

function setUpButtonEvents() {
	$(".addProjectButton").click(function() {
		$('#addNewProjectDialog').dialog("open");
	});
	
	$('body').on('click', '.goToProjectButton', function() {
		var selectedProjectKey = $(this).attr("projectKey");
		reloadPageWithNewProjectKey(selectedProjectKey);
	});
	
	$('.runLinksButton').click(function(event) {
		var projectKey = $(this).attr("projectKey");
		runLinksForProject(projectKey);
	});
	
	$('.deleteProjectButton').click(function(event) {
		var projectKey = $(this).attr("projectKey");
		
		$.post('/docfacto-links-servlet/LinksServlet',
				{ action:"deleteProject",
					projectKey:projectKey
				},
				function(data) {
					var linksServletResponse = $.parseJSON(data);
					var type = linksServletResponse.type;
					if (type == "REDIRECT_TO_PAGE") {
						var pageToRedirectTo = linksServletResponse.data;
						window.location.href = pageToRedirectTo;
					}
					else {
						alert("Don't know what to do");
					}
				}
		);
	});
	
	$('.editProjectButton').click(function(event) {
		var projectKey = $(this).attr("projectKey");
		enableProjectConfigurationInputFieldsForProjectKey(projectKey);
	});
	
	$('.cancelEditProjectButton').click(function(event) {
		var projectKey = $(this).attr("projectKey");
		disableProjectConfigurationInputFieldsForProjectKey(projectKey);
	});
	
	$('.saveProjectChangesButton').click(function(event) {
		var projectKey = $(this).attr("projectKey");
		saveProjectChanges(projectKey);
	});
	
	$('.saveAndRunProjectChangesButton').click(function(event) {
		var projectKey = $(this).attr("projectKey");
		saveProjectChanges(projectKey);
		runLinksForProject(projectKey);
	});
}

function saveProjectChanges(projectKey) {
	var projectName = $("#" + projectKey + "ProjectNameField").val();
	var workingDirectory = $("#" + projectKey + "WorkingDirectoryField").val();
	var sourceDirectory=$("#" + projectKey + "SourceDirectoryField").val();
	var docDirectory=$("#" + projectKey + "DocDirectoryField").val();
	var xmlConfigDirectory=$("#" + projectKey + "XmlConfigDirectoryField").val();
	var sourceType=$("#" + projectKey + "SourceTypeField").val();
	var sourceExtensions=$("#" + projectKey + "SourceExtensionsField").val();
	var docType=$("#" + projectKey + "DocTypeField").val();
	var docExtensions=$("#" + projectKey + "DocExtensionsField").val();
	
	
	if (stringIsEmpty(projectName)) {
		alert("Please set a project name");
		return;
	}
	
	$.post('/docfacto-links-servlet/LinksServlet', 
			{ action:"EditProject",
				projectKey:projectKey,
				newProjectName:projectName, 
				newWorkingDirectory:workingDirectory,
				newSourceDirectory:sourceDirectory,
				newDocDirectory:docDirectory,
				newXMLConfigDirectory:xmlConfigDirectory,
				newSourceType:sourceType,
				newSourceExtensions:sourceExtensions,
				newDocType:docType,
				newDocExtensions:docExtensions
			},
			function(data) {
				var linksServletResponse = $.parseJSON(data);
				var type = linksServletResponse.type;
				if (type == "ERROR_EDITING_PROJECT") {
					var error = linksServletResponse.data;
					alert(error);
				}
				else if (type == "PROJECT_EDITED"){
					disableProjectConfigurationInputFieldsForProjectKey(projectKey);
				}
				else {
					disableProjectConfigurationInputFieldsForProjectKey(projectKey);
					alert("Don't know what to do for response type: " + type);
				}
			}
		);
}