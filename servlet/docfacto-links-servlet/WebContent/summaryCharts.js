var showingSourceLinksExtraSummaryCharts = false;
var showingDocLinksExtraSummaryCharts = false;

function drawSummaryCharts(projectKey, projectStatistics) {
	drawSourceWithDocChart(projectKey, projectStatistics);
	drawDocWithSourceChart(projectKey, projectStatistics);
}

function drawSourceWithDocChart(projectKey, projectStatistics) {
	var sourceWithDocChartId = projectKey + "SourceWithDocChart";
	var sourceFilesWithDocLink = getStatisticValueForKey("source-files-with-doc-link", projectStatistics);
	var sourceFilesProcessed = getStatisticValueForKey("source-files-processed", projectStatistics);
	var noSourceFilesMessage = "No Source Files Found";
	
	addSummaryGaugeChartBlock("Source Linked to Doc", 
			sourceWithDocChartId, 
			"Source Files With At Least One Working Link To A Doc File",
			sourceFilesWithDocLink,
			"Source Files",
			sourceFilesProcessed,
			noSourceFilesMessage,
			false,
			function() {
				showExtraDocLinkCharts(projectKey, projectStatistics);
			});
}

function drawDocWithSourceChart(projectKey, projectStatistics) {
	var docWithSourceChartId = projectKey + "DocWithSourceChart";
	var docFilesWithSourceLink = getStatisticValueForKey("doc-files-with-source-link", projectStatistics);
	var docFilesProcessed = getStatisticValueForKey("doc-files-processed", projectStatistics);
	var noDocFilesMessage = "No Doc Files Found";
	
	addSummaryGaugeChartBlock("Doc Linked to Source",
			docWithSourceChartId, 
			"Doc Files With At Least One Working Link To A Source File",
			docFilesWithSourceLink, 
			"Doc Files",
			docFilesProcessed,
			noDocFilesMessage,
			false,
			function() {
				showExtraSourceLinkCharts(projectKey, projectStatistics);
			});
}

function addSummaryGaugeChartBlock(chartTitle, 
		chartId, 
		numeratorDescription, 
		numeratorValue, 
		denominatorDescription, 
		denominatorValue, 
		emptyDenominatorErrorMessage, 
		invertColors, 
		selectHandler) {

	addGaugeChartBlockToSelector('.summaryCharts',
									chartTitle, 
									chartId, 
									numeratorDescription, 
									numeratorValue, 
									denominatorDescription, 
									denominatorValue, 
									emptyDenominatorErrorMessage, 
									invertColors, 
									selectHandler);
	

	var chartBlockLinkId = getChartBlockLinkId(chartId);
	makeChartBlockLinkSelectable(chartBlockLinkId);
}

function setShowingSourceLinksExtraSummaryCharts(isShowing) {
	showingSourceLinksExtraSummaryCharts = isShowing;
	
	if (showingSourceLinksExtraSummaryCharts) {
		showingDocLinksExtraSummaryCharts = false;
	}
}

function toggleArrowForSummaryChartBlock(chartBlockId) {
	if ($('#' + chartBlockId).hasClass("selectableChartBlock")) {
		
		if (!$('#' + chartBlockId).hasClass("selectedGaugeChart")) {
			clearSummaryChartArrows();
			$('#' + chartBlockId).addClass("selectedGaugeChart");
		}
		else {
			clearSummaryChartArrows();
		}
	}
}

function clearSummaryChartArrows() {
	$('.selectableChartBlock').removeClass("selectedGaugeChart");
}

function setShowingDocLinksExtraSummaryCharts(isShowing) {
	showingDocLinksExtraSummaryCharts = isShowing;
	
	if (showingDocLinksExtraSummaryCharts) {
		showingSourceLinksExtraSummaryCharts = false;
	}
}

function showExtraDocLinkCharts(projectKey, projectStatistics) {
	
	if (showingDocLinksExtraSummaryCharts) {
		clearExtraSummaryCharts();
		setShowingDocLinksExtraSummaryCharts(false);
		return;
	}
	
	if (!extraSummaryChartsIsEmpty()) {
		clearExtraSummaryCharts();
	}
	
	insertExtraSummaryChartsArea();
	
	addWorkingDocLinksGaugeChart(projectKey, projectStatistics);
	addDocLinkVersionMismatchesGaugeChart(projectKey, projectStatistics);
	addDocLinkMissingUrisGaugeChart(projectKey, projectStatistics);
	addDocLinkInvalidUrisGaugeChart(projectKey, projectStatistics);
	addDocLinksBrokenGaugeChart(projectKey, projectStatistics)
	
	setShowingDocLinksExtraSummaryCharts(true);
}

function showExtraSourceLinkCharts(projectKey, projectStatistics) {
	
	if (showingSourceLinksExtraSummaryCharts) {
		clearExtraSummaryCharts();
		setShowingSourceLinksExtraSummaryCharts(false);
		return;
	}
	
	if (!extraSummaryChartsIsEmpty()) {
		clearExtraSummaryCharts();
	}
	
	insertExtraSummaryChartsArea();
	
	addWorkingSourceLinksGaugeChart(projectKey, projectStatistics);
	addSourceLinkVersionMismatchesGaugeChart(projectKey, projectStatistics);
	addSourceLinkMissingUrisGaugeChart(projectKey, projectStatistics);
	addSourceLinkInvalidUrisGaugeChart(projectKey, projectStatistics);
	addSourceLinksBrokenGaugeChart(projectKey, projectStatistics);
	
	setShowingSourceLinksExtraSummaryCharts(true);
}

function insertExtraSummaryChartsArea() {
	$('.extraSummaryChartsWrapper').append('<div class="extraSummaryCharts"></div>');
}

function addWorkingDocLinksGaugeChart(projectKey, projectStatistics) {
	var workingDocLinksChartId = projectKey + "workingDocLinksChart";
	var noOfNonWorkingDocLinks = getNumberOfNonWorkingDocLinks(projectStatistics);
	var noOfDocLinks = getStatisticValueForKey("doc-link-found", projectStatistics);
	var noOfWorkingDocLinks = noOfDocLinks - noOfNonWorkingDocLinks;
	var noDocLinksMessage = "No doc links found";
	
	addExtraSummaryGaugeChart("Working Doc Links", 
			workingDocLinksChartId, 
			"Working Doc Links",
			noOfWorkingDocLinks, 
			"Number of Doc Links",
			noOfDocLinks,
			noDocLinksMessage,
			false);
}

function addDocLinkVersionMismatchesGaugeChart(projectKey, projectStatistics) {
	var docLinkVersionMismatchesChartId = projectKey + "docLinkVersionMismatchesChart";
	var noOfDocLinkVersionMismatches = getStatisticValueForKey("doc-link-version-mismatch", projectStatistics);
	var noOfDocLinks = getStatisticValueForKey("doc-link-found", projectStatistics);
	var noDocLinksMessage = "No doc links found";
	
	addExtraSummaryGaugeChart("Doc Link Version Mismatches", 
								docLinkVersionMismatchesChartId,
								"Doc Links with Version Mismatches",
								noOfDocLinkVersionMismatches,
								"Number of Doc Links",
								noOfDocLinks,
								noDocLinksMessage,
								true);
}

function addDocLinkInvalidUrisGaugeChart(projectKey, projectStatistics) {
	var chartId = projectKey + "docLinkInvalidUrisChartId";
	var noOfDocLinksWithInvalidUri = getStatisticValueForKey("doc-link-invalid-uri", projectStatistics);
	var noOfDocLinks = getStatisticValueForKey("doc-link-found", projectStatistics);
	var noDocLinksMessage = "No doc links found";
	
	addExtraSummaryGaugeChart("Doc Link Invalid Uri Errors", 
								chartId,
								"Doc Links with Invalid Uri",
								noOfDocLinksWithInvalidUri,
								"Number of Doc Links",
								noOfDocLinks,
								noDocLinksMessage,
								true);
}

function addDocLinkMissingUrisGaugeChart(projectKey, projectStatistics) {
	var chartId = projectKey + "docLinkMissingUrisChartId";
	var noOfDocLinksWithMissingUri = getStatisticValueForKey("doc-link-tag-no-uri", projectStatistics);
	var noOfDocLinks = getStatisticValueForKey("doc-link-found", projectStatistics);
	var noDocLinksMessage = "No doc links found";
	
	addExtraSummaryGaugeChart("Doc Link Missing Uri Errors", 
								chartId,
								"Doc Links with Invalid Uri",
								noOfDocLinksWithMissingUri,
								"Number of Doc Links",
								noOfDocLinks,
								noDocLinksMessage,
								true);
}

function addDocLinksBrokenGaugeChart(projectKey, projectStatistics) {
	var chartId = projectKey + "docLinksBrokenLinkChartId";
	var noOfDocLinksBroken = getStatisticValueForKey("doc-broken-link", projectStatistics);
	var noOfDocLinks = getStatisticValueForKey("doc-link-found", projectStatistics);
	var noDocLinksMessage = "No doc links found";
	
	addExtraSummaryGaugeChart("Doc Links Broken", 
								chartId,
								"Doc Links Broken",
								noOfDocLinksBroken,
								"Number of Doc Links",
								noOfDocLinks,
								noDocLinksMessage,
								true);
}

function addWorkingSourceLinksGaugeChart(projectKey, projectStatistics) {
	var workingSourceLinksChartId = projectKey + "workingSourceLinksChart";
	var noOfNonWorkingSourceLinks = getNumberOfNonWorkingSourceLinks(projectStatistics);
	var noOfSourceLinks = getStatisticValueForKey("source-link-found", projectStatistics);
	var noOfWorkingSourceLinks = noOfSourceLinks - noOfNonWorkingSourceLinks;
	var noSourceLinksMessage = "No source links found";
	
	addExtraSummaryGaugeChart("Working Source Links", 
								workingSourceLinksChartId, 
								"Working Source Links",
								noOfWorkingSourceLinks, 
								"Number of Source Links",
								noOfSourceLinks,
								noSourceLinksMessage,
								false);
}

function addSourceLinkVersionMismatchesGaugeChart(projectKey, projectStatistics) {
	var sourceLinkVersionMismatchesChartId = projectKey + "sourceLinkVersionMismatchesChart";
	var noOfSourceLinkVersionMismatches = getStatisticValueForKey("source-link-version-mismatch", projectStatistics);
	var noOfSourceLinks = getStatisticValueForKey("source-link-found", projectStatistics);
	var noSourceLinksMessage = "No source links found";
	
	addExtraSummaryGaugeChart("Source Link Version Mismatches", 
								sourceLinkVersionMismatchesChartId,
								"Source Links with Version Mismatches",
								noOfSourceLinkVersionMismatches,
								"Number of Source Links",
								noOfSourceLinks,
								noSourceLinksMessage,
								true);
}

function addSourceLinkInvalidUrisGaugeChart(projectKey, projectStatistics) {
	var chartId = projectKey + "sourceLinkInvalidUrisChartId";
	var noOfSourceLinksWithInvalidUri = getStatisticValueForKey("source-link-invalid-uri", projectStatistics);
	var noOfSourceLinks = getStatisticValueForKey("source-link-found", projectStatistics);
	var noSourceLinksMessage = "No source links found";
	
	addExtraSummaryGaugeChart("Source Link Invalid Uri Errors", 
								chartId,
								"Source Links with Invalid Uri",
								noOfSourceLinksWithInvalidUri,
								"Number of Source Links",
								noOfSourceLinks,
								noSourceLinksMessage,
								true);
}

function addSourceLinkMissingUrisGaugeChart(projectKey, projectStatistics) {
	var chartId = projectKey + "sourceLinkMissingUrisChartId";
	var noOfSourceLinksWithMissingUri = getStatisticValueForKey("source-link-tag-no-uri", projectStatistics);
	var noOfSourceLinks = getStatisticValueForKey("source-link-found", projectStatistics);
	var noSourceLinksMessage = "No source links found";
	
	addExtraSummaryGaugeChart("Source Link Missing Uri Errors", 
								chartId,
								"Source Links with Invalid Uri",
								noOfSourceLinksWithMissingUri,
								"Number of Source Links",
								noOfSourceLinks,
								noSourceLinksMessage,
								true);
}

function addSourceLinksBrokenGaugeChart(projectKey, projectStatistics) {
	var chartId = projectKey + "sourceLinksBrokenLinkChartId";
	var noOfSourceLinksBroken = getStatisticValueForKey("source-broken-link", projectStatistics);
	var noOfSourceLinks = getStatisticValueForKey("source-link-found", projectStatistics);
	var noSourceLinksMessage = "No source links found";
	
	addExtraSummaryGaugeChart("Source Links Broken", 
								chartId,
								"Source Links Broken",
								noOfSourceLinksBroken,
								"Number of Source Links",
								noOfSourceLinks,
								noSourceLinksMessage,
								true);
}

function getSummaryGaugeHTMLBlock(chartTitle, chartId, numeratorDescription, numeratorValue, denominatorDescription, denominatorValue) {
	
	var gaugeChartHTMLString = getGaugeChartHTMLString(chartId);
	var gaugeChartStatisticsHTMLString = getGaugeChartStatisticsHTMLString(numeratorDescription, numeratorValue, denominatorDescription, denominatorValue);
	var gaugeChartBlockLinkId = getChartBlockLinkId(chartId);
	
	var htmlString = "";
	
	htmlString += '<a id="' + gaugeChartBlockLinkId + '" class="summaryGaugeBlockLink" href="javascript:void(0)">';
	htmlString += '<div class="summaryGaugeBlock">';
	htmlString += '<div class="summaryGaugeTitle">' + chartTitle + '</div>';
	htmlString += '<div class="summaryGaugeChartWrapper">' + gaugeChartHTMLString + '</div>';
	htmlString += '<div class="summaryGaugeChartStatisticsWrapper">' + gaugeChartStatisticsHTMLString + '</div>';
	htmlString += '<div class="summaryChartArrowWrapper"><div class="summaryChartArrow"></div></div>';
	htmlString += '</div>';
	
	return htmlString;
}

function getChartBlockLinkId(chartId) {
	return chartId + "BlockLink";
}

function getGaugeChartHTMLString(chartId) {
	var htmlString = "";
	htmlString += '<div class="summaryGaugeChart">';
	htmlString += getChartHTMLString(chartId);
	htmlString += '</div>';
	return htmlString;
}

function addExtraSummaryGaugeChart(chartTitle, 
									chartId, 
									numeratorDescription, 
									numeratorValue, 
									denominatorDescription, 
									denominatorValue, 
									emptyDenominatorErrorMessage, 
									invertColors, 
									selectHandler) {

	addGaugeChartBlockToSelector('.extraSummaryCharts',
							chartTitle, 
							chartId, 
							numeratorDescription, 
							numeratorValue, 
							denominatorDescription, 
							denominatorValue, 
							emptyDenominatorErrorMessage, 
							invertColors, 
							selectHandler);
	
	var chartBlockLinkId = getChartBlockLinkId(chartId);
	makeChartBlockLinkNonSelectable(chartBlockLinkId);
}

function makeChartBlockLinkSelectable(chartBlockLinkId) {
	$('#' + chartBlockLinkId).addClass("selectableChartBlock");
}

function makeChartBlockLinkNonSelectable(chartBlockLinkId) {
	$('#' + chartBlockLinkId).addClass("nonSelectableChartBlock");
}

function addGaugeChartBlockToSelector(selectorToAddChartTo,
								chartTitle, 
								chartId, 
								numeratorDescription, 
								numeratorValue, 
								denominatorDescription, 
								denominatorValue, 
								emptyDenominatorErrorMessage, 
								invertColors, 
								selectHandler) {
	
	var gaugeHTMLBlock = getSummaryGaugeHTMLBlock(chartTitle, 
			chartId, 
			numeratorDescription,
			numeratorValue, 
			denominatorDescription,
			denominatorValue);

	$(selectorToAddChartTo).append(gaugeHTMLBlock);

	var gaugeChartBlockLinkId = getChartBlockLinkId(chartId);
	
	$('#' + gaugeChartBlockLinkId).on('click', function() {
		toggleArrowForSummaryChartBlock(gaugeChartBlockLinkId);
		if (selectHandler != null) {
			selectHandler();
		}
	});
	
	drawGaugeChart(chartId,
			numeratorValue,
			denominatorValue,
			emptyDenominatorErrorMessage,
			invertColors);
}

function getGaugeChartStatisticsHTMLString(numeratorDescription, numeratorValue, denominatorDescription, denominatorValue) {
	var htmlString = "";
	
	htmlString += '<div class="summaryGaugeChartStatistics">';
	htmlString += '<table class="summaryGaugeChartTable">';
	htmlString += "<tr>";
	htmlString += "<td>";
	htmlString += numeratorDescription + ":";
	htmlString += "</td>";
	htmlString += "<td>";
	htmlString += numeratorValue;
	htmlString += "</td>";
	htmlString += "</tr>";
	htmlString += "<tr>";
	htmlString += "<td>";
	htmlString += denominatorDescription + ":";
	htmlString += "</td>";
	htmlString += "<td>";
	htmlString += denominatorValue;
	htmlString += "</td>";
	htmlString += "</tr>";
	htmlString += "</table>";
	htmlString += '</div>';

	return htmlString;
}

function extraSummaryChartsIsEmpty() {
	return $('.extraSummaryChartsWrapper').html().trim().length == 0;
}

function clearExtraSummaryCharts() {
	$('.extraSummaryChartsWrapper').html("");
}

function getCodeAndDocLinesStatisticsHTMLString(projectStatistics) {
	var htmlString = "";
	
	var linesOfCode = getStatisticValueForKey("code-lines", projectStatistics);
	var linesOfDoc = getStatisticValueForKey("doc-lines", projectStatistics);
	
	htmlString += "Lines of code: " + linesOfCode + "<br />";
	htmlString += "Lines of doc: " + linesOfDoc + "<br />";

	return htmlString;
}

function getSourceWithDocStatisticsHTMLString(projectStatistics) {
	var htmlString = "";
	
	var noOfSourceFiles = getStatisticValueForKey("source-files-processed", projectStatistics);
	var noOfSourceFilesWithDoc = getStatisticValueForKey("source-files-with-doc-link", projectStatistics);
	
	var noOfSourceFilesWithoutDoc = noOfSourceFiles - noOfSourceFilesWithDoc;
	
	htmlString += "<table>";
	htmlString += "<tr>";
	htmlString += "<td>"
	htmlString += "Source Files: ";
	htmlString += "</td>";
	htmlString += "<td>";
	htmlString += noOfSourceFiles;
	htmlString += "</td>";
	htmlString += "</tr>";
	htmlString += "<tr>";
	htmlString += "<td>";
	htmlString += "Source Files Without At Least One <br />Working Link To A Doc File: ";
	htmlString += "</td>";
	htmlString += "<td>";
	htmlString += noOfSourceFilesWithoutDoc;
	htmlString += "</td>";
	htmlString += "</tr>";
	htmlString += "</table>";

	return htmlString;
}

function getDocWithSourceStatisticsHTMLString(projectStatistics) {
	var htmlString = "";
	
	var noOfDocFiles = getStatisticValueForKey("doc-files-processed", projectStatistics);
	var noOfDocFilesWithSource = getStatisticValueForKey("doc-files-with-source-link", projectStatistics);
	
	var noOfDocFilesWithoutSource = noOfDocFiles - noOfDocFilesWithSource;
	
	htmlString += "<table>";
	htmlString += "<tr>";
	htmlString += "<td>"
	htmlString += "Doc Files: ";
	htmlString += "</td>";
	htmlString += "<td>";
	htmlString += noOfDocFiles;
	htmlString += "</td>";
	htmlString += "</tr>";
	htmlString += "<tr>";
	htmlString += "<td>";
	htmlString += "Doc Files Without At Least One <br />Working Link To A Source File: ";
	htmlString += "</td>";
	htmlString += "<td>";
	htmlString += noOfDocFilesWithoutSource;
	htmlString += "</td>";
	htmlString += "</tr>";
	htmlString += "</table>";

	return htmlString;
}

function getWorkingLinksInSourceStatisticsHTMLString(projectStatistics) {
	var htmlString = "";
	
	var noOfNonWorkingSourceLinks = getNumberOfNonWorkingSourceLinks(projectStatistics);
	var noOfSourceLinks = getStatisticValueForKey("source-link-found", projectStatistics);
	var noOfWorkingSourceLinks = noOfSourceLinks - noOfNonWorkingSourceLinks;
	
	htmlString += "<table>";
	htmlString += "<tr>";
	htmlString += "<td>";
	htmlString += "Working Source Links: ";
	htmlString += "</td>";
	htmlString += "<td>";
	htmlString += noOfWorkingSourceLinks;
	htmlString += "</td>";
	htmlString += "</tr>";
	htmlString += "<tr>";
	htmlString += "<td>";
	htmlString += "Number of Source Links: ";
	htmlString += "</td>";
	htmlString += "<td>";
	htmlString += noOfSourceLinks;
	htmlString += "</td>";
	htmlString += "</tr>";
	htmlString += "</table>";

	return htmlString;
}