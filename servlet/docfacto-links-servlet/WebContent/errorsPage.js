function fillInErrorsPageForProject(projectKey, project) {
	
	var results = project.linksResults;
	var statistics = project.linksStatistics;
	var linksProcessingErrors = project.linksProcessingErrors;
	
	if (linksProcessingErrors.length > 0) {
		var projectErrorsHTMLString = getProjectErrorsHTMLString(linksProcessingErrors);
		$('#errorsPage').html(projectErrorsHTMLString);
		return;
	}
	
	//drawSourcePackageChart(statistics, "sourcePackagePieChart", null);
	//drawDocPackageChart(statistics, "docPackagePieChart", null);
	
	addAllPackagesToPackagesEnabled(statistics);
	setPackagesEnabledTreeToDiv("packagesSelectorTree", getURLParameter("projectKey"), statistics);
	setLinksResultsTable(results);
	
	$('body').on('click', '.enableAllPackagesButton', function() {
		enableAllPackages("packagesSelectorTree");
	});
	
	$('body').on('click', '.disableAllPackagesButton', function() {
		disableAllPackages("packagesSelectorTree");
	});
	
	$('body').on('click', '.updatePackagesEnabledButton', function() {
		syncPackagesEnabledTreeWithPackagesEnabledMap("packagesSelectorTree");
		
//		var filteredLinksStatistics = filterStatisticsWithDisabledPackages(statistics);
		var filteredLinksResults = filterResultsWithDisabledPackages(results);
//		drawSourcePackageChart(filteredLinksStatistics, "sourcePackagePieChart", null);
//		drawDocPackageChart(filteredLinksStatistics, "docPackagePieChart", null);
		setLinksResultsTable(filteredLinksResults);
	});
}

function getProjectErrorsHTMLString(linksProcessingErrors) {
	var htmlString = "";
	htmlString += '<h3>Errors Found</h3>';
	htmlString += '<p>There were errors when running links for this project (this may be a problem to do with your xml configuration file):';
	
	for (var i=0; i < linksProcessingErrors.length; i++) {
		htmlString += '<p style="color:red">';
		htmlString += linksProcessingErrors[i];
		htmlString += '</p>';
	}
	
	return htmlString;
}