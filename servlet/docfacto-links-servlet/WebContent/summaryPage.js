function fillInSummaryPageForProject(projectKey, project) {
	var projectStatistics = project.linksStatistics;
	var linksProcessingErrors = project.linksProcessingErrors;
	
	if (linksProcessingErrors.length > 0) {
		var projectErrorsHTMLString = getProjectErrorsHTMLString(linksProcessingErrors);
		$('#summaryPage').html(projectErrorsHTMLString);
		return;
	}
	
	var projectSummaryTextString = getSummaryTextSectionString(projectKey, projectStatistics);
	$("#projectSummaryText").html(projectSummaryTextString);
	
	var projectSummaryChartsString = getSummaryChartsSectionString(projectKey, projectStatistics);
	$("#projectSummaryCharts").html(projectSummaryChartsString);

	drawSummaryCharts(projectKey, projectStatistics);
	
	var projectStatisticsString = getProjectStatisticsHTMLString(projectStatistics)
	
	$("#projectStatistics").html(projectStatisticsString);
}


function getProjectStatisticsHTMLString(projectStatistics) {
	var htmlString = "";
	
	for (var i=0; i < projectStatistics.length; i++) {
		var statistic = projectStatistics[i];
		var rule = statistic.rule;
		var message = rule.message;
		var value = statistic.value;
		htmlString += message + ": " + value + "<br />";
	}
	return htmlString;
}

function getSummaryTextSectionString(projectKey, projectStatistics) {
	var htmlString = "";

	var sourceLinksFound = getStatisticValueForKey("source-link-found", projectStatistics);
	var docLinksFound = getStatisticValueForKey("doc-link-found", projectStatistics);
	var nonWorkingLinks = getNumberOfNonWorkingLinks(projectStatistics);

	htmlString += '<div class="summaryTextSectionWrapper">';
	htmlString += '<div class="summaryTextSection">';
	
	htmlString += '<div class="summaryTextBlock">';
	htmlString += "Source Links";
	htmlString += "<br />";
	htmlString += '<span class="summaryTextValue">';
	htmlString += sourceLinksFound;
	htmlString += "</span>";
	htmlString += '</div>';
	
	htmlString += '<div class="summaryTextBlock">';
	htmlString += "Doc Links";
	htmlString += "<br />";
	htmlString += '<span class="summaryTextValue">';
	htmlString += docLinksFound;
	htmlString += "</span>";
	htmlString += '</div>';
	
	htmlString += '<div class="summaryTextBlock">';
	htmlString += "Non working Links";
	htmlString += "<br />";
	htmlString += '<span class="summaryTextValue">';
	htmlString += nonWorkingLinks;
	htmlString += "</span>";
	htmlString += '</div>';
	
	htmlString += '</div>';
	htmlString += '</div>';
	
	return htmlString;
}

function getSummaryChartsSectionString(projectKey, projectStatistics) {
	var htmlString = "";
	
	htmlString += '<div class="summaryChartsSectionWrapper">';
	htmlString += '<div class="summaryChartsSection">';
	htmlString += '<div class="summaryCharts"></div>';
	htmlString += '<div class="extraSummaryChartsWrapper"></div>';
	htmlString += '</div>';
	htmlString += '</div>';
	
	return htmlString;
}