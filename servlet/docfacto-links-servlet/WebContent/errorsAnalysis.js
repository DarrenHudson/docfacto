function setLinksResultsTable(linksResults) {
	
	//Clear the existing tables
	$("#linksRules").empty();
	$("#linksPackages").empty();
	$("#linksFiles").empty();
	$("#linksResults").empty();
	
	var linksResultsHTMLString = "";
	
	if (linksResults.length > 0)
		linksResultsHTMLString = getRulesTableString(linksResults);
	else {
		linksResultsHTMLString = '<p id="resultsCongratulationsText" align="center"><font size="4"><br />No Error Results</font></p>';
	}
	
	$("#linksRules").html(linksResultsHTMLString);
	
	$('.ruleSelected').click(function() {
		var ruleName = $(this).attr("ruleName");
		currentRuleSelected = ruleName;
		showPackagesForRule(currentRuleSelected, linksResults);
		clearFilesTable();
		clearResultsTable();
		
		var selectedLinkElement = $(this);
		setRuleLinkAsSelected(selectedLinkElement);
		return false;
	});
	
	$('body').on('click', 'a.packageSelected', function() {
		var packageName = $(this).attr("packageName");
		currentPackageSelected = packageName;
		showFilesForRuleAndPackage(currentRuleSelected, currentPackageSelected, linksResults);
		clearResultsTable();
		
		var selectedLinkElement = $(this);
		setPackageLinkAsSelected(selectedLinkElement);
		return false;
	});
	
	$('body').on('click', 'a.fileSelected', function() {
		var fileName = $(this).attr("fileName");
		currentFileSelected = fileName;
		showResultsForRuleAndPackageAndFile(currentRuleSelected, currentPackageSelected, currentFileSelected, linksResults);
		
		var selectedLinkElement = $(this);
		setFileLinkAsSelected(selectedLinkElement);
		return false;
	});
}

function setRuleLinkAsSelected(ruleLinkElement) {
	$(".ruleSelected").removeClass("selectedLink");
	ruleLinkElement.addClass("selectedLink");
}

function setPackageLinkAsSelected(ruleLinkElement) {
	$(".packageSelected").removeClass("selectedLink");
	ruleLinkElement.addClass("selectedLink");
}

function setFileLinkAsSelected(ruleLinkElement) {
	$(".fileSelected").removeClass("selectedLink");
	ruleLinkElement.addClass("selectedLink");
}

function getRunLinksButtonHTMLString(projectKey) {
	return '<input type="button" class="runLinksButton" projectKey="' + projectKey + '"value="Run Links">';
}

function getRulesTableString(linksResults) {
	var htmlString = "";
	
	var rulesAlreadyAdded = [];
	
	htmlString += '<div style="border:1px solid black; overflow:auto; max-height:300px;">';
	htmlString += '<table class="resultsAnalysisTable">';
	htmlString += '<tr style="border:1px solid black; border-collapse:collapse;"><th>Rule</th><tr>';
	for (var i=0; i<linksResults.length; i++) {
		var resultData = linksResults[i];
		var result = resultData.result;
		var rule = result.rule;
		var name = rule.name;
		if ($.inArray(name, rulesAlreadyAdded) < 0) {
			htmlString += '<tr><td><a class="ruleSelected" href="#" ruleName="' + rule.name +  '">' + name + '</a></td></tr>';
			rulesAlreadyAdded.push(name);
		}
	}
	
	htmlString += "</table>";
	htmlString += "</div>";
	
	return htmlString;
}

function showPackagesForRule(selectedRule, linksResults) {
	var htmlString = getPackageTableHTMLStringForRule(selectedRule, linksResults);
	$('#linksPackages').html(htmlString);
}

function getPackageTableHTMLStringForRule(selectedRule, linksResults) {
	var htmlString = "";
	
	var packagesAlreadyAdded = [];
	
	htmlString += '<div style="border:1px solid black; overflow:auto; max-height:300px;">';
	htmlString += '<table class="resultsAnalysisTable">';
	htmlString += '<tr style="border:1px solid black; border-collapse:collapse;"><th>Package</th><tr>';
	
	for (var i=0; i<linksResults.length; i++) {
		var resultData = linksResults[i];
		var result = resultData.result;
		var rule = result.rule;
		var ruleName = rule.name;
		var position = result.position;
		var packageName = position.resolvedPackage;
		
		if ( (ruleName == selectedRule) && ($.inArray(packageName, packagesAlreadyAdded) < 0)) {
			htmlString += '<tr><td><a class="packageSelected" href="#" packageName="' + packageName +  '">' + packageName + '</a></td></tr>';
			packagesAlreadyAdded.push(packageName);
		}
	}
	htmlString += "</table>";
	htmlString += "</div>";
	return htmlString;
}

function clearFilesTable() {
	$('#linksFiles').html("");
}

function showFilesForRuleAndPackage(selectedRule, selectedPackage, linksResults) {
	var htmlString = getFilesTableHTMLStringForRuleAndPackage(selectedRule, selectedPackage, linksResults);
	$('#linksFiles').html(htmlString);
}

function getFilesTableHTMLStringForRuleAndPackage(selectedRule, selectedPackage, linksResults) {
	var htmlString = "";
	var filesAlreadyAdded = [];
	
	htmlString += '<div style="border:1px solid black; overflow:auto; max-height:300px;">';
	htmlString += '<table class="resultsAnalysisTable">';
	htmlString += '<tr style="border:1px solid black; border-collapse:collapse;"><th>File</th><tr>';
	
	for (var i=0; i<linksResults.length; i++) {
		var resultData = linksResults[i];
		var result = resultData.result;
		var rule = result.rule;
		var ruleName = rule.name;
		var position = result.position;
		var file = position.resolvedFile;
		var filePackage = position.resolvedPackage;
		
		if ( (ruleName == selectedRule) && (filePackage == selectedPackage) && ($.inArray(file, filesAlreadyAdded) < 0)) {
			htmlString += '<tr><td><a class="fileSelected" href="#" fileName="' + file +  '">' + file + '</a></td></tr>';
			filesAlreadyAdded.push(file);
		}
	}
	htmlString += "</table>";
	htmlString += "</div>";
	return htmlString;
}

function clearResultsTable() {
	$('#linksResults').html("");
}

function showResultsForRuleAndPackageAndFile(selectedRule, selectedPackage, selectedFile, linksResults) {
	var htmlString = "";
	htmlString += '<div style="overflow:auto; max-height:800px;">';
	for (var i=0; i<linksResults.length; i++) {
		var resultData = linksResults[i];
		var result = resultData.result;
		var rule = result.rule;
		var ruleName = rule.name;
		var position = result.position;
		var line = position.line;
		var file = position.resolvedFile;
		var filePackage = position.resolvedPackage;
		var snapshot = resultData.snapshot;
		var resultLineInSnapshot = snapshot.resultLine;
		var snapshotLines = snapshot.snapshotLines;
		
		if ( (ruleName == selectedRule) && (filePackage == selectedPackage) && (file == selectedFile)) {
			htmlString += '<table class="resultsAnalysisTable">';
			htmlString += '<tr><td class="resultsAnalysisHeading"><b>Rule</b></td><td>' + ruleName + "</tr>";
			htmlString += '<tr><td class="resultsAnalysisHeading"><b>Package</b></td><td>' + filePackage + "</td></tr>";
			htmlString += '<tr><td class="resultsAnalysisHeading"><b>File</b></td><td>' + file + "</td></tr>";
			htmlString += '<tr><td class="resultsAnalysisHeading"><b>Line</b></td><td>' + line + "</td></tr>";
			htmlString += '<tr style="border:1px solid black;"><td colspan="2">';
			for (var j=0 ; j < snapshotLines.length; j++) {
				if (j == resultLineInSnapshot+1) {
					htmlString += '<pre><span style="background-color: #fca6a6">';
					htmlString += encodeHTML(snapshotLines[j]);
					htmlString += "</span></pre>";
				}
				else
					htmlString += "<pre>" + encodeHTML(snapshotLines[j]) + "</pre>";
			}
			htmlString += "</td></tr>";
			htmlString += "</table><br/>";
		}
	}
	htmlString += "</div>";
	
	$('#linksResults').html(htmlString);
}

function showResultsForRuleAndPackage(selectedRule, selectedPackage, linksResults) {
	var htmlString = "";
	
	for (var i=0; i<linksResults.length; i++) {
		var resultData = linksResults[i];
		var result = resultData.result;
		var rule = result.rule;
		var ruleName = rule.name;
		var position = result.position;
		var file = position.resolvedFile;
		var filePackage = position.resolvedPackage;
		var snapshot = resultData.snapshot;
		var resultLineInSnapshot = snapshot.resultLine;
		var snapshotLines = snapshot.snapshotLines;
		
		if (filePackage == selectedPackage && ruleName == selectedRule) {
			htmlString += '<table width="100%" class="resultsAnalysisTable">';
			htmlString += "<tr><td><b>Rule</b></td><td>" + ruleName + "</tr>";
			htmlString += "<tr><td><b>Package</b></td><td>" + filePackage + "</td></tr>";
			htmlString += "<tr><td><b>File</b></td><td>" + file + "</td></tr>";
			htmlString += '<tr style="border:1px solid black;"><td colspan="2">';
			for (var j=0 ; j < snapshotLines.length; j++) {
				if (j == resultLineInSnapshot+1) {
					htmlString += '<pre><span style="background-color: #fca6a6">';
					htmlString += encodeHTML(snapshotLines[j]);
					htmlString += "</span></pre>";
				}
				else
					htmlString += "<pre>" + encodeHTML(snapshotLines[j]) + "</pre>";
			}
			htmlString += "</td></tr>";
			htmlString += "</table><br/>";
		}
	}
	
	$('#linksResults').html(htmlString);
}

function encodeHTML(value) {
	return $('<div/>').text(value).html();
}