var projectsMap;

$(document).ready(function() {
	// Enable jquery tooltips
	$("[title]").tooltip({
		// place tooltip on the right edge
		position: {
			my: 'left top-28',
			at: 'right+10',
			collision: 'none'
		},
		tooltipClass: 'rightTooltip'
	});
	
	var selectedProjectKey = getURLParameter("projectKey");
	//Get all the projects data from the ProjectsDataServlet
	$.ajax({
		async:false,
		type:'GET',
		url:'/docfacto-links-servlet/ProjectsDataServlet',
		success: function(responseText) {
			var servletResponse = $.parseJSON(responseText);
			
			if (servletResponse.type == "NO_PROJECTS_ADDED") {
				fillInNoProjectSelectedText();
				return;
			}
			else if (servletResponse.type == "PROJECTS_ADDED") {
			
				var projectsMapJsonString = servletResponse.data;
				projectsMap = $.parseJSON(projectsMapJsonString);
				
				fillInProjectsListSection(selectedProjectKey, projectsMap);
				
				if(selectedProjectKey == null) {
					fillInNoProjectSelectedText();
				}
				else {
					var selectedProject = projectsMap[selectedProjectKey];
					setProjectSelected(selectedProject.projectConfiguration.projectName);
					showRestrictionWarningText(selectedProject);
					fillInProjectDetailsPageForProject(selectedProjectKey, selectedProject);
					fillInSummaryPageForProject(selectedProjectKey, selectedProject);
					fillInErrorsPageForProject(selectedProjectKey, selectedProject);
				}
			}
			else {
				alert("Don't know what to do");
			}
		}
	});
	
	setUpConfirmAddProjectDialog();
	setUpAddNewProjectDialog();
	
	//TODO:Enable sortable with cookies
//	$( ".sortable" ).sortable();
//  $( ".sortable" ).disableSelection();
	
	//$("#tabs").tabs();
	
    var tabs = $("#tabs").tabs();
    tabs.find( ".ui-tabs-nav" ).sortable({
      axis: "x",
      stop: function() {
        tabs.tabs("refresh");
      }
    });
	
	$(".accordion").accordion({
		collapsible: true,
		heightStyle: "content"
	});
	
	setButtonIcons();
	setUpButtonEvents();
	
	$('#projectsListTab').on("click", function() {
		toggleProjectsList();
	});
});

function toggleProjectsList() {
	if ($('#projectsListWrapper').hasClass("closed")) {
		showProjectsList();
	}
	else {
		hideProjectsList();
	}
}

function showProjectsList() {
	$('#projectsListWrapper').removeClass("closed");
	
	$('#projectsListWrapper').css('border', '1px solid black');
	$('#projectsListWrapper').css('border-left', 'none');
	
	$('#projectsListWrapper').animate({
		width:'30em'
	}, 400, 'easeOutBack');
}

function hideProjectsList() {
	$('#projectsListWrapper').addClass("closed");
	
	$('#projectsListWrapper').animate({
		width:'0'
	}, 400, 'easeOutBack');
	
	$('#projectsListWrapper').css('border', 'none');
}

/**
 * This function keeps the projects tab in the same position relative to the top of the page
 * when scrolling so it doesn't disappear to the top.
 */
function fixProjectsTabPositionWhenScrolling() {
	// Get the current margin from the top
	var projectsListTabTopPosition = parseFloat($('#projectsListTab').css('margin-top'));
	
	// When scrolling add the scroll amount to the top margin.
	$(window).scroll(function() {
		var scrollAmount = parseFloat($(window).scrollTop());
		var newTopMarginForProjectsListTab = parseFloat(projectsListTabTopPosition + scrollAmount);
		$('#projectsListTab').css('margin-top', newTopMarginForProjectsListTab);
	});
}

function fillInNoProjectSelectedText() {
	$('#projectSelected').html("No project selected");
	$('#projectDetailsPage').html("<p>No project selected</p>");
	$('#summaryPage').html("<p>No project selected</p>");
	$('#errorsPage').html("<p>No project selected</p>");
}

function setUpConfirmAddProjectDialog() {
	$("#confirmAddProjectDialog").dialog({
		autoOpen: false,
		resizable:false,
		width:"auto",
		height:"auto",
		modal:false,
		stack:true,
		buttons: {
			"Yes": function() {
				addProject();
				$(this).dialog("close");
				return;
			},
			"No": function() {
				$(this).dialog("close");
				return;
			}
		}
	});
}

function setUpAddNewProjectDialog() {
	$('#addNewProjectDialog').dialog({
		  autoOpen: false,
		  autoResize: true,
	      modal: true,
	      width: 'auto',
	      height:'auto',
	      position: 'center',
	      buttons: {
	        "Add Project": function() {
	        	var projectName=$('#newProjectName').val();
	        	
	        	if (stringIsEmpty(projectName)) {
	    			alert("Please set a project name");
	    			return;
	    		}
	        	else {
		        	if (projectNameIsInProjectsMap(projectName)) {
		        		$('#confirmAddProjectDialog').dialog("open");
		        	}
		        	else {
		        		addProject();
		        	}
	        	}
	        },
	        Cancel: function() {
	          $(this).dialog("close");
	        }
	      },
	      close: function() {
	    	  //Do something when closed
	      },
	      open: function(event, ui) {
	    	  $('input').blur();
	      }
	});
}

function runLinksForProject(projectKey) {
	$.post('/docfacto-links-servlet/LinksServlet',
		{ action:"runLinksForProject",
			projectKey:projectKey
		},
		function(data) {
			var linksServletResponse = $.parseJSON(data);
			var type = linksServletResponse.type;
			if (type == "REFRESH") {
				location.reload();
			}
			else {
				alert("Don't know what to do");
			}
		}
	);
}

function showRestrictionWarningText(project) {
	var maxFileLimit = project.maxFileLimit;
	var maxFileLimitReached = project.maxFileLimitReached;
	
	if (maxFileLimitReached) {
		$('#projectSelected').append(getMaxFileLimitReachedHTMLString(maxFileLimit));
	}
}

function getMaxFileLimitReachedHTMLString(maxFileLimit) {
	htmlString = "";
	htmlString += '<span class="warningLabelWrapper">';
	htmlString += '<span class="warningLabel">';
	htmlString += 'WARNING: You have reached the max number of allowed files (' + maxFileLimit + ') according to your license. The remaining files have not been processed.';
	htmlString += '</span>';
	htmlString += '</span>';
	return htmlString;
}

function setButtonIcons() {
	$(".addProjectButton").button({
		icons: {
			primary:"ui-icon-plusthick"
		},
		text: true
	});
	
	$(".enableAllPackagesButton").button({
		icons: {
			primary:"ui-icon-check"
		},
		text: true
	});
	
	$(".disableAllPackagesButton").button({
		icons: {
			primary:"ui-icon-minus"
		},
		text: true
	});
	
	$(".updatePackagesEnabledButton").button({
		icons: {
			primary:"ui-icon-arrowrefresh-1-s"
		},
		text: true
	});
	
	$(".runLinksButton").button({
		icons: {
			primary:"ui-icon-play"
		},
		text: true
	});
	
	$(".goToProjectButton").button({
		icons: {
			primary:"ui-icon-carat-1-e"
		},
		text: true
	});
	
	$(".editProjectButton").button({
		icons: {
			primary:"ui-icon-pencil"
		},
		text: true
	});
	
	$(".cancelEditProjectButton").button({
		icons: {
			primary:"ui-icon-close"
		},
		text: true
	});
	
	$(".saveProjectChangesButton").button({
		icons: {
			primary:"ui-icon-disk"
		},
		text: true
	});
	
	$(".saveAndRunProjectChangesButton").button({
		icons: {
			primary:"ui-icon-play"
		},
		text: true
	});
	
	$(".deleteProjectButton").button({
		icons: {
			primary:"ui-icon-trash"
		},
		text: true
	});
}

function addProject() {
	var projectName=$('#newProjectName').val();
	var workingDirectory=$('#newWorkingDirectory').val();
	var sourceDirectory=$('#newSourceDirectory').val();
	var docDirectory=$('#newDocDirectory').val();
	var xmlConfigDirectory=$('#newXMLConfigDirectory').val();
	var sourceExtensions=$('#newSourceExtensions').val();
	var docExtensions=$('#newDocExtensions').val();
	
	$.ajax({
		async:false,
		type:'POST',
		url:'/docfacto-links-servlet/LinksServlet',
		data:{
			"action":"addNewProject",
			"newProjectName":projectName,
			"newWorkingDirectory":workingDirectory,
			"newSourceDirectory":sourceDirectory,
			"newDocDirectory":docDirectory,
			"newXMLConfigDirectory":xmlConfigDirectory,
			"newSourceExtensions":sourceExtensions,
			"newDocExtensions":docExtensions
		},
		success: function(data) {
			var linksServletResponse = $.parseJSON(data);
			var type = linksServletResponse.type;
			if (type == "REDIRECT_TO_PROJECT_KEY") {
				var projectKeyAdded = linksServletResponse.data;
				reloadPageWithNewProjectKey(projectKeyAdded);
			}
			else if (type == "NO_MORE_NEW_PROJECTS_ALLOWED") {
				alert("You have reached the maximum number of projects allowed for your license");
			}
			else {
				alert("Don't know what to do for response type: " + type);
			}
		}
	});
}

function projectNameIsInProjectsMap(targetProjectName) {
	for (var projectKey in projectsMap) {
		var project = projectsMap[projectKey];
		var projectConfiguration = project.projectConfiguration;
		var projectName = projectConfiguration.projectName;
		
		if (projectName == targetProjectName) {
			return true;
		}
		
		return false;
	}
}

function stringIsEmpty(inputString) {
	// Check if the browser support the 'trim()' method, if not, do the trim
	if (!('trim' in String.prototype)) {
	    String.prototype.trim= function() {
	        inputString.replace(/^\s+/, '').replace(/\s+$/, '');
	    };
	}
	
	return (inputString.trim() === '');
}

function getProjectListItemHTMLString(selectedProjectKey, projectKey, projectConfiguration) {
	var htmlString = "";
	
	htmlString += '<div class="projectsListItemWrapper">';
	
	if (selectedProjectKey == projectKey) {
		htmlString += '<div class="selectedProjectsListItem">';
	}
	else { 
		htmlString += '<div class="projectsListItem">';
	}
	
	//TODO: Do sortable with cookie
	//htmlString += '<span class="projectsListItemIcon ui-icon ui-icon-arrowthick-2-n-s"></span>';
	htmlString += '<div class="projectsListItemText">' + projectConfiguration.projectName + "</div>";
	
	if (selectedProjectKey == projectKey) {
		htmlString += '<button class="projectsListItemButton runLinksButton" projectKey="' + projectKey + '">Run Links</button>';
	}
	else {
		htmlString += '<button class="projectsListItemButton goToProjectButton" projectKey="' + projectKey + '">Go to project</button>'; 
	}
	
	htmlString += '</div>';
	htmlString += '</div>';
	
	return htmlString;
}

function getURLParameter(name) {
	var parametersString = window.location.search.substring(1);
    var parametersArray = parametersString.split('&');
    for(var i = 0; i < parametersArray.length; i++){
        var keyValuePair = parametersArray[i].split('=');
        if(keyValuePair[0] == name){
            return keyValuePair[1];
        }
    }
}

function reloadPageWithNewProjectKey(projectKey) {
	/*
	 * queryParameters -> handles the query string parameters
	 * queryString -> the query string without the fist '?' character
	 * re -> the regular expression
	 * m -> holds the string matching the regular expression
	 */
	var queryParameters = {};
	var queryString = location.search.substring(1);
	var re = /([^&=]+)=([^&]*)/g;
	var m;
	 
	// Creates a map with the query string parameters
	while (m = re.exec(queryString)) {
	    queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
	}
	 
	// Add new parameters or update existing ones
	queryParameters['projectKey'] = projectKey;
	 
	/*
	 * Replace the query portion of the URL.
	 * jQuery.param() -> create a serialized representation of an array or
	 *     object, suitable for use in a URL query string or Ajax request.
	 */
	location.search = $.param(queryParameters); // Causes page to reload
}

function fillInProjectsListSection(selectedProjectKey, projectsMap) {
	fillInNumberOfProjectsLeft();
	
	var projectsHTMLString = "";
	
	projectsHTMLString += '<ul class="sortable projectsList">';
	
	for (var key in projectsMap) {
		var project = projectsMap[key];
		var projectKey = project.key;
		var projectsConfiguration = project.projectConfiguration;
		var projectListItemString = getProjectListItemHTMLString(selectedProjectKey, projectKey, projectsConfiguration);
		
		projectsHTMLString += '<li class="ui-state-default projectsListItemOuter">';
		projectsHTMLString += projectListItemString;
		projectsHTMLString += '</li>';
	}
	projectsHTMLString += '</ul>';
	
	$('#projectsList').html(projectsHTMLString);
}

function fillInNumberOfProjectsLeft() {
	// Get the number of projects left
	$.ajax({
		async:false,
		type:'GET',
		url:'/docfacto-links-servlet/ProjectsDataServlet',
		data:{
			"action":"getProjectsLeft"
		},
		success: function(responseText) {
			var linksServletResponse = $.parseJSON(responseText);
			var type = linksServletResponse.type;
			if (type == "NO_LIMIT_ON_PROJECTS") {
				/* $("#numberOfProjectsLeft").html("");*/
				$(".addProjectButton").prop('disabled', false);
			}
			else if (type == "PROJECTS_LEFT") {
				var projectsLeft = linksServletResponse.data;
				if (projectsLeft == 0) {
					$(".addProjectButton").prop('disabled', true);
					$("#numberOfProjectsLeft").html("You have reached the maximum number of projects allowed for your license");
				}
				else {
					$("#numberOfProjectsLeft").html("Number of projects left: " + projectsLeft);
				}
			}
			else {
				alert("Do not know what to do with response type: " + type);
			}
		}
	});
}

function getNumberOfNonWorkingLinks(projectStatistics) {
	var linksWithUnknownType = getStatisticValueForKey("links-with-unknown-type", projectStatistics);
	
	var noOfNonWorkingSourceLinks = getNumberOfNonWorkingSourceLinks(projectStatistics);
	
	var noOfNonWorkingDocLinks = getNumberOfNonWorkingDocLinks(projectStatistics);
	
	var noOfNonWorkingLinks = linksWithUnknownType
								+ noOfNonWorkingSourceLinks
								+ noOfNonWorkingDocLinks;
	
	return noOfNonWorkingLinks;
}

function getNumberOfNonWorkingSourceLinks(projectStatistics) {
	var sourceLinksWithNoUri  = getStatisticValueForKey("source-link-tag-no-uri", projectStatistics);
	var sourceLinksWithInvalidUri = getStatisticValueForKey("source-link-invalid-uri", projectStatistics);
	var brokenSourceLinks = getStatisticValueForKey("source-broken-link", projectStatistics);
	
	var noOfNonWorkingSourceLinks = sourceLinksWithNoUri
								+ sourceLinksWithInvalidUri
								+ brokenSourceLinks;
	
	return noOfNonWorkingSourceLinks;
}

function getNumberOfNonWorkingDocLinks(projectStatistics) {
	var docLinksWithNoUri  = getStatisticValueForKey("doc-link-tag-no-uri", projectStatistics);
	var docLinksWithInvalidUri = getStatisticValueForKey("doc-link-invalid-uri", projectStatistics);
	var brokenDocLinks = getStatisticValueForKey("doc-broken-link", projectStatistics);
	
	var noOfNonWorkingDocLinks = docLinksWithNoUri
								+ docLinksWithInvalidUri
								+ brokenDocLinks;
	
	return noOfNonWorkingDocLinks;
}

function getChartHTMLString(projectChartId) {
	return '<div id="' +  projectChartId + '"></div>';
}

function getStatisticValueForKey(targetKey, projectStatistics) {
	for (var i=0; i < projectStatistics.length; i++) {
		var statistic = projectStatistics[i];
		var rule = statistic.rule;
		var ruleKey = rule.key;
		
		if (ruleKey == targetKey) {
			return statistic.value;
		}
	}
	return 0;
}

function setProjectSelected(projectName) {
	$('#projectSelected').html("Project Selected: " + projectName);
}
