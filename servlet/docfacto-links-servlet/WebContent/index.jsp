<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--  reset css in case there is difference between browsers -->
<link rel="stylesheet" type="text/css" href="css/reset.css">

<!--Load the AJAX API-->
<script type="text/javascript" src="js-lib/jquery-2.0.2.js"></script>
<script type="text/javascript" src="js-lib/jquery-ui-1.10.3.js"></script>
<script type="text/javascript" src="js-lib/jquery.json-2.4.js"></script>
<script type="text/javascript" src="js-lib/google-jsapi.js"></script>

<!-- dynatree resources -->
<script type="text/javascript" src="js-lib/jquery.cookie.js"></script>
<link rel='stylesheet' type='text/css' href='dynatree/ui.dynatree.css'>
<script type="text/javascript" src='js-lib/jquery.dynatree.js'></script>

<script type="text/javascript" src="linksChart.js"></script>
<script type="text/javascript" src="navigation.js"></script>
<script type="text/javascript" src="errorsAnalysis.js"></script>
<script type="text/javascript" src="errorsPage.js"></script>
<script type="text/javascript" src="packagesFilter.js"></script>
<script type="text/javascript" src="summaryCharts.js"></script>
<script type="text/javascript" src="summaryPage.js"></script>
<script type="text/javascript" src="projectDetails.js"></script>
<script type="text/javascript" src="index.js"></script>

<link rel="stylesheet" type="text/css" href="css/styles.css">
<link rel="stylesheet" type="text/css"
	href="css/links-theme/jquery-ui-1.10.3.custom.css">
<link rel="stylesheet" type="text/css" href="navigationLogin.css">
<title>Links Main Page</title>
</head>
<body>
	<%
		Object loggedIn = request.getSession().getAttribute("isLoggedIn");
	%>

	<header id="banner" class="body"></header>

	<section id="content" class="body">
		<div id="confirmAddProjectDialog" title="Add New Project"
			class="dialog">
			<p>
				<span class="ui-icon ui-icon-alert"
					style="float: left; margin: 0 7px 20px 0;"></span> Project Name
				already exists. Are you sure you want to add the project?
			</p>
		</div>

		<div id="addNewProjectDialog" title="Add New Project" class="dialog">
			<div id="addNewProjectForm">
				<br />
				<table id=addProjectTable>
					<tr>
						<td class=form-label>Project Name</td>
						<td><input type=text id=newProjectName 
							name=newProjectName size="51" value=""
							title="The name for this project so you can distinguish between different projects."
							placeholder="Project Name"
							/>
						</td>
					</tr>
					<tr>
						<td class=form-label>Source Directory</td>
						<td><input type=text id=newSourceDirectory
							name=newSourceDirectory size="51" value=""
							title="The absolute path to the directory containing the source files you to be processed."
							placeholder="/Users/user/workspace/project/src"
							/>
						</td>
					</tr>
					<tr>
						<td class=form-label>Documentation Directory</td>
						<td><input type=text id=newDocDirectory 
							name=newDocDirectory size="51" value=""
							title="The absolute path to the directory containing the document files to be processed."
							placeholder="/Users/user/workspace/project/doc"
							/>
						</td>
					</tr>
					<tr>
						<td class=form-label>XML Configuration Directory</td>
						<td><input type=text id=newXMLConfigDirectory
							name=newXMLConfigDirectory size="51" value=""
							title="The absolute path to the Docfacto XML Configuration file which contains the Docfacto license and Links configuration you want to use."
							placeholder="/Users/user/workspace/project/Docfacto.xml"
							/>
						</td>
					</tr>
					<tr>
						<td class=form-label>Source File Extensions</td>
						<td><input type=text id=newSourceExtensions
							name=newSourceExtensions size="51" value=.java
							title="A list of the source file extensions which you want to be processed separated by commas. Source files which do not end in any of the given extensions will not be processed. For example: .java,.js,.html"
							placeholder=".java,.js,.html"
							/>
						</td>
					</tr>
					<tr>
						<td class=form-label>Doc File Extensions</td>
						<td><input type=text id=newDocExtensions
							name=newDocExtensions size="51" value=.dita
							title="A list of the document file extensions which you want to be processed separated by commas. Document files which do not end in any of the given extensions will not be processed. For example: .dita,.xml,.txt"
							placeholder=".dita,.xml,.txt"
							/>
							</td>
					</tr>
					<tr>
						<td class=form-label>Base Directory</td>
						<td><input type=text id=newWorkingDirectory
							name=newWorkingDirectory size="51" 
							title="Optional: A new base directory to override the one set in the Docfacto XML Configuration, so all paths will now be calculated according to this base directory as opposed to the one set into the Docfacto XML Configuration file."
							placeholder="."
							/>
						</td>
					</tr>
				</table>
			</div>
		</div>

		<div id="projectsListWrapper" class="closed">
			<div id="projectsListContainer">
				<div id="projectsPage">
					<%
						if (loggedIn != null && (Boolean) loggedIn) {
					%>
					<button class="addProjectButton" name="addProjectButton">Add
						a Project</button>
					<br />
					<%
						} else {
					%>
					<p style="text-align: center;">Log in to add projects</p>
					<%
						}
					%>

					<div id="numberOfProjectsLeftWrapper">
						<div id="numberOfProjectsLeft">
						</div>
					</div>

					<div id="projectsList">
						<p style="text-align: center; padding: 2em">No projects added
							yet</p>
					</div>
				</div>
			</div>
		</div>
		
		<div id="projectsListTab" class="projectsListTab">Projects</div>

		<div id="projectContent">
			<div id="projectSelectedWrapper">
				<div id="projectSelected"></div>
			</div>
			<div id="tabContainerWrapper">
				<div id="tabContainer">
					<div id="tabs">
						<ul>
							<li><a href="#summaryTab">Summary</a></li>
							<li><a href="#errorsTab">Errors</a></li>
							<li><a href="#projectDetailsTab">Project Details</a></li>
						</ul>
						<div id="projectDetailsTab">
							<div id="projectDetailsPage">
								<div id="projectConfigurationWrapper">
									<div id="projectConfigurationAccordionWrapper"
										class="accordion">
										<h3>Project Configuration</h3>
										<div id="projectConfiguration">
											<div id="projectConfigurationForm"></div>
											<%
												if (loggedIn != null && (Boolean) loggedIn) {
											%>
											<div id="projectConfigurationButtons"></div>
											<%
												} else {
													//Do Nothing
												}
											%>
										</div>
									</div>
								</div>
								<div id="projectErrors"></div>
							</div>
						</div>
						<div id="summaryTab">
							<div id="summaryPage">
								<div id="projectSummaryTextWrapper">
									<div id="projectSummaryTextAccordionWrapper" class="accordion">
										<h3>Summary</h3>
										<div id="projectSummaryText"></div>
									</div>
								</div>
								<div id="projectSummaryChartsWrapper">
									<div id="projectSummaryChartsAccordionWrapper"
										class="accordion">
										<h3>Summary Charts</h3>
										<div id="projectSummaryCharts"></div>
									</div>
								</div>
								<div id="projectStatisticsWrapper">
									<div id="projectStatisticsAccordionWrapper" class="accordion">
										<h3>Statistics</h3>
										<div id="projectStatistics"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="errorsTab">
							<div id="errorsPage">
								<div id="packagesSelectorWrapper">
									<div id="packagesSelectorAccordionWrapper" class="accordion">
										<h3>Packages</h3>
										<div id="packagesSelector">
											<div id="packagesSelectorTree"></div>
											<div id="packagesSelectorButtons">
												<button class="updatePackagesEnabledButton"
													style="display: inline; float: right">Update</button>
												<button type="button" class="enableAllPackagesButton"
													style="display: inline; float: left">Enable All</button>
												<button type="button" class="disableAllPackagesButton"
													style="display: inline; float: left; clear: left;">Disable
													All</button>
											</div>
										</div>
									</div>
								</div>
								<div id="projectResultsWrapper">
									<div id="projectResultsAccordionWrapper" class="accordion">
										<h3>Results</h3>
										<div id="projectResults">
											<table>
												<tr>
													<td valign="top" style="width: 30%"><div
															id="linksRules"></div></td>
													<td valign="top" style="width: 60%"><div
															id="linksPackages"></div></td>
												</tr>
												<tr>
													<td valign="top" colspan="2" style="width: 100%"><div
															id="linksFiles"></div></td>
												</tr>
											</table>
											<br />
											<div id="linksResults"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>