$(document).ready(function() {
	$.ajax({
		async:false,
		type:'GET',
		url:'/docfacto-links-servlet/LoginServlet',
		data:{
			"action":"getLoginStatus"
		},
		success: function(responseText) {
				var htmlString = "";

				var isLoggedIn = false;
				
				var linksServletResponse = $.parseJSON(responseText);
				var type = linksServletResponse.type;
				if (type == "IS_LOGGED_IN") {
					var isLoggedInResponseData = linksServletResponse.data;
					if (isLoggedInResponseData == "true") {
						isLoggedIn = true;
					}
				}
				
				var navLoginHTMLString = "";
				if (isLoggedIn == true) {
					navLoginHTMLString = getNavLoggedInHTMLString();
				}
				else {
					navLoginHTMLString = getNavNotLoggedInHTMLString();
				}
				
				htmlString += '<nav>'
							+ '<ul>'
							//+ '<li><a id="projectsLink" href="#">Projects</a></li>'
							+ navLoginHTMLString
							+ '</ul>'
							+ '</nav>';
				
				htmlString += '<a href="http://docfacto.com/" target="_blank" onmouseover="onLogoHover();" onmouseout="offLogoHover();"><img class="logo" src="images/doodle-links-icon.png"/></a>';
				
				$('#banner').html(htmlString);
			}
	});

	$(".navLoginButton").button();
	$(".navLogoutButton").button();

	$('body').on('click', '#projectsLink', function() {
		window.location.href = 'projectsList.jsp';
	});
	
	$('body').click(function(e){
		  if($(e.target).closest('#navLoginContent').length === 0 && ($(e.target).closest('#navLoginTrigger').length === 0)){
			  $('#navLoginxsContent').hide();
		  }
	});
	
	$('body').on('click', '#navLoginTrigger', function() {
		toggleLoginBox();
	});
	
	$('body').on('click', '.navLoginButton', function() {
		var usernameInput=$('#username').val();
		var passwordInput=$('#password').val();
		
		$.post('/docfacto-links-servlet/LoginServlet', 
			{ action:"login",
				username:usernameInput,
				password:passwordInput
			},
			function(data) {
				var linksServletResponse = $.parseJSON(data);
				var type = linksServletResponse.type;
				if (type == "LOGIN_FAILED") {
					var loginFailedData = linksServletResponse.data;
					alert(loginFailedData);
				}
				else {
					location.reload();
				}
			}
		);
	});
	
	$('body').on('click', '.navLogoutButton', function() {
		$.post('/docfacto-links-servlet/LoginServlet', 
			{ action:"logout" },
			function(data) {
				location.reload();
			}
		);
	});
});

function getNavLoggedInHTMLString() {
	var htmlString = "";
	
	htmlString += '<li id="navLogin">'
				+ 	'<a id="navLoginTrigger" href="#">Logged in</a>'
				+ 	'<div id="navLoginContent" >'
				+ 		'<form">'
				+ 			'<fieldset id="actions">'
				+ 				'<input type="button" class="navLogoutButton" value="Log out">'
				+ 			'</fieldset>'
				+ 		'</form>'
				+ 	'</div>'
				+ '</li>';
	
	return htmlString;
}

function getNavNotLoggedInHTMLString() {
	var htmlString = "";
	
	htmlString += '<li id="navLogin">'
				+ 	'<a id="navLoginTrigger" href="#">Log in</a>'
				+ 	'<div id="navLoginContent" >'
				+ 		'<form>'
				+ 			'<fieldset id="inputs">'
				+ 				'<input id="username" type="text" placeholder="username" required><br />'
				+ 				'<input id="password" type="password" placeholder="password" required>'
				+ 			'</fieldset>'
				+ 			'<fieldset id="actions">'
				+ 				'<input type="button" class="navLoginButton" value="Log in">'
				+ 			'</fieldset>'
				+ 		'</form>'
				+ 	'</div>'
				+ '</li>';
	
	return htmlString;
}

function toggleLoginBox() {
	$('#navLoginContent').slideToggle("fast", "easeOutBack");
}

function onLogoHover() {
	fadeInLogoThenFadeOut();
}

function fadeInLogoThenFadeOut() {
	$(".logo").animate({
		"opacity":1,
		"easing":"easeOutBack"
	}, 300, fadeOutLogoThenFadeIn());
}

function fadeOutLogoThenFadeIn() {
	$(".logo").animate({
		"opacity":0.4,
		"easing":"easeInBack"
	}, 600, fadeInLogoThenFadeOut);
}

function offLogoHover() {
	fadeInLogoAndStop();
}

function fadeInLogoAndStop() {

	$(".logo").stop(true);
	
	$(".logo").animate({
		"opacity":1
	}, 100);
}
