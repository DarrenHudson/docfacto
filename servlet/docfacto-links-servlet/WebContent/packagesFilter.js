var packagesEnabledMap = {};

function packagesEnabledState(projectKey, packagesEnabledMap) {
	this.projectKey = projectKey;
	this.packagesEnabledMap = packagesEnabledMap;
}

function enableAllPackages(packagesEnabledTreeDiv) {
	$("#" + packagesEnabledTreeDiv).dynatree("getRoot").visit(function(node){
        node.select(true);
      });
    return false;
}

function disableAllPackages(packagesEnabledTreeDiv) {
	$("#" + packagesEnabledTreeDiv).dynatree("getRoot").visit(function(node){
        node.select(false);
      });
    return false;
}

function setAllPackagesEnabledStatusTo(status) {
	for (var packageName in packagesEnabledMap) {
		packagesEnabledMap[packageName] = status;
	}
}

function filterStatisticsWithDisabledPackages(projectStatistics) {
	
	var filteredLinksStatistics = getDeepCopyOfArray(projectStatistics);
	
	for (var i=0; i < filteredLinksStatistics.length; i++) {
		var statistic = filteredLinksStatistics[i];
		
		var packageToValueMap = statistic.packageToValueMap;
		
		for (var packageName in packageToValueMap) {
			if (packagesEnabledMap[packageName] == false) {
				delete packageToValueMap[packageName];
			}
		}
	}
	
	return filteredLinksStatistics;
}

function filterResultsWithDisabledPackages(linksResults) {
	
	var filteredLinksResults = [];
	
	for (var i=0; i<linksResults.length; i++) {
		var resultData = linksResults[i];
		var result = resultData.result;
		var position = result.position;
		var packageName = position.resolvedPackage;
		
		if (packagesEnabledMap[packageName] != false) {
			filteredLinksResults.push(linksResults[i]);
		}
	}
	
	return filteredLinksResults;
}

function getDeepCopyOfArray(array) {
	return JSON.parse(JSON.stringify(array));
}

function addAllPackagesToPackagesEnabled(linksStatistics) {
	for (var i=0; i < linksStatistics.length; i++) {
		var statistic = linksStatistics[i];
		var packageToValueMap = statistic.packageToValueMap;
		
		for (var packageName in packageToValueMap) {
			packagesEnabledMap[packageName] = true;
		}
	}
}

function setPackagesEnabledTreeToDiv(div, projectKey, linksStatistics) {
	var treeData = getTreeDataArrayFromStatistics(linksStatistics);
	
	if (treeData.length == 0) {
		$("#" + div).html('<p style="text-align:center">No packages found</p>');
		return;
	}
	
	// Attach the dynatree widget to an existing <div id="tree"> element
    // and pass the tree options as an argument to the dynatree() function:
    $("#" + div).dynatree({
    	title: "Packages Enabled Tree",
    	imagePath:"../images/dynatree",
    	persist: true, // Keep status to cookie
    	cookieId: projectKey,
    	checkbox: true,
    	clickFolderMode: 2, // 1:activate, 2:expand, 3:activate and expand
    	selectMode: 3, // 1:single, 2:multi, 3:multi-hier
        children: treeData
    });
    
    syncPackagesEnabledTreeWithPackagesEnabledMap("packagesEnabledTree");
}

function syncPackagesEnabledTreeWithPackagesEnabledMap(packagesEnabledTreeDiv) {
	var selectedRootNodes = $("#" + packagesEnabledTreeDiv).dynatree("getSelectedNodes");
	var selectedRootNodePaths = [];
	
	// reset them all back to false
	for (var packageName in packagesEnabledMap) {
		packagesEnabledMap[packageName] = false;
	}
	
	// Go through all the root nodes selected by the tree and the ones which aren't the beginning of
	// the packages in the packages enabled map are set to true (enabled)
	for (var i=0; i < selectedRootNodes.length; i++) {
		selectedRootNodePaths.push(getNodePathTitle(selectedRootNodes[i]));
		var selectedNodePathTitle = getNodePathTitle(selectedRootNodes[i]);
		for (var packageName in packagesEnabledMap) {
			// if package name starts with the selected node path
			if (packageName.indexOf(selectedNodePathTitle) == 0) {
				packagesEnabledMap[packageName] = true;
			}
		}
	}
}

function getTreeDataArrayFromStatistics(linksStatistics) {
	var treeDataArray = [];
	
	for (var i=0; i < linksStatistics.length; i++) {
		var statistic = linksStatistics[i];
		var packageToValueMap = statistic.packageToValueMap;
		
		for (var packageName in packageToValueMap) {
			addPackageToTreeArray(packageName, treeDataArray);
			//var lastTreeDataAdded = treeDataArray[treeDataArray.length-1];
			//console.log("Added! Looks like this: " + JSON.stringify(lastTreeDataAdded));
			//console.log("First children is: " + JSON.stringify(lastTreeDataAdded.children));
		}
	}
	
	return treeDataArray;
}

function addPackageToTreeArray(packageName, treeDataArray) {
	if (packageName == null || packageName == "" || treeDataArray == null) {
		return;
	}
	
	if (packageName[0] == "/") {
		packageName = packageName.substr(1);
	}
	
	var packageNameFoldersArray = packageName.split("/");
	var firstFolderInPackage = packageNameFoldersArray[0];
	
	// The rest of the package without the first folder just found
	var packageNameWithoutFirstFolder = packageName.substring(packageName.indexOf(firstFolderInPackage)+firstFolderInPackage.length);
	for (var i=0; i < treeDataArray.length; i++) {
		var treeData = treeDataArray[i];
		if (treeData != null) {
			//console.log("tree data at position: " + i + " is " + JSON.stringify(treeData));
			if (treeData.title == firstFolderInPackage) {
				var treeDataChildrenArray = treeData.children;
				//console.log("so add the package without first folder to children: " + JSON.stringify(treeDataChildrenArray));
				addPackageToTreeArray(packageNameWithoutFirstFolder, treeDataChildrenArray);
				return;
			}
		}
	}
	//Didn't find matching first folder so start to add package folders into treeDataArray
	if (packageNameFoldersArray.length > 1) {
		var newChildrenTreeArray = [];
		addPackageToTreeArray(packageNameWithoutFirstFolder, newChildrenTreeArray);
		
		var newTreeData = {title: firstFolderInPackage,
							isFolder: true,
							select: true,
							children: newChildrenTreeArray
							};
		treeDataArray.push(newTreeData);
	}
	else {
		var newTreeData = {title: firstFolderInPackage,
				isFolder: true,
				select: true,
				children: []
		};
		treeDataArray.push(newTreeData);
	}
}

function getNodePathTitle(node) {
	var path = [];
	node.visitParents(function(node) {
		if (node.parent != null) {
			if (node.parent) {
				path.unshift(node.data.title);
			}	
		}
	}, true);
	
	return path.join("/");
}