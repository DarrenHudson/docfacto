google.load('visualization', '1', { 'packages' : [ 'corechart' ] });
google.load('visualization', '1', { 'packages' : [ 'gauge' ] });

var maxNoOfPackagesToShowInCharts = 9;

function roundToOneDecimalPlaces(number) {
	var numberTimesTen = number * 10;
	var numberTimesTenRounded = Math.round(numberTimesTen);
	var numberToOneDecimalPlaces = numberTimesTenRounded / 10;
	return numberToOneDecimalPlaces;
}

function drawGaugeChart(idToDrawChart, numeratorValue, denominatorValue, emptyDenominatorErrorMessage, invertColors, selectHandler) {

	if(denominatorValue == 0) {
		$("#" + idToDrawChart).html('<p><font size="4">' + emptyDenominatorErrorMessage + '</font></p>');
		return;
	}
	
	var percentage = (numeratorValue / denominatorValue) * 100;
	var percentageToOneDecimalPlace = roundToOneDecimalPlaces(percentage);
	
	var data = new google.visualization.DataTable();
	
    data.addColumn('string', 'Label');
    data.addColumn('number', 'Percentage');
    data.addRows([
      ['%', percentageToOneDecimalPlace]
    ]);

    drawGaugeChartToDiv(data, idToDrawChart, invertColors, selectHandler);
}

function drawCodeAndDocLinesChart(projectStatistics, idToDrawChart, selectHandler) {
	var noOfCodeLines = 0;
	var noOfDocLines = 0;
	
	for (var i=0; i < projectStatistics.length; i++) {
		var statistic = projectStatistics[i];
		
		var rule = statistic.rule;
		var ruleKey = rule.key;
		
		if (ruleKey == "code-lines") {
			noOfCodeLines = statistic.value;
		}
		
		if (ruleKey == "doc-lines") {
			noOfDocLines = statistic.value;
		}
	}
	
	if(noOfCodeLines == 0 && noOfDocLines == 0) {
		$("#" + idToDrawChart).html('<p><font size="4">No source or code lines found</font></p>');
		return;
	}
	
	var data = new google.visualization.DataTable();
	
    data.addColumn('string', 'Type');
    data.addColumn('number', 'Number of Lines');
    data.addRows([
      ['Code Lines', noOfCodeLines],
      ['Doc Lines', noOfDocLines]
    ]);

    drawPieChartToDiv(data, idToDrawChart, selectHandler);
}

function drawSourcePackageChart(projectStatistics, idToDrawChart, selectHandler) {
	var sourceFilesProcessedStatistic = getStatisticWithRuleKey(projectStatistics, "source-files-processed");
	var sourceFilesWithDocLinkStatistic = getStatisticWithRuleKey(projectStatistics, "source-files-with-doc-link");
	if ((getMapSize(sourceFilesProcessedStatistic.packageToValueMap) - getMapSize(sourceFilesWithDocLinkStatistic.packageToValueMap)) > 0) {
		drawPackageChart(projectStatistics, idToDrawChart, selectHandler, sourceFilesProcessedStatistic, sourceFilesWithDocLinkStatistic, "Number of files without doc");
	}
	else {
		$("#" + idToDrawChart).html('<p id="sourceCongratulationsText" align="center"><font size="4">All files are linked to doc</font></p>');
	}
}

function drawDocPackageChart(projectStatistics, idToDrawChart, selectHandler) {
	
	var docFilesProcessedStatistic = getStatisticWithRuleKey(projectStatistics, "doc-files-processed");
	var docFilesWithDocLinkStatistic = getStatisticWithRuleKey(projectStatistics, "doc-files-with-source-link");
	
	if ((getMapSize(docFilesProcessedStatistic.packageToValueMap) - getMapSize(docFilesWithDocLinkStatistic.packageToValueMap)) > 0) {
		drawPackageChart(projectStatistics, idToDrawChart, selectHandler, docFilesProcessedStatistic, docFilesWithDocLinkStatistic, "Number of files without source");
	}
	else {
		$("#" + idToDrawChart).html('<p id="docCongratulationsText" align="center"><font size="4">All files are linked to source</font></p>');
	}
}

function flash(el, c1, c2) {
    var text = document.getElementById(el);
    text.style.color = (text.style.color == c2) ? c1 : c2;
}

function getMapSize(map) {
	var size = 0;
	
	for (var key in map) {
		size++;
	}
	
	return size;
}
	
function getPieChartDataArrayFromArrayOfPackageAndValuePairs(packageAndValueArray) {
	var pieChartDataArray = new Array();

	for (var i=0; i<packageAndValueArray.length; i++) {
		var packageAndValue = packageAndValueArray[i];
		
		var packageName = packageAndValue.packageName;
		var value = packageAndValue.value;
		
		var valuesArray = new Array();
		valuesArray[0] = packageName;
		valuesArray[1] = value;
		pieChartDataArray.push(valuesArray);
	}
	
	return pieChartDataArray;
}

function getStatisticWithRuleKey(statistics, targetRuleKey) {
	for (var i=0; i < statistics.length; i++) {
		var statistic = statistics[i];
		
		var rule = statistic.rule;
		var ruleKey = rule.key;
		
		if (ruleKey == targetRuleKey) {
			return statistic;
		}
	}
	return null;
}

function PackageAndValue(packageName, value) {
	this.packageName = packageName;
	this.value = value;
}

function addPackageAndValueToMapOfHighestValuesIfItIsHighEnough(packageAndValuePairsWithHighestValues, packageAndValue) {
	
	if (packageAndValuePairsWithHighestValues.length < maxNoOfPackagesToShowInCharts) {
		putPackageNameAndValueIntoCorrectPosition(packageAndValuePairsWithHighestValues, packageAndValue);
		return;
	}
	
	for (var i=0; i<packageAndValuePairsWithHighestValues.length; i++) {
		if (packageAndValuePairsWithHighestValues[i].value < packageAndValue.value) {
			putPackageNameAndValueIntoCorrectPosition(packageAndValuePairsWithHighestValues, packageAndValue);
			return;
		}
	}
}

function putPackageNameAndValueIntoCorrectPosition(packageAndValuePairsWithHighestValues, packageAndValue) {
	if (packageAndValuePairsWithHighestValues.length < maxNoOfPackagesToShowInCharts)
		packageAndValuePairsWithHighestValues.push(packageAndValue);
	else
		packageAndValuePairsWithHighestValues[0] = packageAndValue;
	
	packageAndValuePairsWithHighestValues.sort(packageAndValueSortingAscendingFunction);
}

function packageAndValueSortingAscendingFunction(packageAndValue1, packageAndValue2) {
	return packageAndValue1.value - packageAndValue2.value;
}

function drawPackageChart(projectStatistics, idToDrawChart, selectHandler, filesProcessedStatistic, correctFilesStatistic, chartValuesDescription) {
	if (filesProcessedStatistic == null || correctFilesStatistic == null) {
		//Couldn't get the necessary statistics
		return;
	}
	
	var filesProcessedPackageToValueMap = filesProcessedStatistic.packageToValueMap;
	var correctFilesPackageToValueMap = correctFilesStatistic.packageToValueMap;
	
	var packageAndValuePairsWithMostIncorrectFiles = new Array();
	
	var totalNoOfIncorrectFiles = 0;
	
	for (var packageName in filesProcessedPackageToValueMap) {
		var noOfFiles = filesProcessedPackageToValueMap[packageName];
		var noOfCorrectFiles = correctFilesPackageToValueMap[packageName];
		if (noOfFiles == null) {
			return;
		}
		
		if (noOfCorrectFiles == null) {
			noOfCorrectFiles = 0;
		}
		
		var noOfIncorrectFiles = noOfFiles - noOfCorrectFiles;
		totalNoOfIncorrectFiles += noOfIncorrectFiles;
		
		if (noOfIncorrectFiles > 0) {
			var noOfIncorrectFilesPackageAndValue = new PackageAndValue(packageName, noOfIncorrectFiles);
			addPackageAndValueToMapOfHighestValuesIfItIsHighEnough(packageAndValuePairsWithMostIncorrectFiles, noOfIncorrectFilesPackageAndValue);
		}
	}
	
	var pieChartDataArray = getPieChartDataArrayFromArrayOfPackageAndValuePairs(packageAndValuePairsWithMostIncorrectFiles);
	
	var data = new google.visualization.DataTable();
	
    data.addColumn('string', 'Package');
    data.addColumn('number', chartValuesDescription);
    data.addRows(pieChartDataArray);
    
	var totalNoOfIncorrectFilesShownInChartSoFar = 0;
	
	for (var i=0; i<pieChartDataArray.length; i++) {
		var pieChartData = pieChartDataArray[i];
		totalNoOfIncorrectFilesShownInChartSoFar += pieChartData[1];
	}
	
	var remainingNoOfIncorrectFiles = totalNoOfIncorrectFiles - totalNoOfIncorrectFilesShownInChartSoFar;
	
	if (remainingNoOfIncorrectFiles > 0)
		data.addRow(['Other', remainingNoOfIncorrectFiles]);
	
    drawPieChartToDiv(data, idToDrawChart, selectHandler);
}

function drawGaugeChartToDiv(data, idToDrawChart, invertColors, selectHandler) {
	var htmlString = "";
	
	// The div to actually put the gauge in in
	var gaugeId = idToDrawChart + "Gauge";
	htmlString += '<div class="gaugeWrapper">';
	htmlString += '<div id="' + gaugeId + '"></div>';
	htmlString += '</div>';
	
	$('#' + idToDrawChart).html(htmlString);
	
	var gaugeChart = new google.visualization.Gauge(document.getElementById(gaugeId));
	
	var options = {
			  'width':230,
			  'height':230,
			  'redFrom':0,
			  'redTo':25,
			  'yellowFrom':25,
			  'yellowTo':75,
			  'greenFrom':75,
			  'greenTo':100,
			  'minorTicks':5
			};
	
	if (invertColors) {
		options = {
				  'width':230,
				  'height':230,
				  'redFrom':75,
				  'redTo':100,
				  'yellowFrom':25,
				  'yellowTo':75,
				  'greenFrom':0,
				  'greenTo':25,
				  'minorTicks':5
				};
	}
	
	gaugeChart.draw(data, options);
	
	//To set the font size smaller
	$(idToDrawChart).find('iframe').each(function() {
	    $('#chartArea svg text:first', this.contentWindow.document||this.contentDocument).attr('font-size', 8);
	});
	
	if (selectHandler != null) {
		$("." + gaugeLinkClass).on('click', function() {
			selectHandler();
		});
	}
}

function drawPieChartToDiv(data, idToDrawChart, selectHandler) {
	var chart = new google.visualization.PieChart(document.getElementById(idToDrawChart));
	
	var options = {
			  'legend':{ position: 'bottom' },
			  'legend.alignment':'center',
			  'is3D':true,
			  'width':250,
			  'height':250,
			  'chartArea':{'width':'80%', 'height':'80%'},
			  'sliceVisibilityThreshold':0
			};
	
	if (selectHandler != null)
		google.visualization.events.addListener(chart, 'select', selectHandler);
	
	chart.draw(data, options);
}