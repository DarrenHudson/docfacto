/*
 * @author dhudson -
 * Created 10 Apr 2012 : 09:38:42
 */

/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 1997-2011 Oracle and/or its affiliates. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License.  You can
 * obtain a copy of the License at
 * https://glassfish.dev.java.net/public/CDDL+GPL_1_1.html
 * or packager/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at packager/legal/LICENSE.txt.
 *
 * GPL Classpath Exception:
 * Oracle designates this particular file as subject to the "Classpath"
 * exception as provided by Oracle in the GPL Version 2 section of the License
 * file that accompanied this code.
 *
 * Modifications:
 * If applicable, add the following below the License Header, with the fields
 * enclosed by brackets [] replaced by your own identifying information:
 * "Portions Copyright [year] [name of copyright owner]"
 *
 * Contributor(s):
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */
package com.docfacto.test;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.TreeModel;

import org.xml.sax.SAXException;

import com.sun.xml.xsom.XSSchemaSet;
import com.sun.xml.xsom.impl.util.SchemaTreeTraverser;
import com.sun.xml.xsom.parser.XSOMParser;
import com.sun.xml.xsom.util.DomAnnotationParserFactory;

/**
 * Parses all the schemas specified as the command line arguments, then shows it
 * as JTree (to see if the parsing was done correctly.)
 * 
 * @author Kirill Grouchnikov (kirillcool@yahoo.com)
 */
public class TreeDumper extends JFrame {

    private static final long serialVersionUID = -8668113631908210846L;

    private final static String FILE_NAME =
    //  "/Users/dhudson/development/diffusion/main/server/src/java/com/pushtechnology/diffusion/xmlproperties/resources/Mime.xsd";
    "/Users/dhudson/development/projects/XSDTree/sample/Extends.xsd";

    public TreeDumper(String mainSchemName,JTree tree) {
        super("Tree for schema '" + mainSchemName + "'");
        this.getRootPane().setLayout(new BorderLayout());
        final JScrollPane scrollPane = new JScrollPane(tree);
        this.getRootPane().add(scrollPane, BorderLayout.CENTER);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }


    public static void main(String[] args) throws Exception {
        // if (args.length != 1) {
        // System.out.println(
        // "Please provide a single (root) schema location");
        // System.exit(0);
        // }

        final XSOMParser reader = new XSOMParser();
        // set an error handler so that you can receive error messages
        // reader.setErrorHandler(new ErrorReporter(System.out));
        // DomAnnotationParserFactory is a convenient default to use
        reader.setAnnotationParser(new DomAnnotationParserFactory());

        try {
            reader.parse(new File(FILE_NAME));

            final XSSchemaSet xss = reader.getResult();
            if (xss==null) {
                System.out.println("error");
            }
            else {
                final SchemaTreeTraverser stt = new SchemaTreeTraverser();
                stt.visit(xss);
                final TreeModel model = stt.getModel();

                final JTree tree = new JTree(model);
                tree.setCellRenderer(new SchemaTreeTraverser.SchemaTreeCellRenderer());
                final TreeDumper dumper = new TreeDumper(FILE_NAME,tree);
                final Dimension screenDim =
                Toolkit.getDefaultToolkit().getScreenSize();
                // dumper.setPreferredSize(screenDim);
                dumper.setSize(screenDim);
                dumper.setVisible(true);
            }
        }
        catch (final SAXException e) {
            if (e.getException()!=null) {
                e.getException().printStackTrace();
            }
            else {
                e.printStackTrace();
            }
            throw e;
        }
    }
}
