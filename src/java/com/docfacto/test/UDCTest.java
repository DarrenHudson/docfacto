/*
 * @author dhudson -
 * Created 13 Sep 2012 : 14:57:24
 */

package com.docfacto.test;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Simple test harness to poke the Usage Data Collection Servlet
 *
 * @author dhudson - created 14 Sep 2012
 * @since 2.0
 */
public class UDCTest {

    private static final String USAGE_COLLECTION_URL =
    "http://info.docfacto.co.uk/usageinfo/";

    UDCTest() {

        try {
            final URL url = new URL(USAGE_COLLECTION_URL);
            final URLConnection connection = url.openConnection();
            ((HttpURLConnection)connection).setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);

            // connection.setRequestProperty("Content-Type","text/plain");
            // Build the string to post
            final StringBuilder builder = new StringBuilder(100);
            builder.append("Docfacto Trial");
            builder.append("\t");
            builder.append("30/12/2012");
            builder.append("\t");
            builder.append("Adam");

            final PrintWriter output =
            new PrintWriter(
                new OutputStreamWriter(connection.getOutputStream()));
            output.write(builder.toString());
            output.flush();
            output.close();

            System.out.println("Sent HTTP request ...["+builder.toString()+"]");

            // Need to wait for the result, to make sure that the request is sent
            final DataInputStream inStream = new DataInputStream(connection.getInputStream());
            final BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));

            final String result = reader.readLine();

            System.out.println("WebServer returned ...["+result+"]");

            inStream.close();


        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }


    public static void main(String[] args) {
        new UDCTest();
        System.exit(0);
    }

}
