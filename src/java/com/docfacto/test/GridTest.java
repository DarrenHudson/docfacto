/* 
 * @author dhudson - 
 * Created 19 May 2012 : 12:27:11
 */

package com.docfacto.test;

import com.docfacto.svg.Grid;
import com.docfacto.svg.SVGDocument;

/**
 * Simple grid test case
 * 
 * @author dhudson - created 22 May 2012
 * @since 2.0
 */
public class GridTest {

    /**
     * Constructor.
     */
    GridTest() {

        SVGDocument SVGDoc = new SVGDocument();

        Grid grid = new Grid();

        SVGDoc.addToRootNode(grid);

        try {
            SVGDoc
                .save("/Users/dhudson/development/projects/XSDTree/sample/grid.svg");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new GridTest();
    }
}
