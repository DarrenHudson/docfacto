package com.docfacto.test;

import java.io.File;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.xml.GenericDynamicElement;
import com.docfacto.xml.XMLFastParser;
import com.docfacto.xml.XMLParserHandler;

public class XMLFastParserTest implements XMLParserHandler {

    XMLFastParserTest() {

        XMLFastParser parser;
        try {
            parser =
                new XMLFastParser(
                    IOUtils
                        .readFileAsString(new File(
                            "/Users/dhudson/development/docfacto/main/doc/dita/test/Test.xml")));
            parser.parse(this);
        }
        catch (DocfactoException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    
    @Override
    public void beginDocument() {
        System.out.println("Begin Doc");
    }

    @Override
    public void endDocument() {
        System.out.println("End Doc");
    }

    @Override
    public void processComment(String comment) {
        System.out.println("I have comment... "+comment);
    }

    @Override
    public void processEndTag(String endTag) {
        System.out.println("I have endTag... "+endTag);
    }

    @Override
    public void processStartTag(GenericDynamicElement gdl) {
        System.out.println("I have start tag... "+gdl);
    }

    @Override
    public void processText(String text) {
        System.out.println(" I have text .. "+text);
    }

    @Override
    public void processDocType(String text) {
        System.out.println(" I have doc type .."+text);
    }

    @Override
    public void processProcessingInstruction(String text) {
        System.out.println(" I have PI ..."+text);
    }

    public static void main(String[] args) {
        new XMLFastParserTest();
    }

}
