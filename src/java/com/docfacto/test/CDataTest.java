package com.docfacto.test;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.XMLUtils;

public class CDataTest {

    private static final String CDATA = "<root><element><![CDATA[<br>]]></element></root>";
    
    private static final String CDATA_FAIL = "<root><element><![CDATA[CRS Room Type. <br>When MAX_RM_RAT = 1<br> 1. And HAC CRS_RM_TYP non-spaces:       CRS_RM_TYP requested<br>     2. Or HOTEL SELECT Vendor and NO RATE PLANS specified: Room type with the most economical rate and with rate plan of  GEN, PRO or COR (COR only if HAC COR_RAT_NUM non-spaces).<br> 3. Or HOTEL SELECT Vendor and RAT_PLN specified: Room type with the most economical rate of rate   plans requested in HAC RAT_PLN.<br> 4. Or HOTEL SOURCE Vendor: Booking code of the most economical rate<br>"+
    "<br> WHEN MAX_RM_RAT > 1 <br> 1. AND HOTEL SELECT VENDOR and NO RATE PLANS specified: The first 60 Room types will be returned with the     rate plan of GEN, PRO or COR (COR only if HAC COR_RAT_NUM non-  spaces).<br> 2. OR HOTEL SELECT VENDOR and RATE-PLANS   specified:      The first 60 Room types will be returned with the       rate plans requested in HAC RAT-PLN. <br>   3. OR HOTEL SOURCE VENDOR:<br>  The first 60 Booking codes will be returned ]]></element></root>";
    
    public CDataTest() {
    
        try {
            System.out.println(XMLUtils.prettyFormat(CDATA_FAIL));
        }
        catch (DocfactoException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    public static void main(String[] args) {
        new CDataTest();
    }
}
