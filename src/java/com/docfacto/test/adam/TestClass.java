
package com.docfacto.test.adam;

import java.io.IOException;

import com.docfacto.common.DocfactoException;

public class TestClass implements TestClassInterface {

    /**
     * 
     */
    public int theFoo;
    
    /** the bar */
    public int theBar;
    
    /**
     * Text alignment type.
     * 
     * @author dhudson - created 22 May 2012
     * @since 2.
     */
    public enum Alignment {
        START, MIDDLE, END;
    }

    /**
     * Text style types
     * 
     * @author dhudson - created 22 May 2012
     * @since 2.0
     */
    public enum Style {
        BOLD, ITALIC, UNDER_LINE
    }


    /**
     * <p>Calls foo and does stuff</p>
     * 
     * @param a
     * @param b
     * @param d
     * @return something
     * @throws DocfactoException
     * @since
     */
    public void foo(int a,float b,boolean c) {

    }

    /**
     * Print the Html table tag for the index summary tables. The table tag
     * printed is
     * &lt;TABLE BORDER="1" CELLPADDING="3" CELLSPACING="0" WIDTH="100%">
     */
    public void tableIndexSummary() {
    }

    /**
     * Print the heading in Html &lt;H2> format.
     *
     * @param str The Header string.
     */
    public void printIndexHeading(String str) {
    }
    
    /**
     * Same as {@link #tableIndexSummary()}.
     */
    public void tableIndexDetail() {

    }
    
    @Deprecated
    public void bar(int a) {
        throw new AdamRuntimeException();
    }

    /**
     * Test to see if Adam grumbles with the Javadoc
     *
     * @throws DocfactoException this is my DocfactoException Description 
     * @since 2.0
     */
    public void methodThrowsNoDescription() throws DocfactoException {

    }

    /**
     * @docfacto.example {@link com.docfacto.xsdtree.XSDTreeDumper}
     * @docfacto.system {This is just for internal use and will never see the
     * light of day for the normal user, this has system doc on
     * 
     * Embed some HTML here } {@inheritDoc java.lang.Object#toString()}
     */
    @Override
    public String toString() {
        return "TestClass";
    }

    /** 
     * {@inhertDoc}
     */
    public void inherit() {
        
    }
    
    /**
     * This is my first line
     * <p>
     * This works, but is orrble with HTML in-balance
     * 
     * @param b
     * @return string
     * @throws IOException when can't write
     * @throws APIException
     * @since 2.0
     */
    public void fooBar(int a) throws DocfactoException, IOException {
        // return "Wibble";
    }

    public class innerClass {

        public void innerFoo() {

        }

    }

    @Override
    public void interfaceMethod(int x,int y) {

    }
}
