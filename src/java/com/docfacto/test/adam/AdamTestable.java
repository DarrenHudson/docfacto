/*
 * @author dhudson -
 * Created 26 Jun 2012 : 15:28:19
 */

package com.docfacto.test.adam;

public class AdamTestable {

    public int field1;

    public static int field2;

    public static final int field3 = 1;
    
    
    /**
     * @since 2.4
     */
    public void setX() {
        
    }

    /**
     * @param x
     * @since 2.4
     */
    public void seX(int x) {
        
    }
    
    /**
     * {@inheritDoc}
     */
    public void foo(int y) {
        
    }
}
