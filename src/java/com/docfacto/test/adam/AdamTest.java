/*
 * @author dhudson -
 * Created 3 Apr 2013 : 11:01:48
 */

package com.docfacto.test.adam;

import com.docfacto.adam.AdamInvoker;
import com.docfacto.adam.AdamOutputProcessor;
import com.docfacto.common.DocfactoException;
import com.docfacto.output.generated.Result;

/**
 * @docfacto.adam ignore
 */
public class AdamTest {

    /*
     * "/Users/dhudson/development/docfacto/main/src/java/com/docfacto/test/adam/AdamTestable.java"
     * ,
     * "/Users/dhudson/development/docfacto/main/src/java/com/docfacto/test/adam/TestEnum.java"
     * ,
     * "/Users/dhudson/development/docfacto/main/src/java/com/docfacto/test/adam/TestClass.java"
     * ,
     * "/Users/dhudson/development/docfacto/main/src/java/com/docfacto/test/adam/TestClassInterface.java"
     * );
     */

    public AdamTest() {

        try {
            final AdamInvoker invoker = new AdamInvoker();

            final AdamOutputProcessor output =
                invoker
                    .invoke(
                        "-sourcepath",
                        "/Users/dhudson/development/docfacto/main/src/java",
                        "-config",
                        "/Users/dhudson/development/docfacto/main/etc/Docfacto.xml",
                        "/Users/dhudson/development/docfacto/main/src/java/com/docfacto/test/adam/TestClass.java");

            if (output!=null) {
                for (final Result result:output.getResults()) {
                    System.out.println(result.getRule().getMessage()+" : "+
                        result.getPosition().getLine()+" : "+
                        result.getPosition().getColumn()+" :"+
                        result.getSeverity());
                }
            }
        }
        catch (DocfactoException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new AdamTest();
    }
}
