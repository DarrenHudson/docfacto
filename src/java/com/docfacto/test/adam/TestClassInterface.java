/*
 * @author dhudson -
 * Created 19 Jun 2012 : 15:08:13
 */

package com.docfacto.test.adam;

public interface TestClassInterface {

    public static final int foo = 3;

    /**
     * Set the current dimension for the interface
     *
     * @param x
     * @param y
     * @since 2.0
     */
    public void interfaceMethod(int x, int y);

}
