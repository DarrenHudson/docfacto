/* 
 * @author dhudson - 
 * Created 10 Jun 2012 : 09:58:26
 */

package com.docfacto.test;

import java.lang.reflect.Method;

import com.sun.tools.xjc.Driver;

/**
 * JAXB Launcher so that debug can be enabled.
 * 
 * In order to debug Jaxb plugins, this bad boy needs to be used. Anything to do
 * with JAXB is a pain.. You will need to add something like <code>-d
 * /Users/dhudson/tmp -extension -Xinterface-injector Logs.xsd</code> to the
 * program arguments
 * 
 * @docfacto.system { -Djava.endorsed.dirs=../../../../../../jaxb-ri-20120516/lib }
 * @docfacto.system { /System/Library/Java/JavaVirtualMachines/1.6.0.jdk/Contents/Home/lib/}
 * @author dhudson - created 10 Jun 2012
 * @since 2.0
 */
public class JAXBLauncher {
    
    public static void main(String[] args) {
        Class<Driver> drvClass = Driver.class;
        try {
            Method mainEntryPoint =
                drvClass.getDeclaredMethod(
                    "_main",
                    new Class[] {String[].class}
                    );
            mainEntryPoint.setAccessible(true); // the _main method is private
            mainEntryPoint.invoke(null,new Object[] {args});
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
