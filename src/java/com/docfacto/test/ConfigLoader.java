/*
 * @author dhudson -
 * Created 29 Jan 2013 : 14:56:23
 */

package com.docfacto.test;

import com.docfacto.common.DocfactoException;
import com.docfacto.config.XmlConfig;
import com.docfacto.config.generated.Product;
import com.docfacto.licensor.Products;

/**
 * @docfacto.adam ignore
 */
public class ConfigLoader {


    /**
     * TODO - Method Title
     * <P>
     * TODO - Method Description
     *
     * @param args
     * @since n.n
     */
    public static void main(String[] args) {

        try {

//            AdamOutputProcessor processor = new AdamOutputProcessor();
            
//            final BeermatLoader loader = new BeermatLoader("/Users/dhudson/development/docfacto/main/eclipse/beermat/Beermat UI/src/com/docfacto/beermat/test/resources/Beermat.xml");
//            
//            loader.save();
            final XmlConfig xmlConfig = new XmlConfig("/Users/dhudson/development/docfacto/main/etc/Docfacto.xml");
//            xmlConfig.save(new File("/Users/dhudson/development/docfacto/main/output/Docfacto.xml"));
//
            xmlConfig.getLicenseConfig();
            System.out.println(xmlConfig.getLicenseConfig().getLicensee());
            
            Product product = xmlConfig.getProduct(Products.ADAM);
            
            System.out.println("Product name " +product.getName());
//
//            final Adam adam = xmlConfig.getAdamConfig();
//
//            final Fields fields = new Fields();
//
//            final RequiredTags requiredTags = new RequiredTags();
//
//
//            final RequiredTag newTag = new RequiredTag();
//            newTag.setValue("since");
//            newTag.setSeverity(Severity.ERROR);

        }catch(final DocfactoException ex) {
            ex.printStackTrace();
        }
    }

}
