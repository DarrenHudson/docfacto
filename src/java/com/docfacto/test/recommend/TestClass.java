package com.docfacto.test.recommend;

import java.util.ArrayList;
import java.util.List;

public class TestClass {

    public static int theFoo;
    
    public static final String COLOR_RED = "red";
    
    
    public List<String> getStuff() {
        return new ArrayList<String>(0);
    }
    
    public void setFoo(int x) {
        theFoo = x;
    }
    
    public int getFoo() {
        return theFoo;
    }
    
    public void addBar(int bar) {
        
    }
    
    public void removeBar(int bar) {
        
    }
    
    public boolean checkBar() {
        return true;
    }
    
    public boolean isBar() {
        return true;
    }
    
    public void add(int x) {
        
    }
    
    public void remove(int x) {
        
    }
}
