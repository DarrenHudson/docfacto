package com.docfacto.test.recommend;

import com.docfacto.adam.AdamInvoker;
import com.docfacto.adam.AdamOutputProcessor;
import com.docfacto.common.DocfactoException;
import com.docfacto.output.generated.Result;

public class Launcher {

    public static void main(String[] args) {
        try {
            final AdamInvoker invoker = new AdamInvoker();

            final AdamOutputProcessor output =
                invoker
                    .invoke("-recommend",
                        "-sourcepath",
                        "/Users/dhudson/development/docfacto/main/src/java",
                        "-config",
                        "/Users/dhudson/development/docfacto/main/etc/Docfacto.xml",
                        "/Users/dhudson/development/docfacto/main/src/java/com/docfacto/test/recommend/TestClass.java");

            if (output!=null) {
                output.dumpResults();
            }
        }
        catch (DocfactoException ex) {
            ex.printStackTrace();
        }
    }

}
