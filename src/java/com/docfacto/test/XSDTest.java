package com.docfacto.test;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTree;

import com.docfacto.common.DocfactoException;
import com.docfacto.config.XmlConfig;
import com.docfacto.xsdtree.XSDTreeTraverser;

public class XSDTest extends JFrame {

    private static final long serialVersionUID = 1L;


    public XSDTest() {
        // JFrame stuff
        getRootPane().setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Call the traverser
        final XSDTreeTraverser traverser = new XSDTreeTraverser();

        traverser.setRootElement("adam");
        traverser.setDebug(true);

        // Parse the file we are interested in
        try {
            final XmlConfig xmlConfig = new XmlConfig("/Users/dhudson/development/docfacto/main/etc/Docfacto.xml");
            
            // traverser.visit(xmlConfig.getSchemaSet());
        }
        catch (final DocfactoException e) {
            e.printStackTrace();
            System.exit(1);
        }

        // Create a tree
        final JTree tree = new JTree(traverser.getRootNode());
        final JScrollPane scrollPane = new JScrollPane(tree);

        getRootPane().add(scrollPane,BorderLayout.CENTER);

        pack();

        setVisible(true);
        
    }
    
    
    public static void main(String[] args) {
        new XSDTest();
    }

}
