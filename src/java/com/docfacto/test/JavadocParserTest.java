package com.docfacto.test;

import com.docfacto.doclets.dita.parser.JavaDocParser;
import com.docfacto.exceptions.DocfactoParsingException;

public class JavadocParserTest {

    private static final String TEST1 = "A Docfacto Taglet understands that attributes exist.\n" +
    " {@docfacto.note title=\"Tag Format\" \n" +
    " <p> \n" +
    " attributes must come first in the supplied text, as in \n"+
    " tag title=\"Tag Format\" uri=\"..\" ... </p> } \n" +
    " \n "+
    " <p> \n " +
    " {@docfacto.system At release 2.4, has changed to understand nested inline \n" +
    " attributes } \n"+
    " </p> \n "+
    "\n" + 
    " @author dhudson - created 21 Feb 2013 \n "+
    " @since 2.2 \n ";
    
    JavadocParserTest() {
     
        try {
            JavaDocParser parser = new JavaDocParser(TEST1);
            
        }
        catch (DocfactoParsingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    
    public static void main(String[] args) {
        new JavadocParserTest();
    }

}
