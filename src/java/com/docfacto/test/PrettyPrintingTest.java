package com.docfacto.test;

import java.io.File;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.common.XMLUtils;

public class PrettyPrintingTest {

    public static void main(String[] args) {

        try {
            System.out.println(XMLUtils.prettyFormat(IOUtils
                .readFileAsString(new File(
                    "/Volumes/Striped/Users/dhudson/development/batik/batik-1.7/samples/henryV.svg"))));
        }
        catch (DocfactoException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
