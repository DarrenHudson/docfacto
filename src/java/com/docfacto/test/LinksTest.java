/*
 * @author dhudson -
 * Created 4 Apr 2013 : 13:14:48
 */

package com.docfacto.test;

import java.util.ArrayList;
import java.util.List;

import com.docfacto.common.DocfactoException;
import com.docfacto.links.LinksConfiguration;
import com.docfacto.links.LinksOutputProcessor;
import com.docfacto.links.LinksProcessor;
import com.docfacto.links.ant.Doc;
import com.docfacto.links.ant.DocSet;
import com.docfacto.links.ant.Source;
import com.docfacto.links.ant.SourceSet;

public class LinksTest {

    public LinksTest() {

        final List<String> sourceFiles = new ArrayList<String>();
        sourceFiles.add("/Volumes/Striped/Users/dhudson/development/docfacto/main/src/java/com/docfacto/taglets/html/ExampleTaglet.java");
        sourceFiles.add("/Volumes/Striped/Users/dhudson/development/docfacto/main/src/java/com/docfacto/xsd2svg/XSD2SVG.java");
        sourceFiles.add("/Volumes/Striped/Users/dhudson/development/docfacto/main/src/java/com/docfacto/adam/AdamOutputProcessor.java");
        sourceFiles.add("/Volumes/Striped/Users/dhudson/development/docfacto/main/src/java/com/docfacto/xsd2svg/XSD2SVG.java");
        
        final List<String> docFiles = new ArrayList<String>();
        docFiles.add("/Volumes/Striped/Users/dhudson/development/docfacto/main/doc/dita/test/Test.xml");
        
        LinksConfiguration config = new LinksConfiguration();
        config.setIsVersionMatching(true);
        config.setConfigPath("/Users/dhudson/development/docfacto/main/etc/Docfacto.xml");
        config.setVerbose(true);

        Source source = new Source();
        SourceSet sourceSet = source.createSourceSet();
        sourceSet.setType("java");
        sourceSet.setFiles(sourceFiles);
        // config.setSource(source);
        
        Doc doc = new Doc();
        DocSet docSet = doc.createDocSet();
        docSet.setType("xml");
        docSet.setFiles(docFiles);
        config.setDoc(doc);
        
        try {
            config.validate();
            final LinksProcessor processor = new LinksProcessor(config);
            processor.process();
            
            LinksOutputProcessor output = processor.getOutput();
            
            output.dump();
            
        }catch(final DocfactoException ex) {
            ex.printStackTrace();
        }

    }

    public static void main(String[] args) {
        new LinksTest();
    }

}
