/*
 * @author dhudson -
 * Created 4 Mar 2013 : 14:24:13
 */

package com.docfacto.test;

import java.io.File;

import com.docfacto.common.DocfactoException;
import com.docfacto.javadoc.CommentParser;
import com.docfacto.javadoc.JavaDocUtils;


public class TestCommentLoader {

    TestCommentLoader() {
        final File file = new File("/Users/dhudson/development/docfacto/main/src/java/com/docfacto/taglets/package.html");

        try {
            final CommentParser parser = new CommentParser(1);

            parser.parse(JavaDocUtils.readPackageHtml(file));

            System.out.println(" I have a since tag " +parser.hasTag("since"));

        }catch(final DocfactoException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new TestCommentLoader();
    }

}
