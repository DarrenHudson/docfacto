/*
 * @author dhudson -
 * Created 31 Aug 2012 : 16:56:46
 */

package com.docfacto.test;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;

public class KeyGenerator {

    private static final String MESSAGE = "Hello World, how are you today...";

    public KeyGenerator() {
        KeyPairGenerator kpg;
        try {
            final SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
            kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(2048, random);
            final KeyPair kp = kpg.genKeyPair();
            saveKeyPair(kp);

            //            final PublicKey publicKey = kp.getPublic();
            //            final PrivateKey privateKey = kp.getPrivate();
            //
            //            final byte [] encrypted = rsaEncrypt(MESSAGE.getBytes(),publicKey);
            //
            //            final String encodedData = Utils.base64Encode(encrypted);
            //
            //            System.out.println(new String(rsaDecrypt(Utils.base64Decode(encodedData),privateKey)));

            //            final KeyFactory fact = KeyFactory.getInstance("RSA");
            //            final RSAPublicKeySpec pub = fact.getKeySpec(publicKey,
            //                RSAPublicKeySpec.class);
            //            final RSAPrivateKeySpec priv = fact.getKeySpec(privateKey,
            //                RSAPrivateKeySpec.class);
            //
            //            saveToFile("public.key",pub.getModulus(),
            //                pub.getPublicExponent());
            //            saveToFile("private.key",priv.getModulus(),
            //                priv.getPrivateExponent());

        }
        catch (final Throwable t) {
            // TODO Auto-generated catch block
            t.printStackTrace();
        }
    }

    public void saveKeyPair(KeyPair keyPair) throws IOException {
        final PrivateKey privateKey = keyPair.getPrivate();
        final PublicKey publicKey = keyPair.getPublic();

        // Store Public Key.
        final X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(
            publicKey.getEncoded());
        FileOutputStream fos = new FileOutputStream("rsapublic.key");
        fos.write(x509EncodedKeySpec.getEncoded());
        fos.close();

        // Store Private Key.
        final PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(
            privateKey.getEncoded());
        fos = new FileOutputStream("rsaprivate.key");
        fos.write(pkcs8EncodedKeySpec.getEncoded());
        fos.close();
    }

    public void saveToFile(String fileName,
    BigInteger mod,BigInteger exp) throws IOException {
        final ObjectOutputStream oout = new ObjectOutputStream(
            new BufferedOutputStream(new FileOutputStream(fileName)));
        try {
            oout.writeObject(mod);
            oout.writeObject(exp);
        }
        catch (final Exception e) {
            throw new IOException("Unexpected error",e);
        }
        finally {
            oout.close();
        }
    }

    PublicKey readKeyFromFile(String keyFileName) throws IOException,
    DocfactoException {
        final InputStream in =
        IOUtils.getResourceAsStream(keyFileName);
        final ObjectInputStream oin =
        new ObjectInputStream(new BufferedInputStream(in));
        try {
            final BigInteger m = (BigInteger)oin.readObject();
            final BigInteger e = (BigInteger)oin.readObject();
            final RSAPublicKeySpec keySpec = new RSAPublicKeySpec(m,e);
            final KeyFactory fact = KeyFactory.getInstance("RSA");
            final PublicKey pubKey = fact.generatePublic(keySpec);
            return pubKey;
        }
        catch (final Exception e) {
            throw new RuntimeException("Spurious serialisation error",e);
        }
        finally {
            oin.close();
        }
    }

    public byte[] rsaEncrypt(byte[] data, PublicKey key) throws DocfactoException, IOException {
        try {
            final Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE,key);
            final byte[] cipherData = cipher.doFinal(data);
            return cipherData;
        }
        catch (final Throwable t) {
            t.printStackTrace();
            return null;
        }
    }

    public byte[] rsaDecrypt(byte[] data, PrivateKey key) throws DocfactoException, IOException {
        try {
            final Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE,key);
            final byte[] cipherData = cipher.doFinal(data);
            return cipherData;
        } catch( final Throwable t) {
            t.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) throws Exception {
        new KeyGenerator();
    }
}
