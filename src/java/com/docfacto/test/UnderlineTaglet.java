/*
 * @author dhudson -
 * Created 30 Jul 2012 : 10:18:12
 */

package com.docfacto.test;

import com.sun.javadoc.Tag;
import com.sun.tools.doclets.Taglet;

/**
 * A sample Inline Taglet representing {@underline ...}. This tag can
 * be used in any kind of {@link com.sun.javadoc.Doc}.
 * The text is underlined.  For example,
 * "@underline UNDERLINE ME" would be shown as: <u>UNDERLINE ME</u>.
 * 
 * This is more of a test for the user taglets
 *
 * @author dhudson - created 30 Jul 2012
 * @since 2.0
 */
public class UnderlineTaglet implements Taglet {

    private static final String NAME = "underline";

    /**
     * Return the name of this custom tag.
     */
    @Override
    public String getName() {
        return NAME;
    }

    /**
     * @return true since this tag can be used in a field
     *         doc comment
     */
    @Override
    public boolean inField() {
        return true;
    }

    /**
     * @return true since this tag can be used in a constructor
     *         doc comment
     */
    @Override
    public boolean inConstructor() {
        return true;
    }

    /**
     * @return true since this tag can be used in a method
     *         doc comment
     */
    @Override
    public boolean inMethod() {
        return true;
    }

    /**
     * @return true since this tag can be used in an overview
     *         doc comment
     */
    @Override
    public boolean inOverview() {
        return true;
    }

    /**
     * @return true since this tag can be used in a package
     *         doc comment
     */
    @Override
    public boolean inPackage() {
        return true;
    }

    /**
     * @return true since this
     */
    @Override
    public boolean inType() {
        return true;
    }

    /**
     * Will return true since this is an inline tag.
     * @return true since this is an inline tag.
     */

    @Override
    public boolean isInlineTag() {
        return true;
    }

    /**
     * Given the <code>Tag</code> representation of this custom
     * tag, return its string representation.
     * @param tag he <code>Tag</code> representation of this custom tag.
     */
    @Override
    public String toString(Tag tag) {
        return "<u>" + tag.text() + "</u>";
    }

    /**
     * This method should not be called since arrays of inline tags do not
     * exist.  Method {@link #toString(Tag)} should be used to convert this
     * inline tag to a string.
     * @param tags the array of <code>Tag</code>s representing of this custom tag.
     */
    @Override
    public String toString(Tag[] tags) {
        return null;
    }
}
