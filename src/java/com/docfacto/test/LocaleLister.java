package com.docfacto.test;

import java.util.Locale;

import com.docfacto.common.Utils;

public class LocaleLister {

    public static void main(String[] args) {

        Locale myLocale = Locale.getDefault();
        
        System.out.println("My Locale " + myLocale);
        
        for(String loc : Utils.getLocales()) {
            System.out.println(loc);
        }
    }
    
}
