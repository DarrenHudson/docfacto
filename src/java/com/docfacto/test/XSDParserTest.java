package com.docfacto.test;

import java.io.File;

import javax.xml.namespace.QName;

import com.docfacto.common.DocfactoException;
import com.docfacto.xsd2common.XSDArgumentParser;
import com.docfacto.xsdtree.XSDTreeTraverser;

public class XSDParserTest {

    XSDParserTest() {
        XSDArgumentParser args = new XSDArgumentParser();
        args.setXSD("/Users/dhudson/development/docfacto/main/src/java/com/docfacto/config/resources/Docfacto.xsd");
        args.setRootNode("adam");
        args.setOutputFile("/tmp/a.out");
        
//        try {
//            XSD2DITAProcessor ditaProcessor = new XSD2DITAProcessor(args);
//            ditaProcessor.process();
//
//            XSD2SVGProcessor svgProcessor = new XSD2SVGProcessor(args);
//            svgProcessor.process();
//        }
//        catch (DocfactoException ex) {
//            ex.printStackTrace();
//        }
//
//        XSDTreeDumper dumper = new XSDTreeDumper(args);
        
        XSDTreeTraverser traverser = new XSDTreeTraverser(args);
        try {
            traverser.visit(new File(args.getXSD()));
            for(QName qname : traverser.getElementNames()) {
                System.out.println("{"+qname.getNamespaceURI()+"}"+qname.getLocalPart());
            }
        }
        catch (DocfactoException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new XSDParserTest();
    }
}
