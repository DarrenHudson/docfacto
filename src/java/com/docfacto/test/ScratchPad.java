/*
 * @author dhudson -
 * Created 6 Nov 2012 : 09:55:02
 */

package com.docfacto.test;

import java.io.File;
import java.io.FileInputStream;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.w3c.dom.Document;

import com.docfacto.common.IOUtils;
import com.docfacto.common.NameValuePair;
import com.docfacto.common.StringUtils;
import com.docfacto.common.XMLUtils;
import com.docfacto.xml.GenericDynamicElement;
import com.docfacto.xml.XMLFastParser;
import com.docfacto.xml.XMLParserHandler;

/**
 * Mess about class for testing stuff
 * 
 * @author dhudson - created 6 Nov 2012
 * @since 2.1
 */
public class ScratchPad implements XMLParserHandler {

    private final String PARSER_TEST =
        "<!-- <a><b>--</b></a> --><entry><xref href=\"http://download.oracle.com/javase/6/docs/api/java/io/IOException.html?is-external=true\" format=\"html\" scope=\"external\">IOException</xref></entry>";

    private final String MATCHER_TEST =
        "  } now this becomes interesting } </p> } ";

    private final Pattern ARRAY_PATTERN = Pattern.compile(Pattern.quote("[]"));

    private static final String DOT_SLASH = "."+File.separator;

    int indent = 0;
    boolean throwNewLine = false;

    ScratchPad() throws Exception {

        String testString = "MyClass[]";

        System.out.println(" --> ["+DOT_SLASH+"] "+
            ARRAY_PATTERN.matcher(testString).replaceAll("Array"));

        final StringTokenizer tokenizer = new StringTokenizer(MATCHER_TEST,"{");

        System.out.println(tokenizer.countTokens());

        final String[] tokens = MATCHER_TEST.split("\\}");

        final Pattern pattern = Pattern.compile("\\}");

        final Matcher matcher = pattern.matcher(MATCHER_TEST);
        final boolean found = matcher.matches();

        // while(matcher.find()) {
        // System.out.println(matcher.group(0));
        // }

        for (int i = 0;i<matcher.groupCount();i++) {
            System.out.println(" Group .. ["+i+"] "+matcher.group(i));
        }

        System.out.println(StringUtils.escapeXML("\"<&>\""));

        // final JavaDocParser parser = new JavaDocParser(PARSER_TEST);

        final XMLFastParser fastParser = new XMLFastParser(PARSER_TEST);
        fastParser.parse(this);

        // final Reader reader = new StringReader("<p>This is a comment");
        //
        // final HTMLEditorKit.Parser parser = new ParserDelegator();
        //
        // try {
        // parser.parse(reader,new ParserCallback(),true);
        // reader.close();
        // }
        // catch (final IOException ex) {
        // throw new DocfactoException("Unable to parse comment ",ex);
        // }
    }

    @Override
    public void beginDocument() {
        // Nothing to do
    }

    @Override
    public void endDocument() {
        // Nothing to do
    }

    @Override
    public void processComment(String comment) {
        write("<!--"+comment+"-->");
    }

    @Override
    public void processEndTag(String endTag) {
        indent -= 2;
        write("</"+endTag+">");
        System.out.println();
    }

    @Override
    public void processStartTag(GenericDynamicElement gdl) {

        if (throwNewLine) {
            System.out.println();
        }

        final StringBuilder builder = new StringBuilder(100);
        builder.append("<");
        builder.append(gdl.getElementName());

        if (gdl.hasAttributes()) {
            builder.append(" ");

            for (final NameValuePair nvp:gdl.getAttributes()) {
                builder.append(nvp.toString());
                builder.append(" ");
            }
        }

        if (gdl.isEmptyElement()) {
            builder.append("/");
        }

        builder.append(">");

        write(builder.toString());

        if (!gdl.isEmptyElement()) {
            indent += 2;
        }
        throwNewLine = true;
    }

    @Override
    public void processText(String text) {
        System.out.print(text);
        throwNewLine = false;
    }

    private void write(String text) {
        for (int i = 0;i<indent;i++) {
            System.out.print(" ");
        }
        System.out.print(text);
    }

    public static void main(String[] args) {

        try {
            Document doc =
                XMLUtils
                    .createDocument(new FileInputStream(
                        new File(
                            "/Users/dhudson/development/docfacto/main/sample/Travel port/Common.xsd")));

            IOUtils
                .writeFile(
                    new File(
                        "/Users/dhudson/development/docfacto/main/sample/Travel port/Common.xsd"),
                    XMLUtils.prettyFormat(doc));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        // try {
        // String contents =
        // IOUtils
        // .readFileAsString(new File(
        // "/Users/dhudson/development/batik/batik-1.7/samples/anne.svg"));
        // // Document doc = XMLUtils.createDocument(contents);
        // System.out.println(contents);
        //
        // System.out.println(XMLUtils.prettyFormat(contents));
        // }
        // catch (DocfactoException ex) {
        // ex.printStackTrace();
        // }
        // try {
        // new ScratchPad();
        // }
        // catch (final Exception ex) {
        // ex.printStackTrace();
        // }
    }

    @Override
    public void processDocType(String text) {
    }

    @Override
    public void processProcessingInstruction(String text) {
    }

}
