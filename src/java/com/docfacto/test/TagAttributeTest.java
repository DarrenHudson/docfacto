/*
 * @author dhudson -
 * Created 21 Feb 2013 : 13:09:51
 */

package com.docfacto.test;

import java.util.ArrayList;
import java.util.List;

import com.docfacto.javadoc.TagFragment;
import com.docfacto.javadoc.TagParser;
import com.docfacto.javadoc.TagTextFragment;


/**
 * Quick test shell to make sure that the tag parsing is OK
 *
 * @author dhudson - created 21 Feb 2013
 * @since 2.2
 */
public class TagAttributeTest {

    private static final String TEST1 = "title=\"fred\" uri='com.docfacto.xsdtree.XSDTreeDumper'    ";
    private static final String TEST2 = "This has no attributes";
    private static final String TEST3 = "title='This is my title' <p> and here is foo=bar some html</p>";
    private static final String TEST4 = "this is a note <div class=\"docfacto-taglet docfacto-taglet-block docfacto-taglet-system\"><em class=\"docfacto-icon icon-list-alt\"></em> <i>hello world</i> </div>";
    private static final String TEST5 = "this is a nested {@docfacto.note title=\"nested\" inner {@docfacto.system title=\"system\" {@docfacto.todo fix me} system stuff } note stuff } ";
    
    public TagAttributeTest() {

        String text = TEST5;
        
        List<TagFragment> frags = getInlineTags(TEST5);
        
        int x = 0;

    }
    
    public static void main(String[] args) {
        new TagAttributeTest();
    }

    /**
     * Return array of tags with text and inline See Tags for a Doc comment.
     */
    static List<TagFragment> getInlineTags(String inlinetext) {
        List<TagFragment> taglist = new ArrayList<TagFragment>();
        
        int delimend = 0, textstart = 0, len = inlinetext.length();

        if (len == 0) {
            return taglist;
        }
        
        while (true) {
            int linkstart;
            if ((linkstart = inlineTagFound(inlinetext,
                                            textstart)) == -1) {
                taglist.add(new TagTextFragment(inlinetext.substring(textstart)));
                break;
            } else {
                int seetextstart = linkstart;
                for (int i = linkstart; i < inlinetext.length(); i++) {
                    char c = inlinetext.charAt(i);
                    if (Character.isWhitespace(c) ||
                        c == '}') {
                        seetextstart = i;
                        break;
                     }
                }
                String linkName = inlinetext.substring(linkstart+2, seetextstart);
                //Move past the white space after the inline tag name.
                while (Character.isWhitespace(inlinetext.
                                                  charAt(seetextstart))) {
                    if (inlinetext.length() <= seetextstart) {
                        taglist.add(new TagTextFragment(inlinetext.substring(textstart, seetextstart)));
//                        docenv.warning(holder,
//                                       "tag.Improper_Use_Of_Link_Tag",
//                                       inlinetext);
                        return taglist;
                    } else {
                        seetextstart++;
                    }
                }
                
                taglist.add(new TagTextFragment(inlinetext.substring(textstart, linkstart)));
                textstart = seetextstart;   // this text is actually seetag
                if ((delimend = findInlineTagDelim(inlinetext, textstart)) == -1) {
                    //Missing closing '}' character.
                    // store the text as it is with the {@link.
                    taglist.add(new TagTextFragment(inlinetext.substring(textstart)));
//                    docenv.warning(holder,
//                                   "tag.End_delimiter_missing_for_possible_SeeTag",
//                                   inlinetext);
                    return taglist;
                } else {
                    TagParser parser = new TagParser(inlinetext.substring(textstart, delimend));
                    parser.setInLine();
                    parser.setTagName(linkName);
                    
                    taglist.add(parser);
                    
                    textstart = delimend + 1;
                }
            }
            if (textstart == inlinetext.length()) {
                break;
            }
        }
        return taglist;
    }

    /**
     * Recursively find the index of the closing '}' character for an inline tag
     * and return it.  If it can't be found, return -1.
     * @param inlineText the text to search in.
     * @param searchStart the index of the place to start searching at.
     * @return the index of the closing '}' character for an inline tag.
     * If it can't be found, return -1.
     */
    private static int findInlineTagDelim(String inlineText, int searchStart) {
        int delimEnd, nestedOpenBrace;
        if ((delimEnd = inlineText.indexOf("}", searchStart)) == -1) {
            return -1;
        } else if (((nestedOpenBrace = inlineText.indexOf("{", searchStart)) != -1) &&
            nestedOpenBrace < delimEnd){
            //Found a nested open brace.
            int nestedCloseBrace = findInlineTagDelim(inlineText, nestedOpenBrace + 1);
            return (nestedCloseBrace != -1) ?
                findInlineTagDelim(inlineText, nestedCloseBrace + 1) :
                -1;
        } else {
            return delimEnd;
        }
    }

    /**
     * Recursively search for the string "{@" followed by
     * name of inline tag and white space,
     * if found
     *    return the index of the text following the white space.
     * else
     *    return -1.
     */
    private static int inlineTagFound(String inlinetext, int start) {
        int linkstart;
        if (start == inlinetext.length() ||
              (linkstart = inlinetext.indexOf("{@", start)) == -1) {
            return -1;
        } else if(inlinetext.indexOf('}', start) == -1) {
            //Missing '}'.
            // docenv.warning(holder, "tag.Improper_Use_Of_Link_Tag",
            // inlinetext.substring(linkstart, inlinetext.length()));
            return -1;
        } else {
            return linkstart;
        }
    }

}
