/*
 * @author dhudson -
 * Created 27 Jul 2012 : 02:35:32
 */

package com.docfacto.test;

import java.util.Iterator;

import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;


// http://www.oracle.com/technetwork/java/javase/documentation/index-137868.html

public class ExampleTest {

    public ExampleTest() {

        //      //Get an instance of java compiler
        final JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        //
        //      //Get a new instance of the standard file manager implementation
        final StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);



        //
        //      // Get the list of java file objects, in this case we have only
        //      // one file, TestClass.java
        final Iterable<? extends JavaFileObject> compilationUnits =
        fileManager.getJavaFileObjects("TestClass.java");

        final Iterator<? extends JavaFileObject> itr = compilationUnits.iterator();

        while(itr.hasNext()) {
            final JavaFileObject stuff = itr.next();
            final int x=0;
        }
        // final JavadocTool tool  = JavadocTool.make0(new Context());
    }

    /**
     * TODO - Method Title
     * <P>
     * TODO - Method Description
     *
     * @param args
     * @since n.n
     */
    public static void main(String[] args) {
        new ExampleTest();
    }

}
