/*
 * @author dhudson -
 * Created 25 Jun 2012 : 09:49:56
 */

package com.docfacto.test;

import com.sun.tools.javah.Main;
/**
 * Programmatically launch the JavaDoc process
 * <P>
 * Launch the JavaDoc process so we can debug it
 * 
 * @author dhudson - created 25 Jun 2012
 * @since 2.0
 */
public class JavaDocLauncher {

    /**
     * Launcher entry point
     * 
     * @param args
     * @since 2.0
     */
    public static void main(String[] args) {
//        final String[] javadocargs = new String[args.length+2];
//        int i = 0;
//
//        for (;i<args.length;i++) {
//            javadocargs[i] = args[i];
//        }
//
//        javadocargs[i] = "-private";
//        i++;
//        javadocargs[i] = "-Xclasses";
//
//        com.sun.tools.javadoc.Main.execute("javadoc",
//            "com.sun.tools.javah.MainDoclet",javadocargs);

        com.sun.tools.javadoc.Main.execute(args);
    }

    //-doclet com.sun.tools.doclets.standard.Standard
}
