/*
 * @author dhudson -
 * Created 8 Mar 2013 : 11:21:47
 */

package com.docfacto.xsd2common;

import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.docfacto.common.DocfactoException;
import com.sun.tools.xjc.model.CCustomizations;
import com.sun.tools.xjc.model.CPluginCustomization;

/**
 * XSD2SVG, XSD2DITA, XSD2JSON share the same argument set, so a common parser
 * was required cross several entry methods, I.E JAXB, ANT, Command line.
 * 
 * This is also used from ANT tasks, hence the set methods as well
 * 
 * @author dhudson - created 8 Mar 2013
 * @since 2.2
 */
public class XSDArgumentParser {

    /**
     * Root Node Option {@value}
     */
    public static final String ROOT_NODE_OPTION = "-rootnode";
    private String theRootNode;

    /**
     * Title option {@value}
     */
    public static final String TITLE_OPTION = "-title";
    private String theTitle;

    /**
     * XSD Option {@value}
     */
    public static final String XSD_OPTION = "-xsd";
    private String theXSD;

    /**
     * Namespace option to be used with <code>ROOT_NODE_OPTION</code>
     */
    public static final String NAME_SPACE_OPTION = "-namespace";
    private String theNamespace;

    /**
     * Output file option {@value}
     */
    public static final String OUTPUT_FILE_OPTION = "-output";
    private String theOutputFile;

    /**
     * Settings is the tag used in JAXB
     */
    private static final String SETTINGS_TAG = "settings";

    private static final String OUTPUT_TAG = "output";

    private static final String TITLE_TAG = "title";

    private static final String ROOT_NODE_TAG = "rootnode";

    private static final String NAMESPACE_TAG = "namespace";

    private String[] theArgs;

    @SuppressWarnings("serial")
    private static final Set<String> ALLOWED_OPTIONS =
        new HashSet<String>(5) {
            {
                add(ROOT_NODE_OPTION);
                add(TITLE_OPTION);
                add(XSD_OPTION);
                add(NAME_SPACE_OPTION);
                add(OUTPUT_FILE_OPTION);
            }
        };

    @SuppressWarnings("serial")
    private static final Set<String> ALLOWED_TAG_SET =
        new HashSet<String>(5) {
            {
                add(SETTINGS_TAG);
                add(OUTPUT_TAG);
                add(TITLE_TAG);
                add(ROOT_NODE_TAG);
                add(NAMESPACE_TAG);
            }
        };

    /**
     * Create a new instance of <code>XSDArgumentParser</code>.
     * 
     * @since 2.2
     */
    public XSDArgumentParser() {
    }

    /**
     * Create a new instance of <code>XSDArgumentParser</code>.
     * 
     * @param args from main
     * @since 2.2
     */
    public XSDArgumentParser(String[] args) {
        parse(args);
        theArgs = args;
    }

    /**
     * Parse the args and set the correct value
     * 
     * @since 2.2
     */
    private void parse(String[] args) {
        final int noOfArgs = args.length;

        for (int i = 0;i<noOfArgs;i += 2) {
            if (ROOT_NODE_OPTION.equals(args[i])) {
                theRootNode = args[i+1];
            }

            if (TITLE_OPTION.equals(args[i])) {
                theTitle = args[i+1];
            }

            if (XSD_OPTION.equals(args[i])) {
                theXSD = args[i+1];
            }

            if (OUTPUT_FILE_OPTION.equals(args[i])) {
                theOutputFile = args[i+1];
            }

            if (NAME_SPACE_OPTION.equals(args[i])) {
                theNamespace = args[i+1];
            }
        }
    }

    /**
     * parse the settings from the JAXB processor
     * 
     * @param settings from the customisations of the XSD
     * @since 2.2
     */
    public void parse(CCustomizations settings) {
        if (settings==null) {
            return;
        }

        final Iterator<CPluginCustomization> it = settings.iterator();

        // Should only be one of these
        while (it.hasNext()) {
            final CPluginCustomization cust = it.next();
            final Element custElement = cust.element;

            final NodeList nodeList = custElement.getChildNodes();

            XSDArgumentParser parser = new XSDArgumentParser();

            Node node;
            Element element;
            for (int i = 0;i<nodeList.getLength();i++) {

                node = nodeList.item(i);
                if (node.getNodeType()==1) {
                    element = (Element)node;

                    if (element.getLocalName().equals(OUTPUT_TAG)) {
                        cust.markAsAcknowledged();
                        parser.setOutputFile(element.getTextContent());
                    }

                    if (element.getLocalName().equals(TITLE_TAG)) {
                        cust.markAsAcknowledged();
                        parser.setTitle(element.getTextContent());
                    }

                    if (element.getLocalName().equals(ROOT_NODE_TAG)) {
                        cust.markAsAcknowledged();
                        parser.setRootNode(element.getTextContent());
                    }

                    if (element.getLocalName().equals(NAMESPACE_TAG)) {
                        cust.markAsAcknowledged();
                        parser.setNamespace(element.getTextContent());
                    }
                }
            }
        }
    }

    /**
     * Return true if its a know tag
     * 
     * @param localName of the tag
     * @return true if the tag is allowed
     * @since 2.2
     */
    public boolean isTagAllowed(String localName) {
        return ALLOWED_TAG_SET.contains(localName);
    }

    /**
     * Check to see if a given option is valid.
     * 
     * @param option to check
     * @return true if the option is valid
     * @since 2.5
     */
    public boolean isOptionAllowed(String option) {
        return ALLOWED_OPTIONS.contains(option);
    }

    /**
     * The parameter supplied with the root node option
     * 
     * @return the root node or null
     * @since 2.2
     */
    public String getRootNode() {
        return theRootNode;
    }

    /**
     * Sets the root node.
     * 
     * {@docfacto.note if not using default null namespace, then a namespace
     * must be specified }
     * 
     * @param rootNode localpart name
     * @since 2.2
     */
    public void setRootNode(String rootNode) {
        theRootNode = rootNode;
    }

    /**
     * Specify the namespace
     * 
     * @param namespace to set
     * @since 2.5
     */
    public void setNamespace(String namespace) {
        theNamespace = namespace;
    }

    /**
     * Return the namespace if specified
     * 
     * @return the namespace if set
     * @since 2.5
     */
    public String getNamespace() {
        return theNamespace;
    }

    /**
     * Check to see if a name space has been set
     * 
     * @return true if the a namespace has been set
     * @since 2.5
     */
    public boolean hasNamespace() {
        return (theRootNode!=null);
    }

    /**
     * Check to see if a root node was supplied
     * 
     * @return true if the root node was supplied
     * @since 2.2
     */
    public boolean hasRootNode() {
        return (theRootNode!=null);
    }

    /**
     * Set the title
     * 
     * @param title to set
     * @since 2.2
     */
    public void setTitle(String title) {
        theTitle = title;
    }

    /**
     * The parameter passed with title option
     * 
     * @return the title, or null
     * @since 2.2
     */
    public String getTitle() {
        return theTitle;
    }

    /**
     * Check to see if title was supplied
     * 
     * @return true if a title was supplied
     * @since 2.2
     */
    public boolean hasTitle() {
        return (theTitle!=null);
    }

    /**
     * The parameter passed with xsd option option
     * 
     * @return the xsd to process
     * @since 2.2
     */
    public String getXSD() {
        return theXSD;
    }

    /**
     * Set the path / filename for the XSD to process
     * 
     * @param fileName
     * @since 2.2
     */
    public void setXSD(String fileName) {
        theXSD = fileName;
    }

    /**
     * The parameter passed with output file option
     * 
     * @return the output file
     * @since 2.2
     */
    public String getOutputFile() {
        return theOutputFile;
    }

    /**
     * Set the path / filename of the output file
     * 
     * @param fileName to use
     * @since 2.2
     */
    public void setOutputFile(String fileName) {
        theOutputFile = fileName;
    }

    /**
     * Check the arguments and validate them
     * 
     * @throws DocfactoException if the arguments are not present or incorrect
     * @since 2.2
     */
    public void validate() throws DocfactoException {
        if (theXSD!=null) {
            File xsdFile = new File(theXSD);
            if (!xsdFile.exists()) {
                throw new DocfactoException("Specified xsd file does not exist");
            }
        }

        if (theArgs!=null) {
            final int noOfArgs = theArgs.length;

            for (int i = 0;i<noOfArgs;i += 2) {
                if (!isOptionAllowed(theArgs[i])) {
                    throw new DocfactoException("Invalid argument ["+
                        theArgs[i]+"]");
                }
            }
        }

        if (theOutputFile==null) {
            throw new DocfactoException("No output file specified");
        }
    }

}
