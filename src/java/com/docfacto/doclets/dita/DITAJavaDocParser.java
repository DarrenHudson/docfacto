/*
 * @author dhudson -
 * Created 8 Nov 2012 : 15:15:09
 */

package com.docfacto.doclets.dita;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.StringUtils;
import com.docfacto.dita.B;
import com.docfacto.dita.DITAElement;
import com.docfacto.dita.P;
import com.docfacto.dita.TBody;
import com.docfacto.dita.TGroup;
import com.docfacto.dita.Table;
import com.docfacto.doclets.dita.parser.JavaDocParser;
import com.docfacto.taglets.dita.DITATaglet;
import com.docfacto.xml.XMLElement;
import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;
import com.sun.tools.doclets.internal.toolkit.taglets.Taglet;

/**
 * Although JavaDoc is structured, it seems that JavaDoc the book allows for
 * incorrect html.. if you look at .. <a href=
 * "http://www.oracle.com/technetwork/java/javase/documentation/index-137868.htm
 * l > t h i s < / a > this encourages use of &lt;p&gt; without a closing tag.
 * It also likes the use of {@literal <br>} tags as well.
 * 
 * XML can't handle any of this, so lets use the Docfacto Fast Parser
 * {@link com.docfacto.xml.XMLFastParser} to break things up for us.
 * 
 * @author dhudson - created 8 Nov 2012
 * @since 2.1
 */
public class DITAJavaDocParser {

    private final Doc theDoc;
    private P theFirstSentence;
    private final DITADocletWriter theWriter;

    /**
     * Create a new instance of <code>DITAJavaDocParser</code>.
     * 
     * @param doc
     * @param writer
     */
    public DITAJavaDocParser(Doc doc,DITADocletWriter writer) {
        theDoc = doc;
        theWriter = writer;
    }

    /**
     * Return the first sentence with all inline tags replaced or if deprecated, a deprecated sentence.
     * 
     * @return A DITA P element
     * @throws DocfactoException
     * @since 2.1
     */
    public P getFirstSentence() throws DocfactoException {
        // Only parse it once
        if (theFirstSentence==null) {

            DITATagletOutput output = null;
            theFirstSentence = new P();

            if(DITADocletUtils.isDeprecated(theDoc)) {
                // Lets just spit out that it is deprecated
                final Tag[] deprs = theDoc.tags("deprecated");

                theFirstSentence.addElement(new B("Deprecated. "));
                if(deprs.length>0) {
                    output =
                    theWriter.commentTagsToDITA(null,theDoc,
                        deprs[0].inlineTags(),true);
                }
            } else {
                // Lets get the first sentence..
                output =
                theWriter.commentTagsToDITA(null,theDoc,
                    theDoc.firstSentenceTags(),true);
            }

            // We pump this out as a string as the comment text may have some
            // tags in it.
            final String first = output.toString();

            // Its not simple text, there is other elements here
            if (first.indexOf("<")>0) {
                final JavaDocParser parser =
                new JavaDocParser(first);

                final P p = (P) parser.getChildElements().get(0);
                theFirstSentence.addElements(p.getElements());
            }
            else {
                theFirstSentence.addTextElement(StringUtils.escapeXML(first));
            }
        }

        return theFirstSentence;
    }

    /**
     * Parse the JavaDoc comments, replace all of the inline tags
     * 
     * @param section to populate
     * @since 2.1
     */
    public void addComments(DITAElement section) throws DocfactoException {

        final DITATagletOutput output =
        theWriter.commentTagsToDITA(null,theDoc,
            theDoc.inlineTags(),
            false);

        // If we are deprecated, lets tell every one
        if(DITADocletUtils.isDeprecated(theDoc)) {
            section.addElement(getFirstSentence());
        }

        final JavaDocParser parser = new JavaDocParser(output.toString());

        for (final XMLElement el:parser.getChildElements()) {
            section.addElement(el);
        }
    }

    /**
     * Add all of the block tags and populate the given section
     * 
     * @param section to populate
     * @since 2.1
     */
    public void addBlockTags(DITAElement section) throws DocfactoException {
        boolean hasTags = false;

        // Return all of the possible tags for this Document, this also produces
        // the order of the taglets
        final Taglet[] taglets =
        DITADocletController.INSTANCE.getTagletManager().getCustomTags(
            theDoc);

        final DITATagletOutput output = new DITATagletOutput();

        for (final Taglet taglet:taglets) {

            try {
                if (taglet instanceof DITATaglet) {
                    final DITATaglet ditaTaglet = (DITATaglet)taglet;
                    final Tag[] tags = theDoc.tags(taglet.getName());

                    final DITATagletOutput tagletOutput;

                    if (tags.length==0) {
                        // Nothing to do here..
                        continue;
                    }

                    if (tags.length==1) {
                        // This is single mode
                        tagletOutput =
                        ditaTaglet.getDITABlockTagletOutput(theDoc,tags[0],
                            theWriter);
                    }
                    else {
                        // This is batch mode
                        tagletOutput =
                        ditaTaglet.getDITABatchTagletOutput(theDoc,
                            theWriter);
                    }

                    if (tagletOutput!=null) {
                        output.appendDITAElements(tagletOutput);
                        hasTags = true;
                    }
                }
            }
            catch (final DocfactoException ex) {
                DITADocletController.INSTANCE.printWarningMessage(
                    theDoc.position(),
                    "Unable to process taglet ["+taglet.getName()+"] "+
                    ex.getMessage());
            }
        }

        if (hasTags) {
            // Create the table body
            final TBody tBody = new TBody();

            // Copy all of the output
            output.populateElement(tBody);

            final JavaDocParser parser = new JavaDocParser(tBody.toXML());

            final Table table = theWriter.buildBlockTagTable();
            final TGroup group = (TGroup)table.getElementAt(0);
            group.addElements(parser.getChildElements());

            // Can't have empty tables in DITA
            section.addElement(table);
        }
    }

}
