/*
 * @author dhudson -
 * Created 7 Nov 2012 : 10:03:33
 */

package com.docfacto.doclets.dita;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import com.docfacto.common.DocfactoException;
import com.docfacto.dita.B;
import com.docfacto.dita.CodePh;
import com.docfacto.dita.DITAElement;
import com.docfacto.dita.I;
import com.docfacto.dita.Lines;
import com.docfacto.dita.P;
import com.docfacto.dita.RefBody;
import com.docfacto.dita.ReferenceTopicDocument;
import com.docfacto.dita.Section;
import com.docfacto.dita.ShortDesc;
import com.docfacto.dita.SimpleTable;
import com.docfacto.dita.SimpleTableEntry;
import com.docfacto.dita.SimpleTableHeader;
import com.docfacto.dita.SimpleTableRow;
import com.docfacto.dita.Title;
import com.docfacto.dita.Xref;
import com.docfacto.dita.Xref.Format;
import com.docfacto.dita.Xref.Scope;
import com.docfacto.xml.XMLTextElement;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.ConstructorDoc;
import com.sun.javadoc.ExecutableMemberDoc;
import com.sun.javadoc.FieldDoc;
import com.sun.javadoc.MethodDoc;
import com.sun.javadoc.Parameter;
import com.sun.javadoc.Type;

/**
 * Creates a DITA file representing a class..
 * 
 * @author dhudson - created 8 Nov 2012
 * @since 2.1
 */
public class DITAClassWriter extends DITADocletWriter {

    private static final String OUTPUTCLASS_ATTRIBUTE = "outputclass";
    
    private static final String ENUM_VALUES_DOC =
    "Returns an array containing the constants of this enum type, in\nthe order they are declared.  "
    +
    "This method may be used to iterate over the constants as follows:\n <pre>\n "
    +
    "for (%s c : %s.values())\n      System.out.println(c);\n</pre>\n"
    +
    "@return an array containing the constants of this enum type, in\n"
    +
    "the order they are declared";

    private static final String ENUM_VALUEOF_DOC =
    "Returns the enum constant of this type with the specified name.\n"
    +
    "The string must match <I>exactly</I> an identifier used to declare an\n"
    +
    "enum constant in this type.  (Extraneous whitespace characters are \n"
    +
    "not permitted.)\n"
    +
    "\n@param name the name of the enum constant to be returned.\n"
    +
    "@return the enum constant with the specified name\n"
    +
    "@throws IllegalArgumentException if this enum type has no constant\n"
    +
    "with the specified name\n"+
    "@throws NullPointerException if the argument is null";

    private ClassDoc theClassDoc;
    private DITAJavaDocParser theParser;

    /**
     * Create a new instance of <code>DITAClassWriter</code>.
     * @param ditaFile
     */
    public DITAClassWriter(File ditaFile) {
        super(ditaFile);
    }

    /**
     * Generate a DITA file for this class
     *
     * @param classDoc to process
     * @throws IOException if unable to process the file
     * @throws DocfactoException if unable to parse the JavaDoc
     * @since 2.1
     */
    public void generateDITA(ClassDoc classDoc)
    throws IOException, DocfactoException {

        DITADocletController.INSTANCE.printNoticeMessage("processing .. "+classDoc.name());

        theClassDoc = classDoc;
        theParser = new DITAJavaDocParser(theClassDoc,this);

        // Create reference topic
        final ReferenceTopicDocument refDoc = new ReferenceTopicDocument();
        refDoc.setReferenceID(classDoc.containingPackage().name()+"."+
        classDoc.name());

        refDoc.addToRootNode(new Title(getDocType(theClassDoc,true)+" "+
        classDoc.name()));
        refDoc.addToRootNode(new ShortDesc(classDoc.containingPackage().name()+
            "."+classDoc.name()));

        final RefBody body = new RefBody();
        refDoc.addToRootNode(body);

        // Class / Interface detail..
        buildSignatureSection(body);
        buildDescriptionSection(body);
        buildTagsSection(body);

        if (!classDoc.isEnum()) {
            buildFieldSummary(body);
        }
        else {
            populateDefaultEnumMethods();
            buildEnumConstantSummary(body);
        }

        if (!classDoc.isInterface()&&!classDoc.isEnum()) {
            buildConstructorSummary(body);
        }

        buildMethodSummary(body);

        if (!classDoc.isEnum()) {
            buildFieldDetail(body);
        }
        else {
            buildEnumConstantDetail(body);
        }

        if (!classDoc.isInterface()&&!classDoc.isEnum()) {
            buildConstructorDetail(body);
        }

        buildMethodDetail(body);

        DITADocletController.INSTANCE.printNoticeMessage("Saving "+classDoc.name());

        refDoc.save(getDITAFile());
    }

    /**
     * Build the class signature section
     *
     * @param element to populate
     * @since 2.1
     */
    private void buildSignatureSection(DITAElement element) {
        final Section section = new Section();
        section.setID("signature");

        final StringBuilder builder = new StringBuilder(100);
        builder.append(theClassDoc.modifiers());
        builder.append(" ");
        builder.append(getDocType(theClassDoc,false));
        builder.append(" ");
        section.addElement(new XMLTextElement(builder.toString()));
        section.addElement(new CodePh(theClassDoc.name()));

        final Type superClass =
        DITADocletUtils.getFirstVisibleSuperClass(theClassDoc);

        if (superClass!=null) {
            section.addElement(new I(" extends "));
            getLink(superClass).populateElement(section);
        }

        final Type[] interfaces = theClassDoc.interfaceTypes();
        if (interfaces!=null&&interfaces.length>0) {

            section.addElement(new I(" implements "));

            for (final Type type:interfaces) {
                getLink(type).populateElement(section);
            }
        }

        element.addElement(section);
    }

    /**
     * Build the description section
     *
     * @param element to populate
     * @throws DocfactoException if unable to parse the JavaDoc
     * @since 2.1
     */
    private void buildDescriptionSection(DITAElement element)
    throws DocfactoException {
        // Class description
        final Section classDescription = new Section();
        classDescription.setID(theClassDoc.name());
        theParser.addComments(classDescription);
        element.addElement(classDescription);
    }

    /**
     * Build the class tags section
     *
     * @param element to populate
     * @since 2.1
     */
    private void buildTagsSection(DITAElement element) throws DocfactoException {
        if (theClassDoc.tags()==null||theClassDoc.tags().length==0) {
            // Nothing to do..
            return;
        }

        final Section section = new Section();
        section.setID("class.tags");

        theParser.addBlockTags(section);

        element.addElement(section);
    }

    /**
     * Build the field summary section
     *
     * @param element to populate
     * @throws DocfactoException if unable to parse the JavaDoc
     * @since 2.1
     */
    private void buildFieldSummary(DITAElement element)
    throws DocfactoException {
        if (theClassDoc.fields()==null||theClassDoc.fields().length==0) {
            // Nothing to do
            return;
        }

        final FieldDoc[] fieldDocs = theClassDoc.fields();
        Arrays.sort(fieldDocs);

        final Section section = new Section();
        section.addAttribute(OUTPUTCLASS_ATTRIBUTE,"fieldSummary");
        section.setID("FieldSummary");

        final SimpleTable simpleTable = new SimpleTable();
        simpleTable.addHeaders("Field Summary");
        SimpleTableRow row;
        SimpleTableEntry entry;

        for (final FieldDoc fieldDoc:fieldDocs) {
            row = new SimpleTableRow();
            entry = new SimpleTableEntry();
            final StringBuilder builder = new StringBuilder(50);
            builder.append(fieldDoc.modifiers());
            builder.append(" ");
            entry.addTextElement(builder.toString());

            getLink(fieldDoc.type()).populateElement(entry);
            row.addElement(entry);

            entry = new SimpleTableEntry();

            final Xref xref = new Xref(fieldDoc.name());
            xref.setScope(Scope.LOCAL);
            xref.setFormat(Format.DITA);
            final StringBuilder link = new StringBuilder(30);
            // Its internal
            link.append("#");

            // Topic ID
            link.append(theClassDoc.containingPackage().name());
            link.append(".");
            link.append(theClassDoc.name());
            link.append("/");
            link.append(DITADocletUtils.getIDFor(fieldDoc));
            xref.setHref(link.toString());

            entry.addElement(xref);

            final DITAJavaDocParser parser =
            new DITAJavaDocParser(fieldDoc,this);
            entry.addElement(parser.getFirstSentence());

            row.addElement(entry);
            simpleTable.addElement(row);
        }

        section.addElement(simpleTable);
        element.addElement(section);
    }

    /**
     * Build Enum constant summary section
     * 
     * @param element to populate
     * @throws DocfactoException if unable to parse the JavaDoc
     * @since 2.1
     */
    private void buildEnumConstantSummary(DITAElement element)
    throws DocfactoException {
        if (theClassDoc.enumConstants()==null||
        theClassDoc.enumConstants().length==0) {
            // Nothing to do
            return;
        }

        final FieldDoc[] fieldDocs = theClassDoc.enumConstants();
        Arrays.sort(fieldDocs);

        final Section section = new Section();
        section.addAttribute(OUTPUTCLASS_ATTRIBUTE,"enumSummary");
        section.setID("EnumConstantSummary");

        final SimpleTable simpleTable = new SimpleTable();
        simpleTable.addHeaders("Enum Constant Summary");
        SimpleTableRow row;
        SimpleTableEntry entry;

        for (final FieldDoc fieldDoc:fieldDocs) {
            row = new SimpleTableRow();
            entry = new SimpleTableEntry();

            final Xref xref = new Xref(fieldDoc.name());
            xref.setScope(Scope.LOCAL);
            xref.setFormat(Format.DITA);
            final StringBuilder link = new StringBuilder(30);
            // Its internal
            link.append("#");

            // Topic ID
            link.append(theClassDoc.containingPackage().name());
            link.append(".");
            link.append(theClassDoc.name());
            link.append("/");
            link.append(DITADocletUtils.getIDFor(fieldDoc));
            xref.setHref(link.toString());

            entry.addElement(xref);

            final DITAJavaDocParser parser =
            new DITAJavaDocParser(fieldDoc,this);
            entry.addElement(parser.getFirstSentence());

            row.addElement(entry);
            simpleTable.addElement(row);
        }

        section.addElement(simpleTable);
        element.addElement(section);
    }

    /**
     * Build constructor summary section
     * 
     * @param element to populate
     * @throws DocfactoException if unable to parse the JavaDoc
     * @since 2.1
     */
    private void buildConstructorSummary(DITAElement element)
    throws DocfactoException {

        if (theClassDoc.constructors()==null||
        theClassDoc.constructors().length==0) {
            // nothing to do
            return;
        }

        final Section section = new Section();
        section.setID("ConstructorSummary");
        section.addAttribute(OUTPUTCLASS_ATTRIBUTE,"constructorSummary");
        
        final SimpleTable simpleTable = new SimpleTable();
        simpleTable.addHeaders("Constructor Summary");
        SimpleTableRow row;
        SimpleTableEntry entry;

        for (final ConstructorDoc doc:theClassDoc.constructors()) {
            row = new SimpleTableRow();
            entry = new SimpleTableEntry();
            buildParams(doc,entry);
            row.addElement(entry);

            entry = new SimpleTableEntry();
            final DITAJavaDocParser parser = new DITAJavaDocParser(doc,this);

            entry.addElement(parser.getFirstSentence());
            row.addElement(entry);

            simpleTable.addElement(row);
        }

        section.addElement(simpleTable);
        element.addElement(section);
    }

    /**
     * Build Method Summary section
     * 
     * @param element to populate
     * @throws DocfactoException if unable to process the JavaDoc
     * @since 2.1
     */
    private void buildMethodSummary(DITAElement element)
    throws DocfactoException {

        if (theClassDoc.methods()==null||theClassDoc.methods().length==0) {
            // Nothing to do
            return;
        }

        // Method Summary
        final Section methodSummary = new Section();
        methodSummary.addAttribute(OUTPUTCLASS_ATTRIBUTE,"methodSummary");
        
        final SimpleTable methodTable = new SimpleTable();
        methodTable.addElement(new SimpleTableHeader("Method Summary"));
        methodSummary.addElement(methodTable);

        final MethodDoc[] methodDocs = theClassDoc.methods();

        Arrays.sort(methodDocs);

        // Process the methods
        for (final MethodDoc methodDoc:methodDocs) {
            final SimpleTableRow row = new SimpleTableRow();

            final SimpleTableEntry entry = new SimpleTableEntry();

            entry.addElement(new XMLTextElement(methodDoc.modifiers()+" "));
            getLink(methodDoc.returnType()).populateElement(entry);
            entry.addElement(new XMLTextElement(" "));
            row.addElement(entry);

            buildParams(methodDoc,entry);
            final DITAJavaDocParser parser =
            new DITAJavaDocParser(methodDoc,this);
            entry.addElement(parser.getFirstSentence());

            methodTable.addElement(row);
        }

        element.addElement(methodSummary);
    }

    /**
     * Build the field detail section
     * 
     * @param element to populate
     * @throws DocfactoException if unable to process the JavaDoc
     * @since 2.1
     */
    private void buildFieldDetail(DITAElement element) throws DocfactoException {
        if (theClassDoc.fields()==null||theClassDoc.fields().length==0) {
            // Nothing to do
            return;
        }

        Section section = new Section();
        section.addElement(new Title("Field Detail"));
        section.addAttribute(OUTPUTCLASS_ATTRIBUTE,"fieldDetail");
        
        element.addElement(section);

        final FieldDoc[] fieldDocs = theClassDoc.fields();

        Arrays.sort(fieldDocs);

        for (final FieldDoc fieldDoc:fieldDocs) {
            section = new Section();
            section.setID(DITADocletUtils.getIDFor(fieldDoc));
            section.addElement(new Title(fieldDoc.name()));
            section.addTextElement(fieldDoc.modifiers()+" "+fieldDoc.name());
            final DITAJavaDocParser parser =
            new DITAJavaDocParser(fieldDoc,this);
            parser.addComments(section);
            parser.addBlockTags(section);

            element.addElement(section);
        }
    }

    /**
     * Build the Enum detail section
     * 
     * @param element to populate
     * @throws DocfactoException if unable to process the JavaDoc
     * @since 2.1
     */
    private void buildEnumConstantDetail(DITAElement element)
    throws DocfactoException {
        if (theClassDoc.fields()==null||theClassDoc.fields().length==0) {
            // Nothing to do
            return;
        }

        Section section = new Section();
        section.addElement(new Title("Enum Constant Detail"));
        section.addAttribute(OUTPUTCLASS_ATTRIBUTE,"enumDetail");
        
        element.addElement(section);

        final FieldDoc[] fieldDocs = theClassDoc.fields();

        Arrays.sort(fieldDocs);

        for (final FieldDoc fieldDoc:fieldDocs) {
            section = new Section();
            section.setID(DITADocletUtils.getIDFor(fieldDoc));

            section.addElement(new Title(fieldDoc.name()));
            section.addTextElement(fieldDoc.modifiers()+" "+fieldDoc.name());
            final DITAJavaDocParser parser =
            new DITAJavaDocParser(fieldDoc,this);
            parser.addComments(section);
            parser.addBlockTags(section);

            element.addElement(section);
        }
    }

    /**
     * Create a table of Constructor details
     * 
     * @param element to append this table to
     * @throws DocfactoException if unable to parse the JavaDoc Comment
     * @since 2.1
     */
    private void buildConstructorDetail(DITAElement element)
    throws DocfactoException {

        if (theClassDoc.constructors()==null||
        theClassDoc.constructors().length==0) {
            // Nothing to do
            return;
        }

        final ConstructorDoc[] constructorDocs = theClassDoc.constructors();

        // Sort them
        Arrays.sort(constructorDocs);

        Section section = new Section();
        section.addElement(new Title("Constructor Detail"));
        section.addAttribute(OUTPUTCLASS_ATTRIBUTE,"constructorDetail");
        
        element.addElement(section);

        for (final ConstructorDoc constructorDoc:constructorDocs) {
            section = new Section();
            section.setID(DITADocletUtils.getIDFor(constructorDoc));

            section.addElement(new Title(constructorDoc.name()));

            final P p = new P();
            buildParams(constructorDoc,p);
            section.addElement(p);

            final DITAJavaDocParser parser =
            new DITAJavaDocParser(constructorDoc,this);

            parser.addComments(section);
            parser.addBlockTags(section);

            element.addElement(section);
        }
    }

    /**
     * Build the method detail section
     * 
     * @param element to populate
     * @throws DocfactoException if unable to parse the JavaDoc
     * @since 2.1
     */
    private void buildMethodDetail(DITAElement element)
    throws DocfactoException {
        if (theClassDoc.methods()==null||theClassDoc.methods().length==0) {
            // Nothing to do
            return;
        }

        final MethodDoc[] methodDocs = theClassDoc.methods();

        Arrays.sort(methodDocs);

        Section section = new Section();
        section.addElement(new Title("Method Detail"));
        section.addAttribute(OUTPUTCLASS_ATTRIBUTE,"methodDetail");
        
        element.addElement(section);

        for (final MethodDoc methodDoc:methodDocs) {
            section = new Section();
            section.setID(DITADocletUtils.getIDFor(methodDoc));

            section.addElement(new Title(methodDoc.name()));

            final Lines lines = new Lines();
            lines.addTextElement(methodDoc.modifiers()+" ");
            getLink(methodDoc.returnType()).populateElement(lines);
            lines.addTextElement(" ");
            buildParams(methodDoc,lines);
            section.addElement(lines);

            populateOverrides(section,methodDoc);


            final DITAJavaDocParser parser =
            new DITAJavaDocParser(methodDoc,this);

            parser.addComments(section);
            parser.addBlockTags(section);

            element.addElement(section);
        }
    }

    /**
     * If the method overrides an existing method, then state it
     *
     * @param section to append to
     * @param methodDoc to process
     * @since 2.1
     */
    private void populateOverrides(Section section, MethodDoc methodDoc) {

        final ClassDoc[] intfacs = methodDoc.containingClass().interfaces();
        final MethodDoc overriddenMethod = methodDoc.overriddenMethod();
        final Type overriddenType = methodDoc.overriddenType();

        // Nothing to doc
        if(overriddenType == null) {
            return;
        }

        if (intfacs.length > 0 || overriddenMethod != null) {
            final ClassDoc holderClassDoc = overriddenType.asClassDoc();
            if (! (holderClassDoc.isPublic() || DITADocletUtils.isLinkable(holderClassDoc))) {
                //This is an implementation detail that should not be documented.
                return;
            }

            if (overriddenType.asClassDoc().isIncluded() && ! methodDoc.isIncluded()) {
                //The class is included but the method is not.  That means that it
                //is not visible so don't document this.
                return;
            }

            final P p = new P();
            final B b = new B();
            p.addElement(b);

            if(overriddenType.asClassDoc().isAbstract() && methodDoc.isAbstract()){
                //Abstract method is implemented from abstract class,
                //not overridden
                b.setValue("Specified by: ");
            } else {
                b.setValue("Overrides: ");
            }

            // Internal / External lookup
            final Xref xref = DITADocletController.INSTANCE.getLink(overriddenMethod,getDITAFile());

            if(xref!= null) {
                xref.setValue(methodDoc.name());
                p.addElement(xref);
            } else {
                p.addTextElement(methodDoc.name());
            }

            if(overriddenMethod.isInterface()) {
                p.addTextElement(" in interface ");
            } else {
                p.addTextElement(" in class ");
            }

            // Get the link and add it to P
            getLink(overriddenType).populateElement(p);

            section.addElement(p);
        }
    }

    /**
     * Populate the params of the Member Doc, building links along the way
     * 
     * @param methodDoc to process
     * @param element to populate
     * @since 2.1
     */
    private void buildParams(ExecutableMemberDoc methodDoc,DITAElement element) {

        final Xref xref = new Xref(methodDoc.name());

        final StringBuilder link = new StringBuilder(30);
        // Its internal
        link.append("#");

        // Topic ID
        link.append(theClassDoc.containingPackage().name());
        link.append(".");
        link.append(theClassDoc.name());
        link.append("/");
        link.append(DITADocletUtils.getIDFor(methodDoc));

        xref.setHref(link.toString());

        element.addElement(xref);

        element.addElement(new XMLTextElement(" ("));

        int paramIndex = 0;
        for (final Parameter param:methodDoc.parameters()) {
            if (paramIndex > 0) {
                element.addTextElement(", ");
            }
            paramIndex++;

            getLink(param.type()).populateElement(element);

            // If its the last parameter and its a var args ...
            if(paramIndex == methodDoc.parameters().length && methodDoc.isVarArgs()) {
                element.addTextElement("...");
            }

            element.addTextElement(" "+param.name());
        }

        element.addTextElement(")");
    }

    /**
     * Populate the default Enum methods, values and valueof
     * 
     * @since 2.1
     */
    private void populateDefaultEnumMethods() {
        final MethodDoc[] methods = theClassDoc.methods();
        for (int j = 0;j<methods.length;j++) {
            final MethodDoc currentMethod = methods[j];
            if (currentMethod.name().equals("values")&&
            currentMethod.parameters().length==0) {
                currentMethod.setRawCommentText(String.format(ENUM_VALUES_DOC,theClassDoc.name(),theClassDoc.name()));
            }
            else if (currentMethod.name().equals("valueOf")&&
            currentMethod.parameters().length==1) {
                final Type paramType = currentMethod.parameters()[0].type();
                if (paramType!=null&&
                paramType.qualifiedTypeName()
                .equals(String.class.getName())) {
                    currentMethod.setRawCommentText(
                        ENUM_VALUEOF_DOC);
                }
            }
        }
    }
}
