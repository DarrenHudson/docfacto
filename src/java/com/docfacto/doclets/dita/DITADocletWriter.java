/*
 * @author dhudson -
 * Created 5 Nov 2012 : 13:56:10
 */

package com.docfacto.doclets.dita;

import java.io.File;
import java.util.Arrays;
import java.util.StringTokenizer;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.StringUtils;
import com.docfacto.dita.CodePh;
import com.docfacto.dita.ColSpec;
import com.docfacto.dita.DITAUtils;
import com.docfacto.dita.RefBody;
import com.docfacto.dita.Section;
import com.docfacto.dita.SimpleTable;
import com.docfacto.dita.SimpleTableEntry;
import com.docfacto.dita.SimpleTableRow;
import com.docfacto.dita.TGroup;
import com.docfacto.dita.Table;
import com.docfacto.dita.TopicRef;
import com.docfacto.dita.TopicRefTypes;
import com.docfacto.dita.Xref;
import com.docfacto.dita.Xref.Format;
import com.docfacto.dita.Xref.Scope;
import com.docfacto.taglets.dita.DITATaglet;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.Doc;
import com.sun.javadoc.ExecutableMemberDoc;
import com.sun.javadoc.MemberDoc;
import com.sun.javadoc.PackageDoc;
import com.sun.javadoc.SeeTag;
import com.sun.javadoc.Tag;
import com.sun.javadoc.Type;
import com.sun.javadoc.WildcardType;
import com.sun.tools.doclets.internal.toolkit.taglets.Taglet;

/**
 * This is passed to the Taglets as the writing engin.
 * 
 * @author dhudson - created 5 Dec 2012
 * @since 2.1
 */
public abstract class DITADocletWriter {

    private final File theDITAFile;

    /**
     * Create a new instance of <code>DITADocletWriter</code>.
     * 
     * @param ditaFile
     */
    public DITADocletWriter(File ditaFile) {
        theDITAFile = ditaFile;
    }

    /**
     * Converts inline tags and text to text strings, expanding the inline tags
     * along the way. Called wherever text can contain an inline tag, such as in
     * comments or in free-form text arguments to non-inline tags.
     * 
     * @param holder specific tag where comment resides
     * @param doc specific doc where comment resides
     * @param tags array of text tags and inline tags (often alternating)
     * present in the text of interest for this doc
     * @param isFirstSentence true if text is first sentence
     */
    public DITATagletOutput commentTagsToDITA(Tag holder,Doc doc,Tag[] tags,
    boolean isFirstSentence) {
        final DITATagletOutput output = new DITATagletOutput();

        // Array of all possible inline tags for this javadoc run
        DITADocletController.INSTANCE.getTagletManager().checkTags(doc,tags,
            true);

        for (int i = 0;i<tags.length;i++) {
            final Tag tagelem = tags[i];
            final String tagName = tagelem.name();
            // if (tagelem instanceof SeeTag) {
            // output.appendDITAElements(seeTagToDITA((SeeTag)tagelem));
            // }
            // else
            if (!tagName.equals("Text")) {
                try {
                    final DITATagletOutput inline =
                        getInlineTagOuput(holder,tagelem);
                    if (inline!=null) {
                        output.appendDITAElements(inline);
                    }
                }
                catch (final DocfactoException ex) {
                    DITADocletController.INSTANCE.printErrorMessage(
                        doc.position(),"Unable to process inline taglet "+
                            tagelem.name()+" for reason ["+ex.getMessage()+"]");
                }
                // result.append(output==null ? "" : output.toString());
                //
                // if (originalLength==0&&isFirstSentence&&
                // tagelem.name().equals("@inheritDoc")&&result.length()>0) {
                // break;
                // }
                // else {
                // continue;
                // }
            }
            else {
                // This is just a regular text tag. The text may contain html
                // links (<a>)
                // or inline tag {@docRoot}, which will be handled as special
                // cases.
                String text =
                    redirectRelativeLinks(tagelem.holder(),tagelem.text());

                // Replace @docRoot only if not represented by an instance of
                // DocRootTaglet,
                // that is, only if it was not present in a source file doc
                // comment.
                // This happens when inserted by the doclet (a few lines
                // above in this method). [It might also happen when passed in
                // on the command
                // line as a text argument to an option (like -header).]
                text = replaceDocRootDir(text);

                if (isFirstSentence) {
                    text = removeNonInlineHtmlTags(text);
                }
                final StringTokenizer lines =
                    new StringTokenizer(text,"\r\n",true);
                final StringBuffer textBuff = new StringBuffer();
                while (lines.hasMoreTokens()) {
                    final StringBuffer line =
                        new StringBuffer(lines.nextToken());
                    // TODO: Speed up..
                    // Util.replaceTabs(theConfig.sourcetab,line);
                    textBuff.append(line.toString());
                }
                // result.append(textBuff);
                // entry.setValue(text);
                output.appendTextElement(text);
            }
        }
        return output;
    }

    private static String redirectRelativeLinks(Doc doc,String text) {
        return text;
    }

    public static String removeNonInlineHtmlTags(String text) {
        if (text.indexOf('<')<0) {
            return text;
        }
        final String noninlinetags[] = {"<ul>","</ul>","<ol>","</ol>",
            "<dl>","</dl>","<table>","</table>",
            "<tr>","</tr>","<td>","</td>",
            "<th>","</th>","<p>","</p>",
            "<li>","</li>","<dd>","</dd>",
            "<dir>","</dir>","<dt>","</dt>",
            "<h1>","</h1>","<h2>","</h2>",
            "<h3>","</h3>","<h4>","</h4>",
            "<h5>","</h5>","<h6>","</h6>",
            "<pre>","</pre>","<menu>","</menu>",
            "<listing>","</listing>","<hr>",
            "<blockquote>","</blockquote>",
            "<center>","</center>",
            "<UL>","</UL>","<OL>","</OL>",
            "<DL>","</DL>","<TABLE>","</TABLE>",
            "<TR>","</TR>","<TD>","</TD>",
            "<TH>","</TH>","<P>","</P>",
            "<LI>","</LI>","<DD>","</DD>",
            "<DIR>","</DIR>","<DT>","</DT>",
            "<H1>","</H1>","<H2>","</H2>",
            "<H3>","</H3>","<H4>","</H4>",
            "<H5>","</H5>","<H6>","</H6>",
            "<PRE>","</PRE>","<MENU>","</MENU>",
            "<LISTING>","</LISTING>","<HR>",
            "<BLOCKQUOTE>","</BLOCKQUOTE>",
            "<CENTER>","</CENTER>"
        };
        for (int i = 0;i<noninlinetags.length;i++) {
            text = replace(text,noninlinetags[i],"");
        }
        return text;
    }

    /**
     * Replace {&#064;docRoot} tag used in options that accept HTML text, such
     * as -header, -footer, -top and -bottom, and when converting a relative
     * HREF where commentTagsToString inserts a {&#064;docRoot} where one was
     * missing. (Also see DocRootTaglet for {&#064;docRoot} tags in doc
     * comments.)
     * <p>
     * Replace {&#064;docRoot} tag in htmlstr with the relative path to the
     * destination directory from the directory where the file is being written,
     * looping to handle all such tags in htmlstr.
     * <p>
     * For example, for "-d docs" and -header containing {&#064;docRoot}, when
     * the HTML page for source file p/C1.java is being generated, the
     * {&#064;docRoot} tag would be inserted into the header as "../", the
     * relative path from docs/p/ to docs/ (the document root).
     * <p>
     * Note: This doc comment was written with '&amp;#064;' representing '@' to
     * prevent the inline tag from being interpreted.
     */
    private String replaceDocRootDir(String htmlstr) {
        // Return if no inline tags exist
        int index = htmlstr.indexOf("{@");
        if (index<0) {
            return htmlstr;
        }
        final String lowerHtml = htmlstr.toLowerCase();
        // Return index of first occurrence of {@docroot}
        // Note: {@docRoot} is not case sensitive when passed in w/command line
        // option
        index = lowerHtml.indexOf("{@docroot}",index);
        if (index<0) {
            return htmlstr;
        }
        final StringBuilder buf = new StringBuilder();
        int previndex = 0;
        while (true) {
            // Search for lowercase version of {@docRoot}
            index = lowerHtml.indexOf("{@docroot}",previndex);
            // If next {@docRoot} tag not found, append rest of htmlstr and exit
            // loop
            if (index<0) {
                buf.append(htmlstr.substring(previndex));
                break;
            }
            // If next {@docroot} tag found, append htmlstr up to start of tag
            buf.append(htmlstr.substring(previndex,index));
            previndex = index+10; // length for {@docroot} string
            // Insert relative path where {@docRoot} was located
            buf.append("relativeNoSlash");
            // Append slash if next character is not a slash

            // TODO:

            // if (relativepathNoSlash.length() > 0 && previndex <
            // htmlstr.length()
            // && htmlstr.charAt(previndex) != '/') {
            // buf.append(DirectoryManager.URL_FILE_SEPERATOR);
            // }
        }
        return buf.toString();
    }

    public static String replace(String text,String tobe,String by) {
        while (true) {
            final int startindex = text.indexOf(tobe);
            if (startindex<0) {
                return text;
            }
            final int endindex = startindex+tobe.length();
            final StringBuffer replaced = new StringBuffer();
            if (startindex>0) {
                replaced.append(text.substring(0,startindex));
            }
            replaced.append(by);
            if (text.length()>endindex) {
                replaced.append(text.substring(endindex));
            }
            text = replaced.toString();
        }
    }

    /**
     * Process the see tag, this is called from the See taglet
     * 
     * @param see to process
     * @return DITA representation of the See Tag
     * @since 2.1
     */
    public DITATagletOutput seeTagToDITA(SeeTag see) {
        final DITATagletOutput output = new DITATagletOutput();

        // Lets see if its an external reference...
        Xref xref =
            DITADocletController.INSTANCE.getConfig().extern
                .getExternalXrefLink(see);

        // If they have provided a label, then use it
        final String label = see.label();

        if (xref!=null) {
            if (!label.isEmpty()) {
                // It is possible that they have changed the label
                xref.setValue(label);
            }
            // All done.
            output.appendDITAElement(xref);
            return output;
        }

        xref = new Xref();

        final String seetext = replaceDocRootDir(see.text());
        // Check if @see is an href or "string"
        if (seetext.startsWith("<")||seetext.startsWith("\"")) {

            if (label.isEmpty()) {
                xref.setValue(seetext);
            }

            xref.setScope(Scope.EXTERNAL);
            xref.setFormat(Format.HTML);

            output.appendDITAElement(xref);
            return output;
        }

        // OK at this point its a internal class or broken link
        final ClassDoc refClass = see.referencedClass();

        // Its internal
        xref.setScope(Scope.LOCAL);
        xref.setFormat(Format.DITA);

        // Set the value of the XREF to the label of the tag, otherwise we will
        // set it to something sensible
        if (!label.isEmpty()) {
            xref.setValue(label);
        }

        if (refClass==null) {
            // @see is not referencing an included class
            final PackageDoc refPackage = see.referencedPackage();

            if (refPackage!=null) {
                // Link to the package file
                final File packageFile =
                    new File(DITADocletUtils.getPackageRoot(refPackage.name()),
                        "r_package.dita");
                xref.setHref(DITAUtils.getRelativePath(getDITAFile()
                    .getParent(),
                    packageFile
                        .getAbsolutePath()));
                if (label.isEmpty()) {
                    xref.setValue(refPackage.name());
                }
                output.appendDITAElement(xref);
            }

            return output;
        }

        if (!refClass.isIncluded()) {
            // I don't know what this is...
            final CodePh codePh = new CodePh();
            if (label.isEmpty()) {
                codePh.setValue(see.text());
            }
            else {
                codePh.setValue(label);
            }
            output.appendDITAElement(codePh);
            return output;
        }

        // Its a member reference
        final String refMemName = see.referencedMemberName();

        if (refMemName==null) {
            // Must be a class reference since refClass is not null and
            if (refClass.isPublic()||refClass.isProtected()) {
                String ditaPath =
                    refClass.containingPackage().name()
                        .replaceAll("[.]+","/");
                ditaPath += "/r_"+refClass.typeName()+".dita";

                final File classFile =
                    new File(
                        DITADocletController.INSTANCE.getDestinationPath(),
                        ditaPath);

                xref.setHref(DITAUtils.getRelativePath(getDITAFile()
                    .getParent(),
                    classFile
                        .getAbsolutePath()));

                if (label.isEmpty()) {
                    xref.setValue(refClass.typeName());
                }

                output.appendDITAElement(xref);
            }
            else {
                final CodePh codePh = new CodePh();
                if (label.isEmpty()) {
                    codePh.setValue(refClass.qualifiedName());
                }
                else {
                    codePh.setValue(label);
                }
                output.appendDITAElement(codePh);
            }
        }
        else {
            final MemberDoc refMem = see.referencedMember();

            if (refMem instanceof ExecutableMemberDoc) {
                if (see.text().trim().startsWith("#")) {
                    xref.setHref("#"+refClass.containingPackage().name()+
                        "."+
                        refClass.name()+"/"+
                        DITADocletUtils.getIDFor((ExecutableMemberDoc)refMem));
                    if (!label.isEmpty()) {
                        xref.setValue(label);
                    }
                    else {
                        xref.setValue(refMemName);
                    }
                }
                else {
                    // Its in another file
                    String ditaPath =
                        refClass.containingPackage().name()
                            .replaceAll("[.]+","/");
                    ditaPath += "/r_"+refClass.typeName()+".dita";

                    final File classFile =
                        new File(
                            DITADocletController.INSTANCE.getDestinationPath(),
                            ditaPath);

                    xref.setHref(DITAUtils.getRelativePath(getDITAFile()
                        .getParent(),
                        classFile
                            .getAbsolutePath())+"#"+
                        refClass.containingPackage().name()+"."+
                        refClass.name()+"/"+
                        DITADocletUtils.getIDFor((ExecutableMemberDoc)refMem));

                    if (label.isEmpty()) {
                        xref.setValue(refMemName);
                    }
                }
                output.appendDITAElement(xref);
            }
        }
        return output;
    }

    /**
     * Given an inline tag, return its output.
     * 
     * @param holderTag The tag this holds this inline tag. Null if there is no
     * tag that holds it.
     * @param inlineTag The inline tag to be documented.
     * @return The output of the inline tag.
     */
    private DITATagletOutput getInlineTagOuput(Tag holderTag,Tag inlineTag)
    throws DocfactoException {
        final DITATagletManager tagletManager =
            DITADocletController.INSTANCE.getTagletManager();
        final Taglet[] definedTags = tagletManager.getInlineCustomTags();

        String inlineTagName = inlineTag.name();
        if (inlineTag.name().startsWith("@")) {
            inlineTagName = inlineTagName.substring(1);
        }

        for (final Taglet taglet:definedTags) {
            // Do the names match
            if (taglet.getName().equals(inlineTagName)) {
                if (taglet instanceof DITATaglet) {
                    tagletManager.seenCustomTag(taglet.getName());

                    final DITATaglet ditaTaglet = (DITATaglet)taglet;
                    // There can only be one ..
                    return ditaTaglet.getDITAInLineTagletOutput(inlineTag,this);
                }
            }
        }

        return null;
    }

    /**
     * DITA pathing is all relative from the documentation root, this method
     * provides a @{TopicRef} element, with the href attribute set correctly.
     * 
     * @return a TopicRef using relative paths
     * @since 2.1
     */
    public TopicRef getTopicRef() {
        final TopicRef topicRef = new TopicRef();
        topicRef.setHref(DITAUtils.getRelativePath(
            DITADocletController.INSTANCE.getConfig().getRootFolder()
                .getAbsolutePath(),theDITAFile
                .getAbsolutePath()));
        topicRef.setType(TopicRefTypes.REFERENCE);

        return topicRef;
    }

    /**
     * Return the current DITA file that we are processing
     * 
     * @return the DITA file that we are currently processing
     * @since 2.1
     */
    public File getDITAFile() {
        return theDITAFile;
    }

    /**
     * Return a link to the class if possible
     * 
     * @param type
     * @return a DITA elements to represent the link, if generics are involved,
     * this can be lots of links, else a code phrased qualified class name
     * @since 2.1
     */
    public DITATagletOutput getLink(Type type) {
        final DITATagletOutput output = new DITATagletOutput();

        if (type==null) {
            return output;
        }

        if (type.isPrimitive()) {
            final CodePh codePh = new CodePh(type.typeName());
            output.appendDITAElement(codePh);
            return output;
        }

        if (type.asWildcardType()!=null) {
            output.appendTextElement("?");
            final WildcardType wildcardType = type.asWildcardType();
            final Type[] extendsBounds = wildcardType.extendsBounds();
            for (int i = 0;i<extendsBounds.length;i++) {
                if (i==0) {
                    output.appendTextElement(" extends ");
                }
                else {
                    output.appendTextElement(", ");
                }

                output.appendDITAElements(getLink(extendsBounds[i]));
            }
            final Type[] superBounds = wildcardType.superBounds();
            for (int i = 0;i<superBounds.length;i++) {
                if (i==0) {
                    output.appendTextElement(", ");
                }
                else {
                    output.appendTextElement(" super ");
                }
                output.appendDITAElements(getLink(superBounds[i]));
            }

            return output;
        }

        final ClassDoc classDoc = type.asClassDoc();

        if (classDoc==null) {
            final CodePh codePh = new CodePh(type.qualifiedTypeName());
            output.appendDITAElement(codePh);
            return output;
        }

        if (classDoc.isIncluded()) {
            String ditaPath =
                classDoc.containingPackage().name().replaceAll("[.]+","/");
            ditaPath += "/r_"+classDoc.typeName()+".dita";

            final File classFile =
                new File(DITADocletController.INSTANCE.getDestinationPath(),
                    ditaPath);

            final Xref xref = new Xref();

            xref.setHref(DITAUtils.getRelativePath(getDITAFile().getParent(),
                classFile
                    .getAbsolutePath()));

            xref.setValue(classDoc.typeName());
            xref.setScope(Scope.LOCAL);
            xref.setFormat(Format.DITA);

            output.appendDITAElement(xref);

            getTypeParameterLinks(type,output);

            return output;
        }
        else {

            final Xref externalLink =
                DITADocletController.INSTANCE.getConfig().extern
                    .getExternalXrefLink(classDoc);

            if (externalLink!=null) {
                // Its an external link..
                output.appendDITAElement(externalLink);
                getTypeParameterLinks(type,output);
                return output;
            }
            else {
                // Don't know what it is, so lets just fully qualify the the
                // name
                final CodePh codePh = new CodePh(type.qualifiedTypeName());
                output.appendDITAElement(codePh);

                getTypeParameterLinks(type,output);
                return output;
            }
        }
    }

    /**
     * Add any generics
     * 
     * @param type to process
     * @param output to append any generics
     * @since 2.1
     */
    public void getTypeParameterLinks(Type type,DITATagletOutput output) {
        Type[] vars = null;

        if (type.asClassDoc() instanceof ExecutableMemberDoc) {
            vars = ((ExecutableMemberDoc)type.asClassDoc()).typeParameters();
        }
        else if (type.asParameterizedType()!=null) {
            vars = type.asParameterizedType().typeArguments();
        }

        if (vars==null||vars.length==0) {
            // Nothing to do
            return;
        }

        output.appendTextElement(" &lt;");

        for (int i = 0;i<vars.length;i++) {
            if (i>0) {
                output.appendTextElement(",");
            }
            output.appendDITAElements(getLink(vars[i]));
        }

        output.appendTextElement("&gt; ");
    }

    /**
     * Return the simple class name (not qualified) but with the generic
     * information As generics use the {@literal < > } then the returning string
     * will be XML escaped
     * 
     * @param doc
     * @return simple class name with generics
     * @since 2.1
     */
    public String getNameWithGenerics(Doc doc) {
        // This includes the generics
        final String qualifiedName = doc.toString();

        final int index = qualifiedName.lastIndexOf('.');
        if (index>0) {
            return StringUtils.escapeXML(qualifiedName.substring(index+1));
        }

        return StringUtils.escapeXML(qualifiedName);
    }

    /**
     * Return a literal string if the doc is an interface, class or enum
     * 
     * @param doc
     * @return a string representation of what type of doc this is
     * @since 2.1
     */
    public String getDocType(Doc doc,boolean capitalise) {
        String typeName = "";
        if (doc.isOrdinaryClass()) {
            typeName = "Class";
        }
        else if (doc.isInterface()) {
            typeName = "Interface";
        }
        else if (doc.isException()) {
            typeName = "Exception";
        }
        else if (doc.isError()) {
            typeName = "Error";
        }
        else if (doc.isAnnotationType()) {
            typeName = "AnnotationType";
        }
        else if (doc.isEnum()) {
            typeName = "Enum";
        }
        if (!capitalise) {
            return typeName.toLowerCase();
        }
        return typeName;
    }

    /**
     * Generate a summary table, using the first sentence of the comment,
     * creating links to the details
     * 
     * @param title of the table
     * @param packageName of the package currently being processed
     * @param docs to process
     * @param body to populate
     * @throws DocfactoException
     * @since 2.1
     */
    public void generateSummaryTable(String title,String packageName,
    Doc[] docs,RefBody body) throws DocfactoException {

        if (docs.length==0) {
            // Nothing to do
            return;
        }

        // Lets sort them in name order
        Arrays.sort(docs);

        final Section section = new Section();

        final SimpleTable table = new SimpleTable();
        table.addHeaders(title);

        SimpleTableRow row;
        SimpleTableEntry entry;
        for (final Doc doc:docs) {
            row = new SimpleTableRow();
            final Xref xref = new Xref(doc.name());

            if (doc instanceof PackageDoc) {
                final File packageFile =
                    new File(DITADocletUtils.createFolders(
                        DITADocletController.INSTANCE.getConfig().destDirName,
                        doc.name()),
                        "r_package.dita");

                xref.setHref(DITAUtils.getRelativePath(
                    DITADocletController.INSTANCE.getConfig().getRootFolder()
                        .getAbsolutePath(),
                    packageFile
                        .getAbsolutePath()));
            }
            else {
                // Include generics
                xref.setValue(getNameWithGenerics(doc));
                xref.setHref("r_"+doc.name()+".dita"+"#"+packageName+"."+
                    doc.name());
            }

            entry = new SimpleTableEntry();
            entry.addElement(xref);
            row.addElement(entry);

            final DITAJavaDocParser parser = new DITAJavaDocParser(doc,this);
            entry = new SimpleTableEntry();
            entry.addElement(parser.getFirstSentence());

            row.addElement(entry);
            table.addElement(row);
        }

        section.addElement(table);
        body.addElement(section);
    }

    /**
     * Produce a complex table with a table spec for blocking tags
     * 
     * @return a DITA table ready for the block tags
     * @since 2.1
     */
    public Table buildBlockTagTable() {
        final Table table = new Table();
        table.addAttribute("frame","none");
        final TGroup tGroup = table.addTableGroup();
        // TechnicalDebt: Add these to TGroup..
        tGroup.addAttribute("cols","3");
        tGroup.addAttribute("colsep","0");
        tGroup.addAttribute("rowsep","0");

        ColSpec colSpec = tGroup.addColSpec("spec0");
        colSpec.setColWidth("1*");

        colSpec = tGroup.addColSpec("spec1");
        colSpec.setColWidth("2*");

        colSpec = tGroup.addColSpec("spec2");
        colSpec.setColWidth("2*");

        return table;
    }

    /**
     * Calculate the relative path between this file and the doc root.
     * 
     * @return the relative path between where this file will be created and the
     * destination root.
     * @since 2.1
     */
    public String getDocRootOutput() {
        return DITAUtils.getRelativePath(getDITAFile().getAbsolutePath(),
            DITADocletController.INSTANCE.getDestinationPath());
    }
}
