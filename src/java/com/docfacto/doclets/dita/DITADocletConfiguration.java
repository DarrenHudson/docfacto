/*
 * @author dhudson -
 * Created 14 Nov 2012 : 11:00:33
 */

package com.docfacto.doclets.dita;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.StringTokenizer;

import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.Doc;
import com.sun.javadoc.DocErrorReporter;
import com.sun.javadoc.PackageDoc;
import com.sun.javadoc.RootDoc;
import com.sun.tools.doclets.internal.toolkit.taglets.TagletManager;
import com.sun.tools.doclets.internal.toolkit.util.DocletConstants;
import com.sun.tools.doclets.internal.toolkit.util.Util;

/**
 * This is the configuration for the JavaDoc to DITA Doclet.
 * 
 * This class contains all of the command line parameters
 * 
 * @author dhudson - created 14 Nov 2012
 * @since 2.1
 */
public class DITADocletConfiguration {

    /**
     * The taglet manager.
     */
    public DITATagletManager theTagletManager;

    /**
     * The path to Taglets
     */
    public String tagletpath = "";

    /**
     * This is true if option "-serialwarn" is used. Default value is false to
     * suppress excessive warnings about serial tag.
     */
    public boolean serialwarn = false;

    /**
     * The specified amount of space between tab stops.
     */
    public int sourcetab = DocletConstants.DEFAULT_TAB_STOP_LENGTH;

    /**
     * True if command line option "-nosince" is used. Default value is false.
     */
    public boolean nosince = false;

    /**
     * True if we should recursively copy the doc-file subdirectories
     */
    public boolean copydocfilesubdirs = false;

    /**
     * The META charset tag used for cross-platform viewing.
     */
    public String charset = "";

    /**
     * The list of doc-file subdirectories to exclude
     */
    protected Set<String> excludedDocFileDirs;

    /**
     * The list of qualifiers to exclude
     */
    protected Set<String> excludedQualifiers;

    /**
     * The Root of the generated Program Structure from the Doclet API.
     */
    public RootDoc theRoot;

    /**
     * Destination directory name, in which doclet will generate the entire
     * documentation. Default is current directory.
     */
    public String destDirName = "";

    /**
     * Destination directory name, in which doclet will copy the doc-files to.
     */
    public String docFileDestDirName = "";

    /**
     * Encoding for this document. Default is default encoding for this
     * platform.
     */
    public String docencoding = null;

    /**
     * True if user wants to suppress descriptions and tags.
     */
    public boolean nocomment = false;

    /**
     * Encoding for this document. Default is default encoding for this
     * platform.
     */
    public String encoding = null;

    /**
     * Generate author specific information for all the classes if @author tag
     * is used in the doc comment and if -author option is used.
     * <code>showauthor</code> is set to true if -author option is used. Default
     * is don't show author information.
     */
    public boolean showauthor = false;

    /**
     * Generate version specific information for the all the classes if @version
     * tag is used in the doc comment and if -version option is used.
     * <code>showversion</code> is set to true if -version option is
     * used.Default is don't show version information.
     */
    public boolean showversion = false;

    /**
     * Sourcepath from where to read the source files. Default is classpath.
     * 
     */
    public String sourcepath = "";

    /**
     * Don't generate deprecated API information at all, if -nodeprecated option
     * is used. <code>nodepracted</code> is set to true if -nodeprecated option
     * is used. Default is generate deprected API information.
     */
    public boolean nodeprecated = false;

    /**
     * The catalog of classes specified on the command-line
     */
    public ClassDocCatalog classDocCatalog;

    /**
     * True if user wants to suppress time stamp in output. Default is false.
     */
    public boolean notimestamp = false;

    /**
     * The package grouping instance.
     */
    public final Group group = new Group();

    /**
     * The tracker of external package links.
     */
    public final Extern extern = new Extern();

    /**
     * This is the Doc Title which will be used as the map title
     */
    private String theDocTitle;

    /**
     * An array of the packages specified on the command-line merged with the
     * array of packages that contain the classes specified on the command-line.
     * The array is sorted.
     */
    public PackageDoc[] packages;

    private File theRootFolder;

    public DITADocletWriter theWriter;

    /**
     * Over-ride the Taglet Manager
     */
    public DITATagletManager tagletManager;

    private String theLink = "http://download.oracle.com/javase/6/docs/api";

    /**
     * Create a new instance of <code>DITADocletConfiguration</code>.
     */
    public DITADocletConfiguration() {
        excludedDocFileDirs = new HashSet<String>();
        excludedQualifiers = new HashSet<String>();
    }

    /**
     * Set the command line options supported by this configuration.
     * 
     * @param options the two dimensional array of options.
     */
    public void setOptions(String[][] options) {
        final LinkedHashSet<String[]> customTagStrs =
            new LinkedHashSet<String[]>();
        for (int oi = 0;oi<options.length;++oi) {
            final String[] os = options[oi];
            final String opt = os[0].toLowerCase();
            if (opt.equals("-d")) {
                destDirName = addTrailingFileSep(os[1]);
                docFileDestDirName = destDirName;
                theRootFolder = new File(destDirName);
            }
            else if (opt.equals("-docfilessubdirs")) {
                copydocfilesubdirs = true;
            }
            else if (opt.equals("-docencoding")) {
                docencoding = os[1];
            }
            else if (opt.equals("-encoding")) {
                encoding = os[1];
            }
            else if (opt.equals("-author")) {
                showauthor = true;
            }
            else if (opt.equals("-version")) {
                showversion = true;
            }
            else if (opt.equals("-nodeprecated")) {
                nodeprecated = true;
            }
            else if (opt.equals("-sourcepath")) {
                sourcepath = os[1];
            }
            else if (opt.equals("-classpath")&&
                sourcepath.length()==0) {
                sourcepath = os[1];
            }
            else if (opt.equals("-doctitle")) {
                theDocTitle = os[1];
            }
            else if (opt.equals("-excludedocfilessubdir")) {
                addToSet(excludedDocFileDirs,os[1]);
            }
            else if (opt.equals("-noqualifier")) {
                addToSet(excludedQualifiers,os[1]);
            }
            else if (opt.equals("-notimestamp")) {
                notimestamp = true;
            }
            else if (opt.equals("-nocomment")) {
                nocomment = true;
            }
            else if (opt.equals("-tag")||opt.equals("-taglet")) {
                customTagStrs.add(os);
            }
            else if (opt.equals("-tagletpath")) {
                tagletpath = os[1];
                // } else if (opt.equals("-keywords")) {
                // keywords = true;
            }
            else if (opt.equals("-serialwarn")) {
                serialwarn = true;
            }
            else if (opt.equals("-group")) {
                group.checkPackageGroups(os[1],os[2]);
            }
            else if (opt.equals("-link")) {
                theLink = os[1];
                final String url = os[1];
                extern.url(url,url,theRoot,false);
            }
            else if (opt.equals("-linkoffline")) {
                final String url = os[1];
                final String pkglisturl = os[2];
                extern.url(url,pkglisturl,theRoot,true);
            }
        }
        if (sourcepath.length()==0) {
            sourcepath = System.getProperty("env.class.path")==null ? "" :
                System.getProperty("env.class.path");
        }
        if (docencoding==null) {
            docencoding = encoding;
        }

        classDocCatalog = new ClassDocCatalog(theRoot.specifiedClasses(),this);

        initTagletManager(customTagStrs);
    }

    /**
     * Return the defined external link
     * 
     * @return the external link
     * @since 2.1
     */
    public String getExternalLink() {
        return theLink;
    }

    /**
     * Returns the "length" of a given option. If an option takes no arguments,
     * its length is one. If it takes one argument, it's length is two, and so
     * on. This method is called by JavaDoc to parse the options it does not
     * recognize. It then calls
     * {@link #validOptions(String[][], DocErrorReporter)} to validate them.
     * <b>Note:</b><br>
     * The options arrive as case-sensitive strings. For options that are not
     * case-sensitive, use toLowerCase() on the option string before comparing
     * it. </blockquote>
     * 
     * @return number of arguments + 1 for a option. Zero return means option
     * not known. Negative value means error occurred.
     */
    public int optionLength(String option) {

        // otherwise look for the options we have added
        option = option.toLowerCase();
        if (option.equals("-help")) {
            // TODO:
            System.out.println("DITADocletHelp ...");
            return 1;
        }
        else if (option.equals("-doctitle")||
            option.equals("-overview")||
            option.equals("-d")||
            option.equals("-link")||
            option.equals("-linkoffline")) {

            return 2;
        }
        else {
            return 0;
        }
    }

    /**
     * Check to see if the options provided to the Doclet are valid
     * 
     * @param options
     * @param reporter
     * @return true if everything is OK, else false which will stop the
     * processing of the Doclet
     * @since 2.1
     */
    public boolean validOptions(String[][] options,DocErrorReporter reporter) {
        boolean overview = false;
        boolean nooverview = false;
        boolean destination = false;
        
        // check shared options
        if (!generalValidOptions(options,reporter)) {
            return false;
        }
        
        // otherwise look at our options
        for (int oi = 0;oi<options.length;++oi) {
            final String[] os = options[oi];
            final String opt = os[0].toLowerCase();

            if(opt.equals("-d")) {
                destination = true;
            }
            
            if (opt.equals("-overview")) {
                if (nooverview==true) {
                    reporter
                        .printError("Option conflict -overview -nooverview");
                    return false;
                }
                overview = true;
            }
            else if (opt.equals("-nooverview")) {
                if (overview==true) {
                    reporter
                        .printError("Option conflict -nooverview -overview");
                    return false;
                }
                nooverview = true;
            }
        }
        
        //Lets check that they have specified an output folder.
        if(!destination) {
            reporter.printError("No -d option specified");
            return false;
        }
        
        return true;
    }

    /**
     * Return true if the doc element is getting documented, depending upon
     * -nodeprecated option and @deprecated tag used. Return true if
     * -nodeprecated is not used or @deprecated tag is not used.
     */
    public boolean isGeneratedDoc(Doc doc) {
        if (!nodeprecated) {
            return true;
        }
        return (doc.tags("deprecated")).length==0;
    }

    /**
     * Return true if the given qualifier should be excluded and false
     * otherwise.
     * 
     * @param qualifier the qualifier to check.
     */
    public boolean shouldExcludeQualifier(String qualifier) {
        if (excludedQualifiers.contains("all")||
            excludedQualifiers.contains(qualifier)||
            excludedQualifiers.contains(qualifier+".*")) {
            return true;
        }
        else {
            int index = -1;
            while ((index = qualifier.indexOf(".",index+1))!=-1) {
                if (excludedQualifiers.contains(qualifier.substring(0,index+1)+
                    "*")) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * Takes a string of : delimited tokens and creates a list out of it.
     * 
     * @param s set to add the tokens to
     * @param str to process
     * @since 2.1
     */
    private void addToSet(Set<String> s,String str) {
        final StringTokenizer st = new StringTokenizer(str,":");
        String current;
        while (st.hasMoreTokens()) {
            current = st.nextToken();
            s.add(current);
        }
    }

    /**
     * Sets the RootDoc and then sets the command line options supported by this
     * configuration.
     */
    public void setOptions(RootDoc root) {
        theRoot = root;
        initPackageArray();
        setOptions(root.options());
        setSpecificDocletOptions(root.options());
    }

    /**
     * To be over-ridden
     * 
     * @param options
     * @since 2.1
     */
    public void setSpecificDocletOptions(String[][] options) {
    }

    /**
     * Set up all of the arrays
     * 
     * @since 2.1
     */
    private void initPackageArray() {
        final Set<PackageDoc> set =
            new HashSet<PackageDoc>(Arrays.asList(theRoot.specifiedPackages()));
        final ClassDoc[] classes = theRoot.specifiedClasses();
        for (int i = 0;i<classes.length;i++) {
            set.add(classes[i].containingPackage());
        }
        final ArrayList<PackageDoc> results = new ArrayList<PackageDoc>(set);
        Collections.sort(results);
        packages = results.toArray(new PackageDoc[] {});
    }

    /**
     * Initialize the taglet manager. The strings to initialize the simple
     * custom tags should be in the following format:
     * "[tag name]:[location str]:[heading]".
     * 
     * @param customTagStrs the set two dimentional arrays of strings. These
     * arrays contain either -tag or -taglet arguments.
     */
    private void initTagletManager(Set<String[]> customTagStrs) {
        tagletManager =
            new DITATagletManager(nosince,showversion,showauthor);

        String[] args;
        for (final Iterator<String[]> it = customTagStrs.iterator();it
            .hasNext();) {
            args = it.next();
            if (args[0].equals("-taglet")) {
                tagletManager.addCustomTag(args[1],tagletpath);
                continue;
            }
            final String[] tokens = Util.tokenize(args[1],
                TagletManager.SIMPLE_TAGLET_OPT_SEPERATOR,3);
            if (tokens.length==1) {
                final String tagName = args[1];
                if (tagletManager.isKnownCustomTag(tagName)) {
                    // reorder a standard tag
                    tagletManager.addNewSimpleCustomTag(tagName,null,"");
                }
                else {
                    // Create a simple tag with the heading that has the same
                    // name as the tag.
                    final StringBuffer heading = new StringBuffer(tagName+":");
                    heading.setCharAt(0,
                        Character.toUpperCase(tagName.charAt(0)));
                    tagletManager.addNewSimpleCustomTag(tagName,
                        heading.toString(),"a");
                }
            }
            else if (tokens.length==2) {
                // Add simple taglet without heading, probably to excluding it
                // in the output.
                tagletManager.addNewSimpleCustomTag(tokens[0],tokens[1],"");
            }
            else if (tokens.length>=3) {
                tagletManager.addNewSimpleCustomTag(tokens[0],tokens[2],
                    tokens[1]);
            }
            else {
                DITADocletController.INSTANCE.printWarningMessage("Error - "+
                    args[1]+" is an invalid argument to the -tag option...");
            }
        }
    }

    /**
     * Add a trailing file separator, if not found or strip off extra trailing
     * file separators if any.
     * 
     * @param path Path under consideration.
     * @return String Properly constructed path string.
     */
    String addTrailingFileSep(String path) {
        final String fs = System.getProperty("file.separator");
        final String dblfs = fs+fs;
        int indexDblfs;
        while ((indexDblfs = path.indexOf(dblfs))>=0) {
            path = path.substring(0,indexDblfs)+
                path.substring(indexDblfs+fs.length());
        }
        if (!path.endsWith(fs)) {
            path += fs;
        }
        return path;
    }

    /**
     * This checks for the validity of the options used by the user. This works
     * exactly like
     * {@link com.sun.javadoc.Doclet#validOptions(String[][], DocErrorReporter)}
     * . This will validate the options which are shared by our doclets. For
     * example, this method will flag an error using the DocErrorReporter if
     * user has used "-nohelp" and "-helpfile" option together.
     * 
     * @param options options used on the command line.
     * @param reporter used to report errors.
     * @return true if all the options are valid.
     */
    public boolean generalValidOptions(String options[][],
    DocErrorReporter reporter) {
        boolean docencodingfound = false;
        String encoding = "";
        for (int oi = 0;oi<options.length;oi++) {
            final String[] os = options[oi];
            final String opt = os[0].toLowerCase();
            if (opt.equals("-d")) {
                final String destdirname = addTrailingFileSep(os[1]);
                final File destDir = new File(destdirname);
                if (!destDir.exists()) {
                    // Create the output directory (in case it doesn't exist
                    // yet)
                    reporter.printNotice("Created "+
                        destdirname);
                    (new File(destdirname)).mkdirs();
                }
                else if (!destDir.isDirectory()) {
                    reporter.printError(
                        "Destination not a directory "+
                            destDir.getPath());
                    return false;
                }
                else if (!destDir.canWrite()) {
                    reporter.printError("Directory Not Writable "+
                        destDir.getPath());
                    return false;
                }
            }
            else if (opt.equals("-docencoding")) {
                docencodingfound = true;
                if (!checkOutputFileEncoding(os[1],reporter)) {
                    return false;
                }
            }
            else if (opt.equals("-encoding")) {
                encoding = os[1];
            }
        }
        if (!docencodingfound&&encoding.length()>0) {
            if (!checkOutputFileEncoding(encoding,reporter)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check the validity of the given Source or Output File encoding on this
     * platform.
     * 
     * @param docencoding output file encoding.
     * @param reporter used to report errors.
     */
    private boolean checkOutputFileEncoding(String docencoding,
    DocErrorReporter reporter) {
        final OutputStream ost = new ByteArrayOutputStream();
        OutputStreamWriter osw = null;
        try {
            osw = new OutputStreamWriter(ost,docencoding);
        }
        catch (final UnsupportedEncodingException exc) {
            reporter.printError("Encoding not supported");
            return false;
        }
        finally {
            try {
                if (osw!=null) {
                    osw.close();
                }
            }
            catch (final IOException exc) {
            }
        }
        return true;
    }

    /**
     * Return true if the given doc-file subdirectory should be excluded and
     * false otherwise.
     * 
     * @param docfilesubdir the doc-files subdirectory to check.
     */
    public boolean shouldExcludeDocFileDir(String docfilesubdir) {
        if (excludedDocFileDirs.contains(docfilesubdir)) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Return the qualified name of the <code>ClassDoc</code> if it's qualifier
     * is not excluded. Otherwise, return the unqualified <code>ClassDoc</code>
     * name.
     * 
     * @param cd the <code>ClassDoc</code> to check.
     */
    public String getClassName(ClassDoc cd) {
        final PackageDoc pd = cd.containingPackage();
        if (pd!=null&&shouldExcludeQualifier(cd.containingPackage().name())) {
            return cd.name();
        }
        else {
            return cd.qualifiedName();
        }
    }

    /**
     * Returns the root destination folder
     * 
     * @return The destination root folder
     * @since 2.1
     */
    public File getRootFolder() {
        return theRootFolder;
    }

    /**
     * Returns the taglet manager
     * 
     * @return taglet manager
     * @since 2.1
     */
    public DITATagletManager getTagletManager() {
        return tagletManager;
    }

    /**
     * Returns the RootDoc given to us from the JavaDoc engine
     * 
     * @return the doc root from the JavaDoc processing engine
     * @since 2.1
     */
    public RootDoc getRootDoc() {
        return theRoot;
    }

    /**
     * If the option doctitle used, then use this as the title
     * 
     * @return the doc title, if one given
     * @since 2.1
     */
    public String getDocTitle() {
        return theDocTitle;
    }
}
