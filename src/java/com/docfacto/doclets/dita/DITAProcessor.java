/*
 * @author dhudson -
 * Created 4 Oct 2012 : 15:35:24
 */

package com.docfacto.doclets.dita;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

import com.docfacto.common.DocfactoException;
import com.docfacto.config.XmlConfig;
import com.docfacto.config.generated.Taglets;
import com.docfacto.dita.MapDocument;
import com.docfacto.dita.TopicRef;
import com.docfacto.javadoc.JavaDocUtils;
import com.docfacto.licensor.Licensor;
import com.docfacto.licensor.Products;
import com.docfacto.licensor.UDCProcessor;
import com.sun.javadoc.AnnotationTypeDoc;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.PackageDoc;
import com.sun.javadoc.RootDoc;

// http://docs.oracle.com/javase/1.5.0/docs/tooldocs/windows/javadoc.html

/**
 * This is the main processing point for the JavaDoc to DITA Doclet
 * 
 * After checking the Docfacto XML, the packages / classes / etc are processed
 * 
 * @author dhudson - created 4 Oct 2012
 * @since 2.1
 */
public class DITAProcessor {

    private final MapDocument theRootMap;

    private XmlConfig theXmlConfig;

    private Taglets theTagletsConfig;

    /**
     * A Map of package names and TopicRefs, the idea here is that users can
     * specify packages and files. They might also specify a file, that has
     * already been defined in a package to be documented.
     */
    private HashMap<String,TopicRef> thePackagesTopicRefs;

    /**
     * Create a new instance of <code>DITAProcessor</code>.
     * 
     */
    public DITAProcessor() {
        // Create a root map..
        theRootMap = new MapDocument();
        thePackagesTopicRefs = new HashMap<String,TopicRef>(5);

        final DITADocletConfiguration config =
            DITADocletController.INSTANCE.getConfig();
        if (config.getDocTitle()!=null) {
            theRootMap.addTitle(config.getDocTitle());
        }

        // Check some XML config stuff here...
    }

    /**
     * Process the JavaDoc comments
     * 
     * @return true if processing was successful
     * @since 2.1
     */
    public boolean process() {

        // No point processing if the correct options are not set
        if (!checkOptions()) {
            return false;
        }

        DITADocletController.INSTANCE
            .printNoticeMessage(DITADoclet.PRODUCT_NAME+": Version "+
                theXmlConfig.getProductRelease());

        try {

            RootDoc rootDoc = DITADocletController.INSTANCE
                .getConfig().getRootDoc();

            // Create a package summary page.
            final File summaryFile =
                new File(DITADocletController.INSTANCE.getConfig()
                    .getRootFolder(),
                    "r_summary.dita");

            final DITAPackagesWriter writer =
                new DITAPackagesWriter(summaryFile);
            writer.generateDITA();

            theRootMap.addToRootNode(writer.getTopicRef());

            // Process specified packages
            for (final PackageDoc packageDoc:rootDoc.specifiedPackages()) {
                processPackage(packageDoc);
            }

            // Process specified fields
            for (final ClassDoc classDoc:rootDoc.specifiedClasses()) {
                processClass(classDoc);
            }

            theRootMap.save(new File(DITADocletController.INSTANCE.getConfig()
                .getRootFolder(),"packages.ditamap"));

            return true;
        }
        catch (final IOException ex) {
            DITADocletController.INSTANCE
                .printWarningMessage("Unable to process JavaDoc "+
                    ex.getMessage());
        }
        catch (final DocfactoException ex) {
            DITADocletController.INSTANCE
                .printWarningMessage("Unable to process JavaDoc "+
                    ex.getMessage());
        }

        return false;
    }

    /**
     * Process the supplied package, creating DITAMAP files
     * 
     * @param packageDoc to process
     * @throws IOException if unable to save the DITA files to disk
     * @throws DocfactoException if unable to parse the JavaDoc
     * @since 2.1
     */
    private void processPackage(PackageDoc packageDoc) throws IOException,
    DocfactoException {

        // Get the package root creating subdirs on the way
        final File packageRoot =
            DITADocletUtils.createFolders(
                DITADocletController.INSTANCE.getConfig().destDirName,
                packageDoc.name());

        // Copy doc files..
        DITADocletUtils.copyDocFiles(packageDoc.name(),packageRoot);

        final TopicRef packageTopicRef =
            processPackage(packageDoc,packageRoot);

        // Might just want to get all of the classes and then sort them
        processClassDocs(packageDoc.interfaces(),packageRoot,
            packageTopicRef);
        processClassDocs(packageDoc.ordinaryClasses(),packageRoot,
            packageTopicRef);
        processClassDocs(packageDoc.enums(),packageRoot,packageTopicRef);
        processClassDocs(packageDoc.exceptions(),packageRoot,
            packageTopicRef);
        processClassDocs(packageDoc.errors(),packageRoot,packageTopicRef);

        processAnnotations(packageDoc.annotationTypes(),packageRoot,
            packageTopicRef);

        theRootMap.addToRootNode(packageTopicRef);
    }

    /**
     * Process the supplied packages, creating DITAMAP files
     * 
     * @param classDoc to process
     * @throws IOException if unable to save the DITA files to disk
     * @throws DocfactoException if unable to parse the JavaDoc
     * @since 2.1
     */
    private void processClass(ClassDoc classDoc) throws IOException,
    DocfactoException {

        // Get the package root creating subdirs on the way
        final File packageRoot =
            DITADocletUtils.createFolders(
                DITADocletController.INSTANCE.getConfig().destDirName,
                classDoc.containingPackage().name());

        TopicRef packageTopicRef =
            thePackagesTopicRefs.get(classDoc.containingPackage().name());

        if (packageTopicRef==null) {
            // First time we have seen this package

            // Copy doc files..
            DITADocletUtils.copyDocFiles(classDoc.containingPackage().name(),
                packageRoot);

            packageTopicRef =
                processPackage(classDoc.containingPackage(),packageRoot);
            
            theRootMap.addToRootNode(packageTopicRef);
        }

        processClassDoc(classDoc,packageRoot,packageTopicRef);
    }

    /**
     * Process the package information
     * 
     * @param packageDoc to process
     * @param packageRoot of the destination file system
     * @return a {@code TopicRef} of generated file
     * @throws DocfactoException if unable to parse the JavaDoc
     * @throws IOException if unable to create the file
     * @since 2.1
     */
    private TopicRef processPackage(PackageDoc packageDoc,File packageRoot)
    throws DocfactoException, IOException {

        final File packageFile = new File(packageRoot,"r_package.dita");
        final DITAPackageWriter writer =
            new DITAPackageWriter(packageFile);
        writer.generateDITA(packageDoc);

        TopicRef packageTopicRef = writer.getTopicRef();

        thePackagesTopicRefs.put(packageDoc.name(),packageTopicRef);
        return packageTopicRef;
    }

    /**
     * Process Class information, generating a DITA file
     * 
     * @param classDocs to process
     * @param packageRoot destination root
     * @param topicRef parent topicref DITA element
     * @throws IOException in unable to create the file
     * @throws DocfactoException if unable to process the JavaDoc
     * @since 2.1
     */
    private void processClassDocs(ClassDoc[] classDocs,
    File packageRoot,TopicRef topicRef) {
        if (classDocs.length==0) {
            // Nothing to do
            return;
        }

        // Lets sort them in name order
        Arrays.sort(classDocs);

        for (final ClassDoc classDoc:classDocs) {
            processClassDoc(classDoc,packageRoot,topicRef);
        }
    }

    /**
     * Process the class doc, generating a DITA file
     * 
     * @param classDoc to process
     * @param packageRoot package root folder
     * @param topicRef package topic ref
     * @since 2.5
     */
    private void processClassDoc(ClassDoc classDoc,File packageRoot,
    TopicRef topicRef) {
        final File classFile =
            new File(packageRoot,"r_"+classDoc.name()+".dita");

        final DITAClassWriter classWriter =
            new DITAClassWriter(classFile);
        try {
            classWriter.generateDITA(classDoc);
        }
        catch (final IOException ex) {
            DITADocletController.INSTANCE
                .printWarningMessage("Unable to process file ["+classFile+
                    "] "+ex.getMessage());
        }
        catch (final DocfactoException ex) {
            DITADocletController.INSTANCE.printWarningMessage(
                classDoc.position(),
                "Unable to process JavaDoc "+ex.getMessage());
        }
        topicRef.addElement(classWriter.getTopicRef());
    }

    /**
     * Process annotations
     * 
     * {@docfacto.todo Needs to be implemented }
     * 
     * @param classDocs
     * @param packageRoot
     * @param topicRef
     * @throws IOException
     * @throws DocfactoException
     * @since 2.1
     */
    private void processAnnotations(AnnotationTypeDoc[] classDocs,
    File packageRoot,TopicRef topicRef) throws IOException, DocfactoException {
        if (classDocs.length==0) {
            return;
        }
    }

    /**
     * Check the license file supplied and see if its OK to proceed
     * 
     * @return true if OK to proceed
     * @since 2.1
     */
    private boolean checkOptions() {
        // Need to check for class path and source path
        final String[][] options =
            DITADocletController.INSTANCE.getConfig().getRootDoc().options();

        try {
            final File configFile = JavaDocUtils.getConfigFile(options);
            theXmlConfig = new XmlConfig(configFile);
            theTagletsConfig = theXmlConfig.getTagletsConfig();

            if (!Licensor.checkLicense(DITADoclet.PRODUCT_NAME,
                theXmlConfig.getProduct(Products.TAGLETS))) {
                DITADocletController.INSTANCE
                    .printErrorMessage("Invalid Docfacto Licence, please contact Docfacto Ltd [www.docfacto.com]");
                return false;
            }

            UDCProcessor.registerProduct(DITADoclet.PRODUCT_NAME,
                theXmlConfig.getProduct(Products.TAGLETS),theXmlConfig);
        }
        catch (final DocfactoException ex) {
            DITADocletController.INSTANCE
                .printErrorMessage("Unable to load config file "+
                    ex.getMessage());
            return false;
        }

        return true;
    }
}
