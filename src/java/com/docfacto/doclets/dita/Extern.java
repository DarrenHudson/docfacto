/*
 * @author dhudson -
 * Created 27 Nov 2012 : 17:16:45
 */

package com.docfacto.doclets.dita;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import com.docfacto.dita.Xref;
import com.docfacto.dita.Xref.Format;
import com.docfacto.dita.Xref.Scope;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.DocErrorReporter;
import com.sun.javadoc.ExecutableMemberDoc;
import com.sun.javadoc.MemberDoc;
import com.sun.javadoc.Parameter;
import com.sun.javadoc.ProgramElementDoc;
import com.sun.javadoc.SeeTag;
import com.sun.javadoc.Type;

public class Extern {

    /**
     * Map package names onto Extern Item objects. Lazily initialized.
     */
    private Map<String,Item> packageToItemMap;

    /**
     * True if we are using -linkoffline and false if -link is used instead.
     */
    private boolean linkoffline = false;

    /**
     * Create a new instance of <code>Extern</code>.
     */
    public Extern() {
    }

    /**
     * Determine if a doc item is externally documented.
     * 
     * @param doc A ProgramElementDoc.
     */
    public boolean isExternal(ProgramElementDoc doc) {
        if (packageToItemMap==null) {
            return false;
        }
        return packageToItemMap.get(doc.containingPackage().name())!=null;
    }

    /**
     * Convert a link to be an external link if appropriate.
     * 
     * @param pkgName The package name.
     * @param relativepath The relative path.
     * @param link The link to convert.
     * @return if external return converted link else return null
     */
    public String getExternalLink(String pkgName,
    String relativepath,String link) {
        final Item fnd = findPackageItem(pkgName);
        if (fnd!=null) {
            final String externlink = fnd.path+link;
            if (fnd.relative) { // it's a relative path.
                return relativepath+externlink;
            }
            else {
                return externlink;
            }
        }
        return null;
    }

    /**
     * Return an external link if possible, or null if not external.
     * 
     * This will check to see if its a package link / class link / method link
     * etc ..
     * 
     * @param tag {@code See} tag to process
     * @return An external link to what is specified by the See tag, or null if
     * not an external link
     * @since 2.1
     */
    public Xref getExternalXrefLink(SeeTag tag) {

        Item found = null;
        if (tag.referencedPackage()!=null) {
            found = findPackageItem(tag.referencedPackage().name());
        }
        else if (tag.referencedClass()!=null) {
            found =
            findPackageItem(tag.referencedClass().containingPackage()
                .name());
        }

        if (found!=null) {
            final Xref xref = new Xref();
            xref.setFormat(Format.HTML);
            xref.setScope(Scope.EXTERNAL);

            if (tag.referencedClass()==null) {
                // Its a package reference
                xref.setHref(getPackageRefLink(found));
                xref.setValue(tag.referencedPackage().name());
                return xref;
            }

            if (tag.referencedMember()==null) {
                // Its a class reference
                xref.setHref(getClassRefLink(found,tag.referencedClass()));
                xref.setValue(tag.referencedClassName());
                return xref;
            }

            // Its a member reference
            final MemberDoc refMem = tag.referencedMember();

            String refMemName =
            tag.referencedClass().name()+"."+tag.referencedMemberName();

            if (refMem instanceof ExecutableMemberDoc) {
                if (refMemName.indexOf('(')<0) {
                    refMemName += ((ExecutableMemberDoc)refMem).signature();
                }
            }

            final StringBuilder builder =
            new StringBuilder(found.path.length()+50);
            builder.append(getClassRefLink(found,tag.referencedClass()));
            builder.append("#");
            builder.append(refMemName);

            xref.setHref(builder.toString());

            xref.setValue(refMemName);
            return xref;
        }

        // Its not in my list
        return null;
    }

    /**
     * More user friendly link generator that the JavaDoc one.
     * 
     * @param doc to process
     * @return external link or null
     * @since 2.1
     */
    public Xref getExternalXrefLink(ClassDoc doc) {
        final Item found = findPackageItem(doc.containingPackage().name());
        if (found!=null) {
            final Xref xref = new Xref();
            xref.setScope(Scope.EXTERNAL);
            xref.setFormat(Format.HTML);
            xref.setHref(getClassRefLink(found,doc));
            xref.setValue(doc.typeName());
            return xref;
        }

        return null;
    }

    /**
     * Generate an external link to a method or constructor
     * 
     * @param doc to process
     * @return an external link to the member doc or null
     * @since 2.1
     */
    public Xref getExternalXrefLink(ExecutableMemberDoc doc) {
        if (doc.containingClass()!=null) {
            final Item item = findPackageItem(doc.containingPackage().name());
            if (item!=null) {
                final StringBuilder builder =
                new StringBuilder(item.path.length()+50);
                builder.append(getClassRefLink(item,doc.containingClass()));
                builder.append("#");

                builder.append(doc.name());

                if (builder.indexOf("(")<0) {
                    final Parameter[] params = doc.parameters();
                    if(params != null) {
                        builder.append("(");
                        for(int i=0; i< params.length; i++) {
                            final Type type = params[i].type();
                            builder.append(type.qualifiedTypeName());
                            if(params.length != i+1) {
                                builder.append(",");
                            }
                        }
                        builder.append(")");
                    }
                }

                final Xref xref = new Xref();
                xref.setScope(Scope.EXTERNAL);
                xref.setFormat(Format.HTML);
                xref.setHref(builder.toString());

                xref.setValue(doc.containingClass().name()+"."+doc.name());

                return xref;
            }
        }

        return null;
    }

    /**
     * More user friendly link generator that the JavaDoc one.
     * 
     * @param doc to process
     * @return external link or null
     * @since 2.1
     */
    private String getClassRefLink(Item item,ClassDoc doc) {
        final StringBuilder builder = new StringBuilder(item.path.length()+40);
        builder.append(item.path);
        builder.append(doc.name());
        builder.append(".html?is-external=true");
        return builder.toString();
    }

    /**
     * Grab a link to an external package
     * 
     * @param doc
     * @return an external link or null
     * @since 2.1
     */
    private String getPackageRefLink(Item item) {
        final StringBuilder builder = new StringBuilder(item.path.length()+40);
        builder.append(item.path);
        builder.append("package-summary.html?is-external=true");
        return builder.toString();
    }

    /**
     * Grab a link to a method or constructor
     *
     * @param item found item
     * @param doc to process
     * @return a link to a method or constructor
     * @since 2.1
     */
    public String getMemberRefLink(Item item,MemberDoc doc) {
        final ClassDoc classDoc = doc.containingClass();
        String refMemName = classDoc.name()+"."+doc.name();

        if (doc instanceof ExecutableMemberDoc) {
            if (refMemName.indexOf('(')<0) {
                refMemName += ((ExecutableMemberDoc)doc).signature();
            }
        }

        final StringBuilder builder = new StringBuilder(item.path.length()+50);
        builder.append(getClassRefLink(item,classDoc));
        builder.append("#");
        builder.append(refMemName);

        return builder.toString();
    }

    /**
     * Build the extern package list from given URL or the directory path. Flag
     * error if the "-link" or "-linkoffline" option is already used.
     * 
     * @param url URL or Directory path.
     * @param pkglisturl This can be another URL for "package-list" or ordinary
     * file.
     * @param reporter The <code>DocErrorReporter</code> used to report errors.
     * @param linkoffline True if -linkoffline isused and false if -link is
     * used.
     */
    public boolean url(String url,String pkglisturl,
    DocErrorReporter reporter,boolean linkoffline) {
        this.linkoffline = linkoffline;
        final String errMsg = composeExternPackageList(url,pkglisturl);
        if (errMsg!=null) {
            reporter.printWarning(errMsg);
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * Get the Extern Item object associated with this package name.
     * 
     * @param pkgname Package name.
     */
    private Item findPackageItem(String pkgName) {
        if (packageToItemMap==null) {
            return null;
        }
        return packageToItemMap.get(pkgName);
    }

    /**
     * Adjusts the end file separator if it is missing from the URL or the
     * directory path and depending upon the URL or file path, fetch or read the
     * "package-list" file.
     * 
     * @param urlOrDirPath URL or the directory path.
     * @param pkgListUrlOrDirPath URL or directory path for the "package-list"
     * file or the "package-list" file itself.
     */
    private String composeExternPackageList(String urlOrDirPath,
    String pkgListUrlOrDirPath) {
        urlOrDirPath = adjustEndFileSeparator(urlOrDirPath);
        pkgListUrlOrDirPath = adjustEndFileSeparator(pkgListUrlOrDirPath);
        return isUrl(pkgListUrlOrDirPath) ?
            fetchURLComposeExternPackageList(urlOrDirPath,pkgListUrlOrDirPath) :
                readFileComposeExternPackageList(urlOrDirPath,pkgListUrlOrDirPath);
    }

    /**
     * If the URL or Directory path is missing end file separator, add that.
     */
    private String adjustEndFileSeparator(String url) {
        final String filesep = "/";
        if (!url.endsWith(filesep)) {
            url += filesep;
        }
        return url;
    }

    /**
     * Fetch the URL and read the "package-list" file.
     * 
     * @param urlpath Path to the packages.
     * @param pkglisturlpath URL or the path to the "package-list" file.
     */
    private String fetchURLComposeExternPackageList(String urlpath,
    String pkglisturlpath) {
        final String link = pkglisturlpath+"package-list";
        try {
            //TODO: Have this cached, as it doesn't change

            readPackageList((new URL(link)).openStream(),urlpath,false);
        }
        catch (final MalformedURLException exc) {
            return "Malformed URL "+link;
        }
        catch (final IOException exc) {
            return "Error fetching URL "+link;
        }
        return null;
    }

    /**
     * Read the "package-list" file which is available locally.
     * 
     * @param path URL or directory path to the packages.
     * @param pkgListPath Path to the local "package-list" file.
     */
    private String readFileComposeExternPackageList(String path,
    String pkgListPath) {

        String link = pkgListPath+"package-list";
        if (!((new File(pkgListPath)).isAbsolute()||linkoffline)) {
            link = DITADocletController.INSTANCE.getDestDirName()+link;
        }
        try {
            final File file = new File(link);
            if (file.exists()&&file.canRead()) {
                readPackageList(new FileInputStream(file),path,
                    !((new File(path)).isAbsolute()||isUrl(path)));
            }
            else {
                return "Error reading file "+link;
            }
        }
        catch (final FileNotFoundException exc) {
            return "Error reading file "+link;
        }
        catch (final IOException exc) {
            return "Error reading file "+link;
        }
        return null;
    }

    /**
     * Read the file "package-list" and for each package name found, create
     * Extern object and associate it with the package name in the map.
     * 
     * @param input InputStream from the "package-list" file.
     * @param path URL or the directory path to the packages.
     * @param relative Is path relative?
     */
    private void readPackageList(InputStream input,String path,
    boolean relative)
    throws IOException {
        final BufferedReader in =
        new BufferedReader(new InputStreamReader(input));
        final StringBuffer strbuf = new StringBuffer();
        try {
            int c;
            while ((c = in.read())>=0) {
                final char ch = (char)c;
                if (ch=='\n'||ch=='\r') {
                    if (strbuf.length()>0) {
                        final String packname = strbuf.toString();
                        final String packpath = path+
                        packname.replace('.','/')+'/';
                        new Item(packname,packpath,relative);
                        strbuf.setLength(0);
                    }
                }
                else {
                    strbuf.append(ch);
                }
            }
        }
        finally {
            input.close();
        }
    }

    public boolean isUrl(String urlCandidate) {
        try {
            new URL(urlCandidate);
            // No exception was thrown, so this must really be a URL.
            return true;
        }
        catch (final MalformedURLException e) {
            // Since exception is thrown, this must be a directory path.
            return false;
        }
    }

    /**
     * Stores the info for one external doc set
     */
    private class Item {

        /**
         * Package name, found in the "package-list" file in the {@link path}.
         */
        final String packageName;

        /**
         * The URL or the directory path at which the package documentation will
         * be avaliable.
         */
        final String path;

        /**
         * If given path is directory path then true else if it is a URL then
         * false.
         */
        final boolean relative;

        /**
         * Constructor to build a Extern Item object and map it with the package
         * name. If the same package name is found in the map, then the first
         * mapped Item object or offline location will be retained.
         * 
         * @param packagename Package name found in the "package-list" file.
         * @param path URL or Directory path from where the "package-list" file
         * is picked.
         * @param relative True if path is URL, false if directory path.
         */
        Item(String packageName,String path,boolean relative) {
            this.packageName = packageName;
            this.path = path;
            this.relative = relative;
            if (packageToItemMap==null) {
                packageToItemMap = new HashMap<String,Item>();
            }
            if (!packageToItemMap.containsKey(packageName)) { // save the
                // previous
                packageToItemMap.put(packageName,this); // mapped location
            }
        }

        /**
         * String representation of "this" with packagename and the path.
         */
        @Override
        public String toString() {
            return packageName+(relative ? " -> " : " => ")+path;
        }
    }
}
