/*
 * @author dhudson -
 * Created 7 Nov 2012 : 15:30:16
 */

package com.docfacto.doclets.dita;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.common.StringUtils;
import com.sun.javadoc.AnnotationDesc;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.Doc;
import com.sun.javadoc.ExecutableMemberDoc;
import com.sun.javadoc.FieldDoc;
import com.sun.javadoc.ProgramElementDoc;
import com.sun.javadoc.Type;

/**
 * Collection of static method utils for the JavaDoc to DITA doclet
 * 
 * @author dhudson - created 4 Dec 2012
 * @since 2.1
 */
public class DITADocletUtils {

    private static final String[] IMPLIED_TAGS = {
        "html","head","body"};

    // private static final Pattern PACKAGE_TO_PATH = Pattern.compile("[.]+/");

    private static final Pattern ARRAY_PATTERN = Pattern.compile(Pattern
        .quote("[]"));

    private static final String DOC_FILES = "doc-files";

    @SuppressWarnings("serial")
    static final Map<String,String> IMPLIED_TAGS_MAP =
        new HashMap<String,String>(IMPLIED_TAGS.length) {
            {
                // Load the static map
                for (final String tag:IMPLIED_TAGS) {
                    put(tag,tag);
                }
            }
        };

    /**
     * Return true if this class is linkable and false if we can't link to the
     * desired class. {docfacto.note You can only link to external classes if
     * they are public or protected.}
     * 
     * @param classDoc the class to check.
     * 
     * @return true if this class is linkable and false if we can't link to the
     * desired class.
     */
    public static boolean isLinkable(ClassDoc classDoc) {
        if (classDoc==null) {
            return false;
        }

        final DITADocletConfiguration config =
            DITADocletController.INSTANCE.getConfig();

        return ((classDoc.isIncluded()&&config.isGeneratedDoc(classDoc)))||
            (config.extern.isExternal(classDoc)&&
            (classDoc.isPublic()||classDoc.isProtected()));
    }

    /**
     * Given a class, return the closest visible super class.
     * 
     * @param classDoc the class we are searching the parent for.
     * @return the closest visible super class. Return null if it cannot be
     * found (i.e. classDoc is java.lang.Object).
     */
    public static Type getFirstVisibleSuperClass(ClassDoc classDoc) {
        if (classDoc==null) {
            return null;
        }

        Type sup = classDoc.superclassType();
        ClassDoc supClassDoc = classDoc.superclass();
        while (sup!=null&&
            (!(supClassDoc.isPublic()||
            isLinkable(supClassDoc)))) {
            if (supClassDoc.superclass().qualifiedName()
                .equals(supClassDoc.qualifiedName()))
                break;
            sup = supClassDoc.superclassType();
            supClassDoc = supClassDoc.superclass();
        }
        if (classDoc.equals(supClassDoc)) {
            return null;
        }
        return sup;
    }

    /**
     * Calculate from the destination path the folder for this package
     * 
     * @param packageDocName to process
     * @return the folder that the destination will reside
     * @since 2.1
     */
    public static File getPackageRoot(String packageDocName) {
        final String path =
            DITADocletController.INSTANCE.getDestinationPath()+"/"+
                packageDocName.replaceAll("[.]+","/");
        return new File(path);
    }

    /**
     * The name is dot notation I.E com.docfacto and we need this in folder of
     * com/docfacto notation
     * 
     * @param rootFolder destination root folder
     * @param name of the package to create
     * @return the newly created folder, or the folder that exists
     * @since 2.1
     */
    public static File createFolders(String rootFolder,String name) {
        final File subDirs =
            new File(rootFolder,name.replaceAll("[.]+","/"));
        subDirs.mkdirs();
        return subDirs;
    }

    /**
     * Return true if the given Doc is deprecated.
     * 
     * @param doc the Doc to check.
     * @return true if the given Doc is deprecated.
     */
    public static boolean isDeprecated(Doc doc) {
        if (doc.tags("deprecated").length>0) {
            return true;
        }
        if (doc instanceof ProgramElementDoc) {
            final ProgramElementDoc peDoc = (ProgramElementDoc)doc;

            final AnnotationDesc[] annotationDescList = peDoc.annotations();
            for (int i = 0;i<annotationDescList.length;i++) {
                if (annotationDescList[i].annotationType().qualifiedName()
                    .equals(
                        java.lang.Deprecated.class.getName())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Produce a unique ID for the field
     * 
     * @param fieldDoc to process
     * @return a unique ID for the field
     * @since 2.1
     */
    public static String getIDFor(FieldDoc fieldDoc) {
        final StringBuilder builder = new StringBuilder(100);
        builder.append(fieldDoc.toString());
        return StringUtils
            .normaliseForNMToken(builder.toString().toCharArray());
    }

    /**
     * Produce a unique ID based on the name and the signature
     * 
     * @param methodDoc to process
     * @return a unique ID for executable member
     * @since 2.1
     */
    public static String getIDFor(ExecutableMemberDoc methodDoc) {
        final StringBuilder builder = new StringBuilder(100);
        builder.append(methodDoc.name());
        builder.append(ARRAY_PATTERN.matcher(methodDoc.flatSignature())
            .replaceAll("Array"));
        return StringUtils
            .normaliseForNMToken(builder.toString().toCharArray());
    }

    /**
     * Copy the doc-files if they exist for a package.
     * 
     * @param packageDocName name of the package doc
     * @param outputPackageRoot destination folder
     * @throws DocfactoException if unable to copy files
     * @since 2.3
     */
    public static void
    copyDocFiles(String packageDocName,File outputPackageRoot)
    throws DocfactoException {
        DITADocletConfiguration config =
            DITADocletController.INSTANCE.getConfig();

        File packageRoot =
            new File(config.sourcepath,packageDocName.replaceAll("[.]+","/"));

        File sourceDocFiles = new File(packageRoot,DOC_FILES);

        if (sourceDocFiles.exists()&&sourceDocFiles.isDirectory()) {
            File destDocFiles = new File(outputPackageRoot,DOC_FILES);
            IOUtils.copyFolder(sourceDocFiles,destDocFiles,
                config.copydocfilesubdirs);
        }
    }

}
