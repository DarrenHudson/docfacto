/*
 * @author dhudson -
 * Created 5 Nov 2012 : 10:43:53
 */

package com.docfacto.doclets.dita;

import java.util.ArrayList;
import java.util.List;

import com.docfacto.dita.DITAElement;
import com.docfacto.xml.Element;
import com.docfacto.xml.XMLTextElement;

/**
 * Taglets should use this to return the DITA elements / XMLTextElements they require
 * 
 * @author dhudson - created 5 Nov 2012
 * @since 2.1
 */
public class DITATagletOutput {


    private final List<Element> theElements;

    /**
     * Create a new instance of <code>DocfactoTagletOutput</code>.
     */
    public DITATagletOutput() {
        theElements = new ArrayList<Element>(3);
    }

    /**
     * Create a new instance of <code>DocfactoTagletOutput</code>.
     * 
     * @param comment
     */
    public DITATagletOutput(String comment) {
        this();
        final XMLTextElement element = new XMLTextElement(comment);
        theElements.add(element);
    }

    /**
     * Returns the list of elements
     *
     * @return a list of DITA elements
     * @since 2.1
     */
    public List<Element> getDITAElements() {
        return theElements;
    }

    /**
     * Append the {@code DITAElement} to the list of elements
     *
     * @param element
     * @since 2.1
     */
    public void appendDITAElement(DITAElement element) {
        theElements.add(element);
    }

    /**
     * Walk through the {@code DITATagletOutput} and append the elements to this element
     *
     * @param output to process
     * @since 2.1
     */
    public void appendDITAElements(DITATagletOutput output) {
        for(final Element element : output.getDITAElements()) {
            theElements.add(element);
        }
    }

    /**
     * Populate the given element with the elements in contained in this class
     *
     * @param element to populate
     * @since 2.1
     */
    public void populateElement(DITAElement element) {
        for(final Element el : theElements) {
            element.addElement(el);
        }
    }

    /**
     * Convenience method to create a {@code XMLTextElement} and append it to the children of this node
     *
     * @param value to give the {@code XMLTextElement}
     * @since 2.1
     */
    public void appendTextElement(String value) {
        if(value == null) {
            return;
        }

        theElements.add(new XMLTextElement(value));
    }

    /**
     * Check to see if there are any elements
     *
     * @return true if there are no elements
     * @since 2.1
     */
    public boolean isEmpty() {
        return theElements.isEmpty();
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder(100);
        for(final Element element : theElements) {
            builder.append(element.toXML());
        }

        return builder.toString();
    }
}
