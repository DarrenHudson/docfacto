/*
 * @author dhudson -
 * Created 9 Nov 2012 : 11:35:51
 */

package com.docfacto.doclets.dita;

import java.io.File;
import java.io.IOException;

import com.docfacto.common.DocfactoException;
import com.docfacto.dita.RefBody;
import com.docfacto.dita.ReferenceTopicDocument;
import com.docfacto.dita.ShortDesc;
import com.docfacto.dita.Title;

/**
 * Class to write the package summary
 * 
 * @author dhudson - created 21 Nov 2012
 * @since 2.2
 */
public class DITAPackagesWriter extends DITADocletWriter {

    /**
     * Create a new instance of <code>DITAPackagesWriter</code>.
     * 
     * @param ditaFile
     */
    public DITAPackagesWriter(File ditaFile) {
        super(ditaFile);
    }

    /**
     * Generate the r_summary package DITA file
     * 
     * @throws IOException
     * @throws DocfactoException
     * @since 2.2
     */
    public void generateDITA() throws IOException, DocfactoException {

        final ReferenceTopicDocument pSummary = new ReferenceTopicDocument();
        pSummary.setReferenceID("package.summary");
        pSummary.addToRootNode(new Title("Package Summary"));
        pSummary.addToRootNode(new ShortDesc("Package Summary"));

        final RefBody body = new RefBody();

        final DITADocletConfiguration config =
        DITADocletController.INSTANCE.getConfig();
        
        generateSummaryTable("Package Summary",config.getRootDoc().name(),
            config.getRootDoc().specifiedPackages(),body);

        pSummary.addToRootNode(body);
        pSummary.save(getDITAFile());
    }
}
