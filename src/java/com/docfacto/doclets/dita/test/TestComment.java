/*
 * @author dhudson -
 * Created 11 Nov 2012 : 12:04:08
 */

package com.docfacto.doclets.dita.test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.docfacto.common.DocfactoException;
import com.docfacto.xml.XMLElement;



/**
 * First Line..
 *<P>
 * TODO - Description.
 *
 * @author dhudson - created 17 Dec 2012
 * @since n.n
 */
public class TestComment {

    /**
     * First <code>line</code> & with an<br>
     * This should be on a line..<br>
     * This then should be in a new p
     * <P>
     * Main description & with an
     *
     * @param x to <code>process</code>
     * @param y an int
     * @param z double
     * @return some stuff
     * @throws IOException
     * @throws DocfactoException
     * @see HashMap#put(Object, Object)
     * @return {@link Locale#getCountry()}
     */
    public List<XMLElement> getStuff(Map<String,Object> y, Foo<?> z,String... stuff) throws IOException, DocfactoException  {
        return null;
    }

    /**
     * TODO - Method Title
     * <P>
     * TODO - Method Description
     *
     */
    public void foo() {

    }

    /**
     * Simple grid so when drawing in Java, able to see X and Y co-ords
     *<P>
     * {@docfacto.todo  Make all this configurable, <circle>This is a circle</circle> <code>going</code> <a href="bollocks">bollocks</a> to be useful in the future }
     *
     * @author dhudson - created 19 May 2012
     * @since 2.0
     */
    public void rubbish(){

    }

    /**
     * This method is deprecated
     *
     * @since 2.1
     * @deprecated
     */
    @Deprecated
    public void deprecated() {

    }

}
