/*
 * @author dhudson -
 * Created 9 Nov 2012 : 09:23:06
 */

package com.docfacto.doclets.dita;

import java.io.File;
import java.io.IOException;

import com.docfacto.common.DocfactoException;
import com.docfacto.dita.RefBody;
import com.docfacto.dita.ReferenceTopicDocument;
import com.docfacto.dita.Section;
import com.docfacto.dita.ShortDesc;
import com.docfacto.dita.Title;
import com.sun.javadoc.PackageDoc;

/**
 * Package details writer
 *
 * @author dhudson - created 21 Nov 2012
 * @since 2.1
 */
public class DITAPackageWriter extends DITADocletWriter {

    /**
     * Create a new instance of <code>DITAPackageWriter</code>.
     * @param ditaFile
     */
    public DITAPackageWriter(File ditaFile) {
        super(ditaFile);
    }

    /**
     * Generate the DITA file for the package
     *
     * @param packageDoc
     * @throws IOException
     * @throws DocfactoException
     * @since 2.1
     */
    public void generateDITA(PackageDoc packageDoc)
    throws IOException, DocfactoException {

        // Create folder ..
        final ReferenceTopicDocument packageOverview =
        new ReferenceTopicDocument();
        packageOverview.setReferenceID(packageDoc.name());
        packageOverview.addToRootNode(new Title(packageDoc.name()));

        final DITAJavaDocParser parser = new DITAJavaDocParser(packageDoc,this);

        packageOverview.addToRootNode(new ShortDesc(packageDoc.name()));

        final RefBody body = new RefBody();
        packageOverview.addToRootNode(body);

        generateSummaryTable("Interface Summary",packageDoc.name(),
            packageDoc.interfaces(),body);
        generateSummaryTable("Class Summary",packageDoc.name(),
            packageDoc.ordinaryClasses(),body);
        generateSummaryTable("Enum Summary",packageDoc.name(),
            packageDoc.enums(),body);
        generateSummaryTable("Exception Summary",packageDoc.name(),
            packageDoc.exceptions(),body);
        generateSummaryTable("Error Summary",packageDoc.name(),
            packageDoc.errors(),body);
        generateSummaryTable("AnnotationTypes Summary",packageDoc.name(),
            packageDoc.annotationTypes(),body);

        // Description section
        final Section section = new Section();
        section.setID("description");
        section.addElement(new Title("Package "+packageDoc.name()+
        " Description"));

        parser.addComments(section);

        parser.addBlockTags(section);

        body.addElement(section);

        packageOverview.generateDocfactoComment();

        packageOverview.save(getDITAFile());

    }
}
