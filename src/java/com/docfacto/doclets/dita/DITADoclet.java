/*
 * @author dhudson -
 * Created 4 Oct 2012 : 15:32:38
 */

package com.docfacto.doclets.dita;

import java.io.File;

import com.docfacto.common.DocfactoException;
import com.docfacto.config.XmlConfig;
import com.docfacto.javadoc.JavaDocUtils;
import com.docfacto.taglets.TagletLoader;
import com.docfacto.taglets.dita.DITATaglet;
import com.sun.javadoc.DocErrorReporter;
import com.sun.javadoc.Doclet;
import com.sun.javadoc.LanguageVersion;
import com.sun.javadoc.RootDoc;

/**
 * This is the entry point for JavaDoc to DITA
 * 
 * @author dhudson - created 28 Nov 2012
 * @since 2.1
 */
public class DITADoclet extends Doclet {

    /**
     * Current product name {@value}
     */
    static final String PRODUCT_NAME = "DITADoclet";

    /**
     * Constructor
     * 
     * @since 2.0
     */
    public DITADoclet() {
        // Will never be called
    }

    /**
     * @see com.sun.javadoc.Doclet#start
     */
    public static boolean start(RootDoc root) {

        // Lets set everything up
        DITADocletController.INSTANCE.setup(root);

        // Load the Docfacto taglets
        final DITATagletManager tagletManager =
        DITADocletController.INSTANCE.getTagletManager();

        final XmlConfig config =
        DITADocletController.INSTANCE.getDocfactoConfig();

        try {
            final TagletLoader loader = new TagletLoader();

            for (final DITATaglet taglet:loader.getDitaTagletsToRegister(config)) {
                tagletManager.replaceCustomTag(taglet);
            }

        }
        catch (final DocfactoException ex) {
            DITADocletController.INSTANCE
            .printErrorMessage("Unable to load Docfacto Taglets "+
            ex.getMessage());
        }

        final DITAProcessor processor = new DITAProcessor();
        return processor.process();
    }

    /**
     * @see com.sun.javadoc.Doclet#validOptions
     */
    public static boolean validOptions(String[][] options,
    DocErrorReporter errorReporter) {
        for (final String[] option:options) {
            if (JavaDocUtils.CONFIG_OPTION.equals(option[0])) {
                final File configFile = new File(option[1]);

                if (!configFile.canRead()) {
                    errorReporter.printError(
                        JavaDocUtils.CONFIG_OPTION+
                    " Unable to read config file.");
                    return false;
                }

                try {
                    final XmlConfig config = new XmlConfig(configFile);
                    DITADocletController.INSTANCE.setDocfactoConfig(config);
                }
                catch (final DocfactoException ex) {
                    errorReporter.printError(
                        "Unable to load Docfacto config file ["+
                        configFile.getPath()+"] "+ex.getMessage());
                    return false;
                }
            }
        }

        return DITADocletController.INSTANCE.getConfig().validOptions(options,
            errorReporter);
    }

    /**
     * @see com.sun.javadoc.Doclet#optionLength
     */
    public static int optionLength(String option) {
        if (JavaDocUtils.CONFIG_OPTION.equals(option)) {
            return 2;
        }

        return DITADocletController.INSTANCE.getConfig().optionLength(option);
    }

    /**
     * Return the 1.5 JVM as we are dealing with generics
     * 
     * @return 1.5 so that we can have Generics
     * @since 2.1
     */
    public static LanguageVersion languageVersion() {
        return LanguageVersion.JAVA_1_5;
    }

    /**
     * Command line entry point
     * 
     * @param args doclet arguments, these should include
     * <dl>
     * <dt>-doclet com.docfacto.doclets.dita.DITADoclet</dt>
     * <dt>-docletpath docfacto.jar</dt>
     * <dt>-config ...</dt>
     * <dt>@packages</dt>
     * </dl>
     * @since 2.0
     */
    public static void main(String[] args) {
        com.sun.tools.javadoc.Main.execute(PRODUCT_NAME,args);
    }
}
