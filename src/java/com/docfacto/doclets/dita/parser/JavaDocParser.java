/*
 * @author dhudson -
 * Created 19 Nov 2012 : 14:06:56
 */

package com.docfacto.doclets.dita.parser;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.docfacto.common.NameValuePair;
import com.docfacto.common.StringUtils;
import com.docfacto.dita.DITAElement;
import com.docfacto.dita.DynamicElement;
import com.docfacto.dita.P;
import com.docfacto.dita.Section;
import com.docfacto.exceptions.DocfactoParsingException;
import com.docfacto.xml.DefaultParserHandler;
import com.docfacto.xml.GenericDynamicElement;
import com.docfacto.xml.XMLElement;
import com.docfacto.xml.XMLFastParser;
import com.docfacto.xml.XMLTextElement;

/**
 * Parse the JavaDoc using a lax XML parser. Replace any tags that we know
 * about, or just remove them if we don't.
 * 
 * @author dhudson - created 21 Nov 2012
 * @since 2.1
 */
public class JavaDocParser extends DefaultParserHandler {
    private static final String[] ALLOWED_ATTRIBUTES = {
        "xml:space","outputclass","class","xtrf","xtrc","translate","xml:lang",
        "id","conref","platform","product","audience","otherprops",
        "importance","rev","status","href","external","format","type","scope",
        "placement","height","width","align","scale"
    };

    @SuppressWarnings("serial")
    private static final Set<String> ALLOWED_ATTRIBUTES_MAP =
        new HashSet<String>(ALLOWED_ATTRIBUTES.length) {
            {
                for (final String attr:ALLOWED_ATTRIBUTES) {
                    add(attr);
                }
            }
        };

    @SuppressWarnings("serial")
    private static final Map<String,String> REPLACE_TAGS_MAP =
        new HashMap<String,String>(12) {
            {
                put("code","codeph");
                put("a","xref");
                put("var","varname");
                put("samp","systemoutput");
                put("kdb","userinput");
                put("em","i");
                put("strong","b");
                put("blockquote","lq");
                // put("pre","codeblock");
                put("table","simpletable");
                put("th","sthead");
                put("tr","strow");
                put("td","stentry");
                put("br","p");
            }
        };

    private static String[] PRESERVE_TAGS = {"codeph","pre","codeblock",
        "messageblock"};

    @SuppressWarnings("serial")
    private static final Set<String> PRESERVE_TAGS_MAP =
        new HashSet<String>(PRESERVE_TAGS.length) {
            {
                for (final String tag:PRESERVE_TAGS) {
                    add(tag);
                }
            }
        };

    private static final String[] ALLOWED_P_TAGS = {
        "p","ph","codeph","synph","filepath","msgph","userinput",
        "systemoutput",
        "b","u","i","tt","sup","sub","uicontrol","menucascade","term","xref",
        "cite","q","boolean","state","keyword","option","parmname","apiname",
        "cmdname","msgnum","varname","wintitle","tm","lq","note","dl","parml",
        "ul","ol","sl","pre","codeblock","msgblock","screen","lines","fig",
        "syntaxdiagram","imagemap","image","title",
        "object","table","simpletable","draft-comment","required-cleanup","fn",
        "indextermref","indexterm","li","dd","dt","table","tgroup","colspec",
        "tbody","entry","row","note","example"};

    @SuppressWarnings("serial")
    static final Set<String> ALLOWED_TAGS_MAP =
        new HashSet<String>(ALLOWED_P_TAGS.length) {
            {
                // Load the static map
                for (final String tag:ALLOWED_P_TAGS) {
                    add(tag);
                }
            }
        };

    private final DITAElement theRoot;
    private DITAElement theCurrentElement;

    private final XMLFastParser theParser;

    /**
     * Create a new instance of <code>JavaDocParser</code>.
     * 
     * @param comment to process
     * @throws DocfactoParsingException if unable to parse the comment
     */
    public JavaDocParser(String comment) throws DocfactoParsingException {
        theParser = new XMLFastParser(comment);

        // Although we have used Section here, can be any node, as its the
        // children that we are after.
        theRoot = new Section();

        theParser.parse(this);
    }

    /**
     * @see com.docfacto.xml.XMLParserHandler#processEndTag(java.lang.String)
     */
    @Override
    public void processEndTag(String endTag) {
        final String name = getTagName(endTag);

        if (name==null) {
            System.err.println(" --> "+theParser.getXML());
        }

        if (!ALLOWED_TAGS_MAP.contains(name)) {
            // Nothing to do
            return;
        }

        // Ripple back to the the correct element, due to unterminated tags
        while (!name.equals(theCurrentElement.getElementName())) {
            theCurrentElement = (DITAElement)theCurrentElement.getParent();

            // Need to test here for theCurrentElement being null. This means
            // that no matching tag was found
            if (theCurrentElement==null) {
                // REMOVE:
                System.out.println("Panic .. could not find closing "+name+"["+
                    theParser.getXML()+"]");
                return;
            }
        }

        theCurrentElement = (DITAElement)theCurrentElement.getParent();
    }

    /**
     * @see com.docfacto.xml.XMLParserHandler#processStartTag(com.docfacto.xml.GenericDynamicElement)
     */
    @Override
    public void processStartTag(GenericDynamicElement gdl) {
        // Look up new tag name, so we are only dealing with DITA tags..
        final String name = getTagName(gdl.getElementName());

        if (!ALLOWED_TAGS_MAP.contains(name)) {
            // Nothing to do here, so lets just return and let the parent handle
            // some data
            return;
        }

        final DynamicElement element = new DynamicElement(name);

        for (final NameValuePair attr:gdl.getAttributes()) {
            // Only copy across attributes that we understand
            if (ALLOWED_ATTRIBUTES_MAP.contains(attr.getName())) {
                if (attr.getName().equals("href")) {
                    final String value = attr.getValue();
                    if (value.startsWith("http:")) {
                        element.addAttribute("scope","external");
                        element.addAttribute("format","html");
                    }
                }
                element.addAttribute(attr);
            }
        }

        if (theCurrentElement!=null) {
            // Is this a nested P
            if (isP(name)&&isP(theCurrentElement.getElementName())) {
                // You can't have nested P's, so lets create a new P
                final DITAElement parent =
                    (DITAElement)theCurrentElement.getParent();
                parent.addElement(element);

                theCurrentElement = element;
                return;
            }
        }

        if (name.equals("dl")) {
            // Inject a dlentry here...
            final DynamicElement dlentry = new DynamicElement("dlentry");
            element.addElement(dlentry);

            theCurrentElement.addElement(element);

            theCurrentElement = dlentry;
            return;
        }

        if (name.equals("dt")) {
            // We need to make sure that there has been no previous dd tags
            final XMLElement parent = theCurrentElement;
            if (parent.containsElementWithName("dd")) {
                // Need to create a new dlentry and add this dt tag to it
                final DITAElement dlentryParent =
                    (DITAElement)parent.getParent();

                // Inject another dlentry
                final DynamicElement dlentry = new DynamicElement("dlentry");
                dlentryParent.addElement(dlentry);
                dlentry.addElement(element);
            }
            else {
                theCurrentElement.addElement(element);
            }

            theCurrentElement = element;
            return;
        }

        // Just normal tags..
        if (theCurrentElement==null) {
            theCurrentElement = element;
            theRoot.addElement(element);
        }
        else {
            theCurrentElement.addElement(element);

            // Only do this if not an empty element
            if (!gdl.isEmptyElement()) {
                theCurrentElement = element;
            }
        }
    }

    /**
     * @see com.docfacto.xml.XMLParserHandler#processText(java.lang.String)
     */
    @Override
    public void processText(String text) {
        final XMLTextElement textElement =
            new XMLTextElement(StringUtils.escapeXML(text));

        if (theCurrentElement==null) {
            // We are at the beginning, if we get text, wrap it in a P node.
            final P p = new P();
            p.addElement(textElement);
            theCurrentElement = p;
            theRoot.addElement(p);
        }
        else {
            if (PRESERVE_TAGS_MAP.contains(theCurrentElement
                .getElementName())) {
                textElement.preserve();
            }

            theCurrentElement.addElement(textElement);
        }
    }

    /**
     * Check the replacement tag map and substitute if required
     * 
     * @param tagName tag name to check
     * @return a substituted tag name or the original tag name
     * @since 2.1
     */
    private String getTagName(String tagName) {
        final String result = REPLACE_TAGS_MAP.get(tagName);
        if (result==null) {
            return tagName;
        }

        return result;
    }

    /**
     * Check to see if tagName is a P
     * 
     * @param tagName to check
     * @return true is this the tagName is a P
     * @since 2.1
     */
    private boolean isP(String tagName) {
        return "p".equals(tagName);
    }

    /**
     * Return the child elements
     * 
     * @return the child elements
     * @since 2.1
     */
    public List<XMLElement> getChildElements() {
        return theRoot.getElements();
    }
}
