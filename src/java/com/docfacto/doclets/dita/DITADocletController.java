/*
 * @author dhudson -
 * Created 27 Nov 2012 : 15:35:18
 */

package com.docfacto.doclets.dita;

import java.io.File;

import com.docfacto.config.XmlConfig;
import com.docfacto.dita.DITAUtils;
import com.docfacto.dita.Xref;
import com.docfacto.dita.Xref.Format;
import com.docfacto.dita.Xref.Scope;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.ExecutableMemberDoc;
import com.sun.javadoc.PackageDoc;
import com.sun.javadoc.RootDoc;
import com.sun.javadoc.SourcePosition;

/**
 * The controller is a {@code enum} singleton pattern. This class makes it
 * simple to reference the configuration
 * 
 * @author dhudson - created 28 Nov 2012
 * @since 2.1
 */
public enum DITADocletController {

    /**
     * All method calls to this class must come through this for example
     * {@code DITADocletController.INSTANCE.printErrorMessage( ..)}
     */
    INSTANCE;

    private final DITADocletConfiguration theConfig;

    private XmlConfig theDocfactoConfig;

    /**
     * Create a new instance of <code>DITADocletController</code>.
     */
    private DITADocletController() {
        theConfig = new DITADocletConfiguration();
    }

    /**
     * This needs to be called from the Doclet entry point @see
     * com.sun.javadoc.Doclet#start
     * 
     * This will configure the DITADocletConfiguration with the supplied command
     * line args This will also kick start the Taglet Manager
     * 
     * @param rootDoc
     * @since 2.1
     */
    public void setup(RootDoc rootDoc) {

        theConfig.setOptions(rootDoc);

        // final ClassTree classTree = new ClassTree();

        // All other setup stuff should go in here ..
    }

    /**
     * Print an error message
     * 
     * @param message to print
     * @since 2.1
     */
    public void printErrorMessage(String message) {
        theConfig.getRootDoc().printError(message);
    }

    /**
     * Print an error message with positional information
     * 
     * @param pos positional information
     * @param message to print
     * @since 2.1
     */
    public void printErrorMessage(SourcePosition pos,String message) {
        theConfig.getRootDoc().printError(pos,message);
    }

    /**
     * Print a warning message
     * 
     * @param message to print
     * @since 2.1
     */
    public void printWarningMessage(String message) {
        theConfig.getRootDoc().printWarning(message);
    }

    /**
     * Print a warning message with positional information
     * 
     * @param pos positional information
     * @param message to print
     * @since 2.1
     */
    public void printWarningMessage(SourcePosition pos,String message) {
        theConfig.getRootDoc().printWarning(pos,message);
    }

    /**
     * Print a notice message
     * 
     * @param message to print
     * @since 2.1
     */
    public void printNoticeMessage(String message) {
        theConfig.getRootDoc().printNotice(message);
    }

    /**
     * Print a notice message with positional information
     * 
     * @param pos positional information
     * @param message to print
     * @since 2.1
     */
    public void printNoticeMessage(SourcePosition pos,String message) {
        theConfig.getRootDoc().printNotice(pos,message);
    }

    /**
     * Returns the Doclet configuration
     * 
     * @return the configuration
     * @since 2.1
     */
    public DITADocletConfiguration getConfig() {
        return theConfig;
    }

    /**
     * Returns the taglet manager
     * 
     * @return the taglet manager
     * @since 2.1
     */
    public DITATagletManager getTagletManager() {
        return theConfig.getTagletManager();
    }

    /**
     * Returns the destination directory
     * 
     * @return the destination directory
     * @since 2.1
     */
    public String getDestDirName() {
        return theConfig.destDirName;
    }

    /**
     * Return the path of the destination folder
     * 
     * @return returns the absolute path of the destination folder
     * @since 2.1
     */
    public String getDestinationPath() {
        return theConfig.getRootFolder().getAbsolutePath();
    }

    /**
     * Returns the root destination folder
     * 
     * @return the root destination folder
     * @since 2.1
     */
    public File getDestinationRootFolder() {
        return theConfig.getRootFolder();
    }

    /**
     * Returns the packages to process
     * 
     * @return the packages to process
     * @since 2.1
     */
    public PackageDoc[] getPackages() {
        return theConfig.packages;
    }

    /**
     * Sets the Docfacto config file
     *
     * @param config
     * @since 2.1
     */
    public void setDocfactoConfig(XmlConfig config) {
        theDocfactoConfig = config;
    }

    /**
     * Return the docfacto config file
     *
     * @return the Docfacto config file
     * @since 2.1
     */
    public XmlConfig getDocfactoConfig() {
        return theDocfactoConfig;
    }

    /**
     * Get a link to a method or constructor
     *
     * @param doc to process
     * @param currentFile processing
     * @return a xref to a local or external constructor or method, null if not possible
     * @since 2.1
     */
    public Xref getLink(ExecutableMemberDoc doc, File currentFile) {
        Xref xref = theConfig.extern.getExternalXrefLink(doc);

        if(xref != null) {
            // Its an external reference
            return xref;
        }

        // Its not part of JavaDoc
        if(!doc.isIncluded()) {
            return null;
        }

        String ditaPath =
        doc.containingPackage().name()
        .replaceAll("[.]+","/");
        ditaPath += "/r_"+doc.containingClass().typeName()+".dita";

        final File classFile =
        new File(
            DITADocletController.INSTANCE.getDestinationPath(),
            ditaPath);

        xref = new Xref();
        xref.setScope(Scope.LOCAL);
        xref.setFormat(Format.DITA);
        xref.setValue(doc.name());

        xref.setHref(DITAUtils.getRelativePath(currentFile
            .getParent(),
            classFile
            .getAbsolutePath())+"#"+
            doc.containingPackage().name()+"."+
            doc.name()+"/"+
            DITADocletUtils.getIDFor(doc));

        return xref;
    }

    /**
     * Get an Xref is possible
     *
     * @param classDoc to process
     * @param currentFile processing
     * @return if possible a internal or external link, or null
     * @since 2.1
     */
    public Xref getLink(ClassDoc classDoc, File currentFile) {
        Xref xref = theConfig.extern.getExternalXrefLink(classDoc);

        if(xref != null) {
            // Its an external reference
            return xref;
        }

        // Its not part of the JavaDoc set ..
        if(!classDoc.isIncluded()) {
            return null;
        }

        xref = new Xref();
        String ditaPath =
        classDoc.containingPackage().name().replaceAll("[.]+","/");
        ditaPath += "/r_"+classDoc.typeName()+".dita";

        final File classFile =
        new File(DITADocletController.INSTANCE.getDestinationPath(),
            ditaPath);

        xref.setHref(DITAUtils.getRelativePath(currentFile.getParent(),
            classFile
            .getAbsolutePath()));

        xref.setValue(classDoc.typeName());
        xref.setScope(Scope.LOCAL);
        xref.setFormat(Format.DITA);


        //        getTypeParameterLinks(type,output);
        return xref;
    }
}
