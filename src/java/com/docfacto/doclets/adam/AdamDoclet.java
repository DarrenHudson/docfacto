/*
 * @author dhudson -
 * Created 18 Jun 2012 : 14:39:50
 */

package com.docfacto.doclets.adam;

import java.io.File;

import com.docfacto.adam.AdamConstants;
import com.docfacto.adam.AdamProcessor;
import com.docfacto.common.DocfactoException;
import com.docfacto.javadoc.JavaDocUtils;
import com.sun.javadoc.DocErrorReporter;
import com.sun.javadoc.Doclet;
import com.sun.javadoc.LanguageVersion;
import com.sun.javadoc.RootDoc;

/**
 * Main entry point for the Doclet. JavaDoc will call the <code>start</code>
 * method
 * 
 * This Doclet can also be called from the command line, which will enter
 * through the <code>main</code> method
 * 
 * @author dhudson - created 26 Jun 2012
 * @since 2.0
 */
public class AdamDoclet extends Doclet {

    /**
     * Current product name {@value}
     */
    public static final String PRODUCT_NAME = "Adam";

    /**
     * Constructor
     * 
     * @since 2.0
     */
    public AdamDoclet() {
    }

    /**
     * @see com.sun.javadoc.Doclet#start
     */
    public static boolean start(RootDoc root) {
        try {
            final AdamProcessor processor = new AdamProcessor(root);
            return processor.process();
        }
        catch (DocfactoException ex) {
            return false;
        }
    }

    /**
     * @see com.sun.javadoc.Doclet#validOptions
     */
    public static boolean validOptions(String[][] options,
    DocErrorReporter errorReporter) {
        for (final String[] option:options) {
            if (JavaDocUtils.CONFIG_OPTION.equals(option[0])) {
                final File configFile = new File(option[1]);

                if (!configFile.canRead()) {
                    errorReporter.printError(
                        JavaDocUtils.CONFIG_OPTION+
                            " Unable to read config file. ["+
                            configFile.getPath()+"]");
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @see com.sun.javadoc.Doclet#optionLength
     */
    public static int optionLength(String option) {
        if (JavaDocUtils.CONFIG_OPTION.equals(option)) {
            return 2;
        }

        if (option.equals(JavaDocUtils.OUTPUT_OPTION)) {
            return 2;
        }
        
        if(option.equals(AdamConstants.RECOMMEND_OPTION)) {
            return 1;
        }
        
        return 0;
    }

    /**
     * Return the 1.5 JVM as we are dealing with generics
     * 
     * @return 1.5 so that we can have Generics
     * @since 2.1
     */
    public static LanguageVersion languageVersion() {
        return LanguageVersion.JAVA_1_5;
    }

    /**
     * Command line entry point
     * 
     * @param args doclet arguments, these should include
     * <dl>
     * <dt>-doclet com.docfacto.doclets.adam.AdamDoclet</dt>
     * <dt>-docletpath docfacto.jar</dt>
     * <dt>-config ...</dt>
     * <dt>@packages</dt>
     * </dl>
     * @since 2.0
     */
    public static void main(String[] args) {
        com.sun.tools.javadoc.Main.execute(PRODUCT_NAME,args);
    }
}
