package com.docfacto.doclets.adam.adamlets;

import com.docfacto.adam.AdamResults;
import com.docfacto.adam.Adamlet;
import com.docfacto.config.generated.Severity;

/**
 * Simple Adamlet to make sure that the first sentence starts with a capital
 * letter
 * 
 * @author dhudson - created 10 Sep 2013
 * @since 2.4
 */
public class CapitaliseAdamlet implements Adamlet {

    /**
     * @see com.docfacto.adam.Adamlet#processComment(java.lang.String)
     */
    @Override
    public AdamResults processComment(String comment) {
        // New result set
        final AdamResults results = new AdamResults();

        if (comment.isEmpty()) {
            // No point grumbling as Adam would have already
            return results;
        }

        char first = comment.charAt(0);

        if (Character.isLetter(first)&&Character.isLowerCase(first)) {
            results.addAdamletResult("Comment starts with lower case letter",
                Severity.WARNING);
        }

        return results;
    }

}
