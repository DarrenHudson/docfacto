/*
 * @author dhudson -
 * Created 8 Jul 2012 : 10:50:13
 */

package com.docfacto.doclets.adam.adamlets;

import com.docfacto.adam.AdamResults;
import com.docfacto.adam.Adamlet;
import com.docfacto.config.generated.Severity;

/**
 * Simple AdamLet to check the since tag for numerical correctness and it's
 * within range
 * 
 * @author dhudson - created 8 Jul 2012
 * @since 2.5.4
 */
public class SinceAdamlet implements Adamlet {

    /**
     * @see com.docfacto.adam.Adamlet#processComment(java.lang.String)
     */
    @Override
    public AdamResults processComment(String comment) {
        // New result set
        final AdamResults results = new AdamResults();

        // The since notation is major.minor
        if (comment.isEmpty()) {
            // Adam will grumble if configured to do so, but we could add
            // another one
            results.addAdamletResult("Missing description",Severity.ERROR);
            return results;
        }

        final int index = comment.indexOf(".");

        if (index==-1) {
            results.addAdamletResult("Invalid release number ["+comment+"]",
                Severity.WARNING);
        }
        else {

            try {
                final int major = Integer.parseInt(comment.substring(0,index));
                Integer.parseInt(comment.substring(index+1,comment.length()));
                if (major>2) {
                    results.addAdamletResult("Major release number too high",
                        Severity.WARNING);
                }
            }
            catch (final NumberFormatException ex) {
                results.addAdamletResult(
                    "Invalid release number ["+comment+"]",Severity.WARNING);
            }
        }

        return results;
    }
}
