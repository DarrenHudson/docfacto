package com.docfacto.licensing;

import com.docfacto.swing.gui.BaseEvent;
import com.docfacto.swing.gui.BaseEventListener;
import com.docfacto.swing.gui.BaseStatusBar;

/**
 * @author dhudson
 * 
 */
public class LicenseGeneratorStatusBar extends BaseStatusBar implements
BaseEventListener {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor
     * 
     * @param controller
     */
    LicenseGeneratorStatusBar(LicenseGeneratorController controller) {
        controller.addListener(this);
    }

    /**
     * @see com.docfacto.swing.gui.BaseEventListener#handleEvent(com.docfacto.swing.gui.BaseEvent)
     */
    @Override
    public void handleEvent(BaseEvent ev) {
        final int action = ((LicenseGeneratorEvent)ev).getAction();
        switch (action) {
        case LicenseGeneratorEvent.LICENSE_GENERATED:
            setLeftMessage("Licence File Generated");
            break;
        case LicenseGeneratorEvent.MESSAGE_EVENT:
            final LicenseGeneratorStatusEvent event =
            (LicenseGeneratorStatusEvent)ev;
            setLeftMessage(event.getMessage());
        }
    }

}