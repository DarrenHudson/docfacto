package com.docfacto.licensing;

import javax.swing.JMenu;
import javax.swing.JMenuBar;

/**
 * @author dhudson
 *
 */
public class LicenseGeneratorMenuBar extends JMenuBar {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    private final LicenseGeneratorController theController;

    /**
     * Constructor
     * @param controller
     */
    LicenseGeneratorMenuBar(LicenseGeneratorController controller){
        theController = controller;
        add(createFileMenu());
    }

    /**
     * createFileMenu
     * @return
     */
    private JMenu createFileMenu() {

        final JMenu fileMenu = new JMenu("File");
        fileMenu.add(theController.getExitAction());
        return fileMenu;
    }
}