package com.docfacto.licensing;

import java.awt.event.ActionEvent;

import javax.swing.Icon;

import com.docfacto.swing.gui.BaseAction;
import com.docfacto.swing.gui.BaseController;

/**
 * @author dhudson
 * 
 */
public class LicenseGeneratorAction extends BaseAction {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor
     * 
     * @param text
     * @param icon
     * @param event
     * @param controller
     */
    public LicenseGeneratorAction(String text,Icon icon,int event,
    BaseController controller) {
        super(text,icon,event,controller);
    }

    /**
     * @see com.docfacto.swing.gui.BaseAction#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        getController()
        .processEvent(new LicenseGeneratorEvent(this,getEvent()));
    }

}