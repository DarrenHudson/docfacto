package com.docfacto.licensing;

/**
 * @author dhudson
 * 
 */
public class LicenseGenerator {

    private static final String APPLICATION_NAME =
    "Docfacto Licence Generator";

    /**
     * Constructor
     */
    public LicenseGenerator() {
        new LicenseGeneratorFrame(new LicenseGeneratorController(
            APPLICATION_NAME,
        "1"));
    }

    /**
     * main
     * 
     * @param args
     */
    public static void main(String[] args) {
        // Kickstart the UI
        new LicenseGenerator();
    }
}
