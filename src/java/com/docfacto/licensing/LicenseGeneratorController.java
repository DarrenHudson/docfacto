package com.docfacto.licensing;

import com.docfacto.swing.gui.BaseController;
import com.docfacto.swing.gui.BaseUtils;

/**
 * Controller for the license generator
 * 
 * @author dhudson
 * 
 */
public class LicenseGeneratorController extends BaseController {

    private LicenseGeneratorAction theExitAction;

    /**
     * Constructor
     * 
     * @param applicationName application name
     * @param applicationRelease application release
     * @since 2.0
     */
    public LicenseGeneratorController(String applicationName,
    String applicationRelease) {
        super(applicationName,applicationRelease);
        createActions();
    }

    /**
     * createActions
     */
    private void createActions() {
        theExitAction =
            new LicenseGeneratorAction("Exit",
                BaseUtils.loadImageIcon("Blank16.gif"),
                LicenseGeneratorEvent.EXIT,this);
    }

    /**
     * getExitAction
     * 
     * @return
     */
    LicenseGeneratorAction getExitAction() {
        return theExitAction;
    }

    /**
     * Display a message at in the status bar
     * 
     * @param message to display in the status bar
     * @since 2.5
     */
    public void sendMessage(String message) {
        processEvent(new LicenseGeneratorStatusEvent(this,message));
    }
}
