package com.docfacto.licensing;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import com.docfacto.swing.gui.BaseController;
import com.docfacto.swing.gui.BaseEvent;
import com.docfacto.swing.gui.BaseFrame;
import com.docfacto.swing.gui.BaseUtils;

/**
 * @author dhudson
 * 
 */
public class LicenseGeneratorFrame extends BaseFrame {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    private LicenseGeneratorPanel thePanel;

    /**
     * Constructor
     * 
     * @param controller
     */
    public LicenseGeneratorFrame(BaseController controller) {
        super(controller);
        layoutGui();
        setTitle(controller.getAppName());
        setVisible(true);
    }

    /**
     * layoutGui
     */
    private void layoutGui() {
        final Container cp = getContentPane();
        final GridBagConstraints gBC = new GridBagConstraints();
        cp.setLayout(new GridBagLayout());

        setJMenuBar(new LicenseGeneratorMenuBar(
            (LicenseGeneratorController)getController()));

        gBC.gridx = 0;
        gBC.gridy = 0;
        gBC.insets = new Insets(2,2,2,2);
        gBC.anchor = GridBagConstraints.NORTHWEST;
        gBC.fill = GridBagConstraints.BOTH;
        gBC.weightx = 0.5;
        gBC.weighty = 0.5;

        thePanel =
        new LicenseGeneratorPanel(
            (LicenseGeneratorController)getController());
        cp.add(thePanel,gBC);
        gBC.gridy += 4;
        gBC.gridheight = 1;
        gBC.weightx = 1.0;
        gBC.weighty = 0.0;

        cp.add(new LicenseGeneratorStatusBar(
            (LicenseGeneratorController)getController()),gBC);

        pack();

        BaseUtils.centreFrame(getController().getFrame());
    }

    /**
     * @see com.docfacto.swing.gui.BaseFrame#handleEvent(com.docfacto.swing.gui.BaseEvent)
     */
    @Override
    public void handleEvent(BaseEvent ev) {

        if (((LicenseGeneratorEvent)ev).isExit()) {
            // To this for now, probably will have dirty flags set somewhere.
            closeFrame();
        }
    }

    /**
     * @see com.docfacto.swing.gui.BaseFrame#closeFrame()
     */
    @Override
    public void closeFrame() {
        System.exit(0);
    }

}
