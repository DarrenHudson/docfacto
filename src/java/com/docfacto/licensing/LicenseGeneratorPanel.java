package com.docfacto.licensing;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import com.docfacto.config.generated.Docfacto;
import com.docfacto.config.generated.Licence;
import com.docfacto.config.generated.ObjectFactory;
import com.docfacto.licensor.CryptoFactory;
import com.docfacto.licensor.Products;
import com.docfacto.swing.gui.BasePanel;

/**
 * This is the UI that allows License Files to be generated.
 * 
 * @author dhudson
 * @since 2.0
 */
public class LicenseGeneratorPanel extends BasePanel {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    private final LicenseGeneratorController theController;

    private JTextField theLicenseeBox;
    private JTabbedPane theTabbedPanel;
    private JButton theGenerateButton;


    /**
     * Constructor
     * 
     * @param controller
     */
    LicenseGeneratorPanel(LicenseGeneratorController controller) {
        theController = controller;
        layoutPanel();
    }

    /**
     * layoutPanel
     */
    private void layoutPanel() {

        final GridBagConstraints gBC = getBaseConstraints();

        gBC.anchor = GridBagConstraints.NORTHWEST;
        gBC.weightx = 1;
        gBC.weighty = 0;
        gBC.gridx = 0;
        gBC.gridy = 0;
        gBC.fill = GridBagConstraints.HORIZONTAL;

        JLabel label =
            new JLabel(
                "<html><b>Generator for release 2.5 and above...</b></html>");
        add(label,gBC);

        gBC.gridy++;

        label = new JLabel("Licensee Name :");
        add(label,gBC);

        gBC.gridx++;
        theLicenseeBox = new JTextField(30);
        add(theLicenseeBox,gBC);

        gBC.gridx = 0;
        gBC.gridy++;
        gBC.gridwidth = 2;

        theTabbedPanel = new JTabbedPane();

        for (Products product:Products.values()) {
            theTabbedPanel.add(product.toString(),
                new LicenseGeneratorProductTab(theController,product));
        }

        add(theTabbedPanel,gBC);

        gBC.gridy++;
        gBC.gridx = 0;
        gBC.gridwidth = 2;

        theGenerateButton = new JButton("Generate");
        theGenerateButton.addActionListener(new ActionListener() {
            /**
             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                generateLicense();
            }
        });

        gBC.gridx++;
        add(theGenerateButton,gBC);
    }

    /**
     * Return the text from the licensee text field
     * 
     * @return
     * @since 2.0
     */
    private String getLicensee() {
        return theLicenseeBox.getText().trim();
    }

    /**
     * validateValues
     * 
     * @return false if ant product panel is invalid
     */
    private boolean validateValues() {

        if(getLicensee().isEmpty()) {
            theController.sendMessage("Licensee empty..");
            return false;
        }
        
        for (Component comp:theTabbedPanel.getComponents()) {
            LicenseGeneratorProductTab productTab =
                (LicenseGeneratorProductTab)comp;
            if (!productTab.validateValues()) {
                // Stop at the first one
                return false;
            }
        }

        // All good
        return true;
    }

    /**
     * generateLicense
     */
    private void generateLicense() {

        if (!validateValues()) {
            return;
        }

        theController.sendMessage("");

        ObjectFactory of = new ObjectFactory();
        // Create the root note
        Docfacto docfacto = of.createDocfacto();
        Licence licence = of.createLicence();
        licence.setProducts(of.createProducts());
        docfacto.setLicence(licence);

        licence.setLicensee(getLicensee());

        for (Component comp:theTabbedPanel.getComponents()) {
            LicenseGeneratorProductTab productTab =
                (LicenseGeneratorProductTab)comp;
            productTab.populate(licence);
        }

        final JFileChooser chooser = new JFileChooser(new File("."));
        chooser.setMultiSelectionEnabled(false);
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setDialogTitle("Select directory to save licence file");

        final int option = chooser.showOpenDialog(theController.getFrame());

        if (option==JFileChooser.APPROVE_OPTION) {
            final String filename =
                chooser.getSelectedFile().getPath()
                    +File.separator
                    +"License.xml";
            try {
                // This is a bit odd so lets explain it..
                // The Key is the whole XML file, and the XML file is just for
                // show.
                JAXBContext ctx =
                    JAXBContext
                        .newInstance("com.docfacto.config.generated");
                Marshaller marshaller = ctx.createMarshaller();
                ByteArrayOutputStream os = new ByteArrayOutputStream();

                marshaller.marshal(docfacto,os);
                
                // Lets create the real Key ...
                licence.setKey(CryptoFactory.generateKey(os.toByteArray()));

                // Lets make is pretty
                marshaller.setProperty(
                    Marshaller.JAXB_FORMATTED_OUTPUT,
                    Boolean.TRUE);
                marshaller.marshal(docfacto,new File(filename));
                theController.sendMessage("Licence file generated");
            }
            catch (final Exception ex) {
                System.out.println(ex);
                theController.sendMessage(ex.getLocalizedMessage());
            }
        }
    }
}
