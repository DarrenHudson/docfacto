package com.docfacto.licensing;

/**
 * @author dhudson
 *
 */
public class LicenseGeneratorStatusEvent extends LicenseGeneratorEvent {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    private final String theMessage;

    /**
     * Constructor
     * @param source
     * @param message
     */
    public LicenseGeneratorStatusEvent(Object source, String message) {
        super(source, MESSAGE_EVENT);
        theMessage = message;
    }

    /**
     * getMessage
     * @return the message
     */
    public String getMessage() {
        return theMessage;
    }

}
