package com.docfacto.licensing;

import com.docfacto.swing.gui.BaseEvent;

/**
 * @author dhudson
 *
 */
public class LicenseGeneratorEvent extends BaseEvent {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    public static final int LICENSE_GENERATED = 3;

    public static final int MESSAGE_EVENT = 4;

    /**
     * Constructor
     * @param source
     * @param action
     */
    public LicenseGeneratorEvent(Object source, int action) {
        super(source, action);
    }

    /**
     * isExit
     * @return true, if the action is exit
     */
    @Override
    public boolean isExit() {
        return (getAction() == EXIT);
    }
}
