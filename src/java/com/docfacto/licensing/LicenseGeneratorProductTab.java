package com.docfacto.licensing;

import java.awt.GridBagConstraints;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.docfacto.common.XMLUtils;
import com.docfacto.config.generated.Address;
import com.docfacto.config.generated.AddressScheme;
import com.docfacto.config.generated.Licence;
import com.docfacto.config.generated.Product;
import com.docfacto.licensor.Products;
import com.docfacto.swing.gui.BasePanel;

/**
 * License Panel for a given product.
 * 
 * @author dhudson - created 30 Dec 2013
 * @since 2.5
 */
public class LicenseGeneratorProductTab extends BasePanel {

    /**
     * @serial
     */
    private static final long serialVersionUID = 1L;

    private final LicenseGeneratorController theController;
    private final Products theProduct;
    private JTextField theIPAddress;
    private JTextField theMacAddress;
    private JTextField theRestriction;
    private JTextField theExpiryDate;

    private JCheckBox isRequiredBox;

    private final SimpleDateFormat theDateFormat;

    /**
     * Constructor.
     * 
     * @param controller Licence controller
     * @param product Product
     * @since 2.5
     */
    public LicenseGeneratorProductTab(LicenseGeneratorController controller,
    Products product) {
        theProduct = product;
        theController = controller;
        theDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        theDateFormat.setLenient(false);
        layoutPanel();
        setDefaults();
    }

    /**
     * Layout the panel
     * 
     * @since 2.5
     */
    private void layoutPanel() {
        GridBagConstraints gBC = getBaseConstraints();

        gBC.anchor = GridBagConstraints.NORTHWEST;
        gBC.weightx = 1;
        gBC.weighty = 0;
        gBC.gridx = 0;
        gBC.gridy = 0;
        gBC.fill = GridBagConstraints.HORIZONTAL;

        JLabel label =
            new JLabel("<html>Product Name <b>"+theProduct+"</b></html>");

        add(label,gBC);

        gBC.gridy++;

        isRequiredBox = new JCheckBox("Is Required?",true);
        add(isRequiredBox,gBC);

        gBC.gridy++;
        add(new JLabel("IP Address"),gBC);

        gBC.gridx++;

        theIPAddress = new JTextField(30);
        theIPAddress
            .setToolTipText("Use 127.0.0.1 for unrestricted IP, or a comma delimited list");
        add(theIPAddress,gBC);

        gBC.gridx = 0;
        gBC.gridy++;

        add(new JLabel("Mac Address"),gBC);

        gBC.gridx++;
        theMacAddress = new JTextField(30);
        theMacAddress
            .setToolTipText("Use 00:00:00:00:00:00 notation, or a comma delimited list");
        add(theMacAddress,gBC);

        gBC.gridx = 0;
        gBC.gridy++;

        add(new JLabel("Restriction. (0 for unlimited)"),gBC);

        gBC.gridx++;

        theRestriction = new JTextField(10);
        add(theRestriction,gBC);

        gBC.gridx = 0;
        gBC.gridy++;

        add(new JLabel("Expiry Date"),gBC);
        gBC.gridx++;

        theExpiryDate = new JTextField(20);
        add(theExpiryDate,gBC);
    }

    /**
     * setDefaults
     */
    private void setDefaults() {
        theIPAddress.setText("127.0.0.1");

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH,1);
        theExpiryDate.setText(theDateFormat.format(cal.getTime()));

        theRestriction.setText("0");
    }

    /**
     * getIPAddress
     * 
     * @return
     */
    String getIPAddress() {
        return theIPAddress.getText();
    }

    /**
     * getMacAddress
     * 
     * @return
     */
    String getMacAddress() {
        return theMacAddress.getText();
    }

    /**
     * Get the restriction
     * 
     * @return
     */
    String getRestriction() {
        return theRestriction.getText();
    }

    /**
     * getExipryDate
     * 
     * @return
     */
    private String getExpiryDate() {
        return theExpiryDate.getText();
    }

    /**
     * validateValues
     * 
     * @return
     */
    boolean validateValues() {
        if (!isRequiredBox.isSelected()) {
            return true;
        }

        try {
            Date date = theDateFormat.parse(getExpiryDate());

            if (System.currentTimeMillis()>date.getTime()) {
                theController.sendMessage(theProduct+"Date already expired");
                return false;
            }
        }
        catch (ParseException ex) {
            theController.sendMessage(theProduct+"Invalid date format");
            return false;
        }

        try {
            Integer.parseInt(theRestriction.getText());
        }
        catch (NumberFormatException ex) {
            theController.sendMessage(theProduct+"Invalid restriction");
            return false;
        }

        // All good
        return true;
    }

    /**
     * populate
     * 
     * @param details
     */
    void populate(Licence licence) {

        if (!isRequiredBox.isSelected()) {
            return;
        }

        Product product = new Product();
        product.setName(theProduct.toString());

        try {
            Date date = theDateFormat.parse(getExpiryDate());

            product.setExpiryDate(XMLUtils.asXMLGregorianCalendar(date));
            product.setRestriction(new Integer(getRestriction()));

            Address address = new Address();

            if (!getIPAddress().trim().equals("")) {
                // Its using a IP Address
                address.setScheme(AddressScheme.IPADDRESS);
                address.setValue(getIPAddress());
            }
            else {
                // Its using Mac Addressing
                address.setScheme(AddressScheme.MAC);
                address.setValue(getMacAddress());
            }

            product.setAddress(address);

            licence.getProducts().getProducts().add(product);
        }
        catch (ParseException ignore) {
            // This has been validated in the validateValues method
        }
    }

    /**
     * copy
     * 
     * @param panel
     */
    // void copy(LicenseGeneratorProductPanel panel) {
    // theExpiryDate.setText(panel.getExpiryDate());
    // theNoOfUsers.setText(panel.getNoOfUsers());
    // theMacAddress.setText(panel.getMacAddress());
    // theIPAddress.setText(panel.getIPAddress());
    // }
}
