package com.docfacto.links;

import java.util.LinkedHashMap;

import com.docfacto.common.DocfactoException;

public class LinksShortenedPathToCompletePathResolver {
	
	private static final String OPEN_DELIMITER = "${";
    private static final String CLOSE_DELIMITER = "}";
    
    // It must be a linked hash map as the order matters
	private LinkedHashMap<String, String> thePathNameToPathValueMap;
	
	public LinksShortenedPathToCompletePathResolver(LinkedHashMap<String, String> pathNameToPathValueMap) {
		this.thePathNameToPathValueMap = pathNameToPathValueMap;
	}

	public String resolvePath(String path) throws DocfactoException {
		return resolvePathUpToPathName(path, null);
	}
	
	public String resolvePathUpToPathName(String path, String lastPathName) throws DocfactoException {
		if (!path.startsWith(OPEN_DELIMITER)) {
			// does not start with opening delimiter, so it is a normal value, just return it.
			return path;
		}
		
        int endPos = path.indexOf(CLOSE_DELIMITER);

        // Must have a matching bracket for substitution to be processed.
        if (endPos==-1) {
            return path;
        }
        
        // Determine the name of path to be substituted
        String pathName = path.substring(OPEN_DELIMITER.length(),endPos).trim();
	
        // Path name is empty so can not resolve
        if (pathName.isEmpty()) {
            throw new DocfactoException(
                "Unable to perform path subsitution as path name is empty ["+path+"]");
        }
        
        LinkedHashMap<String, String> pathNameToPathValueMap;
        
        if (lastPathName == null) {
        	pathNameToPathValueMap = thePathNameToPathValueMap;
        }
        else {
        	pathNameToPathValueMap = getPathNameToPathValueMapUpToPathName(lastPathName);
        }
        
        String pathValue = pathNameToPathValueMap.get(pathName);
        
        // Path is not defined so can not resolve
        if (pathValue == null) {
        	if (lastPathName == null)
        		throw new DocfactoException("Unable to find specified path " + OPEN_DELIMITER + pathName + CLOSE_DELIMITER);
        	else 
        		throw new DocfactoException("Path " + OPEN_DELIMITER + pathName + CLOSE_DELIMITER + " is not defined before path [" + lastPathName + "]");
        }
        
        // Replace the path name with the path
        StringBuilder builder = new StringBuilder(path);
        builder.replace(0,endPos+1,pathValue);
        
        // Continue recursing
        return resolvePathUpToPathName(builder.toString(), pathName);
	}

	private LinkedHashMap<String, String> getPathNameToPathValueMapUpToPathName(String targetPathName) {
		
		LinkedHashMap<String, String> newMap = new LinkedHashMap<String, String>();
		
		for (String pathName : thePathNameToPathValueMap.keySet()) {
			if (pathName.equals(targetPathName)) {
				return newMap;
			}
			else {
				String pathValue = thePathNameToPathValueMap.get(pathName);
				newMap.put(pathName, pathValue);
			}
		}
		
		return newMap;
	}
}
