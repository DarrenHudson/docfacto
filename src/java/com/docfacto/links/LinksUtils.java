package com.docfacto.links;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.docfacto.common.IOUtils;

/**
 * Utility class for Links
 *
 * @author dhudson - created 31 May 2013
 * @since 2.3
 */
public class LinksUtils {
        
    /**
     * Constructor.
     * @since 2.3
     */
    private LinksUtils() {    
    }
    
    /**
     * Based on the file, see if we can work out the doc type.
     * 
     * As of 2.3.0, there is only one doc type of {@code DocumentType.XML}
     * 
     * @param path of the document
     * @return a {@code DocumentType} for a given file
     * @since 2.3
     */
//    public static final DocumentType getDocumentTypeFor(String path) {
//        return DocumentType.XML;
//    }

    /**
     * Based on the file, see if we can work out the doc type.
     * 
     * As of 2.3.0, there is only one doc type of {@code DocumentType.XML}
     * 
     * @param path of the document
     * @return a {@code DocumentType} for a given file
     * @since 2.3
     */
//    public static DocumentType getDocumentTypeForFile(String docFile) {
//        return DocumentType.XML;
//    }
    
    /**
     * Check to see if we can work out the SourceType given the file
     * 
     * @param path of the source file
     * @return a {@code SourceType} for the given file
     * @since 2.3
     */
//    public static final FileType getSourceTypeForFile(String path) {
//        if(path == null) {
//            return null;
//        }
//        
//        String extn = IOUtils.extension(new File(path));
//        if(extn == null) {
//            return null;
//        }
//        
//        // This may return null if not found
//        return FILE_TYPE_EXTENSIONS.get(extn);
//    }

    /**
     * Get the SourceType for a given source type string
     * 
     * @param sourceType the source type string to get the source type for
     * @return a {@code SourceType} for the given source type string
     * @since 2.4
     */
//    public static FileType getSourceTypeFor(String sourceType) {
//        return FILE_TYPE_EXTENSIONS.get(sourceType.toLowerCase());
//    }    
}
