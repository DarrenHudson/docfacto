package com.docfacto.links;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.docfacto.common.DocfactoException;
import com.docfacto.config.generated.Severity;
import com.docfacto.output.AbstractOutputProcessor;
import com.docfacto.output.generated.Position;
import com.docfacto.output.generated.Result;
import com.docfacto.output.generated.Rule;
import com.docfacto.output.generated.Statistic;

/**
 * Links Output processor. Contains all of the statistics for a links run
 * 
 * @author dhudson - created 12 Apr 2013
 * @since 2.2
 */
public class LinksOutputProcessor extends AbstractOutputProcessor {

    private static final String XML_LOCATION =
        "com/docfacto/links/resources/LinksOutput.xml";
    
    private Map<String, PackageStatistic> packageStatisticsMap;

    /**
     * Create a new instance of <code>LinksOutputProcessor</code>.
     * 
     * Load the internal LinksOutput.xml file, which contains the template
     * statistics.
     * 
     * @param theLinksConfiguration the links configuration where the links paths and settings are set.
     * @throws DocfactoException if unable to load the internal LinksOutput.xml file.
     */
    public LinksOutputProcessor(LinksConfiguration theLinksConfiguration, RestrictionCounter restrictionCounter) throws DocfactoException {
        super(restrictionCounter);
        packageStatisticsMap = new LinkedHashMap<String, PackageStatistic>();
        
        for (Statistic statistic:getOutput().getStatistics().getStatistics()) {
            PackageStatistic packageStatistic = new PackageStatistic();
            packageStatistic.setRule(statistic.getRule());
            packageStatistic.setValue(statistic.getValue());
            packageStatisticsMap.put(statistic.getRule().getKey(), packageStatistic);
        }
    }

    /**
     * @see com.docfacto.config.BaseXmlConfig#getDefaultFileName()
     */
    @Override
    public String getDefaultFileName() {
        return XML_LOCATION;
    }

    /**
     * Add a new {@code Result} to the output
     * 
     * @param fileName current file processing
     * @param filePackage the package of the file being processed
     * @param lineNo if -1 then no position information will be recorded
     * @param severity
     * @param message 
     * @param ruleKey of the rule or statistic
     * @return the newly created result
     * @since 2.2
     */
    public Result addResult(String fileName,String filePackage,int lineNo,Severity severity,
    String message,String ruleKey) {
        String resolvedFileName = LinksPathsManager.INSTANCE.getShortenedPathFromAbsolutePath(fileName);
        String resolvedFilePackage = LinksPathsManager.INSTANCE.getShortenedPathFromAbsolutePath(filePackage);
        
        final Result result = new Result();
        final Rule rule = new Rule();
        rule.setName(message);
        rule.setKey(ruleKey);
        
        rule.setMessage(message+" ["+resolvedFileName+"]");
        
        result.setRule(rule);

        result.setSeverity(severity.name());

        if (lineNo!=-1) {
            final Position position = new Position();
            position.setColumn(0);
            position.setLine(lineNo);
            position.setFile(fileName);
            position.setPackageName(filePackage);
            position.setResolvedFile(resolvedFileName);
            position.setResolvedPackage(resolvedFilePackage);
            result.setPosition(position);
        }

        // Add to the stats
        incrementStatistic(ruleKey,severity);
        incrementPackageStatistic(resolvedFilePackage, ruleKey, severity);
        
        addResult(result);
        return result;
    }

    public List<PackageStatistic> getPackageStatistics() {
        List<PackageStatistic> packageStatisticsList = new ArrayList<PackageStatistic>(packageStatisticsMap.values());
        return packageStatisticsList;
    }
    
    private void incrementPackageStatistic(String filePackage, PackageStatistic packageStatistic) {
        if (packageStatistic != null) {
            if (packageStatistic.hasValueForPackage(filePackage)) {
                int initialStatisticValueForPackage = packageStatistic.getValueForPackage(filePackage);
                packageStatistic.setValueForPackage(filePackage, initialStatisticValueForPackage+1);
            }
            else
                packageStatistic.setValueForPackage(filePackage, 1);
            
            packageStatistic.setValue(packageStatistic.getValue()+1);
        }
    }
    
    /**
     * TODO - Method Title
     * <p>
     * TODO - Method Description
     * </p>
     * @param filePackage
     * @param ruleKey
     * @param severity
     * @since n.n
     */
    public void incrementPackageStatistic(String filePackage,String ruleKey,Severity severity) {
        String resolvedFilePackage = LinksPathsManager.INSTANCE.getShortenedPathFromAbsolutePath(filePackage);
        
        PackageStatistic packageStatistic = packageStatisticsMap.get(ruleKey);
        incrementPackageStatistic(resolvedFilePackage, packageStatistic);
        
        switch (severity) {
        case ERROR:
            incrementPackageStatistic(resolvedFilePackage, packageStatisticsMap.get(LinksStatKey.LINKS_ERROR));
            break;

        case WARNING:
            incrementPackageStatistic(resolvedFilePackage, packageStatisticsMap.get(LinksStatKey.LINKS_WARNING));
            break;

        case INFO:
            incrementPackageStatistic(resolvedFilePackage, packageStatisticsMap.get(LinksStatKey.LINKS_INFO));
            break;
        }
    }

    /**
     * TODO - Method Title
     * <p>
     * TODO - Method Description
     * </p>
     * @param filePackage
     * @param sourceLinkMatchedKey
     * @since n.n
     */
    public void incrementPackageStatistic(String filePackage,String ruleName) {
        String resolvedFilePackage = LinksPathsManager.INSTANCE.getShortenedPathFromAbsolutePath(filePackage);
        if (packageStatisticsMap.containsKey(ruleName)) {
            incrementPackageStatistic(resolvedFilePackage, packageStatisticsMap.get(ruleName));
        }
    }

    /**
     * TODO - Method Title
     * <p>
     * TODO - Method Description
     * </p>
     * @param package1
     * @param codeLinesLinkedToDoc
     * @param numberOfCodeLines
     * @since n.n
     */
    public void addToPackageStatistic(String filePackage, String ruleName, int addition) {
        String resolvedFilePackage = LinksPathsManager.INSTANCE.getShortenedPathFromAbsolutePath(filePackage);
        PackageStatistic packageStatistic = packageStatisticsMap.get(ruleName);
        if (packageStatistic!=null) {
            if (packageStatistic.hasValueForPackage(resolvedFilePackage)) {
                packageStatistic.setValueForPackage(resolvedFilePackage,packageStatistic.getValueForPackage(filePackage)+addition);
            }
            
            packageStatistic.setValue(addition+packageStatistic.getValue());
        }
    }
}
