package com.docfacto.links;

/**
 * An interface for Document Processor classes.
 *<P>
 * This interface class has a method for processing document files.
 *
 * @author damonli - created Jun 10, 2013
 * @since 2.2
 */
public interface IDocumentProcessor {

    /**
     * Process document files
     * @since 2.2
     */
    public void processDocumentFiles(RestrictionCounter restrictionCounter);
}
