package com.docfacto.links;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.docfacto.common.ClassLoaderUtils;
import com.docfacto.common.DocfactoException;
import com.docfacto.config.XmlConfig;
import com.docfacto.config.generated.Libraries;
import com.docfacto.config.generated.Library;
import com.docfacto.config.generated.Links;
import com.docfacto.links.ant.Doc;
import com.docfacto.links.ant.Source;

/**
 * Class containing the configuration information for the {@code LinksProcessor}
 * 
 * @author dhudson - created 17 Apr 2013
 * @since 2.2
 */
public class LinksConfiguration {

    private boolean isVersionMatching;
    private String theConfigPath;

    private XmlConfig theXmlConfig;
    private boolean isVerbose = true;
    private boolean hasValidated;

    private Doc theDoc;
    private Source theSource;
    
    private String theCurrentWorkingDirectory;

    /**
     * Constructor.
     * @since 2.2
     */
    public LinksConfiguration() {
    }

    /**
     * Set true if version machine is required.
     * 
     * @param value
     * @since 2.2
     */
    public void setIsVersionMatching(boolean value) {
        isVersionMatching = value;
    }

    /**
     * Check to see if version matching is required
     * 
     * @return true if version matching
     * @since 2.2
     */
    public boolean isVersionMatching() {
        return isVersionMatching;
    }

    /**
     * Set the config path, no validation is done at this point. This is done
     * when {@link validate} is called.
     * 
     * @param path to the Docfacto.xml
     * @since 2.2
     */
    public void setConfigPath(String path) {
        theConfigPath = path;
    }

    /**
     * Return the config path
     * 
     * @return config path
     * @since 2.2
     */
    public String getConfigPath() {
        return theConfigPath;
    }

    /**
     * Return the XmlConfig, will only be valid if validate has been called
     * 
     * @return the xml configuration belonging to this links configuration
     * @since 2.2
     */
    public XmlConfig getXmlConfig() {
        return theXmlConfig;
    }

    /**
     * If set to true will detail a lot more information on the stdout channel
     * 
     * @param value
     * @since 2.2
     */
    public void setVerbose(Boolean value) {
        isVerbose = value;
    }

    /**
     * Check to see if verbose mode
     * 
     * @return true if verbose is required
     * @since 2.2
     */
    public boolean isVerbose() {
        return isVerbose;
    }

    /**
     * Set the source, which contains source sets.
     * 
     * @param source from Ant
     * @since 2.3
     */
    public void setSource(Source source) {
        theSource = source;
    }

    /**
     * Return the source from Ant
     * 
     * @return source from Ant
     * @since 2.3
     */
    public Source getSource() {
        return theSource;
    }

    /**
     * Set the Doc, which contains doc sets
     * 
     * @param doc from Ant
     * @since 2.3
     */
    public void setDoc(Doc doc) {
        theDoc = doc;
    }

    /**
     * Return the Doc supplied by Ant
     * 
     * @return the Doc section
     * @since 2.3
     */
    public Doc getDoc() {
        return theDoc;
    }
    
    /**
     * Return whether the configuration has been validated
     * @return whether the configuration is validated
     */
    public boolean isValidated() {
    	return hasValidated;
    }
    
    /**
     * Set the current working directory
     * 
     * @param currentWorkingDirectory the current working directory
     * @since 2.3
     */
    public void setCurrentWorkingDirectory(String currentWorkingDirectory) {
        this.theCurrentWorkingDirectory = currentWorkingDirectory;
    }
    
    public String getCurrentWorkingDirectory() {
		return theCurrentWorkingDirectory;
    }
    
    /**
     * Validate the settings, this should be called before using resolve path or
     * passing the config to the LinksProcessor
     * 
     * @throws DocfactoException if unable to validate the Docfacto.xml
     * @since 2.2
     */
    public void validate() throws DocfactoException {
    	validate(true, true);
    }

    /**
     * Validate the settings, this should be called before using resolve path or
     * passing the config to the LinksProcessor
     * 
     * @param loadLibraries true if loading the library jar files
     * @param loadFileTypeData true if loading the file type data
     * @throws DocfactoException if unable to validate the Docfacto.xml
     * @since 2.2
     */
    public void validate(boolean loadLibraries, boolean loadFileTypeData) throws DocfactoException {

        // Let's try and load the config file..
        theXmlConfig = new XmlConfig(theConfigPath);
        
        /* If the current working directory hasn't been set yet make the base directory 
         * the current working directory 
         */
        if (theCurrentWorkingDirectory == null || theCurrentWorkingDirectory.isEmpty()) {
            File xmlFile = new File(theConfigPath);
            File parent = xmlFile.getParentFile();
            String baseDirString = theXmlConfig.getLinksConfig().getBasedir();
            theCurrentWorkingDirectory = baseDirString;
            File baseDir = new File(parent, baseDirString);
            try {
                new File(baseDir.getCanonicalPath());
            }
            catch (IOException e) {
                throw new DocfactoException("Error calculating base directory [" + baseDirString + "]");
            }
        }
        
        loadPathsFromConfig();
        
        if (loadLibraries)
        	loadLibrariesFromConfig();
        
        if(loadFileTypeData)
        	loadFileTypeDataFromConfig();
        
        hasValidated = true;
        
//        if (isVerbose) {
//            System.out.println("Current working directory "+
//                theCurrentWorkingDirectory.getPath());
//        }

        if (theSource==null&&theDoc==null) {
            //throw new DocfactoException("No source files or doc files to process");
        }

        if (theConfigPath==null) {
            throw new DocfactoException("No config path attribute specified");
        }
    }

    private void loadPathsFromConfig() throws DocfactoException {
    	LinksPathsManager.INSTANCE.loadSettingsFromConfig(this);
    }
    
    private void loadLibrariesFromConfig() throws DocfactoException {
		Links linksConfig = getXmlConfig().getLinksConfig();
		Libraries libraries = linksConfig.getLibraries();
		
		if (libraries == null)
			return;
		
		List<Library> libraryList = libraries.getLibraries();
		
		for (Library library : libraryList) {
			String libraryName = library.getName();
			String libraryPath = library.getPath();
			String absoluteLibraryPath = LinksPathsManager.INSTANCE.resolvePathRelativeToBaseDir(libraryPath);
			File dir = new File(absoluteLibraryPath);
			
			if (dir.isDirectory()) {
				ClassLoaderUtils.addJars(dir);
			}
			else if (dir.isFile()) {
				try {
					ClassLoaderUtils.addFile(dir);
				} catch (IOException e) {
					throw new DocfactoException("Error loading jar file for library: " + libraryName + " at path [" + libraryPath + "]");
				}
			}
			else {
				throw new DocfactoException("The library [" + libraryName + "] with path [" + libraryPath + "] is not a valid directory or file");
			}
		}
	}
    
    private void loadFileTypeDataFromConfig() throws DocfactoException {
    	FileTypeProvider.INSTANCE.loadSettingsFromConfig(getXmlConfig());
    }
}
