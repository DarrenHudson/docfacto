package com.docfacto.links;

/**
 * An interface for Source Processor classes.
 *<P>
 * This interface class has a method for processing source files.
 *
 * @author damonli - created Jun 10, 2013
 * @since 2.2
 */
public interface ISourceProcessor {
    
    /**
     * Process source files
     * @since 2.2
     */
    public void processSourceFiles(RestrictionCounter restrictionCounter);
}
