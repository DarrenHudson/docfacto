package com.docfacto.links;

import java.io.IOException;

import com.docfacto.common.DocfactoException;
import com.docfacto.config.generated.Severity;
import com.docfacto.links.ant.Doc;
import com.docfacto.links.ant.DocSet;
import com.docfacto.links.links.ILink;
import com.docfacto.links.scanners.LinkScannerWrapper;

/**
 * A Class for processing documents
 *<p>
 * This class processes the documents for a configuration and sets the appropriate results 
 * and statistics to the output.
 * </p>
 * @author damonli - created Jun 10, 2013
 * @since 2.2
 */
public class DocumentProcessor implements IDocumentProcessor {

    /**
     * The links configuration with the documents to be processed.
     */
    protected final LinksConfiguration theLinksConfiguration;
    /**
     * The output processor where results and statistics are set for
     */
    protected final LinksOutputProcessor theOutput;
    
    private SourceLinksProcessor sourceLinksProcessor;
    
    /**
     * Constructor.
     * 
     * @param theLinksConfiguration the links configuration with the documents to be processed
     * @param theOutput the output processor where results and statistics are set for
     * @since 2.2
     */
    public DocumentProcessor(LinksConfiguration theLinksConfiguration,LinksOutputProcessor theOutput) {
        this.theLinksConfiguration = theLinksConfiguration;
        this.theOutput = theOutput;
        
        sourceLinksProcessor = new SourceLinksProcessor(theLinksConfiguration, theOutput);
    }

    /**
     * @see com.docfacto.links.IDocumentProcessor#processDocumentFiles()
     */
    public void processDocumentFiles(RestrictionCounter restrictionCounter) {
        Doc docDoc = theLinksConfiguration.getDoc();

        if (docDoc==null) {
            // Nothing to do
            return;
        }

        if (theLinksConfiguration.isVerbose()) {
            System.out.println("Processing doc files");
        }

        for (DocSet docSet:docDoc.getDocSets()) {
            for (final String fileName:docSet.getFiles()) {

            	if (restrictionCounter.isRestrictionExceeded()) {
            		theOutput.setMaxFilesReached(true);
            		return;
            	}
            	
            	restrictionCounter.incrementCount();
            	
                String filePackage = PackageRetriever.getPackageForFile(fileName);
                
                theOutput.incrementStatistic(LinksStatKey.DOC_FILES_PROCESSED_KEY);
                theOutput.incrementPackageStatistic(filePackage, LinksStatKey.DOC_FILES_PROCESSED_KEY);
                
                String fileType;
				try {
					fileType = FileTypeProvider.INSTANCE.getTypeForFile(fileName);
				} catch (UnknownFileTypeException e) {
					fileType = FileTypeProvider.INSTANCE.DEFAULT_FILE_TYPE;
				}
				
                LinkScannerWrapper scanner = FileTypeProvider.INSTANCE.getLinkScannerWrapper(fileName, filePackage, fileType);

                try {
                    scanner.scan();

                    addStatisticsToOutput(theOutput, scanner);

                    if (scanner.hasSourceLinks()) {
                        sourceLinksProcessor.processSourceLinks(scanner);
                    }
                    
                    if (scanner.hasLinksOfUnknownType()) {
                        for (ILink unknownLink : scanner.getLinksOfUnknownType()) {
                            theOutput.addResult(unknownLink.getContainingFileUri(),
                                unknownLink.getContainingFilePackage(),
                                unknownLink.getLineNumber(),
                                Severity.WARNING,"Unknown link type, is this a link to doc or source?",
                                LinksStatKey.LINKS_WITH_UNKNOWN_TYPE);
                        }
                    }
                }
                catch (DocfactoException ex) {
                    theOutput.addResult(fileName,filePackage,-1,
                        Severity.WARNING,
                        "Unable to scan doc file due to ["+ex.getMessage()+"]",
                        LinksStatKey.DOC_FILES_IOEXCEPTION);
                }
                catch (IOException ex) {
                    theOutput.addResult(fileName,filePackage,-1,
                        Severity.WARNING,
                        "Unable to scan doc file due to ["+ex.getMessage()+"]",
                        LinksStatKey.DOC_FILES_IOEXCEPTION);
                }
            }
        }
    }

    /**
     * Adds the necessary statistics to a given links output processor with values from a given doc scanner
     * 
     * @param theOutput the links output processors to add statistics to
     * @param scanner the doc scanner to get statistic values from
     * @since 2.4.7
     */
    private void addStatisticsToOutput(LinksOutputProcessor theOutput, LinkScannerWrapper scanner) {
        theOutput.addToStatistic(LinksStatKey.DOC_LINES_KEY, scanner.getNumberOfLines());
        theOutput.addToPackageStatistic(scanner.getPackage(), LinksStatKey.DOC_LINES_KEY, scanner.getNumberOfLines());
    }   
}
