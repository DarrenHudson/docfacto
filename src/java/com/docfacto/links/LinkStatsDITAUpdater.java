package com.docfacto.links;

import java.io.File;
import java.io.IOException;

import com.docfacto.common.DocfactoException;
import com.docfacto.dita.RefBody;
import com.docfacto.dita.Reference;
import com.docfacto.dita.ReferenceTopicDocument;
import com.docfacto.dita.Section;
import com.docfacto.dita.ShortDesc;
import com.docfacto.dita.SimpleTable;
import com.docfacto.dita.SimpleTableRow;
import com.docfacto.dita.Title;
import com.docfacto.output.generated.Statistic;

/**
 * Generate the DITA file for the link statistics
 *
 * @author dhudson - created 24 Apr 2013
 * @since 2.2
 */
public class LinkStatsDITAUpdater {

    public LinkStatsDITAUpdater(String outputFilePath) {
        try {
            LinksOutputProcessor processor = new LinksOutputProcessor(null, null);

            final ReferenceTopicDocument refDoc = new ReferenceTopicDocument();

            final Reference reference = refDoc.getRootNode();

            reference.setID("links_statistics");

            reference.addElement(new Title("Links Statistics"));

            reference
                .addElement(new ShortDesc(
                    "<keyword keyref=\"links\" /> can collate a lot of statistics about the links bewteen the code and the documentation."));

            // Create the reference body element
            final RefBody body = new RefBody();

            Section section = new Section();
            body.addElement(section);

            section.addElement(new Title(
                "Below represents a table of the collated statistics"));

            SimpleTable table = new SimpleTable();
            table.addAttribute("relcolwidth","90* 150*");
            
            table.addHeaders("Name","Description");

            for (Statistic statistic:processor.getStatistics()) {
                if (statistic.getRule().getName()!=null) {
                    SimpleTableRow row = new SimpleTableRow();
                    row.addTableEntries(statistic.getRule().getName(),statistic
                        .getRule().getMessage());
                    table.addElement(row);
                }
            }

            section.addElement(table);

            // Add the body to the root node
            reference.addElement(body);

            final File outputFile =
                new File(outputFilePath);

            // save it to disk
            refDoc.save(outputFile.getPath());
            
            System.out.println("Generated "+
            outputFile.getPath());
            
        }
        catch (DocfactoException ex) {
            ex.printStackTrace();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    
    public static void main(String[] args) {
        new LinkStatsDITAUpdater(args[0]);
    }

}
