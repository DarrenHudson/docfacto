package com.docfacto.links;

import java.util.HashMap;
import java.util.Map;

import com.docfacto.output.generated.Statistic;

/**
 * A Statistic which holds the values for packages too
 *<P>
 * A Package Statistic is a Statistic which also holds a map to store the value of the 
 * statistic for packages.
 *
 * @author damonli - created 12 Aug 2013
 * @since 2.4.4
 */
public class PackageStatistic extends Statistic {

    private Map<String, Integer> packageToValueMap;
    
    /**
     * Constructor.
     */
    public PackageStatistic() {
        packageToValueMap = new HashMap<String, Integer>();
    }
    
    /**
     * Gets the value of this statistic for a package
     * <p>
     * Gets the value of this statistic for a package. If a value has not been set for the given
     * package then 0 is returned as default.
     * </p>
     * @param packageName to retrieve the value of the statistic for
     * @return the value of the statistic for the given package
     * @since 2.4.4
     */
    public int getValueForPackage(String packageName) {
        if (this.hasValueForPackage(packageName))
            return packageToValueMap.get(packageName);
        else
            return 0;
    }
    
    /**
     * Sets the value of the statistic for a given package.
     * 
     * @param packageName to set the value for
     * @param value to set for the given package name
     * @since 2.4.4
     */
    public void setValueForPackage(String packageName, int value) {
        packageToValueMap.put(packageName,value);
    }
    
    /**
     * Get whether a value has been set for a package
     * 
     * @param packageName to check whether a value has been set for
     * @return true if a value has been set for the given package, otherwise false
     * @since 2.4.4
     */
    public boolean hasValueForPackage(String packageName) {
        return packageToValueMap.containsKey(packageName);
    }
}
