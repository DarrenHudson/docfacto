package com.docfacto.links;


/**
 * An exception to be thrown when there are problems processing a source/doc link.
 *
 * @author damonli - created Sep 23, 2013
 * @since 2.4.7
 */
public class LinkProcessingException extends Exception {
    
    /**
     * Constructor
     * @since 2.4.7
     */
    public LinkProcessingException() {
        super();
    }
}
