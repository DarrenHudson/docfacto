package com.docfacto.links;

import java.io.IOException;

import com.docfacto.common.DocfactoException;
import com.docfacto.config.generated.Severity;
import com.docfacto.links.ant.Source;
import com.docfacto.links.ant.SourceSet;
import com.docfacto.links.links.ILink;
import com.docfacto.links.scanners.LinkScannerWrapper;

/**
 * A Class for processing source files
 *<P>
 * This class processes the source files for a configuration and sets the appropriate results 
 * and statistics to the output.
 * 
 * @author damonli - created Jun 10, 2013
 * @since 2.2
 */
public class SourceProcessor implements ISourceProcessor {

    protected final LinksConfiguration theLinksConfiguration;
    protected final LinksOutputProcessor theOutput;
    
    private DocLinksProcessor docLinksProcessor;
    
    public SourceProcessor(LinksConfiguration theLinksConfiguration,LinksOutputProcessor theOutput) {
        this.theLinksConfiguration = theLinksConfiguration;
        this.theOutput = theOutput;
        
        docLinksProcessor = new DocLinksProcessor(theLinksConfiguration, theOutput);
    }

    /**
     * @see com.docfacto.links.ISourceProcessor#processSourceFiles()
     */
    public void processSourceFiles(RestrictionCounter restrictionCounter) {
        Source source = theLinksConfiguration.getSource();

        if (source==null) {
            // Nothing to do..
            return;
        }

        if (theLinksConfiguration.isVerbose()) {
            System.out.println("Processing source files");
        }

        for (SourceSet sourceSet:source.getSourceSets()) {
            for (String fileName:sourceSet.getFiles()) {
            	
            	if (restrictionCounter.isRestrictionExceeded()) {
            		theOutput.setMaxFilesReached(true);
            		return;
            	}
            	
            	restrictionCounter.incrementCount();
                String filePackage = PackageRetriever.getPackageForFile(fileName);
                
                theOutput.incrementStatistic(LinksStatKey.SOURCE_FILES_PROCESSED_KEY);
                theOutput.incrementPackageStatistic(filePackage,LinksStatKey.SOURCE_FILES_PROCESSED_KEY);
                
                String fileType;
				try {
					fileType = FileTypeProvider.INSTANCE.getTypeForFile(fileName);
				} catch (UnknownFileTypeException e) {
					fileType = FileTypeProvider.INSTANCE.DEFAULT_FILE_TYPE;
				}
				
                LinkScannerWrapper scanner = FileTypeProvider.INSTANCE.getLinkScannerWrapper(fileName, filePackage, fileType);

                try {
                    scanner.scan();

                    addStatisticsToOutput(theOutput, scanner);
                    
                    if (scanner.hasDocLinks()) {
                        docLinksProcessor.processDocLinks(scanner);
                    }
                    
                    if (scanner.hasLinksOfUnknownType()) {
                        for (ILink unknownLink : scanner.getLinksOfUnknownType()) {
                            theOutput.addResult(unknownLink.getContainingFileUri(),
                                unknownLink.getContainingFilePackage(),
                                unknownLink.getLineNumber(),
                                Severity.WARNING,"Unknown link type, is this a link to doc or source?",
                                LinksStatKey.LINKS_WITH_UNKNOWN_TYPE);
                        }
                    }
                }
                catch (DocfactoException ex) {
                    theOutput.addResult(fileName,filePackage,-1,
                        Severity.WARNING,
                        "Unable to scan file due to ["+ex.getMessage()+"]",
                        LinksStatKey.SOURCE_FILES_IO_EXCEPTION);
                }
                catch (final IOException ex) {
                    theOutput.addResult(fileName,filePackage,-1,
                        Severity.WARNING,
                        "Unable to scan file due to ["+ex.getMessage()+"]",
                        LinksStatKey.SOURCE_FILES_IO_EXCEPTION);
                }
            }
        }
    }
    
    /**
     * Adds the necessary statistics to a given links output processor with values from a given source scanner
     * 
     * @param theOutput the links output processors to add statistics to
     * @param scanner the source scanner to get statistic values from
     * @since 2.4.7
     */
    private void addStatisticsToOutput(LinksOutputProcessor theOutput, LinkScannerWrapper scanner) {
        theOutput.addToStatistic(LinksStatKey.CODE_LINES_KEY, scanner.getNumberOfCodeLines());
        theOutput.addToPackageStatistic(scanner.getPackage(), LinksStatKey.CODE_LINES_KEY, scanner.getNumberOfCodeLines());

        theOutput.addToStatistic(LinksStatKey.SOURCE_LINES_KEY, scanner.getNumberOfLines());
        theOutput.addToPackageStatistic(scanner.getPackage(), LinksStatKey.SOURCE_LINES_KEY, scanner.getNumberOfLines());

        theOutput.addToStatistic(LinksStatKey.COMMENT_LINES_KEY, scanner.getNumberOfCommentLines());
        theOutput.addToPackageStatistic(scanner.getPackage(), LinksStatKey.COMMENT_LINES_KEY, scanner.getNumberOfCommentLines());
    }
}
