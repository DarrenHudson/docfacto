package com.docfacto.links;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.config.XmlConfig;
import com.docfacto.config.generated.Links;
import com.docfacto.config.generated.Path;
import com.docfacto.config.generated.Paths;

/**
 * A class which manages the paths configured in the Docfacto XML Config. This class loads the paths 
 * configured and provides methods to resolve a shortened path to its absolute path and vice versa.
 * @author damonli
 *
 */
public enum LinksPathsManager {

	INSTANCE;
	
	private static final String OPEN_DELIMITER = "\\$\\{";
    private static final String CLOSE_DELIMITER = "\\}";
	
	private LinksConfiguration theLinksConfiguration;
	
	// A map to hold the path values defined by the user
	private LinkedHashMap<String, String> pathNameToPathValueMap;
	
	// A map to hold the absolute path values to make path resolving quicker
	private LinkedHashMap<String, String> pathNameToAbsolutePathValueMap;
	
	private String baseDir;
	private String absoluteBaseDir;
	private String xmlConfigDir;
	
	private LinksShortenedPathToCompletePathResolver shortenedPathResolver;
	
	private LinksPathsManager() {
		pathNameToPathValueMap = new LinkedHashMap<String, String>();
		pathNameToAbsolutePathValueMap = new LinkedHashMap<String, String>();
	}
	
	public void loadSettingsFromConfig(LinksConfiguration theLinksConfiguration) throws DocfactoException {
		this.theLinksConfiguration = theLinksConfiguration;
		
		clearExistingSettings();
		
		String xmlConfigPath = theLinksConfiguration.getConfigPath();
		this.xmlConfigDir = new File(xmlConfigPath).getParent();
		
		XmlConfig xmlConfig = theLinksConfiguration.getXmlConfig();
		Links linksConfig = xmlConfig.getLinksConfig();
		
		this.baseDir = theLinksConfiguration.getCurrentWorkingDirectory();
		
		if (this.baseDir == null || this.baseDir.isEmpty()) {
			this.baseDir = linksConfig.getBasedir();
		}
		
		this.absoluteBaseDir = getAbsoluteBaseDir();
		
		Paths pathsElement = linksConfig.getPaths();
		List<Path> paths = pathsElement.getPaths();
		for (Path path : paths) {
			String name = path.getName();
			
			if (pathNameAlreadyLoaded(name)) {
				throw new DocfactoException("Path name [" + name + "] has been defined twice.");
			}
			String value = path.getValue();
			pathNameToPathValueMap.put(name, value);
		}
		
		shortenedPathResolver = new LinksShortenedPathToCompletePathResolver(pathNameToPathValueMap);
		
		loadAbsolutePaths();
	}
	
	private void clearExistingSettings() {
		absoluteBaseDir = null;
		pathNameToPathValueMap.clear();
		pathNameToAbsolutePathValueMap.clear();
	}

	private boolean pathNameAlreadyLoaded(String name) {
		return pathNameToPathValueMap.containsKey(name);
	}
	
	private void loadAbsolutePaths() throws DocfactoException {
		for (String pathName : pathNameToPathValueMap.keySet()) {
			String pathValue = pathNameToPathValueMap.get(pathName);
			String absolutePathValue = getAbsolutePathFromShortenedPathUpToDefinedPath(pathValue, pathName);
			pathNameToAbsolutePathValueMap.put(pathName, absolutePathValue);
		}
	}

	public String getBaseDir() throws DocfactoException {
		if (baseDir == null) {
			throw new DocfactoException("Base directory is null and has not been loaded from the XML Config yet");
		}
		return this.baseDir;
	}
	
	public String getXmlConfigDir() throws DocfactoException {
		if (xmlConfigDir == null) {
			throw new DocfactoException("XML Config directory is null and has not been loaded yet");
		}
		return xmlConfigDir;
	}
	
	public String getAbsoluteBaseDir() throws DocfactoException {
		if (absoluteBaseDir != null)
			return absoluteBaseDir;
		
		try {
			return new File(xmlConfigDir, baseDir).getCanonicalPath();
		} catch (IOException e) {
			throw new DocfactoException("Error calculating path of base directory.", e);
		}
	}
	
	public String resolvePathRelativeToBaseDir(String path) throws DocfactoException {
		if (path.startsWith(".")) {
			// the path is relative to the base dir
			File baseDir = new File(LinksPathsManager.INSTANCE.getAbsoluteBaseDir());
			try {
				return new File(baseDir, path).getCanonicalPath();
			} catch (IOException e) {
				throw new DocfactoException("Error calculating path [" + path + "]");
			}
		}
		else {
			return path;
		}
	}
	
	public String getAbsolutePathFromShortenedPathUpToDefinedPath(String shortPath, String lastDefinedPath) throws DocfactoException {
		if (shortPath.startsWith(".")) {
			return resolvePathRelativeToBaseDir(shortPath);
		}
		
		String resolvedPath = shortenedPathResolver.resolvePathUpToPathName(shortPath, lastDefinedPath);
		if (resolvedPath.startsWith(".")) {
			resolvedPath = resolvePathRelativeToBaseDir(resolvedPath);
		}
		return resolvedPath;
	}
	
	public String getAbsolutePathFromShortenedPath(String shortPath) throws DocfactoException {
		if (shortPath.startsWith(".")) {
			return resolvePathRelativeToBaseDir(shortPath);
		}
		
		String resolvedPath = shortenedPathResolver.resolvePath(shortPath);
		if (resolvedPath.startsWith(".")) {
			resolvedPath = resolvePathRelativeToBaseDir(resolvedPath);
		}
		return resolvedPath;
	}
	
	public String getShortenedPathFromAbsolutePath(String absolutePath) {
		String shortenedPath = absolutePath;
		
		for (String pathName : pathNameToAbsolutePathValueMap.keySet()) {
			String foundAbsolutePath = pathNameToAbsolutePathValueMap.get(pathName);
			if (absolutePath.startsWith(foundAbsolutePath)) {
				shortenedPath = absolutePath.replaceFirst(foundAbsolutePath, OPEN_DELIMITER + pathName + CLOSE_DELIMITER);
			}
		}
		
		// Haven't changed it, so just get the path which is relative to the base dir
		if (shortenedPath.equals(absolutePath)) {
			try {
				shortenedPath = IOUtils.getRelativePath(getAbsoluteBaseDir(), absolutePath);
			} catch (DocfactoException e) {
				// Failed to get the path relative to the base dir, so just return the original path
				return absolutePath;
			}
		}
		
		return shortenedPath;
	}
}
