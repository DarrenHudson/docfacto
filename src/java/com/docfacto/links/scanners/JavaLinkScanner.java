package com.docfacto.links.scanners;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.docfacto.common.DocfactoException;
import com.docfacto.javadoc.CommentParser;
import com.docfacto.javadoc.TagParser;
import com.docfacto.links.api.ILinkScanner;
import com.docfacto.links.api.ILinkStore;
import com.docfacto.links.links.LinksCommentParser;
import com.docfacto.taglets.LinkTagletConstants;

/**
 * A link scanner which scans java files.
 * 
 * @author damonli
 *
 */
public class JavaLinkScanner implements ILinkScanner {

	private String filter;
	private LinksJavaSourceScanner theJavaSourceScanner;
	
	/**
	 * @see com.docfacto.links.api.ILinkScanner#scan(java.io.InputStream, com.docfacto.links.api.ILinkStore)
	 */
	@Override
	public void scan(InputStream inputStream, ILinkStore linkStore) throws DocfactoException {
		
		theJavaSourceScanner = new LinksJavaSourceScanner(inputStream);
        
        // Only interested in the Link taglet
        theJavaSourceScanner.setFilter(LinkTagletConstants.LINK_TAGLET_NAME);
        
        if (filter != null) {
        	theJavaSourceScanner.setFilter(filter);
        }
        
        try {
			theJavaSourceScanner.scan();
		} catch (IOException e) {
			throw new DocfactoException("I/O error when scanning input stream", e);
		}

        for (CommentParser parser:theJavaSourceScanner.getComments()) {
            for (final TagParser tagParser:parser.getTags(LinkTagletConstants.LINK_TAGLET_NAME)) {
                linkStore.addLink(tagParser.getLineNumber(), tagParser);
            }
        }
	}
	
	/**
	 * Set the tag which is processed and filter out other tags
	 * @param filter the filter tag
	 */
	public void setFilter(String filter) {
		this.filter = filter;
	}
	
	/**
	 * Get the list of comments scanned.
	 * @return the list of comments scanned.
	 */
	public List<LinksCommentParser> getComments() {
		return theJavaSourceScanner.getComments();
	}

	@Override
	public int getNumberOfCodeLines() {
		return theJavaSourceScanner.getNumberOfCodeLines();
	}

	@Override
	public int getNumberOfCommentLines() {
		return theJavaSourceScanner.getTotalNumberOfCommentLines();
	}

	@Override
	public int getNumberOfLines() {
		return theJavaSourceScanner.getNumberOfSource();
	}
}
