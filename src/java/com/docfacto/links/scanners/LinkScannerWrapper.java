package com.docfacto.links.scanners;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.docfacto.common.DocfactoException;
import com.docfacto.javadoc.TagParser;
import com.docfacto.links.api.ILinkScanner;
import com.docfacto.links.api.IUriHandler;
import com.docfacto.links.api.ILinkStore;
import com.docfacto.links.links.ILink;
import com.docfacto.links.links.Link;
import com.docfacto.links.links.LinkType;

/**
 * A class to wrap around a link scanner.
 * 
 * @author dhudson - created 7 Jun 2013
 * @since 2.3
 */
public class LinkScannerWrapper implements ILinkScannerWrapper, ILinkStore {

    private final String theURI;
    private final String thePackage;
    
    private List<ILink> theLinks;
    private List<ILink> theSourceLinks;
    private List<ILink> theDocLinks;
    private List<ILink> theLinksWithUnknownType;
    
    private final IUriHandler uriHandler;
    private final ILinkScanner linkScanner;
    
    /**
     * Constructor.
     * 
     * @param uri the uri of the file to be scanned.
     * @param thePackage the package of the file to be scanned.
     * @since 2.3
     */
    public LinkScannerWrapper(String uri, String thePackage, IUriHandler uriHandler, ILinkScanner linkScanner) {
        theURI = uri;
        this.thePackage = thePackage;
        theLinks = new ArrayList<ILink>();
        theSourceLinks = new ArrayList<ILink>();
        theDocLinks = new ArrayList<ILink>();
        theLinksWithUnknownType = new ArrayList<ILink>();
        
        this.uriHandler = uriHandler;
        this.linkScanner = linkScanner;
    }
    
    @Override
    public void scan() throws IOException, DocfactoException {
    	linkScanner.scan(getInputStreamFromUri(theURI), this);
    }

    /**
     * Return the tag URI
     * 
     * @return the tag URI
     * @since 2.3
     */
    @Override
    public String getURI() {
        return theURI;
    }

    /**
     * @see com.docfacto.links.scanners.FileScanner#getPackage()
     */
    @Override
    public String getPackage() {
        return thePackage;
    }

    /**
     * @see com.docfacto.links.scanners.FileScanner#getNumberOfLines()
     */
    @Override
    public int getNumberOfLines() {
        return linkScanner.getNumberOfLines();
    }

    /**
     * @see com.docfacto.links.scanners.FileScanner#hasLinks()
     */
    @Override
    public boolean hasLinks() {
        return !theLinks.isEmpty();
    }

    /**
     * @see com.docfacto.links.scanners.FileScanner#getLinks()
     */
    @Override
    public List<ILink> getLinks() {
        return theLinks;
    }

    /**
     * @see com.docfacto.links.scanners.FileScanner#hasDocLinks()
     */
    @Override
    public boolean hasDocLinks() {
        return !theDocLinks.isEmpty();
    }

    /**
     * @see com.docfacto.links.scanners.FileScanner#getDocLinks()
     */
    @Override
    public List<ILink> getDocLinks() {
        return theDocLinks;
    }

    /**
     * @see com.docfacto.links.scanners.FileScanner#hasSourceLinks()
     */
    @Override
    public boolean hasSourceLinks() {
        return !theSourceLinks.isEmpty();
    }

    /**
     * @see com.docfacto.links.scanners.FileScanner#getSourceLinks()
     */
    @Override
    public List<ILink> getSourceLinks() {
        return theSourceLinks;
    }

    @Override
    public boolean hasLinksOfUnknownType() {
        return !theLinksWithUnknownType.isEmpty();
    }

    /**
     * @see com.docfacto.links.scanners.FileScanner#getLinksOfUnknownType()
     */
    @Override
    public List<ILink> getLinksOfUnknownType() {
        return theLinksWithUnknownType;
    }
    
    /**
     * Gets a file object from a uri.
     * @param uri the uri of the file.
     * @return the file at the given uri.
     * @throws DocfactoException 
     * @since 2.5.1
     */
    private InputStream getInputStreamFromUri(String uri) throws DocfactoException {
    	return uriHandler.getInputStreamFromUri(uri);
    }

	public void scanStream(InputStream inputStream) throws IOException, DocfactoException {
		linkScanner.scan(inputStream, this);
	}

	@Override
	public int getNumberOfCodeLines() {
		return linkScanner.getNumberOfCodeLines();
	}

	@Override
	public int getNumberOfCommentLines() {
		return linkScanner.getNumberOfCommentLines();
	}

	@Override
	public void addLink(int lineNumber, String uri, String key, String version,
			LinkType linkType, String metadata) {
		addLink(new Link(theURI, thePackage, lineNumber, uri, key, version, linkType, metadata));
	}

	@Override
	public void addLink(int lineNumber, TagParser tagParser) {
    	String uri = tagParser.getAttributeValue(TagParser.URI_ATTRIBUTE);
        String key = tagParser.getAttributeValue(TagParser.LINK_KEY_ATTRIBUTE);
        String version = tagParser.getAttributeValue(TagParser.VERSION_ATTRIBUTE);
        LinkType linkType = LinkType.getLinkTypeFor(tagParser.getAttributeValue(TagParser.LINK_TO_ATTRIBUTE));
        String metadata = tagParser.getAttributeValue(TagParser.METADATA_ATTRIBUTE);
        
        addLink(lineNumber, uri, key, version, linkType, metadata);
	}
	
    private void addLink(ILink newLink) {
        theLinks.add(newLink);
        
        if (newLink.getLinkType() == LinkType.DOC_LINK)
            theDocLinks.add(newLink);
        else if (newLink.getLinkType() == LinkType.SOURCE_LINK)
            theSourceLinks.add(newLink);
        else
            theLinksWithUnknownType.add(newLink);
    }
}
