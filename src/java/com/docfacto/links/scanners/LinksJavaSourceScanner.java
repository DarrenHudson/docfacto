package com.docfacto.links.scanners;

import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.common.Platform;
import com.docfacto.javadoc.CommentParser;
import com.docfacto.javadoc.JavaDocUtils;
import com.docfacto.links.links.LinksCommentParser;

/**
 * Scan a source file, looking for all of the comments and the parsing them.
 * 
 * Comments can be either Javadoc, multi-line or single line comment.
 * 
 * A tag filter can be added so only comments with a tag will be added to the
 * list of comments.
 * 
 * TODO Note: This is an altered version of JavaSourceScanner as other classes use that so this new version was made specifically
 * for links so that it does not affect other classes. This should be refactored to minimise duplication of code.
 * 
 * @author damonli - created Oct 30, 2013
 * @since 2.4.8
 */
public class LinksJavaSourceScanner {

    private static final int COMMENT_HINT_SIZE = 100;

    private static final String SINGLE_LINE_PREFIX = "//";
    private static final String BEGIN_MULTILINE_COMMENT = "/*";

    private final InputStream theFileStream;
    private final List<LinksCommentParser> theComments;

    private String theTagFilter;

    // Source lines include comments
    private int theNoSourceLines = 0;
    private int theNoJavadocLines = 0;
    private int theNoMultiLineLines = 0;
    private int theNoSingleLines = 0;

    /**
     * Create a new instance of <code>JavaSourceScanner</code>.
     * 
     * @param sourceFile to process
     * @since 2.4.8
     */
    public LinksJavaSourceScanner(InputStream fileStream) {
        theFileStream = fileStream;
        theComments = new ArrayList<LinksCommentParser>(5);
    }

    /**
     * The filter will ignore comments that don't have this tag. If this is not
     * set then all comments are added to the comments
     * 
     * @param filterTag
     * @since 2.2
     */
    public void setFilter(String filterTag) {
        theTagFilter = filterTag;
    }

    /**
     * Scan the Java source file
     * 
     * @throws DocfactoException if unable to read the source file
     * @throws IOException if there was an error processing the source file
     * @since 2.2
     */
    public void scan() throws DocfactoException, IOException {

        LineNumberReader reader = null;
        try {
            reader =
                IOUtils.createFileReader(theFileStream);

            boolean isMultiLine = false;
            String line;
            int start = -1;
            int end = -1;
            StringBuilder comment = new StringBuilder(COMMENT_HINT_SIZE);
            String trimmedLine;

            while ((line = reader.readLine())!=null) {

                theNoSourceLines++;

                // Lets only do this once
                trimmedLine = line.trim();

                if (line.indexOf(SINGLE_LINE_PREFIX)>=0) {
                    theNoSingleLines++;
                    LinksCommentParser parser = new LinksCommentParser(reader.getLineNumber()-1);
                    parser.setCommentOffset(line.indexOf(SINGLE_LINE_PREFIX)+1);
                    parser.setEndLine(reader.getLineNumber());
                    parser.parse(trimmedLine);
                    theComments.add(parser);

                    // Can't be anything else
                    continue;
                }

                if (line.indexOf(JavaDocUtils.BEGIN_JAVADOC_COMMENT)>=0) {
                    start = reader.getLineNumber();
                }
                else if (line.indexOf(BEGIN_MULTILINE_COMMENT)>=0) {
                    // if (trimmedLine.equals(BEGIN_MULTILINE_COMMENT)) {
                    start = reader.getLineNumber();
                    isMultiLine = true;
                }

                if (start>-1) {
                    comment.append(line);
                    comment.append(Platform.LINE_SEPARATOR);
                    if (isMultiLine) {
                        theNoMultiLineLines++;
                    }
                    else {
                        theNoJavadocLines++;
                    }
                }

                // Same for multi line
                if (line.indexOf(JavaDocUtils.END_JAVADOC_COMMENT)>=0) {
                    // if (trimmedLine.equals(JavaDocUtils.END_JAVADOC_COMMENT))
                    // {
                    end = reader.getLineNumber();
                    final LinksCommentParser parser = new LinksCommentParser(start-1);

                    // Same line Javadoc
                    if (line.indexOf(BEGIN_MULTILINE_COMMENT)>0) {
                        parser.setCommentOffset(line
                            .indexOf(BEGIN_MULTILINE_COMMENT)+1);
                    }
                    else {
                        parser.setCommentOffset(line
                            .indexOf(JavaDocUtils.END_JAVADOC_COMMENT)+1);
                    }
                    
                    parser.setEndLine(end);
                    String commentString = comment.toString();
                    parser.parse(commentString);

                    if (theTagFilter==null||parser.hasTag(theTagFilter)) {
                        // Add it to the list of comments
                        theComments.add(parser);
                    }

                    // Wait for the next comment
                    start = -1;
                    isMultiLine = false;
                    comment = new StringBuilder(COMMENT_HINT_SIZE);
                }

            }
        }

        finally {
            IOUtils.close(reader);
        }
    }

    /**
     * Return if there are comment depending what the filter was set to
     * 
     * @return true if comments are present
     * @since 2.2
     */
    public boolean hasComments() {
        return theComments.size()>0;
    }

    /**
     * Return the comments, which may be empty, as these are only comments that
     * have the docfacto.links tag in them
     * 
     * @return the list of comments
     * @since 2.2
     */
    public List<LinksCommentParser> getComments() {
        return theComments;
    }

    /**
     * The number of lines in the source file.
     * 
     * This the total lines in the file
     * 
     * @return the number of lines in the source file
     * @since 2.2
     */
    public int getNumberOfSource() {
        return theNoSourceLines;
    }

    /**
     * The number of Javadoc lines, regardless of filter
     * 
     * @return the number of Javadoc lines
     * @since 2.2
     */
    public int getTotalNumberOfJavadocLines() {
        return theNoJavadocLines;
    }

    /**
     * Return the number of code lines.
     * 
     * @return source lines - comment lines
     * @since 2.2
     */
    public int getNumberOfCodeLines() {
        return Math.abs(getNumberOfSource()-getTotalNumberOfCommentLines());
    }

    /**
     * Return the number of comment lines.
     * 
     * Javadoc + Multi line + Single line comment
     * 
     * @return number of comment lines
     * @since 2.2
     */
    public int getTotalNumberOfCommentLines() {
        return theNoJavadocLines+theNoMultiLineLines+theNoSingleLines;
    }

    /**
     * Return the file's input stream
     * 
     * @return the file's input stream
     * @since 2.4
     */
    public InputStream getFileStream() {
        return theFileStream;
    }

    /**
     * Find the comment relative to this line position.
     * 
     * {@docfacto.note title="Warning"
     * <p>
     * You should only use this method, if you know that the item you are
     * searching for has javadoc, otherwise it will return another items javadoc
     * </p>}
     * 
     * {@docfacto.system <p>
     * The Javadoc doclet only provides positional information on the item it is
     * reporting on and not the Javadoc itself.
     * </p>
     * It is permissible that there can be several blank lines or annotations
     * between the item and the javadoc, this method will return the correct
     * comment parser, which in turn has the true line position.}
     * 
     * @param line the line for the declaration
     * @return the comment parser
     * @since 2.4
     */
    public CommentParser findCommentFor(int line) {

        for (int i = theComments.size()-1;i>=0;i--) {
            CommentParser parser = theComments.get(i);
            if (parser.getStartLine()<=line) {
                // Must be for me..
                return parser;
            }
        }

        return theComments.get(0);
    }
}

