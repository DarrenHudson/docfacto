package com.docfacto.links.scanners;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.javadoc.TagParser;
import com.docfacto.links.api.ILinkScanner;
import com.docfacto.links.api.ILinkStore;
import com.docfacto.xml.GenericDynamicElement;
import com.docfacto.xml.XMLFastParser;
import com.docfacto.xml.XMLParserHandler;

/**
 * A link scanner which scans xml files.
 * 
 * @author damonli
 *
 */
public class XMLLinkScanner implements ILinkScanner, XMLParserHandler {

	/**
     * Comment search pattern
     */
    static final Pattern TAG_PATTERN = Pattern
        .compile("@docfacto.link([^-]*|-[^-]+)*",
            Pattern.MULTILINE|
                Pattern.DOTALL);
    
    private XMLFastParser theParser;
    
    private ILinkStore theLinkStore;
    
    private int theNumberOfLines;
    
    
    public XMLLinkScanner() {
    	theNumberOfLines = 0;
    }
    
	/**
	 * @see com.docfacto.links.api.ILinkScanner#scan(java.io.InputStream, com.docfacto.links.api.ILinkStore)
	 */
	@Override
	public void scan(InputStream inputStream, ILinkStore linkStore)
			throws DocfactoException {
		
	    theLinkStore = linkStore;
		
		try {
            String contents = IOUtils.inputStreamAsString(inputStream);
    
            theParser = new XMLFastParser(contents);
            theParser.parse(this);
        }
        catch (DocfactoException ex) {
            if (ex.getCause() instanceof IOException) {
                throw new DocfactoException("I/O error when scanning input stream", ex);
            }
            else {
                throw ex;
            }
        }
	}

	@Override
	public int getNumberOfLines() {
		return theNumberOfLines;
	}

	@Override
	public int getNumberOfCodeLines() {
		return getNumberOfLines() - getNumberOfCommentLines();
	}

	@Override
	public int getNumberOfCommentLines() {
		return theParser.getCommentLines();
	}

	@Override
	public void beginDocument() { }

	@Override
	public void endDocument() {
		setNumberOfLines(theParser.getCurrentLine());
	}
	
	private void setNumberOfLines(int numberOfLines) {
		theNumberOfLines = numberOfLines;
	}

	@Override
	public void processComment(String comment) { 
		int index = comment.indexOf("@docfacto.link");
        
        if(index>-1) {
            String tagText = comment.substring(index + 14);
            TagParser tagParser = new TagParser(tagText);
            theLinkStore.addLink(theParser.getCurrentLine(), tagParser);
        }
	}

	@Override
	public void processEndTag(String endTag) { }

	@Override
	public void processStartTag(GenericDynamicElement gdl) { }

	@Override
	public void processText(String text) { }

	@Override
	public void processDocType(String text) { }

	@Override
	public void processProcessingInstruction(String text) { }
}
