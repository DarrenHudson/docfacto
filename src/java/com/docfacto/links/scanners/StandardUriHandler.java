package com.docfacto.links.scanners;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import com.docfacto.common.DocfactoException;
import com.docfacto.links.api.IUriHandler;

/**
 * A Standard Uri Handler which treats the given uri as a file and creates an
 * input stream from the file.
 * 
 * @author damonli
 * @since 2.5.1
 */
public class StandardUriHandler implements IUriHandler {

	/**
	 * @see com.docfacto.links.api.IUriHandler#getInputStreamFromUri(java.lang.String)
	 */
	@Override
	public InputStream getInputStreamFromUri(String uri) throws DocfactoException {
		try {
			return new FileInputStream(uri);
		} catch (FileNotFoundException e) {
			throw new DocfactoException("Could not get file at uri [" + uri + "] please check that the uri is correct");
		}
	}

}
