package com.docfacto.links.scanners;

import java.io.IOException;
import java.util.List;

import com.docfacto.common.DocfactoException;
import com.docfacto.links.links.ILink;

public interface ILinkScannerWrapper {
	
	/**
     * Return the number of lines in the file, maybe zero if not scanned
     * 
     * @return the number of lines in the file
     * @since 2.2
     */
    public int getNumberOfLines();
    
    /**
     * Return the absolute uri of the file being scanned.
     * 
     * @return the absolute uri of the file being scanned.
     * @since 2.2
     */
    public String getURI();
    
    /**
     * Return the package of the file being scanned.
     *
     * @return the package of the file being scanned.
     * @since 2.4.3
     */
    public String getPackage();
    
    /**
     * Check to see if there are any links.
     * 
     * @return true if links were found
     * @since 2.2
     */
    public boolean hasLinks();

    /**
     * Return the links, may be empty
     * 
     * @return the links
     * @since 2.2
     */
    public List<ILink> getLinks();
    
    /**
     * Check to see if there are any source links.
     * 
     * @return true if source links were found
     * @since 2.2
     */
    public boolean hasSourceLinks();

    /**
     * Return the source links, may be empty
     * 
     * @return the source links
     * @since 2.2
     */
    public List<ILink> getSourceLinks();
    
    /**
     * Check to see if there are any doc links.
     * 
     * @return true if doc links were found
     * @since 2.2
     */
    public boolean hasDocLinks();

    /**
     * Return the doc links, may be empty
     * 
     * @return the doc links
     * @since 2.2
     */
    public List<ILink> getDocLinks();

    /**
     * Return the number of code lines.
     * 
     * @return source lines - comment lines
     * @since 2.2
     */
    public int getNumberOfCodeLines();

    /**
     * Return the number of comment lines.
     * 
     * Javadoc + Multi line + Single line comment
     * 
     * @return number of comment lines
     * @since 2.2
     */
    public int getNumberOfCommentLines();

    /**
     * Check to see if there are any links of an unknown type (not a doc or source link)
     * 
     * @return true if links with unknown types are found
     * @since 2.4.8
     */
    public boolean hasLinksOfUnknownType();
    
    /**
     * Return the links with unknown type
     * 
     * @return the links with unknown type
     * @since 2.2
     */
    public List<ILink> getLinksOfUnknownType();

    /**
     * Run the scan for links.
     * 
     * @throws IOException when there was an error parsing file
     * @throws DocfactoException when there was an error parsing the file
     */
	public void scan() throws IOException, DocfactoException;
}
