package com.docfacto.links.scanners;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import com.docfacto.common.DocfactoException;
import com.docfacto.links.api.IUriHandler;

/**
 * A Uri handler for links with the 'http' protocol. This uri handler connects
 * to the given http uri and creates a stream to the data.
 * 
 * @author damonli
 * @since 2.5.1
 */
public class HTTPUriHandler implements IUriHandler {

	@Override
	public InputStream getInputStreamFromUri(String uri)
			throws DocfactoException {
		try {
			URL url = new URL(uri);
			URLConnection urlConnection = url.openConnection();
			InputStream inputStream = urlConnection.getInputStream();
			return inputStream;
		} catch (MalformedURLException e) {
			throw new DocfactoException("No protocol specified for uri [" + uri + "]", e);
		} catch (IOException e) {
			throw new DocfactoException("I/O error when opening input stream for uri [" + uri + "]", e);
		}
	}
}
