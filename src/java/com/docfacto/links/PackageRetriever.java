package com.docfacto.links;

import java.io.File;

/**
 * A class is for retrieving the packages for files
 * <p>
 * This class will calculate the package for a file.
 * </p>
 * @author damonli - created Aug 2, 2013
 * @since 2.4.4
 */
public class PackageRetriever {

    /**
     * Gets the package for a given file location
     * <p>
     * For a given file location, this method will workout the package for the file.
     * </p>
     * @param fileURL of the file to get the package for
     * @return the package of the file location given
     * @since 2.4.4
     */
    public static String getPackageForFile(String fileURL) {
        File file = new File(fileURL);
        
        int lastIndexOfFileSeparator = fileURL.lastIndexOf(file.separator);
        if (lastIndexOfFileSeparator < 0) {
            System.out.println(fileURL + " doesn't have separator");
            return fileURL;
        }
        
        String packageForFile = fileURL.substring(0, fileURL.lastIndexOf(file.separator));
        
        return packageForFile;
    }
}
