package com.docfacto.links;

/**
 * This class is for keeping track of a restriction value and checking whether 
 * that value has been exceeded.
 * 
 * @author damonli
 * @since 2.5.1
 */
public class RestrictionCounter {

	/**
	 * The restriction value which means there is no restriction.
	 */
	private final int UNLIMITED_RESTRICTION_VALUE = 0;
	
	private int restrictionValue;
	private int currentCount;
	private boolean restrictionExceeded;
	
	/**
	 * Constructor
	 * @param restrictionValue the restriction value which should not be exceeded.
	 * @since 2.5.1
	 */
	public RestrictionCounter(int restrictionValue) {
		this.restrictionValue = restrictionValue;
		this.currentCount = 0;
		verifyWhetherRestrictionIsExceeded();
	}
	
	/**
	 * Increment the count and sets whether the count has exceeded the restriction value.
	 * @since 2.5.1
	 */
	public void incrementCount() {
		currentCount++;
	}
	
	private void verifyWhetherRestrictionIsExceeded() {
		if (restrictionValue == UNLIMITED_RESTRICTION_VALUE)
			restrictionExceeded = false;
		else if (currentCount >= restrictionValue)
			restrictionExceeded = true;
		else
			restrictionExceeded = false;
	}
	
	/**
	 * Check whether the restriction value has been exceeded.
	 * @return whether the restriction value is exceeded.
	 * @since 2.5.1
	 */
	public boolean isRestrictionExceeded() {
		verifyWhetherRestrictionIsExceeded();
		return restrictionExceeded;
	}

	public int getRestrictionValue() {
		return restrictionValue;
	}
	
}
