package com.docfacto.links.links;

import com.docfacto.javadoc.CommentParser;
import com.docfacto.javadoc.TagParser;

/**
 *A Comment Parser which parses a given comment for links tags
 * <p>
 * This class will store the 'docfacto.link' tags and attributes found in a given comment into the list of tags.
 * 
 * Links tags are more flexible than typical javadoc tags: 
 * 
 * The comment can be either an inline comment, javadoc or block comment. How many stars/slashes are irrelevant. 
 * There can be multiple links tags in one comment on the same line. If the links attributes continue onto 
 * the next line(s), that is fine too.
 * 
 * TODO NOTE: This class was made to extend CommentParser and override the parse method to quickly create 
 * a parser for links tags instead of making changes to CommentParser as it was too confusing and troublesome due to how
 * much more flexible links tags are. It may be better to either change CommentParser to do what this class can do, 
 * or separate this away from CommentParser retaining only the minimum necessary functionality.
 * </p>
 * @author damonli - created Oct 29, 2013
 * @since 2.4.8
 */
public class LinksCommentParser extends CommentParser {

    private final String DOCFACTO_LINK_TAG_NAME = "@docfacto.link";
    
//    NOTE: This main method is for testing purposes, leave it here if you are editing this class and want to test more easily
    
//    public static void main(String[] args) {
//        LinksCommentParser parser = new LinksCommentParser(1);
//        
//        String testComment = "";
//        testComment += "/** \n";
//        testComment += "* Hello \n";
//        testComment += "* @docfacto.link {@note blah} uri=" + '"' + '{'+ "some uri" + '}' + "/something@/something" + '"' + " blah blah \n";
//        testComment += "* @docfacto.link blah blah @since 2.0 @docfacto.link blah blah \n";
//        testComment += "* blah blah \n";
//        testComment += "*/\n";
//        
//        String testComment2 = "//@docfacto.link {@note notes stuff} blah blah blah";
//        
//        String testComment3 = "/* @docfacto.link blah blah blah */";
//        
//        String testComment4 = "";
//        testComment4 += "/* some stuff \n";
//        testComment4 += "@docfacto.link blah blah blah \n";
//        testComment4 += "@docfacto.linkblah blah blah \n";
//        testComment4 += "@docfacto.link blah blah \n";
//        testComment4 += "@someOtherTag blah blah \n";
//        testComment4 += "blah blah @docfacto.link hello hello hello \n";
//        testComment4+= "*/ \n";
//        
//        File file = new File("/Users/damonli/Desktop/commentTester.txt");
//        String fileContent;
//        try {
//            fileContent = new Scanner(file).useDelimiter("\\Z").next();
//            //System.out.println(fileContent);
//            //parser.parse(fileContent);
//        }
//        catch (FileNotFoundException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        parser.parse(testComment);
//        parser.parse(testComment2);
//        parser.parse(testComment3);
//        parser.parse(testComment4);
//    }
    
    /**
     * Constructor.
     * @param startLine the starting line
     * @since 2.4.8
     */
    public LinksCommentParser(int startLine) {
        super(startLine);
    }

    @Override
    public void parse(String commentString) {
        //System.out.println("parsing: " + commentString);
        boolean lookingAtLinkTag = false;
        boolean lookingAtText = false; // when the parsing is currently on 
        boolean ignoringMode = false; // This is for when there are some escape characters such as { }
        String currentLine = "";
        int currentLineNo = 0;
        int currentTagLineNo = 0;
        
        String lines[] = commentString.split("\\r?\\n");
        
        String currentLinkAttributes = "";
        
        for (int i=0; i<lines.length; i++) {
            currentLineNo = i+1;
            currentLine = lines[i];
            
            if (currentLine.trim().isEmpty())
                continue;
            
            String nextLine = null;
            
            if (i != lines.length-1)
                nextLine = lines[i+1];
            
            //System.out.println("looking at line no: " + currentLineNo);
            //System.out.println("which is: " + currentLine);
            
            if (currentLine.contains(DOCFACTO_LINK_TAG_NAME) || lookingAtLinkTag) {
                // Note: A line might contain multiple tags (it shouldn't but it might)
                //System.out.println("contains a link tag!");
                // Go through the characters in the line
                for (int j=0; j < currentLine.length(); j++) {
                    
                    char currentChar = currentLine.charAt(j);
                    
                    //System.out.println("\nCurrent char is: " + currentChar + " at position: " + j);
                    //System.out.println("Looking at link tag: " + lookingAtLinkTag);
                    
                    if (!lookingAtText && currentChar == '"')
                        lookingAtText = true;
                    else if (lookingAtText && currentChar == '"')
                        lookingAtText = false;
                    
                    if (!lookingAtText && currentChar == '}')
                        ignoringMode = false;
                    
                    if (!lookingAtText && currentChar == '{')
                        ignoringMode = true;
                    
                    //System.out.println("Looking at text: " + lookingAtText);
                    //System.out.println("Ignoring mode: " + ignoringMode);
                    
                    if (lookingAtLinkTag) {
                        if (!ignoringMode) {
                            if (currentChar == '}' && !lookingAtText) {
                                //Don't add ending curly brackets unless they are in the text
                            }
                            else {
                                if (currentChar != '*')
                                    currentLinkAttributes += currentChar;
                            }
                        }
                        
                      //System.out.println("current link attributes is so far: " + currentLinkAttributes);
                        if (haveReachedEndOfLinkAttributes(j, currentLine, nextLine, ignoringMode, lookingAtText)) {
                            //System.out.println("Reached end of link attributes!!! \n");
                            //System.out.println("The link attributes for the link tag found at line no: " + currentTagLineNo + " is: " + currentLinkAttributes);
                            
                            TagParser parser = new TagParser(DOCFACTO_LINK_TAG_NAME.substring(1),currentLinkAttributes);
                            parser.setLineNumber(this.getStartLine() + currentTagLineNo);
                            this.getTags().add(parser);
                            //System.out.println("Added parser to line: " + (this.getStartLine() + currentTagLineNo));
                            lookingAtLinkTag = false;
                            currentLinkAttributes = "";
                            continue;
                        }
                    }
                    else {
                        if (ignoringMode)
                            continue;
                        
                        if (currentChar == '@') {
                            if (currentLine.length() >= j + DOCFACTO_LINK_TAG_NAME.length()+1) {
                                String potentialLinkTagName = currentLine.substring(j, j+DOCFACTO_LINK_TAG_NAME.length()+1);
                                
                                if (tagNameIsLinkTag(potentialLinkTagName)) {
                                    // found a docfacto link tag
                                    currentTagLineNo = currentLineNo;
                                    //System.out.println("Found a link tag at: " + currentTagLineNo);
                                    
                                    // Skip over the link tag name
                                    currentChar = currentLine.charAt(j+DOCFACTO_LINK_TAG_NAME.length());
                                    //System.out.println("Jumping to char: " + currentChar);
                                    lookingAtLinkTag = true;
                                    currentLinkAttributes += currentChar;
                                    j += DOCFACTO_LINK_TAG_NAME.length();
                                }
                            }
                        }
                    }
                }
            }
                
        }
        
        //System.out.println("Finished parsing");
    }

    private boolean haveReachedEndOfLinkAttributes(int currentCharPosition, String currentLine, String nextLine, boolean ignoringMode, boolean lookingAtText) {
        char currentChar = currentLine.charAt(currentCharPosition);
        boolean isLastLine = (nextLine == null || nextLine.isEmpty());
        
        boolean currentCharIsTheLastCharacterOnTheCurrentLine = charPositionIsLastOfString(currentCharPosition, currentLine);
        boolean currentCharIsTheLastCharacter = currentCharIsTheLastCharacterOnTheCurrentLine && isLastLine;
        
        Character charAfterCurrentChar = null;
        
        if (!currentCharIsTheLastCharacterOnTheCurrentLine) {
            charAfterCurrentChar = currentLine.charAt(currentCharPosition+1);
        }
        else if (!currentCharIsTheLastCharacter) {
            charAfterCurrentChar = nextLine.charAt(0);   
        }
        else {
            // It is the last character
            return true;
        }
        
        if (ignoringMode)
            return false;
            
        //System.out.println("Character after is: " + charAfterCurrentChar);
        if (charAfterCurrentChar == '@' && !lookingAtText)
            return true;
        
        if (currentChar == '*' && charAfterCurrentChar == '/')
            return true;
        
        return currentCharIsTheLastCharacter && isLastLine;
    }

    private boolean charPositionIsLastOfString(int charPosition, String string) {
        return charPosition == string.length()-1;
    }
    
    private boolean tagNameIsLinkTag(String potentialLinkTagName) {
        return potentialLinkTagName.equals(DOCFACTO_LINK_TAG_NAME + " ");
    }
}
