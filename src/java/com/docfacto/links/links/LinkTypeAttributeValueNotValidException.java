package com.docfacto.links.links;

/**
 * An exception to be thrown when a link type value is not valid
 * 
 * @author damonli
 * @since 2.5.0
 */
public class LinkTypeAttributeValueNotValidException extends Exception {

}
