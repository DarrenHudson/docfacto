package com.docfacto.links.links;

/**
 * An interface which determine what Links should have.
 * 
 * @author dhudson - created 22 Apr 2013
 * @since 2.2
 */
public interface ILink {

    /**
     * Check to see if the version data element is present
     * 
     * @return true if version element present
     * @since 2.2
     */
    public boolean hasVersion();
    
    /**
     * Return the version data, or null if not present
     * 
     * @return version data
     * @since 2.2
     */
    public String getVersion();

    /**
     * Check to see if the key data element is present
     * 
     * @return true if key element present
     * @since 2.2
     */
    public boolean hasKey();

    /**
     * Return the key data, or null if not present
     * 
     * @return key data
     * @since 2.2
     */
    public String getKey();

    /**
     * Check to see if the URI attribute is present
     * 
     * @return true if source element present
     * @since 2.2
     */
    public boolean hasURI();

    /**
     * Return the uri attribute value, or null if not present
     * 
     * @return the source file URI
     * @since 2.2
     */
    public String getURI();

    /**
     * Check to see if the metadata element is present
     * 
     * @return true if the metadata element is present
     * @since 2.2
     */
    public boolean hasMetadata();

    /**
     * Return the metadata data, or null if not present
     * 
     * @return metadata data
     * @since 2.2
     */
    public String getMetadata();
    
    /**
     * Check to see if the keys match
     * 
     * @param link to check
     * @return true of the keys match
     * @since 2.2
     */
    public boolean keysMatch(ILink link);
    
    
    /**
     * Check to see if the 'link-to' type is the correct corresponding type.
     * A 'link-to' type of 'source' matches up to another
     * 'link-to' type 'doc'.
     * 
     * @param link to check
     * @return true if the links correspond to each other
     * @since 2.2
     */
    public boolean linkTypesCorrespond(ILink link);
    
    /**
     * Check to see if the version match
     * 
     * @param link to check
     * @return true if the versions match
     * @since 2.2
     */
    public boolean versionMatch(ILink link);
    
    /**
     * Check to see if the metadata matches
     * 
     * @param metadata to check
     * @return true if the metadata matches
     * @since 2.2
     */
    public boolean metadataEquals(String metadata);
    
    /**
     * Return the line number of the link
     * 
     * @return the line number
     * @since 2.2
     */
    public int getLineNumber();

    /**
     * Uri of the file which contains the link
     *
     * @return the uri of the file which contains the link
     * @since 2.2
     */
    public String getContainingFileUri();
    
    /**
     * The package of the file which contains the link
     * 
     * @return the package of the file which contains the link
     * @since 2.4.3
     */
    public String getContainingFilePackage();
    
    /**
     * Return the type of the link
     * 
     * @return the type of the link
     * @since 2.3
     */
    public LinkType getLinkType();
    
    /**
     * Check to see if the type of this link is known (whether the link is a doc or source link)
     * 
     * @return true if the link type is known
     * @since 2.2
     */
    public boolean hasLinkType();
}
