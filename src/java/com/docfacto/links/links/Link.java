package com.docfacto.links.links;

/**
 * A basic Link data object.
 * 
 * @author dhudson - created 25 Apr 2013
 * @since 2.2
 */
public class Link implements ILink {

	private final String theContainingFileUri;
	private final String theContainingFilePackage;
	private int theLineNumber;

	private String theURI;
	private String theMetadata;
	private String theVersion;
	private String theKey;
	private LinkType theLinkType;

	/**
	 * Constructor.
	 * 
	 * @param fileUri the absolute uri of the file where this link was found.
	 * @param filePackage the package of the file where this link was found.
	 * @param lineNumber the line number of of the file where this link was found.
	 * @param uri the uri value of the link
	 * @param key the key value of the link
	 * @param version the version value of the link
	 * @param linkType the type of the link
	 * @param metadata the metadata of the link
	 * @since 2.2
	 */
	public Link(String fileUri, String filePackage, int lineNumber,
			String uri, String key, String version, LinkType linkType,
			String metadata) {
		this.theContainingFileUri = fileUri;
		this.theContainingFilePackage = filePackage;
		this.theLineNumber = lineNumber;

		this.theURI = uri;
		this.theKey = key;
		this.theVersion = version;
		this.theLinkType = linkType;
		this.theMetadata = metadata;
	}

	/**
	 * @see com.docfacto.links.links.Link#keysMatch(com.docfacto.links.links.Link)
	 */
	@Override
	public boolean keysMatch(ILink link) {
		if (!link.hasKey()) {
			return false;
		}

		if (!hasKey()) {
			return false;
		}

		return getKey().equals(link.getKey());
	}

	/**
	 * @see com.docfacto.links.links.Link#versionMatch(com.docfacto.links.links.Link)
	 */
	@Override
	public boolean versionMatch(ILink link) {
		if (!link.hasVersion()) {
			return false;
		}

		if (!hasVersion()) {
			return false;
		}

		return getVersion().equals(link.getVersion());
	}

	/**
	 * @see com.docfacto.links.links.Link#metadataEquals(java.lang.String)
	 */
	@Override
	public boolean metadataEquals(String metadata) {
		if (!hasMetadata()) {
			return false;
		}

		if (metadata == null) {
			return false;
		}

		return getMetadata().equals(metadata);
	}

	/**
	 * @see com.docfacto.links.links.Link#getContainingFileUri()
	 */
	@Override
	public String getContainingFileUri() {
		return theContainingFileUri;
	}

	/**
	 * @see com.docfacto.links.links.ILink#getContainingFilePackage()
	 */
	@Override
	public String getContainingFilePackage() {
		return theContainingFilePackage;
	}

	/**
	 * @see com.docfacto.links.links.Link#getLineNumber()
	 */
	@Override
	public int getLineNumber() {
		return theLineNumber;
	}

	/**
	 * @see com.docfacto.links.links.Link#hasVersion()
	 */
	@Override
	public boolean hasVersion() {
		return theVersion != null;
	}

	/**
	 * @see com.docfacto.links.links.Link#getVersion()
	 */
	@Override
	public String getVersion() {
		return theVersion;
	}

	/**
	 * @see com.docfacto.links.links.Link#hasKey()
	 */
	@Override
	public boolean hasKey() {
		return theKey != null;
	}

	/**
	 * @see com.docfacto.links.links.Link#getKey()
	 */
	@Override
	public String getKey() {
		return theKey;
	}

	/**
	 * @see com.docfacto.links.links.Link#hasMetadata()
	 */
	@Override
	public boolean hasMetadata() {
		return theMetadata != null;
	}

	/**
	 * @see com.docfacto.links.links.Link#getMetadata()
	 */
	@Override
	public String getMetadata() {
		return theMetadata;
	}

	/**
	 * @see com.docfacto.links.links.Link#hasURI()
	 */
	@Override
	public boolean hasURI() {
		return theURI != null;
	}

	/**
	 * @see com.docfacto.links.links.Link#getURI()
	 */
	@Override
	public String getURI() {
		return theURI;
	}

	/**
	 * @see com.docfacto.links.links.Link#getLinkType()
	 */
	@Override
	public LinkType getLinkType() {
		return this.theLinkType;
	}

	/**
	 * @see com.docfacto.links.links.Link#hasLinkType()
	 */
	@Override
	public boolean hasLinkType() {
		return theLinkType != null && theLinkType != LinkType.UNKNOWN;
	}

	/**
	 * @see com.docfacto.links.links.ILink#linkTypesCorrespond(com.docfacto.links.links.ILink)
	 */
	@Override
	public boolean linkTypesCorrespond(ILink link) {
		// If either of the types are unknown then it means they do not
		// correspond
		if (!this.hasLinkType())
			return false;

		if (!link.hasLinkType())
			return false;

		// Other wise if they don't match it means they correspond to each ther
		return !(this.getLinkType() == link.getLinkType());
	}
}
