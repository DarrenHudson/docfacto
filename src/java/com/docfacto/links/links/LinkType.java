package com.docfacto.links.links;

import java.util.ArrayList;
import java.util.List;

/**
 * The possible types of links
 * <p>
 * There are two types of links: doc links (DOC_LINK) with a link attribute
 * value of 'doc' source links (SOURCE_LINK) with a link attribute value of
 * 'source'
 * 
 * when a link has not been declared with a type it should have an unknown type
 * (UNKNOWN)
 * </p>
 * 
 * @author damonli - created Oct 20, 2013
 * @since 2.4.8
 */
public enum LinkType {

    /**
     * Doc link
     */
    DOC_LINK("doc"),
    /**
     * Source link
     */
    SOURCE_LINK("source"),
    /**
     * Unknown link
     */
    UNKNOWN("");

    private final String theAttributeValue;

    private LinkType(String attributeValue) {
        theAttributeValue = attributeValue;
    }

    /**
     * Get the attribute value for the link type.
     * 
     * @return the attribute value
     * @since 2.4.8
     */
    public String getAttributeValue() {
        return theAttributeValue;
    }

    /**
     * Get the corresponding link type for the link type.
     * 
     * @return the corresponding link type.
     * @since 2.4.8
     */
    public LinkType getCorrespondingLinkType() {
        if (this.equals(DOC_LINK))
            return SOURCE_LINK;
        else if (this.equals(SOURCE_LINK))
            return DOC_LINK;
        else
            return UNKNOWN;
    }

    /**
     * Get the corresponding attribute value for a given attribute value.
     * 
     * @param attributeValue the attribute value to get the corresponding value
     * for.
     * @return the corresponding attribute value for the given attribute value.
     */
    public static String getCorrespondingAttributeValue(String attributeValue) {
        if (attributeValue.equals(DOC_LINK.getAttributeValue()))
            return SOURCE_LINK.getAttributeValue();
        else
            return DOC_LINK.getAttributeValue();
    }

    /**
     * Get the possible attribute values.
     * 
     * @return an array of the possible attribute values.
     */
    public static String[] getAttributeValues() {
        // A list is used initially instead of an array as some attribute values
        // can be null
        // so we do not know the length of the array yet.
        List<String> attributeValuesList = new ArrayList<String>();

        for (int i = 0;i<values().length;i++) {
            String attributeValue = values()[i].getAttributeValue();
            if (attributeValue!=null&&!attributeValue.trim().isEmpty())
                attributeValuesList.add(attributeValue);
        }

        return attributeValuesList.toArray(new String[attributeValuesList
            .size()]);
    }

    /**
     * Validate the attribute value.
     * 
     * @param attributeValue value to test.
     * @return true if the attribute value exists.
     * @since 2.3
     */
    public static boolean attributeValueIsValid(String attributeValue) {
        LinkType linkType = getLinkTypeFor(attributeValue);
        return !linkType.equals(UNKNOWN);
    }

    /**
     * Get the corresponding link type for the given attribute value.
     * 
     * @param attributeValue the attribute value to get the link type for.
     * @return the corresponding link type for the given attribute value.
     */
    public static LinkType getLinkTypeFor(String attributeValue) {
        for (int i = 0;i<LinkType.values().length;i++) {

            LinkType linkType = LinkType.values()[i];

            if (linkType.getAttributeValue().equalsIgnoreCase(attributeValue))
                return linkType;
        }

        return UNKNOWN;
    }
}
