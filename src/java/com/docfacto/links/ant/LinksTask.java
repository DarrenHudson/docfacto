/*
 * @author dhudson -
 * Created 3 Apr 2013 : 11:59:01
 */

package com.docfacto.links.ant;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import com.docfacto.links.LinksConfiguration;
import com.docfacto.links.LinksOutputProcessor;
import com.docfacto.links.LinksProcessor;

/**
 * Links task.
 * 
 * @author dhudson - created 3 Apr 2013
 * @since 2.2
 */
public class LinksTask extends Task {

    private Source theSource;
    private Doc theDoc;
    private final LinksConfiguration theLinksConfig;

    /**
     * Create a new instance of <code>LinksTask</code>.
     */
    public LinksTask() {
        theLinksConfig = new LinksConfiguration();
    }

    /**
     * Set to true if the version attributes must match
     * 
     * @param value
     * @since 2.2
     */
    public void setversionmatch(Boolean value) {
        theLinksConfig.setIsVersionMatching(value);
    }

    /**
     * Path to the Docfacto.xml
     * 
     * @param path
     * @since 2.2
     */
    public void setconfigpath(String path) {
        theLinksConfig.setConfigPath(path);
    }

    /**
     * Sets the verbose flag
     * 
     * @param value
     * @since 2.2
     */
    public void setverbose(Boolean value) {
        theLinksConfig.setVerbose(value);
    }

    /**
     * @see org.apache.tools.ant.Task#execute()
     */
    @Override
    public void execute() throws BuildException {

        if (theSource!=null) {
            if (theSource.hasSourceSets()) {
                theSource.processSets(getProject());
                theLinksConfig.setSource(theSource);
            }
        }

        if (theDoc!=null) {
            if (theDoc.hasDocSets()) {
                theDoc.processSets(getProject());
                theLinksConfig.setDoc(theDoc);
            }
        }

        try {
            theLinksConfig.validate();

            final LinksProcessor processor = new LinksProcessor(theLinksConfig);
            processor.process();

            LinksOutputProcessor output = processor.getOutput();
            output.dump();
        }
        //catch (DocfactoException ex) {
        //    throw new BuildException(ex.getMessage(),ex);
        //}
        catch(Exception ex) {
            ex.printStackTrace();
            log("Exception : " +ex.getMessage());
        }
    }

    /**
     * Ant factory method for nested creation of elements
     * 
     * @return the Source element
     * @since 2.2
     */
    public Source createSource() {
        theSource = new Source();
        return theSource;
    }

    /**
     * Ant factory method for nested creation of elements
     * 
     * @return the Doc object
     * @since 2.2
     */
    public Doc createDoc() {
        theDoc = new Doc();
        return theDoc;
    }

}
