/*
 * @author dhudson -
 * Created 4 Apr 2013 : 10:52:10
 */

package com.docfacto.links.ant;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.FileSet;

/**
 * Handles the collection of {@code FileSet}s within the ant task
 * 
 * @author dhudson - created 4 Apr 2013
 * @since 2.2
 */
public class AbstractFileSetHandler {

    private final List<FileSet> theFileSets;

    /**
     * Store the result of the file sets
     */
    private List<String> theFiles;

    /**
     * Create a new instance of <code>AbstractFileSetHandler</code>.
     * @since 2.2 
     */
    public AbstractFileSetHandler() {
        theFileSets = new ArrayList<FileSet>(3);
    }

    /**
     * Add a {@code FileSet} to the list
     * 
     * @param fileSet to add
     * @since 2.2
     */
    public void addFileSet(FileSet fileSet) {
        theFileSets.add(fileSet);
    }

    /**
     * Return the list of File Sets
     * 
     * @return a list of {@code FileSet}s
     * @since 2.2
     */
    public List<FileSet> getFileSets() {
        return theFileSets;
    }

    /**
     * Check to see if a file set has been specified
     * 
     * @return true if a file set has been specified
     * @since 2.2
     */
    public boolean hasFileSet() {
        return theFileSets.size()>0;
    }

    /**
     * Generate a list of files that have been supplied via the {@code FileSet}
     * 's
     * 
     * @param project
     * @throws BuildException if unable to create a directory scanner
     * @since 2.2
     */
    public void processFileSets(Project project) throws BuildException {

        theFiles = new ArrayList<String>(100);

        DirectoryScanner ds = null;

        for (final FileSet fs:theFileSets) {
            try {
                ds = fs.getDirectoryScanner(project);
                final File baseDir = fs.getDir(project);

                final String[] srcFiles = ds.getIncludedFiles();

                for (final String file:srcFiles) {
                    theFiles.add(baseDir.getPath()+File.separator+file);
                }
            }
            catch (final BuildException ex) {
                // Not much we can do here
                throw ex;
            }
        }
    }

    /**
     * Return all of the files of all of the {@code FileSet}'s
     * 
     * @return the aggregates files of all of the sets
     * @since 2.3
     */
    public List<String> getFiles() {
        return theFiles;
    }
    
    /**
     * Set the files, this is normally done via processFileSets.
     * 
     * This method is here so that we can drive thing programmatically.
     * 
     * @param files
     * @since 2.3
     */
    public void setFiles(List<String> files) {
        theFiles = files;
    }
}
