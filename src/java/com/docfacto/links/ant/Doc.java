/*
 * @author dhudson -
 * Created 4 Apr 2013 : 10:51:54
 */

package com.docfacto.links.ant;

import java.util.ArrayList;
import java.util.List;

import org.apache.tools.ant.Project;

/**
 * The Doc definition from within the Ant task
 * 
 * @author dhudson - created 4 Apr 2013
 * @since 2.2
 */
public class Doc {

    private List<DocSet> theDocSets;

    /**
     * Create a new instance of <code>Doc</code>.
     */
    public Doc() {
        theDocSets = new ArrayList<DocSet>(3);
    }

    /**
     * Factory method for Ant
     * 
     * @return a new {@code DocSet}
     * @since 2.3
     */
    public DocSet createDocSet() {
        DocSet docSet = new DocSet();
        theDocSets.add(docSet);
        return docSet;
    }

    /**
     * Check to see if there are doc sets
     * 
     * @return true if there are doc sets
     * @since 2.3
     */
    public boolean hasDocSets() {
        return !theDocSets.isEmpty();
    }

    /**
     * Return the DocSets
     * 
     * @return the doc sets, may be empty
     * @since 2.3
     */
    public List<DocSet> getDocSets() {
        return theDocSets;
    }
    
    /**
     * Loop around tge doc sets processing the nested file sets
     * 
     * @param project current project
     * @since 2.3
     */
    public void processSets(Project project) {
        for (DocSet docSet:theDocSets) {
            docSet.processFileSets(project);
        }
    }
}
