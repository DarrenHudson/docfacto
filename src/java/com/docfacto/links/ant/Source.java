/*
 * @author dhudson -
 * Created 4 Apr 2013 : 10:51:40
 */

package com.docfacto.links.ant;

import java.util.ArrayList;
import java.util.List;

import org.apache.tools.ant.Project;

/**
 * The Source definition from within the Ant task.
 * 
 * Source can contain many source sets
 * 
 * @author dhudson - created 4 Apr 2013
 * @since 2.2
 */
public class Source {

    private List<SourceSet> theSourceSets;

    /**
     * Create a new instance of <code>Source</code>.
     * 
     * @since 2.3
     */
    public Source() {
        theSourceSets = new ArrayList<SourceSet>(3);
    }

    /**
     * Ant factory method for nested creation of elements
     * 
     * @return the SourceSet element
     * @since 2.3
     */
    public SourceSet createSourceSet() {
        SourceSet sourceSet = new SourceSet();
        theSourceSets.add(sourceSet);
        return sourceSet;
    }

    /**
     * Return the Source Sets
     * 
     * @return source sets, which may contain nested filesets
     * @since 2.3
     */
    public List<SourceSet> getSourceSets() {
        return theSourceSets;
    }
    
    /**
     * Check to see if there are source sets
     * 
     * @return true if there are source sets
     * @since 2.3
     */
    public boolean hasSourceSets() {
        return !theSourceSets.isEmpty();
    }
    
    /**
     * Loop around the doc sets processing the nested file sets
     * 
     * @param project current project
     * @since 2.3
     */
    public void processSets(Project project) {
        for (SourceSet sourceSet:theSourceSets) {
            sourceSet.processFileSets(project);
        }
    }
}
