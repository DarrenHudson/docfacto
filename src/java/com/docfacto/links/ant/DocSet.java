package com.docfacto.links.ant;



/**
 * A Documentation set contains one type of document and has a nested
 * {@code fileset}
 * 
 * @author dhudson - created 30 May 2013
 * @since 2.3
 */
public class DocSet extends AbstractFileSetHandler {

    private String theType;
    
    /**
     * Constructor.
     * @since 2.3
     */
    public DocSet() {
        
    }
    
    /**
     * Set the documentation type
     * 
     * @param type documentation type
     * @since 2.3
     */
    public void setType(String type) {
            theType = type;
    }
    
    /**
     * Return the doc type
     * 
     * @return the doc type
     * @since 2.3
     */
    public String getType() {
        return theType;
    }
}
