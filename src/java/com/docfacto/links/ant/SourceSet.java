package com.docfacto.links.ant;


/**
 * A source set contains one type of source and has a nested {@code fileset}
 * 
 * @author dhudson - created 30 May 2013
 * @since 2.3
 */
public class SourceSet extends AbstractFileSetHandler {

    private String theType;

    /**
     * Constructor.
     * 
     * @since 2.3
     */
    public SourceSet() {

    }

    /**
     * Set the documentation type
     * 
     * @param type documentation type
     * @since 2.3
     */
    public void setType(String type) {
        theType = type;
    }

    /**
     * Return the source type
     * 
     * @return the source type
     * @since 2.3
     */
    public String getType() {
        return theType;
    }
}
