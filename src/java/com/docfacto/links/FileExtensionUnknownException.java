package com.docfacto.links;

/**
 * An exception to be thrown when a file type could not be found for an extension
 * 
 * @author damonli
 * @since 2.5.0
 */
public class FileExtensionUnknownException extends Exception {
	
	/**
	 * Constructor
	 * @param fileExtension the file extension which a file type could not be found for
	 */
	public FileExtensionUnknownException(String fileExtension) {
		super("Unknown file type for extension [ " + fileExtension + " ]");
	}
}
