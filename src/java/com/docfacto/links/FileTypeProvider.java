package com.docfacto.links;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.StringUtils;
import com.docfacto.config.XmlConfig;
import com.docfacto.config.generated.Association;
import com.docfacto.config.generated.Extensions;
import com.docfacto.config.generated.FileAssociations;
import com.docfacto.config.generated.Links;
import com.docfacto.config.generated.Scanner;
import com.docfacto.config.generated.UriHandler;
import com.docfacto.links.api.ILinkScanner;
import com.docfacto.links.api.IUriHandler;
import com.docfacto.links.scanners.HTTPUriHandler;
import com.docfacto.links.scanners.JavaLinkScanner;
import com.docfacto.links.scanners.LinkScannerWrapper;
import com.docfacto.links.scanners.StandardUriHandler;
import com.docfacto.links.scanners.XMLLinkScanner;

/**
 * An enum singleton that handles the file types and which file extensions and file scanners are associated with them.
 * 
 * @author damonli
 *
 */
public enum FileTypeProvider {

	INSTANCE;
	
	/**
	 * The default file type
	 */
	public final String DEFAULT_FILE_TYPE = "xml";
	private final ILinkScanner DEFAULT_LINK_SCANNER = new XMLLinkScanner();
	private final IUriHandler DEFAULT_URI_HANDLER = new StandardUriHandler();
	
	private Set<String> supportedFileTypes;
	private Map<String, String> fileExtensionToFileTypeAssociationsMap;
	private Map<String, ILinkScanner> fileTypeToFileScannerMap;
	private Map<String, IUriHandler> protocolToUriHandlerMap;
	
	private FileTypeProvider() {
		supportedFileTypes = new TreeSet<String>();
		fileExtensionToFileTypeAssociationsMap = new HashMap<String, String>();
		fileTypeToFileScannerMap = new HashMap<String, ILinkScanner>();
		protocolToUriHandlerMap = new HashMap<String, IUriHandler>();
		loadDefaultSettings();
	}

	private void loadDefaultSettings() {
		loadDefaultFileTypes();
		loadDefaultFileAssociations();
		loadDefaultFileScanners();
		loadDefaultUriHandlers();
	}

	private void loadDefaultFileTypes() {
		/**
         * Java
         */
        addFileType("java");
        /**
         * Javascript
         */
        addFileType("javascript");
        /**
         * C and C++
         */
        addFileType("c");
        /**
         * XML
         */
        addFileType("xml");
        /**
         * HTML
         */
        addFileType("html");
        /**
         * Markdown
         */
        addFileType("markdown");
	}
	
	private void loadDefaultFileScanners() {
		fileTypeToFileScannerMap.put("java", new JavaLinkScanner());
		fileTypeToFileScannerMap.put("c", new JavaLinkScanner());
		fileTypeToFileScannerMap.put("javascript", new JavaLinkScanner());
	}

	private void loadDefaultFileAssociations() {
		addMultipleAssociations("java", "java");
		addMultipleAssociations("javascript", "js");
		addMultipleAssociations("c", "c");
		addMultipleAssociations("xml", "xml", "dita", "svg");
		addMultipleAssociations("html", "html", "htm");
		addMultipleAssociations("markdown", "markdown", "mdown", "mkdn", "mdwn", "mkd");
	}
	
	private void loadDefaultUriHandlers() {
		protocolToUriHandlerMap.put("http", new HTTPUriHandler());
	}
	
	private void addMultipleAssociations(String fileType, String...extensions) {
		for (int i=0; i < extensions.length; i++) {
			addAssociation(extensions[i], fileType);
		}
	}
	
	private void addAssociation(String extension, String fileType) {
		fileExtensionToFileTypeAssociationsMap.put(extension, fileType);
	}
    
    private void addFileType(String fileTypeName) {
    	supportedFileTypes.add(fileTypeName);
    }
	
	/**
	 * Validate the file type
	 * 
	 * @param fileType
	 *            to test
	 * @return true if the file type exists
	 * @since 2.3
	 */
	public boolean fileTypeIsValid(String fileType) {
		return supportedFileTypes.contains(fileType);
	}
    
	/**
	 * Load file type settings from the xml config.
	 * 
	 * @param xmlConfig the config to load the file type settings from.
	 * @throws DocfactoException when there was an error getting the file type settings from the xml config.
	 */
	public void loadSettingsFromConfig(XmlConfig xmlConfig) throws DocfactoException {
		clearExistingSettings();
		
		// Load the default file types first
		loadDefaultSettings();
		
		loadFileScannersFromConfig(xmlConfig);
		loadUriHandlersFromConfig(xmlConfig);
		loadFileTypesFromXmlConfig(xmlConfig);
		loadFileAssociationsFromConfig(xmlConfig);
	}
	
	private void clearExistingSettings() {
		supportedFileTypes.clear();
		fileExtensionToFileTypeAssociationsMap.clear();
	}
	
	private void loadFileTypesFromXmlConfig(XmlConfig xmlConfig) {
		Links linksConfig = xmlConfig.getLinksConfig();
		Extensions extensions = linksConfig.getExtensions();
		
		if (extensions == null) {
			return;
		}
		
		List<Scanner> scanners = extensions.getScanners();
		
		for (Scanner scanner : scanners) {
			String fileType = scanner.getFileType();
			supportedFileTypes.add(fileType);
		}
	}
	
	private void loadFileScannersFromConfig(XmlConfig xmlConfig) throws DocfactoException {
		Links linksConfig = xmlConfig.getLinksConfig();
		Extensions extensions = linksConfig.getExtensions();
		
		if (extensions == null)
			return;
		
		List<Scanner> scanners = extensions.getScanners();
		
		if (scanners.isEmpty()) {
			return;
		}
		
		for (Scanner scanner : scanners) {
			String scannerFileType = scanner.getFileType();
			String className = scanner.getClassName();
			
			try {
				Class scannerClass = Class.forName(className);
				ILinkScanner scannerObject = (ILinkScanner) scannerClass.newInstance();
				associateLinkScannerWithFileType(scannerFileType, scannerObject);
			} catch (ClassNotFoundException e) {
				throw new DocfactoException("Could not load scanner class [" + className + "]", e);
			} catch (InstantiationException e) {
				throw new DocfactoException("Could not load scanner class [" + className + "]", e);
			} catch (IllegalAccessException e) {
				throw new DocfactoException("Could not load scanner class [" + className + "]", e);
			}
		}
	}
	
	private void associateLinkScannerWithFileType(String scannerFileType, ILinkScanner linkScanner) {
		fileTypeToFileScannerMap.put(scannerFileType, linkScanner);
	}
	
	private void loadUriHandlersFromConfig(XmlConfig xmlConfig) throws DocfactoException {
		Links linksConfig = xmlConfig.getLinksConfig();
		Extensions extensions = linksConfig.getExtensions();
		
		if (extensions == null)
			return;
		
		List<UriHandler> uriHandlers = extensions.getUriHandlers();
		
		if (uriHandlers.isEmpty()) {
			return;
		}
		
		for (UriHandler uriHandler : uriHandlers) {
			String protocol = uriHandler.getProtocol();
			String className = uriHandler.getClassName();
			
			try {
				Class uriHandlerClass = Class.forName(className);
				IUriHandler uriHandlerObject = (IUriHandler) uriHandlerClass.newInstance();
				associateUriHandlerWithProtocol(protocol, uriHandlerObject);
			} catch (ClassNotFoundException e) {
				throw new DocfactoException("Could not load uri handler class [" + className + "]", e);
			} catch (InstantiationException e) {
				throw new DocfactoException("Could not load uri handler class [" + className + "]", e);
			} catch (IllegalAccessException e) {
				throw new DocfactoException("Could not load uri handler class [" + className + "]", e);
			}
		}
	}
	
	private void associateUriHandlerWithProtocol(String protocol, IUriHandler uriHandler) {
		protocolToUriHandlerMap.put(protocol, uriHandler);
	}
	
	private void loadFileAssociationsFromConfig(XmlConfig xmlConfig) throws DocfactoException {
		FileAssociations fileAssociations = xmlConfig.getLinksConfig().getFileAssociations();
		if(fileAssociations!=null){
		    for (Association association: fileAssociations.getAssociations()) {
		        
		        String fileTypeString = association.getFileType();
		        
		        if (fileTypeString == null || fileTypeString.isEmpty())
		            throw new DocfactoException("File association does not have a file type");
		        
		        if (!fileTypeIsValid(fileTypeString)) {
		        	throw new DocfactoException("File type: [" + fileTypeString + "] is either not supported or does not have a scanner associated with it.");
		        }
		        
		        List<String> extensions = association.getFileExtensions();
		        
		        if (extensions == null || extensions.isEmpty())
		            throw new DocfactoException("File association for type: [" + association.getFileType() + "] has no file extensions defined");
		        
		        for (String extension : extensions) {
		            String trimmedExtension = extension.trim();
		            
		            if (trimmedExtension.startsWith("."))
		                trimmedExtension = trimmedExtension.substring(1);
//		            
//		            if (fileExtensionToFileTypeAssociationsMap.containsKey(trimmedExtension))
//		                throw new DocfactoException("The extension: [" + trimmedExtension + "] has been defined multiple times");
		            
		            addAssociation(trimmedExtension, fileTypeString);
		        }
		    }
        }
	}
	
	/**
	 * Gets the file type which has been associated with the given extension in the config.
	 * @param extension the extension to get the FileType for
	 * @return the FileType associated with the given extension
	 * @since 2.5.1
	 */
	public String getFileTypeAssociatedWithExtension(String extension) {
		
		String chosenExtension = extension;
		
		if (extension.startsWith("."))
			chosenExtension = extension.substring(1);
		
    	return fileExtensionToFileTypeAssociationsMap.get(chosenExtension);
    }
	
	/**
	 * Get the file for a given file.
	 * 
	 * @param file the file to get the file type for
	 * @return the file type of the file
	 * @throws UnknownFileTypeException when the file type for the given file is unknown
	 */
	public String getTypeForFile(String file) throws UnknownFileTypeException {
		String fileExtension = StringUtils.getExtension(file);

        if (fileExtension==null) {
            throw new UnknownFileTypeException();
        }

        String fileType =  fileExtensionToFileTypeAssociationsMap.get(fileExtension);

        if (fileType!=null)
            return fileType;
        
        throw new UnknownFileTypeException();
	}
	
	private ILinkScanner getLinkScannerForFileType(String fileType) {
		
		ILinkScanner fileScanner = fileTypeToFileScannerMap.get(fileType);
		
		if (fileScanner == null)
			return DEFAULT_LINK_SCANNER;
		else
			return fileScanner;
		
	}

	public LinkScannerWrapper getLinkScannerWrapper(String docFileUri, String docPackage, String docType) {
		IUriHandler uriHandler = getUriHandlerForUri(docFileUri);
		ILinkScanner linkScanner = getLinkScannerForFileType(docType);
		return new LinkScannerWrapper(docFileUri, docPackage, uriHandler, linkScanner);
	}
	
	private IUriHandler getUriHandlerForUri(String uri) {
		String uriProtocol = getProtocolForUri(uri);
		
		IUriHandler uriHandler = protocolToUriHandlerMap.get(uriProtocol);
		if (uriHandler == null)
			return DEFAULT_URI_HANDLER;
		else
			return uriHandler;
	}
	
	private String getProtocolForUri(String uri) {
		if (uri.contains(":")) {
			int indexOfColon = uri.indexOf(":");
			String protocol = uri.substring(0, indexOfColon);
			return protocol;
		}
		else
			return null;
	}
}
