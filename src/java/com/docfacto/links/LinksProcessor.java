/*
 * @author dhudson -
 * Created 4 Apr 2013 : 11:35:54
 */

package com.docfacto.links;

import java.io.File;
import java.util.List;

import com.docfacto.common.ClassLoaderUtils;
import com.docfacto.common.DocfactoException;
import com.docfacto.config.generated.Libraries;
import com.docfacto.config.generated.Library;
import com.docfacto.config.generated.Links;
import com.docfacto.config.generated.Product;
import com.docfacto.licensor.Licensor;
import com.docfacto.licensor.Products;

/**
 * This is the main links processor, this will check the source and the docs to
 * see if the match.
 * 
 * @author dhudson - created 4 Apr 2013
 * @since 2.2
 */
public class LinksProcessor {

    /**
     * Product Name {@value}
     */
    public static final String PRODUCT_NAME = "Links";

    private final LinksConfiguration theLinksConfiguration;

    private LinksOutputProcessor theOutput;

    /**
     * Create a new instance of <code>LinksProcessor</code>.
     * 
     * @param config the configuration
     * @since 2.2
     */
    public LinksProcessor(LinksConfiguration config) {
        theLinksConfiguration = config;
    }

    public LinksProcessor(LinksConfiguration config,String workingDirectory) {
        this(config);

        if (workingDirectory!=null && !workingDirectory.isEmpty())
            config.setCurrentWorkingDirectory(workingDirectory);
    }

    /**
     * Process the source files and the doc files
     * 
     * @throws DocfactoException If
     * <ul>
     * <li>Invalid licence information</li>
     * <li>Unable to create {@code LinksOutputProcessor}</li>
     * </ul>
     * @since 2.2
     */
    public void process() throws DocfactoException {
    	
        Product product = theLinksConfiguration.getXmlConfig().getProduct(Products.LINKS);
        
        if (!Licensor.checkLicense(PRODUCT_NAME,product)) {
            throw new DocfactoException(
                "Invalid Docfacto Licence, please contact Docfacto Ltd [www.docfacto.com]");
        }
        
        Integer restrictionValue = product.getRestriction();
        RestrictionCounter restrictionCounter = new RestrictionCounter(restrictionValue);

        theOutput = new LinksOutputProcessor(theLinksConfiguration, restrictionCounter);

        processSourceFiles(restrictionCounter);
        
        // If haven't reached the max number of files yet continue to process the doc files.
        if (!theOutput.isMaxFilesReached())
        	processDocFiles(restrictionCounter);
    }

	/**
     * Process the Documentation files
     * 
     * @since 2.2
     */
    private void processDocFiles(RestrictionCounter restrictionCounter) {
        IDocumentProcessor documentProcessor =
            new DocumentProcessor(theLinksConfiguration,theOutput);
        documentProcessor.processDocumentFiles(restrictionCounter);
    }

    /**
     * Process the source files.
     * 
     * @param output
     * @since 2.2
     */
    private void processSourceFiles(RestrictionCounter restrictionCounter) {
        ISourceProcessor sourceProcessor =
            new SourceProcessor(theLinksConfiguration,theOutput);
        sourceProcessor.processSourceFiles(restrictionCounter);
    }

    /**
     * Return the results of running the process.
     * 
     * If process hasn't been run, then the results will be empty.
     * 
     * @return the results
     * @since 2.2
     */
    public LinksOutputProcessor getOutput() {
        return theOutput;
    }
}