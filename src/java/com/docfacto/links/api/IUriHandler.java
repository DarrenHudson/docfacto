package com.docfacto.links.api;

import java.io.InputStream;

import com.docfacto.common.DocfactoException;

/**
 *
 * An interface for Uri handling. Uri handlers are assigned to a protocol which can be configured in the Docfacto XML Config. Uris will be handled
 * by the uri handler which is associated with the uri's protocol (for example, the protocol for 'http://www.your-site.com/file.txt' is 'http'). 
 * 
 * <p>
 * A Uri handler should convert the data the given uri into an input stream.
 * </p>
 * 
 * @author damonli
 *
 */
public interface IUriHandler {
	
	/**
	 * Generate the input stream for a given uri.
	 * @param uri the uri to get an input stream for.
	 * @return the input stream for the given uri.
	 * @throws DocfactoException when there was an error generating the input stream.
	 */
	public InputStream getInputStreamFromUri(String uri) throws DocfactoException;
}
	