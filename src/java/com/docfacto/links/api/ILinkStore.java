package com.docfacto.links.api;

import com.docfacto.javadoc.TagParser;
import com.docfacto.links.links.LinkType;

/**
 * An interface for classes which can store links.
 * @author damonli
 *
 */
public interface ILinkStore {

	/**
	 * Add a new link to the link store.
	 * @param lineNumber the line number of the link.
	 * @param uri the uri of the link.
	 * @param key the key of the link.
	 * @param version the version of the link.
	 * @param linkType the type of the link.
	 * @param metadata the metadata of the link.
	 */
	public void addLink(int lineNumber,
			String uri, String key, String version, LinkType linkType,
			String metadata);

	/**
	 * Add a new link to the link store.
	 * @param lineNumber the line number of the link.
	 * @param tagParser the tag parser which contains the required attributes and values for a link.
	 */
	public void addLink(int lineNumber, TagParser tagParser);
}
