package com.docfacto.links.api;

import java.io.InputStream;

import com.docfacto.common.DocfactoException;

/**
 * An interface for Link Scanning. Link Scanners are assigned to a file type which can be configured in the Docfacto XML Config. Files will 
 * be scanned by the scanner associated with the file's file type.
 * 
 * <p>
 * A Link scanner should scan a given input stream for links and then add those links into the given LinkStore. These links will then be used 
 * in analysis.
 * </p>
 * 
 * @author damonli
 */
public interface ILinkScanner {

	/**
	 * Scan an input stream for links and then add them to the given link store.
	 * @param inputStream the input stream to scan.
	 * @param linkStore the link store where links found are added to.
	 * @throws DocfactoException when an error occurs when scanning the input stream.
	 */
	public void scan(InputStream inputStream, ILinkStore linkStore)  throws DocfactoException;
	
	/**
	 * Gets the number of lines scanned.
	 * @return the number of lines scanned.
	 */
	public int getNumberOfLines();
	
	/**
	 * Gets the number of code lines scanned.
	 * @return the number of code lines scanned.
	 */
	public int getNumberOfCodeLines();
	
	/**
	 * Gets the number of comment lines scanned.
	 * @return the number of comment lines scanned. 
	 */
	public int getNumberOfCommentLines();
}
