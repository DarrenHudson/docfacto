package com.docfacto.links;

import java.io.IOException;

import com.docfacto.common.DocfactoException;
import com.docfacto.config.generated.Severity;
import com.docfacto.links.links.ILink;
import com.docfacto.links.links.LinkType;
import com.docfacto.links.scanners.LinkScannerWrapper;

/**
 * A Class for processing source links
 * <p>
 * This class processes a given Document Scanner to find source links and add the appropriate
 * results and statistics during processing.
 * </p>
 * @author damonli - created Jun 13, 2013
 * @since 2.2
 */
public class SourceLinksProcessor {

    private final LinksConfiguration theLinksConfiguration;
    private final LinksOutputProcessor theOutput;
    
    /**
     * Constructor.
     * @param linksConfiguration the links configuration for resolving paths and getting the settings
     * @param output the output to write results and statistics to
     * @since 2.2
     */
    public SourceLinksProcessor(LinksConfiguration linksConfiguration, LinksOutputProcessor output) {
        theLinksConfiguration = linksConfiguration;
        theOutput = output;
    }
    
    /**
     * Process the source links and see if there are matching doc links.
     * 
     * @param documentScanner to process
     * @since 2.4.0
     */
    public void processSourceLinks(LinkScannerWrapper documentScanner) {

        boolean foundWorkingSourceLink = false;
        int numberOfWorkingSourceLinks = 0;
        
        for (ILink sourceLink:documentScanner.getSourceLinks()) {
            theOutput.incrementStatistic(LinksStatKey.SOURCE_LINK_FOUND_KEY);
            theOutput.incrementPackageStatistic(documentScanner.getPackage(),LinksStatKey.SOURCE_LINK_FOUND_KEY);
            
            LinkScannerWrapper sourceScanner;
            try {
                sourceScanner = getSourceScannerForSourceLink(documentScanner, sourceLink);
                runSourceScannerScan(sourceScanner, documentScanner, sourceLink);
            }
            catch (LinkProcessingException e) {
                // Process the next source link
                continue;
            }
    
            // The file may contain may source links and lets see if
            // we can find the right one
            
            if (!sourceScanner.hasDocLinks()) {
                theOutput.addResult(documentScanner.getURI(),
                    documentScanner.getPackage(),
                    sourceLink.getLineNumber(),
                    Severity.WARNING,"Missing corresponding doc link",
                    LinksStatKey.SOURCE_BROKEN_LINK_KEY);
                
                continue;
            }
            
            boolean found = false;
            
            for (ILink docLink:sourceScanner.getDocLinks()) {
                found = linksMatch(sourceLink,docLink);
                if (found) {
                    if (found) {
                        if (theLinksConfiguration.isVerbose()) {
                            String resolvedSourceScannerURI = LinksPathsManager.INSTANCE.getShortenedPathFromAbsolutePath(sourceScanner.getURI());
                            String resolvedDocScannerURI = LinksPathsManager.INSTANCE.getShortenedPathFromAbsolutePath(documentScanner.getURI());
                            
                            System.out.println(resolvedDocScannerURI+":"+
                                sourceLink.getLineNumber()+" -> "+resolvedSourceScannerURI);
                        }
                        break;
                    }
                }
            }

            if (!found) {
                theOutput.addResult(documentScanner.getURI(),
                    documentScanner.getPackage(),
                    sourceLink.getLineNumber(),
                    Severity.WARNING,"Missing corresponding doc link",
                    LinksStatKey.SOURCE_BROKEN_LINK_KEY);
            }
            else {
                if (!foundWorkingSourceLink) {
                    //No errors with source link
                    theOutput.incrementStatistic(LinksStatKey.DOC_FILES_WITH_SOURCE_LINK);
                    theOutput.incrementPackageStatistic(documentScanner.getPackage(),LinksStatKey.DOC_FILES_WITH_SOURCE_LINK);
                    foundWorkingSourceLink = true;
                }
                numberOfWorkingSourceLinks++;
            }
        }
        
        if (numberOfWorkingSourceLinks > 0) {
            theOutput.addToStatistic(LinksStatKey.DOC_LINES_LINKED_TO_CODE, documentScanner.getNumberOfLines());
            theOutput.addToPackageStatistic(documentScanner.getPackage(),LinksStatKey.DOC_LINES_LINKED_TO_CODE,documentScanner.getNumberOfLines());
        }
    }
    
    /**
     * Runs a source scanner scan.
     * <p>
     * Runs a scan for a given source scanner, if exceptions are caught, they are added as results to the output.
     * </p>
     * @param sourceScanner the source scanner to run.
     * @param documentScanner the document scanner which the source scanner's source link was retrieved from.
     * @param sourceLink the source link the source scanner is being run for.
     * @throws LinkProcessingException when there was an exception running the scan.
     * @since 2.4.7
     */
    private void runSourceScannerScan(LinkScannerWrapper sourceScanner, LinkScannerWrapper documentScanner, ILink sourceLink) throws LinkProcessingException {
        try {
            sourceScanner.scan();
        }
        catch (final IOException ex) {
            theOutput.addResult(documentScanner.getURI(),
                documentScanner.getPackage(),
                sourceLink.getLineNumber(),
                Severity.WARNING,
                "Invalid source link uri",
                LinksStatKey.SOURCE_LINK_TAG_INVALID_URI_KEY);
            
            throw new LinkProcessingException();
        } 
        catch (DocfactoException ex) {
            theOutput.addResult(sourceScanner.getURI(),
                sourceScanner.getPackage(),
                sourceLink.getLineNumber(),
                Severity.WARNING,
                "Unable to load source file [" + 
                    ex.getMessage() + "]",
                LinksStatKey.SOURCE_FILES_IO_EXCEPTION);
            
            throw new LinkProcessingException();
        }
    }

    /**
     * Gets the correct source scanner for a given source link.
     *
     * @param docScanner the doc scanner the source link was retrieved from.
     * @param sourceLink the source link to get the source scanner for.
     * @return the source scanner for the source link.
     * @throws LinkProcessingException when there was a problem retrieving the necessary attributes to get the 
     * document scanner for the source link.
     * @since 2.4.7
     */
    private LinkScannerWrapper getSourceScannerForSourceLink(LinkScannerWrapper docScanner, ILink sourceLink) throws LinkProcessingException {
        if (!sourceLink.hasURI()) {
            theOutput.addResult(docScanner.getURI(),
                                docScanner.getPackage(),
                                -1,
                                Severity.WARNING,
                                "Source link missing uri",
                                LinksStatKey.SOURCE_LINK_TAG_NO_URI_KEY);
            
            throw new LinkProcessingException();
        }

        String sourceFile = null;
        try {
            sourceFile = LinksPathsManager.INSTANCE.getAbsolutePathFromShortenedPath(sourceLink.getURI());
        }
        catch (DocfactoException ex) {
            theOutput.addResult(docScanner.getURI(),
                                docScanner.getPackage(),
                                sourceLink.getLineNumber(),
                                Severity.WARNING,
                                "Unable to resolve path link " + ex.getMessage() + "]",
                                LinksStatKey.DOC_LINK_TAG_INVALID_URI_KEY);
            
            throw new LinkProcessingException();
        }

        String sourceType;
		try {
			sourceType = FileTypeProvider.INSTANCE.getTypeForFile(sourceFile);
		} catch (UnknownFileTypeException e) {
			theOutput.addResult(docScanner.getURI(),
                docScanner.getPackage(),
                sourceLink.getLineNumber(),
                Severity.WARNING,
                "Unknown link uri file type",
                LinksStatKey.UNKNOWN_FILE_TYPE);
			
			sourceType = FileTypeProvider.INSTANCE.DEFAULT_FILE_TYPE;
		}

        String sourcePackage = PackageRetriever.getPackageForFile(sourceFile);
        LinkScannerWrapper sourceScanner = FileTypeProvider.INSTANCE.getLinkScannerWrapper(sourceFile,sourcePackage,sourceType);
    
        return sourceScanner;
    }

    /**
     * Compare two links, they can get either doc or source links
     * 
     * @param a the first link
     * @param b the second link
     * @return true if the links match
     * @since 2.2
     */
    protected boolean linksMatch(ILink a,ILink b) {
        boolean found = false;

        boolean keysMatch = a.keysMatch(b);
        boolean linkTypesMatch = a.linkTypesCorrespond(b);
        
        if (keysMatch && linkTypesMatch) {
            found = true;

            if (theLinksConfiguration.isVersionMatching()) {
                // Lets see if the versions match
                if (a.versionMatch(b)) {
                 // Its all good
                    if (a.getLinkType() == LinkType.SOURCE_LINK) {
                        theOutput.incrementStatistic(LinksStatKey.SOURCE_LINK_MATCHED_KEY);
                        theOutput.incrementPackageStatistic(a.getContainingFilePackage(),LinksStatKey.SOURCE_LINK_MATCHED_KEY);
                    }
                    else if (a.getLinkType() == LinkType.DOC_LINK) {
                        theOutput.incrementStatistic(LinksStatKey.DOC_LINK_MATCHED_KEY);
                        theOutput.incrementPackageStatistic(a.getContainingFilePackage(),LinksStatKey.DOC_LINK_MATCHED_KEY);
                    }
                    else {
                        // Do nothing
                    }
                }
                else {
                    if (a.getLinkType() == LinkType.SOURCE_LINK) {
                        theOutput.addResult(
                                a.getContainingFileUri(),
                                a.getContainingFilePackage(),
                                a.getLineNumber(),
                                Severity.INFO,
                                "Source link version mismatch",
                                LinksStatKey.SOURCE_LINK_VERSION_MISMATCH_KEY);
                    }
                    else if (a.getLinkType() == LinkType.DOC_LINK) {
                        theOutput.addResult(
                                a.getContainingFileUri(),
                                a.getContainingFilePackage(),
                                a.getLineNumber(),
                                Severity.INFO,
                                "Doc link version mismatch",
                                LinksStatKey.DOC_LINK_VERSION_MISMATCH_KEY);
                    }
                    else {
                        // Do Nothing
                    }
                }
            }
            else {
                // No version matching, but still good..
                if (a.getLinkType() == LinkType.SOURCE_LINK)
                    theOutput.incrementStatistic(LinksStatKey.SOURCE_LINK_MATCHED_KEY);
                else if (a.getLinkType() == LinkType.DOC_LINK)
                    theOutput.incrementStatistic(LinksStatKey.DOC_LINK_MATCHED_KEY);
                else {
                    // Do Nothing
                }
            }
        }

        return found;
    }
}
