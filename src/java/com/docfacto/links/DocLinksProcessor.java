package com.docfacto.links;

import java.io.IOException;

import com.docfacto.common.DocfactoException;
import com.docfacto.config.generated.Severity;
import com.docfacto.links.links.ILink;
import com.docfacto.links.links.LinkType;
import com.docfacto.links.scanners.LinkScannerWrapper;

/**
 * A Class for processing doc links
 * <p>
 * This class processes a given Source Scanner to find doc links and add the appropriate
 * results and statistics during processing.
 * </p>
 * @author damonli - created Jun 13, 2013
 * @since 2.2
 */
public class DocLinksProcessor {

    private final LinksConfiguration theLinksConfiguration;
    private final LinksOutputProcessor theOutput;
    
    /**
     * Constructor.
     * @param theLinksConfiguration the links configuration for resolving paths and getting the settings
     * @param theOutput the output to write results and statistics to
     * @since 2.2
     */
    public DocLinksProcessor(LinksConfiguration theLinksConfiguration, LinksOutputProcessor theOutput) {
        this.theLinksConfiguration = theLinksConfiguration;
        this.theOutput = theOutput;
    }
    
    /**
     * Process the source links and see if there are matching doc links
     * 
     * @param sourceScanner to process
     * @since 2.2
     */
    public void processDocLinks(LinkScannerWrapper sourceScanner) {

        boolean foundWorkingDocLink = false;
        int numberOfWorkingDocLinks = 0;
        
        for (ILink docLink:sourceScanner.getDocLinks()) {
            theOutput.incrementStatistic(LinksStatKey.DOC_LINK_FOUND_KEY);
            theOutput.incrementPackageStatistic(sourceScanner.getPackage(), LinksStatKey.DOC_LINK_FOUND_KEY);
    
            LinkScannerWrapper docScanner;
            try {
                docScanner = getDocumentScannerForDocLink(sourceScanner, docLink);
                runDocScannerScan(docScanner, sourceScanner, docLink);
            }
            catch (LinkProcessingException e) {
                // Process the next doc link
                continue;
            }
    
            // The file may contain may source links and lets see if
            // we can find the right one
            
            if (!docScanner.hasSourceLinks()) {
                theOutput.addResult(sourceScanner.getURI(),
                    sourceScanner.getPackage(),
                    docLink.getLineNumber(),
                    Severity.WARNING,"Missing corresponding source link",
                    LinksStatKey.DOC_BROKEN_LINK_KEY);
                continue;
            }

            boolean found = false;
            
            // Go through each source link and check if they match
            for (ILink sourceLink:docScanner.getSourceLinks()) {
                found = linksMatch(docLink,sourceLink);
                if (found) {
                    if (theLinksConfiguration.isVerbose()) {
                        String resolvedSourceScannerURI = LinksPathsManager.INSTANCE.getShortenedPathFromAbsolutePath(sourceScanner.getURI());
                        String resolvedDocScannerURI = LinksPathsManager.INSTANCE.getShortenedPathFromAbsolutePath(docScanner.getURI());
                    
                        System.out.println(resolvedSourceScannerURI+":"+
                            docLink.getLineNumber()+" -> "+resolvedDocScannerURI);
                    }
                    break;
                }
            }

            // None were matched so 
            if (!found) {
                theOutput.addResult(sourceScanner.getURI(),
                    sourceScanner.getPackage(),
                    docLink.getLineNumber(),
                    Severity.WARNING,"Missing corresponding source link",
                    LinksStatKey.DOC_BROKEN_LINK_KEY);
            }
            else {
                if (!foundWorkingDocLink) {
                    //No errors with doc link so a working Doc link was found
                    theOutput.incrementStatistic(LinksStatKey.SOURCE_FILES_WITH_DOC_LINK);
                    theOutput.incrementPackageStatistic(sourceScanner.getPackage(), LinksStatKey.SOURCE_FILES_WITH_DOC_LINK);
                    foundWorkingDocLink = true;
                }
                numberOfWorkingDocLinks++;
            }
        }
        
        if (numberOfWorkingDocLinks > 0) {
            theOutput.addToStatistic(LinksStatKey.CODE_LINES_LINKED_TO_DOC, sourceScanner.getNumberOfCodeLines());
            theOutput.addToPackageStatistic(sourceScanner.getPackage(), LinksStatKey.CODE_LINES_LINKED_TO_DOC, sourceScanner.getNumberOfCodeLines());
        }
    }
    
    /**
     * Runs a doc scanner scan.
     * <p>
     * Runs a scan for a given doc scanner, if exceptions are caught, they are added as results to the output.
     * </p>
     * @param docScanner the docScanner to run.
     * @param sourceScanner the source scanner which the doc scanner's doc link was retrieved from.
     * @param docLink the doc link the doc scanner is being run for.
     * @throws LinkProcessingException when there was an excption running the scan.
     * @since 2.4.7
     */
    private void runDocScannerScan(LinkScannerWrapper docScanner, LinkScannerWrapper sourceScanner, ILink docLink) throws LinkProcessingException {
        try {
            docScanner.scan();
        }
        catch (final IOException ex) {
            theOutput.addResult(sourceScanner.getURI(),
                sourceScanner.getPackage(),
                docLink.getLineNumber(),
                Severity.WARNING,
                "Invalid doc link uri",
                LinksStatKey.DOC_LINK_TAG_INVALID_URI_KEY);
            
            throw new LinkProcessingException();
        }
        catch (final DocfactoException ex) {
            theOutput.addResult(sourceScanner.getURI(),
                sourceScanner.getPackage(),
                docLink.getLineNumber(),
                Severity.WARNING,"Unable to load doc file ["+
                    ex.getMessage()+"]",
                LinksStatKey.DOC_FILES_IOEXCEPTION);
            
            throw new LinkProcessingException();
        }
    }

    /**
     * Gets the correct document scanner for a given doc link.
     * 
     * @param sourceScanner the source scanner the doc link was retrieved from.
     * @param docLink the doc link to get the source scanner for.
     * @return the doc scanner for the doc link.
     * @throws LinkProcessingException when there was a problem retrieving the necessary attributes to get the 
     * document scanner for the source link.
     * @since 2.4.7
     */
    private LinkScannerWrapper getDocumentScannerForDocLink(LinkScannerWrapper sourceScanner, ILink docLink) throws LinkProcessingException {
        if (!docLink.hasURI()) {
            // Issue an error where the URI is missing
            theOutput.addResult(
                sourceScanner.getURI(), 
                sourceScanner.getPackage(),
                docLink.getLineNumber(),
                Severity.WARNING,"Missing attribute uri",
                LinksStatKey.DOC_LINK_TAG_NO_URI_KEY);
            
            throw new LinkProcessingException();
        }

        String docFile = null;
        String docPackage = null;
        try {
            docFile = LinksPathsManager.INSTANCE.getAbsolutePathFromShortenedPath(docLink.getURI());
            docPackage = PackageRetriever.getPackageForFile(docFile);
        }
        catch (DocfactoException ex) {
            theOutput.addResult(
                sourceScanner.getURI(),
                sourceScanner.getPackage(),
                docLink.getLineNumber(),
                Severity.WARNING,
                "Unable to resolve path link "+
                    ex.getMessage()+"]",
                LinksStatKey.DOC_LINK_TAG_INVALID_URI_KEY);
            
            throw new LinkProcessingException();
        }

        String docType;
		try {
			docType = FileTypeProvider.INSTANCE.getTypeForFile(docFile);
		} catch (UnknownFileTypeException e) {
			theOutput.addResult(sourceScanner.getURI(),
                sourceScanner.getPackage(),
                docLink.getLineNumber(),
                Severity.WARNING,
                "Unknown link uri file type",
                LinksStatKey.UNKNOWN_FILE_TYPE);
			
			docType = FileTypeProvider.INSTANCE.DEFAULT_FILE_TYPE;
		}

        LinkScannerWrapper docScanner = FileTypeProvider.INSTANCE.getLinkScannerWrapper(docFile,docPackage,docType);
        return docScanner;
    }

    /**
     * Compare two links, they can get either doc or source links
     * 
     * @param a the first link
     * @param b the second link
     * @return true if the links match
     * @since 2.2
     */
    protected boolean linksMatch(ILink a,ILink b) {
        boolean found = false;

        boolean keysMatch = a.keysMatch(b);
        boolean linkTypesMatch = a.linkTypesCorrespond(b);
            
        if (keysMatch && linkTypesMatch) {
            found = true;

            if (theLinksConfiguration.isVersionMatching()) {
                // Lets see if the versions match
                if (a.versionMatch(b)) {
                 // Its all good
                    if (a.getLinkType() == LinkType.SOURCE_LINK) {
                        theOutput.incrementStatistic(LinksStatKey.SOURCE_LINK_MATCHED_KEY);
                        theOutput.incrementPackageStatistic(a.getContainingFilePackage(),LinksStatKey.SOURCE_LINK_MATCHED_KEY);
                    }
                    else if (a.getLinkType() == LinkType.DOC_LINK) {
                        theOutput.incrementStatistic(LinksStatKey.DOC_LINK_MATCHED_KEY);
                        theOutput.incrementPackageStatistic(a.getContainingFilePackage(),LinksStatKey.DOC_LINK_MATCHED_KEY);
                    }
                    else {
                        // Do nothing
                    }
                }
                else {
                    if (a.getLinkType() == LinkType.SOURCE_LINK) {
                        theOutput.addResult(
                                a.getContainingFileUri(),
                                a.getContainingFilePackage(),
                                a.getLineNumber(),
                                Severity.INFO,
                                "Source link version mismatch",
                                LinksStatKey.SOURCE_LINK_VERSION_MISMATCH_KEY);
                    }
                    else if (a.getLinkType() == LinkType.DOC_LINK) {
                        theOutput.addResult(
                                a.getContainingFileUri(),
                                a.getContainingFilePackage(),
                                a.getLineNumber(),
                                Severity.INFO,
                                "Doc link version mismatch",
                                LinksStatKey.DOC_LINK_VERSION_MISMATCH_KEY);
                    }
                    else {
                        // Do Nothing
                    }
                }
            }
            else {
                // No version matching, but still good..
                if (a.getLinkType() == LinkType.SOURCE_LINK)
                    theOutput.incrementStatistic(LinksStatKey.SOURCE_LINK_MATCHED_KEY);
                else if (a.getLinkType() == LinkType.DOC_LINK)
                    theOutput.incrementStatistic(LinksStatKey.DOC_LINK_MATCHED_KEY);
                else {
                    // Do Nothing
                }
            }
        }
        return found;
    }
}
