package com.docfacto.links;

/**
 * An exception to be thrown when a file type is not valid
 * 
 * @author damonli
 * @since 2.5.0
 */
public class FileTypeNotValidException extends Exception {

	/**
	 * Constructor
	 * 
	 * @param fileType the file type which was not valid
	 */
	public FileTypeNotValidException(String fileType) {
		super(fileType + " is not a valid file type");
	}
}
