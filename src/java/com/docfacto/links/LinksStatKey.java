package com.docfacto.links;

/**
 * List of statistic keys which are listed in the xml
 * 
 * @author dhudson - created 12 Apr 2013
 * @since 2.2
 * @docfacto.adam ignore
 */
public interface LinksStatKey {

    public static final String SOURCE_FILES_PROCESSED_KEY = "source-files-processed";

    public static final String DOC_FILES_PROCESSED_KEY = "doc-files-processed";

    public static final String SOURCE_LINES_KEY = "source-lines";

    public static final String CODE_LINES_KEY = "code-lines";

    public static final String DOC_LINES_KEY = "doc-lines";

    public static final String COMMENT_LINES_KEY = "comment-lines";

    public static final String DOC_LINK_FOUND_KEY = "doc-link-found";

    public static final String SOURCE_LINK_FOUND_KEY = "source-link-found";
    
    public static final String LINKS_WITH_UNKNOWN_TYPE = "links-with-unknown-type";

    public static final String DOC_LINK_TAG_NO_URI_KEY = "doc-link-tag-no-uri";

    public static final String SOURCE_LINK_TAG_NO_URI_KEY = "source-link-tag-no-uri";

    public static final String SOURCE_LINK_TAG_INVALID_URI_KEY = "source-link-invalid-uri";

    public static final String DOC_LINK_TAG_INVALID_URI_KEY = "doc-link-invalid-uri";

    public static final String SOURCE_BROKEN_LINK_KEY = "source-broken-link";

    public static final String UNKNOWN_FILE_TYPE = "unknown-file-type";

    public static final String DOC_BROKEN_LINK_KEY = "doc-broken-link";

    public static final String DOC_LINK_VERSION_MISMATCH_KEY = "doc-link-version-mismatch";

    public static final String SOURCE_LINK_VERSION_MISMATCH_KEY = "source-link-version-mismatch";

    public static final String SOURCE_LINK_MATCHED_KEY = "source-link-matched";

    public static final String DOC_LINK_MATCHED_KEY = "doc-link-matched";

    public static final String SOURCE_FILES_IO_EXCEPTION = "source-ioexception";

    public static final String DOC_FILES_IOEXCEPTION = "doc-ioexception";

    public static final String SOURCE_FILES_WITH_DOC_LINK = "source-files-with-doc-link";
    
    public static final String DOC_FILES_WITH_SOURCE_LINK = "doc-files-with-source-link";
    
    public static final String CODE_LINES_LINKED_TO_DOC = "lines-of-code-linked-to-doc";
    
    public static final String DOC_LINES_LINKED_TO_CODE = "lines-of-doc-linked-to-source";
    
    public static final String LINKS_ERROR = "links-errors";
    
    public static final String LINKS_WARNING = "links-warnings";
    
    public static final String LINKS_INFO = "links-info";
}
