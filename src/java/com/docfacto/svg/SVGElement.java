/*
 * @author dhudson -
 * Created 22 May 2012 : 08:10:12
 */

package com.docfacto.svg;

import com.docfacto.common.NameValuePair;
import com.docfacto.xml.Element;

/**
 * Wrapper class for a svg simple element
 * 
 * @author dhudson - created 22 May 2012
 * @since 2.0
 */
public abstract class SVGElement extends Element {

    /**
     * Abstract class for SVGElements.
     * 
     * @param value element value
     * @since 2.0
     */
    public SVGElement(String value) {
        super(value);
    }

    /**
     * Abstract class of SVGElements
     * 
     * @since 2.0
     */
    public SVGElement() {
        super(null);
    }

    /**
     * Sets the <code>height</code> attribute
     * 
     * @param height value
     * @since 2.0
     */
    public void setHeight(int height) {
        addAttribute(new NameValuePair("height",Integer.toString(height)));
    }

    /**
     * Returns the attribute value <i>height</i> as an int or Integer.MIN_VALUE
     * if the attribute is not set
     * 
     * @return height value
     * @since 2.0
     */
    public int getHeight() {
        return getIntAttributeValue("height");
    }

    /**
     * Sets the <i>width</i> attribute
     * 
     * @param width value
     * @since 2.0
     */
    public void setWidth(int width) {
        addAttribute(new NameValuePair("width",Integer.toString(width)));
    }

    /**
     * Returns the attribute value <i>width</i> as an int or Integer.MIN_VALUE
     * if the attribute is not set
     * 
     * @return width value
     * @since 2.0
     */
    public int getWidth() {
        return getIntAttributeValue("width");
    }

    /**
     * Set the class attribute
     * 
     * @param value to set
     * @since 2.5
     */
    public void setClass(String value) {
        addAttribute("class",value);
    }

    /**
     * Sets the <i>x</i> attribute for this element
     * 
     * @param x co-ordinate
     * @since 2.0
     */
    public void setX(int x) {
        addAttribute(new NameValuePair("x",Integer.toString(x)));
    }

    /**
     * Sets the <i>y</i> attribute for the element
     * 
     * @param y co-ordinate
     * @since 2.0
     */
    public void setY(int y) {
        addAttribute(new NameValuePair("y",Integer.toString(y)));
    }

}
