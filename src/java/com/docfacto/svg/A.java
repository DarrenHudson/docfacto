/* 
 * @author dhudson - 
 * Created 4 Jun 2012 : 12:10:22
 */

package com.docfacto.svg;

/**
 * Class to wrap the {@code a} SVG element
 *
 * @author dhudson - created 4 Jun 2012
 * @since 2.0
 */
public class A extends SVGElement {

    /**
     * Create an object that represents the {@code a} SVG element
     */
    public A() {
    }
    
    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return "a";
    }

    /**
     * A convenience method to add an attribute of <i>xlink:href</i>
     *
     * @param url to the external resource
     * @since 2.0
     */
    public void setXlinkHref(String url) {
        addAttribute("xlink:href",url);
    }
}
