/*
 * @author dhudson -
 * Created 4 Jun 2012 : 12:06:30
 */

package com.docfacto.svg;

/**
 * Class to wrap the {@code symbol} element
 *
 * @author dhudson - created 4 Jun 2012
 * @since 2.0
 */
public class Symbol extends SVGElement {

    /**
     * Create a new instance of {@code Symbol}.
     */
    public Symbol(){
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return "symbol";
    }

}
