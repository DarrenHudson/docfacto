package com.docfacto.svg;

/**
 * Simple wrapper class to represent a {@code circle} svg element
 * 
 * @author dhudson - created 22 May 2012
 * @since 2.0
 */
public class Circle extends SVGElement {

    /**
     * Create a new instance of <code>Circle</code>.
     */
    public Circle() {
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return "circle";
    }
}
