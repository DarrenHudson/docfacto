package com.docfacto.svg;

/**
 * Wrapper class for the {@code line} svg element
 * 
 * @author dhudson - created 22 May 2012
 * @since 2.0
 */
public class Line extends SVGElement {

    /**
     * Creates a new instance of {@code Line}.
     * 
     * @param x1 attribute defines the start of the line on the x-axis
     * @param y1 attribute defines the start of the line on the y-axis
     * @param x2 attribute defines the end of the line on the x-axis
     * @param y2 attribute defines the end of the line on the y-axis
     */
    public Line(int x1,int y1,int x2,int y2) {
        setX1(x1);
        setY1(y1);
        setX2(x2);
        setY2(y2);
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return "line";
    }

    /**
     * Set the attribute defines the start of the line on the x-axis
     * 
     * @param x1 value
     * @since 2.0
     */
    public void setX1(int x1) {
        addAttribute("x1",Integer.toString(x1));
    }

    /**
     * Set the attribute defines the start of the line on the y-axis
     * 
     * @param y1 value
     * @since 2.0
     */
    public void setY1(int y1) {
        addAttribute("y1",Integer.toString(y1));
    }

    /**
     * Set the attribute defines the end of the line on the x-axis
     * 
     * @param x2 value
     * @since 2.0
     */
    public void setX2(int x2) {
        addAttribute("x2",Integer.toString(x2));
    }

    /**
     * Set the attribute defines the end of the line on the y-axis
     * 
     * @param y2 value
     * @since 2.0
     */
    public void setY2(int y2) {
        addAttribute("y2",Integer.toString(y2));
    }

    /**
     * Helper method which sets a style attribute "stroke:grey"
     * 
     * @since 2.0
     */
    public void setGrey() {
        addStyleAttribute("stroke: grey");
    }

    /**
     * Helper method which sets a style attribute "stoke:black"
     * 
     * @since 2.0
     */
    public void setBlack() {
        addStyleAttribute("stroke: black");
    }
}
