/*
 * @author dhudson -
 * Created 4 Jun 2012 : 12:04:44
 */

package com.docfacto.svg;

/**
 * Class to wrap the {@code defs} SVG element
 *
 * @author dhudson - created 4 Jun 2012
 * @since 2.0
 */
public class Defs extends SVGElement {

    /**
     * Create a new instance of <code>Defs</code>.
     */
    public Defs() {
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return "defs";
    }

}
