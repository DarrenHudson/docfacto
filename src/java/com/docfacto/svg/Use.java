/*
 * @author dhudson -
 * Created 4 Jun 2012 : 12:24:19
 */

package com.docfacto.svg;

/**
 * Class to encapsulate the {@code use} SVG element
 *
 * @author dhudson - created 4 Jun 2012
 * @since 2.0
 */
public class Use extends SVGElement {

    /**
     * Create a new instance of <code>Use</code>.
     */
    public Use() {
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return "use";
    }

    /**
     * A convenience method to add an attribute of <i>xlink:href</i>
     *
     * @param url to the external resource
     * @since 2.0
     */
    public void setXlinkHref(String url) {
        addAttribute("xlink:href",url);
    }
}
