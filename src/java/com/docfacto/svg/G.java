package com.docfacto.svg;

/**
 * Simple wrapper class to represent a {@code g} svg element
 * 
 * @author dhudson - created 22 May 2012
 * @since 2.0
 */
public class G extends SVGElement {

    /**
     * Create a new instance of <code>G</code>.
     */
    public G() {
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return "g";
    }

}