/*
 * @author dhudson -
 * Created 20 May 2012 : 02:38:39
 */

package com.docfacto.svg;

/**
 * Wrapper class for the <code>title</code> svg element
 * 
 * @author dhudson - created 22 May 2012
 * @since 2.0
 */
public class Title extends SVGElement {

    /**
     * Create a new instance of <code>Title</code>.
     */
    public Title() {
    }

    /**
     * Create a new instance of <code>Title</code> with the given value.
     * @param value element value
     */
    public Title(String value) {
        super(value);
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return "title";
    }

}
