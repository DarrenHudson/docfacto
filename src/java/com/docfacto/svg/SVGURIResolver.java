package com.docfacto.svg;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;

/**
 * This class helps resolve the svg11.dtd, as common with SVG files, then tend
 * to have this file declared in local scope and not as a well form URL. If that
 * is the case then this URIResolver will use the svg11.dtd shipped in this jar.
 * 
 * 
 * @author dhudson - created 21 May 2012
 * @since 2.0
 */
public class SVGURIResolver implements EntityResolver {

    /**
     * A constant to the SVG dtd resource {@value}
     */
    private static final String SVG_DTD =
    "com/docfacto/svg/resources/svg11.dtd";

    /**
     * Create a new instance of <code>SVGURIResolver</code>.
     */
    public SVGURIResolver() {
    }

    /**
     * @see org.xml.sax.EntityResolver#resolveEntity(java.lang.String,
     * java.lang.String)
     */
    @Override
    public InputSource resolveEntity(String publicId,String systemId)
    throws SAXException, IOException {

        // Load the dtd from the internal resource
        if (systemId.endsWith("svg11.dtd")) {

            try {
                final InputStream is = IOUtils.getResourceAsStream(SVG_DTD);
                final InputStreamReader reader = new InputStreamReader(is);
                return new InputSource(reader);

            }
            catch (final DocfactoException ex) {
                throw new IOException(
                    "Unable to load svg11.dtd from internal resources ",ex);
            }
        }

        return null;
    }

}
