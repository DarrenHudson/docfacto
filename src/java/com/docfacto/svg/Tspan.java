/*
 * @author dhudson -
 * Created 26 May 2012 : 08:09:48
 */

package com.docfacto.svg;

/**
 * Wrapper class for the {@code tspan} SVG Element.
 * 
 * @author dhudson - created 5 Jul 2012
 * @since 2.0
 */
public class Tspan extends SVGElement {

    /**
     * Create a new instance of <code>Tspan</code>.
     */
    public Tspan() {
    }

    /**
     * Create a new instance of <code>Tspan</code> with the given value.
     * 
     * @param value element value
     */
    public Tspan(String value) {
        super(value);
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return "tspan";
    }

}
