/*
 * @author dhudson -
 * Created 19 May 2012 : 12:04:35
 */

package com.docfacto.svg;

import com.docfacto.svg.Text.Alignment;
import com.docfacto.xml.XMLElement;

/**
 * Simple grid so when drawing in Java, able to see X and Y co-ords
 *<P>
 * {@docfacto.todo  Make all this configurable, going to be useful in the future }
 *
 * @author dhudson - created 19 May 2012
 * @since 2.0
 */
public class Grid implements XMLElement {


    private final G theGridGroup;

    /**
     * Create a new instance of <code>Grid</code>.
     */
    public Grid() {
        theGridGroup = new G();
        theGridGroup.setID("Grid");
        buildGrid();
    }

    /**
     * Build the grid
     *
     * @since 2.0
     */
    private void buildGrid() {
        for (int x = 10;x<1000;x += 10) {
            final Line line = new Line(x,0,x,1000);
            line.setGrey();
            // Add a title to the line for a quick way of getting a tool tip
            line.addElement(new Title(Integer.toString(x)));
            theGridGroup.addElement(line);
            if (x%20==0) {
                final Text text = new Text(Integer.toString(x));
                text.setX(x);
                text.setAlignment(Alignment.MIDDLE);
                text.setFontSize(12);

                if (x%40==0) {
                    text.setY(980);
                }
                else {
                    text.setY(1000);
                }
                theGridGroup.addElement(text);
            }
        }

        for (int y = 10;y<1000;y += 10) {
            final Line line = new Line(0,y,1000,y);
            line.setGrey();
            line.addElement(new Title(Integer.toString(y)));

            theGridGroup.addElement(line);
            if (y%20==0) {
                final Text text = new Text(Integer.toString(y));
                text.setY(y+4);
                text.setAlignment(Alignment.MIDDLE);
                text.setFontSize(12);

                if (y%40==0) {
                    text.setX(980);
                }
                else {
                    text.setX(1000);
                }
                theGridGroup.addElement(text);
            }
        }
    }

    /**
     * @see com.docfacto.xml.XMLElement#toXML()
     */
    @Override
    public String toXML() {
        return theGridGroup.toXML();
    }

    @Override
    public void setParent(XMLElement parent) {
        // TODO Auto-generated method stub

    }

    @Override
    public XMLElement getParent() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean containsElementWithName(String elementName) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String getElementName() {
        // TODO Auto-generated method stub
        return null;
    }

}
