package com.docfacto.svg;

/**
 * Wrapper class for the {@code rect} svg element
 * 
 * @author dhudson - created 22 May 2012
 * @since 2.0
 */
public class Rect extends SVGElement {

    /**
     * Create a new instance of {@code Rect}.
     */
    public Rect() {
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return "rect";
    }

}
