package com.docfacto.svg;

import com.docfacto.common.NameValuePair;

/**
 * Wrapper class for the {@code text} svg element
 * 
 * @author dhudson - created 22 May 2012
 * @since 2.0
 */
public class Text extends SVGElement {

    /**
     * Create a new instance of <code>Text</code>.
     */
    public Text() {
    }

    /**
     * Create a new instance of <code>Text</code>.
     * 
     * @param value element value
     */
    public Text(String value) {
        super(value);
    }

    /**
     * Text alignment type.
     * 
     * @author dhudson - created 22 May 2012
     * @since 2.0
     */
    public enum Alignment {
        /**
         * Anchor start
         */
        START,

        /**
         * Anchor middle
         */
        MIDDLE,

        /**
         * Anchor end
         */
        END;
    }

    /**
     * Text style types
     * 
     * @author dhudson - created 22 May 2012
     * @since 2.0
     */
    public enum Style {
        /**
         * Font weight bold
         */
        BOLD,

        /**
         * Font style italic
         */
        ITALIC,

        /**
         * Text decoration underline
         */
        UNDER_LINE
    }

    /**
     * Helper method to set the style attributes
     * 
     * @param style required style
     * @since 2.0
     */
    public void setStyle(Style style) {
        switch (style) {
        case BOLD:
            addStyleAttribute("font-weight:bold");
            break;

        case ITALIC:
            addStyleAttribute("font-style:italic");
            break;

        case UNDER_LINE:
            addStyleAttribute("text-decoration:underline");
            break;
        }
    }

    /**
     * Helper method to set the style attributes
     * 
     * @param alignment text alignment
     * @since 2.0
     */
    public void setAlignment(Alignment alignment) {
        switch (alignment) {
        case START:
            addStyleAttribute("text-anchor:start");
            break;

        case MIDDLE:
            addStyleAttribute("text-anchor:middle");
            break;

        case END:
            addStyleAttribute("text-anchor:end");
            break;
        }
    }

    /**
     * Helper method to set the <i>textLength</i> attribute, this method also
     * sets <i>lengthAdjust:spaceAndGlyphs</i>
     * 
     * @param size of the text
     * @since 2.0
     */
    public void setTextSize(int size) {
        addAttribute("textLength",Integer.toString(size));
        addAttribute("lengthAdjust","spaceAndGlyphs");
    }

    /**
     * Helper method to set the style attribute <i>font-size</i>
     * 
     * @param size of the fonts
     * @since 2.0
     */
    public void setFontSize(int size) {
        addStyleAttribute("font-size: "+Integer.toString(size));
    }

    /**
     * Helper class to the the attribute <i>xml:space</i> to <i>preserve</i>
     * 
     * @since 2.0
     */
    public void preserve() {
        addAttribute(new NameValuePair("xml:space","preserve"));
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return "text";
    }

}
