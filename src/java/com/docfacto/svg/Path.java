/*
 * @author dhudson -
 * Created 4 Jun 2012 : 12:31:41
 */

package com.docfacto.svg;

/**
 * Wrapper class for the {@code path} SVG Element
 * 
 * @author dhudson - created 4 Jun 2012
 * @since 2.0
 */
public class Path extends SVGElement {

    /**
     * Create a new instance of {@code Path}.
     */
    public Path() {
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return "path";
    }

    /**
     * Convenience method to set the {@code d} attribute
     * 
     * @param value if the {@code d} attribute
     * @since 2.0
     */
    public void setD(String value) {
        addAttribute("d",value);
    }
}
