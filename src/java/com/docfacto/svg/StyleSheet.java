package com.docfacto.svg;

import com.docfacto.common.Platform;

/**
 * Style Sheet node.
 * 
 * This will add the correct attributes and encapsulate the styles in CDATA
 * tags.
 * 
 * @author dhudson - created 10 Dec 2013
 * @since 2.5
 */
public class StyleSheet extends SVGElement {

    private static final String START_CDATA = "<![CDATA["+
        Platform.LINE_SEPARATOR;
    private static final String END_CDATA = "]]>"+Platform.LINE_SEPARATOR;

    /**
     * Constructor.
     * 
     * @since 2.5
     */
    public StyleSheet() {
        addAttribute("type","text/css");
        addAttribute("xml:space","preserve");
    }

    /**
     * Wraps the contents in CData tags
     * 
     * @param contents to set
     * @since 2.5
     */
    public void setStyle(String contents) {
        StringBuilder builder =
            new StringBuilder(contents.length()+START_CDATA.length()+
                END_CDATA.length());
        builder.append(START_CDATA);
        builder.append(contents);
        builder.append(END_CDATA);
        addTextElement(builder.toString());
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return "style";
    }

}
