package com.docfacto.svg;

import java.io.BufferedReader;
import java.io.IOException;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.xml.XMLDocument;

/**
 * Class to encapsulate a SVG XML Document. Note not a true document in the
 * sense of <code>Document</code> This class will automatically add in the
 * DOCTYPE elements.
 * 
 * A <code>svg</code> element will automatically be created. To add elements to
 * the <code>svg</code> element use <code>addToRootNode</code>
 * 
 * @author dhudson - created 22 May 2012
 * @since 2.0
 */
public class SVGDocument extends XMLDocument {

    private static final String DOCTYPE_DECL =
    "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">";

    /**
     * Create a new instance of <code>SVGDocument</code>.
     * 
     * {@docfacto.note The xml name spaces of <i>svg</i> and <i>xlink</i> will be automatically set }
     * @since 2.0
     */
    public SVGDocument() {
        theRootNode = new SVG();
        theRootNode.addAttribute("xmlns","http://www.w3.org/2000/svg");
        // Add the xlink name space
        theRootNode.addAttribute("xmlns:xlink","http://www.w3.org/1999/xlink");
    }

    /**
     * Create a style sheet element and add contents
     * 
     * @param contents of the style sheet
     * @since 2.5
     */
    public void addStyleSheet(String contents) {
        StyleSheet styleSheet = new StyleSheet();
        styleSheet.setStyle(contents);
        theRootNode.addElement(styleSheet);
    }
    
    /**
     * Add the Docfacto Logo to the Document
     * 
     * @since 2.0
     */
    public void addDocfactoLogo() {

        // In the defs element need to add a symbol element
        final Defs defs = new Defs();
        final Symbol symbol = new Symbol();

        // Add the symbol element to defs
        defs.addElement(symbol);

        symbol.setID("DocfactoLogo");

        // Add the paths to the symbol
        BufferedReader reader = null;
        try {
            // Read the path d attributes
            reader =
            IOUtils.createResourceReader("com/docfacto/svg/resources/logo");
            String line = null;

            while ((line = reader.readLine())!=null) {
                final Path p = new Path();
                p.setD(line);
                symbol.addElement(p);
            }

            // Add the defs to the root node
            theRootNode.addElement(defs);

            // Now that the symbol has all of the elements, lets add the anchor
            // and use lines in
            final A anchor = new A();
            anchor.setXlinkHref("http://www.docfacto.com");

            final Use use = new Use();
            use.setXlinkHref("#DocfactoLogo");

            anchor.addElement(use);
            // Add the anchor to the root node
            theRootNode.addElement(anchor);
        }
        catch (final DocfactoException ex) {
            ex.printStackTrace();
        }
        catch (final IOException ex) {
            ex.printStackTrace();
        }
        finally {
            // Close the resource
            if (reader!=null) {
                IOUtils.close(reader);
            }
        }
    }

    /**
     * Return the root <code>svg</code> node
     * 
     * @return the root node of the SVG document
     * @since 2.0
     */
    public SVG getRootNode() {
        return (SVG)theRootNode;
    }

    /**
     * @see com.docfacto.xml.XMLDocument#getDocumentDeclaration()
     */
    @Override
    public String getDocumentDeclaration() {
        return DOCTYPE_DECL;
    }
}
