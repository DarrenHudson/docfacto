package com.docfacto.svg;

/**
 * Wrapper class for a {@code svg} element
 * 
 * @author dhudson - created 22 May 2012
 * @since 2.0
 */
public class SVG extends SVGElement {

    /**
     * Create a new instance of {@code SVG}.
     */
    public SVG() {
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return "svg";
    }
}
