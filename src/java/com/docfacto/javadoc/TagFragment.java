package com.docfacto.javadoc;

/**
 * A Tag Fragment can be a section of text or a Tag Parser which can also have a collection of fragments 
 *
 * @author dhudson - created 16 Aug 2013
 * @since 2.4
 */
public interface TagFragment {

    /**
     * Return the fragment text
     * 
     * @return the text for the fragment, this will also include any child fragments as well
     * @since 2.4
     */
    public CharSequence getTagText();
    
}
