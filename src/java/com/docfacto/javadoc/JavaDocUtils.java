/*
 * @author dhudson -
 * Created 25 Jul 2012 : 12:11:23
 */

package com.docfacto.javadoc;

import java.io.File;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.common.StringUtils;

/**
 * Class to provide common {@code JavaDoc} utilities.
 * 
 * @author dhudson - created 26 Jul 2012
 * @since 2.0
 */
public class JavaDocUtils {

    /**
     * Config option that specifies the XML Config file {@value}
     */
    public static final String CONFIG_OPTION = "-config";

    /**
     * Config option for output file {@value}
     */
    public static final String OUTPUT_OPTION = "-output";

    /**
     * Constant {@value}
     */
    public static final String SOURCE_PATH_OPTION = "-sourcepath";

    /**
     * Constant {@value}
     */
    public static final String BEGIN_JAVADOC_COMMENT = "/**";

    /**
     * Constant {@value}
     */
    public static final String END_JAVADOC_COMMENT = "*/";

    /**
     * Constant {@value}
     */
    public static final String PACKAGE_INFO_JAVA = "package-info.java";

    /**
     * Constant {@value}
     */
    public static final String PACKAGE_HTML = "package.html";

    /**
     * Static methods only..
     */
    private JavaDocUtils() {
    }

    /**
     * Process the options provided to the {@code JavaDoc} processor and get the
     * Docfacto config file.
     * 
     * @param options from the JavaDoc processor
     * @return the config file
     * @throws DocfactoException if unable to find the config option or file
     * @since 2.0
     */
    public static File getConfigFile(String[][] options)
    throws DocfactoException {

        for (final String[] option:options) {
            if (CONFIG_OPTION.equals(option[0])) {

                if (option.length==1) {
                    throw new DocfactoException("No file specified for the "+
                        CONFIG_OPTION);
                }

                final File configFile = new File(option[1]);

                if (!configFile.exists()) {
                    throw new DocfactoException("Can't find config file "+
                        option[1]);
                }

                if (!configFile.canRead()) {
                    throw new DocfactoException("Can't read config file "+
                        option[1]);
                }
                // OK we got this far, so lets return the file..
                return configFile;

            }
        }

        // If we get here the option was not specified
        throw new DocfactoException(CONFIG_OPTION+" was not specified");
    }

    /**
     * Get the output option from the options
     * 
     * @param options to process
     * @return a file, or null
     * @throws DocfactoException if the output file not specified
     * @since 2.2
     */
    public static File getOutputFile(String[][] options)
    throws DocfactoException {
        for (final String[] option:options) {
            if (OUTPUT_OPTION.equals(option[0])) {

                if (option.length==1) {
                    throw new DocfactoException("No file specified for the "+
                        OUTPUT_OPTION);
                }

                final File configFile = new File(option[1]);

                // OK we got this far, so lets return the file..
                return configFile;

            }
        }

        return null;
    }

    /**
     * Scan the options and return the source path. The path is passed in via
     * the normal javadoc engine, it must exist or the javadoc engine fails.
     * 
     * @param options
     * @return the source path
     * @since 2.2
     */
    public static File getSourcePath(String[][] options) {
        for (final String[] option:options) {
            if (SOURCE_PATH_OPTION.equals(option[0])) {
                return new File(option[1]);
            }
        }

        return null;
    }

    /**
     * Process the Package.html and parse any tags
     * 
     * @param packageFile to process
     * @return a comment parser, which may contain tags
     * @throws DocfactoException if unable to read the file
     * @since 2.2
     */
    public static String readPackageHtml(File packageFile)
    throws DocfactoException {
        final String contents = IOUtils.readFileAsString(packageFile);
        final int bodyIndex = StringUtils.indexOfIgnoreCase("<body",contents);
        if (bodyIndex==-1) {
            throw new DocfactoException("Unable to find start body tag");
        }
        final int endIndex =
            StringUtils.indexOfIgnoreCase("</body",contents);

        if (endIndex==-1) {
            throw new DocfactoException("Unable to find end body tag");
        }

        try {
            return contents.substring(bodyIndex,endIndex);
        }
        catch (final IndexOutOfBoundsException ex) {
            throw new DocfactoException("Unable to parse html file");
        }
    }

    /**
     * Return the index to the start of the comment within the package file
     * 
     * @param packageFile to process
     * @return the index of the body tag
     * @throws DocfactoException if unable to read the file or find body tags
     * @since 2.4
     */
    public static int getPackageCommentStart(File packageFile)
    throws DocfactoException {
        final String contents = IOUtils.readFileAsString(packageFile);
        final int bodyIndex = StringUtils.indexOfIgnoreCase("<body",contents);
        if (bodyIndex==-1) {
            throw new DocfactoException("Unable to find start body tag");
        }
        final int endIndex =
            StringUtils.indexOfIgnoreCase("</body",contents);

        if (endIndex==-1) {
            throw new DocfactoException("Unable to find end body tag");
        }

        return endIndex;
    }

    /**
     * Remove the stars from the raw comment text
     * 
     * @param textWithStars
     * @return Comment without the '*'s
     * @since 2.2
     */
    public static String removeDocLineIntros(String textWithStars) {
        final String lineBreakGroup = "(\\r\\n?|\\n)";
        final String noBreakSpace = "[^\r\n&&\\s]";
        return textWithStars.replaceAll(
            lineBreakGroup+noBreakSpace+"*\\*","$1");
    }

    /**
     * Look to see if there is a package-info.java or a package.html, if one is
     * found, return the comments
     * 
     * @param packagePath to process
     * @return the raw text comment
     * @since 2.2
     */
    // public static String getPackageComments(String packagePath) {
    //
    // }
}
