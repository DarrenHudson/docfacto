/*
 * @author dhudson -
 * Created 4 Mar 2013 : 12:08:06
 */

package com.docfacto.javadoc;

import java.util.ArrayList;
import java.util.List;

import com.docfacto.common.StringUtils;

/**
 * Comment contains all information in comment part. It allows users to get
 * first sentence of this comment, get comment for different tags...
 * 
 * <p>
 * {@docfacto.system This is based off com.sun.tools.javadoc.Comment}
 * </p>
 * 
 * @author dhudson - created 4 Mar 2013
 * @since 2.2
 */
public class CommentParser {

    private final int theStartLine;
    private int theEndLine;
    private int theCommentOffset;
    private boolean hasParsed;

    /**
     * sorted comments with different tags.
     */
    private final ArrayList<TagParser> theTagList = new ArrayList<TagParser>(3);

    /**
     * text minus any tags.
     */
    private String theText;

    /**
     * Constructor.
     * 
     * @param startLine start line of the comment
     * @since 2.4
     */
    public CommentParser(int startLine) {
        theStartLine = startLine;
    }

    /**
     * Parse the content into a list of {@code TagParser}'s
     * 
     * @param commentString comment to process, this should have all of the '*'s
     * removed.
     * @since 2.2
     */
    public void parse(final String commentString) {
        hasParsed = true;
        /**
         * Separate the comment into the text part and zero to N tags. Simple
         * state machine is in one of three states:
         * 
         * <pre>
         * IN_TEXT: parsing the comment text or tag text.
         * TAG_NAME: parsing the name of a tag.
         * TAG_GAP: skipping through the gap between the tag name and
         * the tag text.
         * </pre>
         */
        class CommentStringParser {
            /**
             * The entry point to the comment string parser
             */
            void parseCommentStateMachine() {
                final int IN_TEXT = 1;
                final int TAG_GAP = 2;
                final int TAG_NAME = 3;
                int state = TAG_GAP;
                boolean newLine = true;
                String tagName = null;
                int tagStart = 0;
                int textStart = 0;
                int lastNonWhite = -1;
                final int len = commentString.length();
                // As /** is taken out
                int lineNo = theStartLine+1;

                for (int inx = 0;inx<len;++inx) {
                    final char ch = commentString.charAt(inx);
                    final boolean isWhite = Character.isWhitespace(ch);
                    switch (state) {
                    case TAG_NAME:
                        if (isWhite) {
                            tagName = commentString.substring(tagStart,inx);
                            state = TAG_GAP;
                        }
                        break;
                    case TAG_GAP:
                        if (isWhite) {
                            break;
                        }
                        textStart = inx;
                        state = IN_TEXT;
                        /* fall thru */
                    case IN_TEXT:
                        if (newLine&&ch=='@') {
                            parseCommentComponent(tagName,textStart,
                                lastNonWhite+1,lineNo);
                            tagStart = inx;
                            state = TAG_NAME;
                        }
                        break;
                    }

                    if (ch=='\n') {
                        newLine = true;
                        lineNo++;
                    }
                    else if (!isWhite) {
                        lastNonWhite = inx;
                        newLine = false;
                    }
                }
                // Finish what's currently being processed
                switch (state) {
                case TAG_NAME:
                    tagName = commentString.substring(tagStart,len);
                    /* fall thru */
                case TAG_GAP:
                    textStart = len;
                    /* fall thru */
                case IN_TEXT:
                    parseCommentComponent(tagName,textStart,lastNonWhite+1,
                        lineNo);
                    break;
                }
            }

            /**
             * Save away the last parsed item.
             */
            void parseCommentComponent(String tagName,
            int from,int upto,int lineNo) {
                final String tx =
                    upto<=from ? "" : commentString.substring(from,upto);
                if (tagName==null) {
                    theText = tx;
                }
                else {
                    // Remove the @
                    final TagParser parser =
                        new TagParser(tagName.substring(1),tx);
                    parser.setLineNumber(lineNo-2);
                    theTagList.add(parser);
                }
            }
        }

        new CommentStringParser().parseCommentStateMachine();
    }

    /**
     * Return the text of the comment.
     * 
     * This can be null, if the parser method hasn't been called.
     * 
     * @return the comment text
     * @since 2.2
     */
    public String commentText() {
        return theText;
    }

    /**
     * Fetch the list of tags for this comment, which may be empty
     * 
     * @return the list of tags
     * @since 2.2
     */
    public List<TagParser> getTags() {
        return theTagList;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        if (!hasParsed) {
            return super.toString();
        }

        StringBuilder builder = new StringBuilder(50);
        builder.append("[");
        builder.append(theStartLine);
        builder.append(":");
        builder.append(theEndLine);
        builder.append("] ");

        builder.append(StringUtils.elipseString(40,theText));

        return builder.toString();
    }

    /**
     * See if there is a tag of a given name
     * 
     * @param tagName to search for
     * @return true if the tag exists
     * @since 2.2
     */
    public boolean hasTag(String tagName) {
        for (final TagParser parser:theTagList) {
            if (tagName.equals(parser.getTagName())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Return tags of the specified kind in this comment.
     * 
     * @param tagname to find
     * @return a List of TagParsers which may be empty, but not null
     * @since 2.2
     */
    public List<TagParser> getTags(String tagname) {
        final ArrayList<TagParser> found = new ArrayList<TagParser>(3);

        for (final TagParser tagParser:theTagList) {
            if (tagname.equals(tagParser.getTagName())) {
                found.add(tagParser);
            }
        }
        return found;
    }

    /**
     * Return the start line of this comment.
     * 
     * @return the start line of this comment
     * @since 2.4
     */
    public int getStartLine() {
        return theStartLine;
    }

    /**
     * Set the end comment end line number within the source file
     * 
     * @param end line number
     * @since 2.2
     */
    public void setEndLine(int end) {
        theEndLine = end;
    }

    /**
     * Line number of the end of the comment
     * 
     * @return the end line if set
     * @since 2.2
     */
    public int getEndLine() {
        return theEndLine;
    }

    /**
     * Set the offset (indent position) of this comment
     * 
     * @param offset value
     * @since 2.4
     */
    public void setCommentOffset(int offset) {
        theCommentOffset = offset;
    }

    /**
     * Return the positional offset of the comment
     * 
     * @return the positional offset of the comment
     * @since 2.4
     */
    public int getCommentOffset() {
        return theCommentOffset;
    }

    /**
     * Return array of tags with text and inline See Tags for a Doc comment.
     */
    // static Tag[] getInlineTags(String inlinetext) {
    // final ListBuffer<Tag> taglist = new ListBuffer<Tag>();
    // int delimend = 0, textstart = 0;
    // final int len = inlinetext.length();
    //
    // if (len==0) {
    // return taglist.toArray(new Tag[taglist.length()]);
    // }
    // while (true) {
    // int linkstart;
    // if ((linkstart = inlineTagFound(inlinetext,
    // textstart))==-1) {
    // taglist.append(new TagImpl(holder,"Text",
    // inlinetext.substring(textstart)));
    // break;
    // }
    // else {
    // int seetextstart = linkstart;
    // for (int i = linkstart;i<inlinetext.length();i++) {
    // final char c = inlinetext.charAt(i);
    // if (Character.isWhitespace(c)||
    // c=='}') {
    // seetextstart = i;
    // break;
    // }
    // }
    // final String linkName =
    // inlinetext.substring(linkstart+2,seetextstart);
    // // Move past the white space after the inline tag name.
    // while (Character.isWhitespace(inlinetext.
    // charAt(seetextstart))) {
    // if (inlinetext.length()<=seetextstart) {
    // taglist.append(new TagImpl(holder,"Text",
    // inlinetext.substring(textstart,seetextstart)));
    // docenv.warning(holder,
    // "tag.Improper_Use_Of_Link_Tag",
    // inlinetext);
    // return taglist.toArray(new Tag[taglist.length()]);
    // }
    // else {
    // seetextstart++;
    // }
    // }
    // taglist.append(new TagImpl(holder,"Text",
    // inlinetext.substring(textstart,linkstart)));
    // textstart = seetextstart; // this text is actually seetag
    // if ((delimend = findInlineTagDelim(inlinetext,textstart))==-1) {
    // // Missing closing '}' character.
    // // store the text as it is with the {@link.
    // taglist.append(new TagImpl(holder,"Text",
    // inlinetext.substring(textstart)));
    // docenv.warning(holder,
    // "tag.End_delimiter_missing_for_possible_SeeTag",
    // inlinetext);
    // return taglist.toArray(new Tag[taglist.length()]);
    // }
    // else {
    // // Found closing '}' character.
    // if (linkName.equals("see")
    // ||linkName.equals("link")
    // ||linkName.equals("linkplain")) {
    // taglist.append(new SeeTagImpl(holder,"@"+linkName,
    // inlinetext.substring(textstart,delimend)));
    // }
    // else {
    // taglist.append(new TagImpl(holder,"@"+linkName,
    // inlinetext.substring(textstart,delimend)));
    // }
    // textstart = delimend+1;
    // }
    // }
    // if (textstart==inlinetext.length()) {
    // break;
    // }
    // }
    // return taglist.toArray(new Tag[taglist.length()]);
    // }

    /**
     * Recursively find the index of the closing '}' character for an inline tag
     * and return it. If it can't be found, return -1.
     * 
     * @param inlineText the text to search in.
     * @param searchStart the index of the place to start searching at.
     * @return the index of the closing '}' character for an inline tag. If it
     * can't be found, return -1.
     */
    // private static int findInlineTagDelim(String inlineText,int searchStart)
    // {
    // int delimEnd, nestedOpenBrace;
    // if ((delimEnd = inlineText.indexOf("}",searchStart))==-1) {
    // return -1;
    // }
    // else if (((nestedOpenBrace = inlineText.indexOf("{",searchStart))!=-1)&&
    // nestedOpenBrace<delimEnd) {
    // // Found a nested open brace.
    // final int nestedCloseBrace =
    // findInlineTagDelim(inlineText,nestedOpenBrace+1);
    // return (nestedCloseBrace!=-1) ?
    // findInlineTagDelim(inlineText,nestedCloseBrace+1) :
    // -1;
    // }
    // else {
    // return delimEnd;
    // }
    // }

    /**
     * Recursively search for the string "{@" followed by name of inline tag
     * and white space, if found return the index of the text following the
     * white space. else return -1.
     */
    // private int
    // inlineTagFound(String inlinetext,int start) {
    // int linkstart;
    // if (start==inlinetext.length()||
    // (linkstart = inlinetext.indexOf("{@",start))==-1) {
    // return -1;
    // }
    // else if (inlinetext.indexOf('}',start)==-1) {
    // // Missing '}'.
    // return -1;
    // }
    // else {
    // return linkstart;
    // }
    // }

    /**
     * Return array of tags for the locale specific first sentence in the text.
     */
    // static Tag[] firstSentenceTags(String text) {
    // final DocLocale doclocale = holder.env.doclocale;
    // return getInlineTags(holder,
    // doclocale.localeSpecificFirstSentence(holder,text));
    // }

}
