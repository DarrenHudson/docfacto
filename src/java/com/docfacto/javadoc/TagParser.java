/*
 * @author dhudson -
 * Created 21 Feb 2013 : 09:25:19
 */

package com.docfacto.javadoc;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;

import com.docfacto.common.NameValuePair;
import com.docfacto.common.TagletUtils;

/**
 * A Docfacto Taglet understands that attributes exist.
 * 
 * {@docfacto.note title="Tag Format"<p>
 * attributes must come first in the supplied text and the rest is treated as text </p>}
 * 
 * <p>
 * {@docfacto.system At release 2.4, has changed to understand nested inline
 * attributes }
 * </p>
 * 
 * @author dhudson - created 21 Feb 2013
 * @since 2.2
 */
public class TagParser implements TagFragment {

    /**
     * Constant {@value}
     */
    public static final String URI_ATTRIBUTE = "uri";
    /**
     * Constant {@value}
     */
    public static final String TITLE_ATTRIBUTE = "title";
    /**
     * Constant {@value}
     */
    public static final String VERSION_ATTRIBUTE = "version";
    /**
     * Constant {@value}
     */
    public static final String METADATA_ATTRIBUTE = "metadata";

    /**
     * Constant {@value}
     */
    public static final String LINK_TO_ATTRIBUTE = "link-to";

    /**
     * Constant {@value}
     */
    public static final String TYPE_ATTRIBUTE = "type";
    
    /**
     * Constant {@value}
     */
    public static final String ALT_ATTRIBUTE = "alt";

    /**
     * Constant {@value}
     */
    public static final String LINK_KEY_ATTRIBUTE = "key";

    // No more than three attributes at the moment.
    private final HashMap<String,TagAttribute> theAttributes =
        new HashMap<String,TagAttribute>(3);

    private int theTextIndex = 0;

    private final CharSequence theTagText;

    private boolean isInLine = false;

    private String theTagName;

    private Object theAttachment;
    
    /**
     * If set, this is the line number within the comment that this tag was
     * present
     */
    private int theLineNumber;

    /**
     * Create a new instance of <code>TagParser</code>.
     * 
     * @param name of the tag
     * @param tagText to process
     * @since 2.2
     */
    public TagParser(String name,CharSequence tagText) {
        theTagText = tagText;
        theTagName = name;        
        parse(tagText);
    }

    /**
     * Create a new instance of <code>TagParser</code>.
     * 
     * @param tagText to process
     * @since 2.2
     */
    public TagParser(CharSequence tagText) {
        this(null,tagText);
    }

    /**
     * Set an attachment to the parser
     * 
     * @param attachment object to attach
     * @since 2.4
     */
    public void attach(Object attachment) {
        theAttachment = attachment;
    }
    
    /**
     * Return an attachment to this parser
     * 
     * @return the attachment if set else null
     * @since 2.4
     */
    public Object getAttachment() {
        return theAttachment;
    }
    
    /**
     * Return true if attributes present
     * 
     * @return true if the tag has Attributes
     * @since 2.2
     */
    public boolean hasAttributes() {
        return (theAttributes.size()>0);
    }

    /**
     * Return the attributes
     * 
     * @return a collection of attributes {@code NameValuePair} which can be
     * empty
     * @since 2.2
     */
    public Collection<TagAttribute> getAttributes() {
        return theAttributes.values();
    }

    /**
     * Returns true if the attribute is present
     * 
     * @param name of the attribute
     * @return true if the attribute exists
     * @since 2.2
     */
    public boolean hasAttribute(String name) {
        return theAttributes.containsKey(name);
    }

    /**
     * Returns the title attribute if present or {@code null}
     * 
     * @return the title attribute if present
     * @since 2.2
     */
    public String getTitleAttribute() {
        return getAttributeValue(TITLE_ATTRIBUTE);
    }

    /**
     * Check to see if the {@code title} attribute is present
     * 
     * @return true if the {@code title} attribute is present
     * @since 2.2
     */
    public boolean hasTitleAttribute() {
        return hasAttribute(TITLE_ATTRIBUTE);
    }

    /**
     * Fetch the attribute value, if the attribute exists
     * 
     * @param name of the attribute
     * @return the attribute value or null if the attribute doesn't exist
     * @since 2.2
     */
    public String getAttributeValue(String name) {
        final NameValuePair attr = theAttributes.get(name);
        if (attr==null) {
            return null;
        }

        return attr.getValue();
    }

    /**
     * Remove an attribute from the parser
     * 
     * @param name attribute to remove
     * @return true if the attribute was removed
     * @since 2.5
     */
    public boolean removeAttribute(String name) {
        TagAttribute attr =  theAttributes.remove(name);
        if(attr != null) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Attributes come first, so when all of the attributes have been processed,
     * this is where the text would start
     * 
     * @return the index of where the text start
     * @since 2.2
     */
    public int getTextIndex() {
        return theTextIndex;
    }

    /**
     * Returns the tag text after the attributes have been processed, which can
     * be all of the text if no attributes are found
     * 
     * @return the html representation of the taglet
     * @since 2.2
     */
    public CharSequence getTagText() {
        String raw = theTagText.toString();
        
        if (hasAttributes()) {
            if (theTextIndex>=theTagText.length()) {
                return "";
            }
            
            raw = theTagText.subSequence(getTextIndex(),theTagText.length()).toString();
        }
        
        return TagletUtils.removeDocLineIntros(raw);
    }

    /**
     * Add an attribute to the list, overwriting an existing one if present
     * 
     * @param name of the attribute
     * @param value of the attribute
     * @since 2.2
     */
    public void setAttribute(String name,String value) {
        theAttributes.put(name,new TagAttribute(name,value));
    }

    /**
     * Parse the tag text and lets see if there are any attributes
     * 
     * {@docfacto.todo check to see that there isn't an attribute in the text }
     * 
     * @param sequence to process
     * @since 2.2
     */
    private void parse(CharSequence sequence) {        
        StringBuilder builder = new StringBuilder(100);
        TagAttribute attr = null;
        char quoteChar = ' ';
        int quoteIndex = 0;
        char c;
        int htmlIndex = 0;

        for (int i = 0;i<sequence.length();i++) {
            c = sequence.charAt(i);
            
            switch (c) {
            case '{':
                // Lets check to see if we are a nested inline tag
//                if ((i<sequence.length()-1)&&sequence.charAt(i+1)=='@') {
//                    // We are in a nested tag..
//                    int startIndex = i+2;
//                    int endIndex = startIndex;
//                    while (endIndex<sequence.length()) {
//                        if (sequence.charAt(endIndex++)=='}') {
//                            CharSequence nested = sequence.subSequence(startIndex,endIndex-1);                            
//                            // Rest the i value
//                        }
//                    }
//                }
//                else {
                    builder.append(c);
//                }
                break;

            case '\'':
            case '"':
                // We are value parsing
                if (quoteIndex==0) {
                    quoteChar = c;
                    quoteIndex = 1;
                }
                else {
                    if (c==quoteChar&htmlIndex==0) {
                        if (attr==null) {
                            // Its just a quote ..
                            builder.append(c);
                        }
                        else {
                            // We are at the end of the value
                            attr.setValue(builder.toString().trim());

                            // Add the attribute to the element
                            theAttributes.put(attr.getName(),attr);

                            // save index, if this is the last attribute, then
                            // text
                            // will start here
                            theTextIndex = i+1;

                            // Reset the builder
                            builder = new StringBuilder(100);
                        }
                        quoteIndex = 0;
                    }
                }
                break;

            case '=':
                // Build a new attr
                if (quoteIndex==0&&htmlIndex==0) {
                    attr =
                        new TagAttribute(builder.toString().trim()
                            .toLowerCase());
                    builder = new StringBuilder(100);
                    quoteIndex = 0;
                    break;
                }
                // Again watch the fall through here...
                builder.append(c);
                break;

            case '<':
                htmlIndex++;
                builder.append(c);
                break;

            case '>':
                htmlIndex--;
                builder.append(c);
                break;
                
            default:
                builder.append(c);
                break;

            }
        }
    }

    /**
     * Set the tag name
     * 
     * @param tagName name of the tag
     * @since 2.2
     */
    public void setTagName(String tagName) {
        theTagName = tagName;
    }

    /**
     * Return the tag name if it has been set
     * 
     * @return the tag name, may be null
     * @since 2.2
     */
    public String getTagName() {
        return theTagName;
    }

    /**
     * Set the flag so that this represents an inline tag
     * 
     * @since 2.2
     */
    public void setInLine() {
        isInLine = true;
    }

    /**
     * Returns true if this has been set to an inline tag
     * 
     * @return true if the represents an inline tag
     * @since 2.2
     */
    public boolean isInLine() {
        return isInLine;
    }

    /**
     * Set the line number of the tag within the parent Javadoc
     * 
     * @param lineNo
     * @since 2.2
     */
    public void setLineNumber(int lineNo) {
        theLineNumber = lineNo;
    }

    /**
     * Return the line number within the Javadoc comment that this tag was
     * present
     * 
     * @return the line number of which this tag took place
     * @since 2.2
     */
    public int getLineNumber() {
        return theLineNumber;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder(100);
        builder.append(this.getClass().getSimpleName());
        builder.append(" : ");

        if (hasAttributes()) {
            builder.append("[");
            for (final Entry<String,TagAttribute> attr:theAttributes
                .entrySet()) {
                builder.append(attr.getValue().toString());
            }
            builder.append("]");
        }

        builder.append(" : ");
        builder.append(getTagText());

        return builder.toString();
    }
}
