package com.docfacto.javadoc;

import com.docfacto.common.NameValuePair;

/**
 * Attribute on a tag
 *
 * @author dhudson - created 16 Aug 2013
 * @since 2.4
 */
public class TagAttribute extends NameValuePair implements TagFragment {

    /**
     * Constructor.
     * 
     * @param name attribute name
     */
    public TagAttribute(String name) {
        super(name);
    }
    
    /**
     * Constructor.
     * 
     * @param name attribute name
     * @param value attribute value
     */
    public TagAttribute(String name, String value) {
        super(name,value);
    }

    /**
     * @see com.docfacto.javadoc.TagFragment#getTagText()
     */
    @Override
    public CharSequence getTagText() {
        return super.toString();
    }

}
