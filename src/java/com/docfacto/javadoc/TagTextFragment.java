package com.docfacto.javadoc;

/**
 * A bit of text within a tag
 * 
 * @author dhudson - created 19 Aug 2013
 * @since 2.4
 */
public class TagTextFragment implements TagFragment {

    private final String theText;

    /**
     * Constructor.
     * 
     * @param text
     * 
     * @since 2.4
     */
    public TagTextFragment(String text) {
        theText = text;
    }

    /**
     * @see com.docfacto.javadoc.TagFragment#getTagText()
     */
    @Override
    public CharSequence getTagText() {
        return theText;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return theText;
    }

}
