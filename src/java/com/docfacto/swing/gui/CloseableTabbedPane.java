/*
 * @author dhudson -
 * Created 6 Aug 2010 : 12:50:15
 */

package com.docfacto.swing.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

/**
 * A Tabbed pane, which is closeable.
 * 
 * @author dhudson - created 17 Sep 2012
 * @since 2.0
 */
public class CloseableTabbedPane extends JTabbedPane {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Create a new instance of <code>CloseableTabbedPane</code>.
     */
    public CloseableTabbedPane() {
        this(SwingConstants.TOP);
    }

    /**
     * Create a new instance of <code>CloseableTabbedPane</code> with the
     * horizontal text position set.
     * 
     * @param horizontalTextPosition tab position
     */
    public CloseableTabbedPane(int horizontalTextPosition) {
        super(horizontalTextPosition);
        setUI(new TestPlaf(this));
    }

    /**
     * Add a component and the select the focus on it
     * 
     * @param title of the tab
     * @param component to add
     * @since 2.0
     */
    public void addAndFocus(String title,Component component) {
        setSelectedComponent(add(title,component));
    }

    /**
     * @author dhudson
     * 
     */
    private class TestPlaf extends BasicTabbedPaneUI {

        private final CloseableTabbedPane theTabbedPane;

        /**
         * Constructor
         * 
         * @param tabbedPane
         */
        TestPlaf(CloseableTabbedPane tabbedPane) {
            theTabbedPane = tabbedPane;
        }

        /**
         * @see javax.swing.plaf.basic.BasicTabbedPaneUI#createLayoutManager()
         */
        @Override
        protected LayoutManager createLayoutManager() {
            return new TestPlafLayout();
        }

        /**
         * Add 40 to the tab size to allow room for the close button and 8 to
         * the height
         * 
         * @see javax.swing.plaf.basic.BasicTabbedPaneUI#getTabInsets(int, int)
         */
        @Override
        protected Insets getTabInsets(int tabPlacement,int tabIndex) {
            // note that the insets that are returned to us are not copies.
            final Insets defaultInsets =
            (Insets)super.getTabInsets(tabPlacement,tabIndex).clone();

            defaultInsets.right += 40;
            defaultInsets.top += 4;
            defaultInsets.bottom += 4;
            return defaultInsets;
        }

        class TestPlafLayout extends TabbedPaneLayout {

            // a list of our close buttons
            private final ArrayList<CloseButton> theCloseButtons =
            new ArrayList<CloseButton>();

            @Override
            public void layoutContainer(Container parent) {
                super.layoutContainer(parent);

                // ensure that there are at least as many close buttons as tabs
                while (theTabbedPane.getTabCount()>theCloseButtons.size()) {
                    theCloseButtons
                    .add(new CloseButton(theCloseButtons.size()));
                }

                Rectangle rect = new Rectangle();

                int i;

                for (i = 0;i<theTabbedPane.getTabCount();i++) {
                    rect = getTabBounds(i,rect);
                    final JButton closeButton = theCloseButtons.get(i);

                    // shift the close button 3 down from the top of the pane
                    // and 20 to the left
                    closeButton.setLocation(rect.x+rect.width-20,rect.y+5);
                    closeButton.setSize(16,16);
                    theTabbedPane.add(closeButton);
                }

                for (;i<theCloseButtons.size();i++) {
                    // remove any extra close buttons
                    theTabbedPane.remove(theCloseButtons.get(i));
                }
            }

            // implement UIResource so that when we add this button to the
            // tabbedpane, it doesn't try to make a tab for it!
            class CloseButton extends JButton
            implements
            javax.swing.plaf.UIResource {
                /**
                 * serialVersionUID
                 */
                private static final long serialVersionUID = 1L;

                public CloseButton(int index) {
                    super(new CloseButtonAction(index));
                    setToolTipText("Close this tab");

                    // remove the typical padding for the button
                    setMargin(new Insets(2,2,2,2));
                    addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseEntered(MouseEvent e) {
                            setForeground(new Color(255,0,0));
                        }

                        @Override
                        public void mouseExited(MouseEvent e) {
                            setForeground(new Color(0,0,0));
                        }
                    });
                }
            }

            class CloseButtonAction extends AbstractAction {
                /**
                 * serialVersionUID
                 */
                private static final long serialVersionUID = 1L;

                int theIndex;

                public CloseButtonAction(int index) {
                    super("X");
                    theIndex = index;
                }

                /**
                 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
                 */
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (theIndex!=0) {
                        theTabbedPane.remove(theIndex);
                    }
                }
            }
        }
    }
}
