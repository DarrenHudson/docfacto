/*

 * Created on Oct 17, 2003

 *

 * Oct 17, 2003 4:29:00 PM

 */

package com.docfacto.swing.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Simple Status Bar at the bottom of UI's.
 * 
 * It has a concept of left and right messages, these can be messages that get
 * removed over time and can also be images.
 * 
 * @author dhudson - created 17 Sep 2012
 * @since 2.0
 */
public class BaseStatusBar extends JPanel {

    /**
     * 
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    private static final Dimension theSize = new Dimension(-1,23);
    private final JLabel theLeftMessage;
    private final JLabel theRightMessage;

    /**
     * Create a new instance of <code>BaseStatusBar</code>.
     */
    public BaseStatusBar() {
        this("","");
    }

    /**
     * Create a new instance of <code>BaseStatusBar</code>, and also display a
     * left and right message.
     * 
     * @param leftMessage to display
     * @param rightMessage to display
     */
    public BaseStatusBar(String leftMessage,String rightMessage) {
        theLeftMessage = new JLabel();
        theRightMessage = new JLabel();
        theLeftMessage.setText(leftMessage);
        theRightMessage.setText(rightMessage);

        // Set up the status bar.
        layoutGui();
    }

    /**
     * General layout method
     * 
     * @since 2.0
     */
    private void layoutGui() {

        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createLoweredBevelBorder());
        setBackground(Color.lightGray);
        setPreferredSize(theSize);
        setMaximumSize(theSize);
        setMinimumSize(theSize);

        theLeftMessage.setHorizontalAlignment(JLabel.LEFT);

        final GridBagConstraints gBC = new GridBagConstraints();
        gBC.anchor = GridBagConstraints.WEST;
        gBC.fill = GridBagConstraints.RELATIVE;
        gBC.gridx = 0;
        gBC.gridy = 0;
        gBC.weightx = 1;
        gBC.weighty = 0;
        gBC.insets = new java.awt.Insets(2,2,2,2);

        add(theLeftMessage,gBC);

        theRightMessage.setHorizontalAlignment(JLabel.RIGHT);

        gBC.gridx = 1;
        gBC.anchor = GridBagConstraints.EAST;
        add(theRightMessage,gBC);
    }

    /**
     * @see javax.swing.JComponent#getPreferredSize()
     */
    @Override
    public Dimension getPreferredSize() {
        return theSize;
    }

    /**
     * Display a message on the left hand side of the status bar.
     * 
     * @param message to display
     * @since 2.0
     */
    public void setLeftMessage(String message) {
        theLeftMessage.setText(message);
    }

    /**
     * Display a message on the right hand side of the status bar, the colour
     * black will be used.
     * 
     * @param message to display
     * @since 2.0
     */
    public void setRightMessage(String message) {
        setRightMessage(message,Color.black);
    }

    /**
     * Set a right hand message with the given colour
     * 
     * @param message to display
     * @param color of the message
     * @since 2.0
     */
    public void setRightMessage(String message,Color color) {
        theRightMessage.setForeground(color);
        theRightMessage.setText(message);
    }

    /**
     * Return the message currently being displayed on the left hand side of the
     * status bar
     * 
     * @return the message
     * @since 2.0
     */
    public String getLeftMessage() {
        return theLeftMessage.getText();
    }

    /**
     * Return the message currently being displayed on the right hand side of
     * the status bar
     * 
     * @return the message
     * @since 2.0
     */
    public String getRightMessage() {
        return theRightMessage.getText();
    }

    /**
     * Clear right hand side message
     * 
     * @since 2.0
     */
    public void clearRightMessage() {
        setRightMessage("");
    }

    /**
     * Clear the left hand message
     * 
     * @since 2.0
     */
    public void clearLeftMessage() {
        setLeftMessage("");
    }

    /**
     * Clear both right hand and left hand messages
     * 
     * @since 2.0
     */
    public void clearMessages() {
        setLeftMessage("");
        setRightMessage("");
    }

    /**
     * Set a message on the right hand side of the status bar, which will be
     * removed after time
     * 
     * @param message to display
     * @since 2.0
     */
    public void setRightTimedMessage(String message) {
        setRightMessage(message,Color.blue);
        new ClearMessage();
    }

    /**
     * Set an icon of the left hand side of the status bar, which will be
     * removed after time
     * 
     * @param image to display
     * @since 2.0
     */
    public void setTimedImage(ImageIcon image) {
        new ClearImage(theLeftMessage.getText());
        theLeftMessage.setText("");
        theLeftMessage.setIcon(image);
    }

    /**
     * Set a left message, which will be removed after time
     * 
     * @param message to display
     * @since 2.0
     */
    public void setLeftTimedMessage(String message) {
        new ClearLeftMessage(theLeftMessage.getText());
        theLeftMessage.setText(message);
    }

    /**
     * @author dhudson
     */
    private class ClearMessage {
        Timer theTimer;

        /**
         * Constructor
         */
        public ClearMessage() {
            theTimer = new Timer();
            theTimer.schedule(new ClearTask(),2000);
        }

        /**
         * Clear Task
         *
         * @author dhudson - created 18 Sep 2012
         * @since 2.0
         */
        class ClearTask extends TimerTask {

            /**
             * Create a new instance of <code>ClearTask</code>.
             */
            public ClearTask() {
            }

            /**
             * @see java.util.TimerTask#run()
             */
            @Override
            public void run() {
                clearRightMessage();

                // Terminate the timer thread
                theTimer.cancel();
            }
        }
    }

    /**
     * Clear Image Task
     *
     * @author dhudson - created 18 Sep 2012
     * @since 2.0
     */
    private class ClearImage {

        private final Timer theTimer;
        private final String theOldText;

        /**
         * Create a new instance of <code>ClearImage</code>.
         * @param text
         */
        public ClearImage(String text) {
            theOldText = text;
            theTimer = new Timer();
            theTimer.schedule(new ClearTask(),700);
        }

        /**
         * @author dhudson
         */
        class ClearTask extends TimerTask {

            /**
             * Create a new instance of <code>ClearTask</code>.
             */
            public ClearTask() {
            }

            /*
             * (non-Javadoc)
             * 
             * @see java.util.TimerTask#run()
             */

            @Override
            public void run() {
                theLeftMessage.setIcon(null);
                theLeftMessage.setText(theOldText);

                // Terminate the timer thread
                theTimer.cancel();
            }
        }
    }

    /**
     * @author dhudson
     */
    private class ClearLeftMessage {

        private final Timer theTimer;
        private final String theOldText;

        /**
         * Create a new instance of <code>ClearLeftMessage</code>.
         * @param text
         */
        public ClearLeftMessage(String text) {
            theOldText = text;
            theTimer = new Timer();
            theTimer.schedule(new ClearTask(),700);
        }

        /**
         * @author dhudson
         */
        class ClearTask extends TimerTask {

            /**
             * Create a new instance of <code>ClearTask</code>.
             */
            public ClearTask() {
            }

            /**
             * @see java.util.TimerTask#run()
             */
            @Override
            public void run() {
                theLeftMessage.setText(theOldText);
                // Terminate the timer thread
                theTimer.cancel();
            }
        }
    }
}
