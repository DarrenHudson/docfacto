/*

 * Created on Oct 19, 2003

 *

 * Oct 19, 2003 2:35:57 AM

 */

package com.docfacto.swing.gui;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

import com.docfacto.common.DocfactoException;

/**
 * This class creates a Dialog and displays an exception
 *
 * @author dhudson - created 18 Sep 2012
 * @since 2.0
 */
public class BaseExceptionDialog extends BaseDialog {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    private final JTextArea theTextArea = new JTextArea(4,30);

    private final BaseController theController;

    private final Exception theException;


    /**
     * Create a new instance of <code>BaseExceptionDialog</code>.
     * @param ex exception to display
     * @param controller application controller
     */
    public BaseExceptionDialog(DocfactoException ex,BaseController controller) {
        this(ex,ex.getMessage(),controller);
    }

    /**
     * Create a new instance of <code>BaseExceptionDialog</code>.
     * @param ex exception to display
     * @param controller application controller
     */
    public BaseExceptionDialog(Exception ex,BaseController controller) {
        this(ex,ex.getMessage(),controller);
    }

    /**
     * Create a new instance of <code>BaseExceptionDialog</code>.
     * @param message to display
     * @param controller application contoller
     */
    public BaseExceptionDialog(String message,BaseController controller) {
        this(null,message,controller);
    }

    /**
     * Create a new instance of <code>BaseExceptionDialog</code>.
     * @param ex the cause
     * @param message message to display
     * @param controller application controller
     */
    public BaseExceptionDialog(Exception ex,String message,
    BaseController controller) {
        super(controller.getFrame(),true);
        theException = ex;
        theController = controller;
        theTextArea.setText(message);
        dialogSetup();
    }

    /**
     * Method dialogSetup.
     */
    private void dialogSetup() {
        setTitle(theController.getAppName()+" Exception");
        layoutGui();
        pack();
        setVisible(true);
    }

    /**
     * Method layoutGui
     * 
     */
    private void layoutGui() {
        final Container cp = getContentPane();

        cp.setLayout(new GridBagLayout());
        final GridBagConstraints gBC = getBaseConstraints();
        gBC.fill = GridBagConstraints.BOTH;

        // If there is an exception, there is a good chance that it can be
        // reported. Offer the user a chance to report it.
        if (theException!=null) {
            cp.add(getTabbedPanel(),gBC);
        }
        else {
            cp.add(getMessagePanel(),gBC);
        }
    }

    /**
     * Method getTabbedPanel
     * 
     * @return
     */
    private JTabbedPane getTabbedPanel() {
        final JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.add("Message",getMessagePanel());
        tabbedPane.add("Report",getReportPanel());

        return tabbedPane;
    }

    /**
     * Method getReportPanel
     * 
     * @return
     */
    private BasePanel getReportPanel() {

        final BasePanel panel = new BasePanel();
        final GridBagConstraints gBC = panel.getBaseConstraints();

        gBC.fill = GridBagConstraints.BOTH;

        final JTextArea textArea = new JTextArea(4,30);
        textArea.setText(getStackTraceText(theException));
        panel.add(panel.createScrollPane(textArea),gBC);

        return panel;
    }

    /**
     * Return the stack trace from the exception
     * 
     * @param exception to process
     * @return the stack trace from the exception
     * @since 2.0
     */
    public String getStackTraceText(Exception exception) {

        if (exception==null) {
            return "";
        }

        final StringBuilder buffer = new StringBuilder();

        buffer.append("\nStack Trace:\n");
        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        exception.printStackTrace(printWriter);
        buffer.append(stringWriter.toString());
        return buffer.toString();
    }

    /**
     * Method normalMessage
     * 
     * @return
     */
    private BasePanel getMessagePanel() {

        final BasePanel panel = new BasePanel();
        final GridBagConstraints gBC = panel.getBaseConstraints();
        gBC.fill = GridBagConstraints.BOTH;
        panel.add(panel.createScrollPane(theTextArea),gBC);
        return panel;
    }
}
