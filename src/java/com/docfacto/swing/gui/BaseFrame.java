/*

 * Created on Oct 17, 2003

 *

 * Oct 17, 2003 5:18:00 PM

 */

package com.docfacto.swing.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

/**
 * Simple wrapper class for {@code JFrame}
 *
 * @author dhudson - created 17 Sep 2012
 * @since 2.0
 */
public abstract class BaseFrame extends JFrame implements BaseEventListener {

    /**
     * Constant {@value}
     */
    private static final long serialVersionUID = 1L;

    private final BaseController theController;

    /**
     * Create a new instance of <code>BaseFrame</code>.
     * @param controller application controller
     */
    public BaseFrame(BaseController controller) {

        theController = controller;

        // Register the frame with the controller
        theController.setFrame(this);

        // Register thyself as a subscriber
        theController.addListener(this);

        setDefaultWindowActions();

        setIconImage(BaseUtils.loadImage("Logo16.png"));

        // Get the native look and feel class name
        final String nativeLF = UIManager.getSystemLookAndFeelClassName();

        // Install the look and feel
        try {
            UIManager.setLookAndFeel(nativeLF);
        } catch (final Throwable ignore) {
            // ignore
        }
    }

    /**
     * Set default set of Frame handlers
     *
     * @since 2.0
     */
    private void setDefaultWindowActions() {

        // Set close action
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        addWindowListener(new WindowAdapter() {
            /**
             * @see java.awt.event.WindowAdapter#windowClosing(java.awt.event.WindowEvent)
             */
            @Override
            public void windowClosing(WindowEvent evt) {
                closeFrame();
            }
        });
    }

    /**
     * Returns the application controller
     *
     * @return the application controller
     * @since 2.0
     */
    public BaseController getController() {
        return theController;
    }

    /**
     * @see com.docfacto.swing.gui.BaseEventListener#handleEvent(com.docfacto.swing.gui.BaseEvent)
     */
    @Override
    public abstract void handleEvent(BaseEvent ev);


    /**
     * Close Frame event handler
     *
     * @since 2.0
     */
    public abstract void closeFrame();

}
