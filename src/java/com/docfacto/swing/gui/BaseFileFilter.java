package com.docfacto.swing.gui;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * Simple wrapper around the {@code FileFilter} class
 * 
 * @author dhudson - created 17 Sep 2012
 * @since 2.0
 */
public class BaseFileFilter extends FileFilter {

    private final String[] theExtensions;
    private final String theDescription;

    /**
     * Create a new instance of <code>BaseFileFilter</code> with the given file extension.
     * @param ext file extension
     */
    public BaseFileFilter(String ext) {
        this(new String[] {ext},null);
    }

    /**
     * Create a new instance of <code>BaseFileFilter</code> with an array of extensions and a file type description.
     * @param exts extensions
     * @param descr file type description
     */
    public BaseFileFilter(String[] exts,String descr) {

        // clone and lowercase the extensions
        theExtensions = new String[exts.length];
        for (int i = exts.length-1;i>=0;i--) {
            theExtensions[i] = exts[i].toLowerCase();
        }

        // make sure we have a valid (if simplistic) description
        theDescription = (descr==null ? exts[0]+" files" : descr);
    }

    /**
     * @see javax.swing.filechooser.FileFilter#accept(File)
     */
    @Override
    public boolean accept(File file) {
        // we always allow directories, regardless of their extension
        if (file.isDirectory()) {
            return true;
        }

        // ok, it's a regular file so check the extension
        final String name = file.getName().toLowerCase();
        for (int i = theExtensions.length-1;i>=0;i--) {
            if (name.endsWith(theExtensions[i])) {
                return true;
            }
        }

        return false;
    }

    /**
     * @see javax.swing.filechooser.FileFilter#getDescription()
     */
    @Override
    public String getDescription() {
        return theDescription;
    }

}
