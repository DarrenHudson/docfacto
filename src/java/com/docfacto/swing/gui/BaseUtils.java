/*

 * Created on Oct 18, 2003

 *

 * Oct 18, 2003 12:19:24 AM

 */

package com.docfacto.swing.gui;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

/**
 * Singleton for a collection of Swing utilities
 * 
 * @author dhudson - created 17 Sep 2012
 * @since 2.0
 */
public class BaseUtils {

    /**
     * Create a new instance of <code>BaseUtils</code>.
     */
    private BaseUtils(){
    }

    /**
     * Constant {@value}
     */
    private static final String RESOURCE_BASE = "com/docfacto/swing/images/";

    private static Class<?> DummyClass = BaseUtils.class;

    /**
     * Load Image from the resource base
     * 
     * @param imageName of the icon from the resource base
     * @return the <code>image</code> or null if unable to load
     * @since 2.0
     */
    public static Image loadImage(String imageName) {
        final URL url = ClassLoader.getSystemResource(RESOURCE_BASE+imageName);
        return new ImageIcon(url).getImage();
    }

    /**
     * Load ImageIcon from the resource base
     * 
     * @param iconName of the icon on the resource base
     * @return the <code>ImageIcon</code> or null if unable to load
     * @since 2.0
     */
    public static ImageIcon loadImageIcon(String iconName) {
        URL url = ClassLoader.getSystemResource(RESOURCE_BASE+iconName);
        if (url==null) {
            // The Applet Client doesn't load the file using the ClassLoader
            url = DummyClass.getResource("/"+RESOURCE_BASE+iconName);
        }
        return new ImageIcon(url);
    }

    /**
     * Positions a frame in the middle of the screen
     * 
     * @param frame to centre
     * @since 2.0
     */
    public static void centreFrame(JFrame frame) {

        final Dimension screenSize =
        Toolkit.getDefaultToolkit().getScreenSize();
        final Dimension size = frame.getSize();

        screenSize.height = screenSize.height/2;
        screenSize.width = screenSize.width/2;

        size.height = size.height/2;
        size.width = size.width/2;

        final int y = screenSize.height-size.height;
        final int x = screenSize.width-size.width;

        frame.setLocation(x,y);
    }
}
