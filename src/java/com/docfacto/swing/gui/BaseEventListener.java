/*

 * Created on Oct 17, 2003

 *

 * Oct 17, 2003 4:51:56 PM

 */

package com.docfacto.swing.gui;

import java.util.EventListener;

/**
 * Event Listener Interface
 * 
 * @author dhudson - created 17 Sep 2012
 * @since 2.0
 */
public interface BaseEventListener extends EventListener {
    /**
     * handle the event if required
     *
     * @param ev to process
     * @since 2.0
     */
    public void handleEvent(BaseEvent ev);
}