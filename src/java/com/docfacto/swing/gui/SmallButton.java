package com.docfacto.swing.gui;

import java.awt.Insets;

import javax.swing.JButton;

/**
 * Used to generate small buttons that wrap around an image without any padding.
 * 
 * @author dhudson
 * 
 * General class to generate small buttons.
 * @since 2.0
 */
public class SmallButton extends JButton {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Create a new instance of <code>SmallButton</code>.
     * 
     * @param iconName to load from the resources folder
     * @param toolTip t display upon mouse hover
     */
    public SmallButton(String iconName,String toolTip) {
        super(BaseUtils.loadImageIcon(iconName));

        if (toolTip!=null) {
            setToolTipText(toolTip);
        }

        setMargin(new Insets(0,0,0,0));
        setFocusable(false);
    }
}