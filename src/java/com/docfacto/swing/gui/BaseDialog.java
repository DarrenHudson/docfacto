/*

 * Created on Nov 1, 2003

 *

 * Nov 1, 2003 6:26:15 AM

 */

package com.docfacto.swing.gui;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 * Simple wrapper class for {@code JDialog}
 *
 * @author dhudson - created 18 Sep 2012
 * @since 2.0
 */
public class BaseDialog extends JDialog {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>BaseDialog</code>
     * @param frame parent frame
     * @param modal is modal?
     */
    public BaseDialog(JFrame frame, boolean modal) {
        super(frame, modal);
        addBaseEventHandlers();
        setLocationRelativeTo(frame);
    }

    /**
     * Returns the base constraints
     *
     * @return the base constraints
     * @since 2.0
     */
    public GridBagConstraints getBaseConstraints() {
        final GridBagConstraints gBC = new GridBagConstraints();
        gBC.insets = new Insets(2, 2, 2, 2);
        gBC.gridx = 0;
        gBC.gridy = 0;
        gBC.weightx = 0.5;
        gBC.weighty = 0.5;
        gBC.anchor = GridBagConstraints.NORTHWEST;

        return gBC;
    }

    /**
     * Add Window handlers
     *
     * @since 2.0
     */
    private void addBaseEventHandlers() {

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent evt) {
                closeDialog(evt);
            }
        });
    }

    /**
     * Dispose of the dialog
     *
     * @param evt closing event
     * @since 2.0
     */
    public void closeDialog(WindowEvent evt) {
        setVisible(false);
        dispose();
    }
}
