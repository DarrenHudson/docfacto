package com.docfacto.swing.gui;

import java.awt.Component;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableModel;

/**
 * Extend the JTable to add Cell Focus Listeners
 * 
 * @author dhudson - created 17 Sep 2012
 * @since 2.0
 */
public class BaseTable extends JTable {

    private static final long serialVersionUID = 1L;

    CellFocusListener theCellFocusListener = null;

    /**
     * Create a new instance of <code>BaseTable</code>.
     */
    public BaseTable() {
        super();
        addEventListeners();
    }

    /**
     * Create a new instance of <code>BaseTable</code>.
     * @param tableModel to create the table with.
     */
    public BaseTable(TableModel tableModel) {
        super(tableModel);
        addEventListeners();
    }

    /**
     * Add Key listener
     * 
     * @since 2.0
     */
    private void addEventListeners() {

        // Ignore any Send key actions on the table
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyChar()==KeyEvent.VK_ENTER) {
                    e.consume();
                }
            }
        });
    }

    /**
     * @see javax.swing.JTable#prepareEditor(javax.swing.table.TableCellEditor,
     * int, int)
     */
    @Override
    public Component prepareEditor(
    TableCellEditor editor,
    int row,
    int column) {

        final Component comp = super.prepareEditor(editor,row,column);

        if (comp!=null) {
            if (theCellFocusListener!=null) {
                theCellFocusListener.detach();
            }
            // Create a new cell focus listener for the table cell editor
            // component
            new CellFocusListener(comp);
        }

        return comp;
    }

    /**
     * Method setSelectedRow
     * 
     * @param rowIndex to select
     * @since 2.0
     */
    public void setSelectedRow(int rowIndex) {
        setRowSelectionInterval(rowIndex,rowIndex);
        // Show the selected row
        scrollRectToVisible(getCellRect(rowIndex,0,true));
    }

    // This listener class ensures that the editing value is set when
    // the cell editor loses focus
    private class CellFocusListener extends FocusAdapter {

        Component theComp;
        boolean thisFocusSet;

        /**
         * Constructor for CellFocusListener
         * 
         * @param comp
         */
        CellFocusListener(Component comp) {

            this.theComp = comp;
            this.theComp.addFocusListener(this);
            thisFocusSet = false;

            // Ensure we start off with the focus set on our cell editor
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    CellFocusListener.this.theComp.requestFocus();
                    thisFocusSet = true;
                }
            });

            theCellFocusListener = this;
        }

        /*
         * (non-Javadoc)
         * 
         * @see
         * java.awt.event.FocusListener#focusLost(java.awt.event.FocusEvent)
         */
        @Override
        public void focusLost(FocusEvent evt) {

            if (thisFocusSet) {

                // We've just lost the focus so we need to update the model
                stopEditing();
            }
        }

        /**
         * Method stopEditing
         * 
         */
        void stopEditing() {

            final TableCellEditor editor = getCellEditor();

            if (editor!=null) {

                // Tell our editor to stop editing and just give us what it's
                // got for the cell
                editor.stopCellEditing();

                final int editrow = getEditingRow();
                final int editcol = getEditingColumn();

                if (editrow>=0
                &&editrow<getRowCount()
                &&editcol>=0
                &&editcol<getColumnCount()) {

                    // Get the value in the cell and set that value in the
                    // model
                    final Object value = editor.getCellEditorValue();
                    setValueAt(value,editrow,editcol);
                }
            }

            // Detach the focus listener from this cell editor
            detach();
        }

        /**
         * Method detach
         * 
         */
        void detach() {

            theComp.removeFocusListener(this);
            theCellFocusListener = null;
        }

    }

}