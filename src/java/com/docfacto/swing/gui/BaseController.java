/*
 * Created on Oct 17, 2003
 *
 * Oct 17, 2003 4:48:57 PM
 */

package com.docfacto.swing.gui;

import java.awt.Cursor;
import java.util.Vector;

import javax.swing.JFrame;

/**
 * Swing controller to handle events
 *
 * @author dhudson - created 18 Sep 2012
 * @since 2.0
 */
public class BaseController {

    // A vector of listeners (subscribers)
    private final Vector<BaseEventListener> theListeners =
    new Vector<BaseEventListener>();

    private JFrame theFrame;

    private final String theApplicationName;

    private final String theApplicationRelease;

    /**
     * Creates a new instance of <code>BaseController</code>.
     * @param applicationName name of the application
     * @param applicationRelease String representation of the version
     */
    public BaseController(String applicationName,String applicationRelease) {
        theApplicationName = applicationName;
        theApplicationRelease = applicationRelease;
    }

    /**
     * Add an application event listener to the controller
     *
     * @param eventListener to add
     * @since 2.0
     */
    public synchronized void addListener(BaseEventListener eventListener) {
        theListeners.add(eventListener);
    }

    /**
     * Remove and application event listener
     *
     * @param eventListener to remove
     * @since 2.0
     */
    public synchronized void removeListener(BaseEventListener eventListener) {
        theListeners.remove(eventListener);
    }

    /**
     * Broadcast this event to all listeners.
     * <p>
     * {@docfacto.note for example Controller.processEvent(new MyExitEvent()); }
     * </p>
     * @param ev to broadcast to all of the listeners
     * @since 2.0
     */
    public synchronized void processEvent(BaseEvent ev) {
        final Vector<BaseEventListener> copy =
        new Vector<BaseEventListener>(theListeners);
        for (final BaseEventListener listener:copy) {
            listener.handleEvent(ev);
        }
    }

    /**
     * Returns the frame
     *
     * @return the frame associated with this controller
     * @since 2.0
     */
    public JFrame getFrame() {
        return theFrame;
    }

    /**
     * Set the frame for the controller
     *
     * @param frame to set
     * @since 2.0
     */
    public void setFrame(JFrame frame) {
        theFrame = frame;
    }

    /**
     * Returns the application name
     *
     * @return the application name
     * @since 2.0
     */
    public String getAppName() {
        return theApplicationName;
    }

    /**
     * Returns the application release string
     *
     * @return the application release string notation
     * @since 2.0
     */
    public String getApplicationRelease() {
        return theApplicationRelease;
    }

    /**
     * Sets the cursor to the wait cursor
     *
     * @since 2.0
     */
    public void cursorWait() {
        theFrame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    }

    /**
     * Sets the cursor to the default cursor
     *
     * @since 2.0
     */
    public void cursorDefault() {
        theFrame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }
}
