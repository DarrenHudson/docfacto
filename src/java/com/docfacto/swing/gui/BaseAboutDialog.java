/**
 * Created on 21 Nov 2006 by dhudson
 */
package com.docfacto.swing.gui;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * This class displays an about dialog for the application
 *
 * @author dhudson - created 18 Sep 2012
 * @since 2.0
 */
public class BaseAboutDialog extends JDialog implements BaseEventListener {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    private final BaseController theController;

    /**
     * Create a new instance of <code>BaseAboutDialog</code>.
     * @param controller application controller
     */
    public BaseAboutDialog(BaseController controller) {
        super(controller.getFrame(), true);

        theController = controller;
        theController.addListener(this);

        layoutGui();
        addEventListeners();

        setResizable(false);
        setBackground(Color.white);
        setTitle(controller.getAppName());
        setLocationRelativeTo(controller.getFrame());

        pack();
    }

    /**
     * Method layoutGui
     * 
     */
    private void layoutGui() {

        final Container cp = getContentPane();
        final JPanel panel = new JPanel();
        final GridBagConstraints gBC = new GridBagConstraints();

        // Keep with the same theme
        panel.setBorder(BorderFactory.createLoweredBevelBorder());
        panel.setLayout(new GridBagLayout());

        gBC.insets = new Insets(2, 2, 2, 2);
        gBC.anchor = GridBagConstraints.NORTHWEST;
        gBC.weightx = 0.5;
        gBC.weighty = 0.5;
        gBC.gridx = 0;
        gBC.gridy = 0;
        gBC.gridheight = 5;

        // panel.add(new JLabel(BaseUtils.loadImageIcon("docfacto.png")), gBC);

        gBC.gridy=6;
        gBC.gridheight = 1;
        gBC.weightx = 1;
        gBC.weighty = 0;
        gBC.anchor = GridBagConstraints.CENTER;
        panel.add(
            new JLabel("<html><i><b>Version:</b></i> " + theController.getApplicationRelease(),
                JLabel.CENTER),
                gBC);


        gBC.gridy++;
        panel.add(new JLabel("www.docfacto.com", JLabel.CENTER), gBC);

        cp.add(panel);
    }

    /**
     * Method addEventListeners
     * 
     */
    private void addEventListeners() {
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent evt) {
                setVisible(false);
            }
        });
    }


    /**
     * @see com.docfacto.swing.gui.BaseEventListener#handleEvent(com.docfacto.swing.gui.BaseEvent)
     */
    @Override
    public void handleEvent(BaseEvent ev) {
        if(ev.isAbout()) {
            setVisible(true);
        }
    }
}
