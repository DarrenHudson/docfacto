/*

 * Created on Oct 17, 2003

 *

 * Oct 17, 2003 4:53:02 PM

 */

package com.docfacto.swing.gui;

import java.util.EventObject;

/**
 * Base Event Class
 * Action 1 Exit and 2 About are always defined
 *
 * @author dhudson - created 17 Sep 2012
 * @since 2.0
 */
public abstract class BaseEvent extends EventObject {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constant {@value}
     */
    public static final int EXIT = 1;

    /**
     * Constant {@value}
     */
    public static final int ABOUT = 2;

    private final int theAction;

    /**
     * Abstract class for BaseEvent
     * @param source of where the event was fired
     * @param action type of event
     */
    public BaseEvent(Object source, int action) {
        super(source);
        theAction = action;
    }

    /**
     * Return the action value
     *
     * @return the action
     * @since 2.0
     */
    public int getAction() {
        return theAction;
    }

    /**
     * Test for about event
     *
     * @return true if this is an about event
     * @since 2.0
     */
    public boolean isAbout() {
        return (getAction() == ABOUT);
    }

    /**
     * Test for exit event
     *
     * @return true if this is an exit event
     * @since 2.0
     */
    public boolean isExit() {
        return (getAction() == EXIT);
    }

}

