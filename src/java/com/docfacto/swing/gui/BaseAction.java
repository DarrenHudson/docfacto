/*
 * Created on Aug 8, 2004
 *
 * Hudsons Holdings Limited
 */
package com.docfacto.swing.gui;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;

/**
 * Wrapper class around the {@code AbstractAction} class
 * 
 * @author dhudson - created 18 Sep 2012
 * @since 2.0
 */
public abstract class BaseAction extends AbstractAction {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    private final int theEvent;
    private final BaseController theController;

    /**
     * Abstract class for actions.
     * @param text action name
     * @param icon action icon
     * @param event number
     * @param controller application controller
     */
    public BaseAction(String text,Icon icon,int event,BaseController controller) {
        super(text,icon);
        putValue(SHORT_DESCRIPTION,text);
        theEvent = event;
        theController = controller;
    }

    /**
     * Returns the event
     * 
     * @return the event
     * @since 2.0
     */
    public int getEvent() {
        return theEvent;
    }

    /**
     * Returns the controller
     * 
     * @return the controller
     * @since 2.0
     */
    public BaseController getController() {
        return theController;
    }

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public abstract void actionPerformed(ActionEvent e);

}
