/*

 * Created on Oct 19, 2003

 *

 * Oct 19, 2003 1:21:56 AM

 */

package com.docfacto.swing.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * Simple wrapper class around the {@code JPanel} class
 *
 * @author dhudson - created 17 Sep 2012
 * @since 2.0
 */
public class BasePanel extends JPanel {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Create a new instance of <code>BasePanel</code>.
     */
    public BasePanel() {
        super();
        setLayout(new GridBagLayout());
    }

    /**
     * Create a new instance of <code>BasePanel</code> but with a background colour.
     * @param colour of the required background
     */
    public BasePanel(Color colour) {
        this();
        setBackground(colour);
    }

    /**
     * Return the base constraints
     *
     * @return the base constraints
     * @since 2.0
     */
    public GridBagConstraints getBaseConstraints() {

        final GridBagConstraints gBC = new GridBagConstraints();
        gBC.insets = new Insets(2, 2, 2, 2);
        gBC.gridx = 0;
        gBC.gridy = 0;
        gBC.weightx = 0.5;
        gBC.weighty = 0.5;
        gBC.anchor = GridBagConstraints.NORTHWEST;

        return gBC;
    }

    /**
     * Return a scroll pane component
     *
     * @param component to encapsulate
     * @return a {@code JScrollPane} with the Horizontal and Vertical policies set as needed
     * @since 2.0
     */
    public JScrollPane createScrollPane(Component component) {

        final JScrollPane scrollPane = new JScrollPane(component);
        scrollPane
        .setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane
        .setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        return scrollPane;
    }

}
