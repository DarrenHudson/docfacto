package com.docfacto.output;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;

import com.docfacto.adam.AdamRuleKeys;
import com.docfacto.common.DocfactoException;
import com.docfacto.common.Platform;
import com.docfacto.config.BaseXmlConfig;
import com.docfacto.config.generated.Severity;
import com.docfacto.links.RestrictionCounter;
import com.docfacto.output.generated.Fix;
import com.docfacto.output.generated.Output;
import com.docfacto.output.generated.Position;
import com.docfacto.output.generated.Result;
import com.docfacto.output.generated.Results;
import com.docfacto.output.generated.Statistic;

/**
 * Output processors load the statistics from the provided file. All the
 * statistics have a value of zero.
 * 
 * This is done so that applications like sonar know all of the possible
 * statistics.
 * 
 * This abstract class handles the incrementation of statistics etc
 * 
 * @author dhudson - created 12 Apr 2013
 * @since 2.2
 */
public abstract class AbstractOutputProcessor extends BaseXmlConfig {

    private static final String JAXB_XSD_LOCATION =
        "com/docfacto/output/resources/";

    /**
     * JAXB Package name constant {@value}
     */
    private static final String JAXB_PACKAGE_NAME =
        "com.docfacto.output.generated";

    /**
     * Name of the validating XSD {@value}
     */
    private static final String XSD_NAME =
        "Output.xsd";

    protected HashMap<String,Statistic> theStats;

    private static final NumberFormat NUMBER_FORMAT = new DecimalFormat("###,###");
    
    private RestrictionCounter restrictionCounter;
    
    private boolean maxFilesReached;
    
    /**
     * Constructor.
     * 
     * @throws DocfactoException
     */
    public AbstractOutputProcessor(RestrictionCounter restrictionCounter) throws DocfactoException {
        theStats = new HashMap<String,Statistic>();

        // Load the stats
        for (Statistic statistic:getOutput().getStatistics().getStatistics()) {
            theStats.put(statistic.getRule().getKey(),statistic);
        }

        getOutput().setResults(new Results());
        
        this.restrictionCounter = restrictionCounter;
        
        maxFilesReached = false;
    }
    
    public AbstractOutputProcessor() throws DocfactoException {
    	this(new RestrictionCounter(-1));
    }

    /**
     * Constructor.
     * 
     * @param configFilePath to load
     * @throws DocfactoException if unable to unmarshal the xml file
     */
    public AbstractOutputProcessor(String configFilePath)
    throws DocfactoException {
        super(configFilePath);
    }

    /**
     * Increment statistic by the given key. The key should come from
     * {@code AdamRuleKeys}
     * 
     * This will also increment the severity statistic as well
     * 
     * @param ruleName
     * @since 2.2
     */
    public void incrementStatistic(String ruleName,Severity severity) {

        incrementStatistic(theStats.get(ruleName));

        switch (severity) {
        case ERROR:
            incrementStatistic(theStats.get(AdamRuleKeys.ERRORS_KEY));
            break;

        case WARNING:
            incrementStatistic(theStats.get(AdamRuleKeys.WARNINGS_KEY));
            break;

        case INFO:
            incrementStatistic(theStats.get(AdamRuleKeys.INFO_KEY));
            break;
        }
    }

    /**
     * Add one to the current value of the statistic
     * 
     * @param statistic
     * @since 2.2
     */
    private void incrementStatistic(Statistic statistic) {
        if (statistic!=null) {
            statistic.setValue((statistic.getValue()+1));
        }
    }

    /**
     * Increment a statistic, but not the severities
     * 
     * @param ruleName
     * @since 2.2
     */
    public void incrementStatistic(String ruleName) {
        if (theStats.containsKey(ruleName)) {
            incrementStatistic(theStats.get(ruleName));
        }
    }

    /**
     * Add an additional value to the statistic
     * 
     * @param ruleName to add to
     * @param addition the additional value
     * @since 2.2
     */
    public void addToStatistic(String ruleName,int addition) {
        Statistic stat = theStats.get(ruleName);
        if (stat!=null) {
            stat.setValue(addition+stat.getValue());
        }
    }

    /**
     * Return the Output
     * 
     * @return the Output properties
     * @since 2.2
     */
    public Output getOutput() {
        return (Output)getProperties();
    }

    /**
     * Add a result to the list
     * 
     * @param result
     * @since 2.2
     */
    public void addResult(Result result) {
        getOutput().getResults().getResults().add(result);
    }

    /**
     * Fetch the results from the output
     * 
     * @return results, which may be empty
     * @since 2.2
     */
    public List<Result> getResults() {
        return getOutput().getResults().getResults();
    }

    /**
     * Fetch the statistics from the output
     * 
     * @return statistics, which may be empty
     * @since 2.2
     */
    public List<Statistic> getStatistics() {
        return getOutput().getStatistics().getStatistics();
    }

    /**
     * Dump the statistics to stdout
     * 
     * @since 2.2
     */
    public void dumpStats() {
        for (Statistic statistic:getOutput().getStatistics().getStatistics()) {
            System.out.println(statistic.getRule().getMessage()+" : "+
                NUMBER_FORMAT.format(statistic.getValue()));
        }
    }

    /**
     * Dump the results to stdout
     * 
     * @since 2.2
     */
    public void dumpResults() {
        for (Result result:getOutput().getResults().getResults()) {
            StringBuilder builder = new StringBuilder(100);
            builder.append(result.getRule().getMessage());
            Position pos = result.getPosition();
            if(pos != null) {
                builder.append(" [");
                builder.append(pos.getLine());
                builder.append(":");
                builder.append(pos.getColumn());
                builder.append("]");
            }
            
            Fix fix = result.getFix();
            if(fix != null) {
                builder.append(Platform.LINE_SEPARATOR);
                builder.append("\t");
                builder.append(fix.getType());
                builder.append(" [");
                builder.append(fix.getTagName());
                builder.append("] ");
                builder.append(fix.getDescription());
            }
            System.out.println(builder.toString());
        }
    }

    /**
     * Dump the results and stats to stdout
     * 
     * @since 2.2
     */
    public void dump() {
    	if (restrictionCounter != null) {
    		if (maxFilesReached)
    			System.out.println("WARNING: The maximum allowed files for your license (" + restrictionCounter.getRestrictionValue() + ") has been processed.\n");
    	}
    	
        dumpResults();
        dumpStats();
    }

    /**
     * @see com.docfacto.config.BaseXmlConfig#getXSDName()
     */
    @Override
    public String getXSDName() {
        return XSD_NAME;
    }

    /**
     * @see com.docfacto.config.BaseXmlConfig#getXSDLocation()
     */
    @Override
    public String getXSDLocation() {
        return JAXB_XSD_LOCATION;
    }

    /**
     * @see com.docfacto.config.BaseXmlConfig#getObjectFactoryPackageName()
     */
    @Override
    public String getObjectFactoryPackageName() {
        return JAXB_PACKAGE_NAME;
    }
    

	public void setMaxFilesReached(boolean maxFilesReached) {
		this.maxFilesReached = maxFilesReached;
	}
	

	public boolean isMaxFilesReached() {
		return maxFilesReached;
	}
}
