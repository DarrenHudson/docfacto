/*
 * @author dhudson -
 * Created 28 May 2012 : 13:09:42
 */

package com.docfacto.dita;

/**
 * Class to encapsulate the {@code image} tag
 * 
 * @author dhudson - created 28 May 2012
 * @since 1.4
 */
public class Image extends HRefAttributeElement {

    /**
     * Create a new instance of <code>Image</code>.
     */
    public Image() {
    }

    /**
     * Helper method to set the <i>alt</i> attribute
     * 
     * @param altText the alternate text for the image
     * @since 2.0
     */
    public void setAlt(String altText) {
        addAttribute("alt",altText);
    }
}
