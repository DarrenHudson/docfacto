/*
 * @author dhudson -
 * Created 26 Nov 2012 : 15:16:29
 */

package com.docfacto.dita;

/**
 * Simple class to wrap the {@code tbody} element
 *
 * @author dhudson - created 9 Mar 2013
 * @since 2.1
 */
public class TBody extends DITAElement {

    /**
     * Constructor.
     */
    public TBody() {

    }
}
