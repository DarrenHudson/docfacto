/*
 * @author dhudson -
 * Created 28 Apr 2012 : 09:00:53
 */

package com.docfacto.dita;

/**
 * Wrapper class for the {@code title} element
 * 
 * @author dhudson - created 22 May 2012
 * @since 1.4
 */
public class Title extends DITAElement {

    /**
     * Creates a new instance of <code>Title</code> with the given text.
     * 
     * @param text for the title element
     */
    public Title(String text) {
        super(text);
    }
}
