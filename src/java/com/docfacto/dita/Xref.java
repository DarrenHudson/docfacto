/*
 * @author dhudson -
 * Created 7 May 2012 : 20:29:06
 */

package com.docfacto.dita;

/**
 * Wrapper class around the DITA {@code Xref} element
 * 
 * @author dhudson - created 5 Jul 2012
 * @since 1.4
 */
public class Xref extends HRefAttributeElement {

    public enum Scope {
        /**
         * Set scope to local when the resource is part of the current set of
         * content, and should be accessed and copied to the output directory.
         */
        LOCAL,
        /**
         * Set scope to peer when the resource is part of the current set of
         * content but is not accessible at build time.
         */
        PEER,
        /**
         * Set scope to external when the resource is not part of the current
         * information set and should open in a new browser window.
         */
        EXTERNAL,
    }

    public enum Format {
        /**
         * The destination uses DITA topic markup, or markup specialised from a
         * DITA topic. Unless otherwise specified, the corresponding default
         * type will be treated as "topic."
         */
        DITA,
        /**
         * The format of the linked-to resource is HTML or XHTML.
         */
        HTML,
        /**
         * The format of the linked-to resource is PDF (opens a new window).
         */
        PDF,
        /**
         * The linked-to resource is a DITA map. It represents a referenced
         * hierarchy at a position within referencing hierarchy, and a
         * referenced relationship table included outside the referencing
         * hierarchy
         */
        DITAMAP
    }

    public enum Type {
        /**
         * Link to a figure
         */
        FIG,
        /**
         * Link to a table
         */
        TABLE,
        /**
         * Link to a ordered list item
         */
        LI,
        /**
         * Link to a foot note
         */
        FN,
        /**
         * Link to a section
         */
        SECTION,
        /**
         * Link to a topic (concept, task, reference)
         */
        TOPIC,
    }

    /**
     * Create a new instance of <code>Xref</code>.
     */
    public Xref() {
    }

    /**
     * Creates a new instance of <code>Xref</code> with the given text
     * 
     * @param text for the <i>Xref</i> attribute
     */
    public Xref(String text) {
        super(text);
    }

    /**
     * Sets the scope attribute. The scope attribute identifies the closeness of
     * the relationship between the current document and the target resource.
     * 
     * @param scope to set
     * @since 2.1
     */
    public void setScope(Scope scope) {
        addAttribute("scope",scope.name().toLowerCase());
    }

    /**
     * Sets the format attribute identifies the format of the resource being
     * cross referenced.
     * 
     * @param format to set
     * @since 2.1
     */
    public void setFormat(Format format) {
        addAttribute("format",format.name().toLowerCase());
    }

    /**
     * Describes the target of a cross-reference and may generate
     * cross-reference text based on that description. Only the {@code <xref>}
     * element can link to content below the topic level: other types of linking
     * can target whole topics, but not parts of topics. Typically
     * {@code <xref>} should also be limited to topic-level targets, unless the
     * output is primarily print-oriented. Web-based referencing works best at
     * the level of whole topics, rather than anchor locations within topics.
     * 
     * @param type to set
     * @since 2.1
     */
    public void setType(Type type) {
        addAttribute("type",type.name().toLowerCase());
    }
}
