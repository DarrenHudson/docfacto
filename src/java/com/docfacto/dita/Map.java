/*
 * @author dhudson -
 * Created 5 Oct 2012 : 08:54:40
 */

package com.docfacto.dita;

/**
 * Simple call to wrap the {@code map} element
 * 
 * @author dhudson - created 5 Oct 2012
 * @since 2.1
 */
public class Map extends DITAElement {

    /**
     * Create a new instance of <code>Map</code>.
     */
    public Map() {
    }

}
