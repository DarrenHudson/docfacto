/*
 * @author dhudson -
 * Created 22 Nov 2012 : 12:06:46
 */

package com.docfacto.dita;

/**
 * Simple class to wrap the {@code dlentry} element
 * 
 * @author dhudson - created 22 Nov 2012
 * @since 2.1
 */
public class DlEntry extends DITAElement {

    /**
     * Constructor.
     */
    public DlEntry() {

    }

    /**
     * Constructor.
     * 
     * @param text
     */
    public DlEntry(String text) {
        super(text);
    }

    /**
     * Create a {@code dd} element and add it to the {@code dlentry}
     * 
     * @return the appended dd entry
     * @since 2.1
     */
    public Dd addDd() {
        final Dd entry = new Dd();
        addElement(entry);
        return entry;
    }

    /**
     * Create a {@code dd} element and add it to the {@code dlentry}
     * 
     * @param value to set the {@code dd} entry
     * @return the appened dd entry
     * @since 2.1
     */
    public Dd addDd(String value) {
        final Dd entry = new Dd(value);
        addElement(entry);
        return entry;
    }

    /**
     * Create a {@code dt} entry and add it to the {@code dlentry}
     * 
     * @return the appended dt entry
     * @since 2.1
     */
    public Dt addDt() {
        final Dt entry = new Dt();
        addElement(entry);
        return entry;
    }

    /**
     * Create a {@code dt} entry and add it to the {@code dlentry}
     * 
     * @param value to set the dt entry
     * @return the appended dt entry
     * @since 2.1
     */
    public Dt addDt(String value) {
        final Dt entry = new Dt(value);
        addElement(entry);
        return entry;
    }
}
