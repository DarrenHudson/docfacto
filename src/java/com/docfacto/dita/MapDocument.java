/*
 * @author dhudson -
 * Created 5 Oct 2012 : 08:42:22
 */

package com.docfacto.dita;

import com.docfacto.xml.XMLDocument;

/**
 * Class to encapsulate a DITA map file
 * 
 * @author dhudson - created 5 Oct 2012
 * @since 2.1
 */
public class MapDocument extends XMLDocument {

    /**
     * Constant Doc Type {@value}
     */
    private static final String DOCTYPE_DECL =
    "<!DOCTYPE map PUBLIC \"-//OASIS//DTD DITA Map//EN\" \"map.dtd\">";

    /**
     * Create a new instance of <code>MapDocument</code>.
     */
    public MapDocument() {
        theRootNode = new Map();
    }

    /**
     * Add a title element to the map
     * 
     * @param title to add to the map
     * @since 2.1
     */
    public void addTitle(String title) {
        theRootNode.addElement(new Title(title));
    }

    /**
     * @see com.docfacto.xml.XMLDocument#getDocumentDeclaration()
     */
    @Override
    public String getDocumentDeclaration() {
        return DOCTYPE_DECL;
    }
}
