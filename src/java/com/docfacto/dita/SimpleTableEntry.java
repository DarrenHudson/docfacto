/*
 * @author dhudson -
 * Created 27 Apr 2012 : 16:17:15
 */

package com.docfacto.dita;

/**
 * Wrapper class for the {@code stentry} DITA element.
 * 
 * @author dhudson - created 27 Apr 2012
 * @since 1.1
 */
public class SimpleTableEntry extends DITAElement {

    /**
     * Create a new instance of <code>SimpleTableEntry</code>.
     * 
     * @since 2.1
     */
    public SimpleTableEntry() {
    }

    /**
     * Creates a new instance of <code>SimpleTableEnty</code> with the given
     * value.
     * 
     * @param value of the table entry
     */
    public SimpleTableEntry(String value) {
        super(value);
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return "stentry";
    }

}
