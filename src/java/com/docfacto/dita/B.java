/*
 * @author dhudson -
 * Created 22 Nov 2012 : 12:15:16
 */

package com.docfacto.dita;

/**
 * Simple class to wrap the {@code B} element
 *
 * @author dhudson - created 22 Nov 2012
 * @since 2.2
 */
public class B extends DITAElement {

    /**
     * Constructor.
     */
    public B() {
    }

    /**
     * Constructor.
     * @param text to set
     */
    public B(String text) {
        super(text);
    }
}
