/*
 * @author dhudson -
 * Created 21 Nov 2012 : 15:06:47
 */

package com.docfacto.dita;

/**
 * Class top wrap the {@code ul} element
 *
 * @author dhudson - created 21 Nov 2012
 * @since 2.1
 */
public class Ul extends DITAElement {

    /**
     * Create a new instance of <code>Ul</code>.
     */
    public Ul() {
    }

    /**
     * Create a new instance of <code>Ul</code>.
     * @param text
     */
    public Ul(String text) {
        super(text);
    }

}
