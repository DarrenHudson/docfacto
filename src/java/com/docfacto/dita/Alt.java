package com.docfacto.dita;

/**
 * Wrapper class for the {@code alt} element
 * 
 * @author dhudson
 * @since 2.3
 */
public class Alt extends DITAElement {

	/**
	 * New Alt Element
	 */
	public Alt(String text) {
		super(text);
	}
}
