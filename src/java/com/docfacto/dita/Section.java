/*
 * @author dhudson -
 * Created 27 Apr 2012 : 17:03:32
 */

package com.docfacto.dita;

/**
 * Wrapper class for the <i>section</i> DITA Element
 * 
 * @author dhudson - created 22 May 2012
 * @since 1.4
 */
public class Section extends DITAElement {

    /**
     * Creates a new instance of <code>Section</code>.
     */
    public Section() {
    }

}
