/*
 * @author dhudson -
 * Created 22 Nov 2012 : 13:31:05
 */

package com.docfacto.dita;

/**
 * Simple class to wrap the {@code note} element
 * 
 * @author dhudson - created 22 Nov 2012
 * @since 2.1
 */
public class Note extends DITAElement {

    /**
     * Type attribute value
     * 
     * @author dhudson - created 22 Nov 2012
     * @since 2.1
     */
    public enum TYPE {
        NOTE,
        TIP,
        FASTPATH,
        RESTRICTION,
        IMPORTANT,
        REMEMBER,
        ATTENTION,
        CAUTION,
        NOTICE,
        DANGER,
        WARNING,
        OTHER
    }

    /**
     * Create a new instance of <code>Note</code>.
     */
    public Note() {
    }

    /**
     * Create a new instance of <code>Note</code>.
     * 
     * @param text
     */
    public Note(String text) {
        super(text);
    }

    /**
     * Sets the attribute {@code type} on the {@code note} element
     *
     * @param type to set
     * @since 2.1
     */
    public void setType(TYPE type) {
        addAttribute("type",type.toString().toLowerCase());
    }

}
