/*
 * @author dhudson -
 * Created 5 Oct 2012 : 12:33:37
 */

package com.docfacto.dita;

/**
 * Describes the target of a cross-reference and may generate cross-reference
 * text based on that description. Only the <i>xref</i> element can link to content
 * below the topic level: other types of linking can target whole topics, but
 * not parts of topics. Typically <i>xref</i> should also be limited to topic-level
 * targets, unless the output is primarily print-oriented. Web-based referencing
 * works best at the level of whole topics, rather than anchor locations within
 * topics.
 * 
 * When targeting DITA content, the type should match one of the values in the
 * target's class attribute. For example, if type="topic", the link could be to
 * a generic topic, or any specialization of topic, including concept, task, and
 * reference.
 * 
 * @author dhudson - created 5 Oct 2012
 * @since 2.1
 */
public enum TopicRefTypes {
    /**
     * Concept Topic
     */
    CONCEPT,
    /**
     * Task Topic
     */
    TASK,
    /**
     * Reference Topic
     */
    REFERENCE,
    /**
     * General Topic
     */
    TOPIC,
    /**
     * DITA map
     */
    DITAMAP,
    /**
     * Figure
     */
    FIG,
    /**
     * Table
     */
    TABLE,
    /**
     * Ordered list item
     */
    LI,
    /**
     * Footnote
     */
    FN;
}
