/*
 * @author dhudson -
 * Created 26 Nov 2012 : 11:55:56
 */

package com.docfacto.dita;

/**
 * Simple class to wrap the {@code parmname} element.
 *
 * @author dhudson - created 26 Nov 2012
 * @since 2.1
 */
public class ParmName extends DITAElement {

    /**
     * Create a new instance of <code>ParamName</code>.
     */
    public ParmName() {
    }

    /**
     * Constructor.
     * @param text
     */
    public ParmName(String text) {
        super(text);
    }

}
