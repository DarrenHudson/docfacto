/*
 * @author dhudson -
 * Created 26 Nov 2012 : 11:52:53
 */

package com.docfacto.dita;

/**
 * Wrapper class around the {@code apiname} element
 * 
 * @author dhudson - created 26 Nov 2012
 * @since 2.1
 */
public class ApiName extends DITAElement {

    /**
     * Create a new instance of <code>ApiName</code>.
     */
    public ApiName() {

    }

    /**
     * Create a new instance of <code>ApiName</code>.
     * 
     * @param text
     */
    public ApiName(String text) {
        super(text);
    }

}
