/*
 * @author dhudson -
 * Created 22 Nov 2012 : 12:10:51
 */

package com.docfacto.dita;

/**
 * Simple class to wrap the {@code dt} element
 *
 * @author dhudson - created 9 Mar 2013
 * @since 2.1
 */
public class Dt extends DITAElement {

    /**
     * Constructor.
     */
    public Dt() {

    }

    /**
     * Constructor.
     * @param text
     */
    public Dt(String text) {
        super(text);
    }

}
