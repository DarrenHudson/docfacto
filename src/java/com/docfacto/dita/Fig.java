package com.docfacto.dita;

/**
 * Class wrapper for the {@code fig} element
 * 
 * @author dhudson
 * @since 2.3
 */
public class Fig extends DITAElement {

	/**
	 * Fig
	 */
	public Fig() {
	}
}
