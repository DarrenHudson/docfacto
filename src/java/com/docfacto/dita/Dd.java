/*
 * @author dhudson -
 * Created 22 Nov 2012 : 12:02:38
 */

package com.docfacto.dita;

/**
 * Simple class to wrap the {@code dd} element
 * 
 * @author dhudson - created 22 Nov 2012
 * @since 2.1
 */
public class Dd extends DITAElement {

    /**
     * Constructor.
     */
    public Dd() {

    }

    /**
     * Constructor.
     * 
     * @param text
     */
    public Dd(String text) {
        super(text);
    }

}
