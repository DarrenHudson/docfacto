/*
 * @author dhudson -
 * Created 22 May 2012 : 11:35:26
 */

package com.docfacto.dita;

/**
 * Wrapper class for the {@code refbody} DITA element
 * 
 * @author dhudson - created 6 Jul 2012
 * @since 2.0
 */
public class RefBody extends DITAElement {

    /**
     * Creates a new instance of <code>RefBody</code>.
     */
    public RefBody() {

    }

}
