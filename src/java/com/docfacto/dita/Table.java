/*
 * @author dhudson -
 * Created 26 Nov 2012 : 14:56:14
 */

package com.docfacto.dita;

/**
 * Simple class to wrap the {@code table} element
 *
 * @author dhudson - created 26 Nov 2012
 * @since 2.1
 */
public class Table extends DITAElement
{

    /**
     * Constructor.
     */
    public Table() {
    }

    /**
     * Create a {@code group} element and append it to the table
     *
     * @return the newly created {@code group} element
     * @since 2.1
     */
    public TGroup addTableGroup() {
        final TGroup tGroup = new TGroup();
        addElement(tGroup);
        return tGroup;
    }
}
