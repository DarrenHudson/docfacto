/*
 * @author dhudson -
 * Created 22 Nov 2012 : 13:06:33
 */

package com.docfacto.dita;

import com.docfacto.xml.XMLTextElement;

/**
 * Simple class to wrap the {@code pre} element.
 *
 * @author dhudson - created 9 Mar 2013
 * @since 2.1
 */
public class Pre extends DITAElement {

    /**
     * Constructor.
     */
    public Pre() {
        super();
    }

    /**
     * Constructor.
     * @param text
     */
    public Pre(String text) {
        final XMLTextElement textEl = new XMLTextElement(text,true);
        addElement(textEl);
    }

}
