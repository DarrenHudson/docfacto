/*
 * @author dhudson -
 * Created 26 Nov 2012 : 15:15:16
 */

package com.docfacto.dita;

import com.docfacto.xml.XMLTextElement;

/**
 * Simple class to wrap {@code row} element
 *
 * @author dhudson - created 26 Nov 2012
 * @since 2.1
 */
public class Row extends DITAElement {

    /**
     * Constructor.
     */
    public Row() {
    }

    /**
     * Append a {@code entry} element to the row
     *
     * @return the newly created entry element
     * @since 2.1
     */
    public Entry addEntry() {
        final Entry entry = new Entry();
        addElement(entry);
        return entry;
    }

    /**
     * Append a {@code entry} element to the row
     *
     * @param text to set in the {@code entry}
     * @return the newly created entry element
     * @since 2.1
     */
    public Entry addEntry(String text) {
        final Entry entry = addEntry();
        entry.addElement(new XMLTextElement(text));
        return entry;
    }
}
