/*
 * @author dhudson -
 * Created 28 Apr 2012 : 08:35:27
 */

package com.docfacto.dita;

import com.docfacto.xml.Element;

/**
 * DITA Element wrapper class for future use
 * 
 * @author dhudson - created 22 May 2012
 * @since 1.4
 */
public abstract class DITAElement extends Element {

    /**
     * Use to indicate operating system and hardware. This is a property
     * attribute which supports conditional processing for filtering or flagging
     */
    public static final String PLATFORM_ATTRIBUTE = "platform";
    /**
     * Contains the name of the product to which the topic applies. This is a
     * property attribute which supports conditional processing for filtering or
     * flagging.
     */
    public static final String PRODUCT_ATTRIBUTE = "product";
    /**
     * Indicates the intended audience for the element. This is a property
     * attribute which supports conditional processing for filtering or
     * flagging.
     */
    public static final String AUDIENCE_ATTRIBUTE = "audience";
    /**
     * This attribute can be used for any other properties that might be needed
     * to describe an audience, or to provide selection criteria for the
     * element.
     */
    public static final String OTHERPROPS_ATTRIBUTE = "otherprops";
    /**
     * A range of values that describe an importance or priority attributed to
     * an element. For example, in steps of a task, the attribute indicates
     * whether a step is optional or required. In other contexts or
     * specializations, other values are possible.
     */
    public static final String IMPORTANCE_ATTRIBUTE = "importance";
    /**
     * Indicates revision level of an element. It is useful for flagging outputs
     * based on revision.
     */
    public static final String REV_ATTRIBUTE = "rev";
    /**
     * The status of the current element.
     */
    public static final String STATUS_ATTRIBUTE = "status";

    /**
     * A range of values that describe an importance or priority attributed to
     * an element.
     * 
     * @author dhudson - created 5 Oct 2012
     * @since 2.1
     */
    public enum Importance {
        OBSOLETE,
        DEPRECATED,
        OPTIONAL,
        DEFAULT,
        LOW,
        NORMAL,
        HIGH,
        RECOMMENDED,
        REQUIRED,
        URGENT;
    }

    /**
     * The status of the current element.
     * 
     * @author dhudson - created 5 Oct 2012
     * @since 2.1
     */
    public enum Status {
        NEW,
        CHANGED,
        DELETED,
        UNCHANGED;
    }

    /**
     * Creates a new instance of <code>DITAElement</code>.
     */
    public DITAElement() {
        super(null);
    }

    /**
     * Creates a new instance of <code>DITAElement</code> with the given value.
     * 
     * @param value to set for the element. Element can not have values and
     * children
     */
    public DITAElement(String value) {
        super(value);
    }

    /**
     * Indicates operating system and hardware. This is a property attribute
     * which supports conditional processing for filtering or flagging.
     * 
     * @param value to set the <i>platform</i> attribute
     * @since 2.1
     */
    public void setPlatform(String value) {
        addAttribute(PLATFORM_ATTRIBUTE,value);
    }

    /**
     * Contains the name of the product to which the topic applies. This is a
     * property attribute which supports conditional processing for filtering or
     * flagging.
     * 
     * @param value to set the <i>product</i> attribute
     * @since 2.1
     */
    public void setProduct(String value) {
        addAttribute(PRODUCT_ATTRIBUTE,value);
    }

    /**
     * Indicates the intended audience for the element. This is a property
     * attribute which supports conditional processing for filtering or
     * flagging.
     * 
     * @param value to set the <i>audience</i> attribute
     * @since 2.1
     */
    public void setAudience(String value) {
        addAttribute(AUDIENCE_ATTRIBUTE,value);
    }

    /**
     * This attribute can be used for any other properties that might be needed
     * to describe an audience, or to provide selection criteria for the
     * element.
     * 
     * @param value to set the <i>otherprops</i> attribute
     * @since 2.1
     */
    public void setOtherProps(String value) {
        addAttribute(OTHERPROPS_ATTRIBUTE,value);
    }

    /**
     * A range of values that describe an importance or priority attributed to
     * an element. For example, in steps of a task, the attribute indicates
     * whether a step is optional or required. In other contexts or
     * specializations, other values are possible.
     * 
     * @param importance to set
     * @since 2.1
     */
    public void setImportance(Importance importance) {
        addAttribute(IMPORTANCE_ATTRIBUTE,importance.name().toLowerCase());
    }

    /**
     * Indicates revision level of an element. It is useful for flagging outputs
     * based on revision.
     * 
     * @param value to set the <i>rev</i> attribute
     * @since 2.1
     */
    public void setRev(String value) {
        addAttribute(REV_ATTRIBUTE,value);
    }

    /**
     * Set the status of the current element.
     * 
     * @param status to set
     * @since 2.1
     */
    public void setStatus(Status status) {
        addAttribute(STATUS_ATTRIBUTE,status.name().toLowerCase());
    }

    /**
     * Returns the class simple name in a lower case form, if this is not the
     * case then this class needs to be over-ridden
     * 
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return getClass().getSimpleName().toLowerCase();
    }

}
