/*
 * @author dhudson -
 * Created 22 May 2012 : 11:27:40
 */

package com.docfacto.dita;


/**
 * Wrapper class for the {@code reference} DITA element
 *
 * @author dhudson - created 22 May 2012
 * @since 2.0
 */
public class Reference extends DITAElement {

    /**
     * Creates a new instance of <code>Reference</code>.
     */
    public Reference() {
    }

}
