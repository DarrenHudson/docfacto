/*
 * @author dhudson -
 * Created 22 Nov 2012 : 13:02:36
 */

package com.docfacto.dita;

import com.docfacto.xml.XMLTextElement;

/**
 * Simple class to wrap the {@code codeblock} element
 * 
 * @author dhudson - created 22 Nov 2012
 * @since 2.1
 */
public class CodeBlock extends DITAElement {

    /**
     * Create a new instance of <code>CodeBlock</code>.
     */
    public CodeBlock() {
        super();
    }

    /**
     * Create a new instance of <code>CodeBlock</code>. The text in this case is
     * created as an {@code XMLTextElement} with preserve set
     * 
     * @param text to assign to the codeblock
     */
    public CodeBlock(String text) {
        super();
        final XMLTextElement textEl = new XMLTextElement(text, true);
        addElement(textEl);
    }

}
