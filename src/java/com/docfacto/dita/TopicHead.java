/*
 * @author dhudson -
 * Created 5 Oct 2012 : 09:35:58
 */

package com.docfacto.dita;

/**
 * Simple class to wrap the {@code topichead} element
 *
 * @author dhudson - created 5 Oct 2012
 * @since 2.1
 */
public class TopicHead extends DITAElement {

    /**
     * Create a new instance of <code>TopicHead</code>.
     */
    public TopicHead() {
    }

}
