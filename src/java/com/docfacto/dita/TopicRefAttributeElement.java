/*
 * @author dhudson -
 * Created 5 Oct 2012 : 12:27:21
 */

package com.docfacto.dita;

import com.docfacto.common.StringUtils;

/**
 * Wrapper class.
 * 
 * @author dhudson - created 5 Oct 2012
 * @since 2.0
 */
abstract class TopicRefAttributeElement extends HRefAttributeElement {

    /**
     * Create a new instance of <code>TopicRefAttributeElement</code>.
     */
    TopicRefAttributeElement() {
    }

    /**
     * Create a new instance of <code>TopicRefAttributeElement</code>.
     * 
     * @param value
     */
    TopicRefAttributeElement(String value) {
        super(value);
    }

    /**
     * Describes the target of a cross-reference and may generate
     * cross-reference text based on that description. Only the <i>xref</i>
     * element can link to content below the topic level: other types of linking
     * can target whole topics, but not parts of topics. Typically <i>xref</i>
     * should also be limited to topic-level targets, unless the output is
     * primarily print-oriented. Web-based referencing works best at the level
     * of whole topics, rather than anchor locations within topics.
     * 
     * @param type
     * @since 2.1
     */
    public void setType(TopicRefTypes type) {
        addAttribute("type",type.name().toLowerCase());
    }

    /**
     * The scope attribute identifies the closeness of the relationship between
     * the current document and the target resource.
     * 
     * @param scope
     * @since 2.1
     */
    public void setScope(TopicRefScope scope) {
        addAttribute("scope",scope.name().toLowerCase());
    }

    /**
     * Collection types describe how links relate to each other. A family
     * represents a tight grouping in which each of the referenced topics not
     * only relates to the current topic but also relate to each other.
     * 
     * @param collectionType
     * @since 2.1
     */
    public void setCollectionType(TopicRefCollectionType collectionType) {
        addAttribute("collection-type",collectionType.name().toLowerCase());
    }

    /**
     * This attribute makes sure the navtitle attribute is used if it is
     * present; if locktitle isn't set to "yes", the navtitle attribute is
     * ignored and text is retrieved from the target.
     * 
     * @param value to set
     * @since 2.1
     */
    public void setLockTitle(boolean value) {
        addAttribute("locktitle",StringUtils.booleanYesNo(value));
    }

    /**
     * Specifies whether a topic appears in the table of contents (toc).
     * 
     * @param value to set
     * @since 2.1
     */
    public void setToc(boolean value) {
        addAttribute("toc",StringUtils.booleanYesNo(value));
    }

    /**
     * Specifies whether the topic should be included in a portable document format (PDF) file
     *
     * @param value to set
     * @since 2.1
     */
    public void setPrint(boolean value) {
        addAttribute("print",StringUtils.booleanYesNo(value));
    }
}
