/*
 * @author dhudson -
 * Created 22 May 2012 : 11:11:34
 */

package com.docfacto.dita;

/**
 * Wrapper class around the {@code sthead} DITA element
 * 
 * @author dhudson - created 5 Jul 2012
 * @since 2.0
 */
public class SimpleTableHeader extends DITAElement {

    /**
     * Creates a new instance of <code>SimpleTableHeader</code>
     */
    public SimpleTableHeader() {
    }

    /**
     * Create a new instance of <code>SimpleTableHeader</code> and add one
     * header entry
     * 
     * @param header to add
     * 
     * @since 2.1
     */
    public SimpleTableHeader(String header) {
        addElement(new SimpleTableEntry(header));
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return "sthead";
    }

    /**
     * A helper method to create <i>stentry</i> for the headers provided
     * 
     * @param headers creates {@code SimpleTableEntry}
     * @since 2.0
     */
    public void addHeaders(String... headers) {
        for (final String header:headers) {
            addElement(new SimpleTableEntry(header));
        }
    }
}
