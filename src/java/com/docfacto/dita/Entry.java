/*
 * @author dhudson -
 * Created 26 Nov 2012 : 15:15:48
 */

package com.docfacto.dita;

/**
 * Simple call to wrap the {@code entry} element
 * 
 * @author dhudson - created 26 Nov 2012
 * @since 2.1
 */
public class Entry extends DITAElement {

    /**
     * Constructor.
     */
    public Entry() {

    }

    /**
     * Constructor.
     * @param text
     */
    public Entry(String text) {
        super(text);
    }
}
