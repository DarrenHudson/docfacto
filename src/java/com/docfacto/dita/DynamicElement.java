/*
 * @author dhudson -
 * Created 19 Nov 2012 : 15:44:09
 */

package com.docfacto.dita;

/**
 * This element is different from the other elements as the name of the element
 * needs to be supplied.
 * 
 * @author dhudson - created 19 Nov 2012
 * @since 2.1
 */
public class DynamicElement extends DITAElement {

    private final String theElementName;

    /**
     * Create a new instance of <code>DynamicElement</code>.
     * @param elementName
     */
    public DynamicElement(String elementName) {
        super();
        theElementName = elementName;
    }

    /**
     * Create a new instance of <code>DynamicElement</code>.
     * @param elementName
     * @param value
     */
    public DynamicElement(String elementName,String value) {
        super(value);
        theElementName = elementName;
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return theElementName;
    }

}
