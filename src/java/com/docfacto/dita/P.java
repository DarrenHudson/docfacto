/*
 * @author dhudson -
 * Created 27 Apr 2012 : 17:03:54
 */

package com.docfacto.dita;

/**
 * Wrapper class around the {@code p} dita element
 * 
 * @author dhudson - created 22 May 2012
 * @since 1.4
 */
public class P extends DITAElement {

    /**
     * Create a new instance of <code>P</code>.
     * 
     * @since 2.1
     */
    public P(){
    }

    /**
     * Creates a new instance of <code>P</code>.
     * 
     * @param text for the paragraph element
     */
    public P(String text) {
        super(text);
    }
}
