package com.docfacto.dita;

import java.io.IOException;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Takes the DITA Home directory and will resolve the dtd's from
 * dtd/technicalContent/dtd
 * 
 * @author dhudson - created 22 May 2012
 * @since 1.4
 */
public class DITAURIResolver implements EntityResolver {

    /**
     * Holds the DITA toolkit home path
     */
    private final String theDITAHomeFolder;
    /**
     * Constant for the directory that holds the DTD {@value}
     */
    private static final String DTD_DIR = "dtd/technicalContent/dtd";

    /**
     * Creates a new instance of <code>DITAURIResolver</code>.
     * 
     * @param DITAHomeFolder the home folder of the DITA toolkit
     */
    DITAURIResolver(String DITAHomeFolder) {
        theDITAHomeFolder = DITAHomeFolder;
    }

    /**
     * @see org.xml.sax.EntityResolver#resolveEntity(java.lang.String,
     * java.lang.String)
     */
    @Override
    public InputSource resolveEntity(String publicID,String systemID)
    throws SAXException, IOException {
        if (systemID.endsWith("reference.dtd")) {
            return new InputSource(theDITAHomeFolder+"/"+DTD_DIR+"/"+
            "reference.dtd");
        }
        return null;
    }

}
