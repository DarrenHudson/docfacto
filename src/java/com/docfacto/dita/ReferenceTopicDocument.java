/*
 * @author dhudson -
 * Created 22 May 2012 : 13:44:02
 */

package com.docfacto.dita;

import com.docfacto.xml.XMLDocument;

/**
 * Class that encapsulates a <code>ReferenceTopic</code> document.
 * 
 * @author dhudson - created 22 May 2012
 * @since 2.0
 */
public class ReferenceTopicDocument extends XMLDocument {

    /**
     * Constant Doc Type {@value}
     */
    private static final String DOCTYPE_DECL =
    "<!DOCTYPE reference PUBLIC \"-//OASIS//DTD DITA Reference//EN\" \"reference.dtd\">";

    /**
     * Creates a new instance of <code>ReferenceTopicDocument</code>.
     * 
     * This will will have its document type set.
     */
    public ReferenceTopicDocument() {
        theRootNode = new Reference();
    }

    /**
     * Return the root <code>reference</code> node
     * 
     * @return the {@code reference} root node
     * @since 2.0
     */
    public Reference getRootNode() {
        return (Reference)theRootNode;
    }

    /**
     * @see com.docfacto.xml.XMLDocument#getDocumentDeclaration()
     */
    @Override
    public String getDocumentDeclaration() {
        return DOCTYPE_DECL;
    }

    /**
     * Sets the attribute <i>id</i> for the root reference element
     *
     * @param id of the root node
     * @since 2.1
     */
    public void setReferenceID(String id) {
        theRootNode.setID(id);
    }
}
