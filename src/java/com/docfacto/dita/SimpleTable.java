/*
 * @author dhudson -
 * Created 27 Apr 2012 : 16:14:19
 */

package com.docfacto.dita;

/**
 * Wrapper class for the {@code simpletable} DITA Element
 * 
 * @author dhudson - created 6 Jul 2012
 * @since 2.0
 */
public class SimpleTable extends DITAElement {

    /**
     * Creates a new instance of <code>SimpleTable</code>.
     */
    public SimpleTable() {
    }

    /**
     * Add {@code SimpleTableHeader}s to the table
     * 
     * @param headers to add to the table
     * @since 2.1
     */
    public void addHeaders(String... headers) {
        final SimpleTableHeader hds = new SimpleTableHeader();
        hds.addHeaders(headers);

        addElement(hds);
    }
}
