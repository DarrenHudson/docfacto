/*
 * @author dhudson -
 * Created 22 Oct 2012 : 11:39:22
 */

package com.docfacto.dita;

import java.io.File;

import com.docfacto.common.IOUtils;

/**
 * Collection of DITA Utils.
 * 
 * @author dhudson - created 22 Oct 2012
 * @since 2.1
 */
public final class DITAUtils {

    private static final String DOT_SLASH = "." + File.separator;
    
    /**
     * Create a new instance of <code>DITAUtils</code>.
     */
    private DITAUtils() {
    }

    /**
     * Returns the relative path between two files. The OT doesn't do this and
     * gets very confused.
     * 
     * @param absolutePath1
     * @param absolutePath2
     * @return a string representing a path sequence from file 1 to file 2
     * @since 2.1
     */
    public static String getRelativePath(String absolutePath1,
    String absolutePath2) {
        String relPath = IOUtils.getRelativePath(absolutePath1,absolutePath2);
        if (relPath.startsWith(DOT_SLASH)) {
            // Remove the ./
            relPath = relPath.substring(2);
        }

        if (relPath.endsWith(File.separator)) {
            relPath = relPath.substring(0,relPath.length()-1);
        }

        return relPath;
    }
}
