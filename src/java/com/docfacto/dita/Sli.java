/*
 * @author dhudson -
 * Created 21 Nov 2012 : 15:39:27
 */

package com.docfacto.dita;

/**
 * Simple class to wrap the {@code sli} element
 * 
 * @author dhudson - created 21 Nov 2012
 * @since 2.1
 */
public class Sli extends DITAElement {

    /**
     * Create a new instance of <code>Sli</code>.
     */
    public Sli() {
    }

    /**
     * Create a new instance of <code>Sli</code>.
     * 
     * @param text
     */
    public Sli(String text) {
        super(text);
    }
}
