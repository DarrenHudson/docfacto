/*
 * @author dhudson -
 * Created 5 Oct 2012 : 09:08:06
 */

package com.docfacto.dita;

/**
 * Simple class to wrap the {@code topicref} element
 * 
 * @author dhudson - created 5 Oct 2012
 * @since 2.1
 */
public class TopicRef extends TopicRefAttributeElement {

    /**
     * Create a new instance of <code>TopicRef</code>.
     */
    public TopicRef() {
    }
}
