/*
 * @author dhudson -
 * Created 5 Oct 2012 : 09:28:14
 */

package com.docfacto.dita;

/**
 * Abstract class for elements that have {@code href} attribute
 *
 * @author dhudson - created 5 Oct 2012
 * @since 2.1
 */
abstract class HRefAttributeElement extends DITAElement {

    HRefAttributeElement() {
    }

    /**
     * Create a new instance of <code>HRefAttributeElement</code>.
     * @param value
     */
    HRefAttributeElement(String value) {
        super(value);
    }

    /**
     * Sets the hyper text reference
     * 
     * @param href the reference
     * @since 2.1
     */
    public void setHref(String href) {
        addAttribute("href",href);
    }
}
