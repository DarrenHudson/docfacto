/*
 * @author dhudson -
 * Created 28 Nov 2012 : 14:49:33
 */

package com.docfacto.dita;

/**
 * Simple class to wrap the {@code codeph} element
 *
 * @author dhudson - created 28 Nov 2012
 * @since 2.2
 */
public class CodePh extends DITAElement {

    /**
     * Constructor.
     */
    public CodePh() {
    }

    /**
     * Constructor.
     * @param text
     */
    public CodePh(String text) {
        super(text);
    }
}
