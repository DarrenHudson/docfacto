/*
 * @author dhudson -
 * Created 22 Feb 2013 : 17:01:44
 */

package com.docfacto.dita;

/**
 * Simple wrapper class around the {@code wintitle} element
 *
 * @author dhudson - created 22 Feb 2013
 * @since 2.2
 */
public class WinTitle extends DITAElement {

    /**
     * Create a new instance of <code>WinTitle</code>.
     */
    public WinTitle() {
    }

    /**
     * Create a new instance of <code>WinTitle</code>.
     * @param text to add to the title
     */
    public WinTitle(String text) {
        super(text);
    }
}
