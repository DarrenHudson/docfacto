/*
 * @author dhudson -
 * Created 21 Nov 2012 : 15:08:48
 */

package com.docfacto.dita;

/**
 * Simple class to wrap the {@code li} element
 *
 * @author dhudson - created 21 Nov 2012
 * @since 2.1
 */
public class Li extends DITAElement {

    /**
     * Create a new instance of <code>Li</code>.
     */
    public Li() {

    }

    /**
     * Create a new instance of <code>Li</code>.
     * @param text
     */
    public Li(String text) {
        super(text);
    }
}
