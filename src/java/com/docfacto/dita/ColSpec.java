/*
 * @author dhudson -
 * Created 26 Nov 2012 : 15:13:01
 */

package com.docfacto.dita;

/**
 * Simple class to wrap the {@code colspec} element
 *
 * @author dhudson - created 26 Nov 2012
 * @since 2.2
 */
public class ColSpec extends DITAElement {

    public ColSpec() {
    }

    /**
     * Sets the colname attribute
     *
     * @param name
     * @since 2.1
     */
    public void setName(String name) {
        addAttribute("colname",name);
    }

    /**
     * Adds the colwidth Attribute
     *
     * @param value
     * @since 2.1
     */
    public void setColWidth(String value) {
        addAttribute("colwidth",value);
    }
}
