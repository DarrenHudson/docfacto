/*
 * @author dhudson -
 * Created 27 Apr 2012 : 16:17:04
 */

package com.docfacto.dita;

/**
 * Wrapper class for the {@code strow} element.
 * <p>
 * {@docfacto.note strow elements, contain simple table entry <i>stentry</i> elements }
 * </p>
 * @author dhudson - created 27 Apr 2012
 * @since 2.0
 */
public class SimpleTableRow extends DITAElement {


    /**
     * Creates an instance of <code>SimpleTableRow<code>
     */
    public SimpleTableRow() {
        super();
    }

    /**
     * Creates an instance of <code>SimpleTableRow</code> with the given value
     * 
     * @param value for the simple table row
     */
    public SimpleTableRow(String value) {
        super(value);
    }

    /**
     * Convenience method for quickly adding a <i>stentry</i> element
     * 
     * @param value for a newly created {@code SimpleTableEntry}
     * @return the newly created entry
     * 
     * @since 2.0
     */
    public SimpleTableEntry addTableEntry(String value) {
        final SimpleTableEntry entry = new SimpleTableEntry(value);
        addElement(entry);
        return entry;
    }

    /**
     * Convenience method for quickly adding a collection <i>stentry</i>
     * elements
     * 
     * @param entries create many {@code SimpleTableEntry} entries
     * @since 2.0
     */
    public void addTableEntries(String... entries) {
        for (final String entry:entries) {
            addElement(new SimpleTableEntry(entry));
        }
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return "strow";
    }

}
