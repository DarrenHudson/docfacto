/*
 * @author dhudson -
 * Created 4 Dec 2012 : 16:50:32
 */

package com.docfacto.dita;

/**
 * Simple classs to wrap the {@code i} element
 *
 * @author dhudson - created 4 Dec 2012
 * @since 2.1
 */
public class I extends DITAElement {

    /**
     * Create a new instance of <code>I</code>.
     */
    public I() {
        super();
    }

    /**
     * Create a new instance of <code>I</code>.
     * @param value
     */
    public I(String value) {
        super(value);
    }
}
