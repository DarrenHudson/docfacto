/*
 * @author dhudson -
 * Created 22 Nov 2012 : 12:03:37
 */

package com.docfacto.dita;

/**
 * Simple class to wrap the {@code dl} element
 * 
 * @author dhudson - created 22 Nov 2012
 * @since 2.1
 */
public class Dl extends DITAElement {

    /**
     * Constructor.
     */
    public Dl() {

    }

    /**
     * Constructor.
     * @param text
     */
    public Dl(String text) {
        super(text);
    }

    /**
     * Create a DlEntry and add it as a child of this node
     * 
     * @return the newly created {@code DlEntry}
     * @since 2.1
     */
    public DlEntry addDlEntry() {
        final DlEntry entry = new DlEntry();
        addElement(entry);
        return entry;
    }

}
