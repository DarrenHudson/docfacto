/*
 * @author dhudson -
 * Created 21 Nov 2012 : 15:23:38
 */

package com.docfacto.dita;

/**
 * Simple class to wrap the {@code sl} element
 *
 * @author dhudson - created 21 Nov 2012
 * @since 2.1
 */
public class Sl extends DITAElement {

    /**
     * Create a new instance of <code>Sl</code>.
     */
    public Sl() {

    }

    /**
     * Create a new instance of <code>Sl</code>.
     * @param text
     */
    public Sl(String text) {
        super(text);
    }

}
