/*
 * @author dhudson -
 * Created 5 Oct 2012 : 09:32:59
 */

package com.docfacto.dita;

/**
 * Simple class tp wrap the {@code mapref} element
 * 
 * @author dhudson - created 5 Oct 2012
 * @since 2.1
 */
public class MapRef extends TopicRefAttributeElement {

    /**
     * Create a new instance of <code>MapRef</code>.
     */
    public MapRef() {
    }

}
