/*
 * @author dhudson -
 * Created 5 Oct 2012 : 12:40:29
 */

package com.docfacto.dita;

/**
 * The scope attribute identifies the closeness of the relationship between the
 * current document and the target resource.
 * 
 * @author dhudson - created 5 Oct 2012
 * @since 2.1
 */
public enum TopicRefScope {
    /**
     * when the resource is part of the current set of content, and should be
     * accessed and copied to the output directory
     */
    LOCAL,
    /**
     * when the resource is part of the current set of content but is not
     * accessible at build time
     */
    PEER,
    /**
     * when the resource is not part of the current information set and should
     * open in a new browser window.
     */
    EXTERNAL
}
