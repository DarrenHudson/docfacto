/*
 * @author dhudson -
 * Created 14 Nov 2012 : 13:13:52
 */

package com.docfacto.dita;

/**
 * Simple wrapper class around the {@code lines} element
 * 
 * @author dhudson - created 14 Nov 2012
 * @since 2.1
 */
public class Lines extends DITAElement {

    /**
     * Create a new instance of <code>Lines</code>.
     */
    public Lines() {
    }

    /**
     * Create a new instance of <code>Lines</code>.
     * 
     * @param text
     */
    public Lines(String text) {
        super(text);
    }

}
