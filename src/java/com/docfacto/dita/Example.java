/*
 * @author dhudson -
 * Created 22 Feb 2013 : 13:10:45
 */

package com.docfacto.dita;

/**
 * Wrapper class round the {@code example} element
 *
 * @author dhudson - created 22 Feb 2013
 * @since 2.2
 */
public class Example extends DITAElement {

    /**
     * Create a new instance of <code>Example</code>.
     */
    public Example(){
    }
}
