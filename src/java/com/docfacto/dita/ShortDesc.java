/*
 * @author dhudson -
 * Created 28 Apr 2012 : 11:19:26
 */

package com.docfacto.dita;


/**
 * Wrapper for the {@code shortdesc} dita element
 * 
 * @author dhudson - created 22 May 2012
 * @since 1.4
 */
public class ShortDesc extends DITAElement {

    /**
     * Creates a new instance of <code>ShortDesc</code> with the given value.
     * 
     * @param text the short description
     */
    public ShortDesc(String text) {
        super(text);
    }
}
