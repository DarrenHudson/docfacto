/*
 * @author dhudson -
 * Created 26 Nov 2012 : 14:59:23
 */

package com.docfacto.dita;

/**
 * Wrapper class round the DITA {@code tgroup} element.
 *
 * @author dhudson - created 26 Nov 2012
 * @since 2.1
 */
public class TGroup extends DITAElement {

    /**
     * Create a new instance of <code>TGroup</code>.
     */
    public TGroup() {
    }

    /**
     * Add a ColSpec with a given name
     *
     * @param name of the col spec
     * @return the newly created {@code ColSpec}
     * @since 2.1
     */
    public ColSpec addColSpec(String name) {
        final ColSpec colSpec = new ColSpec();
        colSpec.addAttribute("colname",name);
        addElement(colSpec);

        return colSpec;
    }

    /**
     * Create, append and return a {@code tbody}
     *
     * @return the newly created {@code tbody}
     * @since 2.1
     */
    public TBody addTBody() {
        final TBody tBody = new TBody();
        addElement(tBody);
        return tBody;
    }
}
