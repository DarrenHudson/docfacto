/*
 * @author dhudson -
 * Created 5 Oct 2012 : 12:45:22
 */

package com.docfacto.dita;

/**
 * Collection types describe how links relate to each other. A family represents
 * a tight grouping in which each of the referenced topics not only relates to
 * the current topic but also relate to each other.
 * 
 * @author dhudson - created 5 Oct 2012
 * @since 2.1
 */
public enum TopicRefCollectionType {
    UNORDERED,
    SEQUENCE,
    CHOICE,
    FAMILY
}
