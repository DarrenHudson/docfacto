package com.docfacto.ant.fogbugz;

import java.util.Set;
import java.util.TreeSet;

/**
 * Encapsulates the details of the release notes for a particular release build.
 * 
 * @author pwalsh
 * 
 */
public final class FogBugzReleaseNotes {

    /**
     * The project name - e.g. "Links" or null for all
     */
    private String theProject;

    /**
     * The release build number - e.g. "2.1.1"
     */
    private String theRelease;

    private String theBuildDate = "";

    /**
     * The (resolved) bugs for the release build
     */
    private Set<FogBugzCase> theBugs = new TreeSet<FogBugzCase>();

    /**
     * The resolve features (and other types) for the release build.
     */
    private Set<FogBugzCase> theFeatures = new TreeSet<FogBugzCase>();

    /**
     * Constructor.
     * 
     * @param project name
     * @param release number
     */
    public FogBugzReleaseNotes(String project,String release) {
        theProject = project;
        theRelease = release;
    }

    /**
     * Returns project.
     * 
     * @return the project
     */
    public String getProject() {
        return theProject;
    }

    /**
     * Returns release.
     * 
     * @return the release
     */
    public String getRelease() {
        return theRelease;
    }

    /**
     * Returns buildDate.
     * 
     * @return the buildDate
     */
    public String getBuildDate() {
        return theBuildDate;
    }

    /**
     * Sets buildDate.
     * 
     * @param buildDate the buildDate value
     */
    public void setBuildDate(String buildDate) {
        theBuildDate = buildDate;
    }

    /**
     * Are there bugs for the release build?
     * 
     * @return
     */
    public boolean hasBugs() {
        return (!theBugs.isEmpty());
    }

    /**
     * Returns bugs.
     * 
     * @return the bugs
     */
    public Set<FogBugzCase> getBugs() {
        return theBugs;
    }

    /**
     * Are there features (or anything other than bugs) for the release build.
     * 
     * @return
     */
    public boolean hasFeatures() {
        return (!theFeatures.isEmpty());
    }

    /**
     * Returns features.
     * 
     * @return the features
     */
    public Set<FogBugzCase> getFeatures() {
        return theFeatures;
    }

    /**
     * Add a case. Only adds it if it applies to the project, is for the same
     * release and has a release note otherwise it is ignored.
     * 
     * @param aCase
     */
    public void addCase(FogBugzCase aCase) {
        if (aCase.hasReleaseNotes()) {
            if (theRelease.equals(aCase.getRelease())) {
                if (theProject==null||theProject.equals(aCase.getProject())) {

                    if (aCase.isBug()) {
                        theBugs.add(aCase);
                    }
                    else if (aCase.isFeature()) {
                        theFeatures.add(aCase);
                    }
                }
            }
        }
    }
}
