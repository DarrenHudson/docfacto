package com.docfacto.ant.fogbugz;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Encapsulates all releases for a project
 * 
 */
public class FogBugzReleases {

    @SuppressWarnings("unused")
    private String theProject;
    private final Map<String,String> theReleaseDates =
    new LinkedHashMap<String,String>();
    private final SimpleDateFormat theInputDateFormat =
    new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat DATE_FORMAT =
    new SimpleDateFormat("d MMM yyyy");
    private final Date theToday = new Date();

    public FogBugzReleases() {
    }

    void addReleaseDate(String release,String releaseDate) {
        String formattedDate = releaseDate;
        final int tIndex = formattedDate.indexOf("T");
        if (tIndex!=-1) {
            formattedDate = formattedDate.substring(0,tIndex);
        }
        try {
            if ((formattedDate==null)||(formattedDate.trim().length()==0)) {
                formattedDate = DATE_FORMAT.format(theToday);
            }
            else {
                final Date date = theInputDateFormat.parse(formattedDate);
                formattedDate = DATE_FORMAT.format(date);
            }
        }
        catch (final Exception ex) {

        }
        theReleaseDates.put(release,formattedDate);
    }

    String getReleaseDate(String release) {
        return theReleaseDates.get(release);
    }

    List<String> getReleases() {
        return new ArrayList<String>(theReleaseDates.keySet());
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return theReleaseDates.toString();
    }

}
