package com.docfacto.ant.fogbugz;

import java.io.InputStream;
import java.net.URL;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.XMLUtils;

/**
 * Provides an interface to the FogBugz API.
 * <p>
 * To use this you must first call logon, then do what is required, then always
 * call logoff.
 * </p>
 * 
 */
public class FogBugzAPI {

    /**
     * The URL of the Fogbugz API service
     */
    private static final String URL_PREFIX =
        "https://docfacto.fogbugz.com/api.asp?";

    private static final String USERNAME = "dhudson@docfacto.com";
    private static final String PASSWORD = "ds10hd";

    /**
     * The logon token - set when login is called.
     */
    private String theToken;

    /**
     * Constructor.
     */
    public FogBugzAPI() {
    }

    /**
     * Logon to the Fogbugz API
     * 
     * @throws Exception if failed
     */
    public void logon() throws Exception {
        final String command =
            URL_PREFIX+"cmd=logon&email="+USERNAME+"&password="+PASSWORD;
        final Document reply = executeCommand(command);
        final Element root = reply.getDocumentElement();
        theToken = XMLUtils.getChildElementText(root,"token");
        if (theToken.length()>0) {
            System.out.println("Logged on to FogBugz with token "+theToken);
        }
        else {
            handleErrorReply(reply);
        }
    }

    /**
     * Logoff the current session
     * 
     * @throws Exception
     */
    public void logoff() throws Exception {
        final StringBuilder command = new StringBuilder(URL_PREFIX);
        command.append("cmd=logoff&token=");
        command.append(theToken);
        executeCommand(command.toString());
        theToken = null;
        System.out.println("Logged off FogBugz");
    }

    /**
     * Requests the release notes for a specified project and release.
     * <P>
     * 
     * @param project as known by FogBugz - e.g. "Diffusion"
     * @param release the release build as known by FogBugz - e.g. "2.1.0"
     * @return an object that encapsulates the release notes.
     * @throws Exception if this fails
     */
    public FogBugzReleaseNotes getReleaseNotes(String project,String release)
    throws Exception {
        // Create empty release notes object
        FogBugzReleaseNotes relNotes = new FogBugzReleaseNotes(project,release);

        // Get all release notes for specified release into an XML doc
        Document document = getReleaseNotesDoc(release);

        // For each case in the XML create a case object and add to the notes
        // object. Note that a case could be for the wrong project so the
        // notes object filters out any that are wrong
        Element response = document.getDocumentElement();
        Element casesRoot =
            (Element)response.getElementsByTagName("cases").item(0);
        NodeList caseList = casesRoot.getElementsByTagName("case");
        FogBugzCase fCase;
        for (int i = 0;i<caseList.getLength();i++) {
            fCase = new FogBugzCase((Element)caseList.item(i));
            relNotes.addCase(fCase);
        }
        return relNotes;
    }

    public FogBugzReleases getReleases(String project) throws Exception {
        // Create empty releases object
        final FogBugzReleases releases = new FogBugzReleases();

        // Get all release notes for specified release into an XML doc
        final Document document = getReleasesDoc();

        // For each case in the XML create a case object and add to the notes
        // object. Note that a case could be for the wrong project so the
        // notes object filters out any that are wrong
        final Element response = document.getDocumentElement();
        final Element releasesRoot =
            (Element)response.getElementsByTagName("fixfors").item(0);
        final NodeList releaseList =
            releasesRoot.getElementsByTagName("fixfor");

        Element releaseElement;
        String releaseProject;
        String releaseName;
        String releaseDate;
        for (int i = 0;i<releaseList.getLength();i++) {
            releaseElement = (Element)releaseList.item(i);
            releaseProject =
                XMLUtils.getChildElementText(releaseElement,"sProject");
            if (project==null||project.equals(releaseProject)) {
                releaseName =
                    XMLUtils.getChildElementText(releaseElement,"sFixFor");
                releaseDate =
                    XMLUtils.getChildElementText(releaseElement,"dt");
                releases.addReleaseDate(releaseName,releaseDate);
            }
        }
        return releases;
    }

    /**
     * Request release notes for a specified release.
     * <P>
     * Note that FogBugz only seems to be able to cater for a query on a release
     * and so if two projects has the same release number this would return them
     * all. The returned info therefore needs to be filtered afterwards to get
     * out only those for the project you are interested in. It will also give
     * you everything that starts with the release number so for 2.1.1 you would
     * also get 2.1.10 etc so that also needs to be filtered out after.
     * 
     * @param release build number - e.g. "2.1.0"
     * @return a document containing details of all release notes for cases with
     * a fixfor matching the build (could be from any project).
     * @throws Exception
     */
    private Document getReleaseNotesDoc(String release) throws Exception {
        // http://mx.pushtechnology.com:9090/api.asp?cmd=search&token=xxx&q=fixfor:2.1.0&cols=ixBug,sCategory,sTitle,sReleaseNotes
        final StringBuilder command = new StringBuilder(URL_PREFIX);
        command.append("cmd=search&token=").append(theToken);
        command.append("&q=fixfor:"); // Can't specify project as well
        command.append(release);
        command
            .append(
            "&cols=ixBug,sCategory,sArea,sTitle,sReleaseNotes,sProject,sStatus,sFixFor");
        return executeCommand(command.toString());
    }

    private Document getReleasesDoc() throws Exception {
        final StringBuilder command = new StringBuilder(URL_PREFIX);
        command.append("cmd=listFixFors&token=").append(theToken);
        command.append("&fIncludeDeleted=1");
        return executeCommand(command.toString());
    }

    /**
     * Executes a command by sending it to the FogBugz API service and parsing
     * the reply into an XML document.
     * 
     * @param command a fully formatted command url string
     * @return a document containing the reply
     * @throws Exception
     */
    private Document executeCommand(String command) throws Exception {
        final URL url = new URL(command);
        final InputStream inputStream = url.openStream();
        final Document document = XMLUtils.createDocument(inputStream);
        inputStream.close();
        return document;
    }

    /**
     * Handle an error
     * 
     * @param reply
     */
    private void handleErrorReply(Document reply) {
        try {
            final String replyString = XMLUtils.prettyFormat(reply);
            System.out.println("ERROR");
            System.out.println(replyString);
        }
        catch (final DocfactoException ex) {
            ex.printStackTrace();
        }
    }
}
