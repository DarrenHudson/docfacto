package com.docfacto.ant.fogbugz;

import java.io.File;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

/**
 * Ant task for generating a release notice.
 * 
 */
public class GenerateReleaseNotice extends Task {

    private String theBuild;
    private String theFileName;
    private String theBaseDir;
    private String theProject;
    
    /**
     * Sets build.
     * 
     * @param build the build to set
     */
    public void setBuild(String build) {
        theBuild = build;
    }

    /**
     * Sets fileName.
     * 
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        theFileName = fileName;
    }

    public void setBaseDir(String baseDir) {
        theBaseDir = baseDir;
    }

    public void setProject(String project) {
        theProject = project;
    }
    
    /**
     * @see org.apache.tools.ant.Task#execute()
     */
    @Override
    public void execute() throws BuildException {
        final ReleaseNoticeGenerator generator = new ReleaseNoticeGenerator();
        try {
            final File relNotice = new File(theBaseDir, theFileName);

            generator.generate(relNotice,theProject,theBuild);
        } catch (final Throwable ex) {
            ex.printStackTrace();
            throw new BuildException("Release Notice Generate failed", ex);
        }
    }

}
