package com.docfacto.ant.fogbugz;

import java.io.File;
import java.io.FileWriter;
import java.util.Set;

import com.docfacto.common.StringUtils;

/**
 * Generates a release notice from Fogbugz
 * 
 * {@docfacto.media uri="doc-files/FagButzApi.svg" title="System Doc"}
 * 
 * @author pwalsh
 * 
 */
public class ReleaseNoticeGenerator {

    /**
     * A reference to a FogBugz API instance
     */
    private FogBugzAPI theAPI;

    /**
     * Constructor.
     */
    public ReleaseNoticeGenerator() {
    }

    /**
     * Generate a release notice for a specified build.
     * <P>
     * The release notice will have notes for all builds up to and including the
     * specified build. For example 2.1.3 would include notes for 2.1.0, 2.1.1,
     * 2.1.2 and 2.1.3.
     * 
     * @param file the file to write to. If the file exists it must be writable
     * and will be overwritten otherwise a new file will be created.
     * @param project name or null for all projects
     * @param build the build number to generate for. Must be of the form
     * major.minor.build
     * @throws Exception if failed to generate
     */
    public void generate(File file,String project,String build)
    throws Exception {
        if (file.exists()) {
            if (!file.canWrite()) {
                throw new Exception("Unable to write to "+file);
            }
        }
        theAPI = new FogBugzAPI();
        theAPI.logon();
        try {
            writeReleaseNotice(file,project,build);
        }
        finally {
            theAPI.logoff();
        }
    }

    /**
     * Write a release notice for a specified build (a.b.c).
     * <P>
     * Writes notes for all builds where build number is 0 to c.
     * 
     * @param file the file to write to
     * @param project the project name as it is known in FogBugz
     * @param build the full build to generate for (a.b.c)
     * @throws Exception if it fails
     */
    public void writeReleaseNotice(
    File file,
    String project,
    String build)
    throws Exception {

        final int dotIndex = build.lastIndexOf('.');
        final String release = build.substring(0,dotIndex);

        // The build part is made up of maint number and an optional build
        // suffix - we are only interested in the maint number.
        String maintNumber = build.substring(dotIndex+1);
        final int scoreIndex = maintNumber.indexOf('_');
        if (scoreIndex!=-1) {
            maintNumber = maintNumber.substring(0,scoreIndex);
        }

        final int finalBuildNumber = Integer.parseInt(maintNumber);

        final FileWriter writer = new FileWriter(file);

        final String title = "Release Notes - : for release "+release;
        writer.write("<html>");
        writer.write("<head>");
        writer.write("<title>"+title+"</title>");
        writeStyle(writer);
        writer.write("</head>");
        writer.write("<body>");
        writer
            .write("<img src=\"http://www.docfacto.com/images/docfacto.png\" />");
        writer.write("<h1>"+title+"</h1>");

        // TODO - could do with some introduction here

        FogBugzReleases releases = theAPI.getReleases(project);

        FogBugzReleaseNotes relNotes;
        for (int buildNumber = 0;buildNumber<=finalBuildNumber;buildNumber++) {
            relNotes = theAPI.getReleaseNotes(project,release+"."+buildNumber);
            relNotes.setBuildDate(
                releases.getReleaseDate(relNotes.getRelease()));
            writeBuildNotes(relNotes,writer);
        }
        writer.write("</body>");
        writer.write("</html>");
        writer.close();

    }

    /**
     * Write style sheet
     * 
     * @param writer
     * @throws Exception
     */
    private void writeStyle(FileWriter writer) throws Exception {
        writer.write("<style type=\"text/css\">");
        writer.write("<!--");
        writer.write("body { font-family: sans-serif }");
        writer.write("th, h3 { color: rgb(28,118,188) }");
        writer.write("h2 { color: black; font-weight: bold }");
        writer.write("-->");
        writer.write("</style>");
    }

    /**
     * Write notes for a given build (n.n.n)
     * 
     * @param relNotes the build notes
     * @param writer
     * @throws Exception
     */
    private void writeBuildNotes(
    FogBugzReleaseNotes relNotes,
    FileWriter writer)
    throws Exception {
        System.out.println(
            "Generating Release Notes for "+relNotes.getRelease());
        if (relNotes.hasBugs()) {
            writer.write(
                "<h2>Bugs Cleared at Build "+
                    relNotes.getRelease()+
                    " ("+relNotes.getBuildDate()+")"+
                    "</h2>");
            writeReleaseNoticeCases(relNotes.getBugs(),writer);
        }
        if (relNotes.hasFeatures()) {
            writer.write(
                "<h2>New Features at Build "+
                    relNotes.getRelease()+
                    " ("+relNotes.getBuildDate()+")"+
                    "</h2>");
            writeReleaseNoticeCases(relNotes.getFeatures(),writer);
        }
    }

    /**
     * Write notes for a set of cases
     * 
     * @param caseSet
     * @param writer
     * @throws Exception
     */
    private void writeReleaseNoticeCases(
    Set<FogBugzCase> caseSet,FileWriter writer) throws Exception {
        for (final FogBugzCase aCase:caseSet) {
            System.out.println(aCase);
            writer.write(
                "<h3>"+
                    aCase.getCaseNumber()+
                    ": "+
                    aCase.getProject()+
                    ": "+
                    aCase.getArea()+
                    ": "+
                    StringUtils.escapeXML(aCase.getTitle())+
                    "</h3>");
            writer.write(
                "<p>"+
                    StringUtils.escapeXML(aCase.getReleaseNotes())+
                    "</p>");
        }
    }

}
