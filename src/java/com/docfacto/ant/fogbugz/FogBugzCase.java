package com.docfacto.ant.fogbugz;

import org.w3c.dom.Element;

import com.docfacto.common.XMLUtils;

/**
 * Encapsulates details of a FogBugz case
 * 
 */
public class FogBugzCase implements Comparable<FogBugzCase> {

    /**
     * The case number
     */
    private final int theCaseNumber;

    /**
     * The case area
     */
    private final String theArea;

    /**
     * The case title
     */
    private final String theTitle;
    /**
     * The case category, e.g. "Bug", "Feature"
     */
    private final String theCategory;

    /**
     * The project name
     */
    private final String theProject;
    /**
     * The release notes - will be null if there were none
     */
    private String theReleaseNotes;

    /**
     * The case status, e.g. "Resolved (Fixed"). This is here for completeness
     * but is not really used.
     */
    private final String theStatus;

    /**
     * The release
     */
    private final String theRelease;

    /**
     * Constructor. Parses the details from a specified DOM element which is
     * expected to be a 'case' taged element.
     * 
     * @param element
     */
    public FogBugzCase(Element element) {

        theCaseNumber =
        Integer.parseInt(XMLUtils.getChildElementText(element,"ixBug"));

        theArea = XMLUtils.getChildElementText(element,"sArea");

        theTitle = XMLUtils.getChildElementText(element,"sTitle");

        theCategory = XMLUtils.getChildElementText(element,"sCategory");

        theProject = XMLUtils.getChildElementText(element,"sProject");

        theStatus = XMLUtils.getChildElementText(element,"sStatus");

        theRelease = XMLUtils.getChildElementText(element,"sFixFor");

        theReleaseNotes = XMLUtils.getChildElementText(element,"sReleaseNotes");
        theReleaseNotes = theReleaseNotes.trim();
        if (theReleaseNotes.length()==0) {
            theReleaseNotes = null;
        }
        else if (theReleaseNotes.toUpperCase().startsWith("NONE")) {
            theReleaseNotes = null;
        }
    }

    /**
     * Returns caseNumber.
     * 
     * @return the caseNumber
     */
    public int getCaseNumber() {
        return theCaseNumber;
    }

    /**
     * Returns area
     * 
     * @return
     */
    public String getArea() {
        return theArea;
    }

    /**
     * Returns title.
     * 
     * @return the title
     */
    public String getTitle() {
        return theTitle;
    }

    /**
     * Returns category.
     * 
     * @return the category
     */
    public String getCategory() {
        return theCategory;
    }

    public boolean isBug() {
        return "Bug".equals(theCategory);
    }

    public boolean isFeature() {
        return "Feature".equals(theCategory);
    }

    /**
     * Returns project.
     * 
     * @return the project
     */
    public String getProject() {
        return theProject;
    }

    public String getRelease() {
        return theRelease;
    }

    /**
     * Returns releaseNotes.
     * 
     * @return the releaseNotes
     */
    public String getReleaseNotes() {
        return theReleaseNotes;
    }

    public boolean hasReleaseNotes() {
        return (theReleaseNotes!=null);
    }

    /**
     * Returns status.
     * 
     * @return the status
     */
    public String getStatus() {
        return theStatus;
    }

    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(FogBugzCase o) {
        final int myCase = getCaseNumber();
        final int theirCase = o.getCaseNumber();
        if (myCase==theirCase) {
            return 0;
        }
        if (myCase<theirCase) {
            return -1;
        }
        return 1;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("(").append(theCategory).append(") ").
        append(theCaseNumber).append(" : ").
        append(theArea).append(" : ").
        append(theTitle);
        return sb.toString();
    }

}
