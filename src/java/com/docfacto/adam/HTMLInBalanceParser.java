package com.docfacto.adam;

import java.util.HashMap;
import java.util.Map.Entry;

import com.docfacto.exceptions.DocfactoParsingException;
import com.docfacto.xml.DefaultParserHandler;
import com.docfacto.xml.GenericDynamicElement;
import com.docfacto.xml.XMLFastParser;

/**
 * Check for in-balance of HTML tags within the comment
 * 
 * {@docfacto.note As of Java 1.8, this will be a requirement }
 * 
 * @author dhudson - created 10 Jun 2013
 * @since 2.4
 */
public class HTMLInBalanceParser extends DefaultParserHandler {

    private HashMap<String,Integer> theTags;

    private String theErrorTag;

    /**
     * Constructor.
     * @since 2.4
     */
    public HTMLInBalanceParser() {
        theTags = new HashMap<String,Integer>(5);
    }

    /**
     * Check the comment for HTML in-balance
     * 
     * @param comment to process
     * @return true if there is HTML in-balance
     * @throws DocfactoParsingException if unable to parse
     * @since 2.4
     */
    public boolean parse(String comment) throws DocfactoParsingException {

        XMLFastParser parser = new XMLFastParser(comment);
        parser.parse(this);

        for (Entry<String,Integer> entry:theTags.entrySet()) {
            if (entry.getValue()!=0) {
                // Can't be balanced
                theErrorTag = entry.getKey();
                return true;
            }
        }

        return false;
    }

    /**
     * @see com.docfacto.xml.DefaultParserHandler#processEndTag(java.lang.String)
     */
    @Override
    public void processEndTag(String endTag) {
        if (theTags.containsKey(endTag)) {
            int num = theTags.get(endTag);
            num--;
            theTags.put(endTag,num);
        }
        else {
            // First time I have seen this and there is no start tag
            // could probably just bomb out here.
            theTags.put(endTag,new Integer(-1));
        }
    }

    /**
     * @see com.docfacto.xml.DefaultParserHandler#processStartTag(com.docfacto.xml.GenericDynamicElement)
     */
    @Override
    public void processStartTag(GenericDynamicElement gdl) {
        if (gdl.isEmptyElement()) {
            // Nothing to do here ..
            return;
        }

        if (theTags.containsKey(gdl.getElementName())) {
            // Add one to the tags to handle nested tags
            int num = theTags.get(gdl.getElementName());
            num++;
            theTags.put(gdl.getElementName(),num);
        }
        else {
            theTags.put(gdl.getElementName(),new Integer(1));
        }
    }

    /**
     * Return the tag name in error
     * 
     * @return the tag name if found
     * @since 2.4
     */
    public String getErrorTag() {
        return theErrorTag;
    }
}
