/*
 * @author dhudson -
 * Created 14 Mar 2013 : 08:59:56
 */

package com.docfacto.adam;

import java.util.List;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.StringUtils;
import com.docfacto.output.AbstractOutputProcessor;
import com.docfacto.output.generated.Result;
import com.docfacto.output.generated.Rule;
import com.docfacto.output.generated.Statistic;

/**
 * Class to marshal the input / output of the output from Adam
 * 
 * @author dhudson - created 14 Mar 2013
 * @since 2.2
 * 
 * @docfacto.link uri="${html-root}/adam/c_adam_output.html" version="1.1"
 * key="AdamOutput" link-type="link-to-doc" target-file-type="xml"
 */
public class AdamOutputProcessor extends AbstractOutputProcessor {

    private static final String RULES_XML_LOCATION =
        "com/docfacto/adam/resources/AdamRules.xml";

    private static final String INTERNAL_ERROR_KEY = "internal-error";

    /**
     * Create a new instance of <code>AdamOutputLoader</code>.
     * 
     * Override the BaseXmlConfig functionality, just to create an empty
     * {@code AdamOutput}
     * 
     * @throws DocfactoException not in this case
     * @since 2.2
     */
    public AdamOutputProcessor() throws DocfactoException {
    }

    /**
     * Prepare the Output for a single error
     * 
     * @param message of the internal error
     * @param ex exception if present
     * @since 2.4
     */
    public void generateInternalError(String message,Exception ex) {

        Result result = new Result();
        Statistic stat = new Statistic();

        result.setSeverity("error");

        Rule rule = new Rule();
        rule.setKey(INTERNAL_ERROR_KEY);
        rule.setName(INTERNAL_ERROR_KEY);
        rule.setMessage(message);

        if (ex!=null) {
            String fullException = DocfactoException
                .getFullExceptionMessage(ex,true);
            rule.setDescription(StringUtils.escapeXML(fullException));
        }

        result.setRule(rule);
        stat.setRule(rule);

        // Add the error to the result as well for completeness
        addResult(result);

        List<Statistic> stats = this.getStatistics();

        // Clear all of the loaded stats from the XML
        stats.clear();
        // Just add the error stat
        stats.add(stat);
    }

    /**
     * Create a new instance of <code>AdamResultsLoader</code>.
     * 
     * @param configFilePath to load
     * @throws DocfactoException if unable to load or unmarshal
     * @since 2.2
     */
    public AdamOutputProcessor(String configFilePath) throws DocfactoException {
        super(configFilePath);
    }

    /**
     * @see com.docfacto.config.BaseXmlConfig#getDefaultFileName()
     */
    @Override
    public String getDefaultFileName() {
        return RULES_XML_LOCATION;
    }

}
