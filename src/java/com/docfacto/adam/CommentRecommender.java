package com.docfacto.adam;

import com.docfacto.common.Platform;
import com.docfacto.common.StringUtils;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.Doc;
import com.sun.javadoc.ExecutableMemberDoc;
import com.sun.javadoc.FieldDoc;
import com.sun.javadoc.MethodDoc;
import com.sun.javadoc.Parameter;
import com.sun.javadoc.Type;

/**
 * Class to recommend comments.
 * 
 * @author dhudson - created 2 Oct 2013
 * @since 2.0
 */
public class CommentRecommender {

    /** The clause used to start generated comments for constructors */
    protected static String cnr_clause = "Constructs";
    /** The clause used for object-values in constructors */
    protected static String obj_clause = "object";
    /** The clause used to start generated comments for getFoo() methods */
    protected static String get_clause = "Returns the";
    /** The clause used to start generated comments for setFoo() methods */
    protected static String set_clause = "Specifies the";
    /** The clause used to start generated comments for isFoo() methods */
    protected static String is_clause = "Returns true if this object is";
    /** The clause used to end generated comments for non-boolean values */
    protected static String val_clause = "value";
    /** The clause used to start generated comments for boolean setFoo() methods */
    protected static String bSet_clause = "Identifies this object as";
    /** The clause used in generated comments for boolean values */
    protected static String bVal_clause = "";
    /** The clause used for generated interface comments. */
    protected static String ifc_clause =
        "Defines the requirements for an object that ...";

    
    public String processField(FieldDoc doc) {

        // ToDo List of instance prefixs
        String name = doc.name();
        if(name.startsWith("the")) {
            name = name.substring(3);
        } else if(name.startsWith("f") && Character.isUpperCase(name.charAt(1))) {
            name = name.substring(1);
        }
        
        String phrase = parse(name);

        StringBuilder builder = new StringBuilder(80);
        if(doc.isStatic() && doc.isFinal()) {
            builder.append("A constant ");
            builder.append(phrase);
            builder.append(" {@value}");
        } else {
            builder.append("The ");
            builder.append(phrase);
        }
        
        return builder.toString();
    }
    
    public String recommend(Doc doc) {
        if (doc instanceof MethodDoc) {
            return processComment((MethodDoc)doc);
        }

        return null;
    }

    public String processComment(MethodDoc methodDoc) {
        final Type returnType = methodDoc.returnType();

        //
        // final Tag[] tags = methodDoc.tags(AdamProcessor.RETURN_TAG);

        StringBuilder builder = new StringBuilder(100);
        builder.append("/** ");
        builder.append(Platform.LINE_SEPARATOR);
        builder.append(" * ");
        builder.append(firstLine(methodDoc));
        builder.append(" * ");
        builder.append(Platform.LINE_SEPARATOR);

        if(methodDoc.parameters().length>0) {
            // We have parameters, so lets add them
            builder.append(addParams(methodDoc));
        }
        if (!"void".equals(returnType.simpleTypeName())) {
            builder.append(" * ");
            builder.append(AdamConstants.RETURN_TAG);
            builder.append(" ");
            builder.append(returnPhrase(methodDoc,returnType));
            builder.append(Platform.LINE_SEPARATOR);
        }

        builder.append(" */");

        return builder.toString();
    }

    private String addParams(ExecutableMemberDoc exeMemberDoc) {
        StringBuilder builder = new StringBuilder(100);
        
        for(Parameter param : exeMemberDoc.parameters()) {
            builder.append(" * @param ");
            builder.append(param.name());
            builder.append(" ");
            builder.append(paramPhrase(exeMemberDoc,param.type()));
            builder.append(Platform.LINE_SEPARATOR);
        }
     
        return builder.toString();
    }
    
    /**
     * Adds the first line for the method comment. Identifies an overridden
     * method, if there is one. For "getFoo" adds "Returns the foo value." For
     * "setFoo", adds "Sets the foo value." For "isFoo", adds "Returns the foo
     * value". (These are the default phrases, which can be controlled using
     * command-line options.) See DocCheck.html for details.
     * 
     * @param d a MethodDoc object
     */
    public String firstLine(MethodDoc d)
    {

        StringBuilder builder = new StringBuilder(100);

        ClassDoc od = d.overriddenClass();
        if (od!=null) {
            if (d.name().equals("toString")&&d.parameters().length==0) {
                builder
                    .append(" * Returns a string that displays and identifies this");
                builder.append(Platform.LINE_SEPARATOR);
                builder.append(" * object's properties.");
                builder.append(Platform.LINE_SEPARATOR);
                builder.append(" *");
                builder.append(Platform.LINE_SEPARATOR);
                builder
                    .append(" * @return a String representation of this object");
                builder.append(Platform.LINE_SEPARATOR);
                return builder.toString();
            }

            builder.append(overrideMessage(d));
        }

        String mName = d.name();
        String pName = propertyName(d);
        String startClause = "";
        String finalClause = "";
        if (pName==null||pName.equals("")) {
            builder.append(StringUtils.capitaliseFirstLetter(parse(d.name())));
            builder.append(".");
        }
        else {
            if (pName.endsWith("color")
                ||pName.endsWith("font")
                ||pName.endsWith("location")
                ||pName.endsWith("name")
                ||pName.endsWith("position")
                ||pName.endsWith("size")
                ||pName.endsWith("text")
                ||pName.endsWith("value"))
            {
                startClause = "this "+obj_clause+"'s";
                finalClause = ".";
            }
            if (mName.startsWith("is")) {
                startClause = is_clause;
                finalClause = " "+bVal_clause+".";
                if (bVal_clause.equals(""))
                    finalClause = ".";
                builder.append(startClause);
                builder.append(" ");
                builder.append(pName);
                builder.append(finalClause);
            }
            else if (mName.startsWith("get")) {

                if (startClause!="")
                    startClause = "Gets "+startClause;
                else {
                    startClause = get_clause;
                    finalClause = " "+val_clause+".";
                    if (val_clause.equals("")) {
                        finalClause = ".";
                    }
                }
                if (pName.equals("enabled")
                    ||pName.equals("visible"))
                {
                    startClause = "Returns true if this "+obj_clause+" is ";
                    finalClause = ".";
                }

                builder.append(startClause);
                builder.append(" ");
                builder.append(pName);
                builder.append(finalClause);
            }
            else if (mName.startsWith("set")) {
                if (d.flatSignature().substring(1).startsWith("boolean")) {
                    if (pName.equals("enabled")) {
                        builder.append("Enables or disables this ");
                        builder.append(obj_clause);
                        builder.append(".");
                        return builder.toString();
                    }
                    if (pName.equals("visible")) {
                        builder.append("Hides or displays this ");
                        builder.append(obj_clause);
                        builder.append(".");
                        return builder.toString();
                    }
                    startClause = bSet_clause;
                    finalClause = " "+bVal_clause+".";
                    if (bVal_clause.equals("")) {
                        finalClause = ".";
                    }
                    builder.append(startClause);
                    builder.append(" ");
                    builder.append(pName);
                    builder.append(finalClause);
                }
                else {
                    // Method takes no argument, or at least not a boolean one
                    if (pName.equals("enabled")) {
                        builder.append("Enables this ");
                        builder.append(obj_clause);
                        builder.append(".");
                        return builder.toString();
                    }
                    if (pName.equals("visible")) {
                        builder.append("Makes this ");
                        builder.append(obj_clause);
                        builder.append(" visible.");
                        return builder.toString();
                    }
                    if (startClause!="") {
                        startClause = "Sets "+startClause;
                    }
                    else {
                        startClause = set_clause;
                        finalClause = " "+val_clause+".";
                        if (val_clause.equals(""))
                            finalClause = ".";
                    }
                    builder.append(startClause);
                    builder.append(" ");
                    builder.append(pName);
                    builder.append(finalClause);
                }
            }

        }
        // Tag[] tags = d.tags();
        // if (!lineWritten&&tags.length>0) {
        // // NOTE: tag.name() returns "@see".
        // // But to get those tags, you use tags("see").
        // if (tags[0].name().equals("@see")) {
        // if (check)
        // return true;
        // String target = tags[0].text().replace('#','.'); // Change
        // // #-separator
        // // to dot
        // et.formLine(level," * Delegates to <code>"+target+"</code>.");
        // lineWritten = true;
        // }
        // if (tags[0].name().equals("@return")) {
        // if (check)
        // return true;
        // et.formLine(level," * Returns "+tags[0].text());
        // lineWritten = true;
        // }
        // }
        // if (check)
        // return false;
        // if (!lineWritten)
        // et.formLine(level," *");
        // return lineWritten;
        builder.append(Platform.LINE_SEPARATOR);
        return builder.toString();

    } // firstLine()

    /**
     * Adds a line identifying the overridden method
     * 
     * @param md a MethodDoc object
     */
    public String overrideMessage(MethodDoc md) {
        StringBuilder builder = new StringBuilder(100);

        ClassDoc od = md.overriddenClass();
        // TODO: When md.overriddenMethod() is available, use it to find out
        // if the method is abstract. (We could do it now, but we would have
        // to get all the methods in the class and loop to match signatures.)
        MethodDoc om = null;
        try {
            om = md.overriddenMethod();
        }
        catch (NoSuchMethodError e) {
            // Method "overriddenMethod()" is not supported by older versions of
            // the Doclet API. We have to search for the overridden method with
            // our
            // own utility method.
            // om = findMethod(od, md);
        }
        if (om!=null&&om.isAbstract()) {
            builder.append(" * Implements abstract method <code>");
            builder.append(name(od));
            builder.append(".");
            builder.append(md.name());
            builder.append("</code>.");
        }
        else {
            // Not an abstract method
            builder.append(" * Overrides <code>");
            builder.append(name(od));
            builder.append(".");
            builder.append(md.name());
            builder.append("</code>.");
        }

        return builder.toString();
    }

    /**
     * Return a string containing the non-qualified class name that uses "$" to
     * separate inner classes from outer classes instead of ".".
     * 
     * @return a String containing the non-qualified name
     */
    private String name(ClassDoc cd) {
        String s = cd.name();
        return (s.replace('.','$'));
    }

    /**
     * Returns an article (a or an) plus the string, depending on its initial
     * letter and whether or not the type is an array.
     * 
     * @param type a String identifying a type
     * @return the String containing the phrase to use for that type
     */
    private String returnPhrase(MethodDoc d,Type type)
    {
        String phrase = "";
        String sType = simpleName(type);

        // Check for isFoo() / getFoo() methods
        int argCount = d.parameters().length;
        String propName = propertyName(d);
        if (propName!=null) {
            if (d.name().startsWith("get")&&argCount==0)
            {
                if (type.equals("boolean")) {
                    // Special property name.
                    phrase = "true if this object is "+propName;
                    return phrase;
                }
                if (propName.equals("color")
                    ||propName.equals("text")
                    ||propName.equals("font")
                    ||propName.equals("size")
                    ||propName.equals("value")
                    ||propName.equals("location")
                    ||propName.equals("position"))
                {
                    // Special property name.
                    phrase = "this "+obj_clause+"'s current "+propName;
                    return phrase;
                }
                // Compare lowercase version of names, since propName has an
                // initial lowercase letter
                if (sType.toLowerCase().equals(propName.toLowerCase())) {
                    // Property name is the same as the object.
                    phrase = "the "+sType+" value";
                }
                else {
                    // Some other type represents the property
                    phrase = typePhrase(type);
                    phrase += " representing the "+propName+" value";
                }
                return phrase;
            }
            if (d.name().startsWith("is")
                &&argCount==0
                &&type.equals("boolean")) {
                phrase = "true if this "+obj_clause+" is "+propName;
                return phrase;
            }
        }

        if (type.equals("boolean")) {
            phrase = "true if ...";
        }
        else if (type.equals("int")) {
            phrase = "an int representing the ... value";
        }
        else if (sType.equals("String")) {
            phrase = "a String ...";
        }
        else {
            phrase = typePhrase(type);
        }
        return phrase;
    } // returnPhrase

    /**
     * Returns a plural phrase if the argument is an array, else returns a
     * singular phrase.
     * 
     * @param type a String containing a data type
     * @return a String containing the appropriate phrase for that type
     */
    private String typePhrase(Type type)
    {
        Type[] vars = null;

        if (type.asClassDoc() instanceof ExecutableMemberDoc) {
            vars = ((ExecutableMemberDoc)type.asClassDoc()).typeParameters();
        }
        else if (type.asParameterizedType()!=null) {
            vars = type.asParameterizedType().typeArguments();
        }

        String phrase = "";
        String sType = simpleName(type);
        String simpleType = type.toString();

        if (simpleType.endsWith("]")) {
            phrase = "an array of "+sType;
            if (simpleType.charAt(simpleType.length()-3)==']')
                phrase += " array";
            else if (initUpper(sType)) {
                if (!sType.equals("String")
                    &&!sType.equals("Object"))
                    phrase += " "+obj_clause;
            }
            phrase += "s";
        }
        else {
            if (sType.equals("List")) {
                phrase = "a list of "+vars[0];
            }
            if (sType.equals("String")) {
                phrase = "a String";
            }
            else if (sType.equals("Object")) {
                phrase = "an Object";
            }
            else {
                phrase = article(sType)+sType;
                if (initUpper(sType)) {
                    phrase += " "+obj_clause;
                }
            }
        }
        return phrase;
    } // typePhrase

    /**
     * Returns the proper article (a or an) to go along with a word, depending
     * on its first letter, plus a space.
     */
    private String article(String s) {
        char first = s.charAt(0);
        if (first=='U') {
            // Special case. An all caps entry like "UIDefaults" is
            // pronounced "a UIDefaults object". This is unusual for U.
            // All other pronounced vowels would use "an" -- "an ABC", etc.
            if (s.length()>1) {
                char next = s.charAt(1);
                if (next>='A'&&next<='Z')
                    return "a ";
            }
        }
        first = s.toLowerCase().charAt(0);
        if (first=='a'||first=='e'||first=='i'
            ||first=='o'||first=='u') {
            return "an ";
        }
        return "a ";
    }

    /**
     * Remove the package prefix and array subscripts from a name.
     * 
     * @param s a String containing a fully qualified name
     * @return the String containing the name without the package qualifier or
     * array subscripts
     */
    private String simpleName(Type type) {
        
        String s = type.toString();
        
        if(type.asParameterizedType() != null) {
            if(s.indexOf("<")>0) {
                s = s.substring(0,s.indexOf("<"));
            }
        }
        
        // Remove package-prefix
        int lastPeriod = s.lastIndexOf('.');
        if (lastPeriod>0) {
            s = s.substring(lastPeriod+1);
        }

        // Strip array brackets from name
        int idx = s.indexOf('[');
        if (idx>0) {
            s = s.substring(0,idx);
        }
        return s;
    }

    /**
     * Returns the name of the property for methods with names like isFoo(),
     * getFoo(), and setFoo(). Returns the property name with a lowercase
     * initial letter.
     * 
     * @param d a MethodDoc object
     * @return a String containing the property name, or null
     */
    private String propertyName(MethodDoc d) {
        String name = d.name();
        // Check for isXx()
        if (name.length()<4) {
            return null;
        }

        if (name.startsWith("is")) {
            if (!Character.isUpperCase(name.charAt(2))) {
                return null;
            }
            return parse(name.substring(2));
        }

        // Check for getXx and setXx
        if (name.length()<5) {
            return null;
        }
        if (name.startsWith("get")||name.startsWith("set")) {
            if (!Character.isUpperCase(name.charAt(3))) {
                return null;
            }
            return parse(name.substring(3));
        }
        return null;
    }

    /**
     * Breaks a string into component names. For a string like ReallyFoo,
     * returns "really foo". Does not attempt to break up strings of capital
     * letters, so AnABC() returns "an ABC" and "VeryGoodABs()" returns
     * "very good ABs". This processing will therefore break up "ALLgoodThings"
     * as "ALLgood things". Similarly, "ALLGoodThings" is returned as
     * "ALLGood things". Since there is no universal standard about how to
     * capitalize the word following a string of capitals, this parsing is the
     * best that can be achieved.
     * 
     * @param s an intercap string to break up
     * @return a String with initial lowercase names and spaces between them
     */
    private String parse(String s)
    {
        // Start with init lowercase string (if 2nd letter isn't capped).
        StringBuffer sb = new StringBuffer(uncap(s));
        final int NONE = 0;
        final int NUM = 1;
        final int LOWER = 2;
        final int UPPER = 3;
        int lastChar = NONE;
        int index = 0;
        INSPECTION: while (index<sb.length()) {
            char letter = sb.charAt(index);
            if (letter=='_') {
                if (index==0)
                    sb.deleteCharAt(index);
                else {
                    // Replace underscore with space
                    sb.setCharAt(index,' ');
                    index++;
                    if (index<sb.length())
                        sb.replace(index,sb.length(),uncap(sb.substring(index)));
                }
                continue INSPECTION;
            }
            if (lastChar==LOWER||lastChar==NUM) {
                // At an uppercase letter following a string of lowercase
                // letters
                // or digits, or at a lowercase character following a string of
                // digits.
                // Add a space and increment past it. Then replace the tail of
                // the
                // string with a properly capitalized version and continue.
                // (We don't break small letters after capitals in order to keep
                // capitlized words together. And we don't break numbers after
                // capitals,
                // on the assumption that they are part of the name.)
                // Here's the decision table:
                // Lower --> upper : break
                // lower --> number: break
                // number --> lower : break
                // number --> upper : break
                // upper --> lower : no break
                // upper --> number: no break
                sb.insert(index,' ');
                index++;
                sb.replace(index,sb.length(),uncap(sb.substring(index)));
            }
            if (letter>='0'&&letter<='9') {
                // Move past a string of numbers
                lastChar = NUM;
                while (letter>='0'&&letter<='9') {
                    if (++index>=sb.length())
                        break INSPECTION;
                    letter = sb.charAt(index);
                }
                continue INSPECTION;
            }
            if (letter>='A'&&letter<='Z') {
                // Move past a string of capital letters
                lastChar = UPPER;
                while (letter>='A'&&letter<='Z') {
                    if (++index>=sb.length())
                        break INSPECTION;
                    letter = sb.charAt(index);
                }
                continue INSPECTION;
            }
            if (letter>='a'&&letter<='z') {
                // Move past a string of lowercase letters
                lastChar = LOWER;
                while (letter>='a'&&letter<='z') {
                    if (++index>=sb.length())
                        break INSPECTION;
                    letter = sb.charAt(index);
                }
                continue INSPECTION;
            }
        }// INSPECTION
        return sb.toString();
    }// parse

    /**
     * Returns a properly capitalized name. Name is assumed to be at least 2
     * characters long. Initial letter is assumed to be upper case. It is
     * converted to lower case if the second letter is also lower case.
     * 
     * @param name a String containing a name
     * @return the String with the first letter capitalized properly for a
     * property name
     */
    private String uncap(String name) {
        if (name.length()<1)
            return "";
        if (name.length()==1)
            return name.substring(0,1).toLowerCase();
        char firstLetter = name.charAt(0);
        char secondLetter = name.charAt(1);
        if ((firstLetter>='0')&&(firstLetter<='9'))
            return name;
        if ((firstLetter>='a')&&(firstLetter<='z'))
            return name;
        // First letter is a capital. Check second.
        if ((secondLetter>='A')&&(secondLetter<='Z'))
            return name;
        if ((secondLetter>='0')&&(secondLetter<='9'))
            return name;
        // First letter is capitalized, and second isn't. Lowercase the first
        // letter.
        return name.substring(0,1).toLowerCase()+name.substring(1);
    }

    /**
     * Returns an article (a or an) plus the string, depending on
     * its initial letter (in the general case) or a somewhat more
     * specialized version for booleans, ints, and arrays.
     *
     * @param type  a String identifying a type
     * @return the String containing the phrase to use for that type
     */
    public String paramPhrase(ExecutableMemberDoc emd, Type type)
    {
        String phrase = "";
        String sType = simpleName(type);
        
        // Check for setFoo() method
        if (emd instanceof MethodDoc) {
            MethodDoc md = (MethodDoc) emd;
            int argCount = md.parameters().length;
            String propName = propertyName(md);
            if (propName != null) {
                if (md.name().startsWith("set") && argCount == 1 && type.equals("boolean")) {
                    phrase = "true to make this " + obj_clause + " " + propName;
                    return phrase;
                }
                phrase =typePhrase(type);
                if (md.name().startsWith("set") && argCount == 1) {
                    // Compare lowercase version of names, since propName has an
                    // initial lowercase letter
                    if (propName.equals("color")
                        ||  propName.equals("text")
                        ||  propName.equals("font")
                        ||  propName.equals("size")
                        ||  propName.equals("value")
                        ||  propName.equals("location")
                        ||  propName.equals("position") )
                    {
                        // Special property name.
                        phrase = "the new " + propName + " for this object";
                        return phrase;
                    }
                    if (propName.equals("enabled")
                        ||  propName.equals("visible")) {
                        // Special property name.
                        phrase = "true to make this object " + propName;
                        return phrase;
                    }
                    if (sType.toLowerCase().equals(propName.toLowerCase())) {
                        // Property name is the same as the object.
                        phrase = "the " + sType + " value";
                        return phrase;
                    } else {
                        // Some other type represents the property
                        phrase += " specifying the " + propName + " value";
                        return phrase;
                    }
                }
                return phrase;
            }
        }
        
        // Construct phrase for other methods
        if (type.equals("boolean")) {
            phrase = "a boolean -- true if ...";
        } else if (type.equals("int")) {
            phrase = "an int specifying ...";
        } else if (sType.equals("String")) {
            phrase = "a String ...";
        } else if (sType.equals("Object")) {
            phrase = "an Object ...";
        } else {
            phrase = typePhrase(type);
        }
        return phrase;
    } //paramPhrase
    
    /**
     * Returns true if the string starts with an uppercase letter.
     * 
     * @param s a String
     * @return true if the string starts with an upper case letter
     */
    private boolean initUpper(String s) {
        if (s.equals("")||s==null) {
            return true;
        }
        String first = s.substring(0,1); // get the first character
        if (first.toUpperCase().equals(first)) {
            return true;
        }
        return false;
    }

}
