package com.docfacto.adam;

/**
 * A collection of constants for Adam.
 *
 * @author dhudson - created 3 Oct 2013
 * @since 2.4
 */
public class AdamConstants {

    public static final String RECOMMEND_OPTION = "-recommend";
    
    /**
     * {@value}
     */
    public static final String RETURN_TAG = "@return";
}
