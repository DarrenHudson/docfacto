package com.docfacto.adam;

import java.util.ArrayList;
import java.util.List;

import com.docfacto.output.generated.Position;
import com.docfacto.output.generated.Result;
import com.docfacto.output.generated.Rule;
import com.sun.javadoc.SeeTag;
import com.sun.tools.javac.util.LayoutCharacters;

/**
 * See Tag Parser will look for errors within the see tag
 * 
 * {@docfacto.system Most of this stuff is taken from {@code SeeTagImpl}
 * 
 * @author dhudson - created 21 Aug 2013
 * @since 2.4
 */
public class SeeTagParser {

    /**
     * where of where#what - i.e. the class name (may be empty)
     */
    private String theWhere;

    /**
     * what of where#what - i.e. the member (may be null)
     */
    private String theWhat;

    private List<Result> theResults;

    /**
     * Constructor.
     */
    public SeeTagParser() {
        theResults = new ArrayList<Result>(3);
    }

    /**
     * Parse the see tag to try and locate any errors
     * 
     * @param tag
     * @since 2.4
     */
    public void parse(SeeTag[] tags) {
        for (SeeTag seeTag:tags) {
            parseSeeTag(seeTag);
        }
    }

    /**
     * where of where#what
     *
     * @return the where or null
     * @since 2.4
     */
    public String getWhere() {
        return theWhere;
    }
    
    /**
     * what of where#what
     * 
     * @return the what or null
     * @since 2.4
     */
    public String getWhat() {
        return theWhat;
    }
    
    /**
     * Returns a list of warning results, which could be empty
     * 
     * @return a list of warning results
     * @since 2.4
     */
    public List<Result> getResults() {
        return theResults;
    }
    
    /**
     * parse @see part of comment. Determine 'where' and 'what'
     */
    private void parseSeeTag(SeeTag tag) {
        String text = tag.text().trim();

        int len = text.length();
        if (len==0) {
            return;
        }
        switch (text.charAt(0)) {
        case '<':
            if (text.charAt(len-1)!='>') {
                addResult(tag,"No close bracket on url");
            }
            return;
        case '"':
            if (len==1||text.charAt(len-1)!='"') {
                addResult(tag,"No close quote");

            }
            return;
        }

        // check that the text is one word, with possible parentheses
        // this part of code doesn't allow
        // @see <a href=.....>asfd</a>
        // comment it.

        // the code assumes that there is no initial white space.
        int parens = 0;
        int commentstart = 0;
        int start = 0;
        int cp;
        for (int i = start;i<len;i += Character.charCount(cp)) {
            cp = text.codePointAt(i);
            switch (cp) {
            case '(':
                parens++;
                break;
            case ')':
                parens--;
                break;
            case '[':
            case ']':
            case '.':
            case '#':
                break;
            case ',':
                if (parens<=0) {
                    addResult(tag,"Malfored see tag");
                    return;
                }
                break;
            case ' ':
            case '\t':
            case '\n':
            case LayoutCharacters.CR:
                if (parens==0) { // here onwards the comment starts.
                    commentstart = i;
                    i = len;
                }
                break;
            default:
                if (!Character.isJavaIdentifierPart(cp)) {
                    addResult(tag,"Illegal character ["+cp+"] ");
                }
                break;
            }
        }
        if (parens!=0) {
            addResult(tag,"Malfored see tag");
            return;
        }

        String seetext = "";
        String labeltext = "";

        if (commentstart>0) {
            seetext = text.substring(start,commentstart);
            labeltext = text.substring(commentstart+1);
            // strip off the white space which can be between seetext and the
            // actual label.
            for (int i = 0;i<labeltext.length();i++) {
                char ch2 = labeltext.charAt(i);
                if (!(ch2==' '||ch2=='\t'||ch2=='\n')) {
                    break;
                }
            }
        }
        else {
            seetext = text;
        }

        int sharp = seetext.indexOf('#');
        if (sharp>=0) {
            // class#member
            theWhere = seetext.substring(0,sharp);
            theWhat = seetext.substring(sharp+1);
        }
        else {
            if (seetext.indexOf('(')>=0) {
                addResult(tag,"Missing sharp symbol");
                theWhere = "";
                theWhat = seetext;
            }
            else {
                // no member specified, text names class
                theWhere = seetext;
                theWhat = null;
            }
        }
    }

    /**
     * Add a result to the list
     * 
     * @param tag with the error
     * @param warning message
     * @since 2.4
     */
    private void addResult(SeeTag tag,String warning) {
        final Result result = new Result();
        final Rule rule = new Rule();
        rule.setKey(AdamRuleKeys.INVALID_SEE_TAG);
        rule.setMessage(warning);

        result.setRule(rule);

        final Position position = new Position();
        position.setColumn(tag.position().column());
        position.setLine(tag.position().line());
        position.setFile(tag.position().file().getPath());
        result.setPosition(position);
        
        theResults.add(result);
    }
}
