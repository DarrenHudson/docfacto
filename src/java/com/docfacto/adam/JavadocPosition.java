package com.docfacto.adam;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.LineNumberReader;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.javadoc.CommentParser;
import com.sun.javadoc.Doc;
import com.sun.javadoc.SourcePosition;
import com.sun.javadoc.Tag;

/**
 * Handles line / column positing within Javadoc comments.
 * 
 * Javadoc Doc implementations has a SourcePosition, this source position is the
 * position of what is being documented, for example a method and not the
 * position of the Javadoc comment itself
 * 
 * 
 * @author dhudson - created 16 Apr 2013
 * @since 2.2
 */
public class JavadocPosition implements SourcePosition {

    private final Doc theDoc;
    private int theLine;
    private int theCol;
    private final CommentParser theCommentParser;

    /**
     * Constructor.
     * 
     * @param doc the processing decl
     * @param parser Comment Parser
     * @since 2.4
     */
    public JavadocPosition(Doc doc,CommentParser parser) {
        theCommentParser = parser;
        theDoc = doc;
        theCol = parser.getCommentOffset();
        theLine = parser.getStartLine();
    }

    /**
     * Try and locate this tag within the javadoc comment
     * 
     * @param tag to search for
     * @return true if the tag was found
     * @since 2.2
     */
    public boolean locate(Tag tag) {
        StringBuilder builder = new StringBuilder(tag.name().length());
        builder.append(tag.name());

        if (!tag.text().isEmpty()) {
            builder.append(" ");
            builder.append(tag.text());
        }
        return locate(builder.toString());
    }

    /**
     * Locate text within the Javadoc comment setting the line number
     * 
     * @param text to search
     * @return true if found the text
     * @since 2.2
     */
    public boolean locate(String text) {

        LineNumberReader reader =
            IOUtils.createStringReader(theDoc.getRawCommentText());
        try {
            String line;
            while ((line = reader.readLine())!=null) {
                if (line.contains(text)) {
                    theLine += reader.getLineNumber();
                    getColumnLocation(text);
                    return true;
                }
            }
        }
        catch (IOException ex) {
            // Will not happen as this is a string
            ex.printStackTrace();
        }

        return false;
    }

    /**
     * Get the true column location.
     * 
     * The javadoc comment has removed the stars and spacing, so now need to
     * look at the source file to locate the column
     * 
     * @param text
     * @since 2.2
     */
    private void getColumnLocation(String text) {
        try {

            LineNumberReader reader =
                IOUtils.createFileReader(new FileInputStream(file()));

            try {
                int lineCount = 0;
                String line;
                while ((line = reader.readLine())!=null) {
                    lineCount++;
                    if (lineCount==theLine) {
                        // indexOf starts at zero
                        theCol = line.indexOf(text)+1;
                        break;
                    }
                }
            }
            catch (IOException ex) {

            }
            finally {
                IOUtils.close(reader);
            }

        }
        catch (DocfactoException ex) {

        }
        catch (FileNotFoundException ex) {

        }
    }

    /**
     * @see com.sun.javadoc.SourcePosition#column()
     */
    @Override
    public int column() {
        return theCol;
    }

    /**
     * @see com.sun.javadoc.SourcePosition#file()
     */
    @Override
    public File file() {
        return theDoc.position().file();
    }

    /**
     * @see com.sun.javadoc.SourcePosition#line()
     */
    @Override
    public int line() {
        return theLine;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Start Line No ["+theLine+"] Line No ["+theLine+
            "] Col ["+theCol+"]";
    }

}
