/*
 * @author dhudson -
 * Created 4 Jul 2012 : 12:23:33
 */

package com.docfacto.adam;

import java.io.File;
import java.util.List;

import com.docfacto.config.generated.Adam;
import com.docfacto.config.generated.RequiredDocumentation;
import com.docfacto.taglets.AdamTagletConstants;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.ConstructorDoc;
import com.sun.javadoc.Doc;
import com.sun.javadoc.MethodDoc;
import com.sun.javadoc.PackageDoc;
import com.sun.javadoc.Tag;

/**
 * Class that has static methods to help with the processing of JavaDoc
 * 
 * @author dhudson - created 7 Jul 2012
 * @since 2.0
 */
public class AdamUtils {

    /**
     * Enums have two synthetic methods, {@value}
     */
    private static final String ENUM_VALUES = "values";
    /**
     * Enums have two synthetic methods, {@value}
     */
    private static final String ENUM_VALUESOF = "valueOf";
    /**
     * Constant value for the inherit doc tag {@value}
     */
    private static final String INHERIT_DOC_TAG = "@inheritDoc";

    /**
     * Constructor.
     */
    private AdamUtils() {
    }

    /**
     * Checks to see if processing is required
     * 
     * @param doc to process
     * @return true if there is a tag which instructs {@code docfacto.adam} to
     * ignore processing
     * @since 2.0
     */
    public static boolean hasAdamIgnoreSet(Doc doc) {
        final Tag[] adamTags = doc.tags(AdamTagletConstants.TAG_NAME);

        if (adamTags.length>0) {
            // We have a tag..
            final Tag adamTag = adamTags[0];
            if (AdamTagletConstants.IGNORE_VALUE.equals(adamTag.text().trim())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns a string representation of Doc type
     * 
     * @param doc JavaDoc doc to process
     * @return string representation of the {@code Doc} type
     * @since 2.0
     */
    public static String getTypeName(Doc doc) {
        if (doc.isMethod()) {
            return "Method";
        }

        if (doc.isClass()) {
            return "Class";
        }

        if (doc.isInterface()) {
            return "Interface";
        }

        if (doc.isField()) {
            return "Field";
        }

        if (doc.isConstructor()) {
            return "Constructor";
        }

        if (doc.isEnum()) {
            return "Enum";
        }

        if (doc instanceof PackageDoc) {
            return "Package";
        }

        return "Unknown";
    }

    /**
     * Checks to see if the class is using the default {@link Object}
     * constructor
     * 
     * {@docfacto.note If a default constructor has been injected, it shares the
     * same line number as the class }
     * 
     * @param constructorDoc to process
     * @return true, if the class is using the default {@link Object}
     * constructor
     * @since 2.0
     */
    public static boolean isDefaultObjectConstructor(
    ConstructorDoc constructorDoc) {
        return (constructorDoc.containingClass().position().line()==constructorDoc
            .position().line());
    }

    /**
     * Checks to see if the method is over ridden from a super class
     * 
     * @param methodDoc to process
     * @return true if the method is over ridden
     * @since 2.0
     */
    public static boolean isOverridenMethod(MethodDoc methodDoc) {

        if (methodDoc.overriddenMethod()==null) {
            return false;
        }

        return true;
    }

    /**
     * Checks to see if JavaDoc is present
     * 
     * @param doc to process
     * @return true if there is JavaDoc present
     * @since 2.0
     */
    public static boolean hasJavadoc(Doc doc) {
        final String comment = doc.getRawCommentText();
        if (comment==null||comment.isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * Check to see that the Javadoc has a comment, this is basically checking
     * for the first line.
     * 
     * @param doc to process
     * @return true if the JavaDoc has a comment
     * @since 2.0
     */
    public static boolean hasComment(Doc doc) {
        final String comment = doc.commentText();
        if (comment==null||comment.trim().isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * Checks to see if method is from a {@link Enum} class
     * 
     * {@docfacto.system The reason that this is so difficult is that
     * com.sun.javadoc.ClassDoc.isEnum() always returns false}
     * 
     * @param methodDoc to process
     * @return true if method is part of a Enum class
     * @since 2.0
     */
    public static boolean isEnumMethod(MethodDoc methodDoc) {

        if (methodDoc.name().equals(ENUM_VALUES)||
            methodDoc.name().equals(ENUM_VALUESOF)) {
            // Injected methods have the same position as the constructor
            if (methodDoc.containingClass().position().line()==methodDoc
                .position().line()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks to see if the method implements an interface. If so, the Standard
     * doclet inherits the comment from that interface.
     * 
     * @param classList the list of class interfaces
     * @param methodDoc to process
     * @return true if the method implements an interface method
     * @since 2.0
     */
    public static boolean implementsInterface(List<ClassDoc> classList,
    MethodDoc methodDoc)
    {
        for (final ClassDoc ifaceDoc:classList) {
            for (final MethodDoc imethod:ifaceDoc.methods()) {
                // Examine each method in the interface
                if (!imethod.name().equals(methodDoc.name())) {
                    continue;
                }
                if (!imethod.returnType().equals(methodDoc.returnType())) {
                    continue;
                }
                if (!imethod.signature().equals(methodDoc.signature())) {
                    continue;
                }
                // Matching method found
                return true;
            }
        }

        return false;
    }

    /**
     * Check to see if the Javadoc comments have a {@code @see} or
     * {@code @inheritDoc}
     * 
     * @param doc to process
     * @return true if the tags have a {@code @see} or {@code @inheritDoc}
     * @since 2.0
     */
    public static boolean hasSeeOrInheritTag(Doc doc) {

        if (doc.seeTags().length==1&&doc.tags().length==1) {
            // Could be an interface method with a @see tag
            return true;
        }

        if (doc.tags(INHERIT_DOC_TAG).length==1) {
            // Inherit doc tag
            return true;
        }

        return false;
    }

    /**
     * Check to see if the package required files are there
     * 
     * @param config which contains the required files
     * @param packagePath to check
     * @return results, which could be empty
     * @since 2.2
     */
    public static AdamResults checkPackageRequiredFile(Adam config,
    String packagePath) {

        final AdamResults results = new AdamResults();

        if (config.getPackages()!=null) {
            final RequiredDocumentation requiredDoc =
                config.getPackages().getRequiredDocumentation();

            if (requiredDoc!=null) {
                for (final com.docfacto.config.generated.Doc doc:requiredDoc
                    .getDocs()) {
                    final File required = new File(packagePath,doc.getValue());
                    if (!required.exists()) {
                        results.addResult(
                            "Required Package File "+doc.getValue()+
                                " missing",doc.getSeverity(),
                            AdamRuleKeys.MISSING_PACKAGE_REQUIRED_FILE_KEY);
                    }
                }
            }
        }

        return results;
    }
}
