package com.docfacto.adam;

import com.docfacto.config.generated.Severity;

/**
 * A Single Adam Result
 * 
 * @author dhudson - created 3 Mar 2013
 * @since 2.2
 */
public class AdamResult {

    private final String theMessage;
    private final Severity theSeverity;
    private String theKey;

    /**
     * Constructor.
     * 
     * @param message
     * @param severity
     * @since 2.2
     */
    public AdamResult(String message,Severity severity) {
        theMessage = message;
        theSeverity = severity;
    }

    /**
     * Constructor.
     * 
     * @param message
     * @param severity
     * @param key
     * @since 2.2
     */
    public AdamResult(String message,Severity severity,String key) {
        this(message,severity);
        theKey = key;
    }

    /**
     * Return the message
     * 
     * @return the message
     * @since 2.2
     */
    public String getMessage() {
        return theMessage;
    }

    /**
     * Set the rule key
     * 
     * @param key
     * @since 2.2
     */
    public void setKet(String key) {
        theKey = key;
    }

    /**
     * Fetch the rule key
     * 
     * @return the rule key
     * @since 2.2
     */
    public String getKey() {
        return theKey;
    }

    /**
     * Return the severity
     * 
     * @return the severity
     * @since 2.2
     */
    public Severity getSeverity() {
        return theSeverity;
    }
}
