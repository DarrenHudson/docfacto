/*
 * @author dhudson -
 * Created 3 Apr 2013 : 09:42:58
 */

package com.docfacto.adam;

import static com.sun.tools.javac.code.Flags.PROTECTED;
import static com.sun.tools.javac.code.Flags.PUBLIC;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.StringTokenizer;

import com.docfacto.common.DocfactoException;
import com.docfacto.doclets.adam.AdamDoclet;
import com.sun.tools.javac.main.CommandLine;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.ListBuffer;
import com.sun.tools.javac.util.Options;
import com.sun.tools.javadoc.JavadocTool;
import com.sun.tools.javadoc.Messager;
import com.sun.tools.javadoc.ModifierFilter;
import com.sun.tools.javadoc.RootDocImpl;

/**
 * This is used to invoke the Adam Doclet from outside of Javadoc.
 * 
 * <p>
 * {@docfacto.system
 * -doclet com.docfacto.doclets.adam.AdamDoclet -docletpath
 * /Users/dhudson/development/docfacto/main/lib/docfacto.jar -sourcepath
 * /Users/dhudson/development/docfacto/main/src/java -output
 * //Users/dhudson/AdamOutput.xml -config
 * /Users/dhudson/development/docfacto/main/etc/Docfacto.xml
 * /Users/dhudson/development
 * /docfacto/main/src/java/com/docfacto/test/ConfigLoader.java
 * /Users/dhudson/development
 * /docfacto/main/src/java/com/docfacto/test/TreeDumper.java
 * } 
 * </p>
 * 
 * <p>
 * Used by Sonar and Eclipse
 * </p>
 * 
 * @author dhudson - created 3 Apr 2013
 * @since 2.2
 */
public class AdamInvoker {

    private final Context theContext;
    private final Messager theMessager;
    private ModifierFilter showAccess = null;
    private ListBuffer<String[]> options = new ListBuffer<String[]>();
    String docLocale = "";

    boolean breakiterator = false;
    boolean quiet = false;
    String encoding = null;

    private final long defaultFilter = PUBLIC|PROTECTED;

    /* Treat warnings as errors. */
    private boolean rejectWarnings = false;

    private RootDocImpl theRootDoc;

    private JavadocTool theCompiler;

    /**
     * Constructor.
     * 
     * @since 2.2
     */
    public AdamInvoker() {
        theContext = new Context();
        theMessager = new AdamMessager(theContext);
    }

    /**
     * Invoke Adam and return an AdamOutputProcessor
     * 
     * @param args list of source files to process
     * @return an AdamOutputProcessor
     * @throws DocfactoException which could be a InvalidLicence, InvalidConfig
     * @since 2.4
     */
    public AdamOutputProcessor invoke(String... args) throws DocfactoException {

        if (parse(args)) {

            final AdamProcessor processor = new AdamProcessor(theRootDoc);
            processor.process();
            return processor.getAdamOutputProcessor();
        }

        return null;
    }

    /**
     * Parse the arguments supplied
     * 
     * @param argv
     * @return
     * @since 2.2
     */
    private boolean parse(String... argv) {
        final ListBuffer<String> javaNames = new ListBuffer<String>();

        // Preprocess @file arguments
        try {
            argv = CommandLine.parse(argv);
        }
        catch (final FileNotFoundException e) {
            theMessager.error(null,"main.cant.read",e.getMessage());
            return false;
        }
        catch (final IOException e) {
            e.printStackTrace();
            return false;
        }

        final ListBuffer<String> subPackages = new ListBuffer<String>();
        final ListBuffer<String> excludedPackages = new ListBuffer<String>();
        final Options compOpts = Options.instance(theContext);
        boolean docClasses = false;

        // Parse arguments
        for (int i = 0;i<argv.length;i++) {
            final String arg = argv[i];
            if (arg.equals("-subpackages")) {
                oneArg(argv,i++);
                addToList(subPackages,argv[i]);
            }
            else if (arg.equals("-exclude")) {
                oneArg(argv,i++);
                addToList(excludedPackages,argv[i]);
            }
            else if (arg.equals("-verbose")) {
                setOption(arg);
                compOpts.put("-verbose","");
            }
            else if (arg.equals("-encoding")) {
                oneArg(argv,i++);
                encoding = argv[i];
                compOpts.put("-encoding",argv[i]);
            }
            else if (arg.equals("-breakiterator")) {
                breakiterator = true;
                setOption("-breakiterator");
            }
            else if (arg.equals("-quiet")) {
                quiet = true;
                setOption("-quiet");
            }
            else if (arg.equals("-Xclasses")) {
                setOption(arg);
                docClasses = true;
            }
            else if (arg.equals("-Xwerror")) {
                setOption(arg);
                rejectWarnings = true;
            }
            else if (arg.equals("-private")) {
                setOption(arg);
                setFilter(ModifierFilter.ALL_ACCESS);
            }
            else if (arg.equals("-package")) {
                setOption(arg);
                setFilter(PUBLIC|PROTECTED|
                    ModifierFilter.PACKAGE);
            }
            else if (arg.equals("-protected")) {
                setOption(arg);
                setFilter(PUBLIC|PROTECTED);
            }
            else if (arg.equals("-public")) {
                setOption(arg);
                setFilter(PUBLIC);
            }
            else if (arg.equals("-source")) {
                oneArg(argv,i++);
                if (compOpts.get("-source")!=null) {
                    usageError("main.option.already.seen",arg);
                }
                compOpts.put("-source",argv[i]);
            }
            else if (arg.equals("-prompt")) {
                compOpts.put("-prompt","-prompt");
                theMessager.promptOnError = true;
            }
            else if (arg.equals("-sourcepath")) {
                oneArg(argv,i++);
                if (compOpts.get("-sourcepath")!=null) {
                    usageError("main.option.already.seen",arg);
                }
                compOpts.put("-sourcepath",argv[i]);
            }
            else if (arg.equals("-classpath")) {
                oneArg(argv,i++);
                if (compOpts.get("-classpath")!=null) {
                    usageError("main.option.already.seen",arg);
                }
                compOpts.put("-classpath",argv[i]);
            }
            else if (arg.equals("-sysclasspath")) {
                oneArg(argv,i++);
                if (compOpts.get("-bootclasspath")!=null) {
                    usageError("main.option.already.seen",arg);
                }
                compOpts.put("-bootclasspath",argv[i]);
            }
            else if (arg.equals("-bootclasspath")) {
                oneArg(argv,i++);
                if (compOpts.get("-bootclasspath")!=null) {
                    usageError("main.option.already.seen",arg);
                }
                compOpts.put("-bootclasspath",argv[i]);
            }
            else if (arg.equals("-extdirs")) {
                oneArg(argv,i++);
                if (compOpts.get("-extdirs")!=null) {
                    usageError("main.option.already.seen",arg);
                }
                compOpts.put("-extdirs",argv[i]);
            }
            else if (arg.equals("-overview")) {
                oneArg(argv,i++);
            }
            else if (arg.equals("-doclet")) {
                i++; // handled in setDocletInvoker
            }
            else if (arg.equals("-docletpath")) {
                i++; // handled in setDocletInvoker
            }
            else if (arg.equals("-locale")) {
                if (i!=0)
                    usageError("main.locale_first");
                oneArg(argv,i++);
                docLocale = argv[i];
            }
            else if (arg.startsWith("-XD")) {
                final String s = arg.substring("-XD".length());
                final int eq = s.indexOf('=');
                final String key = (eq<0) ? s : s.substring(0,eq);
                final String value = (eq<0) ? s : s.substring(eq+1);
                compOpts.put(key,value);
            }
            // call doclet for its options
            // other arg starts with - is invalid
            else if (arg.startsWith("-")) {
                final int optionLength = AdamDoclet.optionLength(arg);
                if (optionLength<0) {
                    // error already displayed
                    return false;
                }
                else if (optionLength==0) {
                    // option not found
                    usageError("main.invalid_flag",arg);
                    return false;
                }
                else {
                    // doclet added option
                    if ((i+optionLength)>argv.length) {
                        usageError("main.requires_argument",arg);
                    }
                    final ListBuffer<String> args = new ListBuffer<String>();
                    for (int j = 0;j<optionLength-1;++j) {
                        args.append(argv[++i]);
                    }
                    setOption(arg,args.toList());
                }
            }
            else {
                javaNames.append(arg);
            }
        }

        if (javaNames.isEmpty()&&subPackages.isEmpty()) {
            usageError("main.No_packages_or_classes_specified");
        }

        // if (!AdamDoclet.validOptions(options.toList())) {
        // // error message already displayed
        // exit();
        // }

        if (theCompiler==null) {
            theCompiler = JavadocTool.make0(theContext);
        }

        if (theCompiler==null) {
            return false;
        }

        if (showAccess==null) {
            setFilter(defaultFilter);
        }

        try {
            theRootDoc = theCompiler.getRootDocImpl(
                docLocale,encoding,showAccess,
                javaNames.toList(),options.toList(),breakiterator,
                subPackages.toList(),excludedPackages.toList(),
                docClasses,false,quiet);
        }
        catch (final IOException ex) {
            return false;
        }

        if (theRootDoc==null) {
            return false;
        }

        return true;
    }

    private void setFilter(long filterBits) {
        if (showAccess!=null) {
            theMessager.error(null,"main.incompatible.access.flags");
        }
        showAccess = new ModifierFilter(filterBits);
    }

    /**
     * Set one arg option. Error and exit if one argument is not provided.
     */
    private void oneArg(String[] args,int index) {
        if ((index+1)<args.length) {
            setOption(args[index],args[index+1]);
        }
        else {
            usageError("main.requires_argument",args[index]);
        }
    }

    private void addToList(ListBuffer<String> list,String str) {
        final StringTokenizer st = new StringTokenizer(str,":");
        String current;
        while (st.hasMoreTokens()) {
            current = st.nextToken();
            list.append(current);
        }
    }

    /**
     * indicate an option with no arguments was given.
     */
    private void setOption(String opt) {
        final String[] option = {opt};
        options.append(option);
    }

    /**
     * indicate an option with one argument was given.
     */
    private void setOption(String opt,String argument) {
        final String[] option = {opt,argument};
        options.append(option);
    }

    /**
     * indicate an option with the specified list of arguments was given.
     */
    private void setOption(String opt,List<String> arguments) {
        final String[] args = new String[arguments.length()+1];
        int k = 0;
        args[k++] = opt;
        for (List<String> i = arguments;i.nonEmpty();i = i.tail) {
            args[k++] = i.head;
        }
        options = options.append(args);
    }

    private void usageError(String key) {
        theMessager.error(null,key);
    }

    private void usageError(String key,String a1) {
        theMessager.error(null,key,a1);
    }
}
