package com.docfacto.adam;

import java.util.ArrayList;
import java.util.List;

import com.docfacto.config.generated.Severity;

/**
 * Contains a collection of {@code AdamResult}s
 * 
 * @author dhudson - created 3 Mar 2013
 * @since 2.2
 */
public class AdamResults {

    private final ArrayList<AdamResult> theResults;

    /**
     * Constructor.
     */
    public AdamResults() {
        theResults = new ArrayList<AdamResult>(3);
    }

    /**
     * Create a new {@code AdamResult} and add it to the list
     * 
     * @param message
     * @param severity
     * @param ruleKey the ruleKey from {@code AdamRuleKeys}
     * @since 2.2
     */
    public void addResult(String message,Severity severity,String ruleKey) {
        theResults.add(new AdamResult(message,severity,ruleKey));
    }

    /**
     * Create a Adamlet result
     * 
     * @param message
     * @param severity
     * @since 2.2
     */
    public void addAdamletResult(String message,Severity severity) {
        theResults.add(new AdamResult(message,severity,
            AdamRuleKeys.ADAMLET_WARNINGS_KEY));
    }

    /**
     * Add a result to the list
     * 
     * @param result to add
     * @since 2.2
     */
    public void addResult(AdamResult result) {
        theResults.add(result);
    }

    /**
     * Return true if there are results
     * 
     * @return true, if there are any results
     * @since 2.2
     */
    public boolean hasResults() {
        return theResults.size()>0;
    }

    /**
     * Return the results
     * 
     * @return the list of results, which could be empty
     * @since 2.2
     */
    public List<AdamResult> getResults() {
        return theResults;
    }

    /**
     * Clear the results
     * 
     * @since 2.2
     */
    public void clearResults() {
        theResults.clear();
    }
}
