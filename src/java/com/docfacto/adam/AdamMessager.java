/*
 * @author dhudson -
 * Created 3 Apr 2013 : 10:09:13
 */

package com.docfacto.adam;

import com.sun.tools.javac.util.Context;
import com.sun.tools.javadoc.Messager;

/**
 * Private class with a protected constructor
 * 
 * @see com.sun.tools.javadoc.Messager
 *
 * @author dhudson - created 3 Apr 2013
 * @since 2.2
 */
public class AdamMessager extends Messager {

    /**
     * Create a new instance of <code>AdamMessager</code>.
     * @param context of which to create a new {@code Messager}
     */
    protected AdamMessager(Context context) {
        super(context,"Adam");
    }

}
