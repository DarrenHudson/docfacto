/*
 * @author dhudson -
 * Created 19 Jun 2012 : 09:48:31
 */

package com.docfacto.adam;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.docfacto.common.DocfactoException;
import com.docfacto.config.AdamXmlConfig;
import com.docfacto.config.XmlConfig;
import com.docfacto.config.generated.AdamletDefinition;
import com.docfacto.config.generated.CommentRegex;
import com.docfacto.config.generated.Comments;
import com.docfacto.config.generated.Enums;
import com.docfacto.config.generated.RequiredTag;
import com.docfacto.config.generated.RequiredTags;
import com.docfacto.config.generated.RuleWithSeverity;
import com.docfacto.config.generated.Severity;
import com.docfacto.doclets.adam.AdamDoclet;
import com.docfacto.exceptions.DocfactoParsingException;
import com.docfacto.exceptions.InvalidConfigException;
import com.docfacto.exceptions.InvalidLicenceException;
import com.docfacto.javadoc.CommentParser;
import com.docfacto.javadoc.JavaDocUtils;
import com.docfacto.javadoc.TagParser;
import com.docfacto.licensor.Licensor;
import com.docfacto.licensor.Products;
import com.docfacto.links.scanners.JavaSourceScanner;
import com.docfacto.output.generated.Fix;
import com.docfacto.output.generated.FixType;
import com.docfacto.output.generated.Position;
import com.docfacto.output.generated.Result;
import com.docfacto.output.generated.Rule;
import com.docfacto.output.generated.Statistic;
import com.docfacto.taglets.TagletLoader;
import com.docfacto.taglets.generated.Attribute;
import com.docfacto.taglets.generated.InternalTagletDefinition;
import com.sun.javadoc.AnnotationDesc;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.ConstructorDoc;
import com.sun.javadoc.Doc;
import com.sun.javadoc.ExecutableMemberDoc;
import com.sun.javadoc.FieldDoc;
import com.sun.javadoc.MethodDoc;
import com.sun.javadoc.PackageDoc;
import com.sun.javadoc.ParamTag;
import com.sun.javadoc.Parameter;
import com.sun.javadoc.ProgramElementDoc;
import com.sun.javadoc.RootDoc;
import com.sun.javadoc.SourcePosition;
import com.sun.javadoc.Tag;
import com.sun.javadoc.ThrowsTag;
import com.sun.javadoc.Type;

/**
 * Main class which will process the {@code RootDoc} from the JavaDoc processor
 * 
 * {@docfacto.media uri="doc-files/JavaDoc.svg" }
 * 
 * {@docfacto.system <p>
 * See <a href=
 * "http://docs.oracle.com/javase/6/docs/technotes/tools/solaris/javadoc.html"
 * ></a>
 * </p>
 * <p>
 * See <a href=
 * "http://www.oracle.com/technetwork/java/javase/documentation/index-137868.html"
 * ></a>
 * </p>
 * * }
 * 
 * @author dhudson - created 7 Jul 2012
 * @since 2.0
 */
public class AdamProcessor {

    /**
     * JavaDoc RootDoc
     */
    private final RootDoc theRootDoc;

    /**
     * HashMap for containing {@code java.lang.runtime} and
     * {@code java.lang.error} exceptions
     */
    private final HashMap<String,String> theLangExceptions;

    /**
     * The Adam section of the un-marshalled Docfacto XML config
     */
    private AdamXmlConfig theAdamConfig;

    /**
     * The Docfacto.xml un-marshalled XMl
     */
    private XmlConfig theXmlConfig;

    /**
     * Map to hold a cache of patterns for performance
     */
    private final HashMap<String,Pattern> thePatternMap;

    private File theSourcePath;

    private AdamOutputProcessor theOutput;

    private File theOutputFile;

    private boolean isQuiet = false;

    private boolean isRecommending = false;

    private final FirstSentenceParser theFSParser;

    private HashMap<String,InternalTagletDefinition> theDocfactoTaglets;

    private CommentRecommender theRecommender;

    /**
     * Constructor.
     * 
     * @param root JavaDoc RootDoc to process
     * @since 2.0
     */
    public AdamProcessor(final RootDoc root) {
        theRootDoc = root;
        theLangExceptions = new HashMap<String,String>();
        thePatternMap = new HashMap<String,Pattern>();
        theFSParser = new FirstSentenceParser();
        theRecommender = new CommentRecommender();
    }

    /**
     * Check to see if the correct options are present
     * 
     * @return false if the correct parameters are not set
     * @throws DocfactoException this could be a InvalidLicenceException or a
     * InvalidConfigException
     * @since 2.0
     */
    private void checkOptions() throws DocfactoException {

        // Need to check for class path and source path
        final String[][] options = theRootDoc.options();

        try {
            theOutput = new AdamOutputProcessor();
        }
        catch (DocfactoException ex) {
            // If I can't do this then there is no point
            theRootDoc.printError("Unable to create AdamOutputProcessor "
                +ex.getLocalizedMessage());
            throw ex;
        }

        try {
            theOutputFile = JavaDocUtils.getOutputFile(options);
        }
        catch (DocfactoException ex) {
            theRootDoc.printError("Unable to get output file "
                +ex.getLocalizedMessage());
            throw ex;
        }

        // From this point on, we can start logging issues in the Output
        // Processor
        try {
            final File configFile = JavaDocUtils.getConfigFile(options);
            theXmlConfig = new XmlConfig(configFile);
            // OK we got this far, so lets
            theAdamConfig = new AdamXmlConfig(theXmlConfig.getAdamConfig(),
                theRootDoc);

        }
        catch (InvalidConfigException ex) {
            generateInternalError("Unable to load the Docfacto config file ",
                ex);
            throw ex;
        }

        try {
            if (!Licensor.checkLicense(AdamDoclet.PRODUCT_NAME,theXmlConfig.getProduct(Products.ADAM))) {
                generateInternalError(
                    "Invalid Docfacto Licence, please contact Docfacto Ltd [www.docfacto.com]",
                    null);
                throw new InvalidLicenceException(
                    "Invalid Docfacto Licence, please contact Docfacto Ltd [www.docfacto.com]");
            }
        }
        catch (InvalidLicenceException ex) {
            generateInternalError("Unable to process licence",ex);
            throw ex;
        }

        // Get the source path from the options
        theSourcePath = JavaDocUtils.getSourcePath(options);

        for (String[] option:options) {
            if (option[0].equals("-quiet")) {
                isQuiet = true;
            }
            if (option[0].equals(AdamConstants.RECOMMEND_OPTION)) {
                isRecommending = true;
            }
        }
    }

    /**
     * Generate an internal error in the AdamOutputProcessor.
     * 
     * This will enable remote systems (Sonar / Eclipse) to have a better idea
     * of any issues.
     * 
     * @param message of the internal error
     * @param ex exception if present
     * @since 2.4
     */
    private void generateInternalError(String message,Exception ex) {

        theOutput.generateInternalError(message,ex);

        theRootDoc.printError(message);

        if (ex!=null) {
            String fullException = DocfactoException.getFullExceptionMessage(
                ex,true);

            theRootDoc.printError(fullException);
        }

        try {
            theOutput.save(theOutputFile);
        }
        catch (DocfactoException e) {
            theRootDoc.printError("Unable to save the output file "
                +ex.getMessage());
        }
    }

    /**
     * Main entry point for the Adam processor
     * 
     * @throws DocfactoException which could be InvalidLicence or InvalidConfig
     * @return true, if the Javadoc was processed
     * @since 2.0
     */
    public boolean process() throws DocfactoException {

        // No point processing if the correct options are not set
        checkOptions();

        // Lets load the Docfacto taglets
        try {
            TagletLoader loader = new TagletLoader();
            List<InternalTagletDefinition> taglets = loader.getCustomTaglets();
            theDocfactoTaglets =
                new HashMap<String,InternalTagletDefinition>(taglets.size());

            for (InternalTagletDefinition def:taglets) {
                theDocfactoTaglets.put("@"+def.getName(),def);
            }
        }
        catch (DocfactoException ex) {
            // Unlikely to happen
            generateInternalError("Unable to load Docfacto internal taglets",ex);
            throw ex;
        }

        printNotice("Version "+theXmlConfig.getProductRelease());

        // Start the clock
        final long startTime = System.currentTimeMillis();

        // Analyse the packages
        for (final PackageDoc packageDoc:theRootDoc.specifiedPackages()) {
            theOutput.incrementStatistic(AdamRuleKeys.PACKAGES_PROCESSED_KEY);

            if (AdamUtils.hasAdamIgnoreSet(packageDoc)) {
                theOutput.incrementStatistic(AdamRuleKeys.PACKAGES_IGNORED_KEY);
                continue;
            }

            processPackage(packageDoc);

            processClasses(packageDoc.allClasses());
        }

        if (theRootDoc.specifiedClasses().length>0) {
            // Just single class files have been specified, so lets just process
            // them
            processClasses(theRootDoc.specifiedClasses());
        }

        // Stop the clock
        final long endTime = System.currentTimeMillis();

        // Lets output results.
        if (!isQuiet) {
            for (final Result result:theOutput.getResults()) {
                printResult(result);
            }
        }

        // Print stats
        if (theAdamConfig.isStatisticsOutput()) {
            // Print the stats if required

            if (theOutput.getStatistics().size()==0) {

                // Something went wrong here ..
                printNotice("No files to process");
            }
            else {

                if (!isQuiet) {
                    printNotice("Stats:");

                    for (final Statistic statistic:theOutput.getStatistics()) {
                        // Only print the stats that have a value
                        if (statistic.getValue()>0) {
                            printStatistic(statistic);
                        }
                    }
                }
            }
        }

        printNotice("processed in "+(endTime-startTime)+" ms ");

        // Save file if required
        if (theOutputFile!=null) {
            try {
                theOutput.save(theOutputFile);
            }
            catch (final DocfactoException ex) {
                theRootDoc.printError("Unable to save output file "
                    +ex.getMessage());
            }
        }
        return true;
    }

    /**
     * Process package information from the JavaDoc processor
     * 
     * @param packageDoc to process
     * @since 2.0
     */
    private void processPackage(PackageDoc packageDoc) {

        final Comments comments = theAdamConfig.getPackageComments();

        if (theAdamConfig.getRequiredPackageFiles()!=null) {
            final String packagePath = packageDoc.name().replace(".","/");
            final File packageFile = new File(theSourcePath,packagePath);

            final AdamResults results = AdamUtils.checkPackageRequiredFile(
                theXmlConfig.getAdamConfig(),packageFile.getPath());

            for (final AdamResult result:results.getResults()) {
                addResult(packageDoc,result.getSeverity(),
                    result.getMessage(),result.getKey());
            }
        }

        // Nothing to do
        if (!comments.getCommentRequired().isValue()) {
            return;
        }

        if (AdamUtils.hasComment(packageDoc)) {

            int index;
            try {
                index =
                    JavaDocUtils.getPackageCommentStart(packageDoc.position()
                        .file());
            }
            catch (DocfactoException ex) {
                index = 1;
            }
            CommentParser parser = new CommentParser(index);
            parser.parse(packageDoc.getRawCommentText());

            processComment(packageDoc,comments,parser);
        }
        else {
            addResult(packageDoc,
                "Missing package-info.java or package.html for package "
                    +packageDoc.name(),
                AdamRuleKeys.MISSING_PACKAGE_DOC_KEY);
        }
    }

    /**
     * Process the classes
     * 
     * @param classDocs
     * @since 2.2
     */
    private void processClasses(ClassDoc[] classDocs) {
        JavaSourceScanner commentScanner = null;

        // Loop around the classes
        for (final ClassDoc classDoc:classDocs) {

            try {
                // Nested classes share the same file
                if (commentScanner==null||
                    !classDoc.position().file()
                        .equals(commentScanner.getFile())) {

                    commentScanner =
                        new JavaSourceScanner(classDoc.position().file());
                    commentScanner.scan();
                }

                if (classDoc.isEnum()) {
                    // Process Enum
                    processEnum(classDoc,commentScanner);
                }
                else if (classDoc.isInterface()) {
                    // Process Interface
                    processInterface(classDoc,commentScanner);
                }
                else {
                    // Process class
                    processClass(classDoc,commentScanner);
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Process the Enum and report any errors
     * 
     * @param classDoc enum class doc
     * @since 2.2
     */
    private void processEnum(ClassDoc classDoc,JavaSourceScanner scanner) {
        theOutput.incrementStatistic(AdamRuleKeys.ENUMS_PROCESSED_KEY);

        Comments comments = theAdamConfig.getCommentSectionFor(classDoc);

        if (!AdamUtils.hasJavadoc(classDoc)) {
            final Severity severity = comments.getCommentRequired()
                .getSeverity();
            addResult(classDoc,severity,"Missing javadoc",
                AdamRuleKeys.ENUMS_MISSINGS_JAVADOC_KEY);

            // TODO: Suggest something
        }
        else {

            // To have the docfacto.adam ignore tag, it must have a comment
            if (AdamUtils.hasAdamIgnoreSet(classDoc)) {
                theOutput.incrementStatistic(AdamRuleKeys.ENUMS_IGNORED_KEY);
                return;
            }

            CommentParser parser =
                scanner.findCommentFor(classDoc.position().line());

            if (AdamUtils.hasSeeOrInheritTag(classDoc)) {
                processSeeTags(classDoc,
                    theAdamConfig.getCommentSectionFor(classDoc));
            }
            else {

                // Nothing to do
                if (comments.getCommentRequired().isValue()) {
                    if (!AdamUtils.hasComment(classDoc)) {

                        final Severity severity = comments.getCommentRequired()
                            .getSeverity();
                        addResult(classDoc,
                            new JavadocPosition(classDoc,parser),
                            severity,
                            "Missing comment",
                            AdamRuleKeys.ENUMS_MISSING_COMMENT_KEY);

                        // TODO: Suggest something
                    }
                    else {
                        processComment(classDoc,comments,parser);
                    }
                }
            }

            final Enums enumSection = theAdamConfig.getEnumsSection();

            final RuleWithSeverity constantRule = enumSection
                .getConstantsRequireJavadoc();

            if (constantRule.isValue()==true) {
                for (final FieldDoc fieldDoc:classDoc.enumConstants()) {
                    if (AdamUtils.hasAdamIgnoreSet(fieldDoc)) {
                        continue;
                    }

                    if (!AdamUtils.hasJavadoc(fieldDoc)) {                        
                        addResult(
                            fieldDoc,
                            constantRule.getSeverity(),
                            "Missing Javadoc for Enum constant ["
                                +fieldDoc.name()+"]",
                            AdamRuleKeys.ENUMS_CONSTANT_MISSING_JAVADOC_KEY);

                        // TODO: Suggest something
                        continue;
                    }

                    comments = theAdamConfig.getCommentSectionFor(fieldDoc);
                    parser = scanner.findCommentFor(fieldDoc.position().line());

                    if (!AdamUtils.hasComment(fieldDoc)) {
                        addResult(
                            fieldDoc,new JavadocPosition(fieldDoc,parser),
                            constantRule.getSeverity(),
                            "Missing comment for Enum constant ["
                                +fieldDoc.name()+"]",
                            AdamRuleKeys.ENUMS_MISSING_COMMENT_KEY);

                        // TODO: if recommending, what is it?
                    }
                    else {
                        processComment(fieldDoc,comments,parser);
                    }
                }
            }
        }

        processFields(classDoc,scanner);

        processConstructors(classDoc,scanner);

        processMethods(classDoc,scanner);
    }

    /**
     * Process the Interface and report any errors
     * 
     * @param classDoc interface class doc
     * @since 2.2
     */
    private void processInterface(ClassDoc classDoc,JavaSourceScanner scanner) {
        theOutput.incrementStatistic(AdamRuleKeys.INTERFACES_PROCESSED_KEY);

        final Comments comments = theAdamConfig.getCommentSectionFor(classDoc);

        if (!AdamUtils.hasJavadoc(classDoc)) {
            final Severity severity = comments.getCommentRequired()
                .getSeverity();
            addResult(classDoc,severity,"Missing javadoc for interface",
                AdamRuleKeys.INTERFACES_MISSING_JAVADOC_KEY);

            // TODO: Suggest something
        }
        else {

            // To have the docfacto.adam ignore tag, it must have a comment
            if (AdamUtils.hasAdamIgnoreSet(classDoc)) {
                theOutput
                    .incrementStatistic(AdamRuleKeys.INTERFACES_IGNORED_KEY);
                return;
            }
            if (AdamUtils.hasSeeOrInheritTag(classDoc)) {
                processSeeTags(classDoc,
                    theAdamConfig.getCommentSectionFor(classDoc));
                return;
            }

            CommentParser parser =
                scanner.findCommentFor(classDoc.position().line());

            if (!AdamUtils.hasComment(classDoc)) {
                final Severity severity = comments.getCommentRequired()
                    .getSeverity();
                addResult(classDoc,new JavadocPosition(classDoc,parser),
                    severity,
                    "Missing comment for interface",
                    AdamRuleKeys.INTERFACES_MISSING_COMMENT_KEY);

                // TODO: Suggest something
            }
            else {
                processComment(classDoc,comments,parser);
            }
        }

        processFields(classDoc,scanner);

        processMethods(classDoc,scanner);
    }

    /**
     * Process the class information from the JavaDoc processor
     * 
     * @param classDoc the class doc from the JavaDoc processor
     * @since 2.0
     */
    private void processClass(ClassDoc classDoc,JavaSourceScanner scanner) {
        theOutput.incrementStatistic(AdamRuleKeys.CLASSES_PROCESSED_KEY);

        final Comments comments = theAdamConfig.getCommentSectionFor(classDoc);

        if (!AdamUtils.hasJavadoc(classDoc)) {
            final Severity severity = comments.getCommentRequired()
                .getSeverity();
            addResult(classDoc,severity,"Missing javadoc for class",
                AdamRuleKeys.CLASSES_MISSING_JAVADOC_KEY);

            // TODO: Suggest something
        }
        else {

            // To have the docfacto.adam ignore tag, it must have a comment
            if (AdamUtils.hasAdamIgnoreSet(classDoc)) {
                theOutput.incrementStatistic(AdamRuleKeys.CLASSES_IGNORED_KEY);
                return;
            }
            if (AdamUtils.hasSeeOrInheritTag(classDoc)) {
                processSeeTags(classDoc,
                    theAdamConfig.getCommentSectionFor(classDoc));
            }
            else {

                CommentParser parser =
                    scanner.findCommentFor(classDoc.position().line());

                if (!AdamUtils.hasComment(classDoc)) {
                    final Severity severity = comments.getCommentRequired()
                        .getSeverity();
                    addResult(classDoc,new JavadocPosition(classDoc,parser),
                        severity,
                        "Missing comment for class",
                        AdamRuleKeys.CLASSES_MISSING_COMMENT_KEY);

                    // TODO: Suggest something
                }
                else {
                    processComment(classDoc,comments,parser);
                }
            }
        }

        processFields(classDoc,scanner);

        processConstructors(classDoc,scanner);

        processMethods(classDoc,scanner);
    }

    /**
     * Process fields in the classDoc
     * 
     * @param classDoc to process
     * @since 2.2
     */
    private void processFields(ClassDoc classDoc,JavaSourceScanner scanner) {
        // Process any fields
        for (final FieldDoc fieldDoc:classDoc.fields()) {
            theOutput.incrementStatistic(AdamRuleKeys.FIELDS_PROCESSED_KEY);

            if (AdamUtils.hasAdamIgnoreSet(fieldDoc)) {
                theOutput.incrementStatistic(AdamRuleKeys.FIELD_IGNORED_KEY);
                continue;
            }
            final Comments comments = theAdamConfig.getFieldComments();

            // Nothing to do
            if (!comments.getCommentRequired().isValue()) {
                continue;
            }

            if (!AdamUtils.hasJavadoc(fieldDoc)) {
                final Severity severity = comments.getCommentRequired()
                    .getSeverity();
                addResult(fieldDoc,severity,"Field has no Javadoc",
                    AdamRuleKeys.MISSING_FIELD_JAVADOC_KEY);

                // TODO: Suggest something

            }
            else {
                CommentParser parser =
                    scanner.findCommentFor(fieldDoc.position().line());

                processField(fieldDoc,comments,parser);
            }
        }
    }

    /**
     * Process the field information from the JavaDoc processor
     * 
     * @param fieldDoc from the JavaDoc processor
     * @since 2.0
     */
    private void processField(FieldDoc fieldDoc,Comments comments,
    CommentParser parser) {

        if (AdamUtils.hasSeeOrInheritTag(fieldDoc)) {
            processSeeTags(fieldDoc,comments);
            return;
        }

        if (!AdamUtils.hasComment(fieldDoc)) {
            final Severity severity = comments.getCommentRequired()
                .getSeverity();
            Result result =
                addResult(fieldDoc,new JavadocPosition(fieldDoc,parser),
                    severity,"Field has no comment",
                    AdamRuleKeys.MISSING_FIELD_COMMENT_KEY);
            if (isRecommending) {
                addFix(result,FixType.ADD,null,
                    theRecommender.processField(fieldDoc));
            }
        }
        else {
            processComment(fieldDoc,comments,parser);
        }
    }

    /**
     * Process the constructors for a Class with can be an Class or a Enum
     * 
     * @param classDoc to process
     * @since 2.2
     */
    private void
    processConstructors(ClassDoc classDoc,JavaSourceScanner scanner) {
        for (final ConstructorDoc constructorDoc:classDoc.constructors()) {
            if (AdamUtils.isDefaultObjectConstructor(constructorDoc)) {
                // Don't count or process these..
                continue;
            }

            theOutput
                .incrementStatistic(AdamRuleKeys.CONSTRUCTORS_PROCESSED_KEY);

            final Comments comments = theAdamConfig.getConstructorComments();

            // Nothing to do
            if (!comments.getCommentRequired().isValue()) {
                continue;
            }

            if (!AdamUtils.hasJavadoc(constructorDoc)) {
                final Severity severity = comments.getCommentRequired()
                    .getSeverity();
                // Valid constructor without a comment
                addResult(constructorDoc,severity,"Constructor has no javadoc",
                    AdamRuleKeys.CONSTRUCTOR_MISSING_JAVADOC_KEY);

                // TODO: Suggest something
                continue;
            }

            CommentParser parser =
                scanner.findCommentFor(constructorDoc.position().line());

            processConstructor(constructorDoc,comments,parser);
        }
    }

    /**
     * Process the constructor information from the JavaDoc processor
     * 
     * @param constructorDoc constructor doc from the JavaDoc processor
     * @since 2.0
     */
    private void processConstructor(ConstructorDoc constructorDoc,
    Comments comments,
    CommentParser parser) {

        if (AdamUtils.hasAdamIgnoreSet(constructorDoc)) {
            theOutput.incrementStatistic(AdamRuleKeys.CONSTRUCTORS_IGNORED_KEY);
            return;
        }

        if (AdamUtils.hasSeeOrInheritTag(constructorDoc)) {
            processSeeTags(constructorDoc,comments);
            return;
        }

        if (!AdamUtils.hasComment(constructorDoc)) {
            final Severity severity = comments.getCommentRequired()
                .getSeverity();
            addResult(constructorDoc,
                new JavadocPosition(constructorDoc,parser),severity,
                "Constructor has no comment",
                AdamRuleKeys.CLASSES_MISSING_COMMENT_KEY);
            // TODO: a suggestion here..
        }
        else {
            processComment(constructorDoc,comments,parser);
        }

        // Still need to do this just because there are is no first line,
        // may be params etc
        paramCheck(constructorDoc,
            comments.getParamTagRequireDescriptions(),parser);

        checkThrows(constructorDoc,
            comments.getThrowsTagRequireDescriptions(),parser);

    }

    /**
     * Process the methods of the class doc
     * 
     * @param classDoc
     * @since 2.2
     */
    private void processMethods(ClassDoc classDoc,JavaSourceScanner scanner) {
        // Build a list of interfaces
        final ArrayList<ClassDoc> interfaceList =
            buildListOfInterfaces(classDoc);

        // Process the methods
        for (final MethodDoc methodDoc:classDoc.methods()) {

            theOutput.incrementStatistic(AdamRuleKeys.METHODS_PROCESSED_KEY);

            if (AdamUtils.hasAdamIgnoreSet(methodDoc)) {
                theOutput.incrementStatistic(AdamRuleKeys.METHODS_INGNORED_KEY);
                continue;
            }

            final Comments comments = theAdamConfig.getMethodComments();

            // Nothing to do
            if (!comments.getCommentRequired().isValue()) {
                continue;
            }

            if (!AdamUtils.hasJavadoc(methodDoc)) {
                if (theAdamConfig.isAllowingNoCommentsForInterfaceMethods()
                    &&AdamUtils.implementsInterface(interfaceList,
                        methodDoc)) {
                    // All OK
                    continue;

                }

                if (AdamUtils.isEnumMethod(methodDoc)) {
                    // All OK
                    continue;
                }

                final Severity severity = comments.getCommentRequired()
                    .getSeverity();
                addResult(methodDoc,severity,"Method has no Javadoc",
                    AdamRuleKeys.METHOD_MISSING_JAVADOC_KEY);

                // TODO: Suggest something
                continue;

            }

            CommentParser parser =
                scanner.findCommentFor(methodDoc.position().line());

            processMethod(methodDoc,interfaceList,comments,parser);
        }
    }

    /**
     * Process the method information from the Javadoc processor
     * 
     * @param methodDoc from the Javadoc processor
     * @param interfaceList list of class interfaces
     * @since 2.0
     */
    private void processMethod(MethodDoc methodDoc,
    ArrayList<ClassDoc> interfaceList,Comments comments,CommentParser parser) {

        if (AdamUtils.hasSeeOrInheritTag(methodDoc)) {
            if (comments.getInvalidSeeTag().isValue()) {
                processSeeTags(methodDoc,comments);
            }
            return;
        }

        if (!AdamUtils.hasComment(methodDoc)) {
            final Severity severity = comments.getCommentRequired()
                .getSeverity();
            addResult(methodDoc,new JavadocPosition(methodDoc,parser),severity,
                "Method has no comment",
                AdamRuleKeys.METHOD_MISSING_COMMENT_KEY);

            // TODO: Maybe suggest something?
        }
        else {
            processComment(methodDoc,comments,parser);
        }

        paramCheck(methodDoc,comments.getParamTagRequireDescriptions(),parser);
        checkThrows(methodDoc,comments.getThrowsTagRequireDescriptions(),parser);
        checkReturn(methodDoc,parser);
    }

    /**
     * Check the return types for a method, produce warnings in they are not
     * correct
     * 
     * @param methodDoc to process
     * @since 2.0
     */
    private void checkReturn(MethodDoc methodDoc,CommentParser parser) {

        final Type returnType = methodDoc.returnType();
        final Tag[] tags = methodDoc.tags(AdamConstants.RETURN_TAG);

        // Check to see if the return type is void, but has a @return tag
        if ("void".equals(returnType.simpleTypeName())) {
            if (tags.length>0) {
                // Its void, but we have a @return tag
                JavadocPosition tagPosition =
                    new JavadocPosition(methodDoc,parser);
                tagPosition.locate("@return");
                final Result result = addResult(methodDoc,tagPosition,
                    Severity.WARNING,
                    "@return tag defined for a void method return",
                    AdamRuleKeys.INVALID_RETURN_TAGS_KEY);
                addFix(result,FixType.REMOVE,"return");
            }
        }
        else {
            // OK, its not void so we should have a @return tag
            if (tags.length==0) {
                final Result result =
                    addResult(methodDoc,new JavadocPosition(methodDoc,parser),
                        Severity.WARNING,
                        "No @return tag defined for a method return",
                        AdamRuleKeys.INVALID_RETURN_TAGS_KEY);
                addFix(result,FixType.ADD,"return");
            }
            else {
                if (tags.length>1) {
                    final Result result =
                        addResult(methodDoc,new JavadocPosition(methodDoc,
                            parser),Severity.WARNING,
                            "Too many @return tags defined for method",
                            AdamRuleKeys.INVALID_RETURN_TAGS_KEY);
                    addFix(result,FixType.REMOVE,"return");
                }
                else {
                    if (tags[0].text().trim().isEmpty()) {
                        final Comments comments = theAdamConfig
                            .getMethodComments();
                        if (comments.getReturnTagRequireDescription().isValue()) {
                            JavadocPosition tagPosition = new JavadocPosition(
                                methodDoc,parser);
                            tagPosition.locate("@return");
                            // No description
                            addResult(methodDoc,tagPosition,theAdamConfig
                                .getMethodComments()
                                .getReturnTagRequireDescription()
                                .getSeverity(),
                                "No description for return tag",
                                AdamRuleKeys.MISSING_RETURN_DESCRIPTION_KEY);
                        }
                    }
                }
            }
        }
    }

    /**
     * Check the throws on method and JavaDoc to see if they match
     * 
     * @param memberDoc either method or constructor
     * @param checkDescription true if a description must be present
     * @since 2.0
     */
    private void checkThrows(ExecutableMemberDoc memberDoc,
    RuleWithSeverity checkDescription,CommentParser parser) {

        final HashMap<String,String> exceptionTypes =
            new HashMap<String,String>(
                memberDoc.thrownExceptionTypes().length);
        final HashMap<String,String> throwTags = new HashMap<String,String>(
            memberDoc.throwsTags().length);

        for (final Type type:memberDoc.thrownExceptionTypes()) {
            exceptionTypes.put(type.simpleTypeName(),type.simpleTypeName());
        }

        for (final ThrowsTag throwTag:memberDoc.throwsTags()) {
            throwTags.put(throwTag.exceptionName(),throwTag.exceptionName());
        }

        // Maps loaded, lets check..
        for (final String exception:exceptionTypes.values()) {
            if (!throwTags.containsKey(exception)) {

                if (isRuntimeException(exception)) {
                    continue;
                }

                final Result result =
                    addResult(memberDoc,new JavadocPosition(memberDoc,parser),
                        Severity.WARNING,
                        AdamUtils.getTypeName(memberDoc)
                            +" signature throws exception "+exception
                            +" which is not defined in the Javadoc",
                        AdamRuleKeys.INVALID_THROWS_KEY);
                addFix(result,FixType.ADD,"throws");
            }
        }

        for (final ThrowsTag throwTag:memberDoc.throwsTags()) {
            final String throwName = throwTag.exceptionName();

            // Throws tag which isn't in the signature
            if (!exceptionTypes.containsKey(throwName)) {

                // Don't grumble about runtime exceptions
                if (!isRuntimeException(throwName)) {

                    JavadocPosition tagPosition =
                        new JavadocPosition(memberDoc,parser);
                    tagPosition.locate("@throws "+throwName);

                    final Result result = addResult(
                        memberDoc,
                        tagPosition,
                        Severity.WARNING,
                        "Javadoc throws tag "+throwName
                            +" not defined in "
                            +AdamUtils.getTypeName(memberDoc)
                            +" signature",
                        AdamRuleKeys.INVALID_THROWS_KEY);
                    addFix(result,FixType.REMOVE,"throws");
                }
            }
            // Its there, so lets see if it has a description
            else if (checkDescription.isValue()) {
                final String description = throwTag.exceptionComment().trim();
                if (description.isEmpty()) {
                    final Severity severity = checkDescription.getSeverity();

                    JavadocPosition tagPosition =
                        new JavadocPosition(memberDoc,parser);
                    tagPosition.locate("@throws "+throwTag.exceptionName());

                    final Result result = addResult(memberDoc,tagPosition,
                        severity,
                        "Javadoc throws tag ["+throwTag.exceptionName()
                            +"] has no description",
                        AdamRuleKeys.MISSING_THROWS_DESCRIPTION_KEY);
                    addFix(result,FixType.MODIFY,"throws");
                }
            }
        }
    }

    /**
     * Check to see if the exception is a runtime exception, as they do not need
     * to be declared as part of the method signature
     * 
     * @param exception name
     * @return true if its a {@code java.lang.RuntimeException} or
     * {@code java.lang.Error} or a subclass of
     * @since 2.0
     */
    private boolean isRuntimeException(String exception) {

        if (theLangExceptions.containsKey(exception)) {
            return true;
        }

        // Lets see if its a java.lang exception
        final String className = "java.lang."+exception;

        Class<?> obj;
        try {
            obj = Class.forName(className);

            if (RuntimeException.class.isAssignableFrom(obj)) {
                // Lets add it to the map and move on
                theLangExceptions.put(obj.getClass().getName(),obj.getClass()
                    .getName());
                return true;
            }

            if (Error.class.isAssignableFrom(obj)) {
                // Lets add it to the map and move on
                theLangExceptions.put(obj.getClass().getName(),obj.getClass()
                    .getName());
                return true;
            }

            return false;
        }
        catch (final ClassNotFoundException ignore) {
        }

        return false;
    }

    /**
     * Firstly, check the {@code all} section for required tags, and then the
     * section that the doc refers to
     * 
     * @param doc to process
     * @since 2.0
     */
    private void processRequiredTags(Doc doc,CommentParser parser) {
        // Could see in future releases, this building a list and calling
        // checkRequiredTags only once, with a merged list.
        // It is possible that you could have the same tag in the comment
        // section as well as the all section
        checkRequiredTags(theAdamConfig.getAllRequiredTags(),doc,parser);
        checkRequiredTags(theAdamConfig.getRequiredTagsForDoc(doc),doc,parser);
    }

    /**
     * Process the JavaDoc and make sure that the required tags are present
     * 
     * @param tags the required tags
     * @param doc to process
     * @since 2.0
     */
    private void checkRequiredTags(RequiredTags tags,Doc doc,
    CommentParser parser) {
        if (tags==null) {
            // Nothing to do
            return;
        }

        for (final RequiredTag requiredTag:tags.getRequiredTags()) {
            if (!requiredTag.isEnabled()) {
                // Its not required..
                continue;
            }

            final Tag[] tagArray = doc.tags(requiredTag.getValue());

            if (tagArray.length==0) {
                // Grumble ...
                final Result result = addResult(
                    doc,
                    new JavadocPosition(doc,parser),
                    requiredTag.getSeverity(),
                    "Required tag ["+requiredTag.getValue()+"] missing",
                    AdamRuleKeys.MISSING_REQUIRED_TAG);

                addFix(result,FixType.ADD,requiredTag.getValue());
                continue;
            }

            // There could be more or one of these tags, need to check that they
            // have a description
            for (final Tag innerTag:tagArray) {
                if (innerTag.text().trim().isEmpty()) {
                    JavadocPosition tagPosition =
                        new JavadocPosition(doc,parser);
                    tagPosition.locate(innerTag.name());
                    final Result result = addResult(doc,tagPosition,
                        requiredTag.getSeverity(),"Required tag ["
                            +requiredTag.getValue()
                            +"] missing description",
                        AdamRuleKeys.MISSING_REQUIRED_TAG);
                    addFix(result,FixType.MODIFY,innerTag.name());
                }
            }
        }
    }

    /**
     * Process the comments for the Javadoc item
     * 
     * @param doc JavaDoc item to process
     * @param comments config
     * @since 2.0
     */
    private void processComment(Doc doc,Comments comments,CommentParser parser) {

        lookForDeprecatedTags(doc,parser);

        final List<CommentRegex> warningList = comments.getWarningIfContains();

        if (warningList!=null) {
            checkWarningList(doc,warningList,parser);
        }

        if (comments.getCheckMeaninglessComment().isValue()) {
            checkMeaninglessComment(doc,comments.getCheckMeaninglessComment()
                .getSeverity());
        }

        // Process first sentence
        processFirstSentence(doc,comments,parser);

        // Process required tags
        processRequiredTags(doc,parser);

        // Process see tags
        processSeeTags(doc,comments);

        if (comments.getHtmlImbalance().isValue()) {
            RuleWithSeverity rule = comments.getHtmlImbalance();

            try {
                HTMLInBalanceParser htmlParser = new HTMLInBalanceParser();
                if (htmlParser.parse(doc.commentText())) {
                    Result result = addResult(
                        doc,
                        rule.getSeverity(),
                        "HTML imbalance tag ["+htmlParser.getErrorTag()+"]",
                        AdamRuleKeys.HTML_IMBALANCE);
                    addFix(result,FixType.MODIFY,htmlParser.getErrorTag());
                }
            }
            catch (DocfactoParsingException ex) {
                printNotice("Unable to parse ["+doc+"] ");
            }
        }

        processTags(doc,comments,parser);

        // Any Adamlets call them now if required?
        processAdamlets(doc,comments,parser);
    }

    /**
     * Check that the tags are correct
     * 
     * @param doc
     * @param comments
     * @since 2.4
     */
    private void processTags(Doc doc,Comments comments,CommentParser parser) {

        processSeeTags(doc,comments);

        RuleWithSeverity malformedRule = comments.getMalformedTag();
        if (!malformedRule.isValue()) {
            // Nothing to do..
            return;
        }

        // Check block tags
        for (Tag tag:doc.tags()) {
            if (theDocfactoTaglets.containsKey(tag.name())) {
                InternalTagletDefinition def =
                    theDocfactoTaglets.get(tag.name());
                // We are inline
                if (!def.isBlock()) {
                    // Invalid tag type
                    JavadocPosition tagPosition =
                        new JavadocPosition(doc,parser);
                    tagPosition.locate(tag.name());

                    Result result =
                        addResult(
                            doc,
                            tagPosition,
                            malformedRule.getSeverity(),
                            "Block tag syntax used for a inline tag ["+
                                tag.name()+"]",AdamRuleKeys.MALFORMED_TAG);
                    addFix(result,FixType.MODIFY,tag.name());
                }

                checkTagAttributes(def,tag,doc,malformedRule,parser);
            }
            else {
                if (tag.text().isEmpty()) {
                    // No arguments to a block tag
                    JavadocPosition tagPosition =
                        new JavadocPosition(doc,parser);
                    tagPosition.locate(tag.name());
                    Result result =
                        addResult(
                            doc,
                            tagPosition,
                            malformedRule.getSeverity(),
                            "Block tag  ["+
                                tag.name()+"] has no arguments",
                            AdamRuleKeys.MALFORMED_TAG);
                    addFix(result,FixType.MODIFY,tag.name());
                }
            }

        }

        // Check inline tags
        for (Tag tag:doc.inlineTags()) {
            if (theDocfactoTaglets.containsKey(tag.name())) {
                InternalTagletDefinition def =
                    theDocfactoTaglets.get(tag.name());
                if (def.isBlock()) {
                    // Invalid tag type
                    JavadocPosition tagPosition =
                        new JavadocPosition(doc,parser);
                    tagPosition.locate(tag.name());

                    Result result =
                        addResult(
                            doc,
                            tagPosition,
                            malformedRule.getSeverity(),
                            "Inline tag syntax used for a block tag ["+
                                tag.name()+"]",AdamRuleKeys.MALFORMED_TAG);
                    addFix(result,FixType.MODIFY,tag.name());
                }

                // Lets check the attributes
                checkTagAttributes(def,tag,doc,malformedRule,parser);
            }
        }
    }

    /**
     * Process any see tags if required
     * 
     * @param doc
     * @param comments
     * @since 2.4
     */
    private void processSeeTags(Doc doc,Comments comments) {
        RuleWithSeverity seeTags = comments.getInvalidSeeTag();
        if (seeTags.isValue()) {
            SeeTagParser seeTagParser = new SeeTagParser();
            seeTagParser.parse(doc.seeTags());

            // Add the results if any
            for (Result result:seeTagParser.getResults()) {
                result.setSeverity(seeTags.getSeverity().name());

                // Add to the stats
                theOutput.incrementStatistic(AdamRuleKeys.INVALID_SEE_TAG,
                    seeTags.getSeverity());
                theOutput.addResult(result);
            }
        }
    }

    /**
     * Check to see if the required attributes for the tag are present
     * 
     * @param def
     * @param tag
     * @param doc
     * @param rule
     * @since 2.4
     */
    private void checkTagAttributes(InternalTagletDefinition def,Tag tag,
    Doc doc,RuleWithSeverity rule,CommentParser parser) {

        if (def.getAttributes()==null) {
            // Nothing to check
            return;
        }

        TagParser tagParser = new TagParser(tag.text());

        for (Attribute attr:def.getAttributes().getAttributes()) {
            if (attr.isRequired()) {
                if (!tagParser.hasAttribute(attr.getName())) {
                    JavadocPosition tagPosition =
                        new JavadocPosition(doc,parser);
                    tagPosition.locate(tag);
                    Result result =
                        addResult(
                            doc,
                            tagPosition,
                            rule.getSeverity(),
                            "Required attribute ["+attr.getName()+
                                "] for tag ["+tag.name()+"] missing",
                            AdamRuleKeys.MALFORMED_TAG);
                    addFix(result,FixType.ADD,tag.name(),attr.getName());
                }
            }
        }
    }

    /**
     * Process the first sentence
     * 
     * @param doc to check
     * @since 2.4
     */
    private void processFirstSentence(Doc doc,Comments comments,
    CommentParser parser) {
        String firstSentence = theFSParser.parse(doc);

        if (firstSentence==null||firstSentence.isEmpty()) {
            // Well this probably an error, or will not get here..
            return;
        }

        RuleWithSeverity rule = comments.getInvalidHtmlInFirstline();

        if (rule.isValue()) {
            if (theFSParser.checkForInvalidHTML(firstSentence)) {
                String tagName = theFSParser.getInvalidTag()
                    .replaceAll("<","").replaceAll(">","");
                final Result result =
                    addResult(doc,new JavadocPosition(doc,parser),
                        rule.getSeverity(),
                        "Invalid first sentence.  Tag ["+tagName+"] ",
                        AdamRuleKeys.INVALID_FIRST_SENTENCE);

                addFix(result,FixType.REMOVE,tagName);
            }
        }
    }

    /**
     * Return true if the given Doc is deprecated.
     * 
     * @param doc the Doc to check.
     */
    private void lookForDeprecatedTags(Doc doc,CommentParser parser) {

        boolean hasDeprecatedTag = false;

        final Comments comments = theAdamConfig.getCommentSectionFor(doc);

        if (comments.getDeprecatedTagRequireDescriptions().isValue()) {
            for (final Tag tag:doc.tags("deprecated")) {
                hasDeprecatedTag = true;
                if (tag.text().trim().isEmpty()) {
                    JavadocPosition tagPosition =
                        new JavadocPosition(doc,parser);
                    tagPosition.locate("@deprecated");

                    final Result result = addResult(doc,tagPosition,comments
                        .getDeprecatedTagRequireDescriptions()
                        .getSeverity(),
                        "Deprecated tag without description",
                        AdamRuleKeys.MISSING_DEPRECATED_DESCRIPTION_KEY);

                    addFix(result,FixType.MODIFY,"deprecated");
                }
            }
        }

        if (comments.getDeprecatedAnnotationRequiresTag().isValue()) {
            if (doc instanceof ProgramElementDoc) {
                final ProgramElementDoc progDoc = (ProgramElementDoc)doc;
                final AnnotationDesc[] annotationDescList = progDoc
                    .annotations();
                for (int i = 0;i<annotationDescList.length;i++) {
                    if (annotationDescList[i].annotationType().qualifiedName()
                        .equals(java.lang.Deprecated.class.getName())) {
                        if (!hasDeprecatedTag) {
                            final Result result = addResult(doc,comments
                                .getDeprecatedAnnotationRequiresTag()
                                .getSeverity(),
                                "Deprecated tag without description",
                                AdamRuleKeys.MISSING_DEPRECATED_TAG_KEY);
                            addFix(result,FixType.ADD,"deprecated");
                        }
                    }
                }
            }
        }
    }

    /**
     * Process the {@code Adamlets}
     * 
     * @param doc to process
     * @param comments for the section
     * @since 2.0
     */
    private void
    processAdamlets(Doc doc,Comments comments,CommentParser parser) {
        final List<AdamletDefinition> adamLets = comments.getAdamlets();
        if (adamLets!=null) {
            for (final AdamletDefinition adamLet:adamLets) {
                final Adamlet runner = theAdamConfig.getAdamletFor(adamLet
                    .getValue());
                if (runner!=null) {
                    JavadocPosition pos = new JavadocPosition(doc,parser);
                    if (adamLet.getTagName()!=null) {
                        // Its just for a tag, could be many of them so process
                        // them
                        final Tag[] tags = doc.tags(adamLet.getTagName());
                        if (tags.length==0) {
                            // Nothing to do
                            continue;
                        }

                        for (final Tag tag:tags) {
                            // Set the location to the tag in question
                            pos.locate(tag);
                            runAdamlet(adamLet.getName(),runner,doc,pos,
                                tag.text(),tag.name());
                        }
                    }
                    else {
                        // Its the whole comment, so process it.
                        runAdamlet(adamLet.getName(),runner,doc,pos,
                            doc.getRawCommentText(),null);
                    }
                }
            }
        }
    }

    /**
     * Run the custom Adamlet
     * 
     * @param name of the {@code Adamlet}
     * @param runner the {@code Adamlet} to run
     * @param doc for name information
     * @param position positional information should the Adamlet return a
     * warning
     * @param comment to present to the {@code Adamlet}
     * @param tagName name of the tag, or null if its the comment
     * @since 2.0
     */
    private void runAdamlet(String name,Adamlet runner,Doc doc,
    JavadocPosition position,String comment,String tagName) {
        try {

            // Go run it
            final AdamResults results = runner.processComment(comment);

            if (results!=null) {
                for (final AdamResult adamletResult:results.getResults()) {
                    final Result result = addResult(doc,position,
                        adamletResult.getSeverity(),"Adamlet ["+name
                            +"] "+adamletResult.getMessage(),
                        AdamRuleKeys.ADAMLET_WARNINGS_KEY);
                    addFix(result,FixType.MODIFY,tagName);
                }
            }
        }
        catch (final Throwable t) {
            theRootDoc.printError("Adamlet ["+name+"] threw an exception "
                +t.getMessage());
        }
    }

    /**
     * Currently just checks to see if the name of the doc is the same as the
     * comment
     * 
     * @param doc to process
     * @since 2.0
     * @param severity
     */
    private void checkMeaninglessComment(Doc doc,Severity severity) {
        final String comment = doc.commentText().trim();

        if (comment.equalsIgnoreCase(doc.name())) {
            // Report a meaningless comment
            addResult(doc,severity,"Meaningless comment ["+comment+"]",
                AdamRuleKeys.MEANINGLESS_COMMENT_KEY);
        }
    }

    /**
     * Check to see if there is anything to warn about
     * 
     * @param doc to process
     * @param warningList the list from the comment section
     * @since 2.0
     */
    private void checkWarningList(Doc doc,List<CommentRegex> warningList,
    CommentParser parser) {

        for (final CommentRegex regex:warningList) {
            final String tagName = regex.getTagName();
            if (tagName==null) {
                // Its against the whole comment
                final Pattern pattern = getPattern(regex.getValue());
                final Matcher matcher = pattern
                    .matcher(doc.getRawCommentText());
                if (matcher.matches()) {
                    addResult(doc,regex.getSeverity(),
                        "Found ["+regex.getValue()+"] in comment",
                        AdamRuleKeys.IF_CONTAINS_KEY);
                }
            }
            else {
                // Its just for a tag
                final Tag[] tags = doc.tags(tagName);

                // There is something to do
                if (tags.length>0) {
                    final Pattern pattern = getPattern(regex.getValue());

                    for (final Tag tag:tags) {
                        final Matcher matcher = pattern.matcher(tag.text());
                        if (matcher.matches()) {
                            JavadocPosition tagPosition = new JavadocPosition(
                                doc,parser);
                            tagPosition.locate(tag);

                            Result result = addResult(doc,tagPosition,
                                regex.getSeverity(),
                                "Found ["+regex.getValue()+"] in tag "
                                    +tag.name(),
                                AdamRuleKeys.IF_CONTAINS_KEY);
                            addFix(result,FixType.MODIFY,tag.name());
                        }
                    }
                }
            }
        }
    }

    /**
     * Builds or extracts from the pattern cache a pattern
     * 
     * @param regex to find or build
     * @return the regex pattern
     * @since 2.0
     */
    private Pattern getPattern(String regex) {
        Pattern pattern = thePatternMap.get(regex);
        if (pattern==null) {
            // Lets build one and cache it
            pattern = Pattern.compile("(?s).*"+regex+".*",
                Pattern.MULTILINE|Pattern.DOTALL);
            // Cache it
            thePatternMap.put(regex,pattern);
        }

        return pattern;
    }

    /**
     * Check the parameters for the method or constructor
     * 
     * @param memberDoc to process
     * @param severity of the issue
     * @param checkDescription true if a description needs to be present for the
     * parameter
     * @since 2.0
     */
    private void paramCheck(ExecutableMemberDoc memberDoc,
    RuleWithSeverity checkDescription,CommentParser parser) {

        // Right, lets have a couple of maps so that we can cross reference tags
        // to parameters
        final HashMap<String,Parameter> paramsMap =
            new HashMap<String,Parameter>(
                memberDoc.parameters().length);
        final HashMap<String,ParamTag> paramTagMap =
            new HashMap<String,ParamTag>(
                memberDoc.paramTags().length);

        for (final Parameter parameter:memberDoc.parameters()) {
            paramsMap.put(parameter.name(),parameter);
        }

        for (final ParamTag paramTag:memberDoc.paramTags()) {
            paramTagMap.put(paramTag.parameterName(),paramTag);
        }

        // Maps loaded, lets see if the match

        for (final Parameter parameter:paramsMap.values()) {
            if (!paramTagMap.containsKey(parameter.name())) {
                final Result result = addResult(memberDoc,
                    AdamUtils.getTypeName(memberDoc)+" parameter ["
                        +parameter.name()+"] not in Javadoc",
                    AdamRuleKeys.INVALID_PARAM_KEY);
                addFix(result,FixType.ADD,"param",parameter.name());
            }
        }

        for (final ParamTag paramTag:paramTagMap.values()) {
            if (!paramsMap.containsKey(paramTag.parameterName())) {
                JavadocPosition tagPosition =
                    new JavadocPosition(memberDoc,parser);
                tagPosition.locate("@param "+paramTag.parameterName());
                final Result result = addResult(
                    memberDoc,
                    tagPosition,
                    Severity.WARNING,
                    "Javadoc has tag @param ["+paramTag.parameterName()
                        +"] which is not in "
                        +AdamUtils.getTypeName(memberDoc)
                        +" signature",AdamRuleKeys.INVALID_PARAM_KEY);
                addFix(result,FixType.REMOVE,"param",
                    paramTag.parameterName());
            }
            else {
                if (checkDescription.isValue()) {
                    final String description = paramTag.parameterComment()
                        .trim();
                    if (description.isEmpty()) {

                        // Need to see if we are checking a method and the
                        // method is a getter / setter
                        if (memberDoc.isMethod()) {
                            if (theAdamConfig
                                .isAllowNoDescriptionForGetterSetter()) {
                                // Lets see if the method name starts with get
                                // or set
                                if (memberDoc.name().startsWith("get")||
                                    memberDoc.name().startsWith("set")) {
                                    // It's all OK
                                    continue;
                                }
                            }
                        }

                        JavadocPosition tagPosition = new JavadocPosition(
                            memberDoc,parser);
                        tagPosition
                            .locate("@param "+paramTag.parameterName());

                        final Result result = addResult(
                            memberDoc,
                            tagPosition,
                            checkDescription.getSeverity(),
                            "Javadoc param tag ["
                                +paramTag.parameterName()
                                +"] has no description",
                            AdamRuleKeys.MISSING_PARAM_DESCRIPTION_KEY);
                        if (isRecommending) {
                            Parameter paramToSuggest =
                                paramsMap.get(paramTag.parameterName());
                            if (paramToSuggest!=null) {
                                addFix(result,FixType.ADD,"",
                                    theRecommender.paramPhrase(memberDoc,
                                        paramToSuggest.type()));
                            }
                        }
                        else {
                            addFix(result,FixType.MODIFY,"param");
                        }
                    }
                }
            }
        }
    }

    /**
     * Build a list of interfaces for this class
     * 
     * @param classDoc to process
     * @return a list of interfaces that the class implements
     * @since 2.0
     */
    private ArrayList<ClassDoc> buildListOfInterfaces(ClassDoc classDoc) {

        final ArrayList<ClassDoc> interfaceList = new ArrayList<ClassDoc>();

        ClassDoc currentClass = classDoc;

        // Loop around all of the super classes, finding all of the implemented
        // interfaces
        while (currentClass!=null) {
            for (final ClassDoc ifaceDoc:currentClass.interfaces()) {
                // These are the interfaces that the class implements
                interfaceList.add(ifaceDoc);

                // These are the interfaces that the interface extends
                for (final ClassDoc extendIfaceDoc:ifaceDoc.interfaces()) {
                    interfaceList.add(extendIfaceDoc);
                }

            }
            currentClass = currentClass.superclass();
        }

        return interfaceList;
    }

    /**
     * Prints a message on the Javadoc warning channel
     * 
     * @param doc holds positional information
     * @param message to display as a warning
     * @return the newly created result
     * @since 2.1
     */
    private Result addResult(Doc doc,String message,String key) {
        return addResult(doc,Severity.WARNING,message,key);
    }

    /**
     * Add a new result to the AdamOutput
     * 
     * @param doc gives name and positional information
     * @param severity of the message
     * @param message to display as a warning
     * @param ruleKey key
     * @return the newly created result
     * 
     * @since 2.2
     */
    private Result addResult(Doc doc,Severity severity,String message,
    String ruleKey) {
        return addResult(doc,doc.position(),severity,message,ruleKey);
    }

    /**
     * Add a new result to the Adam output
     * 
     * @param doc provides name information
     * @param sourcePos provides location information
     * @param severity level
     * @param message to display
     * @param ruleKey rule
     * @return the newly created result
     * @since 2.2
     */
    private Result addResult(Doc doc,SourcePosition sourcePos,
    Severity severity,String message,String ruleKey) {
        final Result result = new Result();
        final Rule rule = new Rule();
        rule.setKey(ruleKey);
        rule.setMessage(message+" ["+doc.name()+"]");

        result.setRule(rule);

        result.setSeverity(severity.name());

        if (sourcePos!=null) {
            final Position position = new Position();
            position.setColumn(sourcePos.column());
            position.setLine(sourcePos.line());
            position.setFile(sourcePos.file().getPath());
            result.setPosition(position);
        }

        // Add to the stats
        theOutput.incrementStatistic(ruleKey,severity);

        theOutput.addResult(result);

        return result;
    }

    /**
     * Add a fix to the given result.
     * 
     * Fix's enable UI's to provide auto complete or quick fixes to the issue
     * 
     * @param result
     * @param type
     * @param tagName
     * @since 2.2
     */
    private void addFix(Result result,FixType type,String tagName) {
        addFix(result,type,tagName,null);
    }

    /**
     * Add a fix with the optional description
     * 
     * @param result
     * @param type
     * @param tagName
     * @param description
     * @since 2.2
     */
    private void addFix(Result result,FixType type,String tagName,
    String description) {
        final Fix fix = new Fix();
        fix.setType(type);
        fix.setTagName(tagName);
        result.setFix(fix);

        if (description!=null) {
            fix.setDescription(description);
        }
    }

    /**
     * Print a notice, or not if isQuiet set
     * 
     * @param notice to display
     * @since 2.3
     */
    private void printNotice(String notice) {
        if (!isQuiet) {
            theRootDoc.printNotice(AdamDoclet.PRODUCT_NAME+": "+notice);
        }
    }

    /**
     * Print the result on the correct stream
     * 
     * @param result to print
     * @since 2.2
     */
    private void printResult(Result result) {
        if (!isQuiet) {
            final StringBuilder builder = new StringBuilder(100);
            builder.append(AdamDoclet.PRODUCT_NAME);
            builder.append(": ");
            builder.append(result.getSeverity());
            builder.append(": ");

            if (result.getPosition()!=null) {
                final Position pos = result.getPosition();
                builder.append(pos.getFile());
                builder.append(":");
                builder.append(pos.getLine());
            }

            builder.append(" ");
            builder.append(result.getRule().getMessage());

            final String severity = result.getSeverity().toLowerCase();

            if (severity.equals("error")) {
                theRootDoc.printError(builder.toString());
            }
            else if (severity.equals("warning")) {
                theRootDoc.printWarning(builder.toString());
            }
            else {
                theRootDoc.printNotice(builder.toString());
            }
        }
    }

    /**
     * Print a statistic to the notice stream
     * 
     * @param statistic to print
     * @since 2.2
     */
    private void printStatistic(Statistic statistic) {
        if (!isQuiet) {
            final StringBuilder builder = new StringBuilder(100);
            builder.append(AdamDoclet.PRODUCT_NAME);
            builder.append("\t");
            builder.append(statistic.getRule().getMessage());
            builder.append(" : ");
            builder.append(statistic.getValue());

            theRootDoc.printNotice(builder.toString());
        }
    }

    /**
     * Return the output processor
     * 
     * @return the adam output
     * @since 2.2
     */
    public AdamOutputProcessor getAdamOutputProcessor() {
        return theOutput;
    }
}
