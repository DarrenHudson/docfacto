package com.docfacto.adam;

import com.sun.javadoc.Doc;

/**
 * Parse a {@code Doc} and return the first sentence
 * 
 * @author dhudson - created 10 Jun 2013
 * @since 2.4
 */
public class FirstSentenceParser {

    private static final String NON_FIRST_SENTENCE_TAGS[] = {"<ul>","</ul>",
        "<ol>","</ol>",
        "<dl>","</dl>","<table>","</table>",
        "<tr>","</tr>","<td>","</td>",
        "<th>","</th>","<p>","</p>",
        "<li>","</li>","<dd>","</dd>",
        "<dir>","</dir>","<dt>","</dt>",
        "<h1>","</h1>","<h2>","</h2>",
        "<h3>","</h3>","<h4>","</h4>",
        "<h5>","</h5>","<h6>","</h6>",
        "<pre>","</pre>","<menu>","</menu>",
        "<listing>","</listing>","<hr>",
        "<blockquote>","</blockquote>",
        "<center>","</center>",
        "<UL>","</UL>","<OL>","</OL>",
        "<DL>","</DL>","<TABLE>","</TABLE>",
        "<TR>","</TR>","<TD>","</TD>",
        "<TH>","</TH>","<P>","</P>",
        "<LI>","</LI>","<DD>","</DD>",
        "<DIR>","</DIR>","<DT>","</DT>",
        "<H1>","</H1>","<H2>","</H2>",
        "<H3>","</H3>","<H4>","</H4>",
        "<H5>","</H5>","<H6>","</H6>",
        "<PRE>","</PRE>","<MENU>","</MENU>",
        "<LISTING>","</LISTING>","<HR>",
        "<BLOCKQUOTE>","</BLOCKQUOTE>",
        "<CENTER>","</CENTER>"
    };

    /**
     * The HTML sentence terminators.
     */
    private static final String[] theSentenceTerminators =
    {
        "<p>","</p>","<h1>","<h2>",
        "<h3>","<h4>","<h5>","<h6>",
        "</h1>","</h2>","</h3>","</h4>","</h5>",
        "</h6>","<hr>","<pre>","</pre>"
    };

    /**
     * Used in checkForInvalidHTML
     */
    private String theInvalidTag;

    /**
     * Create a first sentence parser, with a given locale. It is envisaged that
     * once the parser is created, then it can parse multiple times by using the
     * parse method
     * 
     * @since 2.4
     */
    public FirstSentenceParser() {

    }

    /**
     * Parse the {@code Doc} looking for the first sentence
     * 
     * @param doc to process
     * @return the first sentence
     * @since 2.4
     */
    public String parse(Doc doc) {
        return getEnglishLanguageFirstSentence(doc.commentText());
    }

    /**
     * Check to see if there are invalid HTML tags.
     * 
     * The standard HTML Javadoc process will remove these.
     * 
     * @param firstSentence to process
     * @return true if there are invalid HTML tags
     * @since 2.4
     */
    public boolean checkForInvalidHTML(String firstSentence) {
        theInvalidTag = null;
        for (String tag:NON_FIRST_SENTENCE_TAGS) {
            if (firstSentence.contains(tag)) {
                theInvalidTag = tag;
                return true;
            }
        }

        return false;
    }

    /**
     * Return the invalid tag.
     * 
     * This needs to be called after checkForInvalidHTML and may be null if
     * there wasn't any invalid tags
     * 
     * @return the invalid tag
     * @since 2.4
     */
    public String getInvalidTag() {
        return theInvalidTag;
    }

    /**
     * Return the first sentence of a string, where a sentence ends with a
     * period followed be white space.
     * 
     * @since 2.4
     */
    private String getEnglishLanguageFirstSentence(String s) {
        if (s==null) {
            return null;
        }
        int len = s.length();
        boolean period = false;
        for (int i = 0;i<len;i++) {
            switch (s.charAt(i)) {
            case '.':
                period = true;
                break;
            case ' ':
            case '\t':
            case '\n':
            case '\r':
            case '\f':
                if (period) {
                    return s.substring(0,i);
                }
                break;
            case '<':
                if (i>0) {
                    if (htmlSentenceTerminatorFound(s,i)) {
                        return s.substring(0,i);
                    }
                }
                break;
            default:
                period = false;
            }
        }
        return s;
    }

    /**
     * Find out if there is any HTML tag in the given string. If found return
     * true else return false.
     * 
     * @since 2.4
     */
    private boolean htmlSentenceTerminatorFound(String str,int index) {
        for (int i = 0;i<theSentenceTerminators.length;i++) {
            String terminator = theSentenceTerminators[i];
            if (str.regionMatches(true,index,terminator,
                0,terminator.length())) {
                return true;
            }
        }
        return false;
    }
}
