package com.docfacto.adam;

/**
 * A list of keys which are in the AdamRules.xml file.
 * 
 * This interface can be a static import
 * 
 * @author dhudson - created 30 Mar 2013
 * @since 2.2
 * @docfacto.adam ignore
 */
public interface AdamRuleKeys {

    public static final String PACKAGES_PROCESSED_KEY = "packages-processed";

    public static final String PACKAGES_IGNORED_KEY = "packages-ignored";

    public static final String MISSING_PACKAGE_DOC_KEY = "missing-package-doc";

    public static final String MISSING_PACKAGE_REQUIRED_FILE_KEY =
        "missing-package-required-file";

    public static final String CLASSES_PROCESSED_KEY = "classes-processed";

    public static final String CLASSES_IGNORED_KEY = "classes-ignored";

    public static final String CLASSES_MISSING_JAVADOC_KEY =
        "classes-missing-javadoc";
    
    public static final String CLASSES_MISSING_COMMENT_KEY = "classes-missing-comment";

    public static final String INTERFACES_PROCESSED_KEY =
        "interfaces-processed";

    public static final String INTERFACES_IGNORED_KEY = "interfaces-ignored";

    public static final String INTERFACES_MISSING_JAVADOC_KEY =
        "interfaces-missing-javadoc";

    public static final String INTERFACES_MISSING_COMMENT_KEY = "interfaces-missing-comment";
    
    public static final String ENUMS_PROCESSED_KEY = "enums-processed";

    public static final String ENUMS_IGNORED_KEY = "enums-ignored";

    public static final String ENUMS_MISSINGS_JAVADOC_KEY =
        "enums-missing-javadoc";
    
    public static final String ENUMS_MISSING_COMMENT_KEY = "enums-missing-comment";

    public static final String ENUMS_CONSTANT_MISSING_JAVADOC_KEY =
        "enums-constant-missing-javadoc";

    public static final String METHODS_PROCESSED_KEY = "methods-processed";

    public static final String METHODS_INGNORED_KEY = "methods-ignored";

    public static final String METHOD_MISSING_JAVADOC_KEY =
        "methods-missing-javadoc";

    public static final String METHOD_MISSING_COMMENT_KEY =
        "methods-missing-comment";

    public static final String INVALID_THROWS_KEY = "invalid-throws";

    public static final String INVALID_PARAM_KEY = "invalid-params";

    public static final String INVALID_RETURN_TAGS_KEY = "invalid-return";

    public static final String CONSTRUCTORS_PROCESSED_KEY =
        "constructors-processed";

    public static final String CONSTRUCTORS_IGNORED_KEY =
        "constructors-ignored";

    public static final String CONSTRUCTOR_MISSING_JAVADOC_KEY =
        "constructor-missing-javadoc";

    public static final String FIELDS_PROCESSED_KEY = "field-processed";

    public static final String FIELD_IGNORED_KEY = "field-ignored";

    public static final String MISSING_FIELD_JAVADOC_KEY =
        "field-missing-javadoc";

    public static final String MISSING_FIELD_COMMENT_KEY = "field-missing-comment";
    
    public static final String MISSING_REQUIRED_TAG = "missing-required-tag";

    public static final String MISSING_TAG_DESCRIPTION_KEY =
        "missing-tag-descriptions";

    public static final String MEANINGLESS_COMMENT_KEY = "meaningless-comment";

    public static final String MISSING_THROWS_DESCRIPTION_KEY =
        "missing-throws-description";

    public static final String MISSING_PARAM_DESCRIPTION_KEY =
        "missing-param-description";

    public static final String MISSING_RETURN_DESCRIPTION_KEY =
        "missing-return-description";

    public static final String MISSING_DEPRECATED_DESCRIPTION_KEY =
        "missing-deprecated-description";

    public static final String MISSING_DEPRECATED_TAG_KEY =
        "missing-deprecated-tag";

    public static final String INVALID_FIRST_SENTENCE =
        "invalid-first-sentence";
    
    public static final String HTML_IMBALANCE = "html-imbalance";
    
    public static final String IF_CONTAINS_KEY = "warning-if-contains";

    public static final String ADAMLET_WARNINGS_KEY = "adamlet-warning";
    
    public static final String MALFORMED_TAG = "malformed-tag";

    public static final String INVALID_SEE_TAG = "invalid-see-tag";
    
    public static final String ERRORS_KEY = "errors";

    public static final String WARNINGS_KEY = "warnings";

    public static final String INFO_KEY = "info";
}
