package com.docfacto.internal;

import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.docfacto.common.Platform;

/**
 * Delegating {@link XMLStreamWriter}.
 * 
 * Override methods in the implementation.
 * 
 * @author dhudson
 * @since 2.4
 */
abstract class DelegatingXMLStreamWriter implements XMLStreamWriter
{
    private final XMLStreamWriter theWriter;

    /**
     * Constructor.
     * 
     * @param writer
     */
    public DelegatingXMLStreamWriter(XMLStreamWriter writer)
    {
        theWriter = writer;
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeStartElement(java.lang.String)
     */
    public void writeStartElement(String localName) throws XMLStreamException
    {
        theWriter.writeStartElement(localName);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeStartElement(java.lang.String,
     * java.lang.String)
     */
    public void writeStartElement(String namespaceURI,String localName)
    throws XMLStreamException
    {
        theWriter.writeStartElement(namespaceURI,localName);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeStartElement(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    public void writeStartElement(String prefix,String localName,
    String namespaceURI) throws XMLStreamException
    {
        theWriter.writeStartElement(prefix,localName,namespaceURI);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeEmptyElement(java.lang.String,
     * java.lang.String)
     */
    public void writeEmptyElement(String namespaceURI,String localName)
    throws XMLStreamException
    {
        theWriter.writeEmptyElement(namespaceURI,localName);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeEmptyElement(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    public void writeEmptyElement(String prefix,String localName,
    String namespaceURI) throws XMLStreamException
    {
        theWriter.writeEmptyElement(prefix,localName,namespaceURI);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeEmptyElement(java.lang.String)
     */
    public void writeEmptyElement(String localName) throws XMLStreamException
    {
        theWriter.writeEmptyElement(localName);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeEndElement()
     */
    public void writeEndElement() throws XMLStreamException
    {
        theWriter.writeEndElement();
    }

    public void writeEndDocument() throws XMLStreamException
    {
        theWriter.writeEndDocument();
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#close()
     */
    public void close() throws XMLStreamException
    {
        theWriter.close();
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#flush()
     */
    public void flush() throws XMLStreamException
    {
        theWriter.flush();
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeAttribute(java.lang.String,
     * java.lang.String)
     */
    public void writeAttribute(String localName,String value)
    throws XMLStreamException
    {
        theWriter.writeAttribute(localName,value);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeAttribute(java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String)
     */
    public void writeAttribute(String prefix,String namespaceURI,
    String localName,String value)
    throws XMLStreamException
    {
        theWriter.writeAttribute(prefix,namespaceURI,localName,value);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeAttribute(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    public void
    writeAttribute(String namespaceURI,String localName,String value)
    throws XMLStreamException
    {
        theWriter.writeAttribute(namespaceURI,localName,value);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeNamespace(java.lang.String,
     * java.lang.String)
     */
    public void writeNamespace(String prefix,String namespaceURI)
    throws XMLStreamException
    {
        theWriter.writeNamespace(prefix,namespaceURI);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeDefaultNamespace(java.lang.String)
     */
    public void writeDefaultNamespace(String namespaceURI)
    throws XMLStreamException
    {
        theWriter.writeDefaultNamespace(namespaceURI);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeComment(java.lang.String)
     */
    public void writeComment(String data) throws XMLStreamException
    {
        theWriter.writeComment(data);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeProcessingInstruction(java.lang.String)
     */
    public void writeProcessingInstruction(String target)
    throws XMLStreamException
    {
        theWriter.writeProcessingInstruction(target+Platform.LINE_SEPARATOR);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeProcessingInstruction(java.lang.String,
     * java.lang.String)
     */
    public void writeProcessingInstruction(String target,String data)
    throws XMLStreamException
    {
        theWriter.writeProcessingInstruction(target,data+
            Platform.LINE_SEPARATOR);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeCData(java.lang.String)
     */
    public void writeCData(String data) throws XMLStreamException
    {
        theWriter.writeCData(data);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeDTD(java.lang.String)
     */
    public void writeDTD(String dtd) throws XMLStreamException
    {
        theWriter.writeDTD(dtd+Platform.LINE_SEPARATOR);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeEntityRef(java.lang.String)
     */
    public void writeEntityRef(String name) throws XMLStreamException
    {
        theWriter.writeEntityRef(name);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeStartDocument()
     */
    public void writeStartDocument() throws XMLStreamException
    {
        theWriter.writeStartDocument();
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeStartDocument(java.lang.String)
     */
    public void writeStartDocument(String version) throws XMLStreamException
    {
        theWriter.writeStartDocument(version);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeStartDocument(java.lang.String,
     * java.lang.String)
     */
    public void writeStartDocument(String encoding,String version)
    throws XMLStreamException
    {
        theWriter.writeStartDocument(encoding,version);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeCharacters(java.lang.String)
     */
    public void writeCharacters(String text) throws XMLStreamException
    {
        theWriter.writeCharacters(text);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeCharacters(char[], int, int)
     */
    public void writeCharacters(char[] text,int start,int len)
    throws XMLStreamException
    {
        theWriter.writeCharacters(text,start,len);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#getPrefix(java.lang.String)
     */
    public String getPrefix(String uri) throws XMLStreamException
    {
        return theWriter.getPrefix(uri);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#setPrefix(java.lang.String,
     * java.lang.String)
     */
    public void setPrefix(String prefix,String uri) throws XMLStreamException
    {
        theWriter.setPrefix(prefix,uri);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#setDefaultNamespace(java.lang.String)
     */
    public void setDefaultNamespace(String uri) throws XMLStreamException
    {
        theWriter.setDefaultNamespace(uri);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#setNamespaceContext(javax.xml.namespace.NamespaceContext)
     */
    public void setNamespaceContext(NamespaceContext context)
    throws XMLStreamException
    {
        theWriter.setNamespaceContext(context);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#getNamespaceContext()
     */
    public NamespaceContext getNamespaceContext()
    {
        return theWriter.getNamespaceContext();
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#getProperty(java.lang.String)
     */
    public Object getProperty(String name) throws IllegalArgumentException
    {
        return theWriter.getProperty(name);
    }
}