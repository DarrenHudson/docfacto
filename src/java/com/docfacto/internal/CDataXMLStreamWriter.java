package com.docfacto.internal;

import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.docfacto.common.Platform;

/**
 * JAXB the piece of crap can't handle the serialisation of CData or pretty
 * printing, so this call does both.
 * 
 * @author dhudson - created 20 Sep 2013
 * @since 2.4
 */
public class CDataXMLStreamWriter extends DelegatingXMLStreamWriter
{
    private static final Pattern XML_CHARS = Pattern.compile("[&<>]");

    private int theLevel = 0;
    private boolean wasWriteEnd;
    
    /**
     * Constructor.
     * 
     * @param del delegate
     * @since 2.4
     */
    public CDataXMLStreamWriter(XMLStreamWriter del) {
        super(del);
    }

    /**
     * @see com.docfacto.common.DelegatingXMLStreamWriter#writeCharacters(java.lang.String)
     */
    @Override
    public void writeCharacters(String text) throws XMLStreamException
    {        
        boolean useCData = XML_CHARS.matcher(text).find();
        if (useCData) {
            super.writeCData(text);
        }
        else {
            super.writeCharacters(text);
        }
        wasWriteEnd = false;
    }

    /**
     * @see com.docfacto.common.DelegatingXMLStreamWriter#writeStartElement(java.lang.String)
     */
    @Override
    public void writeStartElement(String localName) throws XMLStreamException {
        indent();
        super.writeStartElement(localName);
        wasWriteEnd = false;
    }

    /**
     * @see com.docfacto.common.DelegatingXMLStreamWriter#writeStartElement(java.lang.String, java.lang.String)
     */
    @Override
    public void writeStartElement(String namespaceURI,String localName)
    throws XMLStreamException {
        indent();
        super.writeStartElement(namespaceURI,localName);
        wasWriteEnd = false;
    }

    /**
     * @see com.docfacto.common.DelegatingXMLStreamWriter#writeStartElement(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public void writeStartElement(String prefix,String localName,
    String namespaceURI) throws XMLStreamException {
        indent();
        super.writeStartElement(prefix,localName,namespaceURI);
        wasWriteEnd = false;
    }

    /**
     * @see com.docfacto.common.DelegatingXMLStreamWriter#writeEmptyElement(java.lang.String, java.lang.String)
     */
    @Override
    public void writeEmptyElement(String namespaceURI,String localName)
    throws XMLStreamException {
        indent();
        super.writeEmptyElement(namespaceURI,localName);
        wasWriteEnd = false;
    }

    /**
     * @see com.docfacto.common.DelegatingXMLStreamWriter#writeEmptyElement(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public void writeEmptyElement(String prefix,String localName,
    String namespaceURI) throws XMLStreamException {
        indent();
        super.writeEmptyElement(prefix,localName,namespaceURI);
        wasWriteEnd = false;
    }

    /**
     * @see com.docfacto.common.DelegatingXMLStreamWriter#writeEmptyElement(java.lang.String)
     */
    @Override
    public void writeEmptyElement(String localName) throws XMLStreamException {
        indent();
        super.writeEmptyElement(localName);
        wasWriteEnd = false;
    }

    /**
     * @see com.docfacto.common.DelegatingXMLStreamWriter#writeEndElement()
     */
    @Override
    public void writeEndElement() throws XMLStreamException {
        theLevel--;
        
        if(wasWriteEnd) {
            writeIndent(theLevel);
        }
        wasWriteEnd = true;

        if(theLevel == 0) {
            newLine();
        }
        super.writeEndElement();
    }

    /**
     * @see com.docfacto.common.DelegatingXMLStreamWriter#writeComment(java.lang.String)
     */
    @Override
    public void writeComment(String data) throws XMLStreamException {
        indent();
        super.writeComment(data);
        newLine();
        wasWriteEnd = false;
    }

    /**
     * @see com.docfacto.common.DelegatingXMLStreamWriter#writeStartDocument()
     */
    @Override
    public void writeStartDocument() throws XMLStreamException {
        super.writeStartDocument();
        newLine();
    }

    /**
     * @see com.docfacto.common.DelegatingXMLStreamWriter#writeStartDocument(java.lang.String)
     */
    @Override
    public void writeStartDocument(String version) throws XMLStreamException {
        super.writeStartDocument(version);
        newLine();
    }

    /**
     * @see com.docfacto.common.DelegatingXMLStreamWriter#writeStartDocument(java.lang.String, java.lang.String)
     */
    @Override
    public void writeStartDocument(String encoding,String version)
    throws XMLStreamException {
        super.writeStartDocument(encoding,version);
        newLine();
    }

    /**
     * Write a platform dependent line separator to the stream
     * 
     * @throws XMLStreamException if unable to write to the stream
     * @since 2.4
     */
    private void newLine() throws XMLStreamException {
        super.writeCharacters(Platform.LINE_SEPARATOR);
    }

    /**
     * Indent the stream output
     * 
     * @throws XMLStreamException if unable to write to the stream
     * @since 2.4
     */
    private final void indent() throws XMLStreamException {
        writeIndent(theLevel++);
    }

    /**
     * Write spaces to the output stream to the required level
     * 
     * @param level to indent
     * @throws XMLStreamException if unable to write to the stream
     * @since 2.4
     */
    private final void writeIndent(int level) throws XMLStreamException {
        StringBuilder builder = new StringBuilder(100);
        
        if(level>0) {
            builder.append(Platform.LINE_SEPARATOR);
        }
        
        for (int i = 0;i<level;i++) {
            // write 2 spaces per level
            builder.append("  ");
        }
        
        // Bypass our regex test
        super.writeCharacters(builder.toString());
    }
}
