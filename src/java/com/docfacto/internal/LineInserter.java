package com.docfacto.internal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * This class contains the method for insert a line into a file for a given line number.
 * 
 * @author damonli - created 14 Oct 2013
 * @since 2.4.8
 */
public class LineInserter {
	
	/**
	 * Inserts a line into a file at a given line number.
	 * <p>
	 * This method creates a new temporary file and goes through the given file, writing each line into the temporary file.
	 * When the given line number is reached, the given line to be inserted will also be written into the temporary file.
	 * The given file is then deleted, and replaced with the temporary file.
	 * </p>
	 * @param file the file to insert the line into
	 * @param lineNumber the line number to insert the line into (starting from 1)
	 * @param lineToInsert the line to be inserted
	 * @throws IOException there was a problem reading/creating/editing the file
	 */
	public void insertLineIntoFileAtLineNumber(File file, int lineNumber, String lineToInsert) throws IOException {
		File tempFile = new File("tempFile.tmp");
		
		FileInputStream fileInputStream = new FileInputStream(file);
		InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		
		FileOutputStream fileOutputStream = new FileOutputStream(tempFile);
		PrintWriter printWriter = new PrintWriter(fileOutputStream);
		
		String thisLine = bufferedReader.readLine();
		int i=1;
		while(thisLine != null) {
			if (i == lineNumber) {
				printWriter.println(lineToInsert);
			}
			
			printWriter.println(thisLine);
			thisLine = bufferedReader.readLine();
			i++;
		}
		
		printWriter.flush();
		printWriter.close();
		bufferedReader.close();
		
		file.delete();
		tempFile.renameTo(file);
	}
}
