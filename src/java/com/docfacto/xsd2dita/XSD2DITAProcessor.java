/*
 * @author dhudson -
 * Created 27 Apr 2012 : 15:23:55
 */

package com.docfacto.xsd2dita;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import com.docfacto.common.DocfactoException;
import com.docfacto.dita.P;
import com.docfacto.dita.RefBody;
import com.docfacto.dita.Reference;
import com.docfacto.dita.ReferenceTopicDocument;
import com.docfacto.dita.Section;
import com.docfacto.dita.ShortDesc;
import com.docfacto.dita.SimpleTable;
import com.docfacto.dita.SimpleTableEntry;
import com.docfacto.dita.SimpleTableHeader;
import com.docfacto.dita.SimpleTableRow;
import com.docfacto.dita.Title;
import com.docfacto.dita.Xref;
import com.docfacto.licensor.UDCProcessor;
import com.docfacto.xsd2common.XSDArgumentParser;
import com.docfacto.xsdtree.AttributeTreeNode;
import com.docfacto.xsdtree.ComplexTreeNode;
import com.docfacto.xsdtree.ElementTreeNode;
import com.docfacto.xsdtree.SchemaTreeNode;
import com.docfacto.xsdtree.XSDTreeTraverser;
import com.docfacto.xsdtree.XmlRootTreeNode;
import com.sun.xml.xsom.XSSchemaSet;

/**
 * A Processor for taking XSD files and generating DITA xml file and SVG's if
 * required.
 * 
 * @author dhudson - created 28 May 2012
 * @since 1.4
 */
public class XSD2DITAProcessor {

    /**
     * Root level reference ID for xrefs
     */
    private String theReferenceID;

    /**
     * A Map of sections, so that sections are not repeated
     */
    private HashMap<String,String> theSectionMap;

    private final XSDArgumentParser theArgParser;

    /**
     * Create an instance of the XSD2DITA processor, the SVG image will not be
     * created.
     * @param parser validated argument parser
     * @since 2.0
     */
    public XSD2DITAProcessor(XSDArgumentParser parser) {
        theArgParser = parser;
    }

    /**
     * Process the SchemaSet and generate a DITA xml file from it. Also if
     * required, generated a SVG which is representative of the XSD
     * 
     * @param schemaSet to process
     * @throws DocfactoException if unable to process the schema or generate the
     * file
     * @since 2.0
     */
    public void process(XSSchemaSet schemaSet) throws DocfactoException {
        // Call the traverser
        final XSDTreeTraverser traverser = new XSDTreeTraverser(theArgParser);

        // Parse the file we are interested in
        traverser.visit(schemaSet);

        generateFile(traverser);
    }

    /**
     * Process the XSD and generate a DITA xml file from it. Also if required,
     * generated a SVG which is representative of the XSD
     * 
     * @throws DocfactoException if unable to create a traverser
     * @since 1.4
     */
    public void process() throws DocfactoException {

        // Call the traverser
        final XSDTreeTraverser traverser = new XSDTreeTraverser(theArgParser);

        // Parse the file we are interested in
        traverser.visit(new File(theArgParser.getXSD()));

        generateFile(traverser);
    }

    /**
     * Generate the DITA file and SVG if required from the XSDTree
     * 
     * @param traverser
     * @param outputDir
     * @throws DocfactoException if unable to generate the file
     * @since 2.0
     */
    private void generateFile(XSDTreeTraverser traverser)
    throws DocfactoException {

        // TODO: Check for licence file here ..
        
        UDCProcessor.registerProduct("XSD2DITA",null,null);

        theSectionMap = new HashMap<String,String>();

        final XmlRootTreeNode rootNode = traverser.getRootNode();

        final ReferenceTopicDocument refDoc = new ReferenceTopicDocument();

        final Reference reference = refDoc.getRootNode();
        theReferenceID = "reference_"+rootNode.getNodeName();

        reference.setID(theReferenceID);

        if (theArgParser.hasTitle()) {
            reference.addElement(new Title(theArgParser.getTitle()));
        }
        else {
            reference.addElement(new Title(rootNode.getNodeName()));
        }

        final String shortDesc = rootNode.getAnnotation();

        if (!shortDesc.isEmpty()) {
            reference.addElement(new ShortDesc(shortDesc));
        }

        // Create the reference body element
        final RefBody body = new RefBody();

        // Walk the schema tree
        walk((SchemaTreeNode)rootNode.getFirstChild(),body);

        // Add the body to the root node
        reference.addElement(body);

        final File outputFile = new File(theArgParser.getOutputFile());

        try {
            // save it to disk
            refDoc.save(outputFile.getPath());

            System.out.println("Generated "+
                outputFile.getPath());
        }
        catch (final IOException ex) {
            throw new DocfactoException("Unable to Generate file. ["+
                outputFile+"]",ex);
        }
    }

    /**
     * Recursive method to generate the correct <i>section</i> elements
     * 
     * @param treeNode
     * @param referenceTopic
     * @since 1.4
     */
    private void walk(SchemaTreeNode treeNode,RefBody refBody) {
        if (treeNode.isComplexTreeNode()) {
            final ComplexTreeNode complexNode = (ComplexTreeNode)treeNode;

            if (theSectionMap.containsKey(complexNode.getNodeName())) {
                // We have already produced a table for this, so no need to do
                // it again
                return;
            }

            // Make sure that we only output sections once
            theSectionMap.put(complexNode.getNodeName(),
                complexNode.getNodeName());

            final Section section = new Section();
            section.setID(complexNode.getNodeName());

            section.addElement(new Title(complexNode.getNodeName()));

            if (complexNode.hasAnnotation()) {
                section.addElement(new P(complexNode.getAnnotation()));
            }

            // Add to reference topic
            refBody.addElement(section);

            final List<AttributeTreeNode> attrs =
                complexNode.getAttributeChildren();
            if (!attrs.isEmpty()) {
                // Build a simple table of the attributes
                buildAttributeTable(attrs,section);
            }

            final SimpleTable table = new SimpleTable();

            if (complexNode.getElementChildren().size()>0) {
                // SimpleTable table = new SimpleTable();
                addTableHeaders(table);

                section.addElement(table);
            }

            for (final SchemaTreeNode child:complexNode.getSchemaChildren()) {
                if (child.isAttributeTreeNode()) {
                    // They have already been processed
                    continue;
                }

                if (child.isElementTreeNode()) {
                    final ElementTreeNode elementNode = (ElementTreeNode)child;
                    final SimpleTableRow row = new SimpleTableRow();

                    // TODO: Sort out Default..
                    row
                        .addElement(new SimpleTableEntry(elementNode
                            .getNodeName()));

                    row.addElement(new SimpleTableEntry(elementNode
                        .getTypeName()));

                    final SimpleTableEntry ann = new SimpleTableEntry("");

                    if (elementNode.getAnnotation()!=null) {
                        ann.setValue(elementNode.getAnnotation());
                    }

                    row.addElement(ann);

                    row.addTableEntry(elementNode.getRangeString());

                    table.addElement(row);
                }
                else {
                    // Complex Node
                    final ComplexTreeNode subComplexNode =
                        (ComplexTreeNode)child;
                    final SimpleTableRow row = new SimpleTableRow();
                    row.addElement(new SimpleTableEntry(subComplexNode
                        .getElementReferenceName().toString()));

                    // Add an xref to a new section
                    final Xref xref = new Xref(subComplexNode
                        .getNodeName());
                    xref.setHref("#"+theReferenceID+"/"+subComplexNode
                        .getNodeName());

                    row.addElement(new SimpleTableEntry(xref.toXML()));

                    final SimpleTableEntry ann = new SimpleTableEntry("");

                    if (subComplexNode.getAnnotation()!=null) {
                        ann.setValue(subComplexNode.getAnnotation());
                    }

                    row.addElement(ann);

                    row.addElement(new SimpleTableEntry(subComplexNode
                        .getRangeString()));

                    table.addElement(row);

                    walk(child,refBody);
                }
            }
        }
    }

    /**
     * Build attribute table
     * 
     * @param attrs
     * @param section
     * @since 1.4
     */
    private void buildAttributeTable(List<AttributeTreeNode> attrs,
    Section section) {
        section.addElement(new P("Attributes"));

        final SimpleTable table = new SimpleTable();
        final SimpleTableHeader headers = new SimpleTableHeader();
        headers.addHeaders("Name","Type","Description","Required");

        table.addElement(headers);

        SimpleTableRow row;

        for (final AttributeTreeNode node:attrs) {
            row = new SimpleTableRow();
            row.addTableEntries(node.getNodeName(),node.getTypeName(),
                node.getAnnotation(),Boolean.toString(node.isRequired()));
            table.addElement(row);
        }

        section.addElement(table);
    }

    /**
     * Add table headers
     * 
     * @param table
     * @since 1.4
     */
    private void addTableHeaders(SimpleTable table) {
        final SimpleTableHeader header = new SimpleTableHeader();
        header.addHeaders("Name","Type","Description","Range");
        table.addElement(header);
    }
}
