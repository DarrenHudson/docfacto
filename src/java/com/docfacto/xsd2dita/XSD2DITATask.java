/*
 * @author dhudson -
 * Created 11 May 2012 : 16:27:24
 */

package com.docfacto.xsd2dita;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import com.docfacto.xsd2common.XSDArgumentParser;

/**
 * The XSD2DITA Ant Task
 * 
 * @author dhudson - created 22 May 2012
 * @since 2.0
 */
public class XSD2DITATask extends Task {

    private final XSDArgumentParser theArgParser = new XSDArgumentParser();

    /**
     * File to store the resulting DITA file
     * 
     * @param output
     * @since 2.2
     */
    public void setoutput(String output) {
        theArgParser.setOutputFile(output);
    }

    /**
     * XSD to process
     * 
     * @param xsd
     * @since 2.2
     */
    public void setxsd(String xsd) {
        theArgParser.setXSD(xsd);
    }

    /**
     * Set the title
     * 
     * @param title
     * @since 2.2
     */
    public void settitle(String title) {
        theArgParser.setTitle(title);
    }

    /**
     * Root node to start from
     * 
     * @param rootnode
     * @since 2.2
     */
    public void setrootnode(String rootnode) {
        theArgParser.setRootNode(rootnode);
    }

    /**
     * Specify the root node name space
     * 
     * @param namespace to specify
     * @since 2.5
     */
    public void setnamespace(String namespace) {
        theArgParser.setNamespace(namespace);
    }
    
    /**
     * @see org.apache.tools.ant.Task#execute()
     */
    @Override
    public void execute() throws BuildException {

        final XSD2DITAProcessor xsdProcessor =
        new XSD2DITAProcessor(theArgParser);

        try {
            xsdProcessor.process();
        }
        catch (final Exception ex) {
            throw new BuildException(ex);
        }
    }
}
