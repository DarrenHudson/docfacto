/*
 * @author dhudson -
 * Created 21 Feb 2013 : 15:09:23
 */

package com.docfacto.config;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.ValidationEventLocator;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;

import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.SAXException;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.exceptions.InvalidConfigException;
import com.docfacto.internal.CDataXMLStreamWriter;
import com.sun.org.apache.xerces.internal.dom.DOMInputImpl;

/**
 * Handle most of the JAXB de-marshalling.
 * 
 * @author dhudson - created 21 Feb 2013
 * @since 2.2
 */
public abstract class BaseXmlConfig implements ValidationEventHandler,
LSResourceResolver {

    // Contexts are thread safe, so only one will do.
    private volatile JAXBContext theJAXBContext;

    private Object theJAXBObject;

    /**
     * The XML file for this config
     */
    private File theConfigFile;

    private Reader theContentReader;

    /**
     * Create a new instance of <code>BaseXmlConfig</code>.
     * 
     * @param url to load
     * @throws InvalidConfigException if unable to load the URL
     */
    public BaseXmlConfig(URL url) throws InvalidConfigException {
        theConfigFile = null;

        try {
            theContentReader = new StringReader(IOUtils.loadURL(url).trim());
            load();
        }
        catch (DocfactoException ex) {
            throw new InvalidConfigException(ex.getMessage(),ex);
        }
    }

    /**
     * Create a new instance of <code>BaseXmlConfig</code>.
     * 
     * Search the class path for the default file
     * 
     * @throws InvalidConfigException if unable to load the default name or load
     * the file
     * @since 2.2
     */
    public BaseXmlConfig() throws InvalidConfigException {
        if (getDefaultFileName()!=null) {
            try {
                final ClassLoader classLoader = IOUtils.getClassLoader();
                final URL url = classLoader.getResource(getDefaultFileName());
                if (url==null) {
                    throw new DocfactoException(
                        "Unable to locate"+getDefaultFileName()+
                            " on the class path");
                }
                theConfigFile = new File(url.getFile());
                theContentReader =
                    new StringReader(IOUtils.loadURL(url).trim());
                load();
            }
            catch (final Throwable t) {
                throw new InvalidConfigException(
                    "Unable to load "+getDefaultFileName(),t);
            }
        }
    }

    /**
     * Constructor.
     * 
     * @param configFile to process
     * @throws InvalidConfigException if unable to load the XML file
     */
    public BaseXmlConfig(File configFile) throws InvalidConfigException {
        theConfigFile = configFile;
        try {
            theContentReader =
                new StringReader(IOUtils.readFileAsString(configFile).trim());
            load();
        }
        catch (DocfactoException ex) {
            throw new InvalidConfigException("Unable to load config file ["+
                configFile.getPath()+"]",ex);
        }
    }

    /**
     * Constructor.
     * 
     * @param configFilePath the path of the Config XML file including the
     * filename
     * @throws InvalidConfigException if unable to load the XML file
     */
    public BaseXmlConfig(String configFilePath) throws InvalidConfigException {
        this(new File(configFilePath));
    }

    /**
     * Load the given XML config file
     * 
     * @throws InvalidConfigException if unable to load the XML file
     * @since 2.0
     */
    protected void load() throws InvalidConfigException {

        try {

            Unmarshaller unmarshaller = getUnmarshaller();
            
            final StreamSource xsdSource =
                new StreamSource(
                    IOUtils.getResourceAsStream(getXSDLocation()+
                        getXSDName()));

            final StreamSource[] sources = {xsdSource};

            final SchemaFactory factory =
                SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

            factory.setResourceResolver(this);

            unmarshaller.setEventHandler(this);
            unmarshaller.setSchema(factory.newSchema(sources));

            theJAXBObject = unmarshaller.unmarshal(theContentReader);

            postLoad();
        }
        catch (DocfactoException ex) {
            throw new InvalidConfigException(
                "Unable to load resource as stream",ex);
        }
        catch (final JAXBException ex) {
            if (ex.getLinkedException()!=null) {
                if (theConfigFile!=null) {
                    throw new InvalidConfigException(
                        "Unable to load config file ["+
                            theConfigFile.getPath()+"] for reason "+
                            ex.getLinkedException().getMessage(),
                        ex.getLinkedException());
                }
                throw new InvalidConfigException(
                    "Unable to load resource for reason "+
                        ex.getLinkedException().getMessage(),
                    ex.getLinkedException());
            }
            if (theConfigFile!=null) {
                throw new InvalidConfigException(
                    "Unable to load config file ["+
                        theConfigFile.getPath()+"] for reason "+ex.getMessage(),
                    ex);
            }
            else {
                throw new InvalidConfigException(
                    "Unable to load resource for reason "+ex.getMessage(),ex);
            }
        }
        catch (final SAXException ex) {
            if (theConfigFile!=null) {
                throw new InvalidConfigException(
                    "Unable to load config file ["+
                        theConfigFile.getPath()+"] for reason "+ex.getMessage(),
                    ex);
            }
            else {
                throw new InvalidConfigException(
                    "Unable to load resource for reason "+ex.getMessage(),ex);
            }
        }
    }

    /**
     * Return a unmarshaller for the ObjectFactory
     * 
     * {@docfacto.system This needs to be done, as the JavaDoc taglet engine doesn't
     *  extend the jar file properly }
     *  
     * @return a JAXB Unmarshaller
     * @throws JAXBException if unable to create a marshaller
     * @since 2.5
     */
    Unmarshaller getUnmarshaller() throws JAXBException {
        theJAXBContext = getJAXBContext();

        return theJAXBContext.createUnmarshaller();
    }

    /**
     * Save the config file
     * 
     * @throws DocfactoException if unable to save the file
     * @since 2.1
     */
    public void save() throws DocfactoException {

        if (theConfigFile==null) {
            // If loaded from a stream, then this isn't going to work
            return;
        }

        try {
            // Lets give it a shot..
            final Marshaller marshaller = getMarshaller();

            XMLOutputFactory xof = XMLOutputFactory.newInstance();
            XMLStreamWriter streamWriter =
                xof.createXMLStreamWriter(new FileOutputStream(theConfigFile));

            // Pretty printing and handles CData correctly
            CDataXMLStreamWriter cdataStreamWriter =
                new CDataXMLStreamWriter(streamWriter);

            marshaller.marshal(theJAXBObject,cdataStreamWriter);

            cdataStreamWriter.flush();
            cdataStreamWriter.close();
        }
        catch (Exception ex) {
            throw new DocfactoException("Unable to save xmlfile "+
                theConfigFile+" due to ",
                ex);
        }
    }

    /**
     * Marshal the properties to to the specified file in XML format
     * 
     * @param filename to generate
     * @throws DocfactoException if unable to save the exception
     * @since 2.2
     */
    public void save(String filename) throws DocfactoException {
        theConfigFile = new File(filename);
        save();
    }

    /**
     * Marshal the properties to the specified file in XML format
     * 
     * @param file to save the XML for to
     * @throws DocfactoException if unable to save the file
     * @since 2.2
     */
    public void save(File file) throws DocfactoException {
        theConfigFile = file;
        save();
    }

    /**
     * Fetch the loaded properties
     * 
     * @return the loaded properties, or null
     * @since 2.2
     */
    public Object getProperties() {
        return theJAXBObject;
    }

    /**
     * Set the properties
     * 
     * @param properties
     * @since 2.2
     */
    public void setProperties(Object properties) {
        theJAXBObject = properties;
    }

    /**
     * @see javax.xml.bind.ValidationEventHandler#handleEvent(javax.xml.bind.ValidationEvent)
     */
    @Override
    public boolean handleEvent(ValidationEvent validationEvent) {
        final StringBuilder message = new StringBuilder(100);
        message.append("Validation Error ");
        if (theConfigFile!=null) {
            message.append("with ");
            message.append(theConfigFile.getPath());
        }

        final ValidationEventLocator vel = validationEvent.getLocator();
        message.append(" Line: ");
        message.append(vel.getLineNumber());
        message.append(" Col: ");
        message.append(vel.getColumnNumber());
        message.append(" : ");
        message.append(validationEvent.getMessage());

        System.err.println(message.toString());

        // Stop processing, one issue is enough
        return false;
    }

    private JAXBContext getJAXBContext() throws JAXBException {
        if (theJAXBContext==null) {
            theJAXBContext =
                JAXBContext.newInstance(getObjectFactoryPackageName(),
                    IOUtils.getClassLoader());
        }

        return theJAXBContext;
    }

    /**
     * Fetch a new marshaller
     * 
     * @throws JAXBException If unable to create a marshaller
     * @return a newly created marshaller
     * @since 2.2
     */
    public Marshaller getMarshaller() throws JAXBException {
        return getJAXBContext().createMarshaller();
    }

    /**
     * Load the schema required.
     * 
     * @param schema to load, for example Links.xsd
     * @return the required schema
     * @throws DocfactoException if unable to find the schema
     * @since 2.2
     */
    public InputStream getSchemaFor(String schema) throws DocfactoException {
        return IOUtils.getResourceAsStream(getXSDLocation()+schema);
    }

    /**
     * @see org.w3c.dom.ls.LSResourceResolver#resolveResource(java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public LSInput resolveResource(String type,String namespaceURI,
    String publicId,String systemId,String baseURI) {
        final LSInput input = new DOMInputImpl();
        try {
            input.setByteStream(IOUtils
                .getResourceAsStream(getXSDLocation()+systemId));
        }
        catch (final DocfactoException ex) {
            System.err
                .println("XmlConfig:resolveResource: Unable to locate ..["+
                    systemId+"] "+ex.getMessage());
        }
        return input;
    }

    /**
     * Implementers need to provide the XSD name here, for example Docfacto.xsd
     * 
     * @return the name of the XSD
     * @since 2.2
     */
    public abstract String getXSDName();

    /**
     * Return the location of the XSD
     * 
     * @return the location of the XSD
     * @since 2.2
     */
    public abstract String getXSDLocation();

    /**
     * Return the default file name. If this is not null then this file will be
     * searched on the class path and if found loaded.
     * 
     * @return the default filename to look on the class path for
     * @since 2.2
     */
    public abstract String getDefaultFileName();

    /**
     * Notification when load is complete
     * 
     * @since 2.2
     */
    protected void postLoad() {
    }

    /**
     * Return the package name, in dot notation of where the ObjectFactory lives
     * 
     * @return the package name of the {@code ObjectFactory}
     * @since 2.2
     */
    public abstract String getObjectFactoryPackageName();
}
