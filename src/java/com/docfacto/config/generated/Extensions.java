//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.01.20 at 10:08:33 AM GMT 
//


package com.docfacto.config.generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A list of extension classes
 * 			
 * 
 * <p>Java class for extensions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="extensions">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="scanner" type="{}scanner" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="uri-handler" type="{}uri-handler" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "extensions", propOrder = {
    "scanners",
    "uriHandlers"
})
public class Extensions {

    @XmlElement(name = "scanner")
    protected List<Scanner> scanners;
    @XmlElement(name = "uri-handler")
    protected List<UriHandler> uriHandlers;

    /**
     * Gets the value of the scanners property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the scanners property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getScanners().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Scanner }
     * 
     * 
     */
    public List<Scanner> getScanners() {
        if (scanners == null) {
            scanners = new ArrayList<Scanner>();
        }
        return this.scanners;
    }

    /**
     * Gets the value of the uriHandlers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the uriHandlers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUriHandlers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UriHandler }
     * 
     * 
     */
    public List<UriHandler> getUriHandlers() {
        if (uriHandlers == null) {
            uriHandlers = new ArrayList<UriHandler>();
        }
        return this.uriHandlers;
    }

}
