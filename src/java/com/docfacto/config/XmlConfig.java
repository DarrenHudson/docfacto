/*
 * @author dhudson -
 * Created 2 Jul 2012 : 16:04:22
 */

package com.docfacto.config;

import java.io.File;
import java.net.URL;

import javax.xml.bind.JAXBException;

import com.docfacto.common.DocfactoException;
import com.docfacto.config.generated.Adam;
import com.docfacto.config.generated.Docfacto;
import com.docfacto.config.generated.Licence;
import com.docfacto.config.generated.Links;
import com.docfacto.config.generated.Product;
import com.docfacto.config.generated.Taglets;
import com.docfacto.exceptions.InvalidConfigException;
import com.docfacto.licensor.CryptoFactory;
import com.docfacto.licensor.Products;

/**
 * Wrapper class for the configuration of Docfacto. Uses JAXB to load the config
 * file and validate it against the shipped XSD. The Shipped XSD is in the jar
 * file
 * 
 * @author dhudson - created 2 Jul 2012
 * @since 2.0
 */
public class XmlConfig extends BaseXmlConfig {

    private static final String JAXB_XSD_LOCATION =
        "com/docfacto/config/resources/";

    /**
     * JAXB Package name constant {@value}
     */
    private static final String JAXB_PACKAGE_NAME =
        "com.docfacto.config.generated";

    /**
     * Name of the validating XSD {@value}
     */
    private static final String XSD_NAME =
        "Docfacto.xsd";

    /**
     * The product release number
     */
    private String theProductReleaseNumber;

    /**
     * {@docfacto.system We could just set the key from the loaded file here,
     * but I am sure dev's will try and change the values of the licence }
     */
    private Licence theCryptedLicence;
    private Licence theClearLicence;

    /**
     * Constructor.
     * 
     * @param url to process
     * @throws InvalidConfigException if unable to load the URL
     * @since 2.4
     */
    public XmlConfig(URL url) throws InvalidConfigException {
        super(url);
    }

    /**
     * Constructor.
     * 
     * @param configFile to process
     * @throws InvalidConfigException if unable to load the XML file
     */
    public XmlConfig(File configFile) throws InvalidConfigException {
        super(configFile);
    }

    /**
     * Constructor.
     * 
     * @param configFilePath the path of the Config XML file including the
     * filename
     * @throws InvalidConfigException if unable to load the XML file
     */
    public XmlConfig(String configFilePath) throws InvalidConfigException {
        super(configFilePath);
    }

    /**
     * Look for Docfacto.xml on the class path and load it.
     * 
     * @throws InvalidConfigException if unable to find the xml, or load it
     */
    public XmlConfig() throws InvalidConfigException {
        super();
    }

    /**
     * Gets the product release number in major.minor_build format
     * 
     * @return the product release number
     * @since 2.1
     */
    public String getProductRelease() {
        return theProductReleaseNumber;
    }

    /**
     * Return the Docfacto configuration
     * 
     * @return the unmarshalled configuration file
     * @since 2.0
     */
    public Docfacto getConfig() {
        return (Docfacto)getProperties();
    }

    /**
     * Returns the {@code Adam} part of the config
     * 
     * @return the adam config, or null, if not present
     * @since 2.0
     */
    public Adam getAdamConfig() {
        if (getProperties()==null) {
            return null;
        }

        return getConfig().getAdam();
    }

    /**
     * Returns the {@code Taglets} part of the config
     * 
     * @return the taglets config, or null, if not present
     * @since 2.0
     */
    public Taglets getTagletsConfig() {
        if (getProperties()==null) {
            return null;
        }

        return getConfig().getTaglets();
    }

    /**
     * Returns {@code licence} section of the XML Config
     * 
     * @return the {@code licence} section of the XML Config
     * @since 2.0
     */
    public Licence getLicenseConfig() {
        if (getProperties()==null) {
            return null;
        }

        return getConfig().getLicence();
    }

    /**
     * Returns the {@code links} section of the config file
     * 
     * @return the links section of the XML config file
     * @since 2.1
     */
    public Links getLinksConfig() {
        if (getProperties()==null) {
            return null;
        }

        return getConfig().getLinks();
    }

    /**
     * Load the given XML config file
     * 
     * @throws InvalidConfigException if unable to load the XML file
     * @since 2.0
     */
    @Override
    protected void load() throws InvalidConfigException {
        super.load();

        try {
            final InternalProperties props = new InternalProperties();
            theProductReleaseNumber = props.getReleaseString();
        }
        catch (DocfactoException ex) {
            throw new InvalidConfigException(
                "Unable to load internal properties",ex);
        }
    }

    /**
     * @see com.docfacto.config.BaseXmlConfig#postLoad()
     */
    @Override
    protected void postLoad() {
        // Change the false license with the correct one from the key
        try {
            theCryptedLicence = getLicenseConfig();

            theClearLicence =
                CryptoFactory.decryptKey(getLicenseConfig().getKey(),
                    getUnmarshaller());
            getConfig().setLicence(theClearLicence);
        }
        catch (JAXBException ex) {
            System.err.println("Unable to create unmarshaller.."+ex);
        }
    }

    /**
     * @see com.docfacto.config.BaseXmlConfig#save()
     */
    @Override
    public void save() throws DocfactoException {
        // Swap the crypted licence back out
        getConfig().setLicence(theCryptedLicence);
        super.save();
        getConfig().setLicence(theClearLicence);
    }

    /**
     * @see com.docfacto.config.BaseXmlConfig#getXSDName()
     */
    @Override
    public String getXSDName() {
        return XSD_NAME;
    }

    /**
     * @see com.docfacto.config.BaseXmlConfig#getDefaultFileName()
     */
    @Override
    public String getDefaultFileName() {
        return "Docfacto.xml";
    }

    /**
     * @see com.docfacto.config.BaseXmlConfig#getXSDLocation()
     */
    @Override
    public String getXSDLocation() {
        return JAXB_XSD_LOCATION;
    }

    /**
     * @see com.docfacto.config.BaseXmlConfig#getObjectFactoryPackageName()
     */
    @Override
    public String getObjectFactoryPackageName() {
        return JAXB_PACKAGE_NAME;
    }

    /**
     * Return the product section of the licence config
     * 
     * @param product required
     * @return the product licence details
     * @since 2.5
     */
    public Product getProduct(Products product) {
        for (Product internalProduct:getLicenseConfig().getProducts()
            .getProducts()) {
            if (internalProduct.getName().equalsIgnoreCase(product.toString())) {
                return internalProduct;
            }
        }

        return null;
    }
}
