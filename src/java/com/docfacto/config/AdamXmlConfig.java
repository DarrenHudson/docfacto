/*
 * @author dhudson -
 * Created 5 Jul 2012 : 11:17:15
 */

package com.docfacto.config;

import java.util.HashMap;
import java.util.List;

import com.docfacto.adam.Adamlet;
import com.docfacto.config.generated.Adam;
import com.docfacto.config.generated.AdamletDefinition;
import com.docfacto.config.generated.Comments;
import com.docfacto.config.generated.Enums;
import com.docfacto.config.generated.RequiredDocumentation;
import com.docfacto.config.generated.RequiredTags;
import com.docfacto.exceptions.InvalidConfigException;
import com.sun.javadoc.Doc;
import com.sun.javadoc.PackageDoc;
import com.sun.javadoc.RootDoc;

/**
 * A Wrapper class around the <code>adam</code> section of Docfacto.xml.
 * 
 * @author dhudson - created 5 Jul 2012
 * @since 2.0
 */
public class AdamXmlConfig {

    /**
     * Holds the {@code Adam} section of the config
     */
    private final Adam theConfig;

    /**
     * Holds a cache of Adamlets loaded from the config. The key is the class
     * name
     */
    private HashMap<String,Adamlet> theAdamletCache;

    /**
     * A map of failed to load AdamLets, so Adam only grumbles once
     */
    private HashMap<String,String> theFailedAdamlets;

    /**
     * Need a reference to the RootDoc for warning processing
     */
    private RootDoc theRootDoc;

    /**
     * Parse the Docfacto.xml and see what the user has set.
     * 
     * @param configXmlPath
     * @throws InvalidConfigException if unable to load the config file
     * @since 2.2
     */
    public AdamXmlConfig(String configXmlPath) throws InvalidConfigException {
        XmlConfig xmlConfig = new XmlConfig(configXmlPath);
        theConfig = xmlConfig.getAdamConfig();
        
        processConfig();
    }
    
    /**
     * Constructor.
     * 
     * @param adam config to process
     * @param rootDoc from the JavaDoc processor
     */
    public AdamXmlConfig(final Adam adam,final RootDoc rootDoc) {
        theConfig = adam;
        theRootDoc = rootDoc;

        theAdamletCache = new HashMap<String,Adamlet>();
        theFailedAdamlets = new HashMap<String,String>();

        // Sort thy self out
        processConfig();
        
        // Load Adamlets
        loadAdamlets();
    }

    /**
     * Process the Adam section of the Xml Config, and work out what the rules
     * are. Load any AdamLets if they are defined
     * 
     * Sections like methods, fields extend all, so if in all comment-required =
     * true, but in fields comment required-false the fields will have
     * comments-required set to false
     * 
     * @since 2.0
     */
    private void processConfig() {
        final Comments allComments = theConfig.getAll().getComments();

        if (allComments!=null) {
            if (allComments.getCommentRequired().isValue()) {
                // Need to push these values to the other comments, the comments
                // may not exist

                Comments comments = theConfig.getPackages().getComments();
                // Set the comments to be the same as all
                if (comments==null) {
                    theConfig.getPackages().setComments(allComments);
                }

                comments = theConfig.getClasses().getComments();
                if (comments==null) {
                    theConfig.getClasses().setComments(allComments);
                }

                comments = theConfig.getEnums().getComments();
                if(comments == null) {
                    theConfig.getEnums().setComments(allComments);
                }

                comments  = theConfig.getInterfaces().getComments();
                if(comments == null) {
                    theConfig.getInterfaces().setComments(allComments);
                }

                comments = theConfig.getConstructors().getComments();
                if (comments==null) {
                    theConfig.getConstructors().setComments(allComments);
                }

                comments = theConfig.getMethods().getComments();
                if (comments==null) {
                    theConfig.getMethods().setComments(allComments);
                }

                comments = theConfig.getFields().getComments();
                if (comments==null) {
                    theConfig.getFields().setComments(allComments);
                }
            }
        }
    }

    /**
     * Load the Adamlets defined in the Docfacto.xml
     * 
     * @since 2.2
     */
    private void loadAdamlets() {
        // Lets see if there are any Adamlets defined, and if so make sure that
        // we can load and cache them
        loadAdamlets(theConfig.getAll().getComments().getAdamlets());
        loadAdamlets(theConfig.getPackages().getComments().getAdamlets());
        loadAdamlets(theConfig.getClasses().getComments().getAdamlets());
        loadAdamlets(theConfig.getEnums().getComments().getAdamlets());
        loadAdamlets(theConfig.getInterfaces().getComments().getAdamlets());
        loadAdamlets(theConfig.getConstructors().getComments().getAdamlets());
        loadAdamlets(theConfig.getMethods().getComments().getAdamlets());
        loadAdamlets(theConfig.getFields().getComments().getAdamlets());       
    }
    
    /**
     * Try and load the Adamlets, if loaded cache them, else issue a warning
     * 
     * @param adamlets to process
     * @since 2.0
     */
    private void loadAdamlets(List<AdamletDefinition> adamlets) {

        if (adamlets==null) {
            return;
        }

        for (final AdamletDefinition littleAdam:adamlets) {
            // The value is the class name
            final String className = littleAdam.getValue();

            if (theFailedAdamlets.containsKey(className)) {
                // Failed to load this one before, so skip it
                continue;
            }

            if (theAdamletCache.containsKey(className)) {
                // Already loaded
                continue;
            }
            try {
                final Adamlet loadedAdamlet =
                (Adamlet)Class.forName(className).newInstance();

                // OK we loaded OK, so cache it
                theAdamletCache.put(className,loadedAdamlet);
            }
            catch (final Throwable t) {
                theRootDoc.printError("Unable to load Adamlet ["+
                littleAdam.getName()+"] "+t);
                theFailedAdamlets.put(className,className);
            }
        }
    }

    /**
     * Return the comments section from the method section
     * 
     * @return comments from the method section
     * @since 2.0
     */
    public Comments getMethodComments() {
        return theConfig.getMethods().getComments();
    }

    /**
     * Return the comments from the field section
     * 
     * @return comments from the field section
     * @since 2.0
     */
    public Comments getFieldComments() {
        return theConfig.getFields().getComments();
    }

    /**
     * Return the comments from the constructor section
     * 
     * @return comments from the constructor section
     * @since 2.0
     */
    public Comments getConstructorComments() {
        return theConfig.getConstructors().getComments();
    }

    /**
     * Return the comments from the class section
     * 
     * @return comments from the class section
     * @since 2.0
     */
    public Comments getClassComments() {
        return theConfig.getClasses().getComments();
    }

    /**
     * Return the comments from the package section
     * 
     * @return comments from the package section
     * @since 2.0
     */
    public Comments getPackageComments() {
        return theConfig.getPackages().getComments();
    }

    /**
     * Return the comments from the all section
     * 
     * @return comments from the all section
     * @since 1.0
     */
    public RequiredTags getAllRequiredTags() {
        return theConfig.getAll().getRequiredTags();
    }

    /**
     * Helper method to retrieve the correct required tags section
     * 
     * @param doc to process
     * @return required tags from the correct config section
     * @since 2.0
     */
    public RequiredTags getRequiredTagsForDoc(Doc doc) {
        if (doc.isMethod()) {
            return theConfig.getMethods().getRequiredTags();
        }

        if (doc.isField()) {
            return theConfig.getFields().getRequiredTags();
        }

        if (doc.isConstructor()) {
            return theConfig.getConstructors().getRequiredTags();
        }

        if (doc.isClass()) {
            return theConfig.getClasses().getRequiredTags();
        }

        if(doc.isEnum()) {
            return theConfig.getEnums().getRequiredTags();
        }

        if(doc.isIncluded()) {
            return theConfig.getInterfaces().getRequiredTags();
        }

        if (doc instanceof PackageDoc) {
            return theConfig.getPackages().getRequiredTags();
        }

        return null;
    }

    /**
     * Return the comments section for a particular doc type
     *
     * @param doc to process
     * @return the comments section or null if not found, unlikely
     * @since 2.2
     */
    public Comments getCommentSectionFor(Doc doc) {
        if (doc.isMethod()) {
            return theConfig.getMethods().getComments();
        }

        if (doc.isField()) {
            return theConfig.getFields().getComments();
        }

        if (doc.isConstructor()) {
            return theConfig.getConstructors().getComments();
        }

        if (doc.isClass()) {
            return theConfig.getClasses().getComments();
        }

        if(doc.isEnum()) {
            return theConfig.getEnums().getComments();
        }

        if(doc.isIncluded()) {
            return theConfig.getInterfaces().getComments();
        }

        if (doc instanceof PackageDoc) {
            return theConfig.getPackages().getComments();
        }

        return null;
    }

    /**
     * Get the required package files
     *
     * @return Required package files, or null
     * @since 2.2
     */
    public RequiredDocumentation getRequiredPackageFiles() {
        if(theConfig.getPackages() != null) {
            return theConfig.getPackages().getRequiredDocumentation();
        }

        return null;
    }

    /**
     * Are the statistics required?
     * 
     * @return true, if the statistical output is required
     * @since 2.0
     */
    public boolean isStatisticsOutput() {
        return theConfig.isStatisticsOutput();
    }

    /**
     * Checks to see if <code>allow-no-comments-for-interface-methods</code> has
     * been set to true
     * 
     * @return true if this has been set in the config
     * @since 2.0
     */
    public boolean isAllowingNoCommentsForInterfaceMethods() {
        return theConfig.getMethods().getAllowNoCommentsForInterfaceMethods().isValue();
    }

    /**
     * Check to see if getters and setters need comments
     * 
     * @return true if this has been set in the config
     * @since 2.4
     */
    public boolean isAllowNoDescriptionForGetterSetter() {
        return theConfig.getMethods().getAllowNoDescriptionForGetterSetter().isValue();
    }
    
    /**
     * Return the Enum section of the config
     *
     * @return the Enum section of the config
     * @since 2.2
     */
    public Enums getEnumsSection() {
        return theConfig.getEnums();
    }

    /**
     * Fetch the pre-loaded {@code Adamlet}
     * 
     * @param className of the {@code Adamlet}
     * @return the loaded {@code Adamlet}, or null if failed to load
     * @since 2.0
     */
    public Adamlet getAdamletFor(String className) {
        return theAdamletCache.get(className);
    }
}
