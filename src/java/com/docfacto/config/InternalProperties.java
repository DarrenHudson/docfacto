/*
 * @author dhudson -
 * Created 23 Oct 2012 : 10:46:48
 */

package com.docfacto.config;

import java.io.InputStream;
import java.util.Properties;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;

/**
 * Simple wrapper class around the internal.properties
 * 
 * @author dhudson - created 23 Oct 2012
 * @since 2.1
 */
public class InternalProperties {

    private static final String PRODUCT_MAJOR_KEY = "product.major";

    private static final String PRODUCT_MINOR_KEY = "product.minor";

    private static final String PRODUCT_BUILD_KEY = "product.build";

    /**
     * Release version from the internal.properties file
     */
    private static String theProductRelease;

    /**
     * Location of internal.properties {@value}
     */
    private static final String INTERNAL_PROPERTIES =
    "com/docfacto/config/resources/internal.properties";

    /**
     * Create a new instance of <code>InternalProperties</code>.
     * 
     * @throws DocfactoException if unable to load the internal.properties file
     */
    public InternalProperties() throws DocfactoException {

        try {
            final ClassLoader classLoader =
            IOUtils.getClassLoader();
            // Load the internal properties file
            final InputStream is =
            classLoader.getResourceAsStream(INTERNAL_PROPERTIES);
            final Properties props = new Properties();
            props.load(is);
            buildReleaseString(props);
        }
        catch (final Throwable t) {
            throw new DocfactoException("Unable to load internal.properties",t);
        }
    }

    /**
     * From the internal properties, build the release string
     * 
     * @param props internal.properties
     * @since 2.1
     */
    private void buildReleaseString(Properties props) {
        final StringBuilder builder = new StringBuilder(100);
        builder.append(props.getProperty(PRODUCT_MAJOR_KEY));
        builder.append(".");
        builder.append(props.getProperty(PRODUCT_MINOR_KEY));
        builder.append("_");
        builder.append(props.getProperty(PRODUCT_BUILD_KEY));

        theProductRelease = builder.toString();
    }

    /**
     * Returns the product release string as major.minor_build
     * 
     * @return the product release string
     * @since 2.1
     */
    public String getReleaseString() {
        return theProductRelease;
    }
}
