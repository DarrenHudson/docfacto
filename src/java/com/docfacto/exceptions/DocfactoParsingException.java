package com.docfacto.exceptions;

import com.docfacto.common.DocfactoException;

/**
 * General Parsing exception
 * 
 * @author dhudson - created 11 Jun 2013
 * @since 2.4
 */
public class DocfactoParsingException extends DocfactoException {

    /**
     * {@value}
     */
    private static final long serialVersionUID = 8769583792200542922L;

    /**
     * Constructor.
     * 
     * @param message error message
     * @since 2.4
     */
    public DocfactoParsingException(String message) {
        super(message);
    }

    /**
     * Constructor.
     * 
     * @param message error message
     * @param t throwable
     * @since 2.4
     */
    public DocfactoParsingException(String message,Throwable t) {
        super(message,t);
    }
}
