package com.docfacto.exceptions;

import com.docfacto.common.DocfactoException;

/**
 * This exception is thrown is the Docfacto.xml is invalid
 * 
 * @author dhudson - created 11 Jun 2013
 * @since 2.4
 */
public class InvalidConfigException extends DocfactoException {

    /**
     * {@value}
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor.
     * 
     * @param message error message
     * @since 2.4
     */
    public InvalidConfigException(String message) {
        super(message);
    }
    
    /**
     * Constructor.
     * 
     * @param message error message
     * @param ex exception
     * @since 2.4
     */
    public InvalidConfigException(String message, Throwable ex) {
        super(message,ex);
    }
}
