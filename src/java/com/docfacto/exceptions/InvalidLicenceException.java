package com.docfacto.exceptions;

import com.docfacto.common.DocfactoException;

/**
 * This exception is thrown if the licence is invalid
 *
 * @author dhudson - created 11 Jun 2013
 * @since 2.4
 */
public class InvalidLicenceException extends DocfactoException {

    /**
     * {@value}
     */
    private static final long serialVersionUID = -1292614672166723035L;

    /**
     * Constructor.
     * 
     * @param message error message
     * @since 2.4
     */
    public InvalidLicenceException(String message) {
        super(message);
    }
    
    /**
     * Constructor.
     * @param message error message
     * @param ex exception
     * @since 2.4
     */
    public InvalidLicenceException(String message, Throwable ex) {
        super(message, ex);
    }
}
