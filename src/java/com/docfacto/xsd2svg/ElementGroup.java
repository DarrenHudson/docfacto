/* 
 * @author dhudson - 
 * Created 28 May 2012 : 07:27:39
 */

package com.docfacto.xsd2svg;

import com.docfacto.common.StringUtils;
import com.docfacto.svg.Rect;
import com.docfacto.svg.Text;
import com.docfacto.svg.Text.Alignment;
import com.docfacto.svg.Title;
import com.docfacto.svg.Tspan;
import com.docfacto.xsdtree.ElementTreeNode;

/**
 * Class that represents a XML Element in SVG notation.
 * 
 * @author dhudson - created 28 May 2012
 * @since 2.0
 */
public class ElementGroup extends SVGGroup {

    private final ElementTreeNode theElementNode;

    /**
     * Constructor.
     * 
     * @param node
     */
    public ElementGroup(ElementTreeNode node) {
        super(node);
        theElementNode = node;
    }

    /**
     * @see com.docfacto.xsd2svg.SVGGroup#generateSVGGroup()
     */
    public void generateSVGGroup() {

        Rect rect = getRect();

        rect.setHeight(ELEMENT_ATTRIBUTE_BOX_HEIGHT);
        rect.setWidth(ELEMENT_ATTRIBUTE_BOX_WIDTH); // 195
        rect.setY(theSVGYOffset);
        rect.setX(ELEMENT_ATTRIBUTE_BOX_LEFT_PADDING);

        rect.setClass("element");

        rect.setID(calculateID(theElementNode));
        rect.addElement(new Title(calculateID(theElementNode)));

        // Element name box
        Text elName = new Text();

        // TODO: How do we get this feature documented?
        if (theElementNode.hasAnnotation()) {
            // Add a tspan element for text and a title element for tooltip
            elName.addElement(new Tspan(theElementNode.getNodeName()));
            Title title = new Title(theElementNode.getAnnotation());
            elName.addElement(title);
        }
        else {
            elName.setValue(theElementNode.getNodeName());
        }

        // Sets the rest of the attributes for the box
        elName.setX(60);
        elName.setY(theSVGYOffset+15);
        // elName.setStyle(Style.ITALIC);

        // Add red text if the node is required
        if (theElementNode.isRequired()) {
            elName.addStyleAttribute("fill:red");
        }

        // Type text
        Text type = new Text();
        type.setAlignment(Alignment.END);
        type.setX(ELEMENT_ATTRIBUTE_BOX_WIDTH-
            ELEMENT_ATTRIBUTE_TYPE_RIGHT_PADDING);
        type.setY(theSVGYOffset+15);

        type.addElement(new Tspan(theElementNode.getTypeName()));

        if (theElementNode.getSimpleType()!=null) {
            type.addElement(new Title(StringUtils.escapeXML(theElementNode
                .getSimpleType().toString())));
        }

        Text range =
            generateRangeText(theElementNode.getXMLRange());
        // Needs to be an offset from the size
        range.setX(ELEMENT_ATTRIBUTE_BOX_LEFT_PADDING+
            ELEMENT_ATTRIBUTE_TEXT_LEFT_PADDING+10);
        range.setY(theSVGYOffset+15);

        // NB: Please note that elements are rendered in order
        addToGroup(elName);
        addToGroup(type);

        // theSVGGroupNode.addElement(rangeBox);
        addToGroup(range);
    }

    /**
     * @see com.docfacto.xsd2svg.SVGGroup#getSVGSize()
     */
    @Override
    public int getSVGSize() {
        return ELEMENT_ATTRIBUTE_BOX_HEIGHT;
    }

}
