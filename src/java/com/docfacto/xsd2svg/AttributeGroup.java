/* 
 * @author dhudson - 
 * Created 28 May 2012 : 07:28:45
 */

package com.docfacto.xsd2svg;

import java.util.List;

import com.docfacto.common.StringUtils;
import com.docfacto.svg.Rect;
import com.docfacto.svg.Text;
import com.docfacto.svg.Title;
import com.docfacto.svg.Tspan;
import com.docfacto.xsdtree.AttributeTreeNode;

/**
 * Class that represents an XML Attribute in SVG notation.
 * 
 * @author dhudson - created 28 May 2012
 * @since 2.0
 */
public class AttributeGroup extends SVGGroup {

    private final AttributeTreeNode theAttrTreeNode;

    /**
     * Constructor.
     * 
     * @param node
     */
    public AttributeGroup(AttributeTreeNode node) {
        super(node);
        theAttrTreeNode = node;
    }

    /**
     * @see com.docfacto.xsd2svg.SVGGroup#generateSVGGroup()
     */
    @Override
    public void generateSVGGroup() {
        // Its this positioned 0/0 to relative
        Rect rect = getRect();
        
        rect.setID(calculateID(theAttrTreeNode));

        rect.setHeight(ELEMENT_ATTRIBUTE_BOX_HEIGHT);
        rect.setWidth(ELEMENT_ATTRIBUTE_BOX_WIDTH);
        rect.setY(theSVGYOffset-15);
        rect.setX(ELEMENT_ATTRIBUTE_BOX_LEFT_PADDING);
        rect.setClass("attribute");

        rect.addAttribute("ry","10");

        rect.addElement(new Title(calculateID(theAttrTreeNode)));
        
        Text attrName = new Text();

        if (theAttrTreeNode.hasAnnotation()) {
            // Add a tspan element for text and a title element for tooltip
            attrName.addElement(new Tspan(theAttrTreeNode.getNodeName()));
            Title title = new Title(theAttrTreeNode.getAnnotation());
            attrName.addElement(title);
        }
        else {
            attrName.setValue(theAttrTreeNode.getNodeName());
        }

        attrName
            .setX(ELEMENT_ATTRIBUTE_BOX_LEFT_PADDING+
                ELEMENT_ATTRIBUTE_TEXT_LEFT_PADDING);
        attrName.setY(theSVGYOffset);
//        attrName.setStyle(Style.ITALIC);

        if (theAttrTreeNode.isRequired()) {
            attrName.addStyleAttribute("fill:red");
        }

        Text type = new Text();
        type.setX(ELEMENT_ATTRIBUTE_BOX_WIDTH-
            ELEMENT_ATTRIBUTE_TYPE_RIGHT_PADDING);
        type.setY(theSVGYOffset);

        if (theAttrTreeNode.hasDefaultValue()&&theAttrTreeNode.isEnum()) {
            type.addElement(new Tspan(theAttrTreeNode.getTypeName()));
            StringBuilder builder = new StringBuilder(100);
            builder.append("Default: ");
            builder.append(theAttrTreeNode.getDefaultValue());
            builder.append("|");
            builder.append(getEnumString(theAttrTreeNode.getEnums()));
            type.addElement(new Title(builder.toString()));
            type.setClass("type-default");
        }
        else if (theAttrTreeNode.hasDefaultValue()) {
            type.addElement(new Tspan(theAttrTreeNode.getTypeName()));
            type.addElement(new Title(theAttrTreeNode.getDefaultValue()));
            type.setClass("type-default");
        }
        else if (theAttrTreeNode.hasFixedValue()) {
            type.setValue(theAttrTreeNode.getFixedValue());
            type.setClass("type-fixed");
        }
        else if (theAttrTreeNode.isEnum()) {
            type.addElement(new Tspan(theAttrTreeNode.getTypeName()));
            type.addElement(new Title(StringUtils.escapeXML(getEnumString(theAttrTreeNode.getEnums()))));
            type.setClass("type");
        }
        else {
            type.setValue(theAttrTreeNode.getTypeName());
            type.setClass("type");
        }

        addToGroup(attrName);
        addToGroup(type);
    }

    /**
     * Provide a list of enums
     * 
     * @param enums to concatenate
     * @return a delimited list of Enums
     * @since 2.5
     */
    private String getEnumString(List<String> enums) {
        StringBuilder builder = new StringBuilder(100);
        for (String value:theAttrTreeNode.getEnums()) {
            builder.append(value);
            builder.append("|");
        }

        //REMOVE:
        System.out.println("  Enum String ..  " + builder.toString());
        return builder.toString();
    }

    /**
     * @see com.docfacto.xsd2svg.SVGGroup#getSVGSize()
     */
    @Override
    public int getSVGSize() {
        return ELEMENT_ATTRIBUTE_BOX_HEIGHT;
    }

}
