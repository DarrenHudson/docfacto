/*
 * @author dhudson -
 * Created 28 May 2012 : 07:27:12
 */

package com.docfacto.xsd2svg;

import java.util.List;

import com.docfacto.svg.Rect;
import com.docfacto.svg.Text;
import com.docfacto.svg.Text.Alignment;
import com.docfacto.svg.Title;
import com.docfacto.xsdtree.AttributeTreeNode;
import com.docfacto.xsdtree.ComplexTreeNode;
import com.docfacto.xsdtree.ElementTreeNode;
import com.docfacto.xsdtree.SchemaTreeNode;

/**
 * Class that represents a Complex XML Element in SVG notation
 * 
 * @author dhudson - created 28 May 2012
 * @since 2.0
 */
public class ComplexGroup extends SVGGroup {

    private final ComplexTreeNode theComplexNode;

    private int theSVGNodeSize = 0;

    /**
     * Constructor.
     * 
     * @param node the complex node
     * @since 2.0
     */
    public ComplexGroup(ComplexTreeNode node) {
        super(node);
        theComplexNode = node;
    }

    /**
     * @see com.docfacto.xsd2svg.SVGGroup#generateSVGGroup()
     */
    @Override
    public void generateSVGGroup() {
        final Rect rect = getRect();

        if (theComplexNode.isEmptyComplexType()) {
            rect.setClass("element");
        }
        else if (theComplexNode.isChoice()) {
            rect.setClass("choice");
        }
        else {
            rect.setClass("complex");
        }

        rect.setID(calculateID(theComplexNode));
        
        rect.addElement(new Title(calculateID(theComplexNode)));
        
        final Text complexName = new Text(theComplexNode.getNodeName());
        complexName.setX(60);
        complexName.setY(15);
        //complexName.setStyle(Style.ITALIC);

        if (theComplexNode.isRequired()) {
            complexName.addStyleAttribute("fill:red");
        }

        final Text range =
            generateRangeText(theComplexNode.getXMLRange());

        range.setX(40);
        range.setY(15);
        range.setAlignment(Alignment.END);

        addToGroup(complexName);
        addToGroup(range);

        int yOffSet = 20;

        List<AttributeTreeNode> attributes =
            theComplexNode.getAttributeChildren();
        if (!attributes.isEmpty()) {
            // Start the attribute list at 35
            yOffSet = 35;

            for (final AttributeTreeNode attrNode:attributes) {
                final AttributeGroup attrGroup = new AttributeGroup(attrNode);
                attrGroup.setYOffset(yOffSet);
                attrGroup.generateSVGGroup();

                addToGroup(attrGroup);
                yOffSet += attrGroup.getSVGSize();
            }
        }

        boolean lastNodeComplex = false;

        for (final SchemaTreeNode treeNode:theComplexNode.getSchemaChildren()) {
            if (treeNode.isAttributeTreeNode()) {
                // Already processed these bad boys
                continue;
            }

            // Add a bit of padding if required
            if (lastNodeComplex&&treeNode.isElementTreeNode()) {
                yOffSet += COMPLEX_PADDING;
                lastNodeComplex = false;
            }

            // Add a bit of padding for a complex node
            if (treeNode.isComplexTreeNode()) {
                yOffSet += COMPLEX_PADDING;
                lastNodeComplex = true;
            }

            SVGGroup subGroup;

            // I think that this should be done in the walkers..
            if (treeNode.isElementTreeNode()) {
                subGroup = new ElementGroup((ElementTreeNode)treeNode);
            }
            else {
                subGroup = new ComplexGroup((ComplexTreeNode)treeNode);
            }

            subGroup.setYOffset(yOffSet);
            subGroup.generateSVGGroup();

            // Pick up Complex and Element nodes
            addToGroup(subGroup);

            yOffSet += subGroup.getSVGSize();
        }

        // All finished, lets add some padding
        theSVGNodeSize = yOffSet+COMPLEX_PADDING;

        rect.setHeight(theSVGNodeSize);
    }

    /**
     * Return the complex tree node for this group
     * 
     * @return the complex tree node that created this group
     * @since 2.0
     */
    public ComplexTreeNode getComplexTreeNode() {
        return theComplexNode;
    }

    /**
     * @see com.docfacto.xsd2svg.SVGGroup#getSVGSize()
     */
    @Override
    public int getSVGSize() {
        return theSVGNodeSize;
    }

}
