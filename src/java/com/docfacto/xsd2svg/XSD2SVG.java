package com.docfacto.xsd2svg;

import com.docfacto.common.DocfactoException;
import com.docfacto.xsd2common.XSDArgumentParser;

/**
 * XSD2SVG command line launcher
 * 
 * @author dhudson - created 25 May 2012
 * @since 2.0
 */
public class XSD2SVG {
    private String theSVG="";
    /**
     * Create a new instance of <code>XSD2SVG</code>.
     * 
     * @param parser validated arguments
     * @since 2.2
     */
    public XSD2SVG(XSDArgumentParser parser) {
        final XSD2SVGProcessor processor = new XSD2SVGProcessor(parser);

        try {
            processor.process();
            theSVG = processor.getSvgAsString();
        }
        catch (final DocfactoException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return theSVG;
    }

    /**
     * Command line launcher for XSD2SVG
     * 
     * @param args first argument is the file to process, second argument is the
     * output folder
     * @since 2.0
     */
    public static void main(String[] args) {
        if (args.length<4||args.length>10) {
            System.out.println("Invalid number of arguments");
            usage();
            System.exit(1);
        }

        final XSDArgumentParser parser = new XSDArgumentParser(args);

        try {
            parser.validate();
            new XSD2SVG(parser);
        }catch(final DocfactoException ex) {
            System.out.println("Arguments not valid cause ["+ex.getLocalizedMessage()+"]");
            usage();
        }
    }

    /**
     * Prints the options for this program
     * 
     * @since 2.0
     */
    private static final void usage() {
        System.out.println("Usage:");
        System.out.println("\t-xsd <filename>    : XSD to process");
        System.out
        .println("\t-rootnode <name>   : Root node name can be used, if the root node is not the first element in the schema, or you require a subset of the schema");
        System.out.println("\t-namespace <namespace> of the root node if not the default null namespace");
        System.out.println("\t-title <title>     : Of the SVG");
        System.out
        .println("\t-output <filename> : Name of the file to generate");
        System.out.println();
    }
}
