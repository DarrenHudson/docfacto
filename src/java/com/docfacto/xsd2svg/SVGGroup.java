/*
 * @author dhudson -
 * Created 28 May 2012 : 07:30:31
 */

package com.docfacto.xsd2svg;

import com.docfacto.svg.G;
import com.docfacto.svg.Rect;
import com.docfacto.svg.SVGElement;
import com.docfacto.svg.Text;
import com.docfacto.xml.XMLRange;
import com.docfacto.xsdtree.SchemaTreeNode;

/**
 * Wrapper class for a group of SVG nodes that represent an XML Element. There
 * is a <i>g</i> element which is the root and then a <i>rect</i> element
 * 
 * @author dhudson - created 28 May 2012
 * @since 2.0
 */
public abstract class SVGGroup extends G {

    /**
     * Statics for positioning of the SVG elements
     */
    static final int ELEMENT_ATTRIBUTE_BOX_WIDTH = 400;

    static final int ELEMENT_ATTRIBUTE_BOX_HEIGHT = 20;

    static final int ELEMENT_ATTRIBUTE_BOX_LEFT_PADDING = 10;

    static final int ELEMENT_ATTRIBUTE_TEXT_LEFT_PADDING = 10;

    static final int ELEMENT_ATTRIBUTE_TYPE_RIGHT_PADDING = 5;

    static final int COMPLEX_PADDING = 5;

    // NB: Scope definition here
    int theSVGYOffset = 0;

    private final Rect theRect;
    
    /**
     * Create a new instance of <code>SVGGroup</code>.
     * @param treeNode schema tree node
     * @since 2.0
     */
    public SVGGroup(SchemaTreeNode treeNode) {
        // Create a RECT and add it
        theRect = new Rect();
        addElement(theRect);
        //calculateID(treeNode);
    }

    /**
     * Create a hieraric unique name 
     * 
     * @param treeNode to process
     * @since 2.4
     */
    public String calculateID(SchemaTreeNode treeNode) {
        String ID = treeNode.getNodeName();
        SchemaTreeNode parent = (SchemaTreeNode) treeNode.getParent();
        while( parent.getParent() != null) {
            ID = parent.getNodeName() + "." +ID;
            parent = (SchemaTreeNode) parent.getParent();
        }

        return ID;
    }
    
    /**
     * Adds a <code>SVGElement</code> to the root group
     * 
     * @param element
     * @since 2.0
     */
    public void addToGroup(SVGElement element) {
        addElement(element);
    }

    /**
     * A <i>rect</i> element is added to the group upon creation of this class
     * 
     * @return the rect added to the group
     * @since 2.0
     */
    public Rect getRect() {
        return theRect;
    }

    /**
     * Sets the width of the inner <i>rect</i>
     * 
     * @param width
     * @since 2.0
     */
    public void setRectWidth(int width) {
        theRect.setWidth(width);
    }

    /**
     * Set the starting Y offset for this node
     * 
     * @param offset
     * @since 2.0
     */
    public void setYOffset(int offset) {
        theSVGYOffset = offset;
    }

    /**
     * Returns the y offset for this node
     * 
     * @return the Y offset for this node
     * @since 2.0
     */
    public int getYOffset() {
        return theSVGYOffset;
    }

    /**
     * Create a SVG text element which represents an XML Range. If the range is
     * 0 .. 0 then the text will be blank
     * 
     * @param range to represent as text
     * @return Text which represents a XML range
     * @since 2.0
     */
    public static Text generateRangeText(XMLRange range) {

        final StringBuilder builder = new StringBuilder(10);

        // No range has been set, so lets not display one
        if (range.getMaxOccurs()!=0||range.getMinOccurs()!=0) {
            builder.append("[");
            builder.append(range.getMinOccurs());
            builder.append(" .. ");

            if (range.getMaxOccurs()==-1) {
                // Infinity Symbol
                builder.append("&#x221e;");
            }
            else {
                builder.append(range.getMaxOccurs());
            }

            builder.append("]");
        }

        final Text text = new Text(builder.toString());

        text.setClass("range");
        return text;
    }

    /**
     * Generate all of the SVG elements required for this node
     * 
     * @since 2.0
     */
    public abstract void generateSVGGroup();

    /**
     * Return the size of this element
     * 
     * @return size of the SVG Element
     * @since 2.0
     */
    public abstract int getSVGSize();
}
