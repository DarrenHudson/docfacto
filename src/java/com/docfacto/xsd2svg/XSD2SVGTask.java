/*
 * @author dhudson -
 * Created 2 Aug 2012 : 11:13:13
 */

package com.docfacto.xsd2svg;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import com.docfacto.common.DocfactoException;
import com.docfacto.xsd2common.XSDArgumentParser;

/**
 * Ant task to run the XSD2SVG processor.
 * 
 * @author dhudson - created 2 Aug 2012
 * @since 2.0
 */
public class XSD2SVGTask extends Task {

    private final XSDArgumentParser theArgParser = new XSDArgumentParser();

    /**
     * File to store the resulting DITA file
     * 
     * @param output
     * @since 2.2
     */
    public void setoutput(String output) {
        theArgParser.setOutputFile(output);
    }

    /**
     * XSD to process
     * 
     * @param xsd
     * @since 2.2
     */
    public void setxsd(String xsd) {
        theArgParser.setXSD(xsd);
    }

    /**
     * Set the title
     * 
     * @param title
     * @since 2.2
     */
    public void settitle(String title) {
        theArgParser.setTitle(title);
    }

    /**
     * Root node to start from
     * 
     * @param rootnode
     * @since 2.2
     */
    public void setrootnode(String rootnode) {
        theArgParser.setRootNode(rootnode);
    }

    /**
     * Specify the root node name space
     * 
     * @param namespace to specify
     * @since 2.5
     */
    public void setnamespace(String namespace) {
        theArgParser.setNamespace(namespace);
    }
    
    /**
     * @see org.apache.tools.ant.Task#execute()
     */
    @Override
    public void execute() throws BuildException {
        final XSD2SVGProcessor xsdProcessor =
        new XSD2SVGProcessor(theArgParser);

        try {
            xsdProcessor.process();
        }
        catch (final DocfactoException ex) {
            throw new BuildException(ex);
        }
    }
}
