/*
 * @author dhudson -
 * Created 24 May 2012 : 18:18:39
 */

package com.docfacto.xsd2svg;

import java.io.File;

import javax.swing.tree.DefaultMutableTreeNode;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.Platform;
import com.docfacto.licensor.UDCProcessor;
import com.docfacto.svg.SVGDocument;
import com.docfacto.svg.Title;
import com.docfacto.xml.XMLElement;
import com.docfacto.xsd2common.XSDArgumentParser;
import com.docfacto.xsdtree.ComplexTreeNode;
import com.docfacto.xsdtree.XSDTreeTraverser;
import com.sun.xml.xsom.XSSchemaSet;

/**
 * Produce a schematic of the XSD in SVG form
 * 
 * @author dhudson - created 24 May 2012
 * @since 2.0
 */
public class XSD2SVGProcessor {

    private static final int X_TRANSFORM_OFFSET = 20;
    private static final int PADDING = 10;

    private final XSDArgumentParser theArgParser;

    private String theSVG;

    /**
     * Create a new instance of <code>XSD2SVGProcessor</code>.
     * 
     * @param parser holder of the arguments and parameters
     * @since 2.0
     */
    public XSD2SVGProcessor(XSDArgumentParser parser) {
        theArgParser = parser;
    }

    /**
     * Generate an SVG schematic from the provided schema set
     * 
     * @param schemaSet to process
     * @throws DocfactoException if unable to parse the schema
     * @since 2.0
     */
    public void process(XSSchemaSet schemaSet) throws DocfactoException {
        // Call the traverser
        final XSDTreeTraverser traverser = new XSDTreeTraverser(theArgParser);
        traverser.setDebug(true);

        // Parse the file we are interested in
        traverser.visit(schemaSet);

        process(traverser);
    }

    /**
     * Generate an SVG schematic from the provided XSD file
     * 
     * @throws DocfactoException if unable to parse the schema
     * @since 2.0
     */
    public void process() throws DocfactoException {
        final XSDTreeTraverser traverser = new XSDTreeTraverser(theArgParser);
        traverser.setDebug(true);

        // Parse the file we are interested in
        traverser.visit(new File(theArgParser.getXSD()));

        process(traverser);
    }

    /**
     * Generate an SVG schematic from the provided XSDTree traverser
     * 
     * @param traverser
     * @throws DocfactoException
     * @since 2.0
     */
    public void process(XSDTreeTraverser traverser)
    throws DocfactoException {

        UDCProcessor.registerProduct("XSD2SVG",null,null);

        final SVGDocument SVGDoc = new SVGDocument();

        addStyleSheet(SVGDoc);

        if (theArgParser.hasTitle()) {
            // Set the title of the Doc
            SVGDoc.addToRootNode(new Title(theArgParser.getTitle()));
        }

        final ComplexGroup complexGroup =
            new ComplexGroup((ComplexTreeNode)traverser.getRootNode()
                .getChildAt(0));

        // This will ripple down and generate the nested doc.
        complexGroup.generateSVGGroup();

        final DefaultMutableTreeNode node =
            (DefaultMutableTreeNode)traverser.getRootNode()
                .getChildAt(0);

        // What needs to happen now is that we need to work out the relative
        // spacing of everything
        walkGroup(complexGroup);

        SVGDoc.addToRootNode(complexGroup);

        // here return svg string

        // Set the size so that browsers know to scroll / zoom
        complexGroup.setRectWidth(getRectWidth(node.getDepth()-1));
        SVGDoc.getRootNode()
            .setWidth(complexGroup.getRect().getWidth()+PADDING);
        SVGDoc.getRootNode().setHeight(
            complexGroup.getRect().getHeight()+PADDING);

        // Lets add a Grid to help about a bit
        // Grid grid = new Grid();
        // rootGroup.addElement(grid);

        theSVG = SVGDoc.getRootNode().toXML();
        if (theArgParser.getOutputFile().equalsIgnoreCase("null")||
            theArgParser.getOutputFile().equalsIgnoreCase("none")) {
            return;
        }

        final File localFile = new File(theArgParser.getOutputFile());

        try {
            SVGDoc.save(localFile.getPath());
            System.out.println("Generated "+localFile.getPath());
        }
        catch (final Exception ex) {
            ex.printStackTrace();
            throw new DocfactoException("Unable to generate SVG file",ex);
        }
    }

    /**
     * Add they style sheet to the document
     * 
     * @param doc to append the style sheet to
     * @since 2.5
     */
    private void addStyleSheet(SVGDocument doc) {
        StringBuilder builder = new StringBuilder(500);
        builder.append(".complex { fill:none; stroke-dasharray: 9, 5; stroke: black; stroke-width: 2 }");
        builder.append(Platform.LINE_SEPARATOR);
        builder.append(".choice { fill:none; stroke-dasharray: 9, 5; stroke: blue; stroke-width: 2 }");
        builder.append(Platform.LINE_SEPARATOR);
        builder.append(".element { fill:none; stroke: black; stroke-width: 2 }");
        builder.append(Platform.LINE_SEPARATOR);
        builder.append(".attribute { fill:none; stroke: black; stroke-width: 2 }");
        builder.append(Platform.LINE_SEPARATOR);
        builder.append(".required { fill:red }");
        builder.append(Platform.LINE_SEPARATOR);
        builder.append(".range { font-size: 10; stroke:black; text-anchor:middle }");
        builder.append(Platform.LINE_SEPARATOR);
        builder.append(".type { text-anchor:end }");
        builder.append(Platform.LINE_SEPARATOR);
        builder.append(".type-default { text-anchor:end;fill:blue }");
        builder.append(Platform.LINE_SEPARATOR);
        builder.append(".type-fixed { text-anchor:end; fill:red }");
        builder.append(Platform.LINE_SEPARATOR);
        builder.append(".connector {stroke: blue }");
        builder.append(Platform.LINE_SEPARATOR);
        doc.addStyleSheet(builder.toString());
    }

    /**
     * A recursive method to calculate <i>rect</i> sizes
     * 
     * @param group
     * @since 2.0
     */
    private void walkGroup(ComplexGroup group) {
        for (final XMLElement element:group.getElements()) {
            if (element instanceof ComplexGroup) {
                final ComplexGroup subGroup = (ComplexGroup)element;

                final int nestedItems =
                    ((DefaultMutableTreeNode)subGroup.getComplexTreeNode())
                        .getDepth()-1;

                subGroup.setRectWidth(getRectWidth(nestedItems));

                final int yOffset = subGroup.getYOffset();

                subGroup.addAttribute("transform","translate("+
                    X_TRANSFORM_OFFSET+
                    ","+
                    yOffset+")");

                walkGroup(subGroup);
            }
        }
    }

    /**
     * Helper method to set the correct size of the <i>rect</i>
     * 
     * @param nestedItems
     * @since 2.0
     */
    private int getRectWidth(int nestedItems) {

        if (nestedItems==0) {
            return (20+SVGGroup.ELEMENT_ATTRIBUTE_BOX_WIDTH);
        }
        else {
            // 40 is the padding each side of the box (20 left and 20 right)
            return ((nestedItems*40)+20+SVGGroup.ELEMENT_ATTRIBUTE_BOX_WIDTH);
        }
    }

    /**
     * Gets the svg string.
     * 
     * @return the SVG as a string
     * @since 2.5.1
     */
    public String getSvgAsString() {
        return theSVG;
    }

}
