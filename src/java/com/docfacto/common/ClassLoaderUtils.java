package com.docfacto.common;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Hashtable;

/**
 * A collection of static utils to help with class loading.
 * 
 * @author dhudson - created 10 Jan 2014
 * @since 2.5
 */
public class ClassLoaderUtils {

    private static final HashMap<String,String> theLoadedJarNames =
        new HashMap<String,String>(5);
    private static final Class<?>[] theParameters = new Class[] {URL.class};

    /**
     * Add classes from a jar file, given a file
     * 
     * @param file to load
     * @throws IOException if unable to load the jar file
     * @since 2.5
     */
    public static void addFile(File file) throws IOException {
        // Lets see if there is a file name clash..
        if (theLoadedJarNames.containsKey(file.getName())) {
            // Lets spit out a warning..
            System.err
                .println("ClassLoaderUtils: Possible loading of duplicate jar file "+
                    file.getName());
        }
        else {
            theLoadedJarNames.put(file.getName(),file.getName());
        }

        addURL(file.toURI().toURL());
    }

    /**
     * Load classes from a jar file given a URL
     * 
     * @param url to load
     * @throws IOException if unable to load the file
     * @since 2.5
     */
    public static void addURL(URL url) throws IOException {
        URLClassLoader sysloader =
            (URLClassLoader)ClassLoader.getSystemClassLoader();
        Class<?> sysclass = URLClassLoader.class;
        try {
            Method method = sysclass.getDeclaredMethod("addURL",theParameters);
            method.setAccessible(true);
            method.invoke(sysloader,new Object[] {url});
        }
        catch (Throwable t) {
            throw new IOException("Unable to add URL to system classloader "
                +url.toString());
        }
    }

    /**
     * Given a directory, find all zip and jar files and add them to the class
     * loader
     * 
     * @param dir to search
     */
    public static void addJars(File dir) {
        Hashtable<String,File> table = new Hashtable<String,File>();
        try {
            findJars(dir,table);
        }
        catch (IOException ex) {
            System.err.println("ClassLoaderUtils: unable to scan folder ["+dir+
                "] "+ex);
            return;
        }

        for (String name:table.keySet()) {
            try {
                addFile(table.get(name));
            }
            catch (IOException ignore) {
                // addURL says it all
            }
        }
    }

    /**
     * Recurs a folder and find all of the jars that can be loaded
     * 
     * @param dir to search
     * @param table to append to
     * @throws IOException if can't search the file system
     */
    private static void findJars(File dir,Hashtable<String,File> table)
    throws IOException {
        File[] entries = dir.listFiles();

        for (int i = 0;entries!=null&&i<entries.length;i++) {
            File entry = entries[i];

            if (entry.isDirectory()) {
                findJars(entry,table);
            }
            else {
                String name = entry.getName().toLowerCase();
                if (name.endsWith(".jar")||name.endsWith(".zip")) {
                    String jar = entry.getCanonicalPath();
                    if (!table.containsKey(jar)) {
                        table.put(jar,entry);
                    }
                }
            }
        }
    }
}
