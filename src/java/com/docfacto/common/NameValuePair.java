package com.docfacto.common;

/**
 * Simple wrapper class around a name / value pair
 *
 * @author dhudson - created 25 Jun 2012
 * @since 2.0
 */
public class NameValuePair {

    /**
     * Holds the name of the value pair
     */
    private final String theName;

    /**
     * Holds the value for the name value pair
     */
    private String theValue;

    /**
     * Create a new instance of <code>NameValuePair</code> with a given name but no value.
     * @param name of the value pair
     */
    public NameValuePair(String name) {
        theName = name;
    }

    /**
     * Create a new instance of <code>NameValuePair</code> with the given name and a value.
     * @param name of the value pair
     * @param value pair value
     */
    public NameValuePair(String name,String value) {
        theName = name;
        theValue = value;
    }

    /**
     * Set the value for this name / value pair
     *
     * @param value pair value
     * @since 2.0
     */
    public void setValue(String value) {
        theValue = value;
    }

    /**
     * Return the name of the pair
     *
     * @return the name of the pair
     * @since 2.0
     */
    public String getName() {
        return theName;
    }

    /**
     * Return the value for this pair
     *
     * @return the value associated with this pair
     * @since 2.0
     */
    public String getValue() {
        return theValue;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder(100);
        builder.append(theName);
        builder.append("=\"");
        builder.append(theValue);
        builder.append("\"");
        return builder.toString();
    }
}