/*
 * @author dhudson -
 * Created 22 May 2012 : 06:11:28
 */

package com.docfacto.common;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

/**
 * Class of static methods that are general utils
 * 
 * @author dhudson - created 25 Jun 2012
 * @since 2.0
 */
public final class Utils {

    /**
     * Constant for timestamp {@value}
     */
    public static final String DEFAULT_DATE_TIME_FORMAT =
        "yyyy-MM-dd HH:mm:ss";

    /**
     * Thread safe date time format
     */
    private static final ThreadLocal<DateFormat> DATE_TIME_FORMAT =
        new ThreadLocal<DateFormat>() {
            /**
             * @see java.lang.ThreadLocal#initialValue()
             */
            @Override
            protected DateFormat initialValue() {
                return new SimpleDateFormat(DEFAULT_DATE_TIME_FORMAT);
            }
        };

    private static String[] theSortedLocales;

    /**
     * Constructor.
     * 
     * @since 2.2
     */
    private Utils() {
    }

    /**
     * Return a sorted list of locales supported by Java
     * 
     * @return a String array of sorted locales
     * @since 2.4
     */
    public static String[] getLocales() {
        if (theSortedLocales==null) {
            Locale locales[] = Locale.getAvailableLocales();

            theSortedLocales = new String[locales.length];

            int i = 0;
            for (Locale locale:locales) {
                theSortedLocales[i++] = locale.toString();
            }

            Arrays.sort(theSortedLocales);
        }

        return theSortedLocales;
    }

    /**
     * Return the current / default locale
     * 
     * @return String representation of the current locale
     * @since 2.4
     */
    public static String getDefaultLocale() {
        return Locale.getDefault().toString();
    }

    /**
     * The supplied date is formatted using the yyyy-MM-dd HH:mm:ss formatting
     * pattern
     * 
     * @param date the date to format
     * @return a formatted date and time string.
     * @since 2.0
     */
    public static String formatDateTime(Date date) {
        if (date==null) {
            return "";
        }
        return DATE_TIME_FORMAT.get().format(date);
    }

    /**
     * The supplied date is formatted using the yyyy-MM-dd HH:mm:ss formatting
     * pattern
     * 
     * @param millis a time represented as the number of milliseconds, between a
     * point in time and midnight, January 1, 1970 UTC.
     * @return a formatted date and time string.
     * @since 2.0
     */
    public static String formatDateTime(long millis) {
        return formatDateTime(new Date(millis));
    }

    /**
     * Dynamically extend the System Class Loader with the given path or jar
     * file
     * 
     * @param path to extend the class path by
     * @throws DocfactoException if invalid path
     * @since 2.0
     */
    public static void extendClassPath(String path)
    throws DocfactoException {
        final File filePath = new File(path);

        final ClassLoader cl = ClassLoader.getSystemClassLoader();

        if (cl instanceof URLClassLoader) {
            final URLClassLoader ul = (URLClassLoader)cl;

            final Class<?>[] paraTypes = new Class[1];
            paraTypes[0] = URL.class;

            try {
                final Method method =
                    URLClassLoader.class.getDeclaredMethod("addURL",paraTypes);

                method.setAccessible(true);
                final Object[] args = new Object[1];

                args[0] = filePath.toURI().toURL();
                method.invoke(ul,args);
            }
            catch (final Exception ex) {
                throw new DocfactoException("Unable to extend class path ",ex);
            }
        }
        else {
            throw new DocfactoException(
                "System Class loader is not a URL class loader");
        }
    }
}
