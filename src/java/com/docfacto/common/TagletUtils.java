package com.docfacto.common;

/**
 * Taglet utils.
 * 
 * @author kporter - created Sep 12, 2013
 * @since 2.4.5
 */
public class TagletUtils {
    
    
    private TagletUtils(){
    }
    
    /**
     * Remove {@literal *} and CR's from the Javadoc text
     * 
     * @param textWithStars to process
     * @return clean tag text
     * @since 2.2
     */
    public static String removeDocLineIntros(String textWithStars) {
        String lineBreakGroup = "(\\r\\n?|\\n)";
        String noBreakSpace = "[^\r\n&&\\s]";
        return textWithStars.replaceAll(
            lineBreakGroup+noBreakSpace+"*\\*","$1");
    }
}
