package com.docfacto.common;

import java.io.IOException;
import java.io.Writer;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * XML Serializer.
 * 
 * This can flat or pretty print. Note that an XMLWriter is not threadsafe and
 * as such may only be used to write one XML document at a time.
 * 
 * @author Darren Hudson
 * @since 2.4
 */
public final class XMLWriter {

    /**
     * Print writer.
     */
    private Writer theWriter;

    /**
     * Indicates whether the writer is pretty printing
     */
    private boolean thisIsPrettyPrinting = false;

    /**
     * Used to track when a new line is required. A new line (plus indentation)
     * is thrown before every start element and also before every end element
     * unless the last node processed was a text node or the element had no
     * body. This is only used when pretty printing.
     */
    private boolean throwNewLine = false;

    /**
     * used for indentation when pretty printing
     */
    private int theLevel = 0;

    private boolean isSortingAttributes = true;

    /**
     * Constructor
     * 
     * @since 2.4
     */
    public XMLWriter() {
    }

    /**
     * Set the feature for attribute sorting.
     * 
     * When serialised attributes can be sorted alphabetically
     * 
     * @param sorting true if required
     * @since 2.4
     */
    public void setAttributeSorting(boolean sorting) {
        isSortingAttributes = sorting;
    }

    /**
     * Write the document, without formatting
     * 
     * @param document to process
     * @param writer to use
     * @throws DocfactoException if unable to serialise the Document
     * @since 2.4
     */
    public void write(Document document,Writer writer)
    throws DocfactoException {
        serialize(document,writer,false);
    }

    /**
     * Print the document to the writer, with pretty formatting
     * 
     * @param document to process
     * @param writer to use
     * @throws DocfactoException if unable to serialise the document
     * @since 2.4
     */
    public void print(Document document,Writer writer)
    throws DocfactoException {
        serialize(document,writer,true);
    }

    /**
     * serialize
     * 
     * @param document to process
     * @param writer to write to
     * @param pretty formatting required
     * @throws DocfactoException
     * @since 2.4
     */
    private void serialize(
    Document document,Writer writer,boolean pretty)
    throws DocfactoException {
        // Ensure we have a document
        if (document==null) {
            throw new DocfactoException("null document");
        }
        // .. and an output stream
        if (writer==null) {
            throw new DocfactoException("null writer");
        }

        thisIsPrettyPrinting = pretty;
        theWriter = writer;

        throwNewLine = false;
        theLevel = -1;
        try {

            // Write the XML Version
            write(document);

            NodeList nodes = document.getChildNodes();
            for (int i = 0;i<nodes.getLength();i++) {
                write(nodes.item(i));
            }
            theWriter.flush();
        }
        catch (Exception ex) {
            throw new DocfactoException("Error serailizing XML document",ex);
        }
        finally {
            theWriter = null;
        }
    }

    /**
     * Prints the specified node, recursively.
     */
    private void write(Node node) throws Exception {

        // is there anything to do?
        if (node==null) {
            return;
        }

        switch (node.getNodeType()) {
        case Node.DOCUMENT_NODE: {

            Document doc = (Document)node;
            StringBuilder builder = new StringBuilder(100);
            builder.append("<?xml ");
            if (doc.getXmlVersion()!=null) {
                builder.append("version=\"");
                builder.append(doc.getXmlVersion());
                builder.append("\" ");
            }

            if (doc.getXmlEncoding()!=null) {
                builder.append("encoding=\"");
                builder.append(doc.getXmlEncoding());
                builder.append("\" ");
            }

            builder.append("standalone=\"");

            if (doc.getXmlStandalone()) {
                builder.append("yes");
            }
            else {
                builder.append("no");
            }

            builder.append("\" ?>");

            theWriter.write(builder.toString());
            theWriter.write(Platform.LINE_SEPARATOR);
            break;
        }

        case Node.DOCUMENT_TYPE_NODE:
            DocumentType docType = (DocumentType)node;

            StringBuilder builder = new StringBuilder(100);
            builder.append("<!DOCTYPE ");
            builder.append(docType.getName());
            builder.append(" ");

            if (docType.getPublicId()!=null) {
                builder.append("PUBLIC ");
                builder.append("\"");
                builder.append(docType.getPublicId());
                builder.append("\"");
            }

            if (docType.getSystemId()!=null) {
                builder.append(" \"");
                builder.append(docType.getSystemId());
                builder.append("\"");
            }

            builder.append(">");

            theWriter.write(builder.toString());
            theWriter.write(Platform.LINE_SEPARATOR);

            break;

        // write element with attributes
        case Node.ELEMENT_NODE: {
            if (thisIsPrettyPrinting) {
                theLevel++;
                theWriter.write(Platform.LINE_SEPARATOR);
                indent();
                throwNewLine = false;
            }
            theWriter.write('<');
            theWriter.write(node.getNodeName());

            // Handle Attributes
            Attr attrs[] = sortAttributes(node.getAttributes());
            for (int i = 0;i<attrs.length;i++) {
                Attr attr = attrs[i];
                theWriter.write(' ');
                theWriter.write(attr.getNodeName());
                theWriter.write("=\"");
                theWriter.write(StringUtils.escapeXML(attr.getNodeValue()));
                theWriter.write('"');
            }

            theWriter.write('>');
            NodeList children = node.getChildNodes();
            if (children!=null) {
                int len = children.getLength();
                for (int i = 0;i<len;i++) {
                    write(children.item(i));
                }
            }

            if (thisIsPrettyPrinting) {
                // write a new line and indent
                if (throwNewLine) {
                    theWriter.write(Platform.LINE_SEPARATOR);
                    indent();
                }
            }

            theWriter.write("</");
            theWriter.write(node.getNodeName());
            theWriter.write('>');

            if (thisIsPrettyPrinting) {
                throwNewLine = true;
                theLevel--;
            }

            break;
        }

        // handle entity reference nodes
        case Node.ENTITY_REFERENCE_NODE: {
            theWriter.write('&');
            theWriter.write(node.getNodeName());
            theWriter.write(';');
            break;
        }

        case Node.CDATA_SECTION_NODE:
            theWriter.write("<![CDATA[");
            theWriter.write(node.getNodeValue());
            theWriter.write("]]>");
            theWriter.write(Platform.LINE_SEPARATOR);
            break;

        // write text
        case Node.TEXT_NODE: {
            if (!node.getNodeValue().trim().isEmpty()) {
                theWriter.write(StringUtils.escapeXML(node.getNodeValue()));
                throwNewLine = false;
            }
            break;
        }

        case Node.COMMENT_NODE:
            if (throwNewLine) {
                theWriter.write(Platform.LINE_SEPARATOR);
                throwNewLine = false;
            }
            indentPlusOne();
            theWriter.write("<!--");
            theWriter.write(node.getNodeValue());
            theWriter.write("-->");
            theWriter.write(Platform.LINE_SEPARATOR);
            break;

        // write processing instruction
        case Node.PROCESSING_INSTRUCTION_NODE: {
            indent();
            theWriter.write("<?");
            theWriter.write(node.getNodeName());
            String data = node.getNodeValue();
            if (data!=null&&data.length()>0) {
                theWriter.write(' ');
                theWriter.write(data);
            }
            theWriter.write("?>");
            theWriter.write(Platform.LINE_SEPARATOR);
            break;
        }
        }

    }

    /**
     * Returns a sorted list of attributes. if sorting is required
     * 
     * @since 2.4
     */
    private Attr[] sortAttributes(NamedNodeMap attrs) {
        int len = (attrs!=null) ? attrs.getLength() : 0;
        Attr array[] = new Attr[len];
        for (int i = 0;i<len;i++) {
            array[i] = (Attr)attrs.item(i);
        }

        if (isSortingAttributes) {
            for (int i = 0;i<len-1;i++) {
                String name = array[i].getNodeName();
                int index = i;
                for (int j = i+1;j<len;j++) {
                    String curName = array[j].getNodeName();
                    if (curName.compareTo(name)<0) {
                        name = curName;
                        index = j;
                    }
                }
                if (index!=i) {
                    Attr temp = array[i];
                    array[i] = array[index];
                    array[index] = temp;
                }
            }
        }

        return array;
    }

    /**
     * This writes the correct number of indentation spaces for the level
     * currently being processed.
     * 
     * @throws IOException if unable to write spaces to the writer
     */
    private void indent() throws IOException {
        for (int i = 0;i<theLevel;i++) {
            // write 2 spaces per level
            theWriter.write("  ");
        }
    }

    /**
     * Indent to the level which an extra one
     * 
     * @throws IOException if unable to write spaces to the writer
     * @since 2.4
     */
    private void indentPlusOne() throws IOException {
        for (int i = 0;i<(theLevel+1);i++) {
            theWriter.write("  ");
        }
    }
}
