package com.docfacto.common;

import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Class with contains static utility methods to deal with XML handling
 * 
 * @author dhudson - created 5 Jul 2012
 * @since 1.4
 */
public class XMLUtils {

    /**
     * Start comment tag {@value}
     */
    public static final String START_COMMENT = "<!--";

    /**
     * End comment tag {@value}
     */
    public static final String END_COMMENT = "-->";

    private static Random theRandom = new SecureRandom();

    /**
     * Thread safe document builder
     */
    private static final ThreadLocal<DocumentBuilder> DOCUMENT_BUILDER =
        new ThreadLocal<DocumentBuilder>() {
            @Override
            protected DocumentBuilder initialValue() {
                try {
                    return DocumentBuilderFactory
                        .newInstance().newDocumentBuilder();
                }
                catch (final Exception ex) {
                    return null;
                }
            }
        };

    /**
     * Thread safe document builder, but with no validation {@docfacto.note
     * http://xerces.apache.org/xerces2-j/features.html }
     */
    private static final ThreadLocal<DocumentBuilder> FAST_DOCUMENT_BUILDER =
        new ThreadLocal<DocumentBuilder>() {
            @Override
            protected DocumentBuilder initialValue() {
                try {
                    final DocumentBuilderFactory factory =
                        DocumentBuilderFactory
                            .newInstance();

                    // Turn off all of the parsing validation to make this
                    // quick
                    // Should only be used by the serialisation method
                    factory.setNamespaceAware(false);
                    factory.setValidating(false);
                    factory.setIgnoringElementContentWhitespace(true);

                    factory.setFeature(
                        "http://xml.org/sax/features/validation",true);
                    factory.setFeature(
                        "http://xml.org/sax/features/namespaces",false);
                    factory.setFeature(
                        "http://xml.org/sax/features/validation",false);
                    factory
                        .setFeature(
                            "http://apache.org/xml/features/nonvalidating/load-external-dtd",
                            false);
                    factory
                        .setFeature(
                            "http://apache.org/xml/features/nonvalidating/load-dtd-grammar",
                            false);
                    factory
                        .setFeature(
                            "http://apache.org/xml/features/nonvalidating/load-external-dtd",
                            false);
                    factory
                        .setFeature(
                            "http://apache.org/xml/features/validation/schema/normalized-value",
                            false);
                    return factory.newDocumentBuilder();
                }
                catch (final Exception ex) {
                    return null;
                }
            }
        };

    /**
     * Constructor
     */
    private XMLUtils() {
    }

    /**
     * Make sure that internal XML Factories are used.
     * 
     * This sets the SAXParser, DocumentBuilder and TransformerFactory to the
     * internal default
     * 
     * @since 1.4
     */
    public static void setSunInternalXmlParsers() {
        System.setProperty(
            "javax.xml.parsers.SAXParserFactory",
            "com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl");
        System
            .setProperty(
                "javax.xml.parsers.DocumentBuilderFactory",
                "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
        System
            .setProperty(
                "javax.xml.transform.TransformerFactory",
                "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl");
    }

    /**
     * Get and reset thread local document builder
     * 
     * @return a <code>DocBuilder</code> which has been reset
     * @since 1.4
     */
    public static DocumentBuilder getDocumentBuilder() {
        final DocumentBuilder builder = DOCUMENT_BUILDER.get();
        builder.reset();
        return builder;
    }

    /**
     * Create an empty DOM Document.
     * 
     * @return new empty DOM document.
     * @since 1.4
     */
    public static Document createDocument() {
        return getDocumentBuilder().newDocument();
    }

    /**
     * Create a DOM document from a String of XML.
     * 
     * @param xmlString a string of XML
     * @return a DOM Document containing the XML parsed from the string.
     * @throws DocfactoException if unable to parse the string.
     * 
     * @since 1.4
     */
    public static Document createDocument(String xmlString)
    throws DocfactoException {
        final Reader reader = new StringReader(xmlString.trim());
        try {
            return getDocumentBuilder().parse(new InputSource(reader));
        }
        catch (final SAXException ex) {
            throw new DocfactoException("SAX Parsing Exception",ex);
        }
        catch (final Exception ex) {
            throw new DocfactoException("Parsing Exception",ex);
        }
    }

    /**
     * Create a DOM document from an input stream of XML.
     * 
     * @param inputStream from which XML is to be read.
     * @return a DOM Document containing the XML parsed from the input stream.
     * @throws DocfactoException if unable to parse XML.
     * 
     * @since 1.4
     */
    public static Document createDocument(InputStream inputStream)
    throws DocfactoException {
        try {
            return getDocumentBuilder().parse(new InputSource(inputStream));
        }
        catch (final SAXException ex) {
            throw new DocfactoException("SAX Parsing Exception",ex);
        }
        catch (final Exception ex) {
            throw new DocfactoException("Parsing Exception",ex);
        }
    }

    /**
     * Create a document from the input stream, but validation is removed
     * 
     * @param inputStream to read the XML from
     * @return a non validating Dom Document
     * @throws DocfactoException if unable to parse the XML
     * @since 2.2
     */
    public static Document createNonValidatingDocument(InputStream inputStream)
    throws DocfactoException {
        try {
            final DocumentBuilder builder = FAST_DOCUMENT_BUILDER.get();
            return builder.parse(inputStream);
        }
        catch (final SAXException ex) {
            throw new DocfactoException("SAX Parsing Exception",ex);
        }
        catch (final Exception ex) {
            throw new DocfactoException("Parsing Exception",ex);
        }
    }

    /**
     * Creates a DOM document from a a resource of the specified name loaded
     * from the classpath.
     * 
     * @param resourceName the resource name.
     * @return document loaded from resource.
     * @throws DocfactoException if no resource with the given name could be
     * found or the XML within it could not be parsed.
     * @since 1.4
     */
    public static Document createDocumentFromResource(String resourceName)
    throws DocfactoException {
        InputStream inputStream = null;
        Document doc = null;
        try {
            inputStream = IOUtils.getResourceAsStream(resourceName);
            doc = createDocument(inputStream);
        }
        catch (final Exception ex) {
            throw new DocfactoException("Unable to load "+resourceName,ex);
        }
        finally {
            IOUtils.close(inputStream);
        }
        return doc;
    }

    /**
     * Takes a string of XML and returns a indented version of the XML
     * 
     * @param xml to format
     * @return pretty (indented) formatted XML
     * @throws DocfactoException if unable to parse the string into XML
     * @since 2.0
     */
    public static String prettyFormat(String xml) throws DocfactoException {
        try {
            final Reader reader = new StringReader(xml.trim());
            final Document doc =
                FAST_DOCUMENT_BUILDER.get().parse(new InputSource(reader));

            return prettyFormat(doc);
        }
        catch (final Exception ex) {
            throw new DocfactoException("Unable to prettyFormat XML ",ex);
        }
    }

    /**
     * Convert a document to a pretty printed String
     * 
     * @param doc to process
     * @return A pretty printed String of the document
     * @throws DocfactoException if unable to serialise the document
     * @since 2.2
     */
    public static String prettyFormat(Document doc) throws DocfactoException {
        try {
            XMLWriter xmlWriter = new XMLWriter();
            StringWriter stringWriter = new StringWriter();
            xmlWriter.print(doc,stringWriter);
            return stringWriter.toString();
        }
        catch (final DocfactoException ex) {
            throw new DocfactoException("Unable to pretty print "+
                ex.getMessage(),ex);
        }
    }

    /**
     * Gets text from a child element.
     * <p>
     * This is a convenience method to return the text from the first child
     * element with a given tag name.
     * </p>
     * <p>
     * If there is no such child element then this will return a zero length
     * String.
     * </p>
     * 
     * @param element the parent element.
     * @param childTagName the child tag
     * @return the text of the first child element with the given name or a zero
     * length string. This will never return null.
     * @since 2.2
     */
    public static String
    getChildElementText(Element element,String childTagName) {
        String text = null;
        if (element!=null) {
            final NodeList children =
                element.getElementsByTagName(childTagName);
            if (children.getLength()>0) {
                text = children.item(0).getTextContent();
            }
        }
        if (text==null) {
            text = "";
        }
        return text;
    }

    /**
     * Return the nodes first element
     * 
     * @param parent node
     * @return the first element of a node
     * @since 2.2
     */
    public static Element getFirstElement(Node parent) {
        Node node = parent.getFirstChild();
        while (node!=null&&Node.ELEMENT_NODE!=node.getNodeType()) {
            node = node.getNextSibling();
        }
        if (node==null) {
            return null;
        }
        return (Element)node;
    }

    /**
     * Return a Element with an ID if present, else null.
     * 
     * {@docfacto.note This method should only be used for none validating
     * documents. Its because the attribute id is a type and not just an
     * attribute called id. }
     * 
     * @param doc to search
     * @param id to locate
     * @return an element with a matching ID or null
     * @since 2.2
     */
    public static Element getElementByID(Document doc,String id) {

        final NodeList nodes1 = doc.getChildNodes();
        for (int i = 0;i<nodes1.getLength();i++) {
            final Element el = treeWalk(nodes1.item(i),id);
            if (el!=null) {
                return el;
            }
        }

        return null;
    }

    /**
     * Recurse the nodes until the element is found
     * 
     * @param node
     * @param id
     * @return the matching id node, or null if not found
     * @since 2.2
     */
    private static Element treeWalk(Node node,String id)
    {
        if (Node.ELEMENT_NODE==node.getNodeType()) {
            final Element element = (Element)node;
            if (id.equals(element.getAttribute("id"))) {
                return element;
            }
        }

        final NodeList list = node.getChildNodes();
        for (int i = 0;i<list.getLength();i++) {
            final Element el = treeWalk(list.item(i),id);
            if (el!=null) {
                return el;
            }
        }

        return null;
    }

    /**
     * Search a document and return all of the elements where the specified
     * attribute as a specified value.
     * 
     * {@docfacto.note <p>
     * Use * for attribute value for all values. Use a name space of "" for
     * default name space
     * </p>}
     * 
     * @param doc to search
     * @param nameSpace name space of the attributes, use null for default
     * @param attribute to check
     * @param value of the attribute or * for all values
     * @return a list of elements that have the attribute with the specified
     * value
     * @since 2.4
     */
    public static List<Element> getElementsWithAttribute(Document doc,
    String nameSpace,String attribute,String value) {
        List<Element> elements = new ArrayList<Element>(3);
        // Lets get them all
        NodeList entries = doc.getElementsByTagName("*");
        for (int i = 0;i<entries.getLength();i++) {
            Node node = entries.item(i);
            if (Node.ELEMENT_NODE==node.getNodeType()) {
                Element element = (Element)node;
                if (element.hasAttributeNS(nameSpace,attribute)) {
                    if ("*".equals(value)) {
                        // Don't care just add it
                        elements.add(element);
                    }
                    else {
                        String attrValue =
                            element.getAttributeNS(nameSpace,attribute);
                        if (value.equals(attrValue)) {
                            elements.add(element);
                        }
                    }
                }
            }
        }
        return elements;
    }

    /**
     * Generate unique ID for elements
     * 
     * @return a unique ID based from random numbers and current time
     * @since 2.2
     */
    public static String generateUnqiueID() {
        long r = theRandom.nextLong();
        r ^= System.currentTimeMillis();

        if (r<0) {
            r = -r;
        }

        return Long.toString(r,36);
    }

    /**
     * Remove all of the children from a node
     * 
     * @param element to remove all of the children
     * @since 2.5
     */
    public static void removeChildren(Element element) {
        while (element.hasChildNodes()) {
            element.removeChild(element.getFirstChild());
        }
    }

    /**
     * Converts a java.util.Date into an instance of XMLGregorianCalendar
     * 
     * @param date Instance of java.util.Date or a null reference
     * @return XMLGregorianCalendar instance whose value is based upon the value
     * in the date parameter. If the date parameter is null then this method
     * will simply return null.
     * @since 2.5
     */
    public static XMLGregorianCalendar asXMLGregorianCalendar(Date date) {
        if (date==null) {
            return null;
        }
        else {
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(date.getTime());
            try {
                DatatypeFactory df = DatatypeFactory.newInstance();
                return df.newXMLGregorianCalendar(gc);
            }
            catch (DatatypeConfigurationException dce) {
                throw new IllegalStateException(
                    "Exception while obtaining DatatypeFactory instance",
                    dce);
            }
        }
    }

    /**
     * Converts an XMLGregorianCalendar to an instance of java.util.Date
     * 
     * @param xgc Instance of XMLGregorianCalendar or a null reference
     * @return java.util.Date instance whose value is based upon the value in
     * the xgc parameter. If the xgc parameter is null then this method will
     * simply return null.
     * @since 2.5
     */
    public static Date asDate(XMLGregorianCalendar xgc) {
        if (xgc==null) {
            return null;
        }
        else {
            return xgc.toGregorianCalendar().getTime();
        }
    }
}
