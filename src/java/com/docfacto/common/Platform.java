package com.docfacto.common;

import java.awt.GraphicsEnvironment;

/**
 * Utilities for the Java Platform
 * 
 * @author dhudson - created 28 Aug 2013
 * @since 2.4
 */
public class Platform {

    private static final String PLATFORM = System.getProperty("os.name");

    /**
     * Constant OS dependent line separator
     */
    public static final String LINE_SEPARATOR = System
        .getProperty("line.separator");

    private Platform() {
        // Static methods only
    }

    /**
     * Check to see if there is a console
     * 
     * @return true if there is no console
     * @since 2.2
     */
    public static boolean isHeadless() {
        return GraphicsEnvironment.isHeadless();
    }

    /**
     * Check to see if running on a Mac
     * 
     * @return true if running on a Mac
     * @since 2.2
     */
    public static boolean isMac() {
        return (PLATFORM.startsWith("Mac"));
    }

    /**
     * Check to see if running on Windows
     * 
     * @return true if the platform is windows
     * @since 2.4
     */
    public static boolean isWindows() {
        return (PLATFORM.startsWith("Windows"));
    }

    /**
     * Get platform string
     * 
     * @return the platform string
     * @since 2.4
     */
    public static String getPlatform() {
        return PLATFORM;
    }
}
