package com.docfacto.common;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.StringTokenizer;

/**
 * Input/Output utilities that will be used across all of the Docfacto utilities
 * 
 * @author dhudson - created 21 May 2012
 * @since 2.0
 */
public final class IOUtils {

    /**
     * Constant {@value}
     */
    public final static String PARENT_PATH = "..";

    /**
     * Constant {@value}
     */
    public final static String SELF_PATH = ".";

    /**
     * Constant {@value}
     */
    private static final String DOT_SLASH = "."+File.separator;

    private static ClassLoader theClassLoader;

    /**
     * Constructor.
     */
    private IOUtils() {
    }

    /**
     * Return a class loader
     * 
     * @return a <code>class loader</code>
     * @since 2.1
     */
    public static ClassLoader getClassLoader() {
        if (theClassLoader==null) {
            synchronized (IOUtils.class) {
                if (theClassLoader==null) {
                    theClassLoader = IOUtils.class.getClassLoader();
                }
            }
        }

        return theClassLoader;
    }

    /**
     * Open for reading, a resource of the specified name from the search path
     * used to load classes.
     * 
     * @param resourceName for example a file name.
     * @return an input stream for the resource.
     * @throws DocfactoException if the resource could not be found.
     * @since 2.0
     */
    public static InputStream getResourceAsStream(String resourceName)
    throws DocfactoException {
        final ClassLoader classLoader = getClassLoader();

        InputStream stream = classLoader.getResourceAsStream(resourceName);
        if (stream==null) {
            stream = classLoader.getResourceAsStream("/"+resourceName);
        }

        if (stream==null) {
            throw new DocfactoException("Resource "+resourceName+" not found");
        }
        return stream;
    }

    /**
     * Locate and return a resource as a file.
     * 
     * @param resource to locate
     * @return a {@code File} of the resource
     * @throws DocfactoException if unable to locate resource
     * @since 2.0
     */
    public static File getResourceAsFile(String resource)
    throws DocfactoException {
        final ClassLoader classLoader = getClassLoader();
        final URL url = classLoader.getResource(resource);

        try {
            return new File(url.toURI());
        }
        catch (final URISyntaxException ex) {
            throw new DocfactoException(ex.getLocalizedMessage(),ex);
        }
    }

    /**
     * Create a file given the specified name and write the contents
     * 
     * @param fileName to create
     * @param contents to write
     * @throws IOException if an error occurs
     * @since 2.0
     */
    public static void writeFile(String fileName,String contents)
    throws IOException {
        final File outputFile =
            new File(fileName);

        writeFile(outputFile,contents);
    }

    /**
     * Create a file given the specified file and write the contents
     * 
     * @param file to create
     * @param contents to write
     * @throws IOException if a write error occurs
     * @since 2.1
     */
    public static void writeFile(File file,String contents) throws IOException {
        final BufferedWriter writer =
            new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
                file),
                "UTF-8"));

        writer.write(contents);
        close(writer);
    }

    /**
     * Close a closeable resource, catching and discarding any IOExceptions that
     * may arise.
     * 
     * @param resource to close
     * @since 2.0
     */
    public final static void close(Closeable resource) {
        if (resource==null) {
            return;
        }
        try {
            resource.close();
        }
        catch (final IOException ignore) {
        }
    }

    /**
     * Returns the file name of the given file without path or extension
     * 
     * @param file to process
     * @return The filename without path or extension
     * @since 2.0
     */
    public static String getFileBaseName(File file) {
        final String fullPath = file.getName();

        final int dot = fullPath.lastIndexOf('.');
        final int sep = fullPath.lastIndexOf(File.pathSeparatorChar);

        return fullPath.substring(sep+1,dot);
    }

    /**
     * Utility method to return the a file extension
     * 
     * @param file to process
     * @return the file extension
     * @since 2.0
     */
    public static String extension(File file) {
        final int dot = file.getName().lastIndexOf('.');
        return file.getName().substring(dot+1);
    }

    /**
     * Return the relative path from file 1 to file 2
     * 
     * @param file1 first file
     * @param file2 second file
     * @return the relative path between two files
     * @since 2.4
     */
    public static String getRelativePathForFiles(String file1,
    String file2) {
        File f1 = new File(file1);
        String parentDir = f1.getParentFile().getAbsolutePath();

        // File f2 = new File(file2);
        // String parent2 = f2.getParentFile().getAbsolutePath();

        String relPath = IOUtils.getRelativePath(parentDir,file2);

        if (relPath.startsWith(DOT_SLASH)) {
            // Remove the ./
            relPath = relPath.substring(2);
        }

        if (relPath.endsWith(File.separator)) {
            relPath = relPath.substring(0,relPath.length()-1);
        }

        return relPath;
    }

    /**
     * Return the relative position between two directories
     * 
     * @param absolutePath1 path 1
     * @param absolutePath2 path 2
     * @return the relative path between path1 and path2
     * @since 2.1
     */
    public static String getRelativePath(String absolutePath1,
    String absolutePath2) {
        final StringBuffer relativePath = new StringBuffer();
        final StringTokenizer tokenizer1 =
            new StringTokenizer(absolutePath1,File.separator);
        final StringTokenizer tokenizer2 =
            new StringTokenizer(absolutePath2,File.separator);
        // are there tokens?
        if (tokenizer1.hasMoreTokens()&&tokenizer2.hasMoreTokens()) {
            // are the first tokens (drive letters) equal?
            String token1 = tokenizer1.nextToken();
            String token2 = tokenizer2.nextToken();
            if (token1.equals(token2)) {
                int parentCount = 0;
                while (tokenizer1.hasMoreTokens()&&tokenizer2.hasMoreTokens()) {
                    token1 = tokenizer1.nextToken();
                    token2 = tokenizer2.nextToken();
                    if (!token1.equals(token2)) {
                        relativePath.append(File.separator);
                        relativePath.append(token2);
                        parentCount++;
                    }
                }
                // one or both are now out of tokens
                if (tokenizer1.hasMoreTokens()) {
                    parentCount += tokenizer1.countTokens();
                }
                else if (tokenizer2.hasMoreTokens()) {
                    while (tokenizer2.hasMoreTokens()) {
                        relativePath.append(File.separator);
                        relativePath.append(tokenizer2.nextToken());
                    }
                }
                // now append parent paths or self path
                if (parentCount>0) {
                    for (int index = 0;index<parentCount-1;index++) {
                        relativePath.insert(0,PARENT_PATH);
                        relativePath.insert(0,File.separator);
                    }
                    relativePath.insert(0,PARENT_PATH);
                }
                else {
                    relativePath.insert(0,SELF_PATH);
                }
                // add a path separator to the end of this
                relativePath.append(File.separator);
            }
            else {
                return (absolutePath2);
            }
        }
        return (relativePath.toString());
    }

    /**
     * Creates a reader for a named resource.
     * 
     * @param resourceName to create a reader for
     * @return a buffered reader for the named resource
     * @throws DocfactoException if the resource could not be found
     * @since 2.0
     */
    public static BufferedReader createResourceReader(
    String resourceName) throws DocfactoException {
        final InputStream inputStream = getResourceAsStream(resourceName);
        final BufferedReader reader = null;
        try {

            final InputStreamReader fr = new InputStreamReader(inputStream);
            final BufferedReader br = new BufferedReader(fr);

            return br;
        }
        catch (final Exception ex) {
            IOUtils.close(reader);
            throw new DocfactoException("Unable to create reader for resource "
                +resourceName,ex);
        }
    }

    /**
     * Creates a reader for a given file input stream. A character set of
     * <code>UTF-8</code> will be used
     * 
     * @param fileInputStream the file input stream to read.
     * @return a Line Number Reader
     * @throws DocfactoException if unable to create a reader for the given file
     * input stream.
     * @since 2.0
     */
    public static LineNumberReader createFileReader(
    InputStream fileInputStream) throws DocfactoException {
        try {
            final LineNumberReader reader =
                new LineNumberReader(new InputStreamReader(
                    fileInputStream,
                    "UTF-8"));
            return reader;
        }
        catch (final Exception ex) {
            throw new DocfactoException(
                "Unable to create reader for file input stream "
                    +fileInputStream,
                ex);
        }
    }

    /**
     * Creates a reader for a given file path. A character set of {@code UTF-8}
     * will be used.
     * 
     * @param filePath to read
     * @return a LineNumberReader
     * @throws DocfactoException if the path is invalid, or unable to read the
     * file
     * @since 2.3
     */
    public static LineNumberReader createFileReader(String filePath)
    throws DocfactoException {
        try {
            return createFileReader(new FileInputStream(new File(filePath)));
        }
        catch (FileNotFoundException ex) {
            throw new DocfactoException("File not found "+filePath,ex);
        }
    }

    /**
     * Count the number of lines in a file.
     * 
     * @param path of the file
     * @return number of lines in a file
     * @throws DocfactoException if unable to read the file
     * @throws IOException if the file doesn't exist
     * @since 2.2
     */
    public static int getLineCount(String path) throws DocfactoException,
    IOException {
        LineNumberReader reader = null;
        try {
            reader = createFileReader(new FileInputStream(new File(path)));
            int lines = 0;
            while ((reader.readLine())!=null) {
                lines++;
            }
            return lines;
        }
        finally {
            close(reader);
        }
    }

    /**
     * Create a LineNumberReader for a given String
     * 
     * @param content to read
     * @return a LineNumberReader
     * @since 2.2
     */
    public static LineNumberReader createStringReader(String content) {
        return new LineNumberReader(new StringReader(content));
    }

    /**
     * Reads the entire contents of a file into a String.
     * 
     * @param file the file to read.
     * @return a string containing the entire file content.
     * @throws DocfactoException if unable to read the file.
     * @since 2.0
     */
    public static String readFileAsString(File file)
    throws DocfactoException {

        if (file==null) {
            throw new DocfactoException("Null file parameter");
        }

        try {
            final LineNumberReader reader =
                createFileReader(new FileInputStream(file));

            int x;
            final char[] data = new char[(int)file.length()];
            int index = 0;

            while ((x = reader.read())!=-1) {
                data[index++] = (char)x;
            }
            reader.close();
            final String retString = new String(data);

            return retString;
        }
        catch (final Exception ex) {
            throw new DocfactoException("Unable to read file "+file,ex);
        }
    }

    /**
     * Copy a source file {@code fromFile} to a destination file {@code toFile}
     * 
     * @param fromFile source file
     * @param toFile destination file
     * @throws DocfactoException if unable to copy the file
     * @since 2.0
     */
    public static void copyFile(File fromFile,File toFile)
    throws DocfactoException {

        // open the input file
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(fromFile);
        }
        catch (final FileNotFoundException ex) {
            close(fis);
            throw new DocfactoException("Unable to open file "
                +fromFile.getAbsolutePath());
        }
        catch (final SecurityException ex) {
            close(fis);
            throw new DocfactoException("Unable to access file "
                +fromFile.getAbsolutePath()
                +" - "
                +ex.getMessage());
        }

        // open the output file (creating if necessary)
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(toFile);
        }
        catch (final Exception ex) {
            close(fis);
            close(fos);
            throw new DocfactoException("Unable to open file "
                +toFile.getAbsolutePath()
                +" for output");
        }

        try {
            copyStream(fis,fos);
        }
        catch (final DocfactoException ex) {
            throw new DocfactoException("Failure copying "
                +fromFile
                +" to "
                +toFile
                +" - "
                +ex.getMessage());
        }
        finally {
            close(fis);
            close(fos);
        }
    }

    /**
     * Copy the input stream to the output stream
     * 
     * @param inputStream inbound stream
     * @param outputStream outbound stream
     * @throws DocfactoException if unable to copy streams
     * @since 2.0
     */
    public static void copyStream(InputStream inputStream,
    OutputStream outputStream) throws DocfactoException {

        final BufferedInputStream bis = new BufferedInputStream(inputStream);
        final BufferedOutputStream bos = new BufferedOutputStream(outputStream);

        try {
            int fileByte = 0;
            final byte[] byteArray = new byte[1024];

            while (fileByte!=-1) {
                fileByte = bis.read(byteArray);
                if (fileByte!=-1) {
                    bos.write(byteArray,0,fileByte);
                }
            }
            bos.flush();
        }
        catch (final IOException ex) {
            throw new DocfactoException("Failure copying streams "
                +ex.getMessage(),ex);
        }
        finally {
            close(bos);
            close(bis);
        }
    }

    /**
     * Reads the input stream to a String
     * 
     * @param inputStream to process
     * @return A {@code String} representation of the input stream
     * @throws DocfactoException if there is an error reading the input
     * @since 2.5
     */
    public static String inputStreamAsString(InputStream inputStream)
    throws DocfactoException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        copyStream(inputStream,baos);
        return new String(baos.toByteArray());
    }

    /**
     * Copy a source folder contents to a destination folder contents.
     * 
     * If the destination folder does not exist, it will be created.
     * 
     * @param sourceFolder folder containing the source
     * @param destFolder folder to copy to
     * @param includeSubDirs copy any sub dirs as well
     * @throws DocfactoException if unable to copy files
     * @since 2.3
     */
    public static void copyFolder(File sourceFolder,File destFolder,
    boolean includeSubDirs) throws DocfactoException {
        if (!destFolder.exists()) {
            destFolder.mkdir();
        }

        File[] files = sourceFolder.listFiles();

        for (File file:files) {
            File destFile = new File(destFolder,file.getName());
            if (file.isFile()) {
                copyFile(file,destFile);
            }
            else if (file.isDirectory()&&includeSubDirs) {
                copyFolder(file,destFile,includeSubDirs);
            }
        }
    }

    /**
     * Load the contents of the URL
     * 
     * @param url to process
     * @return the contents of the URL
     * @throws DocfactoException if an {@code IOException} occurs
     * @since 2.2
     */
    public static String loadURL(URL url) throws DocfactoException {
        BufferedReader reader = null;
        try {
            reader =
                new BufferedReader(new InputStreamReader(
                    url.openStream()));
            final StringBuffer buffer = new StringBuffer(1500);
            String line = reader.readLine();
            while (line!=null) {
                buffer.append(line);
                buffer.append(Platform.LINE_SEPARATOR);
                line = reader.readLine();
            }
            return buffer.toString();
        }
        catch (final IOException ex) {
            throw new DocfactoException("Unload to load URL ["+url+"] ");
        }
        finally {
            close(reader);
        }
    }
}
