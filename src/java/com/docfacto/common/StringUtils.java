/*
 * @author dhudson -
 * Created 20 Nov 2012 : 10:36:36
 */

package com.docfacto.common;

/**
 * Collection of static String utils.
 * 
 * @author dhudson - created 5 Dec 2012
 * @since 2.1
 */
public class StringUtils {

    /**
     * Create a new instance of <code>StringUtils</code>.
     */
    private StringUtils() {
    }

    /**
     * Escape XML characters. Suggested by hussein_shafie
     * 
     * @param s value needed to be escaped
     * @return escaped value
     * @since 2.1
     */
    public static String escapeXML(final String s) {
        final char[] chars = s.toCharArray();
        return escapeXML(chars,0,chars.length);
    }

    /**
     * Escape XML characters.
     * 
     * @param chars char arrays
     * @param offset start position
     * @param length arrays length
     * @return escaped value
     * @since 2.1
     */
    public static String escapeXML(final char[] chars,final int offset,
    final int length) {
        final StringBuffer escaped = new StringBuffer(length);

        final int end = offset+length;
        for (int i = offset;i<end;++i) {
            final char c = chars[i];

            switch (c) {
            case '\'':
                escaped.append("&apos;");
                break;
            case '\"':
                escaped.append("&quot;");
                break;
            case '<':
                escaped.append("&lt;");
                break;
            case '>':
                escaped.append("&gt;");
                break;
            case '&':
                // Get what is left
                final String sub = new String(chars,i,(end-i));
                if (sub.startsWith("&lt;")||sub.startsWith("&gt;")||
                    sub.startsWith("&quot;")||sub.startsWith("&apos;")||
                    sub.startsWith("&nbsp;")) {
                    // Already escaped, so lets just add it
                    escaped.append(c);
                }
                else {
                    escaped.append("&amp;");
                }
                break;
            default:
                escaped.append(c);
            }
        }

        return escaped.toString();
    }

    /**
     * Encode byte array of data to a Base64 encoded String
     * 
     * @param data to encode
     * @return a Base64 encoded string
     * @since 2.0
     */
    public static String base64Encode(byte[] data) {
        return javax.xml.bind.DatatypeConverter.printBase64Binary(data);
    }

    /**
     * Decode a Base64 Encoded String
     * 
     * @param data to decode
     * @return a Base64 decoded byte array
     * @since 2.0
     */
    public static byte[] base64Decode(String data) {
        return javax.xml.bind.DatatypeConverter.parseBase64Binary(data);
    }

    /**
     * Convert a boolean value to either yes or no
     * 
     * @param value to convert
     * @return yes or no
     * @since 2.1
     */
    public static String booleanYesNo(boolean value) {
        if (value) {
            return "yes";
        }
        return "no";
    }

    /**
     * Returns the index the first occurrence of the substring within the
     * supplied string, from the given position, with the case ignored.
     * 
     * @param string to search
     * @param substring to find
     * @param fromIndex from position
     * @return offset of the substring or -1 if the substring is not found
     * @since 2.1
     */
    public static int indexOfIgnoreCase(String string,String substring,
    int fromIndex) {
        for (int i = fromIndex;i<string.length();i++) {
            if (startsWithIgnoreCase(string,substring,i))
                return i;
        }
        return -1;
    }

    /**
     * Returns the index of the first occurrence of the substring within the
     * supplied string, with case ignored.
     * 
     * @param string to search
     * @param substring to find
     * @return offset of substring or -1 if substring not found
     * @since 2.1
     */
    public static int indexOfIgnoreCase(String string,String substring) {
        return substring.toLowerCase().indexOf(string.toLowerCase());
    }

    /**
     * Tests to see if the sub string can be found in the supplied string at the
     * given index, ignoring case.
     * 
     * @param string to search
     * @param substring to find
     * @param fromIndex where from
     * @return true is the substring is found in the string from the index
     * @since 2.1
     */
    public static boolean startsWithIgnoreCase(String string,String substring,
    int fromIndex) {
        if ((fromIndex<0)||((fromIndex+substring.length())>string.length())) {
            return false;
        }

        for (int i = 0;i<substring.length();i++)
            if (Character.toUpperCase(string.charAt(fromIndex+i))!=Character
                .toUpperCase(substring.charAt(i)))
                return false;
        return true;
    }

    /**
     * Tests if the supplied string starts with the specified prefix, but the
     * case is ignored
     * 
     * @param string to search
     * @param substring to find
     * @return true if the string starts with the substring
     * @since 2.1
     */
    public static boolean startsWithIgnoreCase(String string,String substring) {
        return startsWithIgnoreCase(string,substring,0);
    }

    /**
     * Tests if a string is blank: null, empty, or only whitespace (" ", \r\n,
     * \t, etc)
     * 
     * @param string string to test
     * @return if string is blank
     * @since 2.1
     */
    public static boolean isBlank(String string) {
        if (string==null||string.length()==0)
            return true;

        final int l = string.length();
        for (int i = 0;i<l;i++) {
            if (Character.isWhitespace(string.codePointAt(i)))
                return false;
        }
        return true;
    }

    /**
     * Removes sequences of white space, leaving just the one
     * 
     * @param string to process
     * @return a string with many white spaces replaced with just one
     * @since 2.1
     */
    public static String normaliseWhitespace(String string) {
        final StringBuilder sb = new StringBuilder(string.length());

        boolean lastWasWhite = false;
        boolean modified = false;

        final int l = string.length();
        int c;
        for (int i = 0;i<l;i += Character.charCount(c)) {
            c = string.codePointAt(i);
            if (Character.isWhitespace(c)) {
                if (lastWasWhite) {
                    modified = true;
                    continue;
                }

                if (c!=' ') {
                    modified = true;
                }

                sb.append(' ');
                lastWasWhite = true;
            }
            else {
                sb.appendCodePoint(c);
                lastWasWhite = false;
            }
        }
        return modified ? sb.toString() : string;
    }

    /**
     * NMTokens only allow certain characters, this method will either rip out
     * or replace invalid characters
     * 
     * @param string to process
     * @return an NMToken safe string
     * @since 2.1
     */
    public static String normaliseForNMToken(char[] string) {

        final StringBuilder result = new StringBuilder(string.length);
        // Lets replace spaces and ()..
        for (int i = 0;i<string.length;i++) {
            switch (string[i]) {
            case ' ':
            case '[':
            case ']':
            case '<':
            case '>':
            case ',':
                // remove
                break;

            case ')':
            case '(':
            case '?':
                result.append("-");
                break;

            default:
                result.append(string[i]);
            }
        }

        return result.toString();
    }

    /**
     * Locate the file extension from this file path
     * 
     * @param filePath to locate the extension
     * @return the file extension or null
     * @since 2.2
     */
    public static String getExtension(String filePath) {
        if(filePath == null) {
            return null;
        }
        
        int index = filePath.lastIndexOf(".");
        if (index!=-1) {
            return filePath.substring(index+1);
        }

        return null;
    }

    /**
     * Capitalise the first letter of the string
     * 
     * @param original string to process
     * @return A capitalised string
     * @since 2.4
     */
    public static String capitaliseFirstLetter(String original) {
        if (original.length()==0) {
            return original;
        }
        return original.substring(0,1).toUpperCase()+original.substring(1);
    }

    /**
     * Check to see if the string is null or empty
     * 
     * @param string to check
     * @return true if the string is null or empty
     * @since 2.4
     */
    public static boolean nullOrEmpty(String string) {
        if (string==null) {
            return true;
        }

        return string.isEmpty();
    }

    /**
     * Return an elipsed "..." string if the string length is larger than the
     * specified length.
     * 
     * @param length of the result string
     * @param source string
     * @return A string of the desired length with ... appended
     * @since 2.4
     */
    public static String elipseString(int length,String source) {
        if (source.length()<=length) {
            return source;
        }

        return source.substring(0,length-3)+"...";
    }
}
