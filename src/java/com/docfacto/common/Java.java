package com.docfacto.common;

/**
 * Simple wrapper class around the Java system properties.
 * 
 * java.runtime.version
 * 
 * @author dhudson - created 20 Mar 2013
 * @since 2.2
 */
public class Java {

    private final String theVersion;
    private final String theVendor;
    // private final int theMajorVersion;
    private int theMinorVersion;
    private int theBuildVersion;

    /**
     * Constructor for JavaVersion
     * 
     * @since 2.2
     * 
     */
    public Java() {
        theVersion = System.getProperties().getProperty("java.version");
        theVendor = System.getProperties().getProperty("java.vendor");
        try {
            theMinorVersion = new Integer(theVersion.substring(2,3)).intValue();
        }
        catch (NumberFormatException ignore) {
            theMinorVersion = -1;
        }

        try {
            int index = theVersion.indexOf("_");
            if (index!=-1) {
                index += 1;
                int dashIndex = theVersion.indexOf("-");
                if (dashIndex==-1) {
                    theBuildVersion =
                        new Integer(theVersion.substring(index));
                }
                else {
                    theBuildVersion =
                        new Integer(theVersion.substring(index,dashIndex));
                }
            }
            else {
                theBuildVersion = 0;
            }

        }
        catch (NumberFormatException ignore) {
            theBuildVersion = -1;
        }
    }

    /**
     * Return 5, 6 or 7 depending on the running java version
     * 
     * @return the minor version
     * @since 2.2
     */
    public int getMinorVersion() {
        return theMinorVersion;
    }

    /**
     * Return the build version
     * 
     * @return the build version
     * @since 2.4
     */
    public int getBuildVersion() {
        return theBuildVersion;
    }

    /**
     * Return the system property java.version
     * 
     * @return the version string
     * @since 2.2
     */
    public String getVersion() {
        return theVersion;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return theVendor +" : " + theVersion;
    }

    /**
     * Return the system property java.vendor
     * 
     * @return the vendor string
     * @since 2.2
     */
    public String getVendor() {
        return theVendor;
    }
}
