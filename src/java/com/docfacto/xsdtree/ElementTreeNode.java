package com.docfacto.xsdtree;

import javax.xml.namespace.QName;

import com.sun.xml.xsom.XSAnnotation;
import com.sun.xml.xsom.XSType;
import com.sun.xml.xsom.impl.ElementDecl;

/*
 * @author dhudson -
 * Created 11 Apr 2012 : 10:58:20
 */

/**
 * This class represents a XML element in the XSD Tree. This node will also have
 * a <code>SimpleType</code> as the element type. It is also possible to get at
 * the <code>ElementDecl</code> used to create this node.
 * 
 * @author dhudson - created 21 Apr 2012
 * @since 1.1
 */
public class ElementTreeNode extends RangeTreeNode {

    /**
     * Constant {@value}
     */
    private static final long serialVersionUID = 1L;

    /**
     * Holds the Element Declaration
     */
    private final ElementDecl theElementDecl;

    /**
     * Holds the Simple type
     */
    private SimpleType theSimpleType;

    /**
     * Element Type QNAME
     */
    private QName theTypeQName;

    /**
     * Creates a new instance of <code>ElementTreeNode</code>.
     * 
     * @param elementDecl to construct the node
     * @param minOccurs for the element
     * @param maxOccurs for the element
     */
    ElementTreeNode(ElementDecl elementDecl,int minOccurs,int maxOccurs) {
        super(elementDecl.getTargetNamespace(),elementDecl.getName(),minOccurs,
            maxOccurs);
        theElementDecl = elementDecl;
        parse();
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#parse()
     */
    @Override
    void parse() {
        final XSAnnotation annotation =
        theElementDecl.getTerm().getAnnotation();

        if (annotation!=null) {
            setAnnotation(annotation.getAnnotation());
        }

        final XSType type = theElementDecl.getType();
        if (type.getName()==null) {
            // Anonymous or ref=".."
            theTypeQName =
            new QName(type.getTargetNamespace(),theElementDecl.getName());
        }
        else {
            theTypeQName = new QName(type.getTargetNamespace(),type.getName());
        }
    }

    /**
     * @see SchemaTreeNode#isElementTreeNode()
     */
    @Override
    public boolean isElementTreeNode() {
        return true;
    }

    /**
     * @see SchemaTreeNode#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder(100);
        builder.append("ElementTreeNode : ");
        builder.append(getNodeName());
        builder.append(" ");
        builder.append(getRangeString());
        builder.append(" Type: ");
        builder.append(getTypeName());

        return builder.toString();
    }

    /**
     * Set simple type
     * 
     * @param simpleType for this element node
     * @since 1.1
     */
    void setSimpleType(SimpleType simpleType) {
        theSimpleType = simpleType;
    }

    /**
     * Returns a encapsulated type for this xml element
     * 
     * @return a <code>SimpleType</code>
     * @since 1.1
     */
    public SimpleType getSimpleType() {
        return theSimpleType;
    }

    /**
     * Return the element declaration associated with this node
     * 
     * @return the elementDecl
     * @since 1.1
     */
    public ElementDecl getElementDecl() {
        return theElementDecl;
    }

    /**
     * Return the simple type name, if exists or the type QName as a string
     * 
     * @return string representation of the type
     * @since 1.4
     */
    public String getTypeName() {
        if (theSimpleType!=null) {
            return theSimpleType.getName();
        }
        else {
            return theTypeQName.getLocalPart();
        }
    }

    /**
     * This is an element with a complex type
     * 
     * @return the type QName,
     * @since 1.2
     */
    public QName getTypeQName() {
        return theTypeQName;
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#clone()
     */
    @Override
    public Object clone() {
        return new ElementTreeNode(theElementDecl,getMinOccurs(),getMaxOccurs());
    }
}
