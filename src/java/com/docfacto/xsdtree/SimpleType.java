package com.docfacto.xsdtree;

import java.util.Iterator;
import java.util.Vector;

import com.sun.xml.xsom.XSFacet;
import com.sun.xml.xsom.XSRestrictionSimpleType;
import com.sun.xml.xsom.XSSimpleType;

/*
 * @author dhudson -
 * Created 11 Apr 2012 : 10:54:43
 */

/**
 * This class is a helper class for an element simple type. An element can have
 * a simple type, which may be based on restrictions.
 * 
 * @author dhudson - created 21 Apr 2012
 * @since 1.1
 */
public class SimpleType extends SchemaTreeNode {

    /**
     * Constant {@value}
     */
    private static final long serialVersionUID = 1L;
    /**
     * SimpleType from JAXB
     */
    private final XSSimpleType theSimpleType;

    /**
     * Restriction enumeration
     */
    private String[] theEnumeration = null;
    /**
     * Restriction max value
     */
    private String theMaxValue = null;
    /**
     * Restriction min value
     */
    private String theMinValue = null;
    /**
     * Restriction length
     */
    private String theLength = null;
    /**
     * Restriction max length
     */
    private String theMaxLength = null;
    /**
     * Restriction min length
     */
    private String theMinLength = null;
    /**
     * Restriction pattern
     */
    private String thePattern = null;
    /**
     * Restriction total digits
     */
    private String theTotalDigits = null;

    /**
     * Create a new instance of <code>SimpleType</code>.
     * 
     * @param simpleType to construct this node
     */
    public SimpleType(XSSimpleType simpleType) {
        super(simpleType.getTargetNamespace(),simpleType.getName());
        theSimpleType = simpleType;
        parse();
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#parse()
     */
    @Override
    void parse() {

        if (theSimpleType.getAnnotation()!=null) {
            setAnnotation(theSimpleType.getAnnotation().getAnnotation());
        }

        if (theSimpleType.isRestriction()) {
            final XSRestrictionSimpleType restriction =
            theSimpleType.asRestriction();

            final Vector<String> enumeration = new Vector<String>();
            final Iterator<? extends XSFacet> i =
            restriction.getDeclaredFacets().iterator();

            while (i.hasNext()) {
                final XSFacet facet = i.next();
                if (facet.getName().equals(XSFacet.FACET_ENUMERATION)) {
                    enumeration.add(facet.getValue().value);
                }
                if (facet.getName().equals(XSFacet.FACET_MAXINCLUSIVE)) {
                    theMaxValue = facet.getValue().value;
                }
                if (facet.getName().equals(XSFacet.FACET_MININCLUSIVE)) {
                    theMinValue = facet.getValue().value;
                }
                if (facet.getName().equals(XSFacet.FACET_MAXEXCLUSIVE)) {
                    theMaxValue =
                    String
                    .valueOf(Integer.parseInt(facet.getValue().value)-1);
                }
                if (facet.getName().equals(XSFacet.FACET_MINEXCLUSIVE)) {
                    theMinValue =
                    String
                    .valueOf(Integer.parseInt(facet.getValue().value)+1);
                }
                if (facet.getName().equals(XSFacet.FACET_LENGTH)) {
                    theLength = facet.getValue().value;
                }
                if (facet.getName().equals(XSFacet.FACET_MAXLENGTH)) {
                    theMaxLength = facet.getValue().value;
                }
                if (facet.getName().equals(XSFacet.FACET_MINLENGTH)) {
                    theMinLength = facet.getValue().value;
                }
                if (facet.getName().equals(XSFacet.FACET_PATTERN)) {
                    thePattern = facet.getValue().value;
                }
                if (facet.getName().equals(XSFacet.FACET_TOTALDIGITS)) {
                    theTotalDigits = facet.getValue().value;
                }
            }
            if (enumeration.size()>0) {
                theEnumeration = enumeration.toArray(new String[] {});
            }

        }
    }

    /**
     * Return the XSSimple type associated with this node
     * 
     * @return the XSSimple type
     * @since 1.1
     */
    public XSSimpleType getSimpleType() {
        return theSimpleType;
    }

    /**
     * Return the enumeration or null
     * 
     * @return the enumeration
     * @since 1.1
     */
    public String[] getEnumeration() {
        return theEnumeration;
    }

    /**
     * Returns true if node has enumerations
     * 
     * @return true if node has enumerations
     * @since 1.1
     */
    public boolean hasEnumeration() {
        if (theEnumeration==null) {
            return false;
        }

        return true;
    }

    /**
     * Return the maximum value or null
     * 
     * @return the maximum value
     * @since 1.1
     */
    public String getMaxValue() {
        return theMaxValue;
    }

    /**
     * Returns true if has max value
     * 
     * @return true if has max value
     * @since 1.1
     */
    public boolean hasMaxValue() {
        if (theMaxValue==null) {
            return false;
        }

        return true;
    }

    /**
     * Return the minimum value
     * 
     * @return the minimum value
     * @since 1.1
     */
    public String getMinValue() {
        return theMinValue;
    }

    /**
     * Return true if has min value
     * 
     * @return true if has min value
     * @since 1.1
     */
    public boolean hasMinValue() {
        if (theMinValue==null) {
            return false;
        }

        return true;
    }

    /**
     * Returns the length or null
     * 
     * @return the length or null
     * @since 1.1
     */
    public String getLength() {
        return theLength;
    }

    /**
     * Return true if has length
     * 
     * @return true if has length
     * @since 1.1
     */
    public boolean hasLength() {
        if (theLength==null) {
            return false;
        }

        return true;
    }

    /**
     * Return the maximum length or null
     * 
     * @return the maximum length
     * @since 1.1
     */
    public String getMaxLength() {
        return theMaxLength;
    }

    /**
     * Returns true if has max length
     * 
     * @return true if has max length
     * @since 1.1
     */
    public boolean hasMaxLength() {
        if (theMaxLength==null) {
            return false;
        }

        return true;
    }

    /**
     * Returns the minimum length or null
     * 
     * @return the minimum length
     * @since 1.1
     */
    public String getMinLength() {
        return theMinLength;
    }

    /**
     * Returns true if has min length
     * 
     * @return true if has min length
     * @since 1.1
     */
    public boolean hasMinLength() {
        if (theMinLength==null) {
            return false;
        }

        return true;
    }

    /**
     * Return the pattern or null
     * 
     * @return pattern or null
     * @since 1.1
     */
    public String getPattern() {
        return thePattern;
    }

    /**
     * Returns true if has pattern
     * 
     * @return true if has pattern
     * @since 1.1
     */
    public boolean hasPattern() {
        if (thePattern==null) {
            return false;
        }

        return true;
    }

    /**
     * Return the total number of digits
     * 
     * @return the total number of digits
     * @since 1.1
     */
    public String getTotalDigits() {
        return theTotalDigits;
    }

    /**
     * Return true if has total digits
     * 
     * @return true if has total digits
     * @since 1.1
     */
    public boolean hasTotalDigits() {
        if (theTotalDigits==null) {
            return false;
        }
        return true;
    }
    
    /**
     * Return the simple type name.
     * 
     * @return the name
     * @since 2.5
     */
    public String getName() {
        return theSimpleType.getName();
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder(100);
        builder.append(theSimpleType.getName());

        if (hasEnumeration()) {
            builder.append(" Enumeration ");
            for (final String enumneration:getEnumeration()) {
                builder.append(enumneration);
                builder.append("|");
            }
        }

        if (hasLength()) {
            builder.append(" Length ");
            builder.append(theLength);
        }

        if (hasMaxLength()) {
            builder.append(" Max Length ");
            builder.append(theMaxLength);
        }

        final String typeName = theSimpleType.getName();

        // There has to be a better way of doing this, this needs to be re-visited
        // to see if these are synthetic restrictions, or user restrictions
        if (!typeName.equals("int")) {

            if (hasMinValue()) {
                builder.append(" Min Value ");
                builder.append(theMinValue);
            }
            if (hasMaxValue()) {
                builder.append(" Max Value ");
                builder.append(theMaxValue);
            }
        }

        if (hasMinLength()) {
            builder.append(" Min Length ");
            builder.append(theMinLength);
        }

        if (hasPattern()) {
            builder.append(" Pattern ");
            builder.append(thePattern);
        }

        return builder.toString();
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#clone()
     */
    @Override
    public Object clone() {
        return new SimpleType(theSimpleType);
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#isSimpleTreeNode()
     */
    @Override
    public boolean isSimpleTreeNode() {
        return true;
    }
}
