/*
 * @author dhudson -
 * Created 18 Apr 2012 : 15:40:25
 */

package com.docfacto.xsdtree;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.namespace.QName;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.XMLUtils;
import com.docfacto.xml.XSDAnnotationFactory;
import com.docfacto.xsd2common.XSDArgumentParser;
import com.sun.xml.xsom.XSAttGroupDecl;
import com.sun.xml.xsom.XSComplexType;
import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSModelGroupDecl;
import com.sun.xml.xsom.XSSchema;
import com.sun.xml.xsom.XSSchemaSet;
import com.sun.xml.xsom.XSSimpleType;
import com.sun.xml.xsom.XSType;
import com.sun.xml.xsom.impl.Const;
import com.sun.xml.xsom.impl.ElementDecl;
import com.sun.xml.xsom.parser.XSOMParser;

/**
 * The XSDTreeTraverser reads the schema (xsd file) and then builds a
 * hierarchical tree of what the resulting XML file will look like. The tree
 * will contain information about the XML elements (elements, complex and
 * simple) as well as annotations.
 * 
 * {@docfacto.note title="Warning" If a complex section, references itself, then
 * the parser will stop at 10 levels deep }
 * 
 * {@docfacto.example title="Example usage"
 * uri="com.docfacto.xsdtree.XSDTreeDumper" }
 * 
 * 
 * @author dhudson - created 21 Apr 2012
 * @since 1.1
 */
public class XSDTreeTraverser {

    /**
     * Map of all of the possible types loaded from schemas
     */
    private HashMap<QName,SchemaTreeNode> theTypes = null;

    /**
     * The root node
     */
    private XmlRootTreeNode theRootNode;

    private QName theRootQName;

    private boolean isDebugging = false;

    /**
     * Create a new instance of <code>XSDTreeTraverser</code>.
     * 
     * @since 1.1
     */
    public XSDTreeTraverser() {
        // Lets make sure that we know what we are dealing with
        XMLUtils.setSunInternalXmlParsers();
    }

    /**
     * Constructor.
     * 
     * @param parser argument parser
     * @since 2.5
     */
    public XSDTreeTraverser(XSDArgumentParser parser) {
        this();

        if (parser.hasRootNode()) {
            if (parser.hasNamespace()) {
                setRootElement(parser.getNamespace(),
                    parser.getRootNode());
            }
            else {
                setRootElement(parser.getRootNode());
            }
        }
    }

    /**
     * Set true for console debugging
     * 
     * @param value true to enable
     * @since 2.0
     */
    public void setDebug(boolean value) {
        isDebugging = value;
    }

    /**
     * Set the root node if not the first element in the schema
     * 
     * @param rootName qualified QName
     * @since 2.2
     */
    public void setRootElement(QName rootName) {
        theRootQName = rootName;
    }

    /**
     * Create a QName with just the local part supplied, the namespace will be
     * set to {@code XMLConstants.NULL_NS_URI}
     * 
     * @param localPart local name
     * @since 2.2
     */
    public void setRootElement(String localPart) {
        setRootElement(new QName(localPart));
    }

    /**
     * Set the root element, with a namespace and localpart
     * 
     * @param namespaceURI namespace
     * @param localPart local name
     * @since 2.2
     */
    public void setRootElement(String namespaceURI,String localPart) {
        setRootElement(new QName(namespaceURI,localPart));
    }

    /**
     * Parse the schema set and create a tree
     * 
     * @param schemaSet Schema Set to process
     * @since 2.0
     */
    public void visit(XSSchemaSet schemaSet) {
        process(schemaSet);
    }

    /**
     * visit
     * 
     * Parse the File, which should be an XSD. This file can also contain
     * imports
     * 
     * @param file to process
     * @throws DocfactoException if unable to process the XSD
     * @since 1.1
     */
    public void visit(File file) throws DocfactoException {

        if (!file.exists()) {
            throw new DocfactoException("Unable to find file ["+file.getPath()+
                "]");
        }

        final XSOMParser parser = getXSOMParser();

        XSSchemaSet sset = null;
        try {
            parser.parse(file);
            sset = parser.getResult();
        }
        catch (final Throwable t) {
            throw new DocfactoException("Unable to parse file "+t.getMessage(),
                t);
        }

        if (sset==null) {
            throw new DocfactoException("Unable to parse schema");
        }

        process(sset);

    }

    /**
     * Parse the input stream, which should be a XSD
     * 
     * @param inputStream to process
     * @throws DocfactoException if unable to process the XSD
     * @since 2.2
     */
    public void visit(InputStream inputStream) throws DocfactoException {
        final XSOMParser parser = getXSOMParser();

        XSSchemaSet sset = null;
        try {
            parser.parse(inputStream);
            sset = parser.getResult();
        }
        catch (final Throwable t) {
            throw new DocfactoException("Unable to parse input stream "+
                t.getMessage(),
                t);
        }

        if (sset==null) {
            throw new DocfactoException("Unable to parse schema");
        }

        process(sset);
    }

    /**
     * Create a XSOM parse with annotation factory
     * 
     * @return a XSOM Parser with the correct annotation factory
     * @since 2.2
     */
    private XSOMParser getXSOMParser() {
        final XSOMParser parser = new XSOMParser();
        // We need annotations
        parser.setAnnotationParser(new XSDAnnotationFactory());

        return parser;
    }

    /**
     * Process the SchemaSet and generate a XSDTree from it
     * 
     * @param sset Schema Set to process
     * @since 2.0
     */
    private void process(XSSchemaSet sset) {

        theTypes = new HashMap<QName,SchemaTreeNode>();
        theRootNode = null;

        // iterate each XSSchema object. XSSchema is a per-namespace schema.
        final Iterator<XSSchema> itr = sset.iterateSchema();
        while (itr.hasNext()) {
            final XSSchema s = itr.next();

            // The standard stuff, we want the simple types
            if (s.getTargetNamespace().equals(Const.schemaNamespace)) {
                final Iterator<XSSimpleType> mySimpleTypes =
                    s.iterateSimpleTypes();
                while (mySimpleTypes.hasNext()) {
                    final XSSimpleType simpleType = mySimpleTypes.next();
                    final SimpleType simpleTreeNode =
                        new SimpleType(simpleType);
                    theTypes.put(simpleTreeNode.getQName(),simpleTreeNode);
                }

                continue;
            }

            final Iterator<XSElementDecl> jtr = s.iterateElementDecls();

            while (jtr.hasNext()) {
                final XSElementDecl e = jtr.next();

                final XSType type = e.getType();

                if (type.isComplexType()) {

                    final ComplexTreeNode comp = new ComplexTreeNode(e);

                    if (isDebugging) {
                        System.out.println("Adding complex node "+
                            comp.getQName());
                    }

                    theTypes.put(comp.getQName(),comp);
                }
                else if (type.isSimpleType()) {
                    // Its a simple Type..
                    final ElementTreeNode elTreeNode =
                        new ElementTreeNode((ElementDecl)e,0,0);

                    if (isDebugging) {
                        System.out.println("Adding simple node "+
                            elTreeNode.getQName());
                    }
                    theTypes.put(elTreeNode.getQName(),elTreeNode);
                }
            }

            final Iterator<XSComplexType> myItt = s.iterateComplexTypes();
            while (myItt.hasNext()) {
                final XSComplexType complexType = myItt.next();

                final ComplexTreeNode complexTreeNode =
                    new ComplexTreeNode(complexType);

                if (isDebugging) {
                    System.out.println("Adding complex node "+
                        complexTreeNode.getQName());
                }

                theTypes.put(complexTreeNode.getQName(),complexTreeNode);
            }

            // Simple Types
            final Iterator<XSSimpleType> mySimpleTypes = s.iterateSimpleTypes();
            while (mySimpleTypes.hasNext()) {
                final XSSimpleType simpleType = mySimpleTypes.next();
                final SimpleType simpleTreeNode = new SimpleType(simpleType);

                if (isDebugging) {
                    System.out.println("Adding simple type "+
                        simpleTreeNode.getQName());
                }

                theTypes.put(simpleTreeNode.getQName(),simpleTreeNode);
            }

            // Groups
            final Iterator<XSModelGroupDecl> modelGroups =
                s.iterateModelGroupDecls();
            while (modelGroups.hasNext()) {
                final XSModelGroupDecl modelGroupDecl = modelGroups.next();

                GroupTreeNode groupTreeNode = new GroupTreeNode(modelGroupDecl);

                if (isDebugging) {
                    System.out.println("Adding group "+
                        modelGroupDecl.getName());
                }

                theTypes.put(groupTreeNode.getQName(),groupTreeNode);
            }

            // Attribute Groups
            final Iterator<XSAttGroupDecl> attributeGroups =
                s.iterateAttGroupDecls();
            while (attributeGroups.hasNext()) {
                final XSAttGroupDecl attributeGroup = attributeGroups.next();
                AttributeGroupTreeNode treeNode =
                    new AttributeGroupTreeNode(attributeGroup);

                if (isDebugging) {
                    System.out.println("Adding attribute group  "+
                        treeNode.getQName());
                }

                theTypes.put(treeNode.getQName(),treeNode);
            }
        }

        SchemaTreeNode node = null;

        if (theRootQName!=null) {

            node = theTypes.get(theRootQName);
            if (node!=null) {
                theRootNode = new XmlRootTreeNode(node);
            }
            else {
                System.out.println("Unable to find root node ["+theRootQName+
                    "]");
            }
        }
        else if (theRootNode!=null) {
            node =
                theTypes.get(theRootNode.getChildElementName());
            theRootNode.add(node);
        }

        if (node!=null) {
            if (isDebugging) {
                System.out.println("Found root node "+node.getNodeName());
            }

            parse(node);
        }
        else {
            // Didn't find the root element, so lets just add all of the types
            theRootNode = new XmlRootTreeNode("unknown");
            for (final SchemaTreeNode subnode:theTypes.values()) {
                if (!subnode.isSchemaNode()) {
                    // Filter out all of the standard types
                    theRootNode.add(subnode);
                }
            }
        }
    }

    /**
     * Return the simple and complex parsed from the schemas
     * 
     * @return the types
     * @since 1.1
     */
    public HashMap<QName,SchemaTreeNode> getTypes() {
        return theTypes;
    }

    /**
     * Returns the tree root node
     * 
     * @return the Root Tree Node
     * @since 1.1
     */
    public XmlRootTreeNode getRootNode() {
        return theRootNode;
    }

    /**
     * Parse
     * <P>
     * Iteratively parse the nodes and swap out the types
     * 
     * @param node to parse
     * @since 1.1
     */
    private void parse(SchemaTreeNode node) {
        for (int i = 0;i<node.getChildCount();i++) {
            final SchemaTreeNode treeNode = (SchemaTreeNode)node.getChildAt(i);

            if (treeNode.isElementTreeNode()) {

                final ElementTreeNode elementTreeNode =
                    (ElementTreeNode)treeNode;

                // Need to swap, do we have a type for it..
                SchemaTreeNode typeNode =
                    theTypes.get(elementTreeNode.getTypeQName());

                if (typeNode!=null) {

                    if (typeNode.isComplexTreeNode()) {
                        final ComplexTreeNode newNode =
                            (ComplexTreeNode)typeNode.clone();
                        newNode.setMinOccurs(elementTreeNode.getMinOccurs());
                        newNode.setMaxOccurs(elementTreeNode.getMaxOccurs());
                        newNode.setAnnotation(elementTreeNode.getAnnotation());

                        // Set the element reference due to name etc..
                        newNode.setElementReferenceName(elementTreeNode
                            .getQName());

                        node.remove(i);
                        node.insert(newNode,i);

                        // Lets check the attributes ..
                        for (final AttributeTreeNode attrNode:newNode
                            .getAttributeChildren()) {
                            if (!attrNode.hasAnnotation()) {
                                final SchemaTreeNode simpleTypeNode =
                                    theTypes.get(attrNode.getQName());
                                if (simpleTypeNode!=null) {
                                    if (simpleTypeNode.hasAnnotation()) {
                                        // Only set the annotation if there is
                                        // one
                                        attrNode.setAnnotation(simpleTypeNode
                                            .getAnnotation());
                                    }
                                }
                            }
                        }

                        // Lets stop at a sensible limit, the reason here is
                        // that a complex node and contain a complex node
                        if (node.getLevel()==15) {
                            return;
                        }

                        parse(newNode);
                    }
                    else {
                        typeNode = theTypes.get(elementTreeNode.getTypeQName());

                        if (typeNode.isSimpleTreeNode()) {
                            elementTreeNode.setSimpleType((SimpleType)typeNode);
                        }
                    }
                }
                else {
                    typeNode = theTypes.get(elementTreeNode.getTypeQName());

                    // Hmm...
                    if (typeNode!=null) {
                        if (typeNode.isSimpleTreeNode()) {
                            elementTreeNode.setSimpleType((SimpleType)typeNode);
                        }
                    }
                }
            }
        }
    }

    /**
     * Returns a list of possible root elements
     * 
     * @return A list of Element or complex types
     * @since 2.5
     */
    public List<QName> getElementNames() {
        ArrayList<QName> names = new ArrayList<QName>(50);

        for (Entry<QName,SchemaTreeNode> entry:theTypes.entrySet()) {
            SchemaTreeNode treeNode = entry.getValue();
            if (treeNode.isComplexTreeNode() || treeNode.isComplexTreeNode()) {
                names.add(entry.getKey());
            }
        }

        return names;
    }
}