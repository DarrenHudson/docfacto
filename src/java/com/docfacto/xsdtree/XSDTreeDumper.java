package com.docfacto.xsdtree;

import java.awt.BorderLayout;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTree;

import com.docfacto.common.DocfactoException;
import com.docfacto.xsd2common.XSDArgumentParser;

/**
 * Simple class to take the XSD and draw a <code>JTree</code> in a
 * <code>JFrame</code>.
 * 
 * @author dhudson - Created 8 Apr 2012 : 13:51:36
 * @since 2.0
 */
public class XSDTreeDumper extends JFrame {

    /**
     * Constant {@value}
     */
    private static final long serialVersionUID = 1L;

    /**
     * Create new instance of XSDTreeDumper.
     * 
     * @param parser XSD Argument parser
     * @since 2.2
     */
    public XSDTreeDumper(XSDArgumentParser parser) {

        // JFrame stuff
        getRootPane().setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Call the traverser
        final XSDTreeTraverser traverser = new XSDTreeTraverser(parser);

        if (parser.hasTitle()) {
            setTitle(parser.getTitle());
        }

        traverser.setDebug(true);

        // Parse the file we are interested in
        try {
            traverser.visit(new File(parser.getXSD()));
        }
        catch (final DocfactoException e) {
            e.printStackTrace();
            System.exit(1);
        }

        // Create a tree
        final JTree tree = new JTree(traverser.getRootNode());
        final JScrollPane scrollPane = new JScrollPane(tree);

        getRootPane().add(scrollPane,BorderLayout.CENTER);

        pack();

        setVisible(true);
    }

    /**
     * Command line launcher
     * 
     * @param args path to the XSD to process
     * @since 2.0
     */
    public static void main(String[] args) {
        if (args.length>0) {
            new XSDTreeDumper(new XSDArgumentParser(args));
        }
        else {
            System.err.println("No arguments supplied");
            System.exit(1);
        }
    }

}
