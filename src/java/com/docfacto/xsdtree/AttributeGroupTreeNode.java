package com.docfacto.xsdtree;

import java.util.Collection;

import com.sun.xml.xsom.XSAttGroupDecl;
import com.sun.xml.xsom.XSAttributeUse;

/**
 * An attribute group, is a list of attributes
 * 
 * @author dhudson - created 15 Nov 2013
 * @since 2.5
 */
public class AttributeGroupTreeNode extends SchemaTreeNode {

    /**
     * @serial {@value}
     */
    private static final long serialVersionUID = 1L;

    private final XSAttGroupDecl theGroupDecl;

    /**
     * Constructor.
     * 
     * @param groupDecl
     * @since 2.5
     */
    AttributeGroupTreeNode(XSAttGroupDecl groupDecl) {
        super(groupDecl.getTargetNamespace(),groupDecl.getName());
        theGroupDecl = groupDecl;
        parse();
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#toString()
     */
    @Override
    public String toString() {
        return "AG: "+theGroupDecl.getName();
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#parse()
     */
    @Override
    void parse() {
        if (theGroupDecl.getAnnotation()!=null) {
            setAnnotation(theGroupDecl.getAnnotation().getAnnotation());
        }

        Collection<? extends XSAttributeUse> collection =
            theGroupDecl.getAttributeUses();

        for (XSAttributeUse attribute:collection) {
            add(new AttributeTreeNode(attribute));
        }
    }
    
    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#isAttributeGroupTeeNode()
     */
    @Override
    public boolean isAttributeGroupTeeNode() {
        return true;
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#clone()
     */
    @Override
    public Object clone() {
        return new AttributeGroupTreeNode(theGroupDecl);
    }

}
