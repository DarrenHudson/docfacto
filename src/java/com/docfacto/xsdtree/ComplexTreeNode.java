package com.docfacto.xsdtree;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.QName;

import com.sun.xml.xsom.XSAttributeUse;
import com.sun.xml.xsom.XSComplexType;
import com.sun.xml.xsom.XSContentType;
import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSModelGroup;
import com.sun.xml.xsom.XSModelGroup.Compositor;
import com.sun.xml.xsom.XSParticle;
import com.sun.xml.xsom.XSTerm;

/*
 * @author dhudson -
 * Created 11 Apr 2012 : 10:54:32
 */

/**
 * This class represents a Complex element in the XSD Tree. It is also possible
 * to get at the <code>XSComplexType</code> used to create this node. It is also
 * possible for a complex node to have attributes as children
 * 
 * @author dhudson - created 21 Apr 2012
 * @since 1.1
 */
public class ComplexTreeNode extends RangeTreeNode {

    /**
     * Constant {@value}
     */
    private static final long serialVersionUID = 1L;

    /**
     * Holds the Complex Type that generated this node
     */
    private final XSComplexType theComplexType;

    private XSElementDecl theElementDecl;

    private boolean isChoice = false;

    /**
     * If this is a clone to replace and element, then we need to know what it
     * is
     */
    private QName theElementReferenceName;

    /**
     * Creates a new instance of <code>ComplexTreeNode</code>.
     * 
     * @param complexType to construct the node
     */
    ComplexTreeNode(XSComplexType complexType) {
        super(complexType.getTargetNamespace(),complexType.getName());
        theComplexType = complexType;
        parse();
    }

    /**
     * Create a new instance of <code>ComplexTreeNode</code>.
     * 
     * @param elementDecl the element which contains a complex structure
     */
    ComplexTreeNode(XSElementDecl elementDecl) {
        super(elementDecl.getTargetNamespace(),elementDecl.getName());
        theComplexType = elementDecl.getType().asComplexType();
        theElementDecl = elementDecl;
        parse();
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#parse()
     */
    @Override
    void parse() {

        if (theComplexType.getAnnotation()!=null) {
            setAnnotation(theComplexType
                .getAnnotation()
                .getAnnotation());
        }

        final XSContentType xsContentType = theComplexType.getContentType();

        // Get the attributes for the complex type
        getAttributes(theComplexType);

        final XSParticle particle = xsContentType.asParticle();
        if (particle!=null) {

            final XSTerm term = particle.getTerm();

            if (term.isModelGroup()) {
                final XSModelGroup xsModelGroup = term.asModelGroup();
                if (xsModelGroup.getCompositor()==Compositor.CHOICE) {
                    isChoice = true;
                }
                processModelGroup(xsModelGroup);
            }
        }
    }

    /**
     * Parse and add any attributes for this complex node
     * 
     * @param xsComplexType to process
     * @since 1.1
     */
    private void getAttributes(XSComplexType xsComplexType) {
        final Collection<? extends XSAttributeUse> c =
            xsComplexType.getAttributeUses();
        final Iterator<? extends XSAttributeUse> i = c.iterator();
        while (i.hasNext()) {
            add(new AttributeTreeNode(i.next()));
        }
    }

    /**
     * Returns a list of children already cast to SchemaTreeNodes
     * 
     * @return List of SchemaTreeNodes
     * @since 1.2
     */
    public List<SchemaTreeNode> getSchemaChildren() {
        final int size = (children==null) ? 0 : children.size();

        final ArrayList<SchemaTreeNode> list =
            new ArrayList<SchemaTreeNode>(size);

        for (int i = 0;i<size;i++) {
            list.add((SchemaTreeNode)children.get(i));
        }

        return list;
    }

    /**
     * Returns a list of <code>ElementTreeNode</code>s for this complex type
     * 
     * @return ElementTreeNodes
     * @since 1.2
     */
    public List<ElementTreeNode> getElementChildren() {
        final int size = (children==null) ? 0 : children.size();

        final ArrayList<ElementTreeNode> list =
            new ArrayList<ElementTreeNode>(size);

        for (int i = 0;i<size;i++) {
            final SchemaTreeNode node = (SchemaTreeNode)children.get(i);
            if (node.isElementTreeNode()) {
                list.add((ElementTreeNode)node);
            }
        }

        return list;
    }

    /**
     * Returns a list of <code>AttributeTreeNode</code>s for this complex type
     * 
     * @return AttributeTreeNodes or an empty list if there are not attributes
     * @since 1.2
     */
    public List<AttributeTreeNode> getAttributeChildren() {
        final int size = (children==null) ? 0 : children.size();

        final ArrayList<AttributeTreeNode> list =
            new ArrayList<AttributeTreeNode>(size);

        for (int i = 0;i<size;i++) {
            final SchemaTreeNode node = (SchemaTreeNode)children.get(i);
            if (node.isAttributeTreeNode()) {
                list.add((AttributeTreeNode)node);
            }
        }

        return list;
    }

    /**
     * XSD Notation often defines a complex type, which has no children.
     * 
     * @return true if the complex type has not children other than attributes
     * @since 2.5
     */
    public boolean isEmptyComplexType() {
        List<SchemaTreeNode> children = getSchemaChildren();
        if (children.isEmpty()) {
            // No attributes or other elements
            return true;
        }

        for (SchemaTreeNode node:children) {
            if (!node.isAttributeTreeNode()) {
                return false;
            }
        }

        // Attributes only
        return true;
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#isComplexTreeNode()
     */
    @Override
    public boolean isComplexTreeNode() {
        return true;
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder(100);
        builder.append("ComplexType : ");
        if (isChoice) {
            builder.append("(c)");
        }
        else {
            builder.append("(s)");
        }
        builder.append(" ");
        builder.append(getNodeName());
        builder.append(getRangeString());

        return builder.toString();
    }

    /**
     * Return the XSComplexType associated for this node
     * 
     * @return the XSComplexType
     * @since 1.1
     */
    public XSComplexType getXSComplexType() {
        return theComplexType;
    }

    /**
     * If this node has replaced an element which had a complex type then this
     * is the element name
     * 
     * @return the <code>QName</code> of the element reference node
     * @since 1.2
     */
    public QName getElementReferenceName() {
        return theElementReferenceName;
    }

    /**
     * Set the Element reference name
     * 
     * @param name element reference name
     * @since 1.2
     */
    void setElementReferenceName(QName name) {
        theElementReferenceName = name;
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#getNodeName()
     */
    @Override
    public String getNodeName() {
        if (theElementReferenceName!=null) {
            return theElementReferenceName.getLocalPart();
        }
        return super.getNodeName();
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#clone()
     */
    @Override
    public Object clone() {
        if (theElementDecl!=null) {
            return new ComplexTreeNode(theElementDecl);
        }

        return new ComplexTreeNode(theComplexType);
    }

    /**
     * Check to see if the complex node is a choice
     * 
     * @return true if a choice and not a sequence
     * @since 2.5
     */
    public boolean isChoice() {
        return isChoice;
    }
}
