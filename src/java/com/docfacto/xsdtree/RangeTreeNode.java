/*
 * @author dhudson -
 * Created 5 May 2012 : 18:36:55
 */

package com.docfacto.xsdtree;

import com.docfacto.xml.XMLRange;

/**
 * Abstract class that handles the Min and Max Occurs
 * 
 * @author dhudson - created 5 May 2012
 * @since 1.2
 */
public abstract class RangeTreeNode extends SchemaTreeNode {

    /**
     * Constant {@value}
     */
    private static final long serialVersionUID = 1L;

    /**
     * Holds the min / max values
     */
    private final XMLRange theRange;

    /**
     * Create a new instance of <code>RangeTreeNode</code>.
     * @param namespace of the node
     * @param name of the node
     */
    RangeTreeNode(String namespace,String name) {
        super(namespace,name);
        theRange = new XMLRange();
    }

    /**
     * Constructor.
     * 
     * @param namespace of the node
     * @param name name of the node
     * @param minOccurs value
     * @param maxOccurs value
     */
    RangeTreeNode(String namespace,String name,int minOccurs,int maxOccurs) {
        this(namespace,name);
        theRange.setMinOccurs(minOccurs);
        theRange.setMaxOccurs(maxOccurs);
    }

    /**
     * Returns the string representation of the min occurs and max occurs
     * 
     * @return String representation of the range
     * @since 1.2
     */
    public String getRangeString() {
        return theRange.toString();
    }

    /**
     * Returns the min occurs for this node
     * 
     * @return the min occurs
     * @since 1.2
     */
    public int getMinOccurs() {
        return theRange.getMinOccurs();
    }

    /**
     * Return the max occurs for this node
     * 
     * @return the max occurs
     * @since 1.2
     */
    public int getMaxOccurs() {
        return theRange.getMaxOccurs();
    }

    /**
     * set the minOccurs for this node
     * 
     * @param minOccurs set the minimum occurrence for this node
     * @since 1.2
     */
    void setMinOccurs(int minOccurs) {
        theRange.setMinOccurs(minOccurs);
    }

    /**
     * set the maxOccurs for this node
     * 
     * @param maxOccurs set the maximum occurrence for this node
     * @since 1.2
     */
    void setMaxOccurs(int maxOccurs) {
        theRange.setMaxOccurs(maxOccurs);
    }

    /**
     * Is this element required
     * 
     * @return true if the min and max occurs are both 1
     * @since 1.4
     */
    public boolean isRequired() {
        return theRange.isRequired();
    }

    /**
     * Returns the XML range for this node
     * 
     * @return the XML Range
     * @since 2.0
     */
    public XMLRange getXMLRange() {
        return theRange;
    }
}
