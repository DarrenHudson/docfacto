package com.docfacto.xsdtree;

import com.sun.xml.xsom.XSModelGroupDecl;

/**
 * Group Schema Tree Node
 * 
 * @author dhudson - created 18 Nov 2013
 * @since 2.5
 */
public class GroupTreeNode extends SchemaTreeNode {

    /**
     * @serial {@value}
     */
    private static final long serialVersionUID = 1L;

    private final XSModelGroupDecl theGroupDecl;

    /**
     * Constructor.
     * 
     * @param groupDecl group decl
     * @since 2.5
     */
    GroupTreeNode(XSModelGroupDecl groupDecl) {
        super(groupDecl.getTargetNamespace(),groupDecl.getName());
        theGroupDecl = groupDecl;
        parse();
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#parse()
     */
    @Override
    void parse() {
        if (theGroupDecl.getAnnotation()!=null) {
            setAnnotation(theGroupDecl
                .getAnnotation()
                .getAnnotation());
        }

        processModelGroup(theGroupDecl.getModelGroup());
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#toString()
     */
    @Override
    public String toString() {
        return "Group : "+theGroupDecl.getName();
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#clone()
     */
    @Override
    public Object clone() {
        return new GroupTreeNode(theGroupDecl);
    }

}
