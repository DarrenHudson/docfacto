package com.docfacto.xsdtree;

import javax.xml.namespace.QName;

import com.sun.xml.xsom.XSAnnotation;
import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSType;
import com.sun.xml.xsom.impl.ElementDecl;

/*
 * @author dhudson -
 * Created 11 Apr 2012 : 09:51:20
 */

/**
 * This class represents the RootTreeNode for the schema.
 * 
 * @author dhudson - created 21 Apr 2012
 * @since 1.1
 */
public class XmlRootTreeNode extends SchemaTreeNode {

    /**
     * Constant {@value}
     */
    private static final long serialVersionUID = 1L;

    /**
     * The element description
     */
    private XSElementDecl theElementDecl;

    /**
     * The Qualified name of the first child element
     */
    private QName theChildElementName;

    private SchemaTreeNode theRootNode;

    /**
     * Create a new instance of <code>XmlRootTreeNode</code>.
     * @param elementDecl to construct this root node with
     */
    public XmlRootTreeNode(XSElementDecl elementDecl) {
        super(elementDecl.getTargetNamespace(),elementDecl.getName());
        theElementDecl = elementDecl;
        parse();
    }

    /**
     * Create a new instance of <code>XmlRootTreeNode</code>, using a tree node
     * @param treeNode
     * @since 2.2
     */
    public XmlRootTreeNode(SchemaTreeNode treeNode) {
        super(treeNode.getQName().getNamespaceURI(),treeNode.getQName().getLocalPart());
        theRootNode = treeNode;

        if(treeNode.isElementTreeNode()) {
            parse();
        } else if(treeNode.isComplexTreeNode()) {
            parse((ComplexTreeNode)treeNode);
        }
    }

    /**
     * Create a new instance of <code>XmlRootTreeNode</code>.
     * @param nodeName name of the root node
     */
    public XmlRootTreeNode(String nodeName) {
        super("",nodeName);
    }

    /**
     * Parse a complex tree node
     *
     * @param complexNode
     * @since 2.2
     */
    void parse(ComplexTreeNode complexNode) {
        setAnnotation(complexNode.getAnnotation());
        add(complexNode);
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#parse()
     */
    @Override
    void parse() {
        final XSAnnotation annotation =
        ((ElementDecl)theElementDecl).getTerm().getAnnotation();

        if (annotation!=null) {
            setAnnotation(annotation.getAnnotation());
        }

        final XSType type = theElementDecl.getType();

        if (type.getName()==null) {
            add(new ComplexTreeNode(type.asComplexType()));
            theChildElementName =
            new QName(theElementDecl.getTargetNamespace(),
                theElementDecl.getName());
        }
        else {
            theChildElementName =
            new QName(type.getTargetNamespace(),type.getName());
        }
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#isXmlRootTreeNode()
     */
    @Override
    public boolean isXmlRootTreeNode() {
        return true;
    }

    /**
     * This returns the first element in the XSD, which we think is the root
     * node
     * 
     * @return the child element name
     * @since 1.1
     */
    public QName getChildElementName() {
        return theChildElementName;
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder(50);
        builder.append("XmlRootTreeNode: ");
        builder.append(getNodeName());

        return builder.toString();
    }

    /**
     * Return the element decl associated with this node
     * 
     * @return the element decl
     * @since 1.1
     */
    public XSElementDecl getXSElementDecl() {
        return theElementDecl;
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#clone()
     */
    @Override
    public Object clone() {
        if (theElementDecl!=null) {
            return new XmlRootTreeNode(theElementDecl);
        }
        
        if(theRootNode != null) {
            return new XmlRootTreeNode(theRootNode);
        }

        return new XmlRootTreeNode(getNodeName());
    }
}
