package com.docfacto.xsdtree;

import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.xml.namespace.QName;

import com.docfacto.common.StringUtils;
import com.sun.tools.xjc.reader.xmlschema.bindinfo.BindInfo;
import com.sun.xml.xsom.XSModelGroup;
import com.sun.xml.xsom.XSModelGroupDecl;
import com.sun.xml.xsom.XSParticle;
import com.sun.xml.xsom.XSTerm;
import com.sun.xml.xsom.impl.Const;
import com.sun.xml.xsom.impl.ElementDecl;

/*
 * @author dhudson -
 * Created 11 Apr 2012 : 09:49:20
 */

/**
 * Abstract class for XSDTree Nodes.
 * 
 * @author dhudson - created 21 Apr 2012
 * @since 1.1
 */
public abstract class SchemaTreeNode extends DefaultMutableTreeNode implements
Cloneable {

    /**
     * Constant {@value}
     */
    private static final long serialVersionUID = 1L;

    /**
     * Name of the node
     */
    private final String theNodeName;

    /**
     * Grabs the annotation if any
     * 
     * {@docfacto.note Not set to null }
     */
    private String theAnnotation = "";

    /**
     * Qualified name of the node
     */
    private final QName theQName;

    /**
     * Create a new instance of <code>SchemaTreeNode</code>.
     * 
     * @param namespace of the node
     * @param name of the node
     */
    SchemaTreeNode(String namespace,String name) {
        super(name);
        theNodeName = name;
        theQName = new QName(namespace.trim(),name.trim());
    }

    /**
     * Return the node name
     * 
     * @return node name
     * @since 1.1
     */
    public String getNodeName() {
        return theNodeName;
    }

    /**
     * Return true if the target name space is
     * {@literal http://www.w3.org/2001/XMLSchema}
     * 
     * @return true if the node is a normal schema node
     * @since 2.2
     */
    public boolean isSchemaNode() {
        return (Const.schemaNamespace.equals(theQName.getNamespaceURI()));
    }

    /**
     * Get QNamefor the node
     * 
     * @return the QName for the node
     * @since 1.1
     */
    public QName getQName() {
        return theQName;
    }

    /**
     * Returns true if element node
     * 
     * @return true if node is element node
     * @since 1.1
     */
    public boolean isElementTreeNode() {
        return false;
    }

    /**
     * Returns this node as a element tree node
     * 
     * @return this node as a Element Tree Node
     * @since 2.5
     */
    public ElementTreeNode asElementTreeNode() {
        return (ElementTreeNode)this;
    }

    /**
     * Returns true if a complex node
     * 
     * @return true if complex node
     * @since 1.1
     */
    public boolean isComplexTreeNode() {
        return false;
    }

    /**
     * Returns this node as a complex tree node
     * 
     * {@docfacto.note will throw a runtime exception if not the correct type }
     * 
     * @return this node as a Complex Tree Node
     * @since 2.5
     */
    public ComplexTreeNode asComplexTreeNode() {
        return (ComplexTreeNode)this;
    }

    /**
     * Returns true if the node is a simple node
     * 
     * @return true if node is simple node
     * @since 1.1
     */
    public boolean isSimpleTreeNode() {
        return false;
    }

    /**
     * Returns this node as a simple type
     * 
     * @return this node as a simple type
     * @since 2.5
     */
    public SimpleType asSimpleType() {
        return (SimpleType)this;
    }

    /**
     * Returns true if the node is he root node
     * 
     * @return true if XML Root node
     * @since 1.1
     */
    public boolean isXmlRootTreeNode() {
        return false;
    }

    /**
     * Returns true if attribute node
     * 
     * @return true if node is attribute node
     * @since 1.1
     */
    public boolean isAttributeTreeNode() {
        return false;
    }

    /**
     * Returns this as an attribute tree node
     * 
     * {@docfacto.note will throw a runtime exception if not the correct type }
     * 
     * @return this node as a attribute tree node
     * @since 2.5
     */
    public AttributeTreeNode asAttributeTreeNode() {
        return (AttributeTreeNode)this;
    }

    /**
     * Returns true if attribute group node
     * 
     * @return true if the node is an Attribute Group Node
     * @since 2.5
     */
    public boolean isAttributeGroupTeeNode() {
        return false;
    }

    /**
     * Return this as a Attribute group tree node
     * 
     * {@docfacto.note will throw a runtime exception if not the correct type }
     * 
     * @return this node as a Attribute Group Tree Node
     * @since 2.5
     */
    public AttributeGroupTreeNode asAttributeGroupTreeNode() {
        return (AttributeGroupTreeNode)this;
    }

    /**
     * Returns true is a group tree node
     * 
     * @return true if a Group Tree Node
     * @since 2.5
     */
    public boolean isGroupTreeNode() {
        return false;
    }

    /**
     * Returns this as a Group tree node
     * 
     * {@docfacto.note will throw a runtime exception if not the correct type }
     * 
     * @return this node as a group tree node
     * @since 2.5
     */
    public GroupTreeNode asGroupTreeNode() {
        return (GroupTreeNode)this;
    }

    /**
     * Returns true if the node has an annotation
     * 
     * @return true if node has annotation
     * @since 1.1
     */
    public boolean hasAnnotation() {
        return (!theAnnotation.isEmpty());
    }

    /**
     * Sets the annotation
     * 
     * If its a BindInfo object, then the annotation has come from the JAXB
     * parsing, else its a string from the Docfacto annotation parser
     * <code>XSAnnotationParser</code>
     * com.sun.tools.xjc.reader.xmlschema.bindinfo.BindInfo
     * 
     * @param annotation for this node
     * @since 1.1
     */
    void setAnnotation(Object annotation) {

        if (annotation==null) {
            return;
        }

        if (annotation instanceof BindInfo) {
            final BindInfo anno = (BindInfo)annotation;

            if (anno.getDocumentation()==null) {
                return;
            }

            theAnnotation = trimParagraph(anno.getDocumentation());
        }
        else {
            theAnnotation = trimParagraph(annotation.toString());
        }
    }

    /**
     * Process the model group and add the elements
     * 
     * {@docfacto.note This is an iterative class }
     * 
     * @param xsModelGroup to process
     * @since 2.0
     */
    void processModelGroup(XSModelGroup xsModelGroup) {
        final XSParticle[] particles = xsModelGroup.getChildren();
        for (final XSParticle p:particles) {
            final XSTerm pterm = p.getTerm();

            if (pterm.isModelGroupDecl()) {
                // Add the group
                XSModelGroupDecl modelGroupDecl = pterm.asModelGroupDecl();
                processModelGroup(modelGroupDecl.getModelGroup());
            }
            else if (pterm.isElementDecl()) {
                final ElementTreeNode elementTreeNode =
                    new ElementTreeNode(
                        (ElementDecl)pterm,
                        p.getMinOccurs().intValue(),
                        p.getMaxOccurs().intValue());
                add(elementTreeNode);
            }
            else {
                if (pterm.isModelGroup()) {
                    processModelGroup(pterm.asModelGroup());
                }
            }
        }
    }

    /**
     * Returns the annotation
     * 
     * @return the annotation, which will be an empty string if not present
     * @since 1.1
     */
    public String getAnnotation() {
        return theAnnotation;
    }

    /**
     * Returns a list of <code>AttributeTreeNode</code>s for this complex type
     * 
     * @return AttributeTreeNodes or an empty list if there are not attributes
     * @since 2.5
     */
    public List<AttributeTreeNode> getAttributeChildren() {
        final int size = (children==null) ? 0 : children.size();

        final ArrayList<AttributeTreeNode> list =
            new ArrayList<AttributeTreeNode>(size);

        for (int i = 0;i<size;i++) {
            final SchemaTreeNode node = (SchemaTreeNode)children.get(i);
            if (node.isAttributeTreeNode()) {
                list.add((AttributeTreeNode)node);
            }
        }

        return list;
    }

    /**
     * Trim extraneous white-space off the left & right of each line in this
     * string
     * 
     * @param str to process
     * @return a trimmed string
     * @since 1.1
     */
    private String trimParagraph(String str) {
        final String lines[] = str.split("\n");
        final StringBuffer result = new StringBuffer();
        for (final String line:lines) {
            result.append(result.length()>0 ? "\n" : "").append(line.trim());
        }

        return StringUtils.escapeXML(result.toString().trim());
    }

    /**
     * @see javax.swing.tree.DefaultMutableTreeNode#toString()
     */
    @Override
    public abstract String toString();

    /**
     * parse
     * 
     * Abstract method used to parse the node
     * 
     * @since 1.1
     */
    abstract void parse();

    /**
     * @see javax.swing.tree.DefaultMutableTreeNode#clone()
     */
    @Override
    abstract public Object clone();
}
