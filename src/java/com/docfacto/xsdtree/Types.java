package com.docfacto.xsdtree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.namespace.QName;

public class Types {

    enum SchemaType {
        ANY,
        COMPLEX,
        ELEMENT
    }

    /**
     * Map of all of the possible types loaded from schemas
     */
    HashMap<QName,SchemaTreeNode> theAnyList;
    HashMap<QName,SchemaTreeNode> theComplexList;
    HashMap<QName,SchemaTreeNode> theElementList;

    Types() {
        // theTypes = new HashMap<QName,SchemaTreeNode>();
        theAnyList = new HashMap<QName,SchemaTreeNode>();
        theComplexList = new HashMap<QName,SchemaTreeNode>();
        theElementList = new HashMap<QName,SchemaTreeNode>();
    }

    void addType(QName qname,SchemaTreeNode treeNode,SchemaType type) {

        switch (type) {
        case ANY:
            theAnyList.put(qname,treeNode);
            break;
        case COMPLEX:
            theComplexList.put(qname,treeNode);
            break;
        case ELEMENT:
            theElementList.put(qname,treeNode);
            break;
        }

        // if (treeNode.getNodeName().equals("PsgrAry")) {
        // Exception ex = new Exception();
        // ex.printStackTrace();
        // }
        //
        // SchemaTreeNode value = theTypes.put(qname,treeNode);
        // if (value!=null) {
        // System.err.println("Duplicate.."+value+" : "+treeNode);
        // }
    }

    SchemaTreeNode get(QName qname,SchemaType type) {
        SchemaTreeNode treeNode = null;
        switch (type) {
        case ANY:
            treeNode = theAnyList.get(qname);
            break;
        case COMPLEX:
            treeNode = theComplexList.get(qname);
            break;
        case ELEMENT:
            treeNode = theElementList.get(qname);
            break;
        }

        return treeNode;
    }

    SchemaTreeNode get(QName qname) {
        SchemaTreeNode treeNode = null;
        treeNode = theComplexList.get(qname);
        if (treeNode!=null) {
            return treeNode;
        }

        treeNode = theElementList.get(qname);
        if (treeNode!=null) {
            return treeNode;
        }

        return theAnyList.get(qname);

    }

    List<QName> getComplexNames() {
        ArrayList<QName> list = new ArrayList<QName>();
        for (Entry<QName,SchemaTreeNode> entry:theComplexList.entrySet()) {
            list.add(entry.getKey());
        }

        return list;
    }

    List<SchemaTreeNode> getAllTypes() {
        ArrayList<SchemaTreeNode> list = new ArrayList<SchemaTreeNode>();
        list.addAll(theComplexList.values());
        list.addAll(theElementList.values());
        list.addAll(theAnyList.values());

        return list;
    }
}
