/*
 * @author dhudson -
 * Created 18 Apr 2012 : 08:03:38
 */

package com.docfacto.xsdtree;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.namespace.QName;

import com.sun.xml.xsom.XSAttributeDecl;
import com.sun.xml.xsom.XSAttributeUse;
import com.sun.xml.xsom.XSFacet;
import com.sun.xml.xsom.XSRestrictionSimpleType;
import com.sun.xml.xsom.XSSimpleType;
import com.sun.xml.xsom.XmlString;

/**
 * This class represents a Attribute element in the XSD Tree. It is also
 * possible to get at the <code>XSAttributeUse</code> used to create this node.
 * 
 * @author dhudson - created 21 Apr 2012
 * @since 1.1
 */
public class AttributeTreeNode extends SchemaTreeNode {

    private static final String ENUM = "Enum";

    /**
     * Constant {@value}
     */
    private static final long serialVersionUID = -5429426296669092359L;

    /**
     * Holds the XSAttributeUse that generated this node
     */
    private final XSAttributeUse theAttributeUse;
    /**
     * True if the attribute is required
     */
    private boolean isRequired = false;
    /**
     * Holds the default value if present
     */
    private String theDefaultValue;
    /**
     * Holds the fix value if present
     */
    private String theFixedValue;
    /**
     * Holds The attribute type
     */
    private String theTypeName;

    private QName theTypeQName;

    /**
     * Is a Enum attribute
     */
    private List<String> theEnums;

    /**
     * Create a new instance of <code>AttributeTreeNode</code>.
     * 
     * @param attributeUse to construct the node
     */
    AttributeTreeNode(XSAttributeUse attributeUse) {
        super(attributeUse.getDecl().getTargetNamespace(),attributeUse
            .getDecl().getName());
        theAttributeUse = attributeUse;

        parse();
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#parse()
     */
    @Override
    void parse() {

        final XSAttributeDecl attrDecl = theAttributeUse.getDecl();

        if (attrDecl.getDefaultValue()!=null) {
            theDefaultValue = attrDecl.getDefaultValue().value;
        }

        if (attrDecl.getFixedValue()!=null) {
            theFixedValue = attrDecl.getFixedValue().value;
        }

        theTypeName = attrDecl.getType().getName();

        // Its a anonymous inner type..
        if (theTypeName==null) {
            XSSimpleType type = attrDecl.getType();

            if (type.isRestriction()) {
                XSRestrictionSimpleType restriction = type.asRestriction();
                Collection<? extends XSFacet> facets =
                    restriction.getDeclaredFacets();
                for (XSFacet facet:facets) {
                    if (XSFacet.FACET_ENUMERATION.equals(facet.getName())) {
                        addEnumeration(facet.getValue());
                    }
                }
            }

            if (!isEnum()) {
                theTypeName = type.getBaseType().getName();
                theTypeQName = new QName(theTypeName);
            }
        }
        else {
            theTypeQName =
                new QName(attrDecl.getType().getTargetNamespace(),theTypeName);
        }

        isRequired = theAttributeUse.isRequired();

        if (attrDecl.getAnnotation()!=null) {
            setAnnotation(attrDecl.getAnnotation().getAnnotation());
        }
    }

    /**
     * Return the type QName, this may be null.
     * 
     * @return the type QName
     * @since 2.5
     */
    public QName getTypeQName() {
        return theTypeQName;
    }
    
    /**
     * Add a enum to the list
     * 
     * @param value to add
     * @since 2.5
     */
    private void addEnumeration(XmlString value) {
        if (theEnums==null) {
            theEnums = new ArrayList<String>(3);
            theTypeName = ENUM;
        }

        theEnums.add(value.value);
    }

    /**
     * Returns true if the attribute has a default value
     * 
     * @return true if the attribute has a default value
     * @since 1.1
     */
    public boolean hasDefaultValue() {
        if (theDefaultValue==null) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if the attribute has a fixed value
     * 
     * @return true if attribute has fixed value
     * @since 1.1
     */
    public boolean hasFixedValue() {
        if (theFixedValue==null) {
            return false;
        }

        return true;
    }

    /**
     * Return the default value or null
     * 
     * @return the default value of null
     * @since 1.1
     */
    public String getDefaultValue() {
        return theDefaultValue;
    }

    /**
     * Return the fixed value if present
     * 
     * @return string representing the fixed value, or null
     * @since 1.1
     */
    public String getFixedValue() {
        return theFixedValue;
    }

    /**
     * Returns if the attribute is required or not
     * 
     * @return true if the attribute is required
     * @since 1.1
     */
    public boolean isRequired() {
        return isRequired;
    }

    /**
     * Returns the simple type name
     * 
     * @return the type name
     * @since 1.1
     */
    public String getTypeName() {
        return theTypeName;
    }

    /**
     * Check to see if its an Enum
     * 
     * @return true if its a Enum
     * @since 2.5
     */
    public boolean isEnum() {
        return (theEnums!=null);
    }

    /**
     * Returns the enums, may be null.
     * 
     * @return the enums
     * @since 2.5
     */
    public List<String> getEnums() {
        return theEnums;
    }

    /**
     * Returns the <code>XSAtrributeUse</code> associated with this node
     * 
     * @return the <code>XSAtrributeUse</code>
     * @since 1.1
     */
    public XSAttributeUse getAttributeUse() {
        return theAttributeUse;
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#isAttributeTreeNode()
     */
    @Override
    public boolean isAttributeTreeNode() {
        return true;
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder(100);
        builder.append("Attribute : ");
        builder.append(getNodeName());
        if (theTypeName!=null) {
            builder.append(" [");
            builder.append(theTypeName);
            builder.append("]");
        }
        if (isRequired) {
            builder.append(" required");
        }

        return builder.toString();
    }

    /**
     * @see com.docfacto.xsdtree.SchemaTreeNode#clone()
     */
    @Override
    public Object clone() {
        return new AttributeTreeNode(theAttributeUse);
    }
}
