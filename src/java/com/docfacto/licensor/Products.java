package com.docfacto.licensor;

/**
 * List of products available to the licence engine
 *
 * @author dhudson - created 23 Dec 2013
 * @since 2.5
 */
public enum Products {

    /**
     * Screen grab application
     */
    GRABIT,
    /**
     * XSD Tools, which contains XSDTree, XSD2DITA, XSD2SVG
     */
    XSDTOOLS,
    /**
     * Javadoc Taglets, with DITA output
     */
    TAGLETS,
    /**
     * Javadoc syntax checker
     */
    ADAM,
    /**
     * SVG Editor
     */
    BEERMAT,
    /**
     * Links Engine
     */
    LINKS,
    /**
     * DITA Editor
     */
    EDITABLE
    
}
