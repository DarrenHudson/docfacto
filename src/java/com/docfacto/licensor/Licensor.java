/*
 * @author dhudson -
 * Created 5 Sep 2012 : 13:06:01
 */

package com.docfacto.licensor;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;

import com.docfacto.common.XMLUtils;
import com.docfacto.config.generated.AddressScheme;
import com.docfacto.config.generated.Product;
import com.docfacto.exceptions.InvalidLicenceException;

/**
 * Class to handle the Licensing aspect of Docfacto
 * 
 * @author dhudson - created 5 Sep 2012
 * @since 2.0
 */
public class Licensor {

    private static final long NUMBER_OF_MS_IN_A_DAY = 24*60*60*1000;

    private static final String DEFAULT_IP_ADDRESS = "127.0.0.1";

    /**
     * Check the licence key
     * 
     * @param productName name of the product to check
     * @param product to check
     * @throws InvalidLicenceException if unable to load the licence file, or
     * the licence is invalid
     * @return true if the key is valid
     * @since 2.0
     */
    public static boolean checkLicense(String productName,Product product)
    throws InvalidLicenceException {
        // Lets un-final marshall the Key final and set up final the licence
        // details
        try {

            final Date expiryDate = XMLUtils.asDate(product.getExpiryDate());
            if (expiryDate.getTime()<=System.currentTimeMillis()) {
                System.err
                    .println("Docfacto licence expired, please contact Docfacto Ltd [www.docfacto.com]");
                return false;
            }

            return checkIPAddressForProduct(product);
        }

        catch (final Throwable t) {
            throw new InvalidLicenceException(
                "Unable to process licence information",t);
        }

    }

    /**
     * Check to see if this is IP restricted
     * 
     * @param product to check
     * @return true if valid
     * @since 2.5
     */
    private static boolean checkIPAddressForProduct(Product product) {

        AddressScheme scheme = product.getAddress().getScheme();

        // Build a map of address, either IP or MAC
        String[] addresses = product.getAddress().getValue().split(",");
        HashMap<String,String> addressMap =
            new HashMap<String,String>(addresses.length);
        for (String address:addresses) {
            addressMap.put(address.trim(),address.trim());
        }

        switch (scheme) {
        case MAC:
            try {
                InetAddress[] IPAddresses = getAllLocalAddresses();
                for (InetAddress address:IPAddresses) {
                    NetworkInterface network =
                        NetworkInterface.getByInetAddress(address);
                    byte[] mac = network.getHardwareAddress();
                    StringBuilder sb = new StringBuilder(50);
                    for (int i = 0;i<mac.length;i++) {
                        sb.append(String.format(
                            "%02X%s",
                            mac[i],
                            (i<mac.length-1) ? ":" : ""));
                    }
                    String macAddress = sb.toString();

                    if (addressMap.containsKey(macAddress)) {
                        return true;
                    }
                }

                // We get here there is no match..
                System.err.println("Unable to match MAC address for ["
                    +product.getName()+"]");
                return false;

            }
            catch (Exception ex) {
                System.err.println(
                    "Unable to checkServerSideAddressing for product [MAC] "
                        +product.getName()+" : "+ex);
                return false;
            }

        case IPADDRESS:
            try {
                // If 127.0.0.1 then allow to continue
                if (addressMap.containsKey(DEFAULT_IP_ADDRESS)) {
                    return true;
                }

                // Get all the IP addresses for the local host
                InetAddress[] hostAddresses = getAllLocalAddresses();

                for (InetAddress netAddress:hostAddresses) {
                    if (addressMap.containsKey(netAddress.getHostAddress())) {
                        return true;
                    }
                }
            }
            catch (Exception ex) {
                System.err.println(
                    "Unable to checkServerSideAddressing for product [IP] "+
                        product.getName()+" : "+ex);
                return false;
            }
            break;

        }

        return false;
    }

    /**
     * Return all IPAddress known by this host.
     * 
     * Multi NIC config
     * 
     * @return an array of IPAddress know this this host
     * @throws UnknownHostException
     */
    private static InetAddress[] getAllLocalAddresses()
    throws UnknownHostException {
        // Get the local host name
        String localHostName = InetAddress.getLocalHost().getHostName();

        // Get all the IP addresses for the local host
        return InetAddress.getAllByName(localHostName);
    }
}
