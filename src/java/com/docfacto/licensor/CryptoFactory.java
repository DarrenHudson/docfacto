package com.docfacto.licensor;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.common.StringUtils;
import com.docfacto.config.generated.Docfacto;
import com.docfacto.config.generated.Licence;

/**
 * Factory class to handle the encryption / decryption of the licence key
 * 
 * @author dhudson - created 30 Dec 2013
 * @since 2.5
 */
public class CryptoFactory {

    /**
     * Constant {@value}
     */
    private static final String COPYRIGHT = "Copyright";

    /**
     * Constant {@value}
     */
    private static final String PRIVATE_KEY =
        "com/docfacto/licensor/resources/rsaprivate.key";

    /**
     * Constant {@value}
     */
    private static final String PUBLIC_KEY =
        "com/docfacto/licensing/resources/rsapublic.key";

    /**
     * Constant {@value} {@docfacto.system IF the key was generated with a
     * different size, then this would need to change }
     */
    private static final int KEY_SIZE = 1216;

    /**
     * Encrypt the licence section of the of the config into a key
     * 
     * @param licence to encrypt
     * @return the encrypted licence key
     * @since 2.5
     */
    public static String generateKey(byte[] licence) {
        return StringUtils.base64Encode(XXtea.encrypt(licence,
            COPYRIGHT.getBytes()));
    }

    /**
     * Decrypt the key and return a proper licence
     * 
     * @param key to process
     * @return the decrypted Licence section
     * @since 2.5
     */
    public static Licence decryptKey(String key,Unmarshaller unmarshaller ) {

        String xml =
            new String(XXtea.decrypt(StringUtils.base64Decode(key),
                COPYRIGHT.getBytes()));

        try {

//            JAXBContext ctx =
//                JAXBContext.newInstance("com.docfacto.config.generated");
//            Unmarshaller unmarshaller = ctx.createUnmarshaller();
            ByteArrayInputStream bais =
                new ByteArrayInputStream(xml.getBytes());

            Object obj = unmarshaller.unmarshal(bais);

            Docfacto docfacto = (Docfacto)obj;
            return docfacto.getLicence();
        }
        catch (JAXBException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    /**
     * Decrypt the key
     * 
     * @param data
     * @return the decrypted data
     * @throws DocfactoException
     * @throws IOException
     * @since 2.0
     */
    private static byte[] decrypt(byte[] data) throws DocfactoException,
    IOException {
        final PrivateKey privateKey = getPrivateKey();
        try {
            final Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE,privateKey);
            final byte[] cipherData = cipher.doFinal(data);
            return cipherData;
        }
        catch (final Throwable t) {
            throw new DocfactoException("Unable to decrypt data",t);
        }
    }

    /**
     * Return the Private Key from the Jar file
     * 
     * @return the {@code PrivateKey}
     * @throws IOException if unable to read the key
     * @throws DocfactoException if unable to return or de-serialise the key
     * @since 2.0
     */
    public static PrivateKey getPrivateKey() throws IOException,
    DocfactoException {
        final InputStream in =
            IOUtils.getResourceAsStream(PRIVATE_KEY);

        final byte[] encodedKey = new byte[KEY_SIZE];
        final BufferedInputStream bis =
            new BufferedInputStream(IOUtils.getResourceAsStream(PRIVATE_KEY));

        bis.read(encodedKey);

        try {
            final KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            final PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(
                encodedKey);
            final PrivateKey privateKey =
                keyFactory.generatePrivate(privateKeySpec);
            return privateKey;
        }
        catch (final Exception ex) {
            throw new DocfactoException("Spurious serialisation error",ex);
        }
        finally {
            IOUtils.close(in);
            IOUtils.close(bis);
        }
    }

    public byte[] rsaEncrypt(byte[] data) throws DocfactoException, IOException {
        final PublicKey pubKey = getPublicKey();
        try {
            final Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE,pubKey);
            final byte[] cipherData = cipher.doFinal(data);
            return cipherData;
        }
        catch (final Throwable t) {
            t.printStackTrace();
            return null;
        }
    }

    /**
     * Get the public key from the resource file
     * 
     * @return A Public Key
     * @throws IOException
     * @throws DocfactoException
     * @since 2.0
     */
    public static PublicKey getPublicKey() throws IOException,
    DocfactoException {
        final InputStream in =
            IOUtils.getResourceAsStream(PUBLIC_KEY);

        final File privateKeyFile = IOUtils.getResourceAsFile(PUBLIC_KEY);
        final byte[] encodedKey = new byte[(int)privateKeyFile.length()];

        final DataInputStream bis = new DataInputStream(in);
        bis.read(encodedKey);

        try {
            final KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            final X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(
                encodedKey);
            final PublicKey publicKey =
                keyFactory.generatePublic(publicKeySpec);
            return publicKey;
        }
        catch (final Exception e) {
            throw new DocfactoException("Spurious serialisation error",e);
        }
        finally {
            IOUtils.close(in);
            IOUtils.close(bis);
        }
    }
}
