/*
 * @author dhudson -
 * Created 21 Feb 2013 : 17:13:56
 */

package com.docfacto.beermat;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.docfacto.beermat.generated.Beermat;
import com.docfacto.beermat.generated.Palette;
import com.docfacto.beermat.generated.SvgDef;
import com.docfacto.common.DocfactoException;
import com.docfacto.config.BaseXmlConfig;

/**
 * Load a palette from a given file
 * 
 * @author dhudson - created 21 Feb 2013
 * @since 2.2
 */
public class BeermatLoader extends BaseXmlConfig {

    private static final String JAXB_XSD_LOCATION =
    "com/docfacto/beermat/resources/";

    /**
     * JAXB Package name constant {@value}
     */
    private static final String JAXB_PACKAGE_NAME =
    "com.docfacto.beermat.generated";

    /**
     * Name of the validating XSD {@value}
     */
    private static final String XSD_NAME =
    "Beermat.xsd";

    /**
     * Create a new instance of <code>BeermatLoader</code>.
     * @param url to load
     * @throws DocfactoException if unable to load the URL
     */
    public BeermatLoader(final URL url) throws DocfactoException {
        super(url);
    }

    /**
     * Create a new instance of <code>PaletteLoader</code>.
     * 
     * @param configFile to load
     * @throws DocfactoException if unable to load the file
     * @since 2.2
     */
    public BeermatLoader(File configFile) throws DocfactoException {
        super(configFile);
    }

    /**
     * Create a new instance of <code>PaletteLoader</code>.
     * 
     * @param fullPath of the file to load
     * @throws DocfactoException if unable to load the file
     * @since 2.2
     */
    public BeermatLoader(String fullPath) throws DocfactoException {
        super(fullPath);
    }

    /**
     * @see com.docfacto.config.BaseXmlConfig#getXSDName()
     */
    @Override
    public String getXSDName() {
        return XSD_NAME;
    }

    /**
     * @see com.docfacto.config.BaseXmlConfig#getDefaultFileName()
     */
    @Override
    public String getDefaultFileName() {
        return "Beermat.xml";
    }


    /**
     * Fetch the root palettes
     * 
     * @return the root palettes
     * @since 2.2
     */
    public List<Palette> getRootPalettes() {
        return getBeermat().getPalettes();
    }

    /**
     * Take a list produced by JAXB and return only palettes
     *
     * @param palettesAndSvgDefs to process
     * @return a list of {@code Palette}s
     * @since 2.2
     */
    public List<Palette> getPalettesFrom(List<Object> palettesAndSvgDefs) {
        final ArrayList<Palette> pallets = new ArrayList<Palette>(5);
        for(final Object obj : palettesAndSvgDefs) {
            if(obj instanceof Palette) {
                pallets.add((Palette)obj);
            }
        }

        return pallets;
    }

    /**
     * Take a list produced by JAXB and return only the SvgDefs
     *
     * @param palettesAndSvgDefs to process
     * @return a list of {@code SvgDef}s
     * @since 2.2
     */
    public List<SvgDef> getSvgDefsFrom(List<Object> palettesAndSvgDefs) {
        final ArrayList<SvgDef> svgDefs = new ArrayList<SvgDef>(5);
        for(final Object obj : palettesAndSvgDefs) {
            if(obj instanceof SvgDef) {
                svgDefs.add((SvgDef)obj);
            }
        }

        return svgDefs;
    }

    /**
     * Return the un-marshalled doc
     * 
     * @return the root pojo
     * @since 2.2
     */
    public Beermat getBeermat() {
        return (Beermat) getProperties();
    }

    /**
     * @see com.docfacto.config.BaseXmlConfig#getXSDLocation()
     */
    @Override
    public String getXSDLocation() {
        return JAXB_XSD_LOCATION;
    }

    /**
     * @see com.docfacto.config.BaseXmlConfig#getObjectFactoryPackageName()
     */
    @Override
    public String getObjectFactoryPackageName() {
        return JAXB_PACKAGE_NAME;
    }

}
