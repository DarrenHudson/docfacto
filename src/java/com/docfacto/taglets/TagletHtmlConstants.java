package com.docfacto.taglets;

/**
 * The HTML constants to use while constructing taglets.
 * @author kporter - created Sep 13, 2013
 * @since 2.4.4
 */
public class TagletHtmlConstants {
    public static final String TAG_CLASS_INLINE = " docfacto-taglet docfacto-taglet-inline ";
    public static final String TAG_CLASS_BLOCK = " docfacto-taglet docfacto-taglet-block ";
    
    public static final String TAGLET_NOTE = " docfacto-taglet-note ";
    public static final String TAGLET_SINCE = " docfacto-taglet-since ";
    public static final String TAGLET_SYSTEM = " docfacto-taglet-system ";
    public static final String TAGLET_TODO = " docfacto-taglet-todo ";
    public static final String TAGLET_EXAMPLE = " docfacto-taglet-example ";
    
    public static final String H4_TITLE = " docfacto-title ";
    
    public static final String ICON_CLASS_WHITE = " docfacto-icon docfacto-icon-white ";
    public static final String ICON_EYE = " icon-eye-open ";
    public static final String ICON_PENCIL = " icon-pencil ";
    public static final String ICON_LIST_ALT = " icon-list-alt ";
    public static final String ICON_EXCLAMATION = " icon-exclamation-sign ";
    
    public static final String DOCFACTO_PRE = " docfacto.pre ";
    
}
