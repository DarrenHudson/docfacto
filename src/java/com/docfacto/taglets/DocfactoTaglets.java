/*
 * @author dhudson -
 * Created 24 Jul 2012 : 11:29:42
 */

package com.docfacto.taglets;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Map;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.config.XmlConfig;
import com.docfacto.config.generated.UserTaglet;
import com.docfacto.licensor.Licensor;
import com.docfacto.licensor.Products;
import com.docfacto.licensor.UDCProcessor;
import com.sun.tools.doclets.Taglet;
import com.sun.tools.doclets.formats.html.HtmlDoclet;
import com.sun.tools.doclets.internal.toolkit.Configuration;

/**
 * This is the main entry point for the taglets provided by the tool kit.
 * 
 * Note that the registration will create an instance of this class
 * 
 * To register this with {@code Javadoc}, you will need to use the <tt>-taglet
 * com.docfacto.taglets.DocfactoTaglets</tt>
 * 
 * {@docfacto.system This is the entry point from Javadoc. {@docfacto.note @see
 * <a href=
 * "http://docs.oracle.com/javase/1.4.2/docs/tooldocs/javadoc/taglet/overview.html"
 * >Taglet Overview</a> }
 * 
 * @author dhudson - created 24 Jul 2012
 * @since 2.0
 */
public class DocfactoTaglets {

    /**
     * System Property which defines the location of the config file {@value}
     */
    public static final String CONFIG_KEY = "docfacto.config";

    private static final String PRODUCT_NAME = "Taglets";

    /**
     * The Map that contains the Taglets
     */
    @SuppressWarnings("rawtypes")
    private static Map theTagletMap;

    /**
     * Docfacto.xml
     */
    private XmlConfig theConfig;

    /**
     * The configuration from the {@code JavaDoc} engine
     */
    private Configuration theJavaDocConfiguration;

    /**
     * Constructor.
     */
    public DocfactoTaglets() {
    }

    /**
     * Entry point from {@code JavaDoc}.
     * 
     * @param tagletMap the map to register this tag to.
     */
    @SuppressWarnings({"rawtypes"})
    public static void register(Map tagletMap) {

        final DocfactoTaglets taglets = new DocfactoTaglets();
        // Lets see if we can load the config
        if (taglets.setup(tagletMap)) {
            // Register the Taglets
            taglets.loadTaglets();

            taglets.loadUserTaglets();
        }
    }

    /**
     * Make sure that we can load the config file, which by default is
     * Docfacto.xml
     * 
     * @param tagletMap provided by the {@code JavaDoc} engine
     * @return true if a config file was found
     * @since 2.0
     */
    @SuppressWarnings("rawtypes")
    private boolean setup(Map tagletMap) {
        theTagletMap = tagletMap;

        // Version 1.5 of the standard JavaDoc doclet
        final HtmlDoclet standardDoclet = new HtmlDoclet();

        // Lets grab the config
        theJavaDocConfiguration = standardDoclet.configuration();

        try {
            // Have they set a CONFIG_KEY
            final String configPath = System.getProperty(CONFIG_KEY);

            if (configPath!=null) {
                // Picked up the file from the -J
                theConfig = new XmlConfig(configPath);
            }
            else {
                // Lets see if we can find a Docfacto.xml on the classpath
                theConfig = new XmlConfig();
            }

            if (!Licensor.checkLicense(PRODUCT_NAME,
                theConfig.getProduct(Products.TAGLETS))) {
                theJavaDocConfiguration.root
                    .printError("Invalid Docfacto Licence, please contact Docfacto Ltd [www.docfacto.com]");
                return false;
            }

            // Phone home
            UDCProcessor.registerProduct("Taglets",
                theConfig.getProduct(Products.TAGLETS),theConfig);

            // Lets copy the resources that we need
            copyResourceFiles();

        }
        catch (final Throwable ex) {
            theJavaDocConfiguration.root
                .printError("Unable to setup Docfacto Taglets "+
                    ex.getMessage());

            ex.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * Copy the required resource files
     * 
     * @throws DocfactoException if unable to create the folder or copy the
     * files
     * @since 2.0
     */
    private void copyResourceFiles() throws DocfactoException {
        final File resourceFolder =
            new File(theJavaDocConfiguration.destDirName,"resources");

        // Is it already there?
        if (!resourceFolder.isDirectory()) {
            if (!resourceFolder.mkdirs()) {
                throw new DocfactoException(
                    "Unable to create resource folder "+
                        resourceFolder.getPath());
            }
        }

        final File whiteIcons = new File(resourceFolder,"icons-white.png");
        if (!whiteIcons.exists()) {
            copyResource("com/docfacto/taglets/resources/icons-white.png",
                whiteIcons);
        }

        final File icons = new File(resourceFolder,"icons.png");
        if (!icons.exists()) {
            copyResource("com/docfacto/taglets/resources/icons.png",icons);
        }
    }

    /**
     * Copy a single resource which is embedded in the jar file
     * 
     * @param resource
     * @param toFile
     * @throws DocfactoException
     * @since 2.0
     */
    private void copyResource(String resource,File toFile)
    throws DocfactoException {
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(toFile);
            IOUtils
                .copyStream(
                    IOUtils
                        .getResourceAsStream(resource),
                    fos);
        }
        catch (final FileNotFoundException ex) {
            throw new DocfactoException("Unable to create "+toFile.getPath(),ex);
        }
    }

    /**
     * Load the Docfacto taglets
     * 
     * @since 2.0
     */
    private void loadTaglets() {

        try {
            final TagletLoader loader = new TagletLoader();

            for (final BaseTaglet taglet:loader
                .getHtmlTagletsToRegister(theConfig)) {
                // Register the bad boy
                registerTaglet(taglet);
            }
        }
        catch (final DocfactoException ex) {
            theJavaDocConfiguration.root
                .printError("Unable to load Taglets.xml "+ex.getMessage());
        }
    }

    /**
     * Load the user defined taglets if any
     * 
     * @since 2.0
     */
    @SuppressWarnings("unchecked")
    private void loadUserTaglets() {
        final List<UserTaglet> userTaglets =
            theConfig.getTagletsConfig().getUserTaglets();
        if (userTaglets==null) {
            // Nothing to do..
            return;
        }

        for (final UserTaglet userTaglet:userTaglets) {
            try {
                final Class<?> userTagletClass =
                    Class.forName(userTaglet.getValue());
                final Taglet toLoad = (Taglet)userTagletClass.newInstance();

                theTagletMap.remove(toLoad.getName());
                // Load the new one
                theTagletMap.put(toLoad.getName(),toLoad);

                theJavaDocConfiguration.root.printNotice("Loaded Taglet "+
                    toLoad.getName());
            }
            catch (final Throwable t) {
                theJavaDocConfiguration.root
                    .printError("Unable to load taglet ["+userTaglet.getName()+
                        "] for reason "+t.getMessage());
            }
        }
    }

    /**
     * Register the taglet with the {@code JavaDoc} engine
     * 
     * @param taglet to register with the {@code JavaDoc} engine
     * @since 2.0
     */
    @SuppressWarnings({"unchecked"})
    private void registerTaglet(BaseTaglet taglet) {
        // Don't do this, as I would like the order retained
        // Remove an old reference if there is one
        // theTagletMap.remove(taglet.getName());

        // Load the new one
        theTagletMap.put(taglet.getName(),taglet);
        theJavaDocConfiguration.root.printNotice("Loaded Taglet "+
            taglet.getName());
    }
}
