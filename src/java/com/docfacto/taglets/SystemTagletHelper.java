package com.docfacto.taglets;
import static com.docfacto.taglets.TagletHtmlConstants.*;

import com.docfacto.dita.Note;
import com.docfacto.dita.P;
import com.docfacto.dita.WinTitle;
import com.docfacto.dita.Note.TYPE;
import com.docfacto.doclets.dita.DITATagletOutput;
import com.docfacto.javadoc.TagParser;
import com.docfacto.xml.XMLTextElement;

/**
 * Helper Class for the {@code docfacto.system} taglet
 * 
 * @author dhudson - created 13 Aug 2013
 * @since 2.4
 */
public class SystemTagletHelper {

    private static final String ICON_CLASS = ICON_CLASS_WHITE + ICON_LIST_ALT;

    private static final String NO_TITLE_PREFIX = "<h4><em class=\""+ICON_CLASS+"\"></em>";
    private static final String TITLE_PREFIX = "<h4 class=\""+H4_TITLE+"\">"+"<em class=\""+ICON_CLASS+"\"></em>";
    private static final String TITLE_POSTFIX = "</h4>";    
    
    private static final String TAG_CLASS = TAG_CLASS_BLOCK+TAGLET_SYSTEM;

    /**
     * HTML prefix to wrap the ToDo section in
     */
    public static final String SYSTEM_PREFIX = "<div class=\""+TAG_CLASS+
        "\">";

    /**
     * HTML postfix to wrap the system section in
     */
    public static final String SYSTEM_POSTFIX = "</div>";

    /**
     * Generate the HTML representation of the taglet
     * 
     * @param parser to process
     * @return a html representation of the taglet
     * @since 2.4
     */
    public static CharSequence generateHTMLOutput(TagParser parser) {
        final StringBuilder builder =
            new StringBuilder(SYSTEM_PREFIX.length()+
                SYSTEM_POSTFIX.length());

        builder.append(SYSTEM_PREFIX);
        
        if (parser.hasTitleAttribute()) {
            builder.append(TITLE_PREFIX);
            builder.append(parser.getTitleAttribute());
        }
        else{
            builder.append(NO_TITLE_PREFIX);
        }

        builder.append(TITLE_POSTFIX);
        
        builder.append(parser.getTagText());
        builder.append(SYSTEM_POSTFIX);

        return builder.toString();
    }

    /**
     * Generate the DITA representation of the taglet
     * 
     * @param parser to process
     * @return the DITA representation of the taglet
     * @since 2.4
     */
    public static DITATagletOutput generateDITAOutput(TagParser parser) {
        DITATagletOutput output = new DITATagletOutput();

        final Note note = new Note();
        note.setType(TYPE.OTHER);
        note.setOtherProps("system");

        if (parser.hasTitleAttribute()) {
            final P p = new P();
            // Shameful use of WinTitle here ..
            p.addElement(new WinTitle(parser
                .getAttributeValue(TagParser.TITLE_ATTRIBUTE)));

            note.addElement(p);
        }

        note.addElement(new XMLTextElement(parser.getTagText().toString()));
        
        output.appendDITAElement(note);

        return output;
    }
}
