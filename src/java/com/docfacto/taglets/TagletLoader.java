package com.docfacto.taglets;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.config.XmlConfig;
import com.docfacto.config.generated.TagletDefinition;
import com.docfacto.taglets.dita.DITATaglet;
import com.docfacto.taglets.eclipse.AbstractEclipseTaglet;
import com.docfacto.taglets.generated.InternalTagletDefinition;
import com.docfacto.taglets.generated.Taglets;

/**
 * Load the Taglets.xml
 * 
 * @author dhudson - created 16 Jan 2013
 * @since 2.1
 */
public class TagletLoader {

    /**
     * JAXB Package name constant {@value}
     */
    private static final String JAXB_PACKAGE_NAME =
    "com.docfacto.taglets.generated";

    /**
     * Name of the validating XSD {@value}
     */
    private static final String JAXB_XSD_LOCATION =
    "com/docfacto/taglets/resources/Taglets.xsd";

    /**
     * Location of the {@code Taglets.xm} {@value};
     */
    private static final String TAGLETS_XML_LOCATION =
    "com/docfacto/taglets/resources/Taglets.xml";

    private Taglets theTaglets;

    /**
     * Create a new instance of <code>TagletLoader</code>.
     * 
     * @throws DocfactoException if unable to load the embedded XML file.
     */
    public TagletLoader() throws DocfactoException {
        loadTaglets();
    }

    /**
     * Load the Docfacto taglets
     * 
     * @since 2.1
     */
    private void loadTaglets() throws DocfactoException {

        try {
            // This needs to be done, as the JavaDoc taglet engine doesn't
            // extend the jar file properly

            final ClassLoader cl = IOUtils.getClassLoader();

            final JAXBContext context =
            JAXBContext.newInstance(JAXB_PACKAGE_NAME,cl);
            final Unmarshaller unmarshaller = context.createUnmarshaller();

            final StreamSource xsdSource =
            new StreamSource(IOUtils.getResourceAsStream(JAXB_XSD_LOCATION));

            final StreamSource[] sources = {xsdSource};

            final SchemaFactory factory =
            SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
            final Schema schema = factory.newSchema(sources);

            unmarshaller.setSchema(schema);

            theTaglets = (Taglets)unmarshaller.unmarshal(IOUtils
                .getResourceAsStream(TAGLETS_XML_LOCATION));

        }
        catch (final JAXBException ex) {
            if (ex.getLinkedException()!=null) {
                throw new DocfactoException(
                    "Unable to load the Taglets.xml file for reason "+
                    ex.getLinkedException().getMessage(),
                    ex.getLinkedException());
            }
            throw new DocfactoException(
                "Unable to load the Taglets.xml file for reason "+
                ex.getMessage(),ex);
        }
        catch (final SAXException ex) {
            throw new DocfactoException(
                "Unable to load the Taglets.xml for reason "+ex.getMessage(),ex);
        }
    }

    /**
     * Return the internal taglet definition for the tagname, or null if not
     * found
     * 
     * @param tagName
     * @return the internal taglet definition or null if not found
     * @since 2.1
     */
    public InternalTagletDefinition getTaglet(String tagName) {
        for (final InternalTagletDefinition taglet:theTaglets
        .getTagletDefinitions()) {
            if (taglet.getName().equals(tagName)) {
                return taglet;
            }
        }

        return null;
    }

    /**
     * Return a list of custom taglets
     * 
     * @return a list of custom taglets
     * @since 2.1
     */
    public List<InternalTagletDefinition> getCustomTaglets() {
        final ArrayList<InternalTagletDefinition> taglets =
        new ArrayList<InternalTagletDefinition>(10);

        for (final InternalTagletDefinition taglet:theTaglets
        .getTagletDefinitions()) {
            if (taglet.isCustom()) {
                taglets.add(taglet);
            }
        }

        return taglets;
    }

    /**
     * Depending on the Docfacto.xml, this will load the taglets, but not
     * register them with the taglet manager
     * 
     * @param config
     * @return a array of BaseTaglets to register
     * @since 2.1
     */
    public List<BaseTaglet> getHtmlTagletsToRegister(XmlConfig config) {

        final List<BaseTaglet> taglets = new ArrayList<BaseTaglet>();

        // Lets add the control taglets
        for (final InternalTagletDefinition internalDef:theTaglets
        .getTagletDefinitions()) {
            try {
                if (internalDef.getHtmlClassName()!=null) {
                    final Class<?> baseTagletClass =
                    Class.forName(internalDef.getHtmlClassName());

                    final BaseTaglet toLoad =
                    (BaseTaglet)baseTagletClass.newInstance();

                    toLoad.setInternalDefinition(internalDef);

                    if (internalDef.isAlwaysLoad()) {
                        // Load it anyway
                        taglets.add(toLoad);
                    }
                    else {
                        // Is it listed in the config?
                        for (final TagletDefinition tagletDef:config
                        .getTagletsConfig()
                        .getTaglets()) {
                            if (internalDef.getName().equals(
                                tagletDef.getName())) {
                                // We have found a match
                                toLoad.setTagletDefinition(tagletDef);
                                // Lets add it
                                taglets.add(toLoad);
                            }
                        }
                    }
                }
            }
            catch (final Exception ex) {
                System.err.println("Unable to load taglet "+
                internalDef.getName()+ " for reason " + ex.getLocalizedMessage());
            }
        }

        return taglets;
    }

    /**
     * Fetch a list of custom DITA taglets, which are enabled in the config
     * 
     * @param config to process
     * @return A list of DITA custom taglets
     * @since 2.2
     */
    public List<DITATaglet> getDitaTagletsToRegister(XmlConfig config) {
        final List<DITATaglet> taglets = new ArrayList<DITATaglet>();

        // Lets add the control taglets
        for (final InternalTagletDefinition internalDef:theTaglets
        .getTagletDefinitions()) {
            try {
                if (internalDef.getDitaClassName()!=null&& internalDef.isCustom()) {
                    final Class<?> baseTagletClass =
                    Class.forName(internalDef.getDitaClassName());

                    final DITATaglet toLoad =
                    (DITATaglet)baseTagletClass.newInstance();

                    toLoad.setInternalDefinition(internalDef);

                    if (internalDef.isAlwaysLoad()) {
                        // Load it anyway
                        taglets.add(toLoad);
                    }
                    else {
                        // Is it listed in the config?
                        for (final TagletDefinition tagletDef:config
                        .getTagletsConfig()
                        .getTaglets()) {
                            if (internalDef.getName().equals(
                                tagletDef.getName())) {
                                // We have found a match
                                toLoad.setTagletDefinition(tagletDef);
                                // Lets add it
                                taglets.add(toLoad);
                            }
                        }
                    }
                }
            }
            catch (final Exception ex) {
                System.err.println("Unable to load taglet "+
                internalDef.getName()+ " for reason " + ex.getLocalizedMessage());
            }
        }

        return taglets;

    }

    /**
     * Fetch a list of eclipse taglets from the internal xml
     * 
     * @return a list of taglets
     * @since 2.2
     */
    public List<AbstractEclipseTaglet> getEclipseTaglets() {
        final ArrayList<AbstractEclipseTaglet> taglets =
        new ArrayList<AbstractEclipseTaglet>();

        for (final InternalTagletDefinition internalDef:theTaglets
        .getTagletDefinitions()) {
            try {
                if (internalDef.getEclipseClassName()!=null) {
                    final Class<?> abstractTagletClass =
                    Class.forName(internalDef.getEclipseClassName());

                    final AbstractEclipseTaglet toLoad =
                    (AbstractEclipseTaglet)abstractTagletClass
                    .newInstance();

                    // Set the internal def
                    toLoad.setInternalDefinition(internalDef);

                    taglets.add(toLoad);
                }
            }
            catch (final Exception ex) {
                System.err.println("Unable to load eclipse taglet "+
                internalDef.getName()+ " for reason " + ex.getLocalizedMessage());
            }
        }

        return taglets;
    }
}
