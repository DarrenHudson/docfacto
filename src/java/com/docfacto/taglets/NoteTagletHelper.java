package com.docfacto.taglets;
import static com.docfacto.taglets.TagletHtmlConstants.*;

import com.docfacto.dita.Note;
import com.docfacto.dita.Note.TYPE;
import com.docfacto.dita.P;
import com.docfacto.dita.WinTitle;
import com.docfacto.doclets.dita.DITATagletOutput;
import com.docfacto.javadoc.TagParser;
import com.docfacto.xml.XMLTextElement;

/**
 * Helper class for the Note taglet
 * 
 * @author dhudson - created 13 Aug 2013
 * @since 2.4
 */
public class NoteTagletHelper {

    /**
     * CSS Classes for taglet
     */
    private static final String TAG_CLASS = TAG_CLASS_INLINE + TAGLET_NOTE;
    /**
     * CSS Classes for icon
     */
    private static final String ICON_CLASS = ICON_CLASS_WHITE + ICON_PENCIL;

    private static final String TAG_PREFIX = "<div class=\""+TAG_CLASS+ "\">";
    private static final String TAG_SUFFIX = "</div>";

    private static final String TITLE_PREFIX = "<h4 class=\""+H4_TITLE+"\">"+"<em class=\""+ICON_CLASS+"\"></em>";
    private static final String NO_TITLE_PREFIX = "<h4><em class=\""+ICON_CLASS+"\"></em>";
    private static final String TITLE_POSTFIX = "</h4>";

    /**
     * Generate the HTML representation of taglet
     * 
     * @param parser to process
     * @return the HTML representation of the taglet
     * @since 2.4
     */
    public static CharSequence generateHTMLOutput(TagParser parser) {
        final StringBuilder builder =
            new StringBuilder(TAG_PREFIX.length()+
                TAG_SUFFIX.length());
        
        builder.append(TAG_PREFIX);

        if (parser.hasTitleAttribute()) {
            builder.append(TITLE_PREFIX);
            builder.append(parser.getTitleAttribute());
        }
        else{
            builder.append(NO_TITLE_PREFIX);
        }
        
        builder.append(TITLE_POSTFIX);
        
        builder.append(parser.getTagText());
        builder.append(TAG_SUFFIX);

        return builder.toString();
    }

    /**
     * Generate the DITA representation of the taglet
     * 
     * @param parser to process
     * @return the DITA representation of the taglet
     * @since 2.4
     */
    public static DITATagletOutput generateDITAOutput(TagParser parser) {
        DITATagletOutput output = new DITATagletOutput();

        final Note note = new Note();
        note.setType(TYPE.NOTE);

        if (parser.hasTitleAttribute()) {
            final P p = new P();
            // Shameful use of WinTitle here ..
            p.addElement(new WinTitle(parser
                .getAttributeValue(TagParser.TITLE_ATTRIBUTE)));

            note.addElement(p);
        }

        //REMOVE:
        System.out.println(parser.getTagText().toString());
        
        note.addElement(new XMLTextElement(parser.getTagText().toString()));

        output.appendDITAElement(note);

        return output;
    }
    
    public static void main(String [] args){
        TagParser parser = new TagParser("note","title=\"always\" This should not be used on live elements unless its in " + '\n' +
        		" *the \nupdate manager thread  ");
        System.out.println(NoteTagletHelper.generateHTMLOutput(parser));
    }

}
