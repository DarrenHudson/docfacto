package com.docfacto.taglets;

import com.docfacto.common.Platform;
import com.docfacto.common.StringUtils;
import com.docfacto.javadoc.TagParser;

/**
 * Constants for the media taglet
 * 
 * @author dhudson - created 27 Mar 2013
 * @since 2.2
 */
public class MediaTagletHelper {

    public static final String TAGLET_NAME = "docfacto.media";

    public static final String HEIGHT_ATTRIBUTE = "height";
    public static final String WIDTH_ATTRIBUTE = "width";
    public static final String ALIGN_ATTRIBUTE = "align";
    public static final String SCALE_ATTRIBUTE = "scale";
    public static final String PLACEMENT_ATTRIBUTE = "placement";

    /**
     * Look at either the attribute type if specified, or the extension of the
     * media type to calculate the type. This can return null if the uri is not
     * present.
     * 
     * @param parser to process
     * @return a string containing the image type
     * @since 2.2
     */
    public static String getImageType(TagParser parser) {

        String uri = parser.getAttributeValue(TagParser.URI_ATTRIBUTE);
        if (uri==null) {
            return null;
        }

        if (parser.hasAttribute(TagParser.TYPE_ATTRIBUTE)) {
            return parser.getAttributeValue(TagParser.TYPE_ATTRIBUTE);
        }

        return StringUtils.getExtension(uri);
    }

    /**
     * Check to see if the image is an SVG
     * 
     * @param parser to process
     * @return true if the uri attribute points to an SVG
     * @since 2.3
     */
    public static boolean isSVG(TagParser parser) {
        String extension = getImageType(parser);
        if (extension!=null) {
            if (extension.equalsIgnoreCase("svg")) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check t see if the image is an MP4 image
     * 
     * @param parser to process
     * @return true if its an MP4 file..
     * @since 2.4
     */
    public static boolean isMP4(TagParser parser) {
        String extension = getImageType(parser);
        if (extension!=null) {
            if (extension.equalsIgnoreCase("mp4")) {
                return true;
            }
        }

        return false;
    }

    /**
     * Generate the HTML representation for this taglet
     * 
     * @param parser to process
     * @return a HTML representation of the media taglet
     * @since 2.4
     */
    public static CharSequence generateHTMLOutput(TagParser parser) {
        String type = MediaTagletHelper.getImageType(parser);
        StringBuilder builder = new StringBuilder(100);

        if (type!=null) {

            if (isMP4(parser)) {
                builder.append("<video ");
                addAttributeIfExists(builder,
                    MediaTagletHelper.HEIGHT_ATTRIBUTE,parser);
                addAttributeIfExists(builder,MediaTagletHelper.WIDTH_ATTRIBUTE,
                    parser);

                builder.append(" controls>");
                builder.append("<source src=\"");
                builder.append(parser
                    .getAttributeValue(TagParser.URI_ATTRIBUTE));
                builder.append("\" type=\"video/mp4\">");
                builder.append("Your browser does not support mp4 videos");
                builder.append("</video>");
            }
            else {

                boolean isSVG = MediaTagletHelper.isSVG(parser);
                if (Platform.isWindows()&&isSVG) {
                    builder.append("<embed type=\"image/svg+xml\" src=\"");
                }
                else {
                    builder.append("<img src=\"");
                }

                builder.append(parser
                    .getAttributeValue(TagParser.URI_ATTRIBUTE));
                builder.append("\" ");

                addAttributeIfExists(builder,
                    MediaTagletHelper.HEIGHT_ATTRIBUTE,parser);
                addAttributeIfExists(builder,MediaTagletHelper.WIDTH_ATTRIBUTE,
                    parser);

                if (Platform.isWindows()&&isSVG) {
                    builder.append("/");
                }

                builder.append(">");
            }
        }

        return builder;
    }

    /**
     * Add attributes if they are in the taglet
     * 
     * @param builder
     * @param attribute
     * @param parser
     * @since 2.4
     */
    public static void addAttributeIfExists(StringBuilder builder,
    String attribute,
    TagParser parser) {
        if (parser.hasAttribute(attribute)) {
            builder.append(" ");
            builder.append(attribute);
            builder.append("=");
            builder.append("\"");
            builder.append(parser.getAttributeValue(attribute));
            builder.append("\"");
        }
    }
}
