package com.docfacto.taglets;
import static com.docfacto.taglets.TagletHtmlConstants.*;

import com.docfacto.dita.Note;
import com.docfacto.dita.Note.TYPE;
import com.docfacto.dita.P;
import com.docfacto.dita.WinTitle;
import com.docfacto.doclets.dita.DITATagletOutput;
import com.docfacto.javadoc.TagParser;
import com.docfacto.xml.XMLTextElement;

/**
 * Helper class for the ToDo Taglet
 * 
 * @author dhudson - created 13 Aug 2013
 * @since 2.4
 */
public class ToDoTagletHelper {

    private static final String TITLE_PREFIX = "<h4>";
    private static final String TITLE_POSTFIX = "</h4>";

    /**
     * CSS Classes for this taglet
     */
    private static final String TAG_CLASS = TAG_CLASS_INLINE+TAGLET_TODO;

    /**
     * CSS Classes for icon
     */
    private static final String ICON_CLASS =
        ICON_CLASS_WHITE+ICON_EXCLAMATION;

    /**
     * HTML prefix to wrap the ToDo section in
     */
    public static final String TODO_PREFIX = "<p><div class=\""+TAG_CLASS+
        "\"><em class=\""+ICON_CLASS+"\"></em><h4>Todo: </h4>";

    /**
     * HTML postfix to wrap the ToDo section in
     */
    private static final String TODO_POSTFIX = "</div></p>";

    /**
     * Generate a HTML representation of the taglet
     * 
     * @param parser to process
     * @return HTML representation of the taglet
     * @since 2.4
     */
    public static CharSequence generateHTMLOutput(TagParser parser) {
        StringBuilder builder =
            new StringBuilder(TODO_PREFIX.length()+
                parser.getTagText().length()+
                TODO_POSTFIX.length());

        builder.append(TODO_PREFIX);
        
        if (parser.hasTitleAttribute()) {
            builder.append(TITLE_PREFIX);
            builder.append(parser.getTitleAttribute());
            builder.append(TITLE_POSTFIX);
        }

        builder.append(parser.getTagText());
        builder.append(TODO_POSTFIX);

        return builder.toString();
    }

    /**
     * Generate the DITA representation of the taglet
     * 
     * @param parser to process
     * @return the DITA representation of the taglet
     * @since 2.4
     */
    public static DITATagletOutput generateDITAOutput(TagParser parser) {

        DITATagletOutput output = new DITATagletOutput();

        final Note note = new Note();
        note.setType(TYPE.OTHER);
        note.setOtherProps("todo");

        if (parser.hasTitleAttribute()) {
            final P p = new P();
            // Shameful use of WinTitle here ..
            p.addElement(new WinTitle(parser
                .getAttributeValue(TagParser.TITLE_ATTRIBUTE)));

            note.addElement(p);
        }

        note.addElement(new XMLTextElement(parser.getTagText().toString()));
        
        output.appendDITAElement(note);

        return output;
    }
}
