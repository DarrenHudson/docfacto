/*
 * @author dhudson -
 * Created 5 Apr 2013 : 11:58:16
 */

package com.docfacto.taglets;

/**
 * Link Taglet Constants
 *
 * @author dhudson - created 5 Apr 2013
 * @since 2.2
 */
public class LinkTagletConstants {

    public static final String LINK_TAGLET_NAME = "docfacto.link";

}
