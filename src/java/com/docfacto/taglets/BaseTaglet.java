/*
 * @author dhudson -
 * Created 28 Jun 2012 : 14:51:01
 */

package com.docfacto.taglets;

import com.docfacto.common.DocfactoException;
import com.docfacto.config.generated.TagletDefinition;
import com.docfacto.taglets.generated.InternalTagletDefinition;
import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;
import com.sun.tools.doclets.formats.html.TagletOutputImpl;
import com.sun.tools.doclets.internal.toolkit.Configuration;
import com.sun.tools.doclets.internal.toolkit.taglets.Taglet;
import com.sun.tools.doclets.internal.toolkit.taglets.TagletOutput;
import com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter;

/**
 * Base Taglet class that handles the Taglet interface
 * 
 * @author dhudson - created 28 Jun 2012
 * @since 2.0
 */
public abstract class BaseTaglet implements Taglet {

    private TagletDefinition theTagletDefinition;

    private InternalTagletDefinition theInternalDefintion;

    /**
     * The configuration from the {@code JavaDoc} engine
     */
    private Configuration theJavaDocConfiguration;

    /**
     * Is the taglet enabled?
     */
    private boolean isEnabled;

    /**
     * From the Configuration file, set if the {@code taglet} is enabled
     * 
     * @param enabled
     * @since 2.0
     */
    public final void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    /**
     * If this {@code taglet} not is enabled, no processing will take place
     * 
     * @return true is the {@code Taglet} is enabled
     * @since 2.0
     */
    public final boolean isEnabled() {
        return isEnabled;
    }

    /**
     * Sets the definition from the XML file, if null, then the taglet is
     * disabled
     * 
     * @param tagletDefinition definition from XML file
     * @since 2.0
     */
    public void setTagletDefinition(TagletDefinition tagletDefinition) {
        if (tagletDefinition==null) {
            setEnabled(false);
        }
        else {
            theTagletDefinition = tagletDefinition;

            setEnabled(tagletDefinition.isEnabled());
        }
    }

    /**
     * Return the taglet definition
     * 
     * @return the taglet definition
     * @since 2.1
     */
    public TagletDefinition getTagletDefintion() {
        return theTagletDefinition;
    }

    /**
     * Set the internal definition, this is done via the TagletLoader
     * 
     * @param internalDefinition
     * @since 2.2
     */
    public void setInternalDefinition(
    InternalTagletDefinition internalDefinition) {
        theInternalDefintion = internalDefinition;
    }

    /**
     * Fetch the internal definition
     * 
     * @return the internal definition as defined by the Taglets.xml
     * @since 2.2
     */
    public InternalTagletDefinition getInternalDefinition() {
        return theInternalDefintion;
    }

    /**
     * @see com.sun.tools.doclets.Taglet#inField()
     */
    @Override
    public boolean inField() {
        return true;
    }

    /**
     * @see com.sun.tools.doclets.Taglet#inConstructor()
     */
    @Override
    public boolean inConstructor() {
        return true;
    }

    /**
     * @see com.sun.tools.doclets.Taglet#inMethod()
     */
    @Override
    public boolean inMethod() {
        return true;
    }

    /**
     * @see com.sun.tools.doclets.Taglet#inOverview()
     */
    @Override
    public boolean inOverview() {
        return true;
    }

    /**
     * @see com.sun.tools.doclets.internal.toolkit.taglets.Taglet#isInlineTag()
     */
    @Override
    public boolean isInlineTag() {
        return true;
    }

    /**
     * @see com.sun.tools.doclets.Taglet#inPackage()
     */
    @Override
    public boolean inPackage() {
        return true;
    }

    /**
     * @see com.sun.tools.doclets.Taglet#inType()
     */
    @Override
    public boolean inType() {
        return true;
    }

    /**
     * This method is used to mask the access restriction for eclipse
     * 
     * @return Return the name of the Taglet
     * @since 2.2
     */
    public String getTagletName() {
        return getName();
    }

    /**
     * Abstract method for {@code inline} tag processing
     * 
     * @param text the inline text
     * @param tag being processed
     * @return the character sequence for the inline tag
     * @throws DocfactoException
     * @since 2.0
     */
    public abstract CharSequence processInlineTag(String text,Tag tag)
    throws DocfactoException;

    /**
     * Abstract method for {@code block} tag processing
     * 
     * @param tag to process
     * @param holder tag holder
     * @param writer taglet writer
     * @return the character sequence for the block tag
     * @throws DocfactoException
     * @since 2.0
     */
    public abstract CharSequence processBlockTag(Tag[] tag,Doc holder,
    TagletWriter writer) throws DocfactoException;

    /**
     * {@docfacto.system This method is used for {@code inLine} tags }
     * 
     * @see com.sun.tools.doclets.internal.toolkit.taglets.Taglet#getTagletOutput(com.sun.javadoc.Tag,
     * com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter)
     * 
     */
    @Override
    public final TagletOutput getTagletOutput(
    Tag tag,TagletWriter writer
    ) throws IllegalArgumentException {
        final StringBuilder builder = new StringBuilder();

        final TagletOutput output = writer.getOutputInstance();

        if (isEnabled()) {
            try {

                builder.append(processInlineTag(
                    writer.commentTagsToOutput(tag,tag.inlineTags())
                        .toString(),tag));
            }
            catch (final DocfactoException ex) {
                theJavaDocConfiguration.root.printError(
                    tag.position(),
                    "Unable to process inline tag "+getName()+" for reason "+
                        ex.getMessage());
            }
        }

        output.setOutput(builder.toString());

        return output;
    }

    /**
     * {@docfacto.system This method is used for {@code block} tags }
     * 
     * @see com.sun.tools.doclets.internal.toolkit.taglets.Taglet#getTagletOutput(com.sun.javadoc.Doc,
     * com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter)
     */
    @Override
    public TagletOutput getTagletOutput(Doc holder,TagletWriter writer)
    throws IllegalArgumentException {
        final Tag[] tags = holder.tags(getName());

        if (tags.length!=0&&isEnabled()) {

            final StringBuilder builder = new StringBuilder(100);
            try {
                builder.append(processBlockTag(tags,holder,writer));
            }
            catch (final DocfactoException ex) {
                theJavaDocConfiguration.root.printError(
                    holder.position(),
                    "Unable to process block tag "+getName()+
                        " for reason "+
                        ex.getMessage());
            }

            return new TagletOutputImpl(
                builder.toString());
        }

        return null;
    }
}
