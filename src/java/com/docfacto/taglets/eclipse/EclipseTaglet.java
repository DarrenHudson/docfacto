package com.docfacto.taglets.eclipse;

import com.docfacto.javadoc.TagParser;

/**
 * Eclipse Taglets must implement this interface.
 * 
 * The reason for not being able to run normal {@code Taglet}s is that Eclipse
 * and 1.7, don't include tools.jar
 * 
 * 
 * @author dhudson - created 23 Mar 2013
 * @since 2.2
 */
public interface EclipseTaglet {

    /**
     * The name of the taglet, without the initial {@literal @}
     *
     * @return name of the taglet
     * @since 2.2
     */
    public String getName();
    
    /**
     * Process the Block Tag.
     * 
     * Taglet processing for Eclipse. Eclipse doesn't use JavaDoc, it has its
     * own rendering engine.
     * 
     * For normal block tags the output should be something like
     * 
     * <pre>
     * <dl>...
     *  <dt>Taglet Title<dt>
     *  <dd>Taglet text</dd>
     * </pre>
     *
     * @param parser to process
     * @return the result
     * @since 2.2
     */
    public CharSequence getEclipseBlockOutput(TagParser parser);
    
    /**
     * Process the Inline Tag.  At this point, all nested inline tags will have been resolved
     *
     * @param parser
     * @return the result
     * @since 2.2
     */
    public CharSequence getEclipseInlineOutput(TagParser parser);
    
}