package com.docfacto.taglets.eclipse;

import com.docfacto.javadoc.TagParser;
import com.docfacto.taglets.generated.InternalTagletDefinition;

/**
 * Abstract Eclipse Taglet
 * 
 * @author dhudson - created 23 Mar 2013
 * @since 2.2
 */
public abstract class AbstractEclipseTaglet implements EclipseTaglet {

    private InternalTagletDefinition theDef;
    
    /**
     * Constructor.
     */
    public AbstractEclipseTaglet() {
        
    }
    
    /**
     * Set the internal definition
     *
     * @param def
     * @since 2.2
     */
    public void setInternalDefinition(InternalTagletDefinition def) {
        theDef = def;
    }
    
    /**
     * @see com.docfacto.taglets.eclipse.EclipseTaglet#getName()
     */
    public String getName() {
        return theDef.getName();
    }
    
    /**
     * @see com.docfacto.taglets.eclipse.EclipseTaglet#getEclipseBlockOutput(com.docfacto.javadoc.TagParser)
     */
    @Override
    public CharSequence getEclipseBlockOutput(TagParser parser) {
        return "<dt>"+getName()+" <i>"+parser.getTagText()+"</i></dt>";
    }

    /**
     * @see com.docfacto.taglets.eclipse.EclipseTaglet#getEclipseInlineOutput(com.docfacto.javadoc.TagParser)
     */
    @Override
    public CharSequence getEclipseInlineOutput(TagParser parser) {
        return "";
    }

    /**
     * Check to see if the tag is inline or not
     * 
     * @return true, if this is an inline tag
     * @since 2.2
     */
    public boolean isInlineTag() {
        return true;
    }
}
