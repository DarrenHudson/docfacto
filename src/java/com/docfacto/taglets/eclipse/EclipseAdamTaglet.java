package com.docfacto.taglets.eclipse;


/**
 * Eclipse Adam Taglet
 * 
 * @author dhudson - created 23 Mar 2013
 * @since 2.2
 */
public class EclipseAdamTaglet extends AbstractEclipseTaglet {

    public EclipseAdamTaglet(){
        
    }
    
    /**
     * @see com.docfacto.taglets.eclipse.AbstractEclipseTaglet#isInlineTag()
     */
    @Override
    public boolean isInlineTag() {
        return false;
    }

}
