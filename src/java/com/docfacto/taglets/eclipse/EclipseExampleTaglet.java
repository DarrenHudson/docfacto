package com.docfacto.taglets.eclipse;

import com.docfacto.javadoc.TagParser;
import com.docfacto.taglets.ExampleTagletHelper;

/**
 * Process the Example Taglet for Eclipse
 * 
 * @author dhudson - created 23 Mar 2013
 * @since 2.2
 */
public class EclipseExampleTaglet extends AbstractEclipseTaglet {

    /**
     * @see com.docfacto.taglets.eclipse.EclipseTaglet#getEclipseInlineOutput(com.docfacto.javadoc.TagParser)
     */
    @Override
    public CharSequence getEclipseInlineOutput(TagParser parser) {
        return ExampleTagletHelper.generateHTML(parser);
    }

}
