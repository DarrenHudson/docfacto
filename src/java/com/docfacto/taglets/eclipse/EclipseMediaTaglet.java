package com.docfacto.taglets.eclipse;

import com.docfacto.javadoc.TagParser;
import com.docfacto.taglets.MediaTagletHelper;

/**
 * Eclipse media taglet
 * 
 * @author dhudson - created 27 Mar 2013
 * @since 2.2
 */
public class EclipseMediaTaglet extends AbstractEclipseTaglet {

    /**
     * @see com.docfacto.taglets.eclipse.AbstractEclipseTaglet#getEclipseInlineOutput(com.docfacto.javadoc.TagParser)
     */
    @Override
    public CharSequence getEclipseInlineOutput(TagParser parser) {
        return MediaTagletHelper.generateHTMLOutput(parser);
    }

}
