package com.docfacto.taglets.eclipse;

import com.docfacto.common.NameValuePair;
import com.docfacto.javadoc.TagParser;

/**
 * Eclipse version of the Link Taglet
 * 
 * @author dhudson - created 23 Mar 2013
 * @since 2.2
 */
public class EclipseLinkTaglet extends AbstractEclipseTaglet {

    /**
     * @see com.docfacto.taglets.eclipse.AbstractEclipseTaglet#getEclipseBlockOutput(com.docfacto.javadoc.TagParser)
     */
    public String getEclipseBlockOutput(TagParser parser) {
        final StringBuilder builder = new StringBuilder(50);
        builder.append("<dt>"+getName()+" <i>");

        for (final NameValuePair attr:parser.getAttributes()) {
            builder.append(attr.toString());
            builder.append(" ");
        }

        builder.append("</i></dt>");
        return builder.toString();
    }
}
