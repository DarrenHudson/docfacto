package com.docfacto.taglets.eclipse;

import com.docfacto.javadoc.TagParser;
import com.docfacto.taglets.NoteTagletHelper;

/**
 * Eclipse version of the Note Taglet
 * 
 * @author dhudson - created 23 Mar 2013
 * @since 2.2
 */
public class EclipseNoteTaglet extends AbstractEclipseTaglet {

    /**
     * @see com.docfacto.taglets.eclipse.AbstractEclipseTaglet#getEclipseInlineOutput(com.docfacto.javadoc.TagParser)
     */
    @Override
    public CharSequence getEclipseInlineOutput(TagParser parser) {
        return NoteTagletHelper.generateHTMLOutput(parser);
    }

}
