package com.docfacto.taglets;

import static com.docfacto.taglets.TagletHtmlConstants.*;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.common.StringUtils;
import com.docfacto.dita.CodeBlock;
import com.docfacto.dita.P;
import com.docfacto.dita.WinTitle;
import com.docfacto.doclets.dita.DITATagletOutput;
import com.docfacto.javadoc.TagParser;

/**
 * Helper class for the example taglet. This is required as no internal sun
 * classes are allowed in Eclipse with version 1.7
 * 
 * @author dhudson - created 23 Mar 2013
 * @since 2.2
 */
public class ExampleTagletHelper {

    private static final String TAG_CLASS = TAG_CLASS_BLOCK+TAGLET_EXAMPLE;
    private static final String ICON_CLASS = ICON_CLASS_WHITE+ICON_EYE;

    private static final String PRE_PREFIX = "<pre class=\""+DOCFACTO_PRE+"\">";

    private static final String TAG_PREFIX = "<div class=\""+TAG_CLASS+"\">";
    private static final String TAG_POSTFIX = "</pre></div>";

    private static final String TITLE_PREFIX = "<h4 class=\""+H4_TITLE+"\">"+
        "<em class=\""+ICON_CLASS+"\"></em>";
    private static final String NO_TITLE_PREFIX = "<h4><em class=\""+
        ICON_CLASS+"\"></em>";

    private static final String TITLE_POSTFIX = "</h4>";

    private ExampleTagletHelper() {
    }

    /**
     * Produce HTML using the attributes set in the parser. This will include
     * titles, of the attribute is present
     * 
     * @param parser to process
     * @return {@code HTML} representation of the taglet
     * @since 2.2
     */
    public static String generateHTML(TagParser parser) {
        final StringBuilder builder =
            new StringBuilder(300);

        builder.append(TAG_PREFIX);

        if (parser.hasTitleAttribute()) {
            builder.append(TITLE_PREFIX);
            builder.append(parser.getTitleAttribute());
        }
        else {
            builder.append(NO_TITLE_PREFIX);
        }
        builder.append(TITLE_POSTFIX);

        builder.append(PRE_PREFIX);

        final String uri = parser.getAttributeValue(TagParser.URI_ATTRIBUTE);

        if (uri==null) {
            buildErrorContent(builder);
        }
        else {
            try {
                builder.append(getURLContent(uri));
            }
            catch (final DocfactoException ex) {
                buildErrorContent(builder);
            }
        }

        builder.append(TAG_POSTFIX);

        return builder.toString();
    }

    /**
     * Register that there is an error
     * 
     * @param builder to append to
     * @since 2.5
     */
    private static void buildErrorContent(StringBuilder builder) {
        builder.append("Unable to locate or resolve URI");
    }

    /**
     * Load the contents of the URL
     * 
     * @param url to process
     * @return the contents, which have been escaped
     * @throws DocfactoException if unable to process the url or load the
     * resource
     * @since 2.2
     */
    public static String getURLContent(String url) throws DocfactoException {
        try {
            final URL toLoad = new URL(url);
            return StringUtils.escapeXML(IOUtils.loadURL(toLoad));
        }
        catch (final MalformedURLException ex) {
            throw new DocfactoException(
                "Example Taglet : Unable to convert uri "+url,ex);
        }
    }

    /**
     * Create a tag parser with the attributes set correctly
     * 
     * @param tagText to process
     * @param file being processed
     * @return a parser with the uri set correctly
     * @throws DocfactoException if unable to locate the file
     * @since 2.2
     */
    public static TagParser createTagParser(String tagText,File file)
    throws DocfactoException {
        final TagParser parser = new TagParser(tagText);

        String resource = null;

        if (parser.hasAttribute(TagParser.URI_ATTRIBUTE)) {
            resource = parser.getAttributeValue(TagParser.URI_ATTRIBUTE);
        }
        else {
            resource = parser.getTagText().toString();
        }

        final File source = locateSourcePath(file);

        resource = resource.trim();

        // Convert the '.' notation to '/' notation
        final String filePath = resource.replace('.','/');
        final File sourceFile = new File(source,filePath+".java");

        // Lets convert this to a URI
        try {
            parser.setAttribute(TagParser.URI_ATTRIBUTE,sourceFile.toURI()
                .toURL().toString());
        }
        catch (final MalformedURLException ex) {
            throw new DocfactoException(
                "Example Taglet : Unable to convert uri "+resource,ex);
        }

        return parser;
    }

    /**
     * Try and locate the source path from the file that we are processing.
     * 
     * Only need to do this once, and then cache the result
     * 
     * @param tag to process
     * @return the file of the current source file
     * @throws DocfactoException if unable to find source path
     * @since 2.0
     */
    private static File locateSourcePath(File file) throws DocfactoException {

        final String path = file.getAbsolutePath();

        final int index =
            path.lastIndexOf(File.separator+"com"+File.separator);
        if (index!=-1) {
            return new File(path.substring(0,index));
        }
        else {
            throw new DocfactoException(
                "ExampleTaglet unable to locate source path");
        }
    }

    /**
     * Generate DITA using the attributes set in the parser
     * 
     * @param parser to process
     * @return DITA representation of the taglet output
     * @throws DocfactoException if unable to locate the example
     * @since 2.2
     */
    public static DITATagletOutput generateDITA(TagParser parser)
    throws DocfactoException {
        final DITATagletOutput tagletOutput = new DITATagletOutput();

        if (parser.hasAttribute(TagParser.TITLE_ATTRIBUTE)) {
            final P p = new P();
            // Shameful use of WinTitle here ..
            p.addElement(new WinTitle(parser
                .getAttributeValue(TagParser.TITLE_ATTRIBUTE)));
            tagletOutput.appendDITAElement(p);
        }

        final String content =
            getURLContent(parser
                .getAttributeValue(TagParser.URI_ATTRIBUTE));

        tagletOutput.appendDITAElement(new CodeBlock(content));

        return tagletOutput;
    }
}
