/*
 * @author dhudson -
 * Created 25 Mar 2013 : 10:56:31
 */

package com.docfacto.taglets;

/**
 * Adam Taglet constans for the other types of Adam taglets
 *
 * @author dhudson - created 25 Mar 2013
 * @since 2.2
 */
public class AdamTagletConstants {

    public static final String TAG_NAME = "docfacto.adam";

    /**
     * Constant value for the for {@value}
     */
    public static final String IGNORE_VALUE = "ignore";

}
