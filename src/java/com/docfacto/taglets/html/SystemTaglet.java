/*
 * @author dhudson -
 * Created 28 Jun 2012 : 14:43:12
 */

package com.docfacto.taglets.html;

import com.docfacto.common.DocfactoException;
import com.docfacto.javadoc.TagParser;
import com.docfacto.taglets.BaseTaglet;
import com.docfacto.taglets.SystemTagletHelper;
import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;
import com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter;

/**
 * This Taglet is used for System Documentation
 * 
 * {@docfacto.system this is an example of an inline {@code docfacto.system}
 * taglet }
 * 
 * @author dhudson - created 30 Jun 2012
 * @since 2.0
 */
public class SystemTaglet extends BaseTaglet {

    private static final String TAG_NAME = "docfacto.system";

    /**
     * Constructor.
     */
    public SystemTaglet() {
    }

    /**
     * @see com.sun.tools.doclets.Taglet#getName()
     */
    @Override
    public String getName() {
        return TAG_NAME;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#processInlineTag(java.lang.String,
     * com.sun.javadoc.Tag)
     */
    @Override
    public CharSequence processInlineTag(String text,Tag tag)
    throws DocfactoException {
        if (isEnabled()) {
            return SystemTagletHelper.generateHTMLOutput(new TagParser(text));
        }

        return null;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#processBlockTag(com.sun.javadoc.Tag[],
     * com.sun.javadoc.Doc,
     * com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter)
     */
    @Override
    public CharSequence
    processBlockTag(Tag[] tag,Doc holder,TagletWriter writer)
    throws DocfactoException {
        // Inline only
        return null;
    }

}
