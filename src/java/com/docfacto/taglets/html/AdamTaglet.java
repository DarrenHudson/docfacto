/*
 * @author dhudson -
 * Created 18 Jul 2012 : 12:35:25
 */

package com.docfacto.taglets.html;

import com.docfacto.taglets.AdamTagletConstants;
import com.docfacto.taglets.BaseTaglet;
import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;
import com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter;

/**
 * This is an empty taglet to stop JavaDoc complaining about
 * {@code docfacto.adam ignore} tags.
 * 
 * @author dhudson - created 18 Jul 2012
 * @since 2.0
 * 
 */
public class AdamTaglet extends BaseTaglet {

    /**
     * Constructor.
     */
    public AdamTaglet() {
    }

    /**
     * @see com.sun.tools.doclets.Taglet#getName()
     */
    @Override
    public String getName() {
        return AdamTagletConstants.TAG_NAME;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#processInlineTag(java.lang.String,
     * com.sun.javadoc.Tag)
     */
    @Override
    public CharSequence processInlineTag(String text,Tag tag) {
        // Don't produce any output
        return "";
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#processBlockTag(com.sun.javadoc.Tag[],
     * com.sun.javadoc.Doc,
     * com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter)
     */
    @Override
    public CharSequence
    processBlockTag(Tag[] tag,Doc holder,TagletWriter writer)
    {
        // Don't produce any output
        return "";
    }

}
