/*
 * @author dhudson -
 * Created 24 Jul 2012 : 11:44:55
 */

package com.docfacto.taglets.html;

import com.docfacto.common.DocfactoException;
import com.docfacto.config.generated.TagletDefinition;
import com.docfacto.taglets.BaseTaglet;
import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;
import com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter;

/**
 * Provide a new {@code since} taglet that has highlight functionality
 * 
 * 
 * @author dhudson - created 26 Jul 2012
 * @since 2.0
 */
public class SinceTaglet extends BaseTaglet {

    /**
     * Note, this doesn't have the Docfacto prefix as it over-rides the default
     * one
     */
    private static final String TAG_NAME = "since";

    private static final String TAG_CLASS =
        "docfacto-taglet docfacto-taglet-block docfacto-taglet-since";

    private static final String TAG_PREFIX = "<dl class=\""+TAG_CLASS+
        "\"><dt><b>Since:</b></dt>";

    private static final String HIGHLIGHT_TEXT = "<dd class=\"highlight\">";

    private static final String NORMAL_TEXT = "<dd>";

    private static final String TAG_SUFFIX = "</dd></dl>";

    /**
     * 
     * The string to match for highlighting
     */
    private String theHighlightString;

    /**
     * Constructor.
     */
    public SinceTaglet() {
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#setTagletDefinition(com.docfacto.config.generated.TagletDefinition)
     */
    @Override
    public void setTagletDefinition(TagletDefinition tagletDefinition) {
        super.setTagletDefinition(tagletDefinition);
        theHighlightString = tagletDefinition.getParam();
    }

    /**
     * @see com.sun.tools.doclets.internal.toolkit.taglets.Taglet#getName()
     */
    @Override
    public String getName() {
        return TAG_NAME;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#isInlineTag()
     */
    @Override
    public boolean isInlineTag() {
        return false;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#processInlineTag(java.lang.String,
     * com.sun.javadoc.Tag)
     */
    @Override
    public CharSequence processInlineTag(String text,Tag tag)
    throws DocfactoException {
        // This is not an inline tag..
        return "";
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#processBlockTag(com.sun.javadoc.Tag[],
     * com.sun.javadoc.Doc,
     * com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter)
     */
    @Override
    public CharSequence
    processBlockTag(Tag[] tag,Doc holder,TagletWriter writer)
    throws DocfactoException {
        final StringBuilder builder = new StringBuilder(100);

        builder.append(TAG_PREFIX);

        if (isHightlighted(tag[0].text())) {
            builder.append(HIGHLIGHT_TEXT);
        }
        else {
            builder.append(NORMAL_TEXT);
        }

        for (int i = 0;i<tag.length;i++) {
            if (i>0) {
                builder.append(", ");
            }

            builder.append(tag[i].text());
        }

        builder.append(TAG_SUFFIX);

        return builder;
    }

    /**
     * See if the text matches the parameter supplied in Docfacto.xml
     * 
     * @param text to check
     * @return true if this tag is to be highlighted
     * @since 2.1
     */
    private boolean isHightlighted(String text) {
        return theHighlightString.equals(text);
    }

}
