package com.docfacto.taglets.html;

import com.docfacto.common.DocfactoException;
import com.docfacto.taglets.BaseTaglet;
import com.docfacto.taglets.ExampleTagletHelper;
import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;
import com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter;

/**
 * This class provides the {@code docfacto.example} taglet for html output.
 * <p>
 * An example of this is {@literal {@docfacto.example
 * uri="com.docfacto.xsdtree.XSDTreeDumper"}
 * 
 * @author dhudson - created 8 Jul 2012
 * @since 2.0
 * 
 * @docfacto.link uri="${doc-taglets}/r_docfacto_taglets.dita" version="2.2" key="example-taglet-class" link-to"doc"
 */
public class ExampleTaglet extends BaseTaglet {

    /**
     * Taglet name {@value}
     */
    private static final String TAG_NAME = "docfacto.example";

    /**
     * Constructor.
     */
    public ExampleTaglet() {
    }

    /**
     * @see com.sun.tools.doclets.Taglet#getName()
     */
    @Override
    public String getName() {
        return TAG_NAME;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#processInlineTag(java.lang.String,
     * com.sun.javadoc.Tag)
     */
    @Override
    public CharSequence processInlineTag(String text,Tag tag)
    throws DocfactoException {
        return ExampleTagletHelper.generateHTML(ExampleTagletHelper
            .createTagParser(tag.text(),tag.position().file()));
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#processBlockTag(com.sun.javadoc.Tag[],
     * com.sun.javadoc.Doc,
     * com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter)
     */
    @Override
    public CharSequence
    processBlockTag(Tag[] tag,Doc holder,TagletWriter writer)
    throws DocfactoException {
        return null;
    }

}
