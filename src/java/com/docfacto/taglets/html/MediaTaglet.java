package com.docfacto.taglets.html;

import com.docfacto.common.DocfactoException;
import com.docfacto.javadoc.TagParser;
import com.docfacto.taglets.BaseTaglet;
import com.docfacto.taglets.MediaTagletHelper;
import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;
import com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter;

/**
 * HTML Version of the media taglet
 * 
 * @author dhudson - created 27 Mar 2013
 * @since 2.2
 */
public class MediaTaglet extends BaseTaglet {

    // <div>
    // <object data="doc-files/docfacto.svg" type="image/svg+xml"></object>
    // </div>

    /**
     * @see com.sun.tools.doclets.internal.toolkit.taglets.Taglet#getName()
     */
    @Override
    public String getName() {
        return MediaTagletHelper.TAGLET_NAME;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#processInlineTag(java.lang.String,
     * com.sun.javadoc.Tag)
     */
    @Override
    public CharSequence processInlineTag(String text,Tag tag)
    throws DocfactoException {
        if (isEnabled()) {
            MediaTagletHelper.generateHTMLOutput(new TagParser(text));
        }

        return null;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#processBlockTag(com.sun.javadoc.Tag[],
     * com.sun.javadoc.Doc,
     * com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter)
     */
    @Override
    public CharSequence processBlockTag(Tag[] tag,Doc holder,
    TagletWriter writer) throws DocfactoException {
        // This is an inline taglet
        return null;
    }
}
