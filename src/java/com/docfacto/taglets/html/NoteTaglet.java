/*
 * @author dhudson -
 * Created 28 Jun 2012 : 15:00:12
 */

package com.docfacto.taglets.html;

import com.docfacto.common.DocfactoException;
import com.docfacto.javadoc.TagParser;
import com.docfacto.taglets.BaseTaglet;
import com.docfacto.taglets.NoteTagletHelper;
import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;
import com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter;

/**
 * NoteTaglet, is a way to provide notes for developers in the JavaDoc
 * 
 * <p>
 * {@docfacto.note This is an example of a note}
 * </p>
 * 
 * @author dhudson - created 30 Jun 2012
 * @since 2.0
 */
public class NoteTaglet extends BaseTaglet {

    /**
     * Taglet name {@value}
     */
    private static final String TAG_NAME = "docfacto.note";

    /**
     * @see com.sun.tools.doclets.Taglet#getName()
     */
    @Override
    public String getName() {
        return TAG_NAME;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#processInlineTag(java.lang.String,
     * com.sun.javadoc.Tag)
     */
    @Override
    public CharSequence processInlineTag(String text,Tag tag)
    throws DocfactoException {
        if (isEnabled()) {
            return NoteTagletHelper.generateHTMLOutput(new TagParser(text));
        }

        return null;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#processBlockTag(com.sun.javadoc.Tag[],
     * com.sun.javadoc.Doc,
     * com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter)
     */
    @Override
    public CharSequence
    processBlockTag(Tag[] tag,Doc holder,TagletWriter writer)
    throws DocfactoException {
        return null;
    }
}
