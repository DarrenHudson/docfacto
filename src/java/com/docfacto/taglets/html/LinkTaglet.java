/*
 * @author dhudson -
 * Created 16 Jan 2013 : 12:50:46
 */

package com.docfacto.taglets.html;

import com.docfacto.common.DocfactoException;
import com.docfacto.taglets.BaseTaglet;
import com.docfacto.taglets.LinkTagletConstants;
import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;
import com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter;

/**
 * The link taglet has no output, just needs to be loaded
 * 
 * @author dhudson - created 16 Jan 2013
 * @since 2.1
 */
public class LinkTaglet extends BaseTaglet {

    /**
     * @see com.sun.tools.doclets.internal.toolkit.taglets.Taglet#getName()
     */
    @Override
    public String getName() {
        return LinkTagletConstants.LINK_TAGLET_NAME;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#processInlineTag(java.lang.String,
     * com.sun.javadoc.Tag)
     */
    @Override
    public CharSequence processInlineTag(String text,Tag tag)
    throws DocfactoException {
        return "";
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#processBlockTag(com.sun.javadoc.Tag[],
     * com.sun.javadoc.Doc,
     * com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter)
     */
    @Override
    public CharSequence
    processBlockTag(Tag[] tag,Doc holder,TagletWriter writer)
    throws DocfactoException {
        return "";
    }

}
