/*
 * @author dhudson -
 * Created 25 Jul 2012 : 16:57:35
 */

package com.docfacto.taglets.html;

import com.docfacto.common.DocfactoException;
import com.docfacto.javadoc.TagParser;
import com.docfacto.taglets.BaseTaglet;
import com.docfacto.taglets.ToDoTagletHelper;
import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;
import com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter;

/**
 * {@docfacto.todo this is an example of the inline {@code docfacto.todo}
 * taglet}
 * 
 * @author dhudson - created 25 Jul 2012
 * @since 2.0
 */
public class ToDoTaglet extends BaseTaglet {

    /**
     * Taglet name {@value}
     */
    private static final String TAG_NAME = "docfacto.todo";

    /**
     * @see com.sun.tools.doclets.internal.toolkit.taglets.Taglet#getName()
     */
    @Override
    public String getName() {
        return TAG_NAME;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#processInlineTag(java.lang.String,
     * com.sun.javadoc.Tag)
     */
    @Override
    public CharSequence processInlineTag(String text,Tag tag)
    throws DocfactoException {
        if (isEnabled()) {
            return ToDoTagletHelper.generateHTMLOutput(new TagParser(text));
        }

        return null;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#processBlockTag(com.sun.javadoc.Tag[],
     * com.sun.javadoc.Doc,
     * com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter)
     */
    @Override
    public CharSequence
    processBlockTag(Tag[] tag,Doc holder,TagletWriter writer)
    throws DocfactoException {
        return null;
    }

}
