/*
 * @author dhudson -
 * Created 26 Nov 2012 : 17:41:20
 */

package com.docfacto.taglets.dita;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import com.docfacto.common.DocfactoException;
import com.docfacto.dita.B;
import com.docfacto.dita.Entry;
import com.docfacto.dita.Row;
import com.docfacto.doclets.dita.DITADocletWriter;
import com.docfacto.doclets.dita.DITATagletOutput;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.Doc;
import com.sun.javadoc.ExecutableMemberDoc;
import com.sun.javadoc.MethodDoc;
import com.sun.javadoc.Tag;
import com.sun.javadoc.ThrowsTag;
import com.sun.javadoc.Type;
import com.sun.tools.doclets.internal.toolkit.taglets.InheritableTaglet;
import com.sun.tools.doclets.internal.toolkit.util.DocFinder;

/**
 * Re-write of the ThrowsTaglet to produce DITA output
 * 
 * @author dhudson - created 26 Nov 2012
 * @since 2.1
 */
public class DITAThrowsTaglet extends DITATaglet implements InheritableTaglet {

    /**
     * Create a new instance of <code>DITAThrowsTaglet</code>.
     */
    public DITAThrowsTaglet() {
    }

    /**
     * @see com.sun.tools.doclets.internal.toolkit.taglets.Taglet#getName()
     */
    @Override
    public String getName() {
        return "throws";
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#inField()
     */
    @Override
    public boolean inField() {
        return false;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#inOverview()
     */
    @Override
    public boolean inOverview() {
        return false;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#isInlineTag()
     */
    @Override
    public boolean isInlineTag() {
        return false;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#inPackage()
     */
    @Override
    public boolean inPackage() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void inherit(DocFinder.Input input,DocFinder.Output output) {
        ClassDoc exception;
        if (input.tagId==null) {
            final ThrowsTag throwsTag = (ThrowsTag)input.tag;
            exception = throwsTag.exception();
            input.tagId = exception==null ?
                throwsTag.exceptionName() :
                    throwsTag.exception().qualifiedName();
        }
        else {
            exception = input.method.containingClass().findClass(input.tagId);
        }

        final ThrowsTag[] tags = input.method.throwsTags();
        for (int i = 0;i<tags.length;i++) {
            if (input.tagId.equals(tags[i].exceptionName())||
            (tags[i].exception()!=null&&
            (input.tagId.equals(tags[i].exception().qualifiedName())))) {
                output.holder = input.method;
                output.holderTag = tags[i];
                output.inlineTags = input.isFirstSentence ?
                    tags[i].firstSentenceTags() : tags[i].inlineTags();
                    output.tagList.add(tags[i]);
            }
            else if (exception!=null&&tags[i].exception()!=null&&
            tags[i].exception().subclassOf(exception)) {
                output.tagList.add(tags[i]);
            }
        }
    }

    /**
     * Add links for exceptions that are declared but not documented.
     */
    private DITATagletOutput linkToUndocumentedDeclaredExceptions(
    Type[] declaredExceptionTypes,Set<String> alreadyDocumented,
    DITADocletWriter writer) {
        final DITATagletOutput result = new DITATagletOutput();

        // Add links to the exceptions declared but not documented.
        for (int i = 0;i<declaredExceptionTypes.length;i++) {
            if (declaredExceptionTypes[i].asClassDoc()!=null&&
            !alreadyDocumented.contains(
                declaredExceptionTypes[i].asClassDoc().name())&&
                !alreadyDocumented.contains(
                    declaredExceptionTypes[i].asClassDoc().qualifiedName())) {
                if (alreadyDocumented.size()==0) {
                    result.appendDITAElements(getThrowsHeader());
                }
                result.appendDITAElements(throwsTagOutput(
                    declaredExceptionTypes[i],writer));
                alreadyDocumented.add(declaredExceptionTypes[i].asClassDoc()
                    .name());
            }
        }
        return result;
    }

    /**
     * Inherit throws documentation for exceptions that were declared but not
     * documented.
     */
    private DITATagletOutput inheritThrowsDocumentation(Doc holder,
    Type[] declaredExceptionTypes,Set<String> alreadyDocumented,
    DITADocletWriter writer) {
        final DITATagletOutput result = new DITATagletOutput();
        if (holder instanceof MethodDoc) {
            final Set<ThrowsTag> declaredExceptionTags =
            new LinkedHashSet<ThrowsTag>();
            for (int j = 0;j<declaredExceptionTypes.length;j++) {
                DocFinder.Output inheritedDoc =
                DocFinder.search(new DocFinder.Input((MethodDoc)holder,
                    this,
                    declaredExceptionTypes[j].typeName()));
                if (inheritedDoc.tagList.size()==0) {
                    inheritedDoc = DocFinder.search(new DocFinder.Input(
                        (MethodDoc)holder,this,
                        declaredExceptionTypes[j].qualifiedTypeName()));
                }
                for (final Object tag:inheritedDoc.tagList) {
                    if (tag instanceof ThrowsTag) {
                        declaredExceptionTags.add((ThrowsTag)tag);
                    }
                }

            }
            result.appendDITAElements(throwsTagsOutput(
                declaredExceptionTags.toArray(new ThrowsTag[] {}),
                alreadyDocumented,false,writer));
        }
        return result;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABlockTagletOutput(com.sun.javadoc.Doc,
     * com.sun.javadoc.Tag, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABlockTagletOutput(Doc holder,Tag tag,
    DITADocletWriter writer) throws DocfactoException {
        final ExecutableMemberDoc execHolder = (ExecutableMemberDoc)holder;
        final ThrowsTag[] tags = execHolder.throwsTags();
        final DITATagletOutput result = new DITATagletOutput();

        final HashSet<String> alreadyDocumented = new HashSet<String>();
        if (tags.length>0) {
            final DITATagletOutput output = throwsTagsOutput(
                execHolder.throwsTags(),alreadyDocumented,true,writer);
            result.appendDITAElements(output);
        }

        result.appendDITAElements(inheritThrowsDocumentation(holder,
            execHolder.thrownExceptionTypes(),alreadyDocumented,writer));
        result.appendDITAElements(linkToUndocumentedDeclaredExceptions(
            execHolder.thrownExceptionTypes(),alreadyDocumented,writer));
        return result;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITAInLineTagletOutput(com.sun.javadoc.Tag,
     * com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITAInLineTagletOutput(Tag tag,
    DITADocletWriter writer) throws DocfactoException {
        return null;
    }

    /**
     * Given an array of <code>Tag</code>s representing this custom tag, return
     * its string representation.
     * 
     * @param throwTags the array of <code>ThrowsTag</code>s to convert.
     * @param writer the TagletWriter that will write this tag.
     * @param alreadyDocumented the set of exceptions that have already been
     * documented.
     * @param allowDups True if we allow duplicate throws tags to be documented.
     * @return the TagletOutput representation of this <code>Tag</code>.
     */
    protected DITATagletOutput throwsTagsOutput(ThrowsTag[] throwTags,
    Set<String> alreadyDocumented,boolean allowDups,
    final DITADocletWriter writer) {
        final DITATagletOutput result = new DITATagletOutput();
        if (throwTags.length>0) {
            for (int i = 0;i<throwTags.length;++i) {
                final ThrowsTag tt = throwTags[i];
                final ClassDoc cd = tt.exception();
                if ((!allowDups)&&
                (alreadyDocumented.contains(tt.exceptionName())||
                (cd!=null&&alreadyDocumented.contains(cd.qualifiedName())))) {
                    continue;
                }
                if (alreadyDocumented.size()==0) {
                    final DITATagletOutput output = getThrowsHeader();
                    final Row row = new Row();
                    output.populateElement(row);
                    result.appendDITAElement(row);
                }
                final DITATagletOutput output = throwsTagOutput(tt,writer);
                final Row row = new Row();
                output.populateElement(row);

                result.appendDITAElement(row);

                alreadyDocumented.add(cd!=null ?
                    cd.qualifiedName() : tt.exceptionName());
            }
        }
        return result;
    }

    /**
     * Get the Throws header
     * 
     * @return
     * @since 2.1
     */
    private DITATagletOutput getThrowsHeader() {
        final DITATagletOutput output = new DITATagletOutput();
        final Entry entry = new Entry();
        entry.addElement(new B("Throws:"));
        output.appendDITAElement(entry);
        return output;
    }

    /**
     * Generate throws tag output
     * 
     * @param throwsTag
     * @param writer
     * @return DITATagletOutput of the throws tag
     * @since 2.1
     */
    private DITATagletOutput throwsTagOutput(ThrowsTag throwsTag,
    DITADocletWriter writer) {
        final DITATagletOutput result = new DITATagletOutput();

        Entry entry = new Entry();
        // Empty entry
        result.appendDITAElement(entry);

        entry = new Entry();
        // entry.addElement(writer.getLink(throwsTag.exceptionType()));
        writer.getLink(throwsTag.exceptionType()).populateElement(entry);
        result.appendDITAElement(entry);

        entry = new Entry();
        final DITATagletOutput inline =
        writer.commentTagsToDITA(throwsTag,null,throwsTag.inlineTags(),
            false);

        if (inline!=null) {
            inline.populateElement(entry);
            result.appendDITAElement(entry);
        }
        // result.appendDITAElements(DITADocletController.INSTANCE.commentTagsToDITA(throwsTag,null,throwsTag.inlineTags(),false));

        // DocletConstants.NL + "<DD>";
        // result += throwsTag.exceptionType() == null ?
        // htmlWriter.codeText(throwsTag.exceptionName()) :
        // htmlWriter.codeText(
        // htmlWriter.getLink(new LinkInfoImpl(LinkInfoImpl.CONTEXT_MEMBER,
        // throwsTag.exceptionType())));
        // final TagletOutput text = new TagletOutputImpl(
        // htmlWriter.commentTagsToString(throwsTag, null,
        // throwsTag.inlineTags(), false));
        // if (text != null && text.toString().length() > 0) {
        // result += " - " + text;
        // }
        // return new TagletOutputImpl(result);

        return result;
    }
    
    /**
     * Generate the output for the throws tag
     * 
     * @param throwsType
     * @param writer
     * @return a new {@code DITATagletOutput} containing the throws information
     * @since 2.2
     */
    public DITATagletOutput throwsTagOutput(Type throwsType,
    DITADocletWriter writer) {
        final DITATagletOutput output = new DITATagletOutput();
        final Entry entry = new Entry();
        output.appendDITAElement(entry);

        writer.getLink(throwsType).populateElement(entry);
        return output;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABatchTagletOutput(com.sun.javadoc.Doc,
     * com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABatchTagletOutput(Doc holder,
    DITADocletWriter writer)
    throws DocfactoException {
        final ExecutableMemberDoc execHolder = (ExecutableMemberDoc)holder;
        final ThrowsTag[] tags = execHolder.throwsTags();
        final DITATagletOutput result = new DITATagletOutput();
        final HashSet<String> alreadyDocumented = new HashSet<String>();
        if (tags.length>0) {
            result.appendDITAElements(throwsTagsOutput(
                execHolder.throwsTags(),alreadyDocumented,true,writer));
        }
        result.appendDITAElements(inheritThrowsDocumentation(holder,
            execHolder.thrownExceptionTypes(),alreadyDocumented,writer));
        result.appendDITAElements(linkToUndocumentedDeclaredExceptions(
            execHolder.thrownExceptionTypes(),alreadyDocumented,writer));
        return result;
    }
}
