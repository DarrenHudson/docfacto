/*
 * @author dhudson -
 * Created 23 Nov 2012 : 15:11:10
 */

package com.docfacto.taglets.dita;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.docfacto.common.DocfactoException;
import com.docfacto.dita.B;
import com.docfacto.dita.Entry;
import com.docfacto.dita.ParmName;
import com.docfacto.dita.Row;
import com.docfacto.doclets.dita.DITADocletController;
import com.docfacto.doclets.dita.DITADocletWriter;
import com.docfacto.doclets.dita.DITATagletOutput;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.Doc;
import com.sun.javadoc.ExecutableMemberDoc;
import com.sun.javadoc.MethodDoc;
import com.sun.javadoc.ParamTag;
import com.sun.javadoc.Parameter;
import com.sun.javadoc.Tag;
import com.sun.javadoc.TypeVariable;
import com.sun.tools.doclets.internal.toolkit.taglets.InheritableTaglet;
import com.sun.tools.doclets.internal.toolkit.util.DocFinder;

/**
 * DITA version of the JavaDoc Param Taglet
 * 
 * @author dhudson - created 23 Nov 2012
 * @since 2.1
 */
public class DITAParamTaglet extends DITATaglet implements InheritableTaglet {

    /**
     * Create a new instance of <code>DITAParamTaglet</code>.
     */
    public DITAParamTaglet() {
    }

    @Override
    public String getName() {
        return "param";
    }

    /**
     * Given an array of <code>Parameter</code>s, return a name/rank number map.
     * If the array is null, then null is returned.
     * 
     * @param params The array of parameters (from type or executable member) to
     * check.
     * @return a name-rank number map.
     */
    private static Map<String,String> getRankMap(Object[] params) {
        if (params==null) {
            return null;
        }
        final HashMap<String,String> result = new HashMap<String,String>();
        for (int i = 0;i<params.length;i++) {
            final String name = params[i] instanceof Parameter ?
                ((Parameter)params[i]).name() :
                    ((TypeVariable)params[i]).typeName();
                result.put(name,String.valueOf(i));
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void inherit(DocFinder.Input input,DocFinder.Output output) {
        if (input.tagId==null) {
            input.isTypeVariableParamTag =
            ((ParamTag)input.tag).isTypeParameter();
            final Object[] parameters = input.isTypeVariableParamTag ?
                (Object[])((MethodDoc)input.tag.holder()).typeParameters() :
                    (Object[])((MethodDoc)input.tag.holder()).parameters();
                final String target = ((ParamTag)input.tag).parameterName();
                int i;
                for (i = 0;i<parameters.length;i++) {
                    final String name = parameters[i] instanceof Parameter ?
                        ((Parameter)parameters[i]).name() :
                            ((TypeVariable)parameters[i]).typeName();
                        if (name.equals(target)) {
                            input.tagId = String.valueOf(i);
                            break;
                        }
                }
                if (i==parameters.length) {
                    // Someone used {@inheritDoc} on an invalid @param tag.
                    // We don't know where to inherit from.
                    // XXX: in the future when Configuration is available here,
                    // print a warning for this mistake.
                    return;
                }
        }

        final ParamTag[] tags = input.isTypeVariableParamTag ?
            input.method.typeParamTags() : input.method.paramTags();
            final Map<String,String> rankMap =
            getRankMap(input.isTypeVariableParamTag ?
                (Object[])input.method.typeParameters() :
                    (Object[])input.method.parameters());
            for (int i = 0;i<tags.length;i++) {
                if (rankMap.containsKey(tags[i].parameterName())&&
                rankMap.get(tags[i].parameterName()).equals((input.tagId))) {
                    output.holder = input.method;
                    output.holderTag = tags[i];
                    output.inlineTags = input.isFirstSentence ?
                        tags[i].firstSentenceTags() : tags[i].inlineTags();
                        return;
                }
            }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean inField() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean inOverview() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean inPackage() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isInlineTag() {
        return false;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABlockTagletOutput(com.sun.javadoc.Doc,
     * com.sun.javadoc.Tag, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABlockTagletOutput(Doc holder,Tag tag,
    DITADocletWriter writer) throws DocfactoException {

        if (holder instanceof ExecutableMemberDoc) {
            final ExecutableMemberDoc member = (ExecutableMemberDoc)holder;
            final DITATagletOutput output =
            getTagletOutput(false,member,
                member.typeParameters(),member.typeParamTags(),writer);

            output.appendDITAElements(getTagletOutput(true,member,
                member.parameters(),member.paramTags(),writer));
            return output;
        }
        else {
            final ClassDoc classDoc = (ClassDoc)holder;
            return getTagletOutput(false,classDoc,
                classDoc.typeParameters(),classDoc.typeParamTags(),writer);
        }
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITAInLineTagletOutput(com.sun.javadoc.Tag,
     * com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITAInLineTagletOutput(Tag tag,
    DITADocletWriter writer) throws DocfactoException {
        return null;
    }

    /**
     * Given an array of <code>ParamTag</code>s,return its string
     * representation. Try to inherit the param tags that are missing.
     * 
     * @param doc the doc that holds the param tags.
     * @param writer the TagletWriter that will write this tag.
     * @param formalParameters The array of parameters (from type or executable
     * member) to check.
     * 
     * @return the TagletOutput representation of these <code>ParamTag</code>s.
     */
    private DITATagletOutput getTagletOutput(boolean isNonTypeParams,
    Doc holder,Object[] formalParameters,ParamTag[] paramTags,
    DITADocletWriter writer) {
        final DITATagletOutput result =
        new DITATagletOutput();
        final Set<String> alreadyDocumented = new HashSet<String>();
        if (paramTags.length>0) {
            result.appendDITAElements(
                processParamTags(isNonTypeParams,paramTags,
                    getRankMap(formalParameters),alreadyDocumented,writer)
            );
        }
        if (alreadyDocumented.size()!=formalParameters.length) {
            // Some parameters are missing corresponding @param tags.
            // Try to inherit them.
            result.appendDITAElements(getInheritedTagletOutput(isNonTypeParams,
                holder,
                formalParameters,alreadyDocumented,writer));
        }
        return result;
    }

    /**
     * Loop through each individual parameter. It it does not have a
     * corresponding param tag, try to inherit it.
     */
    private DITATagletOutput getInheritedTagletOutput(boolean isNonTypeParams,
    Doc holder,Object[] formalParameters,
    Set<String> alreadyDocumented,DITADocletWriter writer) {
        final DITATagletOutput result = new DITATagletOutput();

        if ((!alreadyDocumented.contains(null))&&
        holder instanceof MethodDoc) {
            for (int i = 0;i<formalParameters.length;i++) {
                if (alreadyDocumented.contains(String.valueOf(i))) {
                    continue;
                }
                // This parameter does not have any @param documentation.
                // Try to inherit it.
                final DocFinder.Output inheritedDoc =
                DocFinder.search(new DocFinder.Input((MethodDoc)holder,
                    this,
                    String.valueOf(i),!isNonTypeParams));
                if (inheritedDoc.inlineTags!=null&&
                inheritedDoc.inlineTags.length>0) {
                    result.appendDITAElements(
                        processParamTag(isNonTypeParams,
                            (ParamTag)inheritedDoc.holderTag,
                            isNonTypeParams ?
                                ((Parameter)formalParameters[i]).name() :
                                    ((TypeVariable)formalParameters[i]).typeName(),
                                    alreadyDocumented.size()==0,writer));
                }
                alreadyDocumented.add(String.valueOf(i));
            }
        }
        return result;
    }

    /**
     * Given an array of <code>Tag</code>s representing this custom tag, return
     * its string representation. Print a warning for param tags that do not map
     * to parameters. Print a warning for param tags that are duplicated.
     * 
     * @param paramTags the array of <code>ParamTag</code>s to convert.
     * @param writer the TagletWriter that will write this tag.
     * @param alreadyDocumented the set of exceptions that have already been
     * documented.
     * @param rankMap a {@link java.util.Map} which holds ordering information
     * about the parameters.
     * @param nameMap a {@link java.util.Map} which holds a mapping of a rank of
     * a parameter to its name. This is used to ensure that the right name is
     * used when parameter documentation is inherited.
     * @return the TagletOutput representation of this <code>Tag</code>.
     */
    private DITATagletOutput processParamTags(boolean isNonTypeParams,
    ParamTag[] paramTags,Map rankMap,
    Set<String> alreadyDocumented,DITADocletWriter writer) {
        final DITATagletOutput result =
        new DITATagletOutput();

        if (paramTags.length>0) {
            for (int i = 0;i<paramTags.length;++i) {
                final ParamTag pt = paramTags[i];
                final String paramName = isNonTypeParams ?
                    pt.parameterName() : "<"+pt.parameterName()+">";
                    if (!rankMap.containsKey(pt.parameterName())) {
                        if (isNonTypeParams) {
                            DITADocletController.INSTANCE.printWarningMessage(
                                pt.position(),"@param argument "+paramName+
                            " is not a parameter name.");
                        }
                        else {
                            DITADocletController.INSTANCE.printWarningMessage(
                                pt.position(),"@param argument "+paramName+
                            " is not a type parameter name.");
                        }
                    }
                    final String rank = (String)rankMap.get(pt.parameterName());
                    if (rank!=null&&alreadyDocumented.contains(rank)) {
                        if (isNonTypeParams) {
                            // Parameter "{0}" is documented more than once.
                            DITADocletController.INSTANCE.printWarningMessage(
                                pt.position(),"Parameter "+paramName+
                            " is documented more than once.");
                        }
                        else {
                            DITADocletController.INSTANCE.printWarningMessage(
                                pt.position(),"Type parameter "+paramName+
                            " is documented more than once.");
                        }
                    }
                    result.appendDITAElements(processParamTag(isNonTypeParams,
                        pt,
                        pt.parameterName(),alreadyDocumented.size()==0,writer));
                    alreadyDocumented.add(rank);
            }
        }
        return result;
    }

    /**
     * Convert the individual ParamTag into TagletOutput.
     * 
     * @param isNonTypeParams true if this is just a regular param tag. False if
     * this is a type param tag.
     * @param writer the taglet writer for output writing.
     * @param paramTag the tag whose inline tags will be printed.
     * @param name the name of the parameter. We can't rely on the name in the
     * param tag because we might be inheriting documentation.
     * @param isFirstParam true if this is the first param tag being printed.
     * 
     */
    private DITATagletOutput processParamTag(boolean isNonTypeParams,
    ParamTag paramTag,String name,
    boolean isFirstParam,DITADocletWriter writer) {
        final DITATagletOutput result = new DITATagletOutput();

        String header;
        if (isNonTypeParams) {
            header = "Parameters:";
        }
        else {
            header = "Type Parameters:";
        }

        if (isFirstParam) {
            // Need to add the parameter row..
            final Row row = new Row();
            final DITATagletOutput output =
            getParamHeader(header);
            output.populateElement(row);
            result.appendDITAElement(row);
        }

        final DITATagletOutput output =
        paramTagOutput(paramTag,name,writer);
        final Row row = new Row();
        row.addEntry();
        output.populateElement(row);
        result.appendDITAElement(row);

        return result;
    }

    /**
     * Produce the header for the Parameters
     * 
     * @param header to use
     * @return the header
     * @since 2.1
     */
    public DITATagletOutput getParamHeader(String header) {
        final Entry entry = new Entry();
        entry.addElement(new B(header));

        final DITATagletOutput output = new DITATagletOutput();
        output.appendDITAElement(entry);
        return output;
    }

    /**
     * Produce the DITA entry for the parameter
     * 
     * @param paramTag the tag
     * @param paramName of the parameter
     * @param writer the DocletWriter
     * @return the DITA elements that represent the param tag, with all inline
     * taglets subsituted
     * @since 2.1
     */
    private DITATagletOutput paramTagOutput(ParamTag paramTag,String paramName,
    DITADocletWriter writer) {
        final DITATagletOutput output = new DITATagletOutput();

        // Add the parameter name
        final Entry entry = new Entry();
        entry.addElement(new ParmName(paramName));
        output.appendDITAElement(entry);

        final Entry description = new Entry();
        final DITATagletOutput inline = writer.commentTagsToDITA(
            paramTag,null,paramTag.inlineTags(),false);

        if (inline!=null) {
            inline.populateElement(description);
            output.appendDITAElement(description);
        }

        return output;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABatchTagletOutput(com.sun.javadoc.Doc,
     * com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABatchTagletOutput(Doc holder,
    DITADocletWriter writer)
    throws DocfactoException {
        if (holder instanceof ExecutableMemberDoc) {
            final ExecutableMemberDoc member = (ExecutableMemberDoc)holder;
            final DITATagletOutput output =
            getTagletOutput(false,member,
                member.typeParameters(),member.typeParamTags(),writer);
            output.appendDITAElements(getTagletOutput(true,member,
                member.parameters(),member.paramTags(),writer));
            return output;
        }
        else {
            final ClassDoc classDoc = (ClassDoc)holder;
            return getTagletOutput(false,classDoc,
                classDoc.typeParameters(),classDoc.typeParamTags(),writer);
        }
    }
}
