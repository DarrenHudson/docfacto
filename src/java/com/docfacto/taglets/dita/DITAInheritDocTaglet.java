/*
 * @author dhudson -
 * Created 3 Dec 2012 : 12:22:12
 */

package com.docfacto.taglets.dita;

import com.docfacto.common.DocfactoException;
import com.docfacto.doclets.dita.DITADocletController;
import com.docfacto.doclets.dita.DITADocletWriter;
import com.docfacto.doclets.dita.DITATagletOutput;
import com.sun.javadoc.Doc;
import com.sun.javadoc.MethodDoc;
import com.sun.javadoc.Tag;
import com.sun.tools.doclets.internal.toolkit.taglets.InheritableTaglet;
import com.sun.tools.doclets.internal.toolkit.taglets.Taglet;
import com.sun.tools.doclets.internal.toolkit.util.DocFinder;

/**
 * DITA version of the JavaDoc inline inheritDoc tag
 * 
 * @author dhudson - created 3 Dec 2012
 * @since 2.1
 */
public class DITAInheritDocTaglet extends DITATaglet {

    /**
     * The inline tag that would appear in the documentation if
     * the writer wanted documentation to be inherited. {@value}
     */
    public static final String INHERIT_DOC_INLINE_TAG = "{@inheritDoc}";

    /**
     * Will return false because this inline tag may only appear in Methods.
     * 
     * @return false since this is not a method.
     */
    @Override
    public boolean inField() {
        return false;
    }

    /**
     * Will return false because this inline tag may only appear in Methods.
     * 
     * @return false since this is not a method.
     */
    @Override
    public boolean inConstructor() {
        return false;
    }

    /**
     * Will return false because this inline tag may only appear in Methods.
     * 
     * @return false since this is not a method.
     */
    @Override
    public boolean inOverview() {
        return false;
    }

    /**
     * Will return false because this inline tag may only appear in Methods.
     * 
     * @return false since this is not a method.
     */
    @Override
    public boolean inPackage() {
        return false;
    }

    /**
     * Will return false because this inline tag may only appear in Methods.
     * 
     * @return false since this is not a method.
     */
    @Override
    public boolean inType() {
        return false;
    }

    /**
     * Given a <code>MethodDoc</code> item, a <code>Tag</code> in the
     * <code>MethodDoc</code> item and a String, replace all occurrences of @inheritDoc
     * with documentation from it's superclass or superinterface.
     * 
     * @param md the {@link MethodDoc} that we are documenting.
     * @param holderTag the tag that holds the inheritDoc tag.
     * @param isFirstSentence true if we only want to inherit the first
     * sentence.
     */
    private DITATagletOutput retrieveInheritedDocumentation(
    MethodDoc md,Tag holderTag,boolean isFirstSentence,DITADocletWriter writer) {

        DITATagletOutput replacement = new DITATagletOutput();

        final Taglet inheritableTaglet =
        holderTag==null ?
            null : DITADocletController.INSTANCE.getTagletManager()
            .getTaglet(holderTag.name());

        if (inheritableTaglet!=null&&
        !(inheritableTaglet instanceof InheritableTaglet)) {
            // This tag does not support inheritance.
            DITADocletController.INSTANCE.printWarningMessage(md.position(),
                "@inheritDoc used but"+md.name()+md.flatSignature()+
            " does not override or implement any method.");
        }
        final DocFinder.Output inheritedDoc =
        DocFinder.search(new DocFinder.Input(md,
            (InheritableTaglet)inheritableTaglet,holderTag,
            isFirstSentence,true));
        if (inheritedDoc.isValidInheritDocTag==false) {
            DITADocletController.INSTANCE.printWarningMessage(md.position(),
                "@inheritDoc used "+md.name()+md.flatSignature()+
            " but does not override or implement any method.");
        }
        else if (inheritedDoc.inlineTags.length>0) {
            replacement =
            writer
            .commentTagsToDITA(inheritedDoc.holderTag,
                inheritedDoc.holder,inheritedDoc.inlineTags,
                isFirstSentence);
        }
        return replacement;
    }

    /**
     * @see com.sun.tools.doclets.internal.toolkit.taglets.Taglet#getName()
     */
    @Override
    public String getName() {
        return "inheritDoc";
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABlockTagletOutput(com.sun.javadoc.Doc, com.sun.javadoc.Tag, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABlockTagletOutput(Doc holder,Tag tag,DITADocletWriter writer)
    throws DocfactoException {
        return null;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITAInLineTagletOutput(com.sun.javadoc.Tag, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITAInLineTagletOutput(Tag tag,DITADocletWriter writer)
    throws DocfactoException {
        if (!(tag.holder() instanceof MethodDoc)) {
            return null;
        }
        return tag.name().equals("@inheritDoc") ?
            retrieveInheritedDocumentation((MethodDoc)tag.holder(),null,false,writer) :
                retrieveInheritedDocumentation((MethodDoc)tag.holder(),tag,false,writer);
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABatchTagletOutput(com.sun.javadoc.Doc, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABatchTagletOutput(Doc holder,DITADocletWriter writer)
    throws DocfactoException {
        return null;
    }

}
