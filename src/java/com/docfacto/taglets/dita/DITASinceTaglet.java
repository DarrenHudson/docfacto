package com.docfacto.taglets.dita;

import com.docfacto.common.DocfactoException;
import com.docfacto.config.generated.TagletDefinition;
import com.docfacto.dita.B;
import com.docfacto.dita.CodePh;
import com.docfacto.dita.Entry;
import com.docfacto.dita.Row;
import com.docfacto.doclets.dita.DITADocletWriter;
import com.docfacto.doclets.dita.DITATagletOutput;
import com.docfacto.xml.XMLTextElement;
import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;

/**
 * DITA version of the since taglet
 * 
 * @author dhudson - created 23 Mar 2013
 * @since 2.2
 */
public class DITASinceTaglet extends DITATaglet {

    /**
     * 
     * The string to match for highlighting
     */
    private String theHighlightString;

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getName()
     */
    @Override
    public String getName() {
        return "since";
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#inField()
     */
    @Override
    public boolean inField() {
        return true;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#inConstructor()
     */
    @Override
    public boolean inConstructor() {
        return true;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#inMethod()
     */
    @Override
    public boolean inMethod() {
        return true;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#inOverview()
     */
    @Override
    public boolean inOverview() {
        return true;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#isInlineTag()
     */
    @Override
    public boolean isInlineTag() {
        return false;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#inPackage()
     */
    @Override
    public boolean inPackage() {
        return true;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#inType()
     */
    @Override
    public boolean inType() {
        return true;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#setTagletDefinition(com.docfacto.config.generated.TagletDefinition)
     */
    @Override
    public void setTagletDefinition(TagletDefinition tagletDefinition) {
        super.setTagletDefinition(tagletDefinition);
        theHighlightString = tagletDefinition.getParam();
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITAInLineTagletOutput(com.sun.javadoc.Tag,
     * com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITAInLineTagletOutput(Tag tag,
    DITADocletWriter writer) throws DocfactoException {
        return null;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABlockTagletOutput(com.sun.javadoc.Doc,
     * com.sun.javadoc.Tag, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABlockTagletOutput(Doc holder,Tag tag,
    DITADocletWriter writer) throws DocfactoException {
        final DITATagletOutput output = new DITATagletOutput();
        Row row = getHeaderRow();
        output.appendDITAElement(row);

        row = new Row();
        // blank row..
        row.addEntry();

        final Entry entry = row.addEntry();

        if (isEnabled()) {
            // Lets check for highlighting
            if (isHightlighted(tag.text())) {
                CodePh codeph = new CodePh(tag.text());
                entry.addElement(codeph);
            }
            else {
                entry.addTextElement(tag.text());
            }
        }
        else {
            entry.addTextElement(tag.text());
        }

        output.appendDITAElement(row);
        return output;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABatchTagletOutput(com.sun.javadoc.Doc, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABatchTagletOutput(Doc holder,
    DITADocletWriter writer) throws DocfactoException {
        final DITATagletOutput output = new DITATagletOutput();
        Row row = getHeaderRow();
        output.appendDITAElement(row);

        row = new Row();
        // blank row..
        row.addEntry();

        final Entry entry = row.addEntry();

        Tag[] tags = holder.tags(getName());

        for (int i = 0;i<tags.length;i++) {
            if (i>0) {
                entry.addElement(new XMLTextElement(", "));
            }

            if (isEnabled()) {
                // Lets check for highlighting
                if (isHightlighted(tags[i].text())) {
                    CodePh codeph = new CodePh(tags[i].text());
                    entry.addElement(codeph);
                }
                else {
                    entry.addTextElement(tags[i].text());
                }
            }
            else {
                entry.addTextElement(tags[i].text());
            }
        }
        return output;
    }

    /**
     * See if the text matches the parameter supplied in Docfacto.xml
     * 
     * @param text to check
     * @return true if this tag is to be highlighted
     * @since 2.3
     */
    private boolean isHightlighted(String text) {
        return theHighlightString.equals(text);
    }

    /**
     * Get the header for this taglet, should end in :
     * 
     * @param header
     * @since 2.3
     */
    private Row getHeaderRow() {
        final Row row = new Row();
        final Entry entry = row.addEntry();
        entry.addElement(new B("Since :"));

        return row;
    }
}
