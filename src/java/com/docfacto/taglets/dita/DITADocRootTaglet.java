/*
 * @author dhudson -
 * Created 3 Dec 2012 : 12:43:46
 */

package com.docfacto.taglets.dita;

import com.docfacto.common.DocfactoException;
import com.docfacto.doclets.dita.DITADocletWriter;
import com.docfacto.doclets.dita.DITATagletOutput;
import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;

/**
 * DITA version of the DocRoot JavaDoc Taglet
 * 
 * @author dhudson - created 3 Dec 2012
 * @since 2.1
 */
public class DITADocRootTaglet extends DITATaglet {

    /**
     * @see com.sun.tools.doclets.internal.toolkit.taglets.Taglet#getName()
     */
    @Override
    public String getName() {
        return "docRoot";
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABlockTagletOutput(com.sun.javadoc.Doc,
     * com.sun.javadoc.Tag, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABlockTagletOutput(Doc holder,Tag tag,
    DITADocletWriter writer)
    throws DocfactoException {
        return null;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITAInLineTagletOutput(com.sun.javadoc.Tag,
     * com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITAInLineTagletOutput(Tag tag,
    DITADocletWriter writer)
    throws DocfactoException {
        final DITATagletOutput output = new DITATagletOutput();
        output.appendTextElement(writer.getDocRootOutput());
        return output;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABatchTagletOutput(com.sun.javadoc.Doc,
     * com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABatchTagletOutput(Doc holder,
    DITADocletWriter writer)
    throws DocfactoException {
        return null;
    }

}
