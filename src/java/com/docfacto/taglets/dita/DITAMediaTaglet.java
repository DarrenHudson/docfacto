package com.docfacto.taglets.dita;

import com.docfacto.common.DocfactoException;
import com.docfacto.dita.Alt;
import com.docfacto.dita.Fig;
import com.docfacto.dita.Image;
import com.docfacto.dita.Title;
import com.docfacto.doclets.dita.DITADocletWriter;
import com.docfacto.doclets.dita.DITATagletOutput;
import com.docfacto.javadoc.TagParser;
import com.docfacto.taglets.MediaTagletHelper;
import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;

/**
 * DITA Media taglet
 * 
 * @author dhudson - created 27 Mar 2013
 * @since 2.2
 */
public class DITAMediaTaglet extends DITATaglet {

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABlockTagletOutput(com.sun.javadoc.Doc,
     * com.sun.javadoc.Tag, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABlockTagletOutput(Doc holder,Tag tag,
    DITADocletWriter writer) throws DocfactoException {
        return null;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITAInLineTagletOutput(com.sun.javadoc.Tag,
     * com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITAInLineTagletOutput(Tag tag,
    DITADocletWriter writer) throws DocfactoException {
        if (isEnabled()) {
            final DITATagletOutput output = new DITATagletOutput();
            TagParser parser = new TagParser(tag.text());

            Image image = new Image();

            // If the image has a title, then make it a figure
            if (parser.hasAttribute(TagParser.TITLE_ATTRIBUTE)) {
                Fig fig = new Fig();
                fig.addElement(new Title(parser
                    .getAttributeValue(TagParser.TITLE_ATTRIBUTE)));
                fig.addElement(image);
                output.appendDITAElement(fig);
            }
            else {
                output.appendDITAElement(image);
            }

            // Does it have an alt
            if (parser.hasAttribute(TagParser.ALT_ATTRIBUTE)) {
                Alt alt =
                    new Alt(parser.getAttributeValue(TagParser.ALT_ATTRIBUTE));
                image.addElement(alt);
            }

            image.setHref(parser.getAttributeValue(TagParser.URI_ATTRIBUTE));

            // Are there any optional attributes?
            addAttributeIfExists(image,MediaTagletHelper.HEIGHT_ATTRIBUTE,
                parser);
            addAttributeIfExists(image,MediaTagletHelper.WIDTH_ATTRIBUTE,parser);
            addAttributeIfExists(image,MediaTagletHelper.ALIGN_ATTRIBUTE,parser);
            addAttributeIfExists(image,MediaTagletHelper.SCALE_ATTRIBUTE,parser);
            addAttributeIfExists(image,MediaTagletHelper.PLACEMENT_ATTRIBUTE,
                parser);

            return output;
        }

        return null;
    }

    private void addAttributeIfExists(Image img,String attribute,
    TagParser parser) {
        if (parser.hasAttribute(attribute)) {
            img.addAttribute(attribute,parser.getAttributeValue(attribute));
        }
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABatchTagletOutput(com.sun.javadoc.Doc,
     * com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABatchTagletOutput(Doc holder,
    DITADocletWriter writer) throws DocfactoException {
        return null;
    }

}
