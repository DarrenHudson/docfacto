/*
 * @author dhudson -
 * Created 3 Dec 2012 : 11:31:15
 */

package com.docfacto.taglets.dita;

import java.util.StringTokenizer;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.StringUtils;
import com.docfacto.doclets.dita.DITADocletController;
import com.docfacto.doclets.dita.DITADocletWriter;
import com.docfacto.doclets.dita.DITATagletOutput;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.Doc;
import com.sun.javadoc.FieldDoc;
import com.sun.javadoc.MemberDoc;
import com.sun.javadoc.Tag;
import com.sun.tools.doclets.internal.toolkit.util.DocletAbortException;

/**
 * DITA version of the {@literal @value } inline taglet
 * 
 * @author dhudson - created 3 Dec 2012
 * @since 2.1
 */
public class DITAValueTaglet extends DITATaglet {

    /**
     * @see com.sun.tools.doclets.internal.toolkit.taglets.Taglet#getName()
     */
    @Override
    public String getName() {
        return "value";
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#inConstructor()
     */
    @Override
    public boolean inConstructor() {
        return false;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#inMethod()
     */
    @Override
    public boolean inMethod() {
        return false;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#inOverview()
     */
    @Override
    public boolean inOverview() {
        return false;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#inPackage()
     */
    @Override
    public boolean inPackage() {
        return false;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#inType()
     */
    @Override
    public boolean inType() {
        return false;
    }

    /**
     * Given the name of the field, return the corresponding FieldDoc.
     * 
     * @param config the current configuration of the doclet.
     * @param tag the value tag.
     * @param name the name of the field to search for. The name should be in
     * <qualified class name>#<field name> format. If the class name is omitted,
     * it is assumed that the field is in the current class.
     * 
     * @return the corresponding FieldDoc. If the name is null or empty string,
     * return field that the value tag was used in.
     * 
     * @throws DocletAbortException if the value tag does not specify a name to
     * a value field and it is not used within the comments of a valid field.
     */
    private FieldDoc getFieldDoc(Tag tag,String name) {
        if (name==null||name.length()==0) {
            // Base case: no label.
            if (tag.holder() instanceof FieldDoc) {
                return (FieldDoc)tag.holder();
            }
            else {
                // This should never ever happen.
                throw new DocletAbortException();
            }
        }
        final StringTokenizer st = new StringTokenizer(name,"#");
        String memberName = null;
        ClassDoc cd = null;
        if (st.countTokens()==1) {
            // Case 2: @value in same class.
            final Doc holder = tag.holder();
            if (holder instanceof MemberDoc) {
                cd = ((MemberDoc)holder).containingClass();
            }
            else if (holder instanceof ClassDoc) {
                cd = (ClassDoc)holder;
            }
            memberName = st.nextToken();
        }
        else {
            // Case 3: @value in different class.
            cd =
            DITADocletController.INSTANCE.getConfig().getRootDoc()
            .classNamed(st.nextToken());
            memberName = st.nextToken();
        }
        if (cd==null) {
            return null;
        }
        final FieldDoc[] fields = cd.fields();
        for (int i = 0;i<fields.length;i++) {
            if (fields[i].name().equals(memberName)) {
                return fields[i];
            }
        }
        return null;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABlockTagletOutput(com.sun.javadoc.Doc,
     * com.sun.javadoc.Tag, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABlockTagletOutput(Doc holder,Tag tag,
    DITADocletWriter writer)
    throws DocfactoException {
        return null;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITAInLineTagletOutput(com.sun.javadoc.Tag,
     * com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITAInLineTagletOutput(Tag tag,
    DITADocletWriter writer)
    throws DocfactoException {
        final FieldDoc field = getFieldDoc(tag,tag.text());
        if (field==null) {
            // Reference is unknown.
            DITADocletController.INSTANCE.printWarningMessage(tag.holder()
                .position(),tag.text()+
            " (referenced by @value tag) is an unknown reference");
        }
        else if (field.constantValue()!=null) {
            final DITATagletOutput output = new DITATagletOutput();
            output.appendTextElement(StringUtils.escapeXML(field
                .constantValueExpression()));
            return output;
        }
        else {
            // Referenced field is not a constant.
            DITADocletController.INSTANCE.printWarningMessage(tag.holder()
                .position(),"@value tag (which references "+field.name()+
            " can only be used in constants.");
        }
        return null;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABatchTagletOutput(com.sun.javadoc.Doc,
     * com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABatchTagletOutput(Doc holder,
    DITADocletWriter writer)
    throws DocfactoException {
        return null;
    }
}
