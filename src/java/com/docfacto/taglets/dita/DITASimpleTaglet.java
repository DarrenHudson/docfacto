/*
 * @author dhudson -
 * Created 29 Nov 2012 : 16:34:01
 */

package com.docfacto.taglets.dita;

import com.docfacto.common.DocfactoException;
import com.docfacto.dita.B;
import com.docfacto.dita.Entry;
import com.docfacto.dita.Row;
import com.docfacto.doclets.dita.DITADocletWriter;
import com.docfacto.doclets.dita.DITATagletOutput;
import com.docfacto.xml.XMLTextElement;
import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;

/**
 * Simple Taglet are block tags, and if there are many of them, they are
 * delimited by a comma
 * 
 * @author dhudson - created 29 Nov 2012
 * @since 2.1
 */
public class DITASimpleTaglet extends DITATaglet {
    /**
     * The marker in the location string for excluded tags.
     */
    public static final String EXCLUDED = "x";

    /**
     * The marker in the location string for packages.
     */
    public static final String PACKAGE = "p";

    /**
     * The marker in the location string for types.
     */
    public static final String TYPE = "t";

    /**
     * The marker in the location string for constructors.
     */
    public static final String CONSTRUCTOR = "c";

    /**
     * The marker in the location string for fields.
     */
    public static final String FIELD = "f";

    /**
     * The marker in the location string for methods.
     */
    public static final String METHOD = "m";

    /**
     * The marker in the location string for overview.
     */
    public static final String OVERVIEW = "o";

    /**
     * Use in location string when the tag is to appear in all locations.
     */
    public static final String ALL = "a";

    /**
     * The name of this tag.
     */
    protected String tagName;

    /**
     * The header to output.
     */
    protected String header;

    /**
     * The possible locations that this tag can appear in.
     */
    protected String locations;

    /**
     * Construct a <code>SimpleTaglet</code>.
     * 
     * @param tagName the name of this tag
     * @param header the header to output.
     * @param locations the possible locations that this tag can appear in. The
     * <code>String</code> can contain 'p' for package, 't' for type, 'm' for
     * method, 'c' for constructor and 'f' for field.
     */
    public DITASimpleTaglet(String tagName,String header,String locations) {
        this.tagName = tagName;
        this.header = header;
        locations = locations.toLowerCase();
        if (locations.indexOf(ALL)!=-1&&locations.indexOf(EXCLUDED)==-1) {
            this.locations = PACKAGE+TYPE+FIELD+METHOD+CONSTRUCTOR+OVERVIEW;
        }
        else {
            this.locations = locations;
        }
    }

    /**
     * @see com.sun.tools.doclets.internal.toolkit.taglets.Taglet#getName()
     */
    @Override
    public String getName() {
        return tagName;
    }

    /**
     * Return true if this <code>SimpleTaglet</code> is used in constructor
     * documentation.
     * 
     * @return true if this <code>SimpleTaglet</code> is used in constructor
     * documentation and false otherwise.
     */
    @Override
    public boolean inConstructor() {
        return locations.indexOf(CONSTRUCTOR)!=-1&&
        locations.indexOf(EXCLUDED)==-1;
    }

    /**
     * Return true if this <code>SimpleTaglet</code> is used in field
     * documentation.
     * 
     * @return true if this <code>SimpleTaglet</code> is used in field
     * documentation and false otherwise.
     */
    @Override
    public boolean inField() {
        return locations.indexOf(FIELD)!=-1&&locations.indexOf(EXCLUDED)==-1;
    }

    /**
     * Return true if this <code>SimpleTaglet</code> is used in method
     * documentation.
     * 
     * @return true if this <code>SimpleTaglet</code> is used in method
     * documentation and false otherwise.
     */
    @Override
    public boolean inMethod() {
        return locations.indexOf(METHOD)!=-1&&locations.indexOf(EXCLUDED)==-1;
    }

    /**
     * Return true if this <code>SimpleTaglet</code> is used in overview
     * documentation.
     * 
     * @return true if this <code>SimpleTaglet</code> is used in overview
     * documentation and false otherwise.
     */
    @Override
    public boolean inOverview() {
        return locations.indexOf(OVERVIEW)!=-1&&locations.indexOf(EXCLUDED)==-1;
    }

    /**
     * Return true if this <code>SimpleTaglet</code> is used in package
     * documentation.
     * 
     * @return true if this <code>SimpleTaglet</code> is used in package
     * documentation and false otherwise.
     */
    @Override
    public boolean inPackage() {
        return locations.indexOf(PACKAGE)!=-1&&locations.indexOf(EXCLUDED)==-1;
    }

    /**
     * Return true if this <code>SimpleTaglet</code> is used in type
     * documentation (classes or interfaces).
     * 
     * @return true if this <code>SimpleTaglet</code> is used in type
     * documentation and false otherwise.
     */
    @Override
    public boolean inType() {
        return locations.indexOf(TYPE)!=-1&&locations.indexOf(EXCLUDED)==-1;
    }

    /**
     * Return true if this <code>Taglet</code> is an inline tag.
     * 
     * @return true if this <code>Taglet</code> is an inline tag and false
     * otherwise.
     */
    @Override
    public boolean isInlineTag() {
        return false;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABlockTagletOutput(com.sun.javadoc.Doc, com.sun.javadoc.Tag, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABlockTagletOutput(Doc holder,Tag tag,DITADocletWriter writer)
    throws DocfactoException {
        return simpleTagOutput(tag,header,writer);
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITAInLineTagletOutput(com.sun.javadoc.Tag, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITAInLineTagletOutput(Tag tag,DITADocletWriter writer)
    throws DocfactoException {
        // Simple Taglets are block only..
        return null;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABatchTagletOutput(com.sun.javadoc.Doc, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABatchTagletOutput(Doc holder,DITADocletWriter writer)
    throws DocfactoException {
        return simpleTagOutput(holder.tags(getName()),header,writer);
    }

    /**
     * Produce the output for a collection of simple tags
     *
     * @param simpleTags the tags to process
     * @param header of the taglet
     * @return the DITA element for this simple taglet
     * @since 2.1
     */
    public DITATagletOutput simpleTagOutput(Tag[] simpleTags,String header,DITADocletWriter writer) {

        final DITATagletOutput output = new DITATagletOutput();
        Row row = getHeaderRow(header);
        output.appendDITAElement(row);

        row = new Row();
        // blank row..
        row.addEntry();

        final Entry entry = row.addEntry();

        for (int i = 0;i<simpleTags.length;i++) {
            if (i>0) {
                entry.addElement(new XMLTextElement(", "));
            }

            final DITATagletOutput inline =
            writer.commentTagsToDITA(
                simpleTags[i],null,simpleTags[i].inlineTags(),false);

            if (inline!=null) {
                inline.populateElement(entry);
            }
        }

        output.appendDITAElement(row);
        return output;
    }

    /**
     * Produce the output for this simple taglet
     *
     * @param simpleTag
     * @param header of the taglet
     * @return the DITA output for the taglet
     * @since 2.1
     */
    public DITATagletOutput simpleTagOutput(Tag simpleTag,String header,DITADocletWriter writer) {
        final DITATagletOutput output = new DITATagletOutput();
        Row row = getHeaderRow(header);
        output.appendDITAElement(row);

        row = new Row();
        // blank row..
        row.addEntry();

        final Entry entry = row.addEntry();

        final DITATagletOutput inline =
        writer.commentTagsToDITA(
            simpleTag,null,simpleTag.inlineTags(),false);

        if (inline!=null) {
            inline.populateElement(entry);
        }

        output.appendDITAElement(row);
        return output;
    }

    /**
     * Get the header for this taglet, should end in :
     *
     * @param header
     * @return the header row
     * @since 2.1
     */
    private Row getHeaderRow(String header) {
        final Row row = new Row();
        final Entry entry = row.addEntry();
        entry.addElement(new B(header));

        return row;

    }
}
