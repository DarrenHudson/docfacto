/*
 * @author dhudson -
 * Created 3 Dec 2012 : 12:53:08
 */

package com.docfacto.taglets.dita;

import com.docfacto.common.DocfactoException;
import com.docfacto.dita.B;
import com.docfacto.dita.Entry;
import com.docfacto.dita.Row;
import com.docfacto.doclets.dita.DITADocletWriter;
import com.docfacto.doclets.dita.DITATagletOutput;
import com.sun.javadoc.Doc;
import com.sun.javadoc.MethodDoc;
import com.sun.javadoc.SeeTag;
import com.sun.javadoc.Tag;
import com.sun.tools.doclets.internal.toolkit.taglets.InheritableTaglet;
import com.sun.tools.doclets.internal.toolkit.util.DocFinder;
import com.sun.tools.doclets.internal.toolkit.util.DocFinder.Input;
import com.sun.tools.doclets.internal.toolkit.util.DocFinder.Output;

/**
 * DITA version of the JavaDoc See tag
 * 
 * @author dhudson - created 3 Dec 2012
 * @since 2.1
 */
public class DITASeeTaglet extends DITATaglet implements InheritableTaglet {

    /**
     * @see com.sun.tools.doclets.internal.toolkit.taglets.Taglet#getName()
     */
    @Override
    public String getName() {
        return "see";
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#isInlineTag()
     */
    @Override
    public boolean isInlineTag() {
        return false;
    }

    /**
     * @see com.sun.tools.doclets.internal.toolkit.taglets.InheritableTaglet#inherit(com.sun.tools.doclets.internal.toolkit.util.DocFinder.Input,
     * com.sun.tools.doclets.internal.toolkit.util.DocFinder.Output)
     */
    @Override
    public void inherit(Input input,Output output) {
        final Tag[] tags = input.method.seeTags();
        if (tags.length>0) {
            output.holder = input.method;
            output.holderTag = tags[0];
            output.inlineTags = input.isFirstSentence ?
                tags[0].firstSentenceTags() : tags[0].inlineTags();
        }
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABlockTagletOutput(com.sun.javadoc.Doc,
     * com.sun.javadoc.Tag, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABlockTagletOutput(Doc holder,Tag tag,
    DITADocletWriter writer)
    throws DocfactoException {
        return getDITABatchTagletOutput(tag.holder(),writer);
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITAInLineTagletOutput(com.sun.javadoc.Tag,
     * com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITAInLineTagletOutput(Tag tag,
    DITADocletWriter writer)
    throws DocfactoException {
        return writer.seeTagToDITA((SeeTag)tag);
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABatchTagletOutput(com.sun.javadoc.Doc,
     * com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABatchTagletOutput(Doc holder,
    DITADocletWriter writer)
    throws DocfactoException {
        SeeTag[] tags = holder.seeTags();
        if (tags.length==0&&holder instanceof MethodDoc) {
            final DocFinder.Output inheritedDoc =
            DocFinder.search(new DocFinder.Input((MethodDoc)holder,this));
            if (inheritedDoc.holder!=null) {
                tags = inheritedDoc.holder.seeTags();
            }
        }
        return seeTagOutput(holder,tags,writer);
    }

    /**
     * Process the see tag
     * 
     * @param holder
     * @param seeTags
     * @param writer
     * @return the row with the {@code xref}
     * @since 2.1
     */
    private DITATagletOutput seeTagOutput(Doc holder,SeeTag[] seeTags,
    DITADocletWriter writer) {
        final DITATagletOutput output = new DITATagletOutput();

        if (seeTags.length>0) {
            output.appendDITAElements(addSeeHeader());
            for (int i = 0;i<seeTags.length;++i) {
                final Row row = new Row();
                // Add empty entry
                row.addEntry();
                final Entry entry = row.addEntry();
                writer.seeTagToDITA(seeTags[i]).populateElement(entry);
                output.appendDITAElement(row);
            }
        }

        return output;
    }

    /**
     * Return the block see tag header
     * 
     * @return A {@code row} containing the header
     * @since 2.1
     */
    private DITATagletOutput addSeeHeader() {
        final DITATagletOutput output = new DITATagletOutput();
        final Row row = new Row();
        final Entry entry = row.addEntry();
        entry.addElement(new B("Also See:"));
        output.appendDITAElement(row);
        return output;
    }
}
