/*
 * @author dhudson -
 * Created 26 Nov 2012 : 17:12:53
 */

package com.docfacto.taglets.dita;

import com.docfacto.common.DocfactoException;
import com.docfacto.dita.B;
import com.docfacto.dita.Entry;
import com.docfacto.dita.Row;
import com.docfacto.doclets.dita.DITADocletController;
import com.docfacto.doclets.dita.DITADocletWriter;
import com.docfacto.doclets.dita.DITATagletOutput;
import com.sun.javadoc.Doc;
import com.sun.javadoc.MethodDoc;
import com.sun.javadoc.Tag;
import com.sun.javadoc.Type;
import com.sun.tools.doclets.internal.toolkit.taglets.InheritableTaglet;
import com.sun.tools.doclets.internal.toolkit.util.DocFinder;
import com.sun.tools.doclets.internal.toolkit.util.DocFinder.Input;
import com.sun.tools.doclets.internal.toolkit.util.DocFinder.Output;

/**
 * DITA version of the Return Taglet
 *
 * @author dhudson - created 26 Nov 2012
 * @since 2.1
 */
public class DITAReturnTaglet extends DITATaglet implements InheritableTaglet {

    /**
     * Create a new instance of <code>DITAReturnTaglet</code>.
     */
    public DITAReturnTaglet() {

    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#inConstructor()
     */
    @Override
    public boolean inConstructor() {
        return false;
    }

    /**
     * @see com.sun.tools.doclets.internal.toolkit.taglets.Taglet#getName()
     */
    @Override
    public String getName() {
        return "return";
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#inField()
     */
    @Override
    public boolean inField() {
        return false;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#inOverview()
     */
    @Override
    public boolean inOverview() {
        return false;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#isInlineTag()
     */
    @Override
    public boolean isInlineTag() {
        return false;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#inPackage()
     */
    @Override
    public boolean inPackage() {
        return false;
    }


    /**
     * @see com.sun.tools.doclets.internal.toolkit.taglets.InheritableTaglet#inherit(com.sun.tools.doclets.internal.toolkit.util.DocFinder.Input, com.sun.tools.doclets.internal.toolkit.util.DocFinder.Output)
     */
    @Override
    public void inherit(Input input,Output output) {
        final Tag[] tags = input.method.tags("return");
        if (tags.length > 0) {
            output.holder = input.method;
            output.holderTag = tags[0];
            output.inlineTags = input.isFirstSentence ?
                tags[0].firstSentenceTags() : tags[0].inlineTags();
        }
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABlockTagletOutput(com.sun.javadoc.Doc, com.sun.javadoc.Tag, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABlockTagletOutput(Doc holder,Tag tag,DITADocletWriter writer) throws DocfactoException {
        final Type returnType = ((MethodDoc) holder).returnType();
        Tag[] tags = holder.tags(getName());

        //Make sure we are not using @return tag on method with void return type.
        if (returnType.isPrimitive() && returnType.typeName().equals("void")) {
            if (tags.length > 0) {
                DITADocletController.INSTANCE.printWarningMessage(holder.position(),"Return tag on void method");
            }
            return null;
        }
        //Inherit @return tag if necessary.
        if (tags.length == 0) {
            final DocFinder.Output inheritedDoc =
            DocFinder.search(new DocFinder.Input((MethodDoc) holder, this));
            tags = inheritedDoc.holderTag == null ? tags : new Tag[] {inheritedDoc.holderTag};
        }

        return returnTagOutput(tags[0],writer);
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITAInLineTagletOutput(com.sun.javadoc.Tag, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITAInLineTagletOutput(Tag tag,DITADocletWriter writer) throws DocfactoException {
        return null;
    }

    /**
     * Return a populated DITA row
     *
     * @param returnTag
     * @param writer
     * @return a DITATagletOutput
     * @since 2.1
     */
    private  DITATagletOutput returnTagOutput(Tag returnTag,DITADocletWriter writer ) {
        final DITATagletOutput output = new DITATagletOutput();
        Row row = new Row();
        Entry entry = row.addEntry();
        entry.addElement(new B("Returns:"));

        output.appendDITAElement(row);

        final DITATagletOutput tagletOutput = writer.commentTagsToDITA(returnTag, null, returnTag.inlineTags(),false);
        if(tagletOutput != null) {
            row = new Row();
            // Empty entry
            row.addEntry();
            row.addEntry();

            entry = row.addEntry();
            tagletOutput.populateElement(entry);
            output.appendDITAElement(row);
        }

        return output;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABatchTagletOutput(com.sun.javadoc.Doc, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABatchTagletOutput(Doc holder,DITADocletWriter writer)
    throws DocfactoException {
        return null;
    }
}
