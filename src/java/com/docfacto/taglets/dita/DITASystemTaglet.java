package com.docfacto.taglets.dita;

import com.docfacto.common.DocfactoException;
import com.docfacto.doclets.dita.DITADocletWriter;
import com.docfacto.doclets.dita.DITATagletOutput;
import com.docfacto.javadoc.TagParser;
import com.docfacto.taglets.SystemTagletHelper;
import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;

/**
 * DITA version of the System Taglet
 * 
 * @author dhudson - created 23 Mar 2013
 * @since 2.2
 */
public class DITASystemTaglet extends DITATaglet {

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABlockTagletOutput(com.sun.javadoc.Doc,
     * com.sun.javadoc.Tag, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABlockTagletOutput(Doc holder,Tag tag,
    DITADocletWriter writer) throws DocfactoException {
        return null;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITAInLineTagletOutput(com.sun.javadoc.Tag,
     * com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITAInLineTagletOutput(Tag tag,
    DITADocletWriter writer) throws DocfactoException {
        if (isEnabled()) {
            return SystemTagletHelper.generateDITAOutput(new TagParser(tag
                .text()));
        }
        return null;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABatchTagletOutput(com.sun.javadoc.Doc,
     * com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABatchTagletOutput(Doc holder,
    DITADocletWriter writer)
    throws DocfactoException {
        return null;
    }

}
