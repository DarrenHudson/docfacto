/*
 * @author dhudson -
 * Created 23 Nov 2012 : 15:17:16
 */

package com.docfacto.taglets.dita;

import com.docfacto.common.DocfactoException;
import com.docfacto.doclets.dita.DITADocletWriter;
import com.docfacto.doclets.dita.DITATagletOutput;
import com.docfacto.taglets.BaseTaglet;
import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;
import com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter;

/**
 * Abstract class for DITA taglets.
 * 
 * @author dhudson - created 23 Nov 2012
 * @since 2.1
 */
public abstract class DITATaglet extends BaseTaglet {

    /**
     * @see com.sun.tools.doclets.internal.toolkit.taglets.Taglet#getName()
     */
    @Override
    public String getName() {
        return getInternalDefinition().getName();
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#processInlineTag(java.lang.String,
     * com.sun.javadoc.Tag)
     */
    @Override
    public CharSequence processInlineTag(String text,Tag tag)
    throws DocfactoException {
        // Nothing to do
        return null;
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#processBlockTag(com.sun.javadoc.Tag[],
     * com.sun.javadoc.Doc,
     * com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter)
     */
    @Override
    public CharSequence
    processBlockTag(Tag[] tag,Doc holder,TagletWriter writer)
    throws DocfactoException {
        // Nothing to do
        return null;
    }

    /**
     * Process a single block taglet
     * 
     * @param holder the comment {@code Doc} being processed
     * @param tag to process
     * @param writer the DocletWriter, which has knowledge of the file being
     * generated
     * @return the DITA representation of the taglet or null
     * @throws DocfactoException
     * @since 2.1
     */
    public abstract DITATagletOutput getDITABlockTagletOutput(Doc holder,
    Tag tag,DITADocletWriter writer) throws DocfactoException;

    /**
     * Process an in-line taglet
     * 
     * @param tag
     * @param writer the DocletWriter, which has knowledge of the file being
     * generated
     * @return the DITA representation of the taglet or null
     * @throws DocfactoException if unable to process the inline taglet
     * @since 2.1
     */
    public abstract DITATagletOutput getDITAInLineTagletOutput(Tag tag,
    DITADocletWriter writer
    ) throws DocfactoException;

    /**
     * Process a batch (number of) the same type taglets, these will be block
     * taglets only
     * 
     * @param holder the comment {@code Doc} being processed
     * @param writer the DocletWriter, which has knowledge of the file being
     * generated
     * @return DITA representation of the taglets, or null
     * @throws DocfactoException if unable to process the batch of taglets
     * @since 2.1
     */
    public abstract DITATagletOutput getDITABatchTagletOutput(Doc holder,
    DITADocletWriter writer) throws DocfactoException;

}
