/*
 * @author dhudson -
 * Created 3 Dec 2012 : 10:31:39
 */

package com.docfacto.taglets.dita;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.StringUtils;
import com.docfacto.doclets.dita.DITADocletWriter;
import com.docfacto.doclets.dita.DITATagletOutput;
import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;

/**
 * Look to changing this to use CDATA tags.
 * 
 * @author dhudson - created 3 Dec 2012
 * @since 2.1
 */
public class DITALiteralTaglet extends DITATaglet {

    /**
     * @see com.sun.tools.doclets.internal.toolkit.taglets.Taglet#getName()
     */
    @Override
    public String getName() {
        return "literal";
    }

    /**
     * @see com.docfacto.taglets.BaseTaglet#isInlineTag()
     */
    @Override
    public boolean isInlineTag() {
        return true;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABlockTagletOutput(com.sun.javadoc.Doc,
     * com.sun.javadoc.Tag, com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABlockTagletOutput(Doc holder,Tag tag,
    DITADocletWriter writer)
    throws DocfactoException {
        return null;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITAInLineTagletOutput(com.sun.javadoc.Tag,
     * com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITAInLineTagletOutput(Tag tag,
    DITADocletWriter writer)
    throws DocfactoException {
        final DITATagletOutput output = new DITATagletOutput();
        output.appendTextElement(StringUtils.escapeXML(tag.text()));
        return output;
    }

    /**
     * @see com.docfacto.taglets.dita.DITATaglet#getDITABatchTagletOutput(com.sun.javadoc.Doc,
     * com.docfacto.doclets.dita.DITADocletWriter)
     */
    @Override
    public DITATagletOutput getDITABatchTagletOutput(Doc holder,
    DITADocletWriter writer)
    throws DocfactoException {
        return null;
    }

}
