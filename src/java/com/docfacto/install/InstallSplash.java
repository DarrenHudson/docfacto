package com.docfacto.install;

import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

/**
 * Didn't have much joy with using the Java Splash screens, so here is a home
 * grown one. It understands headless mode, so will not display a splash screen
 * 
 * <p>
 * {@docfacto.media uri="doc-files/InstallSplash.png" }
 * </p>
 * 
 * @author dhudson
 * @since 2.3
 */
public class InstallSplash {

    private JProgressBar theProgressBar;
    private boolean isHeadless;
    private JFrame theSplash;

    /**
     * Constructor
     * 
     * @param noOfEntries Number of entries in the Jarball
     * @since 2.3
     */
    public InstallSplash(int noOfEntries) {
        isHeadless = GraphicsEnvironment.isHeadless();

        if (!isHeadless) {
            theSplash = new JFrame();
            theSplash.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            theSplash.setUndecorated(true);

            theSplash.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            JLabel label = new JLabel(loadImageIcon());

            Container container = theSplash.getContentPane();
            container.setLayout(new GridBagLayout());
            container.setBackground(Color.WHITE);

            GridBagConstraints gBC = new GridBagConstraints();
            gBC.gridx = 0;
            gBC.gridy = 0;
            gBC.weightx = 0.5;
            gBC.weighty = 0.5;
            gBC.insets = new Insets(0,0,0,0);
            gBC.anchor = GridBagConstraints.NORTHWEST;
            gBC.fill = GridBagConstraints.BOTH;

            container.add(label,gBC);

            gBC.gridy++;

            theProgressBar = new JProgressBar(0,noOfEntries);
            theProgressBar.setValue(0);
            container.add(theProgressBar,gBC);

            theSplash.setSize(400,110);
            centreFrame(theSplash);

            theSplash.setVisible(true);

            // This all happens too fast, so just slow it down..
            sleep();
        }
        else {
            System.out.print("Extracting .");
        }
    }

    /**
     * Update the progress of the install
     * 
     * @param value current file entry number
     * @since 2.3
     */
    void update(int value) {
        if (isHeadless) {
            if (value%10==0) {
                System.out.print(".");
            }
        }
        else {
            theProgressBar.setValue(value);
        }
    }

    /**
     * All done, any tiding up can be done here.
     * 
     * @since 2.3
     */
    void finished() {
        if (isHeadless) {
            System.out.println("\ndone");
        }
        else {
            sleep();
            theSplash.setVisible(false);
            theSplash.dispose();
        }
    }

    /**
     * Just sleep for a second
     * 
     * @since 2.3
     */
    private void sleep() {
        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException ignore) {
        }
    }

    /**
     * Load the Image Icon from the jar ball resource
     * 
     * @return the docfacto icon
     */
    private ImageIcon loadImageIcon() {
        URL imageURL = getClass().getResource(
            "/com/docfacto/install/resources/docfacto_logo.jpg");
        if (imageURL!=null) {
            return new ImageIcon(imageURL);
        }

        return null;
    }

    /**
     * Positions a frame in the middle of the screen
     * 
     * @param frame to centre
     * @since 2.3
     */
    private void centreFrame(JFrame frame) {

        final Dimension screenSize = Toolkit.getDefaultToolkit()
            .getScreenSize();
        final Dimension size = frame.getSize();

        screenSize.height = screenSize.height/2;
        screenSize.width = screenSize.width/2;

        size.height = size.height/2;
        size.width = size.width/2;

        final int y = screenSize.height-size.height;
        final int x = screenSize.width-size.width;

        frame.setLocation(x,y);
    }

    /**
     * @param args
     * @docfacto.adam ignore
     */
    public static void main(String[] args) {
        new InstallSplash(100);
    }

}
