package com.docfacto.install;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * This class is used to make the jarball self extracting
 * 
 * @author dhudson - created 30 Apr 2013
 * @since 2.2
 */
public class SelfExtractor {

    private static final String MANIFEST = "META-INF/MANIFEST.MF";

    /**
     * Constructor.
     * @since 2.2
     */
    public SelfExtractor() {
    }

    /**
     * Extract the jarball
     * 
     * @since 2.2
     */
    public void extract() {
        
        File archive =
            new File(this.getClass().getProtectionDomain().getCodeSource()
                .getLocation().getFile());

        try {
            ZipFile zf = new ZipFile(archive);
            FileOutputStream out = null;
            InputStream in = null;
            byte[] buf = new byte[1024];

            Enumeration<? extends ZipEntry> entries = zf.entries();
            
            int size = zf.size();
            
            InstallSplash splash = new InstallSplash(size);
            
            for (int i = 0;i<size;i++) {
                ZipEntry entry = (ZipEntry)entries.nextElement();
                if (entry.isDirectory()) {
                    continue;
                }

                splash.update(i);
                
                String pathname = entry.getName();

                // We don't want the Self Extractor or the manifest
                if (pathname.startsWith("com")||
                    MANIFEST.equals(pathname.toUpperCase())) {
                    continue;
                }

                in = zf.getInputStream(entry);

                File outFile = new File(pathname);

                File parent = new File(outFile.getParent());
                if (parent!=null&&!parent.exists())
                {
                    parent.mkdirs();
                }

                out = new FileOutputStream(outFile);

                while (true)
                {
                    int nRead = in.read(buf,0,buf.length);
                    if (nRead<=0) {
                        break;
                    }
                    out.write(buf,0,nRead);
                }

                out.close();
            }

            zf.close();
            
            splash.finished();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * This is called from the MainClass in the manifest
     * 
     * @param args none provided
     * @since 2.2
     */
    public static void main(String[] args) {
        new SelfExtractor().extract();
        System.exit(0);
    }

}
