/*
 * @author dhudson -
 * Created 19 Nov 2012 : 17:07:59
 */

package com.docfacto.xml;

import java.util.Vector;

import com.docfacto.common.NameValuePair;
import com.docfacto.common.StringUtils;

/**
 * This class represents a text fragment.
 * 
 * Text fragments have no element names or attributes, they just contain a value
 * of some text.
 * 
 * @author dhudson - created 19 Nov 2012
 * @since 2.1
 */
public class XMLTextElement extends Element {

    private boolean isPreserveFormat = false;

    private static final String NBS = "&#160;";

    /**
     * Create a new instance of <code>XMLTextElement</code>.
     * 
     * @param value
     */
    public XMLTextElement(String value) {
        this(value,false);
    }

    /**
     * Create a new instance of <code>XMLTextElement</code>.
     * 
     * @param value
     * @param preserveFormat
     */
    public XMLTextElement(String value,boolean preserveFormat) {
        super(value);
        isPreserveFormat = preserveFormat;
    }

    /**
     * Preserve text formatting
     * 
     * @since 2.1
     */
    public void preserve() {
        isPreserveFormat = true;
    }

    /**
     * @see com.docfacto.xml.Element#getValue()
     */
    @Override
    public String getValue() {
        if (!isPreserveFormat) {
            return StringUtils.normaliseWhitespace(super.getValue());
        }
        return super.getValue();
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return null;
    }

    /**
     * @see com.docfacto.xml.Element#isTextElement()
     */
    @Override
    public boolean isTextElement() {
        return true;
    }

    /**
     * @see com.docfacto.xml.Element#addElement(com.docfacto.xml.XMLElement)
     */
    @Override
    public void addElement(XMLElement element) throws RuntimeException {
        throw new RuntimeException("Text Elements can not have children");
    }

    /**
     * @see com.docfacto.xml.Element#getElements()
     */
    @Override
    public Vector<XMLElement> getElements() {
        return null;
    }

    /**
     * @see com.docfacto.xml.Element#hasChildren()
     */
    @Override
    public boolean hasChildren() {
        return false;
    }

    /**
     * @see com.docfacto.xml.Element#getNumberOfElements()
     */
    @Override
    public int getNumberOfElements() {
        return 0;
    }

    /**
     * @see com.docfacto.xml.Element#addStyleAttribute(java.lang.String)
     */
    @Override
    public void addStyleAttribute(String attribute) throws RuntimeException {
        throw new RuntimeException("Text Elements can not have attributes");
    }

    /**
     * @see com.docfacto.xml.Element#addAttribute(java.lang.String,
     * java.lang.String)
     */
    @Override
    public void addAttribute(String name,String value) {
        throw new RuntimeException("Text Elements can not have attributes");
    }

    /**
     * @see com.docfacto.xml.Element#addAttribute(com.docfacto.common.NameValuePair)
     */
    @Override
    public void addAttribute(NameValuePair attribute) {
        throw new RuntimeException("Text Elements can not have attributes");
    }

    /**
     * @see com.docfacto.xml.Element#setID(java.lang.String)
     */
    @Override
    public void setID(String id) {
        throw new RuntimeException("Text Elements can not have attributes");
    }

    /**
     * @see com.docfacto.xml.Element#getID()
     */
    @Override
    public String getID() {
        return null;
    }

    /**
     * @see com.docfacto.xml.Element#getStyleAttributes()
     */
    @Override
    public String getStyleAttributes() {
        return null;
    }

    /**
     * @see com.docfacto.xml.Element#hasStyleAttributes()
     */
    @Override
    public boolean hasStyleAttributes() {
        return false;
    }

    /**
     * @see com.docfacto.xml.Element#getIntAttributeValue(java.lang.String)
     */
    @Override
    public int getIntAttributeValue(String attributeName)
    throws RuntimeException {
        throw new RuntimeException("Text Elements can not have attributes");
    }

    /**
     * @see com.docfacto.xml.Element#hasAttributes()
     */
    @Override
    public boolean hasAttributes() {
        return false;
    }

    /**
     * @see com.docfacto.xml.Element#toString()
     */
    @Override
    public String toString() {
        if (getValue()!=null) {
            return getValue();
        }
        return "Text Element";
    }

}
