package com.docfacto.xml;

/**
 * Default Parser Handler has empty methods for the interface
 * {@code XMLParserHandler}
 * 
 * These methods need to be overridden by the sub class
 * 
 * @author dhudson - created 10 Jun 2013
 * @since 2.4
 */
public class DefaultParserHandler implements XMLParserHandler {

    /**
     * @see com.docfacto.xml.XMLParserHandler#beginDocument()
     */
    @Override
    public void beginDocument() {
    }

    /**
     * @see com.docfacto.xml.XMLParserHandler#endDocument()
     */
    @Override
    public void endDocument() {
    }

    /**
     * @see com.docfacto.xml.XMLParserHandler#processComment(java.lang.String)
     */
    @Override
    public void processComment(String comment) {
    }

    /**
     * @see com.docfacto.xml.XMLParserHandler#processEndTag(java.lang.String)
     */
    @Override
    public void processEndTag(String endTag) {
    }

    /**
     * @see com.docfacto.xml.XMLParserHandler#processStartTag(com.docfacto.xml.GenericDynamicElement)
     */
    @Override
    public void processStartTag(GenericDynamicElement gdl) {
    }

    /**
     * @see com.docfacto.xml.XMLParserHandler#processText(java.lang.String)
     */
    @Override
    public void processText(String text) {
    }

    /**
     * @see com.docfacto.xml.XMLParserHandler#processDocType(java.lang.String)
     */
    @Override
    public void processDocType(String text) {
    }

    /**
     * @see com.docfacto.xml.XMLParserHandler#processProcessingInstruction(java.lang.String)
     */
    @Override
    public void processProcessingInstruction(String text) {
    }

}
