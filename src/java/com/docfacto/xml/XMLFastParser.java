package com.docfacto.xml;

import com.docfacto.exceptions.DocfactoParsingException;

/**
 * This XML parser can handle fragments of XML as well as unqualified XML I.E
 * HTML
 * 
 * @author dhudson - created 11 Dec 2012
 * @since 2.1
 */
public class XMLFastParser {

    private final XMLStringReader theReader;
    private int theCommentLines = 0;

    /**
     * Constructor.
     * 
     * @param xml to process
     */
    public XMLFastParser(String xml) {
        theReader = new XMLStringReader(xml);
    }

    /**
     * Parse the given XML in the constructor and callback the relevant methods
     * of the handler
     * 
     * @param handler to which methods to callback
     * @throws DocfactoParsingException if something goes wrong
     * @since 2.1
     */
    public void parse(XMLParserHandler handler) throws DocfactoParsingException {
        try {
            char c;
            StringBuilder text = new StringBuilder(100);
            boolean isInComment = false;
            boolean isInPI = false;
            boolean isInDocType = false;

            // Call the begin document
            handler.beginDocument();

            while ((c = theReader.read())!=XMLStringReader.EOF) {

                if (c=='\n'&&isInComment) {
                    theCommentLines++;
                }

                switch (c) {

                case '?':
                    if (isInPI) {
                        if (theReader.peek()=='>') {
                            // Its the end of the processing instruction
                            handler.processProcessingInstruction(text
                                .toString());
                            text = new StringBuilder(100);
                            isInPI = false;
                            theReader.read();
                            continue;
                        }
                    }
                    else {
                        text.append(c);
                        continue;
                    }

                case '>':
                    if (isInDocType) {
                        // Its the end of the doc type..
                        handler.processDocType(text.toString());
                        text = new StringBuilder(100);
                        isInDocType = false;
                    }
                    continue;

                case '<':
                    if (isInComment) {
                        theCommentLines++;
                        text.append(c);
                        break;
                    }

                    char next = theReader.peek();

                    switch (next) {
                    case '?':
                        // Processing instruction
                        isInPI = true;
                        theReader.read();
                        continue;

                    case '!':
                        // Possible that this could be a comment
                        theReader.mark();
                        // Read the !
                        theReader.read();

                        if (theReader.peek()=='D') {
                            isInDocType = true;
                            continue;
                        }

                        if (theReader.read()=='-'&&theReader.read()=='-') {
                            // We are in a comment..
                            isInComment = true;
                            continue;
                        }
                        else {
                            theReader.rewindToMark();
                        }
                        break;

                    case '/':
                        text = processText(text,handler);

                        // Consume the /
                        theReader.read();
                        final String endTag = theReader.returnEndTag();
                        handler.processEndTag(endTag);
                        continue;

                    default:
                        // Right has to be a start or end tag, so if there any
                        // text
                        // between these elements?

                        if (!isInComment) {
                            text = processText(text,handler);

                            // It can be a start or end tag sequence
                            // Its a start sequence
                            final GenericDynamicElement el =
                                theReader.returnStartTag();
                            handler.processStartTag(el);

                            continue;
                        }

                        break;
                    }

                case '-':
                    // This is only used for end of comment processing..
                    if (isInComment) {
                        if (theReader.peek()=='-') {
                            theReader.mark();
                            theReader.read();
                            if (theReader.read()=='>') {
                                // Its the end of the comment
                                isInComment = false;
                                handler.processComment(text.toString());
                                text = new StringBuilder(100);
                                break;
                            }
                            else {
                                theReader.rewindToMark();
                            }
                        }
                    }

                default:
                    // Watch out for the case statements with no break
                    // as I want them to end up here
                    text.append(c);
                }
            }

            // Is there some text left over? - should only happen for HTML
            text = processText(text,handler);

            // Call the end document
            handler.endDocument();
        }
        catch (Throwable t) {
            throw new DocfactoParsingException(t.getMessage(),t);
        }
    }

    private StringBuilder processText(StringBuilder text,
    XMLParserHandler handler) {
        if (text.length()>0) {
            final String trimText = text.toString().trim();

            if (!trimText.isEmpty()) {
                handler.processText(trimText);
            }
            return new StringBuilder(100);
        }

        return text;
    }

    /**
     * Return the current column within the XML file
     * 
     * @return the current reading position
     * @since 2.3
     */
    public int getCurrentCol() {
        return theReader.getCurrentCol();
    }

    /**
     * Return the current line number.
     * 
     * If called with the {@code endDocument} then this will return the number
     * of lines in the file
     * 
     * @return the current line number
     * @since 2.3
     */
    public int getCurrentLine() {
        return theReader.getCurrentLine();
    }

    /**
     * The number of comment lines processed so far.
     * 
     * This is different to the number of comments
     * 
     * @return the number of lines that comment encompass
     * @since 2.3
     */
    public int getCommentLines() {
        return theCommentLines;
    }

    /**
     * Return the XML that is currently being parsed
     * 
     * @return the XML
     * @since 2.3
     */
    public String getXML() {
        return theReader.getXML();
    }
}
