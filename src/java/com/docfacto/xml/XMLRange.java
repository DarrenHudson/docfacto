/*
 * @author dhudson -
 * Created 26 May 2012 : 08:46:27
 */

package com.docfacto.xml;

/**
 * Helper class to encapsulate minOccurs and maxOccurs
 * 
 * @author dhudson - created 26 May 2012
 * @since 2.0
 */
public class XMLRange {

    /**
     * {@value}
     */
    public static final int UNBOUNDED = -1;
    
    /**
     * Min occurrence for this range
     */
    private int theMinOccurs = 0;
    /**
     * Max occurrence for this range
     */
    private int theMaxOccurs = 0;

    /**
     * Create a new instance of <code>XMLRange</code>.
     */
    public XMLRange() {
    }

    /**
     * Create a new instance of <code>XMLRange</code>, with the min and max values set.
     * 
     * {@docfacto.note maxOccurs unbounded = -1 }
     * 
     * @param minOccurs the minimum occurrence for this range
     * @param maxOccurs the maximum occurrence for this range
     */
    public XMLRange(int minOccurs,int maxOccurs) {
        theMinOccurs = minOccurs;
        theMaxOccurs = maxOccurs;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder(100);
        builder.append("[");
        builder.append(theMinOccurs);
        builder.append(" .. ");
        builder.append(theMaxOccurs);
        builder.append("]");
        return builder.toString();
    }

    /**
     * Returns the min occurs for this node
     * 
     * @return the min occurs
     * @since 2.0
     */
    public int getMinOccurs() {
        return theMinOccurs;
    }

    /**
     * Return the max occurs for this node
     * 
     * @return the max occurs
     * @since 2.0
     */
    public int getMaxOccurs() {
        return theMaxOccurs;
    }

    /**
     * Set the minimum occurrence
     * 
     * @param minOccurs value
     * @since 2.0
     */
    public void setMinOccurs(int minOccurs) {
        theMinOccurs = minOccurs;
    }

    /**
     * Set the maximum occurrence
     * 
     * @param maxOccurs value
     * @since 2.0
     */
    public void setMaxOccurs(int maxOccurs) {
        theMaxOccurs = maxOccurs;
    }

    /**
     * Is this element required
     * 
     * @return true if the min and max occurs are both 1
     * @since 2.0
     */
    public boolean isRequired() {
        if (theMinOccurs==1&&theMaxOccurs==1) {
            return true;
        }

        return false;
    }
    
    /**
     * Check to see if the max occurs is unbounded.
     * 
     * @return true if the Max Occurs is unbounded
     * @since 2.5
     */
    public boolean isMaxUnbounded() {
        return theMaxOccurs == UNBOUNDED;
    }
}
