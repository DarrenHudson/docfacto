/*
 * @author dhudson -
 * Created 22 May 2012 : 06:31:31
 */

package com.docfacto.xml;

import java.io.File;
import java.io.IOException;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.common.Utils;
import com.docfacto.common.XMLUtils;

/**
 * An abstract class which forms as helpers to other XML type documents.
 * 
 * 
 * @author dhudson - created 22 May 2012
 * @since 2.0
 */
public abstract class XMLDocument {

    static final String XML_DECL =
    "<?xml version=\"1.0\" encoding=\"utf-8\"?>";

    /**
     * The root node of the XMLDocument
     */
    protected Element theRootNode;

    /**
     * Create a new instance of <code>XMLDocument</code>.
     */
    public XMLDocument() {
    }

    /**
     * Generates a Created By Docfacto [timestamp] XML comment
     * 
     * @return An XML comment, which has been time stamped
     * @since 2.0
     */
    public String generateDocfactoComment() {
        final StringBuilder builder = new StringBuilder(100);
        builder.append("<!-- Created by Docfacto [");
        builder.append(Utils.formatDateTime(System.currentTimeMillis()));
        builder.append("] -->");

        return builder.toString();
    }

    /**
     * Returns the default XML declaration
     * 
     * @return the default XML declaration
     * @since 2.0
     */
    public String getXMLDeclaration() {
        return XML_DECL;
    }

    /**
     * Saves the representation of this document to disk The <code>toXML</code>
     * method will be called to generate the content. The content will also be
     * pretty formatted
     * 
     * @param fileName path and filename where to save the file
     * @throws IOException if unable to write to the file
     * @throws DocfactoException if unable to process the XML
     * @since 2.0
     */
    public void save(String fileName) throws IOException, DocfactoException {
        IOUtils.writeFile(fileName,XMLUtils.prettyFormat(toXML()));
    }

    /**
     * Saves the representation of this document to disk The <code>toXML</code>
     * method will be called to generate the content. The content will also be
     * pretty formatted
     *
     * @param file to save
     * @throws IOException if unable to write to the file
     * @throws DocfactoException if unable to process the XML
     * @since 2.1
     */
    public void save(File file) throws IOException, DocfactoException {
        IOUtils.writeFile(file,XMLUtils.prettyFormat(toXML()));
    }

    /**
     * @see com.docfacto.xml.XMLDocument#toXML()
     */
    public String toXML() {
        final StringBuilder builder = new StringBuilder(2000);
        builder.append(getXMLDeclaration());
        builder.append(getDocumentDeclaration());
        builder.append(generateDocfactoComment());
        builder.append(theRootNode.toXML());
        return builder.toString();

    }

    /**
     * Add a Element to the root node
     * 
     * @param element to add to the root node
     * @since 2.0
     */
    public void addToRootNode(XMLElement element) {
        theRootNode.addElement(element);
    }

    /**
     * Return the DOC TYPE for the given document
     * 
     * @return the document declaration
     * @since 2.0
     */
    public abstract String getDocumentDeclaration();
}
