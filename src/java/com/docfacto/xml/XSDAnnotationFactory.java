package com.docfacto.xml;

import com.sun.xml.xsom.parser.AnnotationParser;
import com.sun.xml.xsom.parser.AnnotationParserFactory;

/*
 * @author dhudson -
 * Created 11 Apr 2012 : 13:53:09
 */

/**
 * An annotation parser factory class which generates the Docfacto annotation parser
 *
 * @author dhudson - created 8 Jul 2012
 * @since 1.4
 */
public class XSDAnnotationFactory implements AnnotationParserFactory {

    /**
     * Create a new instance of <code>XSDAnnotationFactory</code>.
     */
    public XSDAnnotationFactory() {
    }

    /**
     * @see com.sun.xml.xsom.parser.AnnotationParserFactory#create()
     */
    @Override
    public AnnotationParser create() {
        return new XSDAnnotationParser();
    }
}
