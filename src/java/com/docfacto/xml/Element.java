package com.docfacto.xml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.docfacto.common.NameValuePair;

/**
 * This class encapsulates an XML element, the element may be complex (has
 * children) or simple, just has a value.
 * 
 * If its complex, the it can have text elements as well as other elements,
 * these will be {@link XMLTextElement}s
 * 
 * 
 * @author dhudson - created 22 May 2012
 * @since 2.0
 */
public abstract class Element implements XMLElement {

    private final List<String> theStyleAttributes;
    private String theValue;

    private final HashMap<String,NameValuePair> theAttributes;

    private Object theAttachment;

    private XMLElement theParent;

    // A simple element doesn't have nested elements,
    // so just create an empty list
    // This is so we can share a common toSVG method with ComplexElement
    List<XMLElement> theElements;

    /**
     * Create a new instance of <code>Element</code>.
     */
    public Element() {
        theStyleAttributes = new ArrayList<String>(5);
        theAttributes = new HashMap<String,NameValuePair>(3);
        theElements = new ArrayList<XMLElement>(3);
    }

    /**
     * Create a new instance of <code>Element</code> with the given value.
     * 
     * @param value of the element if simple type
     */
    public Element(String value) {
        this();
        theValue = value;
    }

    /**
     * Add an XML Element to this complex type, setting the parent on the way
     * 
     * @param element to add as a child
     * @since 2.0
     */
    public void addElement(XMLElement element) {
        element.setParent(this);
        theElements.add(element);
    }

    /**
     * Add a new {@code XMLTextElement} to the children, with the given value
     * 
     * @param text to add
     * @since 2.1
     */
    public void addTextElement(String text) {
        addElement(new XMLTextElement(text));
    }

    /**
     * Adds a list of XML elements to the end of the children
     * 
     * @param elements to add to this node
     * @since 2.1
     */
    public void addElements(List<XMLElement> elements) {
        for (final XMLElement element:elements) {
            addElement(element);
        }
    }

    /**
     * Return the list of children
     * 
     * @return a <code>List</code> of elements
     * @since 2.0
     */
    public List<XMLElement> getElements() {
        return theElements;
    }

    /**
     * Retrieve an element at a given index. Runtime exception thrown if index
     * out of bounds
     * 
     * @see java.util.ArrayList#get(int)
     * @param index
     * @return the element at the given index
     * @since 2.1
     */
    public XMLElement getElementAt(int index) {
        return theElements.get(index);
    }

    /**
     * Remove all of the children from this node, this will also set the parent
     * to null for all of the children as well.
     * 
     * @since 2.1
     */
    public void removeChildren() {
        for (final XMLElement element:theElements) {
            // Remove this from the child
            element.setParent(null);
        }
        theElements = new ArrayList<XMLElement>(3);
    }

    /**
     * Set the parent of this node
     * 
     * @param parent of this node
     * @since 2.1
     */
    @Override
    public void setParent(XMLElement parent) {
        theParent = parent;
    }

    /**
     * Returns the parent if known or null if not
     * 
     * @return the parent if known
     * @since 2.1
     */
    @Override
    public XMLElement getParent() {
        return theParent;
    }

    /**
     * Test to see if the complex element has children
     * 
     * @return true if the this element has children
     * @since 2.0
     */
    public boolean hasChildren() {
        return !theElements.isEmpty();
    }

    /**
     * Returns the number of sub elements
     * 
     * @return the number of sub elements this element has
     * @since 2.1
     */
    public int getNumberOfElements() {
        return theElements.size();
    }

    /**
     * Adds attribute to the style attribute for example
     * <code>"fill:none"</code> Note that the termination semi colon is not
     * required many style attributes can be add at once using the semi colon as
     * the feature delimiter
     * 
     * @param attribute to add to the element
     * @since 2.0
     */
    public void addStyleAttribute(String attribute) {
        theStyleAttributes.add(attribute);
    }

    /**
     * Add attribute attribute to this element. If the attribute already exists,
     * then this will over-write it.
     * 
     * @param name of attribute
     * @param value of attribute
     * @since 2.0
     */
    public void addAttribute(String name,String value) {
        addAttribute(new NameValuePair(name,value));
    }

    /**
     * Add an attribute to this element. If the attribute already exists, then
     * this will over-write it.
     * 
     * @param attribute to add to this element
     * @since 2.0
     */
    public void addAttribute(NameValuePair attribute) {
        theAttributes.put(attribute.getName(),attribute);
    }

    /**
     * Return a list of attributes for this element
     * 
     * @return a list if attributes, which can be empty
     * @since 2.1
     */
    public List<NameValuePair> getAttributes() {
        final ArrayList<NameValuePair> attrs =
        new ArrayList<NameValuePair>(theAttributes.size());
        for (final Entry<String,NameValuePair> entry:theAttributes.entrySet()) {
            attrs.add(entry.getValue());
        }
        return attrs;
    }

    /**
     * Helper method to set the <code>id</code> attribute
     * 
     * @param id value of the attribute <code>id</code>
     * @since 2.0
     */
    public void setID(String id) {
        addAttribute("id",id);
    }

    /**
     * Returns the Attribute <code>id</code> if set or null
     * 
     * @return the id or null
     * @since 2.0
     */
    public String getID() {
        final NameValuePair attribute = theAttributes.get("id");
        if (attribute==null) {
            return null;
        }
        return attribute.getValue();
    }

    /**
     * Returns the style attributes associated with this element
     * 
     * @return style attributes or null if none set
     * @since 2.0
     */
    public String getStyleAttributes() {
        final StringBuilder builder = new StringBuilder(100);
        builder.append("style=\"");

        int index = 0;

        for (final String attr:theStyleAttributes) {
            if (index!=0) {
                builder.append(" ");
            }
            index++;

            builder.append(attr);
            // NB: May want to take out the extra space.
            builder.append(";");
        }

        builder.append("\"");
        return builder.toString();
    }

    /**
     * Returns true if there are style attributes
     * 
     * @return true if there are style attributes
     * @since 2.0
     */
    public boolean hasStyleAttributes() {
        return !theStyleAttributes.isEmpty();
    }

    /**
     * Sets the element value, if this is a complex element and this is set then
     * the children of this element will be ignored as it is not possible to
     * have both
     * 
     * @param value of element
     * @since 2.0
     */
    public void setValue(String value) {
        theValue = value;
    }

    /**
     * Returns the element value if set, should be overridden to provide extra
     * processing if required.
     * 
     * @return the element value or null
     * @since 2.0
     */
    public String getValue() {
        return theValue;
    }

    /**
     * Helper method to return a given attribute value as an int
     * 
     * @param attributeName name of the attribute
     * @return the attribute value as an int or <code>Integer.MIN_VALUE</code>
     * if attribute not present of parseable
     * @since 2.0
     */
    public int getIntAttributeValue(String attributeName) {
        final NameValuePair attr = theAttributes.get(attributeName);
        if (attr==null) {
            return Integer.MIN_VALUE;
        }

        try {
            return Integer.parseInt(attr.getValue());
        }
        catch (final NumberFormatException ex) {
            return Integer.MIN_VALUE;
        }
    }

    /**
     * True if the element has attributes
     * 
     * @return true if the element has attributes
     * @since 2.0
     */
    public boolean hasAttributes() {
        return (!theAttributes.isEmpty());
    }

    /**
     * @see com.docfacto.xml.XMLElement#toXML()
     */
    @Override
    public String toXML() {

        // If its a text fragment, then just spit out the text as they can not
        // have children
        if (isTextElement()) {
            return theValue;
        }

        final StringBuilder builder = new StringBuilder(100);
        builder.append("<");
        builder.append(getElementName());

        if (hasAttributes()) {
            for (final NameValuePair attr:theAttributes.values()) {
                builder.append(" ");
                builder.append(attr.toString());
            }
        }

        if (!theStyleAttributes.isEmpty()) {
            builder.append(" ");
            builder.append(getStyleAttributes());
        }

        // Its is possible that we are a complex item (can have children), but
        // don't and just have a value
        if (getValue()!=null) {
            builder.append(">");
            builder.append(getValue());
            builder.append("</");
            builder.append(getElementName());
            builder.append(">");

            return builder.toString();
        }

        // Value must be null, do we have any elements
        // This works for complex or simple
        if (theElements.isEmpty()) {
            builder.append(" />");
            return builder.toString();
        }

        // Need to iterate through the elements
        builder.append(">");

        for (final XMLElement element:theElements) {
            builder.append(element.toXML());
        }

        builder.append("</");
        builder.append(getElementName());
        builder.append(">");

        return builder.toString();
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "<"+getElementName()+">";
    }

    /**
     * Attaches a give object to this element
     * 
     * An attached object may later be retrieved via the attachment method. Only
     * one object may be attached at a time; invoking this method causes any
     * previous attachment to be discarded. The current attachment may be
     * discarded by attaching null.
     * 
     * @param attachment user object
     * @since 2.0
     */
    public void attach(Object attachment) {
        theAttachment = attachment;
    }

    /**
     * Retrieves the current attachment
     * 
     * @return The object currently attached to this element or null
     * @since 2.0
     */
    public Object attachment() {
        return theAttachment;
    }

    /**
     * Check to see if the element should be treated like a text fragment
     * 
     * @return true if element is a text fragment
     * @since 2.1
     */
    public boolean isTextElement() {
        return false;
    }

    /**
     * @see com.docfacto.xml.XMLElement#containsElementWithName(java.lang.String)
     */
    @Override
    public boolean containsElementWithName(String elementName) {
        if (theElements.isEmpty()) {
            return false;
        }

        for (final XMLElement element:theElements) {
            if (elementName.equalsIgnoreCase(element.getElementName())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Return the element tag without the &lt; &gt;
     * 
     * @return the element tag
     * @since 2.0
     */
    @Override
    public abstract String getElementName();

}
