/*
 * @author dhudson -
 * Created 20 Nov 2012 : 18:28:56
 */

package com.docfacto.xml;

/**
 * General purpose Dynamic Element. This will be created my the XMLFastParser before any children has been read.
 * 
 * @author dhudson - created 20 Nov 2012
 * @since 2.1
 */
public class GenericDynamicElement extends Element {

    private final String theElementName;

    private boolean isEmptyElement;

    /**
     * Create a new instance of <code>GenericDynamicElement</code>.
     * 
     * @param elementName
     */
    public GenericDynamicElement(String elementName) {
        super(null);
        theElementName = elementName;
    }

    /**
     * Create a new instance of <code>GenericDynamicElement</code>.
     * 
     * @param elementName
     * @param value
     */
    public GenericDynamicElement(String elementName,String value) {
        super(value);
        theElementName = elementName;
    }

    /**
     * Check to see if the element is an empty element, like
     * {@literal <entry /> }
     * 
     * @return true if the the element will not have children or value
     * @since 2.1
     */
    public boolean isEmptyElement() {
        return isEmptyElement;
    }

    /**
     * Sets the Empty tag to true
     * 
     * @since 2.1
     */
    public void setEmptyElement() {
        isEmptyElement = true;
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return theElementName;
    }
    
}
