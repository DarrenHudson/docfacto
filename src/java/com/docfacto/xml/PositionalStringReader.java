package com.docfacto.xml;

/**
 * This class will try and keep track on col / line information
 * 
 * @author dhudson - created 6 Jun 2013
 * @since 2.3
 */
public class PositionalStringReader {

    /**
     * End of file marker {@value}
     */
    public static final char EOF = (char)-1;

    // A Mark to rewind to
    private int theMark;

    // The input
    private final char[] theInput;

    // The length of the String
    private final int theLength;

    // the current buffer position
    private int theBufferPos;

    private int theLine;
    private int theCol;

    /**
     * Constructor.
     * 
     * @param string to read
     */
    public PositionalStringReader(String string) {
        theInput = string.toCharArray();
        theLength = theInput.length;
        theBufferPos = 0;
        theMark = 0;
        theLine = 1;
        theCol = 0;
    }

    /**
     * The length of the string supplied
     * 
     * @return the length of the string
     * @since 2.3
     */
    public int getLength() {
        return theLength;
    }

    /**
     * The next character or EOF
     * 
     * @return the next character from the buffer
     * @since 2.3
     */
    public char read() {
        if (theBufferPos==theLength) {
            return EOF;
        }

        char next = theInput[theBufferPos++];

        if (next=='\n') {
            theLine++;
            theCol = 0;
        }
        else {
            theCol++;
        }

        return next;
    }

    /**
     * Return the next character but <b>not</b> moving the position forward one
     * 
     * @return the next character
     * @since 2.3
     */
    public char peek() {
        return theInput[theBufferPos];
    }

    /**
     * Rewind the position to the mark, if the mark has not been set, the the
     * array will be completely rewound to the beginning of the array
     * 
     * @since 2.3
     */
    public void rewindToMark() {
        // Need to see if the is a '\n' and take numbers off line and col
        theCol -= (theMark-theBufferPos);
        theBufferPos = theMark;
    }

    /**
     * Sets both the position and the mark to zero
     * 
     * @since 2.3
     */
    public void rewind() {
        theBufferPos = 0;
        theMark = 0;
        theLine = 0;
        theCol = 0;
    }

    /**
     * Get the current position of the character array
     * 
     * @return the current position of the character array
     * @since 2.3
     */
    public int pos() {
        return theBufferPos;
    }

    /**
     * Set a mark, which is the current position, the mark than then be rewound
     * to.
     * 
     * @since 2.3
     */
    void mark() {
        theMark = theBufferPos;
    }

    /**
     * Returns the number of characters between the current position and the
     * next instance of the input char
     * 
     * @param c scan target
     * @return offset between current position and next instance of target. -1
     * if not found.
     */
    int nextIndexOf(char c) {
        // doesn't handle scanning for surrogates
        for (int i = theBufferPos;i<theLength;i++) {
            if (c==theInput[i])
                return i-theBufferPos;
        }
        return -1;
    }

    /**
     * Return the current line number
     * 
     * @return the current line number
     * @since 2.3
     */
    public int getLineNumber() {
        return theLine;
    }

    /**
     * The current column position
     * 
     * @return the current column position
     * @since 2.3
     */
    public int getColumn() {
        return theCol;
    }
}
