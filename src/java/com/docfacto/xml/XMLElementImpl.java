/*
 * @author dhudson -
 * Created 5 Sep 2012 : 12:17:59
 */

package com.docfacto.xml;

/**
 * Simple class to encapsulate an XML element
 * 
 * @author dhudson - created 5 Sep 2012
 * @since 2.0
 */
public class XMLElementImpl extends Element implements XMLElement {

    private final String theElementName;

    /**
     * Create a new instance of <code>XMLElementImpl</code>. As the element is
     * given a value, it is a simple element and can not have any children.
     * 
     * @param elementName the element name
     * @param value and the value
     */
    public XMLElementImpl(String elementName,String value) {
        super(value);
        theElementName = elementName;
    }

    /**
     * Create a new instance of <code>XMLElementImpl</code> with the element
     * name.
     * 
     * @param elementName name of the element
     */
    public XMLElementImpl(String elementName) {
        this(elementName,null);
    }

    /**
     * @see com.docfacto.xml.Element#getElementName()
     */
    @Override
    public String getElementName() {
        return theElementName;
    }

}
