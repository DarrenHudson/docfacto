package com.docfacto.xml;

/**
 * Interface listing callbacks for the XMLFastParser.
 * 
 * The text will have been trimmed
 *
 * @author dhudson - created 11 Dec 2012
 * @since 2.1
 */
public interface XMLParserHandler {


    /**
     * This method gets triggered at the beginning of the document
     * 
     * @since 2.1
     */
    public void beginDocument();

    /**
     * This method gets triggered at the end of the document
     * 
     * @since 2.1
     */
    public void endDocument();

    /**
     * This method gets triggered when a comment is encountered
     * 
     * @param comment to process, with the start and end tags
     * @since 2.1
     */
    public void processComment(String comment);

    /**
     * This method gets triggered when an end element is encountered.
     * This will not be triggered for empty elements
     * 
     * @param endTag name of the tag
     * @since 2.1
     */
    public void processEndTag(String endTag);

    /**
     * This method gets triggered when a starting tag is encountered. The
     * {@code GenericDynamicElement} will have all of the attributes set if
     * there is any.
     * 
     * If its an empty element, I.E a {@literal <element .. />} then the
     * {@code GenericDynamicElement.isEmptyElement} will return true
     * 
     * @param gdl with any attributes set
     * @since 2.1
     */
    public void processStartTag(GenericDynamicElement gdl);
    
    /**
     * This method gets triggered when text is encountered and is not just white space
     *
     * @param text to process
     * @since 2.1
     */
    public void processText(String text);
    
    /**
     * This method gets triggered when a DOCTYPE is encountered
     * 
     * @param text of the Doc Type
     * @since 2.3
     */
    public void processDocType(String text);
    
    /**
     * This method gets triggered when a Processing Instruction is encountered
     * 
     * @param text of the processing instruction
     * @since 2.3
     */
    public void processProcessingInstruction(String text);
}
