/*
 * @author dhudson -
 * Created 22 May 2012 : 08:25:07
 */

package com.docfacto.xml;

/**
 * Simple interface for XML Elements
 * 
 * @author dhudson - created 22 May 2012
 * @since 2.0
 */
public interface XMLElement {

    /**
     * Return the element in XML notation as a string
     * 
     * @return an XML representation of the element
     * @since 2.0
     */
    public String toXML();

    /**
     * Sets the parent of this element
     * 
     * @param parent
     * @since 2.1
     */
    public void setParent(XMLElement parent);

    /**
     * Return the parent if known
     * 
     * @return the parent element, or null
     * @since 2.1
     */
    public XMLElement getParent();

    /**
     * Check to see if a child of this node has a child element of the given
     * name
     * 
     * @param elementName
     * @return true if one of the children has a element with the given name
     * @since 2.1
     */
    public boolean containsElementWithName(String elementName);

    /**
     * Returns the name of the element, maybe null if a text node
     * 
     * @return the name of the element
     * @since 2.1
     */
    public String getElementName();
}
