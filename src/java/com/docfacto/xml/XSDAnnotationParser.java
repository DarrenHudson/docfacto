package com.docfacto.xml;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

import com.sun.xml.xsom.parser.AnnotationContext;
import com.sun.xml.xsom.parser.AnnotationParser;

/*
 * @author dhudson -
 * Created 11 Apr 2012 : 13:51:30
 */

/**
 * XSD Annotation Parsers for XSD's. Had real problems getting the Java Default
 * one to work
 * 
 * @author dhudson - created 8 Jul 2012
 * @since 1.4
 */
public class XSDAnnotationParser extends AnnotationParser {

    /**
     * Builder for the Annontation
     */
    private final StringBuilder documentation = new StringBuilder();

    /**
     * Create a new instance of <code>XSDAnnotationParser</code>.
     */
    public XSDAnnotationParser() {
    }

    /**
     * @see com.sun.xml.xsom.parser.AnnotationParser#getContentHandler(com.sun.xml.xsom.parser.AnnotationContext,
     * java.lang.String, org.xml.sax.ErrorHandler, org.xml.sax.EntityResolver)
     */
    @Override
    public ContentHandler getContentHandler(AnnotationContext context,
    String parentElementName,ErrorHandler errorHandler,
    EntityResolver entityResolver) {
        return new ContentHandler() {
            private boolean parsingDocumentation = false;

            /**
             * @see org.xml.sax.ContentHandler#characters(char[], int, int)
             */
            @Override
            public void characters(char[] ch,int start,int length)
            throws SAXException {
                if (parsingDocumentation) {
                    documentation.append(ch,start,length);
                }
            }

            /**
             * @see org.xml.sax.ContentHandler#endElement(java.lang.String,
             * java.lang.String, java.lang.String)
             */
            @Override
            public void endElement(String uri,String localName,String name)
            throws SAXException {
                if (localName.equals("documentation")) {
                    parsingDocumentation = false;
                }
            }

            /**
             * @see org.xml.sax.ContentHandler#startElement(java.lang.String,
             * java.lang.String, java.lang.String, org.xml.sax.Attributes)
             */
            @Override
            public void startElement(String uri,String localName,String name,
            Attributes atts) throws SAXException {
                if (localName.equals("documentation")) {
                    parsingDocumentation = true;
                }
            }

            /**
             * @see org.xml.sax.ContentHandler#endDocument()
             */
            @Override
            public void endDocument() throws SAXException {
            }

            /**
             * @see org.xml.sax.ContentHandler#endPrefixMapping(java.lang.String)
             */
            @Override
            public void endPrefixMapping(String arg0) throws SAXException {
            }

            /**
             * @see org.xml.sax.ContentHandler#ignorableWhitespace(char[], int,
             * int)
             */
            @Override
            public void ignorableWhitespace(char[] arg0,int arg1,int arg2)
            throws SAXException {
            }

            /**
             * @see org.xml.sax.ContentHandler#processingInstruction(java.lang.String,
             * java.lang.String)
             */
            @Override
            public void processingInstruction(String arg0,String arg1)
            throws SAXException {
            }

            /**
             * @see org.xml.sax.ContentHandler#setDocumentLocator(org.xml.sax.Locator)
             */
            @Override
            public void setDocumentLocator(Locator arg0) {
            }

            /**
             * @see org.xml.sax.ContentHandler#skippedEntity(java.lang.String)
             */
            @Override
            public void skippedEntity(String arg0) throws SAXException {
            }

            /**
             * @see org.xml.sax.ContentHandler#startDocument()
             */
            @Override
            public void startDocument() throws SAXException {
            }

            /**
             * @see org.xml.sax.ContentHandler#startPrefixMapping(java.lang.String,
             * java.lang.String)
             */
            @Override
            public void startPrefixMapping(String arg0,String arg1)
            throws SAXException {
            }
        };
    }

    /**
     * @see com.sun.xml.xsom.parser.AnnotationParser#getResult(java.lang.Object)
     */
    @Override
    public Object getResult(Object existing) {
        return documentation.toString().trim();
    }

}
