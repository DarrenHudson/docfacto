package com.docfacto.xml;

import com.docfacto.common.NameValuePair;

/**
 * Character reader with an understanding of XML formats.
 * 
 * The reason for using our own reader / parser is for speed and it is able to
 * be lax, which is required for JavaDoc / html.
 * 
 * Tag names and attribute names will always be lower case
 * 
 * @author dhudson - created 11 Dec 2012
 * @since 2.1
 */
public class XMLStringReader {

    private final PositionalStringReader theReader;
    private final String theXML;

    /**
     * End of file marker {@value}
     */
    public static final char EOF = (char)-1;

    /**
     * Create a new instance of <code>XMLStringReader</code>.
     * 
     * @param xml to process
     * @since 2.3
     */
    public XMLStringReader(String xml) {
        theReader = new PositionalStringReader(xml);
        theXML = xml;
    }

    /**
     * Get the current position of the character array
     * 
     * @return the current position of the character array
     * @since 2.1
     */
    public int pos() {
        return theReader.pos();
    }

    /**
     * Set a mark, which is the current position, the mark than then be rewound
     * to.
     * 
     * @since 2.1
     */
    void mark() {
        theReader.mark();
    }

    /**
     * Rewind the position to the mark, if the mark has not been set, the the
     * array will be completely rewound to the beginning of the array
     * 
     * @since 2.0
     */
    void rewindToMark() {
        theReader.rewindToMark();
    }

    /**
     * Sets both the position and the mark to zero
     * 
     * @since 2.1
     */
    void rewind() {
        theReader.rewind();
    }

    /**
     * Return the next character moving the position forward one
     * 
     * @return the next character
     * @since 2.1
     */
    public char read() {
        return theReader.read();
    }

    /**
     * Return the next character but <b>not</b> moving the position forward one
     * 
     * @return the next character
     * @since 2.1
     */
    public char peek() {
        return theReader.peek();
    }

    /**
     * Scan from the current position looking for the next {@literal >} and
     * return the name of the end tag
     * 
     * @return the name of the end tag
     * @since 2.1
     */
    public String returnEndTag() {
        StringBuilder builder = new StringBuilder(10);
        char c;
        for (int i = theReader.pos();i<theReader.getLength();i++) {
            c = theReader.read();
            if (c=='>') {
                return builder.toString().toLowerCase();
            }
            else {
                builder.append(c);
            }
        }

        return null;
    }

    /**
     * Return a {@code GeneralDyanmicElement} with its name and attributes set.
     * This can return null id the element can not be processed.
     * 
     * @return a {@code GeneralDyanmicElement} with its name and attributes, if
     * any, set
     * @since 2.1
     */
    public GenericDynamicElement returnStartTag() {
        final int index = theReader.nextIndexOf('>');

        if (index!=-1) {
            char c;
            StringBuilder tagName = new StringBuilder(20);
            // Bounds of the start element..
            for (int i = 0;i<index;i++) {
                c = theReader.read();
                if (Character.isWhitespace(c)) {
                    final GenericDynamicElement element =
                        new GenericDynamicElement(tagName.toString()
                            .toLowerCase());
                    try {
                        processAttributes(element);
                    }
                    catch (final Throwable t) {
                        System.out
                            .println("XMLStringReader: Unable to process : "+
                                theXML);
                    }
                    return element;
                }
                else {
                    tagName.append(c);
                }
            }

            // Read the last >
            theReader.read();

            // No attributes
            return new GenericDynamicElement(tagName.toString().toLowerCase());
        }

        return null;
    }

    /**
     * Process the start tag and assign any attributes to the node
     * 
     * @docfacto.todo Replace this method with
     * {@code TagletAttributes.parse(CharSequence)}
     * @param element to assign the attributes
     * @since 2.1
     */
    private void processAttributes(GenericDynamicElement element) {
        StringBuilder builder = new StringBuilder(100);
        NameValuePair attr = null;
        char quoteChar = ' ';
        int quoteIndex = 0;
        char c;

        while ((c = theReader.read())!='>') {

            switch (c) {
            case '\'':
            case '"':
                // We are value parsing
                if (quoteIndex==0) {
                    quoteChar = c;
                    quoteIndex = 1;
                }
                else {
                    if (c==quoteChar) {
                        // We are at the end of the value
                        attr.setValue(builder.toString().trim());

                        // Add the attribute to the element
                        element.addAttribute(attr);

                        // Reset the builder
                        builder = new StringBuilder(100);
                        quoteIndex = 0;
                    }
                }
                break;

            case '=':
                // Build a new attr
                if (quoteIndex==0) {
                    attr =
                        new NameValuePair(builder.toString().trim()
                            .toLowerCase());
                    builder = new StringBuilder(100);
                    quoteIndex = 0;
                    break;
                }
                // Again watch the fall through here...

            case '/':
                // Its an terminating start tag like <entry />
                if (quoteIndex==0) {
                    // We are no in a value of an attribute, so its empty
                    element.setEmptyElement();
                    break;
                    // return;
                }
                // watch the fall through there, there is a / in the attribute
                // value..

            default:
                builder.append(c);
                break;

            }
        }
    }

    /**
     * Return the current line that the reader is on
     * 
     * @return the current line
     * @since 2.1
     */
    public int getCurrentLine() {
        return theReader.getLineNumber();
    }

    /**
     * Return the current column
     * 
     * @return the current column within the line
     * @since 2.1
     */
    public int getCurrentCol() {
        return theReader.getColumn();
    }

    /**
     * The XML String that is being processed
     * 
     * @return the XML that is being read
     * @since 2.4
     */
    public String getXML() {
        return theXML;
    }

}
