package com.docfacto.xsd2jsonschema;

/**
 * A list of primitive types as of draft v4
 *
 * @author dhudson - created 16 Jan 2014
 * @since 2.5
 */
public enum PrimitiveTypes {

    /**
     * A JSON array.
     */
    ARRAY,
    /**
     * A JSON boolean.
     */
    BOOLEAN,
    /**
     * A JSON number without a fraction or exponent part.
     */
    INTERGER,
    /**
     * Any JSON number. Number includes integer.
     */
    NUMBER,
    /**
     * The JSON null value.
     */
    NULL,
    /**
     * A JSON object.
     */
    OBJECT,
    /**
     * A JSON string.
     */
    STRING
    
    
}
