package com.docfacto.xsd2jsonschema;

import com.docfacto.common.DocfactoException;
import com.docfacto.xsd2common.XSDArgumentParser;

/**
 * Command line launcher for the XSD to JSON Schema tool
 * 
 * @author dhudson - created 16 Jan 2014
 * @since 2.5
 */
public class XSD2JSON {

    /**
     * Constructor.
     * 
     * @param parser the validated argument parser
     */
    public XSD2JSON(XSDArgumentParser parser) {

        final XSD2JSONProcessor processor = new XSD2JSONProcessor(parser);

        try {
            processor.process();
        }
        catch (final Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Command line launcher for XSD2DITA
     * 
     * @param args first argument is the file to process, second argument is the
     * output folder
     * @since 2.5
     */
    public static void main(String[] args) {
        if (args.length<4||args.length>10) {
            usage();
            System.exit(1);
        }

        try {
            final XSDArgumentParser parser = new XSDArgumentParser(args);
            parser.validate();
            new XSD2JSON(parser);
        }
        catch (final DocfactoException ex) {
            System.out.println("Arguments not valid cause ["+ex.getLocalizedMessage()+"]");
            usage();
        }
    }

    /**
     * Prints the options for this program
     * 
     * @since 2.5
     */
    private static final void usage() {
        System.out.println("Usage:");
        System.out.println("\t-xsd <filename>    : XSD to process");
        System.out
            .println("\t-rootnode <name>   : Root node name can be used, if the root node is not the first element in the schema, or you require a subset of the schema");
        System.out
            .println("\t-namespace <namespace> of the root node if not the default null namespace");
        System.out.println("\t-title <title>     : Of the schema");
        System.out
            .println("\t-output <filename> : Name of the file to generate");
        System.out.println();
    }
}
