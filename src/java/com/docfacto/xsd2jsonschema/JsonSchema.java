package com.docfacto.xsd2jsonschema;

import java.util.HashMap;
import java.util.LinkedHashSet;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.xml.namespace.QName;

import com.docfacto.xsdtree.AttributeTreeNode;
import com.docfacto.xsdtree.ComplexTreeNode;
import com.docfacto.xsdtree.ElementTreeNode;
import com.docfacto.xsdtree.SchemaTreeNode;
import com.docfacto.xsdtree.XmlRootTreeNode;
import com.sun.xml.xsom.impl.Const;

/**
 * Class to represent a JSON schema
 * 
 * @author dhudson - created 16 Jan 2014
 * @since 2.5
 */
public class JsonSchema {

    private final HashMap<QName,SchemaTreeNode> theTypes;

    public JsonSchema(HashMap<QName,SchemaTreeNode> types) {
        theTypes = types;
    }

    public JsonObjectBuilder process(XmlRootTreeNode root) {
        JsonObjectBuilder schema = Json.createObjectBuilder();

        if (!root.getQName().getNamespaceURI().isEmpty()) {
            schema.add(JsonSchemaConstants.ID_KEY_WORD,root.getQName()
                .getNamespaceURI());
        }

        schema.add(JsonSchemaConstants.SCHEMA_KEY_WORD,
            JsonSchemaConstants.SCHEMA_DRAFT4_VALUE);

        if (root.hasAnnotation()) {
            schema.add(JsonSchemaConstants.DESCRIPTION_KEY_WORD,
                root.getAnnotation());
        }

        schema.add(JsonSchemaConstants.SCHEMA_KEY_WORD,
            JsonSchemaConstants.SCHEMA_DRAFT4_VALUE);

        if (root.hasAnnotation()) {
            schema.add(JsonSchemaConstants.DESCRIPTION_KEY_WORD,
                root.getAnnotation());
        }

        schema.add(JsonSchemaConstants.TYPE_KEY_WORD,
            JsonSchemaConstants.OBJECT_KEY_WORD);

        ComplexTreeNode child = (ComplexTreeNode)root.getChildAt(0);



        if (child.isRequired()) {
            JsonArrayBuilder required = Json.createArrayBuilder();
            required.add(child.getNodeName());
            schema.add(JsonSchemaConstants.REQUIRED_KEY_WORD,required);
        }

        JsonComplex complex = new JsonComplex(child);

        // The complex root node will be a reference
        schema.add(JsonSchemaConstants.PROPERTIES_KEY_WORD,complex.referenceDefinition());

        // Add the definitions.
        JsonObjectBuilder defs = processDefinitions(child);
        
        if(defs != null) {
            schema.add(JsonSchemaConstants.DEFINITIONS_KEY_WORD,defs);
        }
        
        return schema;
    }

    private JsonObjectBuilder processDefinitions(ComplexTreeNode root) {
        LinkedHashSet<QName> definitions = new LinkedHashSet<QName>(5);

        // Lets build a list of definitions
        walk(root,definitions);

        if(definitions.isEmpty()) {
            return null;
        }
        
        // REMOVE:
        System.out.println("I have definitions of ..."+definitions);

        JsonObjectBuilder defs = Json.createObjectBuilder();
        
        for (QName type:definitions) {
            // JsonObjectBuilder def = Json.createObjectBuilder();
            
            SchemaTreeNode typeNode = theTypes.get(type);

            JsonSchemaTreeNode jsonNode = null;

            if (typeNode.isComplexTreeNode()) {
                jsonNode =
                    new JsonComplex(typeNode.asComplexTreeNode());
            }
            else if (typeNode.isElementTreeNode()) {
                jsonNode =
                    new JsonElement(typeNode.asElementTreeNode());
            }
            else if (typeNode.isAttributeTreeNode()) {
                jsonNode = new JsonAttribute(typeNode.asAttributeTreeNode());
            } else if( typeNode.isSimpleTreeNode()) {
                jsonNode = new JsonSimpleType(typeNode.asSimpleType());
            }

            if (jsonNode!=null) {
                defs.add(typeNode.getNodeName(),jsonNode.buildDefinition());
            }
            else {
                System.out.println("I have got a ["+typeNode+
                    "] that I don't know how to handle");
            }
        }
        
        return defs;
    }

    private void walk(SchemaTreeNode treeNode,LinkedHashSet<QName> definitions) {

        if (treeNode.isComplexTreeNode()) {
            final ComplexTreeNode complexNode = treeNode.asComplexTreeNode();

            definitions.add(complexNode.getQName());

            for (final SchemaTreeNode child:complexNode.getSchemaChildren()) {
                if (child.isAttributeTreeNode()) {
                    AttributeTreeNode attr = child.asAttributeTreeNode();
                    QName typeQName = attr.getTypeQName();
                    if (!typeQName.getNamespaceURI().equals(
                        Const.schemaNamespace)) {
                        definitions.add(typeQName);
                    }
                }
                if (child.isElementTreeNode()) {
                    ElementTreeNode element = child.asElementTreeNode();
                    QName typeQName = element.getTypeQName();
                    if (!typeQName.getNamespaceURI().equals(
                        Const.schemaNamespace)) {
                        definitions.add(typeQName);
                    }
                }
                if (child.isComplexTreeNode()) {
                    walk(child,definitions);
                }
            }
        }
    }
}
