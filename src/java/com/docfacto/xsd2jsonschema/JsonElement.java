package com.docfacto.xsd2jsonschema;

import java.util.List;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import com.docfacto.xsdtree.AttributeTreeNode;
import com.docfacto.xsdtree.ElementTreeNode;
import com.docfacto.xsdtree.SchemaTreeNode;

public class JsonElement extends JsonSchemaTreeNode {

    public JsonElement(SchemaTreeNode node) {
        super(node);
    }

    @Override
    JsonObjectBuilder referenceDefinition() {

        ElementTreeNode elementNode = getTreeNode().asElementTreeNode();

        JsonObjectBuilder builder = Json.createObjectBuilder();

        processAnnotation(builder);
        
        
//        if(elementNode.)
//        if (isReferenceType(elementNode.getTypeQName())) {
//            element.add(JsonSchemaConstants.REFERENCE_KEY_WORD,
//                "#/definitions/"+elementNode.getTypeName());
//        }
//        else {
//            processAnnotation(element);
//            element.add(JsonSchemaConstants.TYPE_KEY_WORD,
//                elementNode.getTypeName());
//        }

        return builder;

    }

    @Override
    JsonObjectBuilder buildDefinition() {
        ElementTreeNode elementNode = getTreeNode().asElementTreeNode();

        //REMOVE:
        System.out.println(" --> " +elementNode.getNodeName());
        
        JsonObjectBuilder element = Json.createObjectBuilder();
        
        processAnnotation(element);
        
        if(JsonSchemaUtils.isReferenceType(elementNode.getTypeQName())) {
            // Lets just reference it..
            element.add(JsonSchemaConstants.REFERENCE_KEY_WORD,"#/definitions/"+elementNode.getTypeName());
            return element;
        }
        
        List<AttributeTreeNode> attrs = elementNode.getAttributeChildren();
        
        if(attrs.isEmpty()) {
            // At this type we are a simple element
            element.add(JsonSchemaConstants.TYPE_KEY_WORD,elementNode.getTypeName());
        } else {
            element.add("content","what");
        }
        
        processAttributes(element);
        
        
        return element;
        
    }
}
