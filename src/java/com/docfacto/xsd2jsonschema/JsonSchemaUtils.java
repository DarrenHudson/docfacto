package com.docfacto.xsd2jsonschema;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.xml.namespace.QName;

import com.sun.xml.xsom.impl.Const;

/**
 * Collection of schema utils.
 *
 * @author dhudson - created 21 Jan 2014
 * @since 2.5
 */
public class JsonSchemaUtils {

    /**
     * Check to see if its a xsi name space.
     * 
     * @param typeQName to process
     * @return true if its a xsi data type
     * @since 2.5
     */
    public static boolean isReferenceType(QName typeQName) {
        if (typeQName.getNamespaceURI().equals(Const.schemaNamespace)) {
            return false;
        }

        return true;
    }

    public static String xsdToJsonDataType(String dataType) {
        if(dataType.equals("float") || dataType.equals("double")) {
            return "string";
        }
        
        return dataType;
    }
    
    /**
     * Produce an Enum schema definition.
     * 
     * {@docfacto.note this will produce "enum
     * 
     * @param values array of possible enum values
     * @return a Json Schema representation of an {@code enum}
     * @since 2.5
     */
    public static JsonArrayBuilder generateJsonList(String[] values) {

        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

        for (String value:values) {
            arrayBuilder.add(value);
        }

        return arrayBuilder;
    }
}
