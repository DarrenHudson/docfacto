package com.docfacto.xsd2jsonschema;

import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;

import com.docfacto.xml.XMLRange;
import com.docfacto.xsdtree.ComplexTreeNode;
import com.docfacto.xsdtree.SchemaTreeNode;

public class JsonComplex extends JsonSchemaTreeNode {

    public JsonComplex(SchemaTreeNode node) {
        super(node);
    }

    @Override
    JsonObjectBuilder referenceDefinition() {
        ComplexTreeNode node = (ComplexTreeNode)getTreeNode();

        XMLRange range = node.getXMLRange();

        JsonObjectBuilder builder = Json.createObjectBuilder();

        if (range.getMinOccurs()>0&&
            (range.isMaxUnbounded()||range.getMaxOccurs()>1)) {
            builder.add(JsonSchemaConstants.TYPE_KEY_WORD,
                JsonSchemaConstants.ARRAY_TYPE_KEY_WORD);
            builder.add(JsonSchemaConstants.MIN_ITEMS_KEY_WORD,
                range.getMinOccurs());
            if (!range.isMaxUnbounded()) {
                builder.add(JsonSchemaConstants.MAX_ITEMS_KEY_WORD,
                    range.getMaxOccurs());
            }

            JsonObjectBuilder items = Json.createObjectBuilder();
            items.add(JsonSchemaConstants.REFERENCE_KEY_WORD,
                "#/definitions/"+node.getNodeName());

            builder.add(JsonSchemaConstants.ITEMS_KEY_WORD,items);
        }
        else {
            builder.add(JsonSchemaConstants.TYPE_KEY_WORD,
                JsonSchemaConstants.OBJECT_KEY_WORD);
            builder.add(JsonSchemaConstants.REFERENCE_KEY_WORD,
                "#/definitions/"+node.getNodeName());
        }

        return builder;
    }

    JsonObjectBuilder buildDefinition() {
        ComplexTreeNode node = (ComplexTreeNode)getTreeNode();

        XMLRange range = node.getXMLRange();

        JsonObjectBuilder builder = Json.createObjectBuilder();

        if (range.getMinOccurs()>0&&
            (range.isMaxUnbounded()||range.getMaxOccurs()>1)) {
            builder.add(JsonSchemaConstants.TYPE_KEY_WORD,
                JsonSchemaConstants.ARRAY_TYPE_KEY_WORD);
            builder.add(JsonSchemaConstants.MIN_ITEMS_KEY_WORD,
                range.getMinOccurs());
            if (!range.isMaxUnbounded()) {
                builder.add(JsonSchemaConstants.MAX_ITEMS_KEY_WORD,
                    range.getMaxOccurs());
            }
        }
        else {
            builder.add(JsonSchemaConstants.TYPE_KEY_WORD,
                JsonSchemaConstants.OBJECT_KEY_WORD);
        }

        processAnnotation(builder);

        JsonObjectBuilder properties = Json.createObjectBuilder();

        List<String> required = new ArrayList<String>(5);

        required.addAll(processAttributes(properties));

        if (range.isMaxUnbounded()) {
            // This now needs to be an array
        }

        for (SchemaTreeNode child:node.getSchemaChildren()) {
            if (child.isComplexTreeNode()) {

                JsonComplex jsonComplex =
                    new JsonComplex(child.asComplexTreeNode());

                // This should just be a reference
                properties.add(child.getNodeName(),
                    jsonComplex.referenceDefinition());
            }

            if (child.isElementTreeNode()) {

                JsonElement jsonElement =
                    new JsonElement(child.asElementTreeNode());

                properties.add(child.getNodeName(),
                    jsonElement.buildDefinition());
            }
        }

        builder.add(JsonSchemaConstants.PROPERTIES_KEY_WORD,properties);

        if (!required.isEmpty()) {
            JsonArrayBuilder whatsRequired = Json.createArrayBuilder();
            for (String attrName:required) {
                whatsRequired.add(attrName);
            }

            builder.add(JsonSchemaConstants.REQUIRED_KEY_WORD,whatsRequired);
        }

        return builder;
    }
}
