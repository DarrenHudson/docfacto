package com.docfacto.xsd2jsonschema;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import com.docfacto.xsdtree.SchemaTreeNode;
import com.docfacto.xsdtree.SimpleType;

public class JsonSimpleType extends JsonSchemaTreeNode {

    public JsonSimpleType(SchemaTreeNode node) {
        super(node);
        // TODO Auto-generated constructor stub
    }

    @Override
    JsonObjectBuilder referenceDefinition() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    JsonObjectBuilder buildDefinition() {
        SimpleType simpleType = getTreeNode().asSimpleType();

        JsonObjectBuilder builder = Json.createObjectBuilder();

        // Lets put the description in
        processAnnotation(builder);

        if (simpleType.hasEnumeration()) {
            builder.add(JsonSchemaConstants.ENUM_TYPE,
                JsonSchemaUtils.generateJsonList(simpleType.getEnumeration()));
            return builder;
        }
        
        builder.add(JsonSchemaConstants.TYPE_KEY_WORD,simpleType.getSimpleType().getName());
        
        // If we have restrictions, the add them now..
        
        if(simpleType.hasPattern()) {
            builder.add(JsonSchemaConstants.PATTERN_KEY_WORD,simpleType.getPattern());
        }
        
        if(simpleType.hasMinLength()) {
            builder.add(JsonSchemaConstants.MIN_LENGTH_KEY_WORD,simpleType.getMinLength());
        }
        
        if(simpleType.hasMaxLength()) {
            builder.add(JsonSchemaConstants.MAX_LENGTH_KEY_WORD,simpleType.getMaxLength());
        }

        if(simpleType.hasMinValue()) {
            builder.add(JsonSchemaConstants.MINIMUM_KEY_WORD,simpleType.getMinValue());
        }
        
        if(simpleType.hasMaxValue()) {
            builder.add(JsonSchemaConstants.MAXIMUM_KEY_WORD,simpleType.getMaxValue());
        }

        return builder;
    }

}
