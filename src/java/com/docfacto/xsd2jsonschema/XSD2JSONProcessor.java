package com.docfacto.xsd2jsonschema;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

import com.docfacto.common.DocfactoException;
import com.docfacto.xsd2common.XSDArgumentParser;
import com.docfacto.xsdtree.XSDTreeTraverser;
import com.docfacto.xsdtree.XmlRootTreeNode;

/**
 * Convert an XSD to a json schema.
 * 
 * {@docfacto.system http://json-schema.org }
 * 
 * At the time of writing, the draft is v4, so this will be a v4 compliant
 * generator
 * 
 * @author dhudson - created 16 Jan 2014
 * @since 2.5
 */
public class XSD2JSONProcessor {

    private final XSDArgumentParser theArgParser;

    /**
     * Constructor.
     * 
     * @param parser Argument parser
     */
    public XSD2JSONProcessor(XSDArgumentParser parser) {
        theArgParser = parser;
    }

    /**
     * Process the schema, and produce a JSON schema
     * 
     * @throws DocfactoException if unable to create a traverser
     * @since 2.5
     */
    public void process() throws DocfactoException {
        final XSDTreeTraverser traverser = new XSDTreeTraverser(theArgParser);
        
        //traverser.setDebug(true);

        // Parse the file we are interested in
        traverser.visit(new File(theArgParser.getXSD()));

        process(traverser);
    }

    /**
     * Generate an SVG schematic from the provided XSDTree traverser
     * 
     * @param traverser to process
     * @throws DocfactoException if unable to create a JSON-Schema object
     * @since 2.0
     */
    public void process(XSDTreeTraverser traverser)
    throws DocfactoException {

        // UDCProcessor.registerProduct("XSD2JSON",null,null);

        XmlRootTreeNode root = traverser.getRootNode();

        // JsonSchema schema = new JsonSchema(traverser.getTypes());

        Map<String,Object> jsonProperties = new HashMap<String,Object>(1);
        jsonProperties.put(JsonGenerator.PRETTY_PRINTING,true);
        JsonWriterFactory writerFactory =
            Json.createWriterFactory(jsonProperties);
        JsonWriter writer = writerFactory.createWriter(System.out);
        //writer.write(schema.process(root).build());

    }
}
