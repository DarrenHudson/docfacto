package com.docfacto.xsd2jsonschema;

/**
 * A collection of schema utils for JSON Schema
 * 
 * @author dhudson - created 16 Jan 2014
 * @since 2.5
 */
public interface JsonSchemaConstants {

    /**
     * {@value}
     */
    public static final String SCHEMA_DRAFT4_VALUE =
        "http://json-schema.org/draft-04/schema#";

    /**
     * {@value}
     */
    public static final String SCHEMA_KEY_WORD = "$schema";

    /**
     * IANA MIME Type {@value}
     */
    public static final String MIME_TYPE = "schema+json";

    /**
     *{@value}
     */
    public static final String REFERENCE_KEY_WORD = "$ref";

    /**
     * {@value}
     */
    public static final String DEFINITIONS_KEY_WORD = "definitions";

    /**
     * {@value}
     */
    public static final String TYPE_KEY_WORD = "type";

    /**
     * {@value}
     */
    public static final String ARRAY_TYPE_KEY_WORD = "array";

    /**
     * {@value}
     */
    public static final String ITEMS_KEY_WORD = "items";

    /**
     * {@value}
     */
    public static final String PROPERTIES_KEY_WORD = "properties";

    /**
     * {@value}
     */
    public static final String REQUIRED_KEY_WORD = "required";

    /**
     * {@value}
     */
    public static final String OBJECT_KEY_WORD = "object";

    /**
     * {@value}
     */
    public static final String DESCRIPTION_KEY_WORD = "description";

    /**
     * {@value}
     */
    public static final String ID_KEY_WORD = "id";

    /**
     * {@value}
     */
    public static final String DEFAULT_KEY_WORD = "default";

    /**
     * {@value}
     */
    public static final String ENUM_TYPE = "enum";

    /**
     * {@value}
     */
    public static final String PATTERN_KEY_WORD = "pattern";

    /**
     * {@value}
     */
    public static final String MIN_LENGTH_KEY_WORD = "minLength";

    /**
     * {@value}
     */
    public static final String MAX_LENGTH_KEY_WORD = "maxLength";

    /**
     * {@value}
     */
    public static final String MAX_ITEMS_KEY_WORD = "maxItems";

    /**
     * {@value}
     */
    public static final String MIN_ITEMS_KEY_WORD = "minItems";

    /**
     * {@value}
     */
    public static final String MINIMUM_KEY_WORD = "minimum";

    /**
     * {@value}
     */
    public static final String MAXIMUM_KEY_WORD = "maximum";

}
