package com.docfacto.xsd2jsonschema;

import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;

import com.docfacto.xsdtree.AttributeTreeNode;
import com.docfacto.xsdtree.SchemaTreeNode;

// http://www.bramstein.com/projects/xsltjson/
// http://spacetelescope.github.io/understanding-json-schema/structuring.html#structuring
// {"product":{"@attributes":{"name":"GRABIT"},"address":"127.0.0.1","restriction":"0","expiry-date":"2014-12-31Z"}}

public abstract class JsonSchemaTreeNode {

    private final SchemaTreeNode theNode;

    public JsonSchemaTreeNode(SchemaTreeNode node) {
        theNode = node;
    }

    abstract JsonObjectBuilder referenceDefinition();

    abstract JsonObjectBuilder buildDefinition();

    List<String> processAttributes(JsonObjectBuilder properties) {
        List<String> required = new ArrayList<String>(1);

        List<AttributeTreeNode> attrs = theNode.getAttributeChildren();
        for (AttributeTreeNode attr:attrs) {
            JsonObjectBuilder jsonAttribute = Json.createObjectBuilder();

            if (attr.hasAnnotation()) {
                jsonAttribute.add(JsonSchemaConstants.DESCRIPTION_KEY_WORD,
                    attr.getAnnotation());
            }

            if (attr.isEnum()) {
                JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
                for (String value:attr.getEnums()) {
                    arrayBuilder.add(value);
                }

                JsonObjectBuilder jsonEnum = Json.createObjectBuilder();
                jsonEnum.add(JsonSchemaConstants.ENUM_TYPE,arrayBuilder);

                properties.add(JsonSchemaConstants.TYPE_KEY_WORD,jsonEnum);
            }
            else {

                if (JsonSchemaUtils.isReferenceType(attr.getTypeQName())) {
                    jsonAttribute.add(JsonSchemaConstants.REFERENCE_KEY_WORD,
                        "#/definitions/"+attr.getTypeName());
                }
                else {
                    jsonAttribute.add(JsonSchemaConstants.TYPE_KEY_WORD,
                        attr.getTypeName());
                }
            }

            if (attr.hasDefaultValue()) {
                jsonAttribute.add(JsonSchemaConstants.DEFAULT_KEY_WORD,
                    attr.getDefaultValue());
            }

            // Make this a config option
            properties.add("@"+attr.getNodeName(),jsonAttribute);

            if (attr.isRequired()) {
                required.add(attr.getNodeName());
            }
        }

        return required;
    }

    void processAnnotation(JsonObjectBuilder builder) {
        if (theNode.hasAnnotation()) {
            builder.add(JsonSchemaConstants.DESCRIPTION_KEY_WORD,
                theNode.getAnnotation());
        }
    }

    SchemaTreeNode getTreeNode() {
        return theNode;
    }

}
