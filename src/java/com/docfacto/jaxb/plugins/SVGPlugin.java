/*
 * @author dhudson -
 * Created 23 Aug 2012 : 11:46:25
 */

package com.docfacto.jaxb.plugins;

import java.util.Collections;
import java.util.List;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;

import com.docfacto.common.DocfactoException;
import com.docfacto.xsd2common.XSDArgumentParser;
import com.docfacto.xsd2svg.XSD2SVGProcessor;
import com.sun.tools.xjc.Options;
import com.sun.tools.xjc.Plugin;
import com.sun.tools.xjc.model.CCustomizations;
import com.sun.tools.xjc.model.CPropertyInfo;
import com.sun.tools.xjc.outline.ClassOutline;
import com.sun.tools.xjc.outline.FieldOutline;
import com.sun.tools.xjc.outline.Outline;
import com.sun.xml.xsom.XSSchemaSet;

/**
 * XSD2SVG JAXB Plugin
 *
 * @author dhudson - created 10 Mar 2013
 * @since 1.4
 */
public class SVGPlugin extends Plugin {

    /**
     * Name of Option to enable this plugin
     */
    private static final String OPTION_NAME = "Xsvg-generator";

    private static final String NAMESPACE = "http://www.docfacto.com";

    private XSDArgumentParser theParser;

    /**
     * Constructor.
     */
    public SVGPlugin() {
        theParser = new XSDArgumentParser();
    }

    /**
     * @see com.sun.tools.xjc.Plugin#getOptionName()
     */
    @Override
    public String getOptionName() {
        return OPTION_NAME;
    }

    @Override
    public String getUsage() {
        return "  -"+
            OPTION_NAME+
            "    : enable the generation of SVG file";
    }

    /**
     * @see com.sun.tools.xjc.Plugin#getCustomizationURIs()
     */
    @Override
    public List<String> getCustomizationURIs() {
        return Collections.singletonList(NAMESPACE);
    }

    /**
     * @see com.sun.tools.xjc.Plugin#isCustomizationTagName(java.lang.String,
     * java.lang.String)
     */
    @Override
    public boolean isCustomizationTagName(String nsUri,String localName) {
        if (NAMESPACE.equals(nsUri)) {
            return theParser.isTagAllowed(localName);
        }
        return false;
    }

    /**
     * @see com.sun.tools.xjc.Plugin#run(com.sun.tools.xjc.outline.Outline,
     * com.sun.tools.xjc.Options, org.xml.sax.ErrorHandler)
     */
    @Override
    public boolean run(Outline outline,Options opt,ErrorHandler errorHandler)
    throws SAXException {
        final CCustomizations settings = outline.getModel().getCustomizations();

        theParser.parse(settings);

        try {
            theParser.validate();
        }
        catch (DocfactoException ex) {
            System.err.println(ex.getMessage());
            return false;
        }

        // For all Classes generated
        for (final ClassOutline co:outline.getClasses()) {

            // check all Fields in Class
            for (final FieldOutline f:co.getDeclaredFields()) {
                final CPropertyInfo fieldInfo = f.getPropertyInfo();

                final XSSchemaSet schemaSet =
                    fieldInfo.getSchemaComponent().getRoot();

                // schemaSet.
                final XSD2SVGProcessor processor =
                    new XSD2SVGProcessor(theParser);

                try {
                    processor.process(schemaSet);
                }
                catch (final DocfactoException ex) {
                    ex.printStackTrace();
                    return false;
                }

                // Only need to do this once, but due to the way that this thing
                // works, need to get at a field
                return true;
            }

        }
        return true;
    }

}
