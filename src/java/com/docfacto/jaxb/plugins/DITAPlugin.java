/*
 * @author dhudson -
 * Created 10 Jun 2012 : 10:09:19
 */

package com.docfacto.jaxb.plugins;

import java.util.Collections;
import java.util.List;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;

import com.docfacto.common.DocfactoException;
import com.docfacto.xsd2common.XSDArgumentParser;
import com.docfacto.xsd2dita.XSD2DITAProcessor;
import com.sun.tools.xjc.Options;
import com.sun.tools.xjc.Plugin;
import com.sun.tools.xjc.model.CCustomizations;
import com.sun.tools.xjc.model.CPropertyInfo;
import com.sun.tools.xjc.outline.ClassOutline;
import com.sun.tools.xjc.outline.FieldOutline;
import com.sun.tools.xjc.outline.Outline;
import com.sun.xml.xsom.XSSchemaSet;

/**
 * JXAB Plugin to create DITA file and SVG files if required
 * 
 * @author dhudson - created 11 Jun 2012
 * @since 2.0
 */
public class DITAPlugin extends Plugin {

    /**
     * Name of Option to enable this plugin
     */
    private static final String OPTION_NAME = "Xdita-generator";

    private static final String NAMESPACE = "http://www.docfacto.com";

    private XSDArgumentParser theParser;

    /**
     * Constructor.
     */
    public DITAPlugin() {
        theParser = new XSDArgumentParser();
    }

    /**
     * @see com.sun.tools.xjc.Plugin#getOptionName()
     */
    @Override
    public String getOptionName() {
        return OPTION_NAME;
    }

    /**
     * @see com.sun.tools.xjc.Plugin#getUsage()
     */
    @Override
    public String getUsage() {
        return "  -"+
            OPTION_NAME+
            "    : enable the generation of DITA file";
    }

    /**
     * @see com.sun.tools.xjc.Plugin#getCustomizationURIs()
     */
    @Override
    public List<String> getCustomizationURIs() {
        return Collections.singletonList(NAMESPACE);
    }

    /**
     * @see com.sun.tools.xjc.Plugin#isCustomizationTagName(java.lang.String,
     * java.lang.String)
     */
    @Override
    public boolean isCustomizationTagName(String nsUri,String localName) {
        if (NAMESPACE.equals(nsUri)) {
            return theParser.isTagAllowed(localName);
        }
        return false;
    }

    /**
     * @see com.sun.tools.xjc.Plugin#run(com.sun.tools.xjc.outline.Outline,
     * com.sun.tools.xjc.Options, org.xml.sax.ErrorHandler)
     */
    @Override
    public boolean run(Outline outline,Options opt,ErrorHandler errorHandler)
    throws SAXException {

        final CCustomizations settings = outline.getModel().getCustomizations();

        theParser.parse(settings);

        try {
            theParser.validate();
        }
        catch (DocfactoException ex) {
            System.err.println(ex.getMessage());
            return false;
        }

        // if (opt.debugMode) {
        // isDebugging = true;
        // }

        // For all Classes generated
        for (final ClassOutline co:outline.getClasses()) {

            // check all Fields in Class
            for (final FieldOutline f:co.getDeclaredFields()) {
                final CPropertyInfo fieldInfo = f.getPropertyInfo();

                final XSSchemaSet schemaSet =
                    fieldInfo.getSchemaComponent().getRoot();

                // schemaSet.
                final XSD2DITAProcessor processor =
                    new XSD2DITAProcessor(theParser);

                try {
                    processor.process(schemaSet);
                }
                catch (final DocfactoException ex) {
                    ex.printStackTrace();
                    return false;
                }

                // Only need to do this once, but due to the way that this thing
                // works, need to get at a field
                return true;
            }

        }
        return true;
    }

}
