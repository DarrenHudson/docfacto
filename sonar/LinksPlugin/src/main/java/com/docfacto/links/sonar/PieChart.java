package com.docfacto.links.sonar;

import java.awt.Color;
import java.awt.Font;

import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.plot.Plot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.RectangleInsets;
import org.sonar.api.charts.AbstractChart;
import org.sonar.api.charts.ChartParameters;

/**
 * A pie chart to show how much of the code has documentation and how much of the code
 * does not have documentation.
 * <p>
 * The pie chart has two sections, a red section to indicate the percentage of the code
 * which does not have documentation, and a green section to indicate the percentage of the
 * code which does have documentation
 * </p>
 * 
 * @author damonli - created Jun 5, 2013
 * @since 2.4.1
 */
public class PieChart extends AbstractChart {

    /**
     * The message shown when no data is available for the pie chart
     */
    private static final String NO_DATA_MESSAGE = "No data available";

    /**
     * The parameter id for the values of the data set from the given chart
     * parameters
     */
    public static final String VALUE_ID = "v";

    /**
     * The color of the green section
     */
    private static final Color GREEN_PIE_COLOR = Color.decode("0x33CC00");
    /**
     * The color of the red section
     */
    private static final Color RED_PIE_COLOR = Color.decode("0xFF0000");

    /**
     * The colors for the sections of the pie chart
     */
    private static final Color[] COLORS = {GREEN_PIE_COLOR,RED_PIE_COLOR};

    /**
     * @see org.sonar.api.charts.Chart#getKey()
     */
    public String getKey() {
        return "pieChart";
    }

    /**
     * @see org.sonar.api.charts.AbstractChart#getPlot(org.sonar.api.charts.ChartParameters)
     */
    @Override
    protected Plot getPlot(ChartParameters params) {
        PiePlot3D plot = generatePiePlot(params);

        plot.setNoDataMessage(NO_DATA_MESSAGE);
        plot.setOutlineVisible(false);
        plot.setDarkerSides(true);
        plot.setBackgroundAlpha(0.0f);
        plot.setLabelBackgroundPaint(Color.white);
        plot.setLabelPaint(Color.black);
        plot.setLabelShadowPaint(Color.white);
        plot.setLabelOutlinePaint(Color.white);
        plot.setLabelPadding(new RectangleInsets(0,0,0,0));
        plot.setLabelFont(new Font("SansSerif",Font.PLAIN,12));
        return plot;
    }

    /**
     * Generates the plot for the 3D Pie Chart
     * <p>
     * This method creates and configures a Pie Plot for a 3D Pie Chart
     * from a given dataset.
     * </p>
     * 
     * @param the chart parameters to get the data set and color series from
     * @return the plot configured to be used by the pie chart
     * @since 2.4.1
     */
    private PiePlot3D generatePiePlot(ChartParameters params) {

        DefaultPieDataset dataset = getDataset(params);
        PiePlot3D plot = new PiePlot3D();
        plot.setDataset(dataset);

        paintPlot(plot,params);
        return plot;
    }

    /**
      * Get the dataset to be used for the pie chart
     * <p>
     * Get the values from the chart parameters to create a dataset which can be set into the pie chart.
     * </p>
     * @param params the chart parameters to get the values from the dataset
     * @return the dataset to be used for the pie chart
     * @since 2.4.1
     */
    private DefaultPieDataset getDataset(ChartParameters params) {
        DefaultPieDataset dataset = new DefaultPieDataset();
        String[] values = params.getValues(VALUE_ID,";");
        addValuesToTheDataset(values,dataset);
        return dataset;
    }

    /**
     * Add values to a dataset
     * <p>
     * For a given array of values, add them to the dataset 
     * so they can be displayed in the pie chart
     * </p>
     * @param values the values to be added to the dataset
     * @param dataset the dataset to add the values to
     * @since 2.4.1
     */
    private void addValuesToTheDataset(String[] values,DefaultPieDataset dataset) {
        for (int i = 0;i<values.length;i++) {
            String[] valuePair = values[i].split("=");
            dataset.setValue(valuePair[0],Double.parseDouble(valuePair[1]));
        }
    }

    /**
     * Paint a given pie plot with the correct colors
     * <p>
     * This method sets the color to be painted for each of the plot sections according to
     * the COLORS color array constant in this class.
     * </p>
     * @param plot the plot to be painted
     * @param the chart parameters to get the key of the pie chart sections which were set
     * @since 2.4.1
     */
    private void paintPlot(PiePlot3D plot,ChartParameters params) {
        String[] values = params.getValues(VALUE_ID,";");
        for (int i = 0;i<values.length;i++) {
            String[] valuePair = values[i].split("=");
            String key = valuePair[0];
            plot.setSectionPaint(key,COLORS[i]);
        }
    }
}
