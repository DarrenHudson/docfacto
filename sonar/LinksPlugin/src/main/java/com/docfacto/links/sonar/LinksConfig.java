package com.docfacto.links.sonar;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * A singleton class to store the config details for Links which can be used by 
 * other classes
 *<p>
 * This class stores the config details necessary for Links to function, which
 * are the path of the doclet (the Docfacto jar), the path of the config (the
 * Docfacto config xml file) and the source path of the source files to be
 * analyzed
 * </p>
 * 
 * @author damonli - created Jun 5, 2013
 * @since 2.4.1
 */
public class LinksConfig {
    
    /**
     * The path to the config (the Docfacto config xml file)
     */
    @Getter @Setter private static String configPath;
    
    /**
    * The doc path of the documentation files which need to be analyzed
    */
   @Getter @Setter private static String docPath;
   
   /**
    * The list of extensions of the documentation files which need to be analyzed
    */
   @Getter @Setter private static List<String> docExtensionTypesList;

    /**
     * A private constructor as this is a singleton to prevent this class from
     * being instantiated by other classes.
     */
    private LinksConfig() {
        configPath = "";
        docExtensionTypesList=new ArrayList<String>();
        docPath = "";
    }
}
