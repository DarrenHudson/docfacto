package com.docfacto.links.sonar.checks;
import java.io.File;
import java.util.List;
import com.docfacto.output.generated.Result;
import com.puppycrawl.tools.checkstyle.api.AbstractFileSetCheck;
import com.docfacto.links.sonar.ResultsStore;
import com.docfacto.links.sonar.CheckstyleUtils;
/**
* @docfacto.adam ignore
*/
public class SourceLinksMismatchChk extends AbstractFileSetCheck {
private String resultKey = "source-link-matched";
@Override
protected void processFiltered(File file, List<String> aLines) {
List<Result> resultsList = ResultsStore.RESULTS_STORE.getResultsForFile(file);
if (resultsList == null) {
return;
}
for (Result result : resultsList) {
if (result.getRule().getKey().equals(getResultKey()))
this.addViolation(result.getPosition().getLine(), result.getRule().getMessage(), result.getSeverity());
}
}
private String getResultKey() {
return resultKey;
}
private void addViolation(int lineNo, String message, Object detailsOfMessage) {
String escapedMessage = CheckstyleUtils.escapeSpecialCharacters(message);
this.log(lineNo, escapedMessage, detailsOfMessage);
}
}
