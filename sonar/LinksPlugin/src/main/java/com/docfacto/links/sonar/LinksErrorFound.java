package com.docfacto.links.sonar;

import lombok.Getter;
import lombok.Setter;


/**
 * An enum singleton for recording whether a Links error was found
 * <p>
 * When an error with Links is found, this singleton will be notified so necessary warnings and steps can
 * be taken elsewhere
 * </p>
 * @author damonli - created Jun 11, 2013
 * @since 2.4.0
 */
public enum LinksErrorFound {

    INSTANCE;
    
    @Getter @Setter private boolean errorFound;

    private LinksErrorFound() {
        errorFound = false;
    }
}
