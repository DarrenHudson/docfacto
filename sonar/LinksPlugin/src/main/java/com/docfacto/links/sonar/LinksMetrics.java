package com.docfacto.links.sonar;
import java.util.Arrays;
import java.util.List;
import org.sonar.api.measures.CoreMetrics;
import org.sonar.api.measures.Metric;
import org.sonar.api.measures.Metrics;
/**
* @docfacto.adam ignore
*/
public final class LinksMetrics implements Metrics {

public static final Metric SOURCE_FILES_PROCESSED = new Metric.Builder("source-files-processed", "Source files processed", Metric.ValueType.INT)
.setDescription("Number of source files processed")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric DOC_FILES_PROCESSED = new Metric.Builder("doc-files-processed", "Doc files processed", Metric.ValueType.INT)
.setDescription("Number of documentation files processed")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric SOURCE_LINES = new Metric.Builder("source-lines", "Source lines scanned", Metric.ValueType.INT)
.setDescription("Number of source lines")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric CODE_LINES = new Metric.Builder("code-lines", "Code lines scanned", Metric.ValueType.INT)
.setDescription("Number of code lines")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric COMMENT_LINES = new Metric.Builder("comment-lines", "Comment lines scanned", Metric.ValueType.INT)
.setDescription("Number of comment lines")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric DOC_LINES = new Metric.Builder("doc-lines", "Doc lines scanned", Metric.ValueType.INT)
.setDescription("Number doc lines")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric DOC_LINK_FOUND = new Metric.Builder("doc-link-found", "Doc links found", Metric.ValueType.INT)
.setDescription("Number of doc links found in source files")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric SOURCE_LINK_FOUND = new Metric.Builder("source-link-found", "Source links found", Metric.ValueType.INT)
.setDescription("Number of source links found in doc files")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric LINKS_WITH_UNKNOWN_TYPE = new Metric.Builder("links-with-unknown-type", "Links with unknown type", Metric.ValueType.INT)
.setDescription("Number of links found with an unknown type")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric DOC_LINK_TAG_NO_URI = new Metric.Builder("doc-link-tag-no-uri", "Source link with no uri attribute", Metric.ValueType.INT)
.setDescription("Number of doc link tags without a uri attribute")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric SOURCE_LINK_TAG_NO_URI = new Metric.Builder("source-link-tag-no-uri", "Source link with no uri attribute", Metric.ValueType.INT)
.setDescription("Number of source link tags without a uri attribute")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric DOC_LINK_INVALID_URI = new Metric.Builder("doc-link-invalid-uri", "Doc link with invalid uri's", Metric.ValueType.INT)
.setDescription("Number of doc link uri's which are invalid")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric SOURCE_LINK_INVALID_URI = new Metric.Builder("source-link-invalid-uri", "Source link with invalid uri's", Metric.ValueType.INT)
.setDescription("Number of source link uri's which are invalid")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric SOURCE_BROKEN_LINK = new Metric.Builder("source-broken-link", "Source links with no doc link", Metric.ValueType.INT)
.setDescription("Number of source links with no corresponding doc link")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric DOC_BROKEN_LINK = new Metric.Builder("doc-broken-link", "Doc links with no source links", Metric.ValueType.INT)
.setDescription("Number of doc links with no corresponding source link")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric SOURCE_LINK_VERSION_MISMATCH = new Metric.Builder("source-link-version-mismatch", "Source link version mismatch", Metric.ValueType.INT)
.setDescription("Number of source links that don't match corresponding doc link version")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric DOC_LINK_VERSION_MISMATCH = new Metric.Builder("doc-link-version-mismatch", "Doc link version mismatch", Metric.ValueType.INT)
.setDescription("Number of doc links that don't match corresponding source link version")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric SOURCE_LINK_MATCHED = new Metric.Builder("source-link-matched", "Source links mismatch", Metric.ValueType.INT)
.setDescription("Number of source links that match doc links")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric DOC_LINK_MATCHED = new Metric.Builder("doc-link-matched", "Doc links mismatch", Metric.ValueType.INT)
.setDescription("Number of doc links that match source links")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric SOURCE_IOEXCEPTION = new Metric.Builder("source-ioexception", "Source files unable to read", Metric.ValueType.INT)
.setDescription("Number of source files unable to read")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric DOC_IOEXCEPTION = new Metric.Builder("doc-ioexception", "Doc files unable to read", Metric.ValueType.INT)
.setDescription("Number of doc files unable to read")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric UNKNOWN_FILE_TYPE = new Metric.Builder("unknown-file-type", "Unknown file types", Metric.ValueType.INT)
.setDescription("Number of unknown file types")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric UNKNOWN_TARGET_FILE_TYPE = new Metric.Builder("unknown-target-file-type", "Links with unknown target file type", Metric.ValueType.INT)
.setDescription("Number of links with an unknown target file type")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric LINKS_ERRORS = new Metric.Builder("links-errors", "Number of errors", Metric.ValueType.INT)
.setDescription("Number of errors")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric LINKS_WARNINGS = new Metric.Builder("links-warnings", "Number of warnings", Metric.ValueType.INT)
.setDescription("Number of warnings")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric LINKS_INFO = new Metric.Builder("links-info", "Number of infos", Metric.ValueType.INT)
.setDescription("Number of infos")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric SOURCE_FILES_WITH_DOC_LINK = new Metric.Builder("source-files-with-doc-link", "Source files with at least one working doc link", Metric.ValueType.INT)
.setDescription("Source files linked to doc")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric DOC_FILES_WITH_SOURCE_LINK = new Metric.Builder("doc-files-with-source-link", "Doc files linked to doc", Metric.ValueType.INT)
.setDescription("Number of doc files with at least one working source link")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric LINES_OF_CODE_LINKED_TO_DOC = new Metric.Builder("lines-of-code-linked-to-doc", "Code lines linked to doc", Metric.ValueType.INT)
.setDescription("Lines of code which have been linked to at least one document")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public static final Metric LINES_OF_DOC_LINKED_TO_SOURCE = new Metric.Builder("lines-of-doc-linked-to-source", "Doc lines linked to source", Metric.ValueType.INT)
.setDescription("Lines of documentation which have been linked to at least one source file")
.setQualitative(false)
.setDomain(CoreMetrics.DOMAIN_GENERAL)
.create();

public List<Metric> getMetrics() {
return Arrays.asList(SOURCE_FILES_PROCESSED,DOC_FILES_PROCESSED,SOURCE_LINES,CODE_LINES,COMMENT_LINES,DOC_LINES,DOC_LINK_FOUND,SOURCE_LINK_FOUND,LINKS_WITH_UNKNOWN_TYPE,DOC_LINK_TAG_NO_URI,SOURCE_LINK_TAG_NO_URI,DOC_LINK_INVALID_URI,SOURCE_LINK_INVALID_URI,SOURCE_BROKEN_LINK,DOC_BROKEN_LINK,SOURCE_LINK_VERSION_MISMATCH,DOC_LINK_VERSION_MISMATCH,SOURCE_LINK_MATCHED,DOC_LINK_MATCHED,SOURCE_IOEXCEPTION,DOC_IOEXCEPTION,UNKNOWN_FILE_TYPE,UNKNOWN_TARGET_FILE_TYPE,LINKS_ERRORS,LINKS_WARNINGS,LINKS_INFO,SOURCE_FILES_WITH_DOC_LINK,DOC_FILES_WITH_SOURCE_LINK,LINES_OF_CODE_LINKED_TO_DOC,LINES_OF_DOC_LINKED_TO_SOURCE);
}
}
