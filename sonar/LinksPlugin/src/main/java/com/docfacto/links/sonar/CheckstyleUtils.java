package com.docfacto.links.sonar;

/**
 * A class for utility methods related to Checkstyle
 * @author damonli
 *
 */
public class CheckstyleUtils {

	/**
	 * Escape the curly braces for a message as Checkstyle.
	 * @param message the message to escape special characters for
	 * @return the message with the escaped characters
	 */
	public static String escapeSpecialCharacters(String message) {
		String escapedMessage = message.replace("{", "'{'");
		escapedMessage = escapedMessage.replace("}", "'}'");
		return escapedMessage;
	}
}
