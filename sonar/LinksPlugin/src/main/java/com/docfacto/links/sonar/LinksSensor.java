package com.docfacto.links.sonar;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.sonar.api.batch.Sensor;
import org.sonar.api.batch.SensorContext;
import org.sonar.api.config.Settings;
import org.sonar.api.measures.Measure;
import org.sonar.api.measures.MeasureUtils;
import org.sonar.api.measures.Metric;
import org.sonar.api.resources.Project;
import org.sonar.api.scan.filesystem.ModuleFileSystem;

import com.docfacto.common.DocfactoException;
import com.docfacto.output.generated.Statistic;

/**
 * The Sensor which will retrieve Statistics from Links and record them when 
 * analysing a project.
 * 
 * @author damonli - created Jun 5, 2013
 * @since 2.4.1
 */
public class LinksSensor implements Sensor {

    private static final String ACCEPTED_FILE_EXTENSION = ".java";

    /**
     * The file system of the project which will be analysed
     */
    private final ModuleFileSystem projectFileSystem;

    /**
     * The constructor for instantiating the Sensor
     * 
     * @param moduleFileSystem the file system of the project which will be
     * analyzed
     * @param settings the settings of the project being analyzed
     * @since 2.4.1
     */
    public LinksSensor(ModuleFileSystem moduleFileSystem,Settings settings) {
        this.projectFileSystem = moduleFileSystem;
        setupLinksConfig(settings);
    }
    
    /**
     * Set up the LinksConfig with the correct values from the settings for the project
     * <p>
     * This method gets the relevant configuration properties for the project and sets them into
     * LinksConfig
     * </p>
     * @param settings the settings to get the configuration from
     * @since 2.4.1
     */
    private void setupLinksConfig(Settings settings) {
        LinksConfig.setConfigPath(settings.getString("sonar.docfacto.config"));
        LinksConfig.setDocPath(settings.getString("sonar.links.docs"));
        
        String docExtensionTypesString = settings.getString("sonar.links.docExtensions");
        List<String> docExtensionsList = Arrays.asList(docExtensionTypesString.split(","));
        LinksConfig.setDocExtensionTypesList(docExtensionsList);
    }

    /**
     * @see org.sonar.api.batch.CheckProject#shouldExecuteOnProject(org.sonar.api.resources.Project)
     */
    public boolean shouldExecuteOnProject(Project project) {
        return true;
    }

    /**
     * @see org.sonar.api.batch.Sensor#analyse(org.sonar.api.resources.Project,
     * org.sonar.api.batch.SensorContext)
     */
    public void analyse(Project project,SensorContext sensorContext) {
        
        setupMetricsProvider();

        try {
            saveStatisticsMeasures(sensorContext);
            saveChartMeasures(sensorContext);
        }
        catch (DocfactoException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Save the Statistics from Links for the project as measures to the SensorContext
     * <p>
     * Get the measures for the statistics returned by Links to the sensor context
     * </p>
     * @param sensorContext the sensor context of the sensor to save measures to
     * @throws IOException There was an error when dealing with files in LinksOutputRetriever
     * @throws DocfactoException There was an error with Links in LinksOutputRetriever
     * @since 2.4.1
     */
    private void saveStatisticsMeasures(SensorContext sensorContext) throws IOException, DocfactoException {

        List<Statistic> statisticsList = getStatisticsForProject();
        for (Statistic statistic:statisticsList) {
            String key = statistic.getRule().getKey();
            String valueString = statistic.getValue()+"";
            
            Metric metric = MetricsProvider.INSTANCE.getMetricForKey(key);
            if (metric!=null) {
                saveMeasureToSensorContext(sensorContext,metric,Double.parseDouble(valueString));
            }
        }
    }

    /**
     * Get Statistics from Links for the project
     * <p>
     * This method will invoke Links to get Statistics for the project
     * </p>
     * @return the list of Statistics Link returned for this project
     * @throws IOException There was an error when dealing with files
     * @throws DocfactoException There was an error with Links in LinksOutputRetriever
     * @since 2.4.1
     */
    private List<Statistic> getStatisticsForProject() throws IOException, DocfactoException {
        List<File> files = getFilesFromFileSystem(projectFileSystem);
        return LinksOutputRetriever.INSTANCE.getStatisticsForFiles(files);
    }

    /**
     * Save the measures relevant to the chart to the sensor context
     * <p>
     * Get the measures that are relevant to the chart and save them to the sensor so the chart 
     * can get the correct values in the correct format.
     * </p>
     * @param sensorContext the sensor context of the sensor to save measures to
     * @since 2.4.1
     */
    private void saveChartMeasures(SensorContext sensorContext) {
        
        sensorContext.saveMeasure(new Measure(SummaryMetrics.PERCENTAGE_OF_LINES_OF_CODE_LINKED_TO_DOC, getPercentageOfCodeLinesLinkedToDoc(sensorContext)));
        
        String pieChartValues = getPieChartValuesString(sensorContext);
        sensorContext.saveMeasure(new Measure(SummaryMetrics.PIECHART,pieChartValues));
    }
    
    /**
     * Get the string with the values to be displayed on a pie chart.
     * <p>
     * This method will return the string formatted to be used by the pie chart so that it will display the values 
     * in the string.
     * </p>
     * @param sensorContext the sensor context to get the pie chart values from 
     * @return the string with the values formatted to be displayed on a pie chart
     * @since 2.4.1
     */
    private String getPieChartValuesString(SensorContext sensorContext) {
        String piechartValues = "";

        double percentageOfCodeLinesLinkedToDoc = getPercentageOfCodeLinesLinkedToDoc(sensorContext);
        double percentageOfCodeLinesNotLinkedToDoc = 100.0 - percentageOfCodeLinesLinkedToDoc;
        
        piechartValues += getPieChartStringForValue("With Doc",Double.toString(percentageOfCodeLinesLinkedToDoc));
        piechartValues += getPieChartStringForValue("Without Doc",Double.toString(percentageOfCodeLinesNotLinkedToDoc));
        
        return piechartValues;
    }
    
    /**
     * Get the percentage of code lines which are linked to documentation
     * <p>
     * This method retrieves the measures for the number of lines of code which are linked to documentation and 
     * the total number of lines of code, then calculates and returns the percentage of lines of code which are
     * linked to documentation.
     * </p>
     * @param sensorContext the SensorContext to get the necessary measures from
     * @return the percentage of code lines which are linked to documentation
     * @since 2.4.1
     */
    private double getPercentageOfCodeLinesLinkedToDoc(SensorContext sensorContext) {
        double numberOfCodeLinesLinkedToDoc = MeasureUtils.getValue(sensorContext.getMeasure(LinksMetrics.LINES_OF_CODE_LINKED_TO_DOC), 0.0);
        double totalNumberOfCodeLines = MeasureUtils.getValue(sensorContext.getMeasure(LinksMetrics.CODE_LINES), 0.0);
        
        double percentageOfCodeLinesLinkedToDoc = (numberOfCodeLinesLinkedToDoc / totalNumberOfCodeLines) * 100;
        
        return percentageOfCodeLinesLinkedToDoc;
    }

    /**
     * Set up the MetricsProvider to have all the Metrics
     * <p>
     * This method method populates the MetricsProvider with all the metrics so that it
     * can be used elsewhere.
     * </p>
     * @since 2.4.1
     */
    private void setupMetricsProvider() {
        LinksMetrics linksMetrics = new LinksMetrics();

        for (Metric metric:linksMetrics.getMetrics())
            MetricsProvider.INSTANCE.setNewKeyMetricPair(metric.getKey(),metric);
    }

    /**
     * Save a measure for a given Metric and double to a SensorContext
     * <p>
     * This method will save a given Metric and double value to a given SensorContext.
     * </p>
     * 
     * @param sensorContext the sensor context to save the measure to
     * @param metric teh metric of the measure
     * @param doubleValue the value of the measure
     * @since 2.4.1
     */
    private void saveMeasureToSensorContext(SensorContext sensorContext,Metric metric,Double doubleValue) {
        Measure measure = new Measure(metric,doubleValue);
        sensorContext.saveMeasure(measure);
    }

    /**
     * Gets the list of files from a ModuleFileSystem
     * <p>
     * This method traverses a given ModuleFileSystem and returns a list of all
     * files found.
     * </p>
     * 
     * @param fileSystem the file system which files are to be retrieved from
     * @return the list of files found in the file system given
     * @since 2.4.1
     */
    private List<File> getFilesFromFileSystem(ModuleFileSystem fileSystem) {
        List<File> fileList;

        fileList = getFilesFromDirs(fileSystem.sourceDirs());
        return fileList;
    }

    /**
     * Gets the list of files from a list of directories
     * <p>
     * This method will traverse through each directory given in a list and
     * returns a list of all files found in each directory.
     * </p>
     * 
     * @param dirs the list of directories which files are to be retrieved from
     * @return the list of files found in all the given directories
     * @since 2.4.1
     */
    private List<File> getFilesFromDirs(List<File> dirs) {
        List<File> files = new ArrayList<File>();
        for (File dir:dirs) {
            if (dir.isDirectory())
                files.addAll(getFilesFromDirs(Arrays.asList(dir.listFiles())));
            else {
                if (dir.getName().endsWith(ACCEPTED_FILE_EXTENSION))
                    files.add(dir);
            }
        }
        return files;
    }

    /**
     * Get the string for a key and value in a format which can be read by the pie chart.
     * <p>
     * The pie chart reads values in the string format: 
     * key1=value1;key2=value2;key3=value3;
     * For a given key and value, this will return it in the string format: "key=value;" so that it can be
     * concatenated with other key and value pair strings to be read by the pie chart.
     * </p>
     * @param key the key for the key and value pair to be read by the chart
     * @param value the value for the key and value pair to be read by the chart
     * @return the string for the given key and value formatted to be read by the chart
     * @since 2.4.1
     */
    private String getPieChartStringForValue(String key,String value) {
        return key+"="+value+";";
    }
}
