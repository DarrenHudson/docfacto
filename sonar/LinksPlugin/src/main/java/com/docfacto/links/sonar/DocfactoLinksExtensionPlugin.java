package com.docfacto.links.sonar;

import java.util.Arrays;
import java.util.List;

import org.sonar.api.SonarPlugin;

/**
 * @docfacto.adam ignore
 */
public class DocfactoLinksExtensionPlugin extends SonarPlugin {
    
    public List getExtensions() {
        List extensions = Arrays.asList(SummaryMetrics.class,
                                LinksMetrics.class,
                                ErrorMetrics.class,
                                LinksSensor.class,
                                LinksViolationsDecorator.class,
                                CheckstyleExtensionRepository.class,
                                LinksSummaryWidget.class,
                                LinksStatisticsWidget.class,
                                PieChart.class);
        return extensions;
    }
}
