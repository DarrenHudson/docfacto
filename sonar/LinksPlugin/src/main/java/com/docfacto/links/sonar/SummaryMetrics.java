package com.docfacto.links.sonar;

import java.util.Arrays;
import java.util.List;

import org.sonar.api.measures.CoreMetrics;
import org.sonar.api.measures.Metric;
import org.sonar.api.measures.Metrics;

/**
 * @docfacto.adam ignore
 */
public class SummaryMetrics implements Metrics {

    public static final Metric PIECHART = new Metric.Builder("summary-piechart","Pie Chart Metric",
        Metric.ValueType.DISTRIB)
        .setDescription("A metric for storing the distribution to be used by the summary bar chart")
        .setQualitative(false)
        .setDomain(CoreMetrics.DOMAIN_GENERAL)
        .create();
    
    public static final Metric PERCENTAGE_OF_LINES_OF_CODE_LINKED_TO_DOC = new Metric.Builder("percentage-of-lines-of-code-linked-to-doc","Percentage of Lines Linked to Doc",
        Metric.ValueType.FLOAT)
        .setDescription("A metric for storing the percentage of lines of code which have been linked to at least one document")
        .setQualitative(false)
        .setDomain(CoreMetrics.DOMAIN_GENERAL)
        .create();

	public static final Metric LINKS_VIOLATIONS = new Metric.Builder("number-of-links-violations",
	        "Number of Links Violations",
	        Metric.ValueType.INT)
	        .setDescription("A metric for the number of Links Rules violated")
	        .setDirection(Metric.DIRECTION_WORST)
	        .setQualitative(false)
	        .setDomain(CoreMetrics.DOMAIN_GENERAL)
	        .create();
    
    public List<Metric> getMetrics() {
        return Arrays.asList(PIECHART, PERCENTAGE_OF_LINES_OF_CODE_LINKED_TO_DOC, LINKS_VIOLATIONS);
    }
}
