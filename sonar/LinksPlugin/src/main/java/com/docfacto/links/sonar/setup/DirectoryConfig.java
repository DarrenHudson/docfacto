package com.docfacto.links.sonar.setup;

import lombok.Getter;

/**
 * A singleton class to store directory data which can be used by other classes
 * <p>
 * This class contains the directory of the sources and resources so it can be referenced
 * by other classes.
 * </p>
 * @author damonli - created Jun 6, 2013
 * @since 2.4.1
 */
public class DirectoryConfig {
    /**
     * The directory of the source files in this project
     */
    @Getter private static String sourcesDirectory = "/src/main/java";
    /**
     * The directory of the resource files in this project
     */
    @Getter private static String resourcesDirectory = "/src/main/resources";
    
    /**
     * A private constructor as this is a singleton to prevent this class from being instantiated by
     * other classes.
     */
    private DirectoryConfig() {
    }
}
