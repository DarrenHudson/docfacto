package com.docfacto.links.sonar.setup;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.docfacto.links.sonar.CheckstyleExtensionRepository;

/**
 * A class which generates the XML file needed to configure the Checkstyle rules
 * for CheckstyleExtensionRepository
 * <p>
 * This class creates and writes the XML file which an XMLParser in
 * CheckstyleExtensionRepository will parse to set up the Check classes (in
 * com.docfacto.checks) to be used in Sonar as Checkstyle rules.
 * </p>
 * The xml file will be written into a directory in the resources directory of
 * this project.
 * 
 * @author damonli - created Apr 21, 2013
 * @since 2.2.3
 */
public class CheckstyleXMLGenerator {

    

    /**
     * Generates the XML file which CheckstyleExtensionRepository can parse to
     * configure the Checkstyle rules to be used in Sonar.
     * <p>
     * Each Check class in com.docfacto.links.checks needs to be configured in the XML
     * to be a Checkstyle rule.
     * </p>
     * <p>
     * Each rule will have the following attributes:<br />
     * 
     * The key for the rule to be displayed by Sonar, which should be the class
     * name e.g. (com.docfacto.links.sonar.checks.ExampleClass) enclosed in the <key>
     * tags. <br />
     * 
     * The name of this rule to be displayed by Sonar enclosed in the <name>
     * tags. <br />
     * 
     * The config key for this rule so Sonar knows the location of the class
     * which should be in the format:
     * Checker/com.docfacto.sonar.checks.ExampleClass enclosed in the
     * <configKey> tags. <br />
     * 
     * The description of the rule enclosed the <description> tags. <br />
     * </p>
     * <p>
     * These attributes are then enclosed by <rule> tags, and all of these are
     * put between <rules> tags
     * </p>
     * 
     * @param ruleDetailsList the list of details for each rule to be put into
     * the xml file
     * @param path the path that this project is in so the xml file can be
     * placed in the correct directory
     * @throws IOException if the xml file couldn't be created or written to
     * @since 2.2.3
     */
    public static void generateXMLFileForResultDetails(List<RuleDetails> ruleDetailsList,String path) throws IOException {

        File resourcesDirectory = new File(path+CheckstyleExtensionRepository.CHECKSTYLE_XML_DIRECTORY);
        
        if (!resourcesDirectory.exists()) {
            resourcesDirectory.mkdir();
        }

        File newFile = new File(path+DirectoryConfig.getResourcesDirectory() 
                                    + CheckstyleExtensionRepository.CHECKSTYLE_XML_DIRECTORY
                                    + CheckstyleExtensionRepository.CHECKSTYLE_XML_NAME);

        String code = "<rules>";
        for (RuleDetails ruleDetails:ruleDetailsList) {
            code += "\n<rule>\n";
            code +=
                "<key>com.docfacto.links.sonar.checks."+ruleDetails.getClassName()+
                    "</key>\n";
            code += "<name>Docfacto Links: "+ruleDetails.getName()+"</name>\n";

            if (ruleDetails.getSeverity()!=null)
                code += "<priority>"+getSonarPriorityForSeverity(ruleDetails.getSeverity())+"</priority>\n";

            code +=
                "<configKey>Checker/com.docfacto.links.sonar.checks."+
                    ruleDetails.getClassName()+"</configKey>\n";
            code +=
                "<description>"+ruleDetails.getDescription()+"</description>\n";

            if (ruleDetails.getCategory()!=null)
                code += "<category name=\""+ruleDetails.getCategory()+"\" />\n";

            code += "</rule>";
        }
        code += "</rules>";

        FileWriter fw = new FileWriter(newFile.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(code);
        bw.close();
    }

    /**
     * Get the equivalent Sonar priority for a Links rule severity
     * <p>
     * Sonar allows 5 priority types: INFO, MINOR, MAJOR, CRITICAL, BLOCKER <br />
     * Links has 3 severity types: ERROR, INFO and WARNING <br />
     * 
     * The Sonar priority equivalent for ERROR is BLOCKER <br />
     * The Sonar priority equivalent for INFO is INFO <br />
     * The Sonar priority equivalent for WARNING is MAJOR <br />
     * The default priority is MAJOR
     * </p>
     * 
     * @param severity the result severity
     * @return the Sonar priority equivalent for the result severity given
     * @since 2.2.3
     */
    private static String getSonarPriorityForSeverity(String severity) {
        if (severity.equalsIgnoreCase("INFO"))
            return "INFO";
        else if (severity.equalsIgnoreCase("ERROR"))
            return "BLOCKER";
        else
            return "MAJOR";
    }
}
