package com.docfacto.links.sonar.setup;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Generates the Metrics class implementation (which needs to implement the Metrics interface) which
 * contains the details of the Metrics to be used by Sonar
 * <p>
 * Each Metric is represented by a constant with it's key, name, description (the message which is shown by Sonar) set.
 * </p>
 * 
 * @author damonli - created Jun 6, 2013
 * @since 2.4.1
 */
public class LinksMetricsGenerator {

    /**
     * The directory where the Metrics class will be created
     */
    public static final String METRICS_DIRECTORY = "/com/docfacto/links/sonar/";
    /**
     * The name of the Metrics class that will be created
     */
    public static final String METRICS_CLASS_NAME = "LinksMetrics";
    /**
     * The name of the Metrics class java file that will be created
     */
    public static final String METRICS_FILE_NAME = METRICS_CLASS_NAME + ".java";
    
    /**
     * Generate the Metrics class for a given list of metric details.
     * <p>
     * Generate a Metrics class which will be used by Sonar to configure the Metrics. Each Metric is a constant in
     * the class and has its key, name, description set according to the given MetricDetails.
     * </p>
     * @param metricDetailsList the list of MetricDetails to configure Metrics for
     * @param path the path of the plugin so the Metric class can be created in the correct location
     * @since 2.4.1
     */
    public static void generateMetricsForMetricDetailsList(List<MetricDetails> metricDetailsList, String path) {
        File newMetricsFile = new File(path + DirectoryConfig.getSourcesDirectory() + METRICS_DIRECTORY + METRICS_FILE_NAME);
        
        if (newMetricsFile.exists())
            newMetricsFile.delete();
        
        
        String code = "package com.docfacto.links.sonar;\n" + 
        
                      "import java.util.Arrays;\n" +
                      "import java.util.List;\n" +

                      "import org.sonar.api.measures.CoreMetrics;\n" +
                      "import org.sonar.api.measures.Metric;\n" +
                      "import org.sonar.api.measures.Metrics;\n" +
                      
                      "/**\n" +
                      "* @docfacto.adam ignore\n" +
                      "*/" +

                      "\npublic final class " + METRICS_CLASS_NAME + " implements Metrics {\n\n";
        
        List<String> metricConstantNamesList = new ArrayList<String>();
        
        for (MetricDetails metricDetails : metricDetailsList) {
            String key = metricDetails.getKey();
            String name = metricDetails.getName();
            String message = metricDetails.getMessage();
            String metricConstantName = metricDetails.getMetricConstantName();
            metricConstantNamesList.add(metricConstantName);
            
            code += "public static final Metric " + metricConstantName + " = new Metric.Builder(\"" + key + "\", \"" + name + "\", Metric.ValueType.INT)\n";
                
            code += ".setDescription(\"" +  message + "\")\n" +
                    ".setQualitative(false)\n" +
                    ".setDomain(CoreMetrics.DOMAIN_GENERAL)\n" +
                    ".create();\n\n";
        }
        
        code += "public List<Metric> getMetrics() {\n" +
                "return Arrays.asList(";
        
        for (int i=0; i < metricConstantNamesList.size(); i++) {
            code += metricConstantNamesList.get(i);
            
            if (i < metricConstantNamesList.size() - 1)
                code += ",";
        }
        
        code += ");\n";
        
        
        code += "}\n";
        code += "}\n";
        
        try {
            FileWriter fw = new FileWriter(newMetricsFile.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(code);
            bw.close();
            
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
}
