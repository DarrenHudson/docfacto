package com.docfacto.links.sonar.setup;

import java.util.Arrays;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * A simple class which stores details for a Rule which can be passed around by other classes
 *<P>
 * Each rule should have the following details: <br />
 * Key - a unique key to identify the rule <br />
 * Name - the name of the rule <br />
 * Description - the description of the rule <br />
 * Filename - the name of the Check class for this rule which can be read by java <br />
 * Category - the category for the rule
 *
 * @author damonli - created Apr 22, 2013
 * @since 2.2.3
 */
public class RuleDetails {
   
    @Getter private final String key;
    @Getter private String name;
    @Getter private String description;
    @Getter private String className;
    @Getter @Setter private String category;
    @Getter @Setter private String severity;
    
    /**
     * The constructor to instantiate a RuleDetails object
     * 
     * @param key the unique to identify the rule
     * @since 2.2.3
     */
    public RuleDetails(String key) {
        this.key = key;
    }
    
    /**
     * The constructor to instantiate a RuleDetails object
     * 
     * @param key the unique to identify the rule
     * @param name the name of the rule
     * @param description the description of the rule
     * @since 2.2.3
     */
    public RuleDetails(String key, String name, String description) {
        this.key = key;
        this.name = name;
        this.description = description;
        this.className = convertToClassName(name);
    }
    
    /**
     * Convert a rule name to a java compilable Check class name
     * <p>
     * Removes the characters which are disallowed from a java name and combines the words
     * to remove spaces and capitalizes the first of each word so the class name is
     * in camel case.
     * </p>
     * @param the rule name to be converted
     * @return the class name for the given rule name
     * @since 2.2.3
     */
    private String convertToClassName(String name) {
        String[] nameWordsArray = name.split("[^\\w]");
        List<String> wordsList = Arrays.asList(nameWordsArray);
        
        String className = convertWordListToCamelCase(wordsList) + "Chk";
        return className;
    }
    
    /**
     * Converts a list of word strings into one camel case string
     * <p>
     * For each string in the given list the first character is set to its upper case, and then
     * all strings are concatenated into on string
     * </p>
     * @param wordsList the list of words to convert to one camel case string
     * @return the camel case string version of the given list of words
     * @since 2.2.3
     */
    private String convertWordListToCamelCase(List<String> wordsList) {
        String finalWord = "";
        
        for (String word : wordsList) {
            String capitalizedWord;
            
            if (word.length() > 1)
                capitalizedWord = word.substring(0,1).toUpperCase() + word.substring(1);
            else if (word.length() == 1)
                capitalizedWord = word.toUpperCase();
            else
                capitalizedWord = "";
            
            finalWord += capitalizedWord;
        }
        return finalWord;
    }
}
