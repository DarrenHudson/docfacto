package com.docfacto.links.sonar.setup;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Set up the Rules for the plugin
 * <p>
 * This class is used for setting up the Rules the plugin should allow.
 * </p>
 * @author damonli - created Apr 23, 2013
 * @since 2.2.3
 */
public class RulesSetup {

    /**
     * Setup the rules for the plugin from a list of given RuleDetails
     * <p>
     * This method will set up the rules for the plugin, which involves creating a Check class for each rule
     * and then writing the required XML file to configure the classes for Sonar.
     * </p>
     * @param ruleDetailsList the list of RuleDetails which will contain details for the rules that need to be
     *        set up.
     * @throws IOException if there was an error in creating the necessary files
     * @since 2.2.3
     */
    public static void setupRulesFromRuleDetails(
    List<RuleDetails> ruleDetailsList) throws IOException {

        List<RuleDetails> filteredRuleDetailsList =
            new ArrayList<RuleDetails>();

        for (RuleDetails ruleDetails:ruleDetailsList) {
            if (ruleDetails.getName()!=null&&ruleDetails.getDescription()!=null)
                filteredRuleDetailsList.add(ruleDetails);
        }

        File tempFile = new File("");
        String path = tempFile.getCanonicalPath();

        CheckClassGenerator.generateCheckClassesForRuleDetails(
            filteredRuleDetailsList,path);
        CheckstyleXMLGenerator.generateXMLFileForResultDetails(
            filteredRuleDetailsList,path);

    }
}
