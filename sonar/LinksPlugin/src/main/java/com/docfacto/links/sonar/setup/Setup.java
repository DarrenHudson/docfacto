package com.docfacto.links.sonar.setup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.docfacto.common.DocfactoException;
import com.docfacto.links.sonar.LinksOutputRetriever;
import com.docfacto.output.generated.Rule;
import com.docfacto.output.generated.Statistic;

/**
 * This class is responsible for setting up the rules and metrics for the plugin.
 * <p>
 * This class has a main method which when run, will set up the rules and metrics according
 * to Links
 * </p>
 * @docfacto.adam ignore
 * @author damonli - created Jun 6, 2013
 * @since 2.4.1
 */
public class Setup {

    public static void main(String[] args) {
        List<MetricDetails> metricDetailsList = new ArrayList<MetricDetails>();
        List<RuleDetails> ruleDetailsList = new ArrayList<RuleDetails>();
        
        try {
            //Get a list of statistics which will contain the details for the rules and metrics
            //from Links
            List<Statistic> statisticsList =
                LinksOutputRetriever.INSTANCE.getEmptyStatistics();
            
          //Create the list of RuleDetails and MetricDetails
            for (Statistic statistic:statisticsList) {
                Rule rule = statistic.getRule();
                String ruleKey = rule.getKey();
                String ruleName = rule.getName();
                String ruleDescription = rule.getDescription();
                String ruleMessage = rule.getMessage();
                String category = "Usability";
                
                RuleDetails ruleDetails;

                if (ruleName!=null&&ruleDescription!=null)
                    ruleDetails =
                        new RuleDetails(ruleKey,ruleName,ruleDescription);
                else
                    ruleDetails = new RuleDetails(ruleKey);

                ruleDetails.setCategory(category);
                
                ruleDetailsList.add(ruleDetails);
                
                metricDetailsList.add(new MetricDetails(ruleKey,ruleName,
                    ruleDescription,ruleMessage));
            }

            //Set up the Rules
            RulesSetup.setupRulesFromRuleDetails(ruleDetailsList);
            //Set up the Metrics
            MetricsSetup.setupMetricsFromMetricDetails(metricDetailsList);
        }
        catch (DocfactoException e) {
            //Links output could not be retrieved so exit
            e.printStackTrace();
            System.exit(0);
        }
        catch (IOException e) {
            //Could not set up Metrics so exit
            e.printStackTrace();
            System.exit(0);
        }
    }
}
