package com.docfacto.sonar.setup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.docfacto.common.DocfactoException;
import com.docfacto.output.generated.Rule;
import com.docfacto.output.generated.Statistic;
import com.docfacto.sonar.AdamOutputRetriever;

/**
 * This class is responsible for setting up the rules and metrics for the plugin.
 * <p>
 * This class has a main method which when run, will set up the rules and metrics according
 * to Adam
 * </p>
 * @docfacto.adam ignore
 * @author damonli - created Apr 23, 2013
 * @since 2.2.3
 */
public class Setup {

    public static void main(String[] args) {
        List<MetricDetails> metricDetailsList = new ArrayList<MetricDetails>();
        List<RuleDetails> ruleDetailsList = new ArrayList<RuleDetails>();

        try {
            //Get a list of statistics which will contain the details for the rules and metrics
            //from Adam
            List<Statistic> statisticsList =
                AdamOutputRetriever.INSTANCE.getEmptyStatistics();
            
            //Create the list of RuleDetails and MetricDetails
            for (Statistic statistic:statisticsList) {
                Rule rule = statistic.getRule();
                String ruleKey = rule.getKey();
                String ruleName = rule.getName();
                String ruleDescription = rule.getDescription();
                String ruleMessage = rule.getMessage();
                String category = "Usability";

                RuleDetails ruleDetails;

                if (ruleName!=null&&ruleDescription!=null)
                    ruleDetails =
                        new RuleDetails(ruleKey,ruleName,ruleDescription);
                else
                    ruleDetails = new RuleDetails(ruleKey);

                ruleDetails.setCategory(category);
                
                ruleDetailsList.add(ruleDetails);
                
                metricDetailsList.add(new MetricDetails(ruleKey,ruleName,
                    ruleDescription,ruleMessage));
            }

            //Set up the Rules
            RulesSetup.setupRulesFromRuleDetails(ruleDetailsList);
            //Set up the Metrics
            MetricsSetup.setupMetricsFromMetricDetails(metricDetailsList);
        }
        catch (DocfactoException e) {
            //Adam output could not be retrieved so exit
            e.printStackTrace();
            System.exit(0);
        }
        catch (IOException e) {
            //Could not set up Rules or Metrics so exit
            e.printStackTrace();
            System.exit(0);
        }
    }
}
