package com.docfacto.sonar.setup;

import lombok.Getter;

/**
 * A simple class which stores details for a Metric which can be passed around by other classes
 * <p>
 * Each metric should have the following details: <br />
 * Key - a unique key to identify the metric <br />
 * Name - the name of the metric <br />
 * Description - the description of the metric <br />
 * Message - the message that will be shown by Sonar for that metric <br />
 * Metric constant name - the name for the metric which can be used as a constant in java
 * </p>
 * @author damonli - created Apr 22, 2013
 * @since 2.2.3
 */
public class MetricDetails {

    /**
     * The unique key to identify the metric
     */
    @Getter private final String key;
    /**
     * The name of the metric
     */
    @Getter private final String name;
    /**
     * The description of the metric
     */
    @Getter private final String description;
    /**
     * The message that will be shown by Sonar for that metric
     */
    @Getter private final String message;
    /**
     * The name for the metric which can be used as a constant by java
     */
    @Getter private final String metricConstantName;
    
    /**
     * The constructor to instantiate a MetricDetails object.
     * @param key the unique key to identify the metric
     * @param name the name of the metric
     * @param description the description of the metric
     * @param message the message that will be shown by Sonar for that metric
     * @since 2.2.3
     */
    public MetricDetails(String key, String name, String description, String message) {
        this.key = key;
        this.name = name;
        this.description = description;
        this.message = message;
        
        String nameWithOnlyAlphanumericCharacters = key.replaceAll("[^\\w]", "_");
        this.metricConstantName = nameWithOnlyAlphanumericCharacters.toUpperCase();
    }
}
