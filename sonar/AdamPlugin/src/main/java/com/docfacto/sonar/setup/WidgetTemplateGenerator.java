package com.docfacto.sonar.setup;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.docfacto.sonar.AdamStatisticsWidget;

/**
 * Generate the widget which can be shown on the Sonar dashboard.
 * <p>
 * This class has the method for generating the widget which will show the data
 * for a Metric and its value. The widget is stored as a Ruby on Rails .erb
 * file.
 * </p>
 * @author damonli - created Apr 23, 2013
 * @since 2.2.3
 */
public class WidgetTemplateGenerator {

    /**
     * Generate the widget file.
     * <p>
     * This method will generate the widget file to show each Metric provided by
     * the given MetricDetails list. When the widget is being shown, the Metric
     * description and the value measured will be displayed.
     * </p>
     * 
     * @param metricDetailsList the list of Metrics which the widget will
     * display
     * @param path the path of this plugin so the files can be created in the
     * correct location
     * @throws IOException if there was an error in creating the file
     * @since 2.2.3
     */
    public static void generateWidgetTemplateForMetricDetails(
    List<MetricDetails> metricDetailsList,String path) throws IOException {

        File resourcesDirectory = new File(path+DirectoryConfig.getResourcesDirectory()+AdamStatisticsWidget.WIDGET_TEMPLATE_DIRECTORY);
        if (!resourcesDirectory.exists()) {
            resourcesDirectory.mkdir();
        }

        File newFile = new File(path+DirectoryConfig.getResourcesDirectory()+AdamStatisticsWidget.WIDGET_TEMPLATE_DIRECTORY+AdamStatisticsWidget.WIDGET_TEMPLATE_NAME);

        String code = "<div class=\"dashbox\">\n"+
            "   <br />\n"+
            "   <h3><%= message('docfacto.adam.statistics.title') -%></h3>\n"+
            "   <br />\n"+
            
            "   <% internal_error_found = measure('internal-error') %>\n"+
            "   <% if internal_error_found %>\n"+
            "      <span style=\"color:red;\"><%= message(\"docfacto.adam.error.message\") -%> </span>\n"+
            "      <br />\n"+
            "   <% else %>\n"+
            
            "   <p>\n"+
            "      <table>\n";

        for (MetricDetails metricDetails:metricDetailsList) {
            String key = metricDetails.getKey();
            code +=
                "         <tr class=<%= cycle(\"even\", \"odd\") %>><td style=\"width:350px; padding-top:2px; padding-right:10px\"><%= metric('"+
                    key+
                    "').description -%></td> <td style=\"width:20px; text-align:right\"><%= format_measure('"+
                    key+"') -%> </td></tr>\n";
        }

        code += "      </table>\n";
        code += "   </p>\n";
        code += "<% end; %>\n";
        code += "</div>\n";

        FileWriter fw = new FileWriter(newFile.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(code);
        bw.close();

    }
}
