package com.docfacto.sonar.setup;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * This class is for generating the Check classes needed for Rules.
 * <p>
 * Every Rule will have a Check class which is run to determine if the Rule was broken. Every Check class 
 * is invoked by Sonar for each file in a project, which then gets the Results of which
 * rules are violated by that file, and then adds the violations to be shown in Sonar.
 * </p>
 * @author damonli - created Apr 23, 2013
 * @since 2.2.3
 */
public class CheckClassGenerator {

    /**
     * The directory where the Check classes will be generated
     */
    public static final String CHECK_CLASS_DIRECTORY =
        "/com/docfacto/sonar/checks/";

    /**
     * Generate the Check class for a given list of RuleDetails
     * <p>
     * A Check class is made for each rule with the attributes outlined by the given list of RuleDetails. If
     * the directory for Check classes already exists, then clear that directory to add the new Check classes.
     * </p>
     * @param ruleDetailsList the list of RuleDetails to build Check classes for
     * @param path the path of this plugin so the classes will be generated in the correct location
     * @throws IOException if there were errors when creating new files
     * @since 2.2.3
     */
    public static void generateCheckClassesForRuleDetails(
    List<RuleDetails> ruleDetailsList,String path) throws IOException {
        File checkDirectory = new File(path+DirectoryConfig.getSourcesDirectory()+CHECK_CLASS_DIRECTORY);
        if (checkDirectory.exists())
            deleteFilesInDir(checkDirectory);

        checkDirectory.mkdir();

        for (RuleDetails ruleDetail:ruleDetailsList)
            generateCheckClassForResultDetail(ruleDetail,path);
    }

    /**
     * Generate a Check class for a given RuleDetail
     * <p>
     * This method will create the Check class according to a RuleDetail.
     * </p>
     * @param ruleDetail the RuleDetail to create a Check class for
     * @param path the path of this plugin so the classes will be generated in the correct location
     * @throws IOException if there was an error in creating a file
     * @since 2.2.3
     */
    private static void generateCheckClassForResultDetail(RuleDetails ruleDetail,String path) throws IOException {
        String resultKey = ruleDetail.getKey();
        String newClassName = ruleDetail.getClassName();

        File newFile = new File(path+DirectoryConfig.getSourcesDirectory()+CHECK_CLASS_DIRECTORY+newClassName+".java");

        String code =
            "package com.docfacto.sonar.checks;\n"+

            "import java.io.File;\n"+
            "import java.util.List;\n"+

            "import com.docfacto.output.generated.Result;\n"+
            "import com.puppycrawl.tools.checkstyle.api.AbstractFileSetCheck;\n"+

            "import com.docfacto.sonar.ResultsStore;\n"+

            "/**\n" +
            "* @docfacto.adam ignore\n" +
            "*/\n" +
            
            "public class "+
            newClassName+
            " extends AbstractFileSetCheck {\n"+

            "private String resultKey = \""+
            resultKey+
            "\";\n"+

            "@Override\n"+
            "protected void processFiltered(File file, List<String> aLines) {\n"+
            "List<Result> resultsList = ResultsStore.RESULTS_STORE.getResultsForFile(file);\n"+

            "for (Result result : resultsList) {\n"+
            "if (result.getRule().getKey().equals(getResultKey()))\n"+
            "this.addViolation(result.getPosition().getLine(), result.getRule().getMessage(), result.getSeverity());\n"+
            "}\n"+
            "}\n"+

            "private String getResultKey() {\n"+
            "return resultKey;\n"+
            "}\n"+

            "private void addViolation(int lineNo, String message, Object detailsOfMessage) {\n"+
            "this.log(lineNo, message, detailsOfMessage);\n"+
            "}\n"+
            "}\n";

        FileWriter fw = new FileWriter(newFile.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(code);
        bw.close();

    }

    /**
     * Delete all files in a directory
     * <p>
     * This method will clear all files and folders in a given directory
     * </p>
     * @param directory the directory to be cleared
     * @since 2.2.3
     */
    private static void deleteFilesInDir(File directory) {
        if (directory.isDirectory()) {

            for (String fileName:directory.list()) {
                File fileToBeDeleted = new File(directory,fileName);
                deleteFilesInDir(fileToBeDeleted);
            }

            if (directory.list().length==0)
                directory.delete();
        }
        else {
            directory.delete();
        }
    }
}
