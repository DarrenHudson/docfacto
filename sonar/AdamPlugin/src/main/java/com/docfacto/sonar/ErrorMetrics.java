package com.docfacto.sonar;

import java.util.Arrays;
import java.util.List;

import org.sonar.api.measures.CoreMetrics;
import org.sonar.api.measures.Metric;
import org.sonar.api.measures.Metrics;

/**
 * @docfacto.adam ignore
 */
public final class ErrorMetrics implements Metrics {

    public static final Metric INTERNAL_ERROR = new Metric.Builder("internal-error","Internal Error",
        Metric.ValueType.STRING)
        .setDescription("There was an internal error")
        .setQualitative(false)
        .setDomain(CoreMetrics.DOMAIN_GENERAL)
        .create();
    
    /**
     * @see org.sonar.api.measures.Metrics#getMetrics()
     */
    public List<Metric> getMetrics() {
        return Arrays.asList(INTERNAL_ERROR);
    }
}
