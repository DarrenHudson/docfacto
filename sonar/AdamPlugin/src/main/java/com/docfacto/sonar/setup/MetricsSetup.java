package com.docfacto.sonar.setup;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Set up the Metrics for the plugin
 * <p>
 * This class is used for setting up the Metrics the plugin should allow.
 * </p>
 * @author damonli - created Apr 23, 2013
 * @since 2.2.3
 */
public class MetricsSetup {

    /**
     * Setup the metrics for the plugin from a list of given MetricDetails
     * <p>
     * This method will set up the metrics for the plugin according to a list of MetricDetails.
     * This involves constructing the Metrics class to have each Metric and then generating the Widget
     * to show each Metric.
     * </p>
     * @param metricDetailsList the list of MetricDetails which will contain details for the metrics that need to be
     *        set up.
     * @throws IOException if there was an error in creating the necessary files
     * @since 2.2.3
     */
    public static void setupMetricsFromMetricDetails(
    List<MetricDetails> metricDetailsList) throws IOException {
            File tempFile = new File("");
            String path = tempFile.getCanonicalPath();

            AdamMetricsGenerator.generateMetricsForMetricDetailsList(metricDetailsList,
                path);
            WidgetTemplateGenerator.generateWidgetTemplateForMetricDetails(
                metricDetailsList,path);
    }
}
