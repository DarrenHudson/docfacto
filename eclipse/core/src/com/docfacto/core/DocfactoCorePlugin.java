package com.docfacto.core;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.internal.ui.text.spelling.SpellCheckEngine;
import org.eclipse.jdt.internal.ui.text.spelling.engine.ISpellCheckEngine;
import org.eclipse.jface.dialogs.MessageDialogWithToggle;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.PreferencesUtil;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import com.docfacto.common.DocfactoException;
import com.docfacto.config.XmlConfig;
import com.docfacto.config.generated.Product;
import com.docfacto.core.utils.PluginUtils;
import com.docfacto.exceptions.InvalidConfigException;
import com.docfacto.licensor.Products;
import com.docfacto.licensor.UDCProcessor;

/**
 * The activator class controls the plug-in life cycle
 */
@SuppressWarnings("restriction")
public class DocfactoCorePlugin extends AbstractUIPlugin implements IStartup {

    /** The plug-in ID */
    public static final String PLUGIN_ID = "com.docfacto.core"; //$NON-NLS-1$

    // The shared instance
    private static DocfactoCorePlugin thePlugin;

    /** Preference store key used for identification purposes */
    public static final String DOCFACTO_KEY = "docfacto.xml";

    /** Preference store key for storing key validation */
    public static final String KEY_VALIDATED = "XML_validated";

    /** Preference store key for storing validation error message */
    public static final String VALIDATION_ERROR = "validation_error_message";

    private static final String VALIDATION_ERROR_PROMPT = "validation_error_message";

    public static final String CORE_DIALOG_CHECK = "Core_dialog_check";

    private boolean isUsingInternalXML = false;

    private static NotifyingXmlConfig theXmlConfig;

    private String theXMLConfigFile;

    private IFile theXmlInWorkspace;

    private ArrayList<XmlChangeListener> theXmlChangeListeners =
        new ArrayList<XmlChangeListener>();

    /**
     * The constructor
     */
    public DocfactoCorePlugin() {
    }

    /**
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
     */
    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);
        thePlugin = this;

        // Do it for the first time
        reloadConfig();
        // Register the Docfacto Dictionary, this is using internal stuff, but I
        // couldn't find another way of making this work
        final ISpellCheckEngine engine = SpellCheckEngine.getInstance();
        engine.registerGlobalDictionary(new DocfactoDictionary());

        ResourcesPlugin.getWorkspace().addResourceChangeListener(new XmlConfigChangeListener());

        // check configuration file state
        if (!DocfactoCorePlugin.getDefault().isUsingInternalXML()) {
            // Prompt user to select a valid 'Docfacto.xml' if none exists!
            if (!getPreferenceStore().getBoolean(DocfactoCorePlugin.KEY_VALIDATED)) {

                // Dialog is displayed in a thread to prevent a lock
                PlatformUI.getWorkbench().getDisplay()
                    .asyncExec(new Runnable() {

                        /**
                         * @see java.lang.Runnable#run()
                         */
                        public void run() {
                            // check user preference
                            if ((!getPreferenceStore().getString(
                                CORE_DIALOG_CHECK)
                                .equals(MessageDialogWithToggle.NEVER))) {
                                MessageDialogWithToggle coreDialog =
                                    MessageDialogWithToggle
                                        .openYesNoQuestion(
                                            PlatformUI.getWorkbench()
                                                .getDisplay()
                                                .getActiveShell(),
                                            "Docfacto Toolkit",
                                            "Would you like to select your Docfacto configuration file now?",
                                            "Don't show this message again",
                                            false,
                                            getPreferenceStore(),
                                            CORE_DIALOG_CHECK);

                                // open core plug-in preference page
                                if (coreDialog.getReturnCode()==MessageDialogWithToggle.INFORMATION) {
                                    PreferencesUtil
                                        .createPreferenceDialogOn(
                                            null,
                                            "com.docfacto.core.preference.Docfacto",
                                            null,null).open();

                                }
                            }
                        }
                    });
            }
        }

    }

    /**
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
     */
    @Override
    public void stop(BundleContext context) throws Exception {
        thePlugin = null;
        super.stop(context);
    }

    /**
     * Returns the shared instance
     * 
     * @return the shared instance
     */
    public static DocfactoCorePlugin getDefault() {
        return thePlugin;
    }

    /**
     * Force the core plugin to re-load the XML file.
     * 
     * This would normally be called when another preference has changed the XML file
     * 
     * @since 2.1
     */
    public void reloadConfig() {
        final IPreferenceStore store = getPreferenceStore();
        final String xmlPath = store.getString(DOCFACTO_KEY);
        if (!xmlPath.isEmpty()) {
            try {

                theXmlConfig = new NotifyingXmlConfig(xmlPath);
                theXMLConfigFile = xmlPath;
                // update store with validation
                getPreferenceStore().putValue(DocfactoCorePlugin.KEY_VALIDATED,"true");
            }
            catch (final DocfactoException ex) {
                logException("Unable to load the Docfacto.xml",ex);
                // set config validation to false
                getPreferenceStore().putValue(DocfactoCorePlugin.KEY_VALIDATED,"false");

            }
            catch (Exception ex) {
                logException("Error loading Docfacto configuration file",null);
                // set config validation to false
                getPreferenceStore().putValue(DocfactoCorePlugin.KEY_VALIDATED,"false");

            }
        }
        else {
            // Lets load the one from etc, just to get things going
            final Bundle bundle = Platform.getBundle(PLUGIN_ID);
            URL url = FileLocator.find(bundle,new Path("etc/Docfacto.xml"),null);
            isUsingInternalXML = true;
            try {
                theXmlConfig = new NotifyingXmlConfig(url);

                try {
                    theXMLConfigFile = PluginUtils.getFileLocationOf(DocfactoCorePlugin.PLUGIN_ID,"etc/Docfacto.xml");
                    DocfactoCorePlugin.logMessage("Using internal XML "+theXMLConfigFile);
                }
                catch (Throwable e) {
                    DocfactoCorePlugin.logMessage("Unable to load internal XML "+theXMLConfigFile);
                }
            }
            catch (final DocfactoException ex) {
                logException("Unable to load the Docfacto.xml",ex);
                // set config validation to false
                getPreferenceStore().putValue(DocfactoCorePlugin.KEY_VALIDATED,"false");

            }
        }
        /*
         * Check if the xmlconfig is null, it may be null if the xml is malformed and cannot be parsed,
         */
        if (theXmlConfig!=null) {
            theXmlInWorkspace = PluginUtils.findIFileInWorkspace(theXMLConfigFile);

            for (XmlChangeListener listener:theXmlChangeListeners) {
                listener.xmlReloaded();
            }
        }
    }

    /**
     * Return the file location of the XML file
     * 
     * @return the File location of Docfacto xml
     * @since 2.4
     */
    public String getXMLConfigFile() {
        return theXMLConfigFile;
    }

    /**
     * Check to see if using the internal xml and not the external one
     * 
     * @return true if using the internal Docfacto.xml
     * @since 2.4
     */
    public boolean isUsingInternalXML() {
        return isUsingInternalXML;
    }

    /**
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#initializeImageRegistry(org.eclipse.jface.resource.ImageRegistry)
     */
    @Override
    protected void initializeImageRegistry(ImageRegistry registry) {
        super.initializeImageRegistry(registry);

        final Bundle bundle = Platform.getBundle(PLUGIN_ID);
        // Change this for lazy loading to speed up the start of the plugin
        for (String imageName:IDocfactoCorePlugin.IMAGES) {
            final ImageDescriptor image =
                ImageDescriptor.createFromURL(FileLocator.find(bundle,new Path("icons/"+imageName),null));
            registry.put(imageName,image);
        }
    }

    /**
     * Return a Docfacto image
     * 
     * @param key of the Image
     * @return the cached image, or null if not found
     * @since 2.1
     */
    public static Image getImage(String key) {
        final ImageRegistry imageRegistry = thePlugin.getImageRegistry();
        return imageRegistry.get(key);
    }

    /**
     * Return the image descriptor from the image registry
     * 
     * @param key of the image
     * @return the image descriptor
     * @since 2.4
     */
    public static ImageDescriptor getImageDescriptor(String key) {
        return thePlugin.getImageRegistry().getDescriptor(key);
    }

    /**
     * Return the loaded XML config, attempt to load a default xml if not set, null is returned if an xml cannot load.
     * 
     * @return the config file
     * @since 2.2
     */
    public static XmlConfig getXmlConfig() {
        return theXmlConfig;
    }

    /**
     * @see org.eclipse.ui.IStartup#earlyStartup()
     */
    @Override
    public void earlyStartup() {
        // This is needed to kickstart the plugin, this will call the
        // constructor on this class again
    }

    /**
     * Log and exception to the console
     * 
     * @param message to display
     * @param ex cause, can be null
     * @since 1.1
     */
    public static void logException(String message,Throwable ex) {
        thePlugin.getLog().log(new Status(Status.ERROR,PLUGIN_ID,Status.ERROR,message,ex));
    }

    /**
     * Log an message.
     * 
     * This will only log a message if debugging is switched on.
     * 
     * @param message to log
     * @since 2.4
     */
    public static void logMessage(String message) {
        thePlugin.getLog().log(new Status(Status.INFO,PLUGIN_ID,Status.INFO,message,null));
    }

    /**
     * Call the UDC (Usage Data Collector)
     * 
     * This automatically adds the "plugin" to the end of the name
     * 
     * @param pluginName to register
     * @param product of which the licence belongs to
     * @since 2.4
     */
    public static void registerPlugin(String pluginName,Products product) {
        thePlugin.registerPluginImpl(pluginName,product);
    }

    /**
     * Call the UDC
     * 
     * @param pluginName to register
     * @since 2.4
     */
    private void registerPluginImpl(String pluginName,Products product) {
        StringBuilder builder = new StringBuilder(50);
        builder.append(pluginName);
        builder.append(" plugin");
        builder.append("\t");
        builder.append("Eclipse Version ");
        builder.append(PluginUtils.getEclipseVersion());

        Product internalProduct = null;
        if (getXmlConfig()!=null) {
            internalProduct = getXmlConfig().getProduct(product);
        }

        UDCProcessor.registerProduct(builder.toString(),internalProduct,
            getXmlConfig());
    }

    /**
     * Adds a {@link XmlChangeListener} to this plugin,
     * 
     * @param listener The XmlChangeListener to add.
     * @since 2.5
     */
    public void addXmlChangeListener(XmlChangeListener listener) {
        theXmlChangeListeners.add(listener);
    }

    /**
     * Remove an XmlChangeListener from this plugin
     * 
     * @param listener The listener to remove
     * @since 2.5
     */
    public void removeXmlChangeListener(XmlChangeListener listener) {
        theXmlChangeListeners.remove(listener);
    }

    /**
     * A class to listen for changes on the XmlConfig file.
     * 
     * @since 2.5
     */
    private class XmlConfigChangeListener implements IResourceChangeListener {
        /**
         * @see org.eclipse.core.resources.IResourceChangeListener#resourceChanged(org.eclipse.core.resources.IResourceChangeEvent)
         */
        @Override
        public void resourceChanged(IResourceChangeEvent event) {
            if (theXmlInWorkspace==null) {
                return;
            }

            IResourceDelta resDel =
                event.getDelta().findMember(theXmlInWorkspace.getFullPath());
            if (resDel!=null) {
                switch (resDel.getKind()) {
                case IResourceDelta.CHANGED:
                    // NotifyingXmlConfig temp;
                    // try {
                    // temp = new NotifyingXmlConfig(theXMLConfigFile);
                    /*
                     * Using the JAXB generated equals() check if the XML is the same as the current held XML. If the
                     * two objects are equal it means the change came from within the plugin, therefore do not notify
                     * the listeners, but if the xml's are not equal, the change came externally, therefore set the
                     * xmlconfig to this newly loaded one and notify listeners
                     */
                    // if(!(temp==theXmlConfig)){
                    // theXmlConfig = temp;
                    // for(XmlChangeListener listener: theXmlChangeListeners){
                    // listener.xmlChanged();
                    // }
                    // }
                    // }
                    // catch (InvalidConfigException e) {
                    // e.printStackTrace();
                    // }
                    break;
                case IResourceDelta.REPLACED:
                    reloadConfig();
                    break;

                case IResourceDelta.REMOVED:
                    getPreferenceStore().setValue(DOCFACTO_KEY,"");
                    reloadConfig();
                    break;
                case IResourceDelta.MOVED_TO:
                    theXMLConfigFile = resDel.getMovedToPath().toFile().getAbsolutePath().toString();
                    getPreferenceStore().setValue(DOCFACTO_KEY,theXMLConfigFile);
                    break;
                }
            }
            return;
        }
    }

    /**
     * XmlConfig but notifies listening classes when saved to.
     * 
     * {@docfacto.system could equally be implemented as an adapter if XmlConfig is changed to not allow extension or
     * cant be extended... this would preferable actually, if XmlConfig implemented an interface then the adapter would
     * take the interface on construction and work for any type}
     * 
     * @author kporter - created 25 Nov 2013
     * @since 2.5
     */
    class NotifyingXmlConfig extends XmlConfig {
        public NotifyingXmlConfig() throws InvalidConfigException {
            super();
        }

        public NotifyingXmlConfig(File configFile)
        throws InvalidConfigException {
            super(configFile);
        }

        public NotifyingXmlConfig(String configFilePath)
        throws InvalidConfigException {
            super(configFilePath);
        }

        public NotifyingXmlConfig(URL url) throws InvalidConfigException {
            super(url);
        }

        /**
         * Saves the config file.
         * 
         * This method checks out the docfacto xml if the file is located within the workspace, The listeners are
         * notified that the xml has changed.
         * 
         * @see com.docfacto.config.BaseXmlConfig#save()
         */
        @Override
        public void save() throws DocfactoException {
            IFile iFile = PluginUtils.findIFileInWorkspace(theXMLConfigFile);
            if (iFile!=null) {
                boolean checkedOutIFile = PluginUtils.checkoutFile(iFile);
                if (!checkedOutIFile)
                    throw new DocfactoException("Could not checkout/overwrite Docfacto XML");

                try {
                    iFile.refreshLocal(IResource.DEPTH_ZERO,null);
                }
                catch (CoreException e) {
                    e.printStackTrace();
                }
            }
            super.save();

            for (XmlChangeListener listener:theXmlChangeListeners) {
                listener.xmlChanged();
            }
        }
    }

}
