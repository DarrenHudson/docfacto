package com.docfacto.core;

/**
 * Core constants.
 * 
 * @author dhudson - created 19 Jan 2013
 * @since 2.1
 * @docfacto.adam ignore
 */
public interface IDocfactoCorePlugin {

    public static final String DOCFACTO_IMAGE = "docfacto_DOODLE.png";

    public static final String ADAM_ICON_16 = "Adam16.png";

    public static final String ADAM_ICON_32 = "Adam32.png";

    public static final String BEERMAT_ICON_16 = "Beermat16.png";

    public static final String BEERMAT_ICON_32 = "Beermat32.png";

    public static final String DITA_ICON_16 = "Dita16.png";

    public static final String EDITABLE_ICON_16 = "eDITAble16.png";

    public static final String GRABIT_ICON_16 = "Grabit16.png";

    public static final String GRABIT_ICON_32 = "Grabit32.png";

    public static final String LINKS_ICON_16 = "Links16.png";

    public static final String LINKS_ICON_32 = "Links32.png";

    public static final String TAGLETS_ICON_16 = "Taglets16.png";

    public static final String TAGLETS_ICON_32 = "Taglets32.png";

    public static final String XSD_ICON_16 = "XSD16.png";

    // Add to this list of images so that the core can load them
    public static final String[] IMAGES = {DOCFACTO_IMAGE,ADAM_ICON_16,
        ADAM_ICON_32,
        BEERMAT_ICON_16,BEERMAT_ICON_32,DITA_ICON_16,EDITABLE_ICON_16,
        GRABIT_ICON_16,GRABIT_ICON_32,LINKS_ICON_16,LINKS_ICON_32,
        TAGLETS_ICON_16,TAGLETS_ICON_32,XSD_ICON_16};
}
