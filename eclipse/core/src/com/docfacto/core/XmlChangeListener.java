package com.docfacto.core;

public interface XmlChangeListener {
    /**
     * The XML configuration file has changed
     * @since 2.5
     */
    public void xmlChanged();
    
    /**
     * The XML configuration file has been completely reloaded.
     * @since 2.5
     */
    public void xmlReloaded();
}
