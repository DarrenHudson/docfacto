/*
 * @author dhudson -
 * Created 22 Jan 2013 : 10:30:24
 */

package com.docfacto.core.preference;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.service.prefs.BackingStoreException;

import com.docfacto.common.DocfactoException;
import com.docfacto.config.XmlConfig;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.core.IDocfactoCorePlugin;

/**
 * Core preference panel. Enable the user to select a Docfacto.xml file.
 * 
 * @author dhudson - created 22 Jan 2013
 * @author pemi
 * 
 * @since 2.2
 */
public class CorePreferencePage extends FieldEditorPreferencePage implements
IWorkbenchPreferencePage {

    private FileFieldEditor theFileDialog;
    private final IEclipsePreferences thePrefs;
    private Text theLicensee;
    private Text theExpiryDate;

    /**
     * Create the preference page.
     * 
     * @since 2.2
     */
    public CorePreferencePage() {
        super(FieldEditorPreferencePage.GRID);
        setTitle("Docfacto Configuration");
        thePrefs = InstanceScope.INSTANCE.getNode(DocfactoCorePlugin.PLUGIN_ID);
    }

    /**
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
     */
    @Override
    protected void createFieldEditors() {

        // Check if internal XML is being used
        if (DocfactoCorePlugin.getDefault().isUsingInternalXML()) {
            // display error message above page
            setMessage("Using default configuration settings", WARNING);
        }

        // Create the field editors
        theFileDialog = new FileFieldEditor(DocfactoCorePlugin.DOCFACTO_KEY, "File path: ", getFieldEditorParent());
        theFileDialog.setFileExtensions(new String[] {"*.xml"});

        addField(theFileDialog);

        Composite parent = getFieldEditorParent();

        addLinePadding(parent, 2);
        GridData data = new GridData();
        data.horizontalSpan = 3;

        Label label = new Label(parent, SWT.NONE);
        label.setText("Licence Information");
        label.setLayoutData(data);

        FontData fontData = label.getFont().getFontData()[0];
        Font font = new Font(Display.getCurrent(), new FontData(fontData.getName(), fontData.getHeight(), SWT.BOLD));
        label.setFont(font);

        data = new GridData();
        label = new Label(parent, SWT.NONE);
        label.setText("licensee");
        label.setLayoutData(data);

        data = new GridData();
        data.horizontalSpan = 1;
        data.horizontalAlignment = GridData.FILL;
        theLicensee = new Text(parent, SWT.NONE);
        theLicensee.setLayoutData(data);

        data = new GridData();
        data.horizontalSpan = 1;
        // Its a padding label
        new Label(parent, SWT.NONE);

        data = new GridData();
        label = new Label(parent, SWT.NONE);
        label.setText("Expiry Date");
        label.setLayoutData(data);

        data = new GridData();
        data.horizontalSpan = 1;
        data.horizontalAlignment = GridData.FILL;
        theExpiryDate = new Text(parent, SWT.NONE);
        theExpiryDate.setLayoutData(data);

        data = new GridData();
        data.horizontalSpan = 1;
        // Its a padding label
        new Label(parent, SWT.NONE);

        // Create some distance between the Licence info and download info
        addLinePadding(parent, 3);

        GridData imageData = new GridData();
        imageData.verticalSpan = 2;
        Label image = new Label(parent, SWT.NONE);
        image.setImage(DocfactoCorePlugin.getImage(IDocfactoCorePlugin.DOCFACTO_IMAGE));
        image.setLayoutData(imageData);

        data = new GridData();
        data.horizontalAlignment = GridData.FILL;
        data.horizontalSpan = 2;

        Link link = new Link(parent, SWT.NONE);
        link.setText("Docfacto runtime as well as Docfacto.xml, can be downloaded from <a href=\"http://www.docfacto.com/download\">here</a>");
        link.addSelectionListener(new SelectionListener() {
            /**
             * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
             */
            @Override
            public void widgetDefaultSelected(SelectionEvent event) {
            }

            /**
             * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent event) {
                // Launch the default OS browser ..
                org.eclipse.swt.program.Program
                    .launch("http://docfacto.com/download?utm_source=eclipse&utm_medium=plugin&utm_content=link&utm_campaign=toolkit");
            }
        });

        font =
            new Font(Display.getCurrent(), new FontData(fontData.getName(),
                fontData
                    .getHeight()+2, SWT.NORMAL));
        link.setFont(font);

        link = new Link(parent, SWT.NONE);
        link.setText("To submit a feature request or bug report please email <a href=\"mailto:cases@docfacto.fogbugz.com\">cases@docfacto.fogbugz.com</a>");
        link.addSelectionListener(new SelectionListener() {

            /**
             * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
             */
            @Override
            public void widgetDefaultSelected(SelectionEvent event) {
            }

            @Override
            public void widgetSelected(SelectionEvent event) {
                // Launch the default email client ..
                org.eclipse.swt.program.Program
                    .launch("mailto:cases@docfacto.fogbugz.com");
            }
        });
        link.setFont(font);
        link.setLayoutData(data);

        String path = DocfactoCorePlugin.getDefault().getXMLConfigFile(); // thePrefs.get(DocfactoCorePlugin.DOCFACTO_KEY,null);
        if (path!=null&&!DocfactoCorePlugin.getDefault().isUsingInternalXML()) {
            validateXml(path);
        }
    }

    /**
     * Add some empty labels to pad the grid out a bit
     * 
     * @param parent composite parent
     * @param lines of padding required
     * @since 2.4
     */
    private void addLinePadding(Composite parent,int lines) {
        for (int i = 0;i<lines;i++) {
            GridData data = new GridData();
            data.horizontalSpan = 3;
            Label label = new Label(parent, SWT.NONE);
            label.setLayoutData(data);
        }
    }

    /**
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    public void init(IWorkbench workbench) {
        // Initialize the preference page
        setPreferenceStore(DocfactoCorePlugin.getDefault().getPreferenceStore());
        setDescription("A configuration file 'Docfacto.xml' is required to run plugin");
    }

    /**
     * Validate the Docfacto config xml, and if good, it it back in the core plugin
     * 
     * @param path for the Docfacto.xml
     * @since 2.1
     */
    private boolean validateXml(String path) {
        boolean success = false;
        try {
            final XmlConfig config = new XmlConfig(path);

            // Save the XML path to store
            thePrefs.put(DocfactoCorePlugin.DOCFACTO_KEY, path);
            thePrefs.flush();

            // Lets update the Licence information
            theLicensee.setText(config.getLicenseConfig().getLicensee());
            // theExpiryDate.setText(config.getLicenseConfig().getExpiryDate());

            // clear preference page warning
            if (!DocfactoCorePlugin.getDefault().isUsingInternalXML()) {
                // remove error message above page
                setMessage(null);
                setErrorMessage(null);
            }

            success = true;

        }
        catch (final DocfactoException ex) {
            // store error
            DocfactoCorePlugin.getDefault().getPreferenceStore()
                .putValue(DocfactoCorePlugin.VALIDATION_ERROR, ex.getMessage());
            DocfactoCorePlugin.logException("Docfacto.xml invalid", ex);

            // create a pop-up message
            Status status =
                new Status(IStatus.ERROR, DocfactoCorePlugin.PLUGIN_ID,
                    "Docfacto.xml invalid ["+ex.getLocalizedMessage()+"]", ex);

            StatusManager.getManager().handle(status, StatusManager.SHOW);
        }
        catch (final BackingStoreException ex) {
            // store error
            DocfactoCorePlugin.getDefault().getPreferenceStore()
                .putValue(DocfactoCorePlugin.VALIDATION_ERROR, ex.getMessage());
            DocfactoCorePlugin.logException(
                "Unable to save location of Docfacto.xml", ex);

            // create a pop-up message
            Status status =
                new Status(IStatus.ERROR, DocfactoCorePlugin.PLUGIN_ID,
                    "Unable to save location of Docfacto.xml", ex);
            StatusManager.getManager().handle(status, StatusManager.SHOW);
        }

        return success;
    }

    /**
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#performOk()
     */
    @Override
    public boolean performOk() {
        if (theFileDialog.getStringValue().equals(
            DocfactoCorePlugin.getDefault().getPreferenceStore().getString(DocfactoCorePlugin.DOCFACTO_KEY))) {
            return true;
        }

        boolean validated = false;
        if (theFileDialog.getStringValue().isEmpty()) {
            validated = true;
        }
        else {
            validated = validateXml(theFileDialog.getStringValue());
        }

        // save validation state
        if (!validated) {
            setErrorMessage("ERROR: invalid docfacto configuration file");
            DocfactoCorePlugin.getDefault().getPreferenceStore().putValue(DocfactoCorePlugin.KEY_VALIDATED, "false");
        }
        else {
            DocfactoCorePlugin.getDefault().getPreferenceStore().putValue(DocfactoCorePlugin.KEY_VALIDATED, "true");
            DocfactoCorePlugin.getDefault().getPreferenceStore()
                .putValue(DocfactoCorePlugin.DOCFACTO_KEY, theFileDialog.getStringValue());
            DocfactoCorePlugin.getDefault().reloadConfig();
        }

        return validated;
    }
}
