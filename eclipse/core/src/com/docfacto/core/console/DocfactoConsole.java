package com.docfacto.core.console;

import org.eclipse.jface.viewers.ColumnPixelData;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.part.ViewPart;

// http://grepcode.com/file/repository.grepcode.com/java/eclipse.org/3.5.2/org.eclipse.ui/ide/3.5.2/org/eclipse/ui/internal/views/markers/ExtendedMarkersView.java#ExtendedMarkersView.createPartControl%28org.eclipse.ui.internal.views.markers.Composite%29

/**
 * Create a console for Docfacto plugins.  This can be used by all of the plugins.
 *
 * @author dhudson - created 10 May 2013
 * @since 2.3
 */
public class DocfactoConsole extends ViewPart {

    private static final String CONSOLE_ID = "com.docfacto.core.console.DocfactoConsole";
    private TreeViewer theTreeViewer;
    
    /**
     * Constructor.
     * 
     * @since 2.3
     */
    public DocfactoConsole() {

    }

    @Override
    public void createPartControl(Composite parent) {
        parent.setLayout(new FillLayout());
        theTreeViewer = new TreeViewer(new Tree(parent, SWT.H_SCROLL
                 | SWT.V_SCROLL | SWT.MULTI | SWT.FULL_SELECTION));
        
         theTreeViewer.getTree().setLinesVisible(true);
         theTreeViewer.setUseHashlookup(true);
    
         createColumns();
         
         theTreeViewer.setContentProvider(getContentProvider());
    }
    
    private void createColumns() {
        Tree tree = theTreeViewer.getTree();
        TableLayout layout = new TableLayout();
        
        TreeViewerColumn column = getDefaultTreeViewerColumn();
        column.getColumn().setText("Product");
        
        layout.addColumnData(new ColumnPixelData(100,true,true));
        
        column = getDefaultTreeViewerColumn();
        column.setLabelProvider(new MessageColumnLabelProvider());
        column.getColumn().setText("Message");
        column.getColumn().setToolTipText("Message");
        
        layout.addColumnData(new ColumnPixelData(500,true,true));
        
        tree.setLinesVisible(true);
        tree.setHeaderVisible(true);
        tree.setLayout(layout);
        tree.layout(true);
    }

    private TreeViewerColumn getDefaultTreeViewerColumn() {
        TreeViewerColumn column = new TreeViewerColumn(theTreeViewer,SWT.NONE);
        column.getColumn().setResizable(true);
        column.getColumn().setMoveable(true);
        
        return column;
    }
    
    private ITreeContentProvider getContentProvider() {
        return new ITreeContentProvider() {

            @Override
            public void dispose() {
                // TODO Auto-generated method stub
                
            }

            @Override
            public void inputChanged(Viewer arg0,Object arg1,Object arg2) {
                // TODO Auto-generated method stub
                
            }

            @Override
            public Object[] getChildren(Object arg0) {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public Object[] getElements(Object arg0) {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public Object getParent(Object arg0) {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public boolean hasChildren(Object arg0) {
                // TODO Auto-generated method stub
                return false;
            }
            
        };
    }
    @Override
    public void setFocus() {
        theTreeViewer.getControl().setFocus();
    }

    

}
