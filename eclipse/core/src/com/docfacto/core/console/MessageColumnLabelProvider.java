package com.docfacto.core.console;

import org.eclipse.jface.viewers.ColumnLabelProvider;

/**
 * Message Column Provider
 *
 * @author dhudson - created 10 May 2013
 * @since 2.3
 */
public class MessageColumnLabelProvider extends ColumnLabelProvider {

    @Override
    public String getText(Object element) {
        return "Hello World";
    }


}
