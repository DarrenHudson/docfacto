package com.docfacto.core.swt;

import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackListener;

/**
 * Default class which implements {@code MouseTrackListener}
 * 
 * This class doesn't have any function other than to make anonymous classes
 * smaller as the implementation only needs to implement the methods required.
 * 
 * @author dhudson - created 20 Jun 2013
 * @since 2.4
 */
public class DefaultMouseTrackListener implements MouseTrackListener {

    /**
     * @see org.eclipse.swt.events.MouseTrackListener#mouseEnter(org.eclipse.swt.events.MouseEvent)
     */
    @Override
    public void mouseEnter(MouseEvent event) {
    }

    /**
     * @see org.eclipse.swt.events.MouseTrackListener#mouseExit(org.eclipse.swt.events.MouseEvent)
     */
    @Override
    public void mouseExit(MouseEvent event) {
    }

    /**
     * @see org.eclipse.swt.events.MouseTrackListener#mouseHover(org.eclipse.swt.events.MouseEvent)
     */
    @Override
    public void mouseHover(MouseEvent event) {
    }

}
