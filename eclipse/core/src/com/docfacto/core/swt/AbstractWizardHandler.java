package com.docfacto.core.swt;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * Abstract class to open wizards from Eclipse commands.
 * 
 * @author dhudson - created 4 Nov 2013
 * @since 2.4
 */
public abstract class AbstractWizardHandler extends AbstractHandler {

    /**
     * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
     */
    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
        INewWizard wizard = getWizard();
        wizard.init(PlatformUI.getWorkbench(),new StructuredSelection());
        WizardDialog dialog =
            new WizardDialog(HandlerUtil.getActiveShell(event),wizard);

        dialog.create();
        dialog.open();

        return null;
    }

    /**
     * Return the wizard to open
     * 
     * @return the wizard to open
     * @since 2.4
     */
    public abstract INewWizard getWizard();
}
