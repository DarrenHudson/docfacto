/*
 * @author dhudson -
 * Created 23 Jan 2013 : 11:45:07
 */

package com.docfacto.core;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.internal.ui.text.spelling.engine.ISpellDictionary;
import org.eclipse.jdt.internal.ui.text.spelling.engine.RankedWordProposal;

import com.docfacto.common.DocfactoException;
import com.docfacto.taglets.TagletLoader;
import com.docfacto.taglets.generated.InternalTagletDefinition;

/**
 * Internal Docfacto dictionary.
 * 
 * This is to be used for Docfacto Product key words
 * 
 * @author dhudson - created 23 Jan 2013
 * @since 2.1
 */
@SuppressWarnings("restriction")
public class DocfactoDictionary implements ISpellDictionary {

    // List of well known words in the Docfacto community,
    private static final String[] WORDS = {"taglet","dhudson","docfacto",
        "adam","pemi","uri","javadoc","damon","plugin","inline","alt","darren",
        "hudson","kelvin","damon","beermat","palette"};

    private final Set<String> theDictionary;
    private final Set<RankedWordProposal> NO_PROPOSALS =
        new HashSet<RankedWordProposal>(
            0);

    /**
     * Create a new instance of <code>DocfactoDictionary</code>.
     */
    public DocfactoDictionary() {
        theDictionary = new HashSet<String>(WORDS.length);

        for (final String word:WORDS) {
            theDictionary.add(word);
        }

        // TODO: Shared instance somewhere ..
        TagletLoader loader;
        try {
            loader = new TagletLoader();
            for (final InternalTagletDefinition tagDef:loader
                .getCustomTaglets()) {
                // Add the taglet names to the
                theDictionary.add("@"+tagDef.getName());
            }
        }
        catch (DocfactoException ex) {
            DocfactoCorePlugin.logException("Unable to load TagletLoader ",ex);
        }
    }

    /**
     * @see org.eclipse.jdt.internal.ui.text.spelling.engine.ISpellDictionary#acceptsWords()
     */
    @Override
    public boolean acceptsWords() {
        return false;
    }

    /**
     * @see org.eclipse.jdt.internal.ui.text.spelling.engine.ISpellDictionary#addWord(java.lang.String)
     */
    @Override
    public void addWord(String word) {
        theDictionary.add(word);
    }

    /**
     * @see org.eclipse.jdt.internal.ui.text.spelling.engine.ISpellDictionary#getProposals(java.lang.String,
     * boolean)
     */
    @Override
    public Set<RankedWordProposal> getProposals(String arg0,boolean arg1) {
        return NO_PROPOSALS;
    }

    /**
     * @see org.eclipse.jdt.internal.ui.text.spelling.engine.ISpellDictionary#isCorrect(java.lang.String)
     */
    @Override
    public boolean isCorrect(String word) {
        return theDictionary.contains(word.toLowerCase());
    }

    /**
     * @see org.eclipse.jdt.internal.ui.text.spelling.engine.ISpellDictionary#isLoaded()
     */
    @Override
    public boolean isLoaded() {
        return true;
    }

    /**
     * @see org.eclipse.jdt.internal.ui.text.spelling.engine.ISpellDictionary#setStripNonLetters(boolean)
     */
    @Override
    public void setStripNonLetters(boolean arg0) {
        // Nothing to do
    }

    /**
     * @see org.eclipse.jdt.internal.ui.text.spelling.engine.ISpellDictionary#unload()
     */
    @Override
    public void unload() {
    }

}
