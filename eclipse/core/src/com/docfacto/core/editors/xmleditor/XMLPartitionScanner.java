package com.docfacto.core.editors.xmleditor;

import org.eclipse.jface.text.rules.IPredicateRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.MultiLineRule;
import org.eclipse.jface.text.rules.RuleBasedPartitionScanner;
import org.eclipse.jface.text.rules.Token;

/**
 * Partition Scanner
 *
 * @author dhudson - created 10 Jul 2013
 * @since 2.4
 */
public class XMLPartitionScanner extends RuleBasedPartitionScanner {
    
	/**
	 * {@value}
	 */
	public final static String XML_COMMENT = "__xml_comment";
	
	/**
	 * {@value}
	 */
	public final static String XML_TAG = "__xml_tag";

	/**
	 * Constructor.
	 * @since 2.4
	 */
	public XMLPartitionScanner() {

		IToken xmlComment = new Token(XML_COMMENT);
		IToken tag = new Token(XML_TAG);

		IPredicateRule[] rules = new IPredicateRule[2];

		rules[0] = new MultiLineRule("<!--", "-->", xmlComment);
		rules[1] = new TagRule(tag);

		setPredicateRules(rules);
	}
}
