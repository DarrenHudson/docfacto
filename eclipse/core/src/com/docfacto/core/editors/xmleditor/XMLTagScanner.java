package com.docfacto.core.editors.xmleditor;

import org.eclipse.jface.text.*;
import org.eclipse.jface.text.rules.*;

/**
 * XMl Tag Scanner.
 *
 * @author dhudson - created 10 Jul 2013
 * @since 2.4
 */
public class XMLTagScanner extends RuleBasedScanner {

	/**
	 * Constructor.
	 * @param manager
	 */
	public XMLTagScanner() {
		IToken string =
			new Token(
				new TextAttribute(ColorManager.getColor(IXMLColorConstants.STRING)));

		IRule[] rules = new IRule[3];

		// Add rule for double quotes
		rules[0] = new SingleLineRule("\"", "\"", string, '\\');
		// Add a rule for single quotes
		rules[1] = new SingleLineRule("'", "'", string, '\\');
		// Add generic whitespace rule.
		rules[2] = new WhitespaceRule(new XMLWhitespaceDetector());

		setRules(rules);
	}
}
