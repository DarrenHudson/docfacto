package com.docfacto.core.editors.xmleditor;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;

/**
 * Rule Based Scanner
 * 
 * @author dhudson - created 10 Jul 2013
 * @since 2.4
 */
public class XMLScanner extends RuleBasedScanner {

    /**
     * Constructor.
     * 
     * @param manager
     */
    public XMLScanner() {
        IToken procInstr = new Token(new TextAttribute(ColorManager.getColor(IXMLColorConstants.PROC_INSTR)));
        
        IRule[] rules = new IRule[2];
        // Add rule for processing instructions
        rules[0] = new SingleLineRule("<?","?>",procInstr);
        // Add generic whitespace rule.
        rules[1] = new WhitespaceRule(new XMLWhitespaceDetector());
        setRules(rules);
    }
}
