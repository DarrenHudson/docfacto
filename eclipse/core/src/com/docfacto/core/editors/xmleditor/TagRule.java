package com.docfacto.core.editors.xmleditor;

import org.eclipse.jface.text.rules.ICharacterScanner;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.MultiLineRule;

/**
 * XML Tag Rule
 *
 * @author dhudson - created 10 Jul 2013
 * @since 2.4
 */
public class TagRule extends MultiLineRule {

	/**
	 * Constructor.
	 * @param token
	 * @since 2.4
	 */
	public TagRule(IToken token) {
		super("<", ">", token);
	}
	
	protected boolean sequenceDetected(
		ICharacterScanner scanner,
		char[] sequence,
		boolean eofAllowed) {
		int c = scanner.read();
		if (sequence[0] == '<') {
			if (c == '?') {
				// processing instruction - abort
				scanner.unread();
				return false;
			}
			if (c == '!') {
				scanner.unread();
				// comment - abort
				return false;
			}
		} else if (sequence[0] == '>') {
			scanner.unread();
		}
		return super.sequenceDetected(scanner, sequence, eofAllowed);
	}
}
