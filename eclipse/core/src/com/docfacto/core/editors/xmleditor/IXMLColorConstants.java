package com.docfacto.core.editors.xmleditor;

import org.eclipse.swt.graphics.RGB;

/**
 * Colour constants
 *
 * @author dhudson - created 10 Jul 2013
 * @since 2.4
 */
public interface IXMLColorConstants {
    
	/**
     * {@value}
	 */
	RGB XML_COMMENT = new RGB(128, 0, 0);
	/**
     * {@value}
	 */
	RGB PROC_INSTR = new RGB(128, 128, 128);
	/**
     * {@value}
	 */
	RGB STRING = new RGB(0, 128, 0);
	/**
     * {@value}
	 */
	RGB DEFAULT = new RGB(0, 0, 0);
	/**
	 * {@value}
	 */
	RGB TAG = new RGB(0, 0, 128);
	
}
