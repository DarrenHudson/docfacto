package com.docfacto.core.editors.xmleditor;

import org.eclipse.jface.text.rules.IWhitespaceDetector;

/**
 * White Space Detector
 *
 * @author dhudson - created 10 Jul 2013
 * @since 2.4
 */
public class XMLWhitespaceDetector implements IWhitespaceDetector {

	public boolean isWhitespace(char c) {
		return (c == ' ' || c == '\t' || c == '\n' || c == '\r');
	}
}
