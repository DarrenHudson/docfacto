package com.docfacto.core.editors.xmleditor;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextDoubleClickStrategy;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;

/**
 * Source Viewer Configuration
 * 
 * @author dhudson - created 10 Jul 2013
 * @since 2.4
 */
public class XMLConfiguration extends SourceViewerConfiguration {
    private XMLDoubleClickStrategy theDoubleClickStrategy;
    private XMLTagScanner theTagScanner;
    private XMLScanner theScanner;

    /**
     * Constructor.
     * 
     * @param colorManager
     */
    public XMLConfiguration() {
    }

    /**
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getConfiguredContentTypes(org.eclipse.jface.text.source.ISourceViewer)
     */
    public String[] getConfiguredContentTypes(ISourceViewer sourceViewer) {
        return new String[] {
            IDocument.DEFAULT_CONTENT_TYPE,XMLPartitionScanner.XML_COMMENT,XMLPartitionScanner.XML_TAG
        };
    }

    /**
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getDoubleClickStrategy(org.eclipse.jface.text.source.ISourceViewer,
     * java.lang.String)
     */
    public ITextDoubleClickStrategy getDoubleClickStrategy(ISourceViewer sourceViewer,String contentType) {
        if (theDoubleClickStrategy==null) {
            theDoubleClickStrategy = new XMLDoubleClickStrategy();
        }
        return theDoubleClickStrategy;
    }

    /**
     * Return the XML Scanner
     * 
     * @return the XML Scanner
     * @since 2.4
     */
    protected XMLScanner getXMLScanner() {
        if (theScanner==null) {
            theScanner = new XMLScanner();
            theScanner.setDefaultReturnToken(
                new Token(new TextAttribute(ColorManager.getColor(IXMLColorConstants.DEFAULT))));
        }
        return theScanner;
    }

    /**
     * Return the tag scanner
     * 
     * @return the Tag Scanner
     * @since 2.4
     */
    protected XMLTagScanner getXMLTagScanner() {
        if (theTagScanner==null) {
            theTagScanner = new XMLTagScanner();
            theTagScanner.setDefaultReturnToken(
                new Token(new TextAttribute(ColorManager.getColor(IXMLColorConstants.TAG))));
        }
        return theTagScanner;
    }

    /**
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getPresentationReconciler(org.eclipse.jface.text.source.ISourceViewer)
     */
    public IPresentationReconciler getPresentationReconciler(
    ISourceViewer sourceViewer) {
        PresentationReconciler reconciler = new PresentationReconciler();

        DefaultDamagerRepairer dr = new DefaultDamagerRepairer(getXMLTagScanner());
        reconciler.setDamager(dr,XMLPartitionScanner.XML_TAG);
        reconciler.setRepairer(dr,XMLPartitionScanner.XML_TAG);

        dr = new DefaultDamagerRepairer(getXMLScanner());
        reconciler.setDamager(dr,IDocument.DEFAULT_CONTENT_TYPE);
        reconciler.setRepairer(dr,IDocument.DEFAULT_CONTENT_TYPE);

        NonRuleBasedDamagerRepairer ndr = new NonRuleBasedDamagerRepairer(
            new TextAttribute(ColorManager.getColor(IXMLColorConstants.XML_COMMENT)));
        reconciler.setDamager(ndr,XMLPartitionScanner.XML_COMMENT);
        reconciler.setRepairer(ndr,XMLPartitionScanner.XML_COMMENT);

        return reconciler;
    }

}