package com.docfacto.core.editors.xmleditor;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;

/**
 * XML text editor colour manager
 * 
 * @author dhudson - created 10 Jul 2013
 * @since 2.4
 */
public class ColorManager {

    protected static Map<RGB,Color> theColourTable = new HashMap<RGB,Color>();

    /**
     * Dispose of the colours
     * 
     * @since 2.4
     */
    public static void dispose() {
        for(Color color : theColourTable.values()) {
            color.dispose();
        }
    }

    /**
     * Get a color from the map, else cache it
     * 
     * @param rgb
     * @return a {@code Color} representing the RGB value
     * @since 2.4
     */
    public static Color getColor(RGB rgb) {
        Color color = (Color)theColourTable.get(rgb);
        if (color==null) {
            color = new Color(Display.getCurrent(),rgb);
            theColourTable.put(rgb,color);
        }
        return color;
    }
}
