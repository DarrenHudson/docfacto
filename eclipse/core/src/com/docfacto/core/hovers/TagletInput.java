package com.docfacto.core.hovers;

import com.docfacto.javadoc.TagParser;

/**
 * This object is return from HoverMarshaller and is set on the set input
 * methods
 * 
 * @author dhudson - created 20 Sep 2013
 * @since 2.4
 */
public class TagletInput {

    private final TagParser theParser;

    /**
     * Constructor.
     * 
     * @param parser A taglet parser
     * @since 2.4
     */
    public TagletInput(TagParser parser) {
        theParser = parser;
    }

    /**
     * Return the taglet parser
     * 
     * @return the taglet parser
     * @since 2.4
     */
    public TagParser getParser() {
        return theParser;
    }
}
