package com.docfacto.core.hovers;

import java.io.File;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

import com.docfacto.core.DocfactoCorePlugin;

/**
 * A Launch action which will launch the default editor
 * 
 * @author dhudson - created 20 Sep 2013
 * @since 2.4
 */
public class LaunchAction extends Action {

    private final DualAbstractInformationControl theInformationControl;
    private String theFileToLaunch;

    /**
     * Constructor.
     * 
     * @param imageDescriptor button image
     * @param text button text
     * @param informationControl control
     */
    public LaunchAction(ImageDescriptor imageDescriptor,String text,
    DualAbstractInformationControl informationControl) {
        theInformationControl = informationControl;

        setText(text);
        setImageDescriptor(imageDescriptor);
    }

    /**
     * Constructor.
     * 
     * @param informationControl control
     */
    public LaunchAction(DualAbstractInformationControl informationControl) {
        theInformationControl = informationControl;
        setText("Open");
        setImageDescriptor(PlatformUI.getWorkbench().getSharedImages()
            .getImageDescriptor(IDE.SharedImages.IMG_OPEN_MARKER));
    }

    /**
     * Location of the file to open in a editor
     * 
     * @param fileToLaunch the external file to launch
     * @since 2.4
     */
    public void setFileToLaunch(String fileToLaunch) {
        theFileToLaunch = fileToLaunch;
    }

    /**
     * This method can be used to change the editor button
     * 
     * @since 2.5
     */
    public void changeImage() {
        if (theFileToLaunch==null) {
            return;
        }

        IEditorDescriptor desc[] = PlatformUI.getWorkbench().
            getEditorRegistry().getEditors(theFileToLaunch);

        if (desc!=null&&desc.length>0) {
            if (desc.length==1) {
                setImage(desc[0]);
            }

            for (IEditorDescriptor editor:desc) {
                if (editor.getId().startsWith("com.docfacto")) {
                    setImage(editor);
                    return;
                }
            }

            // Set the default image
            setImage(desc[0]);
        }
    }

    /**
     * Use the editor descriptor image to set the image. If no image then the
     * standard open image will be used.
     * 
     * @param descriptor
     * @since 2.4
     */
    private void setImage(IEditorDescriptor descriptor) {
        if (descriptor.getImageDescriptor()!=null) {
            setImageDescriptor(descriptor.getImageDescriptor());
        }
    }

    /**
     * @see org.eclipse.jface.action.Action#run()
     */
    @Override
    public void run() {
        if (theFileToLaunch==null) {
            return;
        }

        File fileToOpen =
            new File(
                theFileToLaunch);

        IFileStore fileStore =
            EFS.getLocalFileSystem().getStore(fileToOpen.toURI());
        IWorkbenchPage page =
            PlatformUI.getWorkbench().getActiveWorkbenchWindow()
                .getActivePage();

        try {
            IDE.openEditorOnFileStore(page,fileStore);
            if (theInformationControl!=null) {
                theInformationControl.lostFocus();
                theInformationControl.dispose();
            }
        }
        catch (PartInitException ex) {
            DocfactoCorePlugin.logException("Unable to open editor",ex);
        }
    }
}
