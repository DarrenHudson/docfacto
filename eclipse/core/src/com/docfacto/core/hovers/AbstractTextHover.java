package com.docfacto.core.hovers;

import org.eclipse.core.resources.IResource;
import org.eclipse.jdt.core.ICodeAssist;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.NodeFinder;
import org.eclipse.jdt.ui.IWorkingCopyManager;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jdt.ui.SharedASTProvider;
import org.eclipse.jdt.ui.text.java.hover.IJavaEditorTextHover;
import org.eclipse.jface.text.DefaultInformationControl;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.IInformationControlCreatorExtension;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextHoverExtension;
import org.eclipse.jface.text.ITextHoverExtension2;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.editors.text.EditorsUI;

import com.docfacto.core.utils.CompleteNodeFinder;

/**
 * Abstract class to handle default hovers.
 * 
 * @author dhudson - created 16 Sep 2013
 * @since 2.4
 */
public abstract class AbstractTextHover implements IJavaEditorTextHover,
ITextHoverExtension,ITextHoverExtension2,IInformationControlCreatorExtension {

    private IEditorPart theEditorPart;

    /**
     * @see org.eclipse.jface.text.ITextHover#getHoverInfo(org.eclipse.jface.text.ITextViewer,
     * org.eclipse.jface.text.IRegion)
     */
    @Override
    public String getHoverInfo(ITextViewer viewer,IRegion region) {
        // Will never be called as getHoverInfo2 is used
        return null;
    }

    /**
     * @see org.eclipse.jface.text.ITextHoverExtension2#getHoverInfo2(org.eclipse.jface.text.ITextViewer,
     * org.eclipse.jface.text.IRegion)
     */
    @Override
    public abstract Object getHoverInfo2(ITextViewer viewer,IRegion region);

    /**
     * @see org.eclipse.jface.text.ITextHover#getHoverRegion(org.eclipse.jface.text.ITextViewer,
     * int)
     */
    @Override
    public IRegion getHoverRegion(ITextViewer viewer,int offset) {
        return null;
    }

    /**
     * @see org.eclipse.jdt.ui.text.java.hover.IJavaEditorTextHover#setEditor(org.eclipse.ui.IEditorPart)
     */
    @Override
    public void setEditor(IEditorPart editor) {
        theEditorPart = editor;

    }

    /**
     * Return the editor
     * 
     * @return the editor or null
     * @since 2.4
     */
    public IEditorPart getEditor() {
        return theEditorPart;
    }

    /**
     * @see org.eclipse.jface.text.ITextHoverExtension#getHoverControlCreator()
     */
    @Override
    public IInformationControlCreator getHoverControlCreator() {
        return new IInformationControlCreator() {
            public IInformationControl createInformationControl(Shell parent) {
                return new DefaultInformationControl(parent,
                    EditorsUI.getTooltipAffordanceString());
            }
        };
    }

    /**
     * This method needs to be over-ridden by the implementation
     * 
     * @return a DefaultInformationControl
     * @since 2.4
     */
    public IInformationControlCreator getInformationPresenterControlCreator() {
        return new IInformationControlCreator() {
            public IInformationControl createInformationControl(Shell shell) {
                return new DefaultInformationControl(shell,true);
            }
        };
    }

    /**
     * @see org.eclipse.jface.text.IInformationControlCreatorExtension#canReplace(org.eclipse.jface.text.IInformationControlCreator)
     */
    @Override
    public boolean canReplace(IInformationControlCreator controlCreator) {
        return false;
    }

    /**
     * @see org.eclipse.jface.text.IInformationControlCreatorExtension#canReuse(org.eclipse.jface.text.IInformationControl)
     */
    @Override
    public boolean canReuse(IInformationControl control) {
        return false;
    }

    /**
     * Utility method to return the code currently being worked on
     * 
     * @return the code being worked on
     * @since 2.4
     */
    public ICodeAssist getCodeAssist() {
        if (theEditorPart!=null) {
            IEditorInput input = theEditorPart.getEditorInput();
            IWorkingCopyManager manager = JavaUI.getWorkingCopyManager();
            return manager.getWorkingCopy(input);
        }

        return null;
    }

    /**
     * Get the resource of the file that the over is on
     * 
     * @return the resource or null
     * @since 2.4
     */
    public IResource getResource() {
        IEditorPart editor = getEditor();
        ICompilationUnit cu =
            JavaUI.getWorkingCopyManager().getWorkingCopy(
                editor.getEditorInput());

        // Nothing to do
        if (cu==null) {
            return null;
        }

        return cu.getResource();
    }

    /**
     * Root is either a ICompliationUnit or a IClassFile
     * 
     * @return the type
     * @since 2.4
     */
    public ITypeRoot getEditorInputJavaElement() {
        IEditorPart editor = getEditor();
        if (editor!=null) {
            return JavaUI.getEditorInputTypeRoot(editor.getEditorInput());
        }
        return null;
    }

    /**
     * Returns the Java elements at the given hover region.
     * 
     * {@docfacto.note can return null}
     * 
     * @param textViewer the text viewer
     * @param hoverRegion region to check
     * @return An array of Java Elements if present
     * @since 2.4
     */
    public IJavaElement[] getJavaElementsAt(ITextViewer textViewer,
    IRegion hoverRegion) {
        /*
         * The region should be a word region an not of length 0. This check is
         * needed because codeSelect(...) also finds the Java element if the
         * offset is behind the word.
         */
        if (hoverRegion.getLength()==0) {
            return null;
        }

        ICodeAssist resolve = getCodeAssist();
        if (resolve!=null) {
            try {
                return resolve.codeSelect(hoverRegion.getOffset(),
                    hoverRegion.getLength());
            }
            catch (JavaModelException x) {
                return null;
            }
        }
        return null;
    }

    /**
     * Return if possible the AST node for the region
     * 
     * @param editorInputElement editor
     * @param hoverRegion region to process
     * @return the AST node that the region is hovered over
     * @since 2.4
     */
    public ASTNode getHoveredASTNode(ITypeRoot editorInputElement,
    IRegion hoverRegion) {

        CompilationUnit unit = getCompilationUnit(editorInputElement);

        if (unit==null) {
            return null;
        }

        return NodeFinder.perform(unit,hoverRegion.getOffset(),
            hoverRegion.getLength());
    }

    /**
     * Return the AST node including comments
     * 
     * @param editorInputElement current editor
     * @param hoverRegion to check
     * @return the ASTNode if any
     * @since 2.5
     */
    public ASTNode getHoveredASTNodeIncludingComments(
    ITypeRoot editorInputElement,
    IRegion hoverRegion) {

        CompilationUnit unit = getCompilationUnit(editorInputElement);

        if (unit==null) {
            return null;
        }

        return CompleteNodeFinder.perform(unit,hoverRegion);
    }

    /**
     * Return the compilation unit for the editor
     * 
     * @param editorInputElement editor input
     * @return the CompilationUnit or null
     * @since 2.5
     */
    private CompilationUnit getCompilationUnit(ITypeRoot editorInputElement) {
        if (editorInputElement==null) {
            return null;
        }

        return SharedASTProvider.getAST(editorInputElement,
            SharedASTProvider.WAIT_ACTIVE_ONLY,null);
    }
}
