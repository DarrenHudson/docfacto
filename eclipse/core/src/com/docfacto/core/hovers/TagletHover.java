package com.docfacto.core.hovers;

import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Comment;
import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;

import com.docfacto.common.StringUtils;
import com.docfacto.javadoc.TagParser;

/**
 * Provide hover information for taglets.
 * 
 * 
 * @author dhudson - created 16 Sep 2013
 * @since 2.4
 */
public abstract class TagletHover extends AbstractTextHover {

    /**
     * @see com.docfacto.taglets.plugin.hover.AbstractTextHover#getHoverInfo2(org.eclipse.jface.text.ITextViewer,
     * org.eclipse.jface.text.IRegion)
     */
    @Override
    public Object getHoverInfo2(ITextViewer viewer,IRegion region) {
        
        IJavaElement[] elements = getJavaElementsAt(viewer,region);

        if (elements!=null&&elements.length>0) {
            // Nothing to do here as the hover is a Java Element
            return null;
        }

        ASTNode node = null;
        if (requiresComments()) {
            node =
                getHoveredASTNodeIncludingComments(getEditorInputJavaElement(),
                    region);

            if(node == null) {
                return null;
            }
            
            if(node.getNodeType()== ASTNode.JAVADOC) {
                return getHoverInfo(viewer,region,(Comment)node);
            }
            
            // At this point its a Javadoc, Block or Line
            // If its line, just fall through and pick up the
            // Javadoc type..
            if (node.getNodeType()!=ASTNode.JAVADOC) {
                if(node.getNodeType() == ASTNode.BLOCK_COMMENT) {
                    return getHoverInfo(viewer,region,(Comment)node);
                }
                // Its not for us
                return null;
            }
        }

        // This will pick up Javadoc..
        node =
            getHoveredASTNode(getEditorInputJavaElement(),region);

        if (node==null) {
            // Nothing to do...
            return null;
        }

        // Lets get the AST Comment node
        TagElement tagElement = null;

        if (node.getNodeType()==ASTNode.TAG_ELEMENT) {
            tagElement = (TagElement)node;
        }
        else if (node.getNodeType()==ASTNode.TEXT_ELEMENT) {
            ASTNode parent = node.getParent();

            if (parent.getNodeType()==ASTNode.TAG_ELEMENT) {
                tagElement = (TagElement)parent;
            }
        }

        // Nothing to do
        if (tagElement==null||StringUtils.nullOrEmpty(tagElement.getTagName())) {
            return null;
        }

        String tagText = tagElement.toString();

        TagParser parser =
            new TagParser(
                tagText.substring(tagElement
                    .getTagName().length()+1,
                    tagText.length()-1));

        // Remove the '@'
        parser.setTagName(tagElement.getTagName().substring(1));
        if (tagText.startsWith("{")) {
            parser.setInLine();
        }

        // Add the element as an attachment
        parser.attach(tagElement);

        return getHoverInfo(viewer,region,parser);
    }

    /**
     * Implementors of this class should check for the taglet name in the parser
     * and return hover information if required, else return null
     * 
     * {@docfacto.note An attachment of TagElement will be set }
     * 
     * @param viewer the viewer
     * @param region the region
     * @param parser qualified tag parser
     * @return hover control information
     * @since 2.4
     */
    public abstract Object getHoverInfo(ITextViewer viewer,IRegion region,
    TagParser parser);

    /**
     * Implementors of this class has set the requiresComments to true.
     * 
     * {@docfacto.note Comment can be Javadoc, line or block }
     * 
     * @param viewer current source viewer
     * @param region the mouse region
     * @param node the node at the region
     * @return hover control information
     * @since 2.4
     */
    public abstract Object getHoverInfo(ITextViewer viewer,IRegion region,
    Comment node);

    /**
     * Check to see if line and block comments are required
     * 
     * @return true if line and block comments are required
     * @since 2.5
     */
    public abstract boolean requiresComments();
}
