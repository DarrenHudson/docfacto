package com.docfacto.core.hovers;

import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;

/**
 * Abstract browser information control, which display html type content.
 * 
 * @author dhudson - created 13 Nov 2013
 * @since 2.5
 */
public abstract class AbstractBrowserInformationControl extends
DualAbstractInformationControl {

    private Browser theBrowser;

    /**
     * Constructor.
     * 
     * {@docfacto.note Super class must call create() }
     * 
     * @param parentShell parent shell
     * 
     * @since 2.4
     */
    public AbstractBrowserInformationControl(Shell parentShell) {
        super(parentShell);
    }

    /**
     * @see com.docfacto.core.hovers.DualAbstractInformationControl#createContent(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void createContent(Composite parent) {
        theBrowser = new Browser(parent,SWT.NONE);
        theBrowser.setMenu(new Menu(getShell(),SWT.NONE));
    }

    /**
     * @see com.docfacto.core.hovers.DualAbstractInformationControl#getToolBarActions()
     */
    @Override
    public List<Action> getToolBarActions() {
        return null;
    }

    /**
     * Return the browser created in createContent
     * 
     * @return the browser
     * @since 2.5
     */
    public Browser getBrowser() {
        return theBrowser;
    }
}
