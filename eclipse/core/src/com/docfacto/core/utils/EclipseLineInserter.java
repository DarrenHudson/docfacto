package com.docfacto.core.utils;

import java.io.IOException;

import org.eclipse.core.filebuffers.FileBuffers;
import org.eclipse.core.filebuffers.ITextFileBuffer;
import org.eclipse.core.filebuffers.ITextFileBufferManager;
import org.eclipse.core.filebuffers.LocationKind;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

import com.docfacto.common.DocfactoException;

/**
 * A class which contains a method to insert a line into an ifile within your eclipse workspace.
 * 
 * @author damonli
 */
public class EclipseLineInserter {

	private final IWorkbench workbench;
	
	public EclipseLineInserter(IWorkbench workbench) {
		this.workbench = workbench;
	}
	
	public void insertLineIntoFileInActiveEditor(int lineNumber, String lineToInsert) throws BadLocationException {
		
		IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
        IWorkbenchPage page = window.getActivePage();
		IEditorPart activeEditor = page.getActiveEditor();
		ITextEditor textEditor = (ITextEditor) activeEditor.getAdapter(ITextEditor.class);
		IDocumentProvider documentProvider = textEditor.getDocumentProvider(); 
		FileEditorInput editorInput = (FileEditorInput) activeEditor.getEditorInput();
		IDocument document = documentProvider.getDocument(editorInput);
		
		insertLineIntoDocument(document, lineNumber, lineToInsert);
	}
	
	private void insertLineIntoDocument(IDocument document, int lineNumberInput, String lineToInsert) throws BadLocationException {
			int lineNumber = lineNumberInput-1;
			
			int charOffsetOfSelectedLineNumber = document.getLineOffset(lineNumber);
			int lengthOfSelectedLine = document.getLineLength(lineNumber);
			String selectedLine = document.get(charOffsetOfSelectedLineNumber, lengthOfSelectedLine);
			
			String indentation = "";
			
			if (lineNumber > 0) {
				int charOffsetOfPreviousLine = document.getLineOffset(lineNumber-1);
				int lengthOfPreviousLine = document.getLineLength(lineNumber-1);
				String previousLine = document.get(charOffsetOfPreviousLine, lengthOfPreviousLine);
				
				indentation = calculateIndentationToUseForNewInsertedLine(previousLine, selectedLine);
			}

			String newTextToBeInserted = "";
			newTextToBeInserted += indentation;
			newTextToBeInserted += lineToInsert;
			newTextToBeInserted += "\n";
			newTextToBeInserted += selectedLine;
			
			document.replace(charOffsetOfSelectedLineNumber, lengthOfSelectedLine, newTextToBeInserted);
	}

	private String calculateIndentationToUseForNewInsertedLine(String previousLine, String currentLine) {
		String indentationOfPreviousLine = getIndentationOfString(previousLine);
		String indentationOfCurrentLine = getIndentationOfString(currentLine);
    	if (indentationOfCurrentLine.length() > indentationOfPreviousLine.length())
    		return indentationOfCurrentLine;
    	else
    		return indentationOfPreviousLine;
	}
	
	public void insertLineIntoFileInEditor(IEditorPart editor, int lineNumber, String lineToInsert) throws BadLocationException, EclipseLineInsertException {
		
		ITextEditor textEditor = (ITextEditor) editor.getAdapter(ITextEditor.class);
		
		if (textEditor == null)
			throw new EclipseLineInsertException("Could not insert line into editor as it is not a text editor");
		
		IDocumentProvider documentProvider = textEditor.getDocumentProvider();
		
		FileEditorInput editorInput = (FileEditorInput) editor.getEditorInput();
		IDocument document = documentProvider.getDocument(editorInput);
		
		insertLineIntoDocument(document, lineNumber, lineToInsert);
	}

	/**
	 * Insert a line into a file for the given line number.
	 * 
	 * @param file the file to insert the line into
	 * @param lineNumber the line number to insert the line into
	 * @param lineToInsert the line to insert into the file
	 * @throws CoreException when there was an error refreshing the file
	 * @throws IOException when there was an error reading the contents from the file
	 * @throws DocfactoException when there was a problem inserting the link into the file
	 * @throws BadLocationException 
	 */
	public void insertLineIntoFileInWorkspace(IFile file,int lineNumber,String lineToInsert) throws CoreException, IOException, DocfactoException, BadLocationException {
        // Checkout file first
        boolean checkedOut = PluginUtils.checkoutFile(file);
    
        if (!checkedOut) {
        	throw new DocfactoException("Do not have permission to insert link into file: [" + file.getFullPath().toString() + "]");
        }
        
        IPath targetPath = file.getFullPath();
        
        ITextFileBufferManager bufferManager = FileBuffers.getTextFileBufferManager();
        try {
            bufferManager.connect(targetPath,LocationKind.NORMALIZE,null);
        }
        catch (CoreException e1) {
            e1.printStackTrace();
        }
        ITextFileBuffer textFileBuffer = bufferManager.getTextFileBuffer(targetPath,LocationKind.NORMALIZE);
        IDocument document = textFileBuffer.getDocument();
        
        insertLineIntoDocument(document, lineNumber, lineToInsert);
        
        try {
			textFileBuffer.commit(null, true);
		} catch (CoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        try {
			bufferManager.disconnect(targetPath, LocationKind.NORMALIZE, null);
		} catch (CoreException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
        
        try {
            file.refreshLocal(IResource.DEPTH_ZERO,null);
        }
        catch (CoreException e) {
            e.printStackTrace();
        }
	}
	
	/**
	 * Returns an empty string which is the same length as the indentation at the start of the 
	 * given string.
	 * 
	 * For example, if the string given is: "    int a;" (with a gap of 4 spaces at the start,
	 * the method will return "    " (an empty string of 4 spaces).
	 * 
	 * @param inputString the string to work out the indentation for
	 * @return an empty string the same length as the indentation of the given string.
	 */
	private String getIndentationOfString(String inputString) {
		
		String stringToBeReturned = "";
		
		if (inputString == "" || inputString.trim().isEmpty())
			return "";
		else {
			for (int i=0; i < inputString.length(); i++) {
				char currentChar = inputString.charAt(i);
				
				if (currentChar == ' ')
					stringToBeReturned += " ";
				else if (currentChar == '\t')
					stringToBeReturned += '\t';
				else
					return stringToBeReturned;
			}
			
			return stringToBeReturned;
		}
	}
}
