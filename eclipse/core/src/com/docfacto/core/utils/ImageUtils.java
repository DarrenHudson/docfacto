package com.docfacto.core.utils;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DirectColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.eclipse.jface.resource.CompositeImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.graphics.Transform;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.docfacto.common.IOUtils;

/**
 * Collection of static methods to help with images
 * 
 * @author dhudson - created 19 Sep 2013
 * @since 2.4
 */
public class ImageUtils {

    /**
     * Style constant for placing decorator image in top left corner of base
     * image.
     */
    public static final int TOP_LEFT = 1;
    /**
     * Style constant for placing decorator image in top right corner of base
     * image.
     */
    public static final int TOP_RIGHT = 2;
    /**
     * Style constant for placing decorator image in bottom left corner of base
     * image.
     */
    public static final int BOTTOM_LEFT = 3;
    /**
     * Style constant for placing decorator image in bottom right corner of base
     * image.
     */
    public static final int BOTTOM_RIGHT = 4;

    /**
     * Return a shared image, the keys reside in {@code ISharedImages}
     * 
     * @param key of the image
     * @return a shared image
     * @since 2.1
     */
    public static Image getSharedImage(String key) {
        return PlatformUI.getWorkbench().getSharedImages().getImage(key);
    }

    /**
     * Get a plugin image
     * 
     * @param symbolicName of the plugin
     * @param imagePath path of the image
     * @return the image or null
     * @since 2.1
     */
    public static Image getPluginImage(String symbolicName,String imagePath) {
        URL url = PluginUtils.getResourceURL(symbolicName,imagePath);
        if (url!=null) {
            return getImageFromURL(url);
        }
        return null;
    }

    /**
     * Load an image from a file path
     * 
     * @param path of the image to load
     * @return an image or null, if its a corrupt image
     * @throws IOException if unable to load the image
     * @since 2.4
     */
    public static Image getImageFromFile(String path) throws IOException {
        File file = new File(path);
        FileInputStream fis = new FileInputStream(file);
        return getImageFromStream(fis);
    }

    /**
     * Given a URL, an Image is returned.
     * 
     * null is a possible return if the URL is incorrect.
     * 
     * @param url The location of the image, as a URL
     * @return An Image
     * @since 2.1 (Merged class)
     */
    public static Image getImageFromURL(URL url) {
        try {
            InputStream stream = url.openStream();
            try {
                return getImageFromStream(stream);
            }
            finally {
                IOUtils.close(stream);
            }
        }
        catch (Throwable ignore) {
            // Ignore
        }
        return null;
    }

    /**
     * Gets an image from an InputStream
     * 
     * @param stream The InputStream with the image
     * @return An image
     * @throws IOException If input is incorrect
     * @since 2.1
     */
    public static Image getImageFromStream(InputStream stream)
    throws IOException {
        try {
            Display disp = Display.getDefault();
            ImageData imageData = new ImageData(stream);
            if (imageData.transparentPixel>0) {
                return new Image(disp,imageData,imageData.getTransparencyMask());
            }
            return new Image(disp,imageData);
        }
        finally {
            IOUtils.close(stream);
        }
    }

    /**
     * Convert a SWT image to an AWT image
     * 
     * @param data to convert
     * @return a buffered image
     * @since 2.2
     */
    public static BufferedImage convertToAWT(ImageData data) {
        ColorModel colorModel = null;
        PaletteData palette = data.palette;
        if (palette.isDirect) {
            colorModel = new DirectColorModel(data.depth,palette.redMask,
                palette.greenMask,palette.blueMask);
            BufferedImage bufferedImage = new BufferedImage(colorModel,
                colorModel.createCompatibleWritableRaster(data.width,
                    data.height),false,null);
            WritableRaster raster = bufferedImage.getRaster();
            int[] pixelArray = new int[3];
            for (int y = 0;y<data.height;y++) {
                for (int x = 0;x<data.width;x++) {
                    int pixel = data.getPixel(x,y);
                    RGB rgb = palette.getRGB(pixel);
                    pixelArray[0] = rgb.red;
                    pixelArray[1] = rgb.green;
                    pixelArray[2] = rgb.blue;
                    raster.setPixels(x,y,1,1,pixelArray);
                }
            }
            return bufferedImage;
        }
        else {
            RGB[] rgbs = palette.getRGBs();
            byte[] red = new byte[rgbs.length];
            byte[] green = new byte[rgbs.length];
            byte[] blue = new byte[rgbs.length];
            for (int i = 0;i<rgbs.length;i++) {
                RGB rgb = rgbs[i];
                red[i] = (byte)rgb.red;
                green[i] = (byte)rgb.green;
                blue[i] = (byte)rgb.blue;
            }
            if (data.transparentPixel!=-1) {
                colorModel = new IndexColorModel(data.depth,rgbs.length,red,
                    green,blue,data.transparentPixel);
            }
            else {
                colorModel = new IndexColorModel(data.depth,rgbs.length,red,
                    green,blue);
            }
            BufferedImage bufferedImage = new BufferedImage(colorModel,
                colorModel.createCompatibleWritableRaster(data.width,
                    data.height),false,null);
            WritableRaster raster = bufferedImage.getRaster();
            int[] pixelArray = new int[1];
            for (int y = 0;y<data.height;y++) {
                for (int x = 0;x<data.width;x++) {
                    int pixel = data.getPixel(x,y);
                    pixelArray[0] = pixel;
                    raster.setPixel(x,y,pixelArray);
                }
            }
            return bufferedImage;
        }
    }

    /**
     * Convert a AWT image to a SWT image
     * 
     * @param bufferedImage
     * @return SWT image
     * @since 2.2
     */
    public static ImageData convertToSWT(BufferedImage bufferedImage) {
        if (bufferedImage.getColorModel() instanceof DirectColorModel) {
            DirectColorModel colorModel = (DirectColorModel)bufferedImage
                .getColorModel();
            PaletteData palette = new PaletteData(colorModel.getRedMask(),
                colorModel.getGreenMask(),colorModel.getBlueMask());
            ImageData data = new ImageData(bufferedImage.getWidth(),
                bufferedImage.getHeight(),colorModel.getPixelSize(),
                palette);
            WritableRaster raster = bufferedImage.getRaster();
            int[] pixelArray = new int[3];
            for (int y = 0;y<data.height;y++) {
                for (int x = 0;x<data.width;x++) {
                    raster.getPixel(x,y,pixelArray);
                    int pixel = palette.getPixel(new RGB(pixelArray[0],
                        pixelArray[1],pixelArray[2]));
                    data.setPixel(x,y,pixel);
                }
            }
            return data;
        }
        else if (bufferedImage.getColorModel() instanceof IndexColorModel) {
            IndexColorModel colorModel = (IndexColorModel)bufferedImage
                .getColorModel();
            int size = colorModel.getMapSize();
            byte[] reds = new byte[size];
            byte[] greens = new byte[size];
            byte[] blues = new byte[size];
            colorModel.getReds(reds);
            colorModel.getGreens(greens);
            colorModel.getBlues(blues);
            RGB[] rgbs = new RGB[size];
            for (int i = 0;i<rgbs.length;i++) {
                rgbs[i] = new RGB(reds[i]&0xFF,greens[i]&0xFF,
                    blues[i]&0xFF);
            }
            PaletteData palette = new PaletteData(rgbs);
            ImageData data = new ImageData(bufferedImage.getWidth(),
                bufferedImage.getHeight(),colorModel.getPixelSize(),
                palette);
            data.transparentPixel = colorModel.getTransparentPixel();
            WritableRaster raster = bufferedImage.getRaster();
            int[] pixelArray = new int[1];
            for (int y = 0;y<data.height;y++) {
                for (int x = 0;x<data.width;x++) {
                    raster.getPixel(x,y,pixelArray);
                    data.setPixel(x,y,pixelArray[0]);
                }
            }
            return data;
        }
        return null;
    }

    /**
     * Create a drop shadow
     * 
     * @param originalImageData The original image. Transparency information
     * will be ignored.
     * @param color The color of the drop shadow
     * @param radius The radius of the drop shadow in pixels
     * @param highlightRadius The radius of the highlight area
     * @param opacity The opacity of the drop shadow
     * @return The drop shadowed image. This image data will be larger than the
     * original. The same image data will be returned if the shadow radius is 0,
     * or null if an error occurred.
     * @since 2
     */
    public static ImageData dropShadow(ImageData originalImageData,
    Color color,int radius,int highlightRadius,int opacity) {
        /*
         * This method will create a drop shadow to the bottom-right of an
         * existing image. This drop shadow is created by creating an altered
         * one-sided glow, and shifting its position around the image. See the
         * Glow class for more details of how the glow is calculated.
         */
        if (originalImageData==null||color==null) {
            return null;
        }

        if (radius==0) {
            return originalImageData;
        }

        int shift = (int)(radius*1.5); // distance to shift "glow" from image
        // the percent increase in color intensity in the highlight radius
        double highlightRadiusIncrease = radius<highlightRadius*2 ? .15
            : radius<highlightRadius*3 ? .09 : .02;
        opacity = opacity>255 ? 255 : opacity<0 ? 0 : opacity;
        // prepare new image data with 24-bit direct palette to hold shadowed
        // copy of image
        ImageData newImageData = new ImageData(originalImageData.width+radius
            *2,originalImageData.height+radius*2,24,
            new PaletteData(0xFF,0xFF00,0xFF0000));
        int[] pixels = new int[originalImageData.width];
        // copy image data
        for (int row = radius;row<radius+originalImageData.height;row++) {
            originalImageData.getPixels(0,row-radius,
                originalImageData.width,pixels,0);
            for (int col = 0;col<pixels.length;col++)
                pixels[col] = newImageData.palette
                    .getPixel(originalImageData.palette.getRGB(pixels[col]));
            newImageData.setPixels(radius,row,originalImageData.width,
                pixels,0);
        }
        // initialize glow pixel data
        int colorInt = newImageData.palette.getPixel(color.getRGB());
        pixels = new int[newImageData.width];
        for (int i = 0;i<newImageData.width;i++) {
            pixels[i] = colorInt;
        }
        // initialize alpha values
        byte[] alphas = new byte[newImageData.width];
        // deal with alpha values on rows above and below the photo
        for (int row = 0;row<newImageData.height;row++) {
            if (row<radius) {
                // only calculate alpha values for top border. they will reflect
                // to the bottom border
                byte intensity = (byte)(opacity*((((row+1))/(double)(radius))));
                for (int col = 0;col<alphas.length/2+alphas.length%2;col++) {
                    if (col<radius) {
                        // deal with corners:
                        // calculate pixel's distance from image corner
                        double hypotenuse = Math.sqrt(Math.pow(
                            radius-col-1,2.0)
                            +Math.pow(radius-1-row,2.0));
                        // calculate alpha based on percent distance from image
                        alphas[col+shift] =
                            alphas[alphas.length-col-1] = (byte)(opacity*Math
                                .max(((radius-hypotenuse)/radius),0));
                        // add highlight radius
                        if (hypotenuse<Math.min(highlightRadius,radius*.5)) {
                            alphas[col+shift] = alphas[alphas.length-col
                                -1] = (byte)Math
                                .min(255,
                                    (alphas[col+shift]&0x0FF)
                                        *(1+highlightRadiusIncrease
                                            *Math.max(
                                                ((radius-hypotenuse)/radius),
                                                0)));
                        }
                    }
                    else {
                        alphas[col+shift] =
                            alphas[alphas.length-col-1] =
                                (byte)((row>Math
                                    .max(radius-highlightRadius-1,radius*.5))
                                    ? Math
                                        .min(255,(intensity&0x0FF)
                                            *(1+highlightRadiusIncrease*row
                                                /radius)) : intensity);
                    }
                }
                if (row+shift<newImageData.height) {
                    newImageData.setAlphas(newImageData.width-radius,row
                        +shift,radius,alphas,alphas.length-radius);
                    newImageData.setPixels(newImageData.width-radius,row
                        +shift,radius,pixels,alphas.length-radius);
                }
                newImageData.setAlphas(0,newImageData.height-1-row,
                    newImageData.width,alphas,0);
                newImageData.setPixels(0,newImageData.height-1-row,
                    newImageData.width,pixels,0);
            }
            // deal with rows the image resides on
            else if (row<=newImageData.height/2) {
                // calculate alpha values
                double intensity = 0;
                for (int col = 0;col<alphas.length;col++) {
                    if (col<radius) {
                        intensity = (opacity*((col+1)/(double)radius));
                        if (col>Math.max(radius-highlightRadius-1,
                            radius*.5)) {
                            intensity = Math.min(255,(intensity)
                                *(1+highlightRadiusIncrease*col
                                    /radius));
                        }
                        alphas[newImageData.width-col-1] =
                            (byte)(int)(intensity);
                        alphas[col] = 0;
                    }
                    else if (col<=newImageData.width/2
                        +newImageData.width%2) {
                        // original image pixels are full opacity
                        alphas[col] =
                            alphas[newImageData.width-col-1] = (byte)(255);
                    }
                }
                newImageData.setPixels(0,newImageData.height-1-row,
                    radius,pixels,0);
                newImageData.setPixels(originalImageData.width+radius,
                    newImageData.height-1-row,radius,pixels,0);
                newImageData.setAlphas(0,newImageData.height-1-row,
                    newImageData.width,alphas,0);
                if (row>=shift+radius) {
                    newImageData.setPixels(0,row,radius,pixels,0);
                    newImageData.setPixels(originalImageData.width+radius,
                        row,radius,pixels,0);
                    newImageData.setAlphas(0,row,newImageData.width,alphas,
                        0);
                }
                else {
                    newImageData.setPixels(0,row,radius,pixels,0);
                    newImageData.setAlphas(0,row,newImageData.width-radius,
                        alphas,0);
                }
            }
        }
        return newImageData;
    }

    /**
     * Return a image based on the display, bound by the supplied rectangle
     * 
     * @param display to process
     * @param bounds of the image
     * @return the image
     * @since 2.2
     */
    public static Image getDesktopAreaImage(Display display,Rectangle bounds) {
        GC gc = new GC(display);
        Image image = new Image(display,new Rectangle(0,0,bounds.width,
            bounds.height));
        gc.copyArea(image,bounds.x,bounds.y);
        gc.dispose();
        return image;
    }

    /**
     * Return an image which is the whole of the desktop
     * 
     * @param display to process
     * @return the desktop image
     * @since 2.2
     */
    public static Image getDesktopImage(Display display) {
        return getDesktopAreaImage(display,display.getBounds());
    }

    /**
     * Return the desktop image, given a shell
     * 
     * @param parentShell
     * @return the desktop image
     * @since 2.2
     */
    public static Image getDesktopAreaImage(Shell parentShell) {
        Display display = parentShell.getDisplay();
        return getDesktopImage(display);
    }

    /**
     * Return the image of the shell
     * 
     * @param shell to process
     * @return the image of the shell
     * @since 2.2
     */
    public static Image getShellImage(Shell shell) {
        Display display = shell.getDisplay();
        return getDesktopAreaImage(display,shell.getBounds());
    }

    /**
     * Return a image of the control
     * 
     * @param control to process
     * @return the image of the control
     * @since 2.2
     */
    public static Image getControlImage(Control control) {
        Display display = control.getDisplay();
        return getDesktopAreaImage(display,
            display.map(control,null,control.getBounds()));
    }

    /**
     * Resizes an image to a given width/height.
     * 
     * @param original The image to resize
     * @param width The required width of the image.
     * @param height The required height of the image
     * @return The resized image.
     * @since 2.2
     */
    public static Image resizeImage(Image original,int width,int height) {
        Point origSize = new Point(original.getBounds().width,
            original.getBounds().height);
        Image scaled = new Image(Display.getDefault(),width,height);
        GC gc = new GC(scaled);
        gc.setAntialias(SWT.ON);
        gc.setInterpolation(SWT.HIGH);
        gc.drawImage(original,0,0,origSize.x,origSize.y,0,0,width,
            height);
        original.dispose();
        return scaled;
    }

    /**
     * Returns an {@link Image} composed of a base image decorated by another
     * image.
     * 
     * @param baseImage the base {@link Image} that should be decorated.
     * @param decorator the {@link Image} to decorate the base image.
     * @param corner the corner to place decorator image.
     * @return the resulting decorated {@link Image}.
     * @since 2.4
     */
    public static Image decorateImage(final Image baseImage,
    final Image decorator,final int corner) {

        final Rectangle bib = baseImage.getBounds();
        final Rectangle dib = decorator.getBounds();
        final Point baseImageSize = new Point(bib.width,bib.height);
        CompositeImageDescriptor compositImageDesc =
            new CompositeImageDescriptor() {
                @Override
                protected void drawCompositeImage(int width,int height) {
                    drawImage(baseImage.getImageData(),0,0);
                    if (corner==TOP_LEFT) {
                        drawImage(decorator.getImageData(),0,0);
                    }
                    else if (corner==TOP_RIGHT) {
                        drawImage(decorator.getImageData(),bib.width-
                            dib.width,0);
                    }
                    else if (corner==BOTTOM_LEFT) {
                        drawImage(decorator.getImageData(),0,bib.height-
                            dib.height);
                    }
                    else if (corner==BOTTOM_RIGHT) {
                        drawImage(decorator.getImageData(),bib.width-
                            dib.width,bib.height-dib.height);
                    }
                }

                /**
                 * @see org.eclipse.jface.resource.CompositeImageDescriptor#getSize()
                 */
                @Override
                protected Point getSize() {
                    return baseImageSize;
                }
            };
        //
        return compositImageDesc.createImage();
    }

    /**
     * Rotates an image by any given angle and draws it on the gc. Rotation
     * happens clockwise. {@docfacto.note <ul>
     * <li>angle 0, draws the image as it is.</li>
     * <li>angle 90, rotates the image 90 degree clockwise</li></ul> }
     * 
     * @param image the image to rotate
     * @param angle the angle to rotate
     * @return A PaintListener that will rotate the given image.
     * @since 2.4.7
     */
    public static PaintListener rotateAndDrawImage(final Image image,final int angle) {
        return new PaintListener() {
            @Override
            public void paintControl(PaintEvent e) {
                GC gc = e.gc;
                gc.fillRectangle(0,0,e.width,e.height);
                // gc.setForeground(SWTUtils.getColor(250,0,0));
                gc.setAdvanced(true);
                int x = image.getImageData().x;
                int y = image.getImageData().y;
                int width = image.getImageData().width;
                int height = image.getImageData().height;
                Transform transform = new Transform(e.display);
                transform.translate(x+width/2,y+height/2);
                transform.rotate(angle);
                transform.translate(-x-width/2,-y-height/2);

                gc.setTransform(transform);
                gc.drawImage(image,0,0);
            }
        };
    }
}
