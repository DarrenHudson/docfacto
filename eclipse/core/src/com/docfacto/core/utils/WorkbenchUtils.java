package com.docfacto.core.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.part.FileEditorInput;

/**
 * This is a utility class for methods which deal with the eclipse workbench
 * @author damonli
 * @since 2.4.8
 */
public class WorkbenchUtils {
	
	/**
	 * Get a list of the editors in the workbench
	 * @param workbench to get editors from
	 * @return the list of editors in the given workbench
	 * @since 2.4.8
	 */
	public static List<IEditorPart> getEditorsInWorkbench(IWorkbench workbench) {
		
		List<IEditorPart> editorParts = new ArrayList<IEditorPart>();
	
		IWorkbenchWindow[] workbenchWindows = workbench.getWorkbenchWindows();
		
		for(int i=0; i<workbenchWindows.length; i++) {
			IWorkbenchWindow workbenchWindow = workbenchWindows[i];
			
			if (workbenchWindow == null) {
				// Do Nothing
			}
			else {
				IWorkbenchPage[] pages = workbenchWindow.getPages();
				
				for (int j=0; j < pages.length; j++) {
					IEditorReference[] editorReferences = pages[j].getEditorReferences();
					
					for (int k=0; k < editorReferences.length; k++) {
						IEditorReference editorReference = editorReferences[k];
						IEditorPart editor = editorReference.getEditor(true);
						editorParts.add(editor);
					}
				}
			}
		}
		
		return editorParts;		
	}

	/**
	 * Refresh the editors in the workbench.
	 * 
	 * @param workbench the workbench with the editors to be refreshed
	 * @since 2.4.8
	 */
	public static void refreshWorkBenchEditors(IWorkbench workbench) {
		List<IEditorPart> editors = getEditorsInWorkbench(workbench);
			
		for (IEditorPart editor : editors) {
			IEditorInput editorInput = editor.getEditorInput();
			if (editorInput instanceof FileEditorInput) {
				IFile file = ((FileEditorInput)editorInput).getFile();
				try {
					file.refreshLocal(IResource.DEPTH_ZERO, null);
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
