package com.docfacto.core.utils;

import java.awt.event.InputEvent;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;

/**
 * A collection of static methods to help with SWT
 * 
 * @author kporter - November 28th 2012
 * @since 2.1
 */
public class SWTUtils {

    /**
     * Static methods only.
     */
    private SWTUtils() {
    }

    /**
     * Returns a {@link Font} based on its name, height and style.
     * 
     * @param name the name of the font
     * @param height the height of the font
     * @param style the style of the font
     * @return {@link Font} The font matching the name, height and style
     */
    public static Font getFont(String name,int height,int style) {
        return getFont(name,height,style,false,false);
    }

    /**
     * Returns a {@link Font} based on its name, height and style.
     * Windows-specific strikeout and underline flags are also supported.
     * 
     * @param name the name of the font
     * @param size the size of the font
     * @param style the style of the font
     * @param strikeout the strikeout flag (warning: Windows only)
     * @param underline the underline flag (warning: Windows only)
     * @return {@link Font} The font matching the name, height, style, strikeout
     * and underline
     */
    public static Font getFont(String name,int size,int style,
    boolean strikeout,boolean underline) {

        FontData fontData = new FontData(name,size,style);
        if (strikeout||underline) {
            try {
                Class<?> logFontClass =
                    Class.forName("org.eclipse.swt.internal.win32.LOGFONT");
                Object logFont =
                    FontData.class.getField("data").get(fontData);
                if (logFont!=null&&logFontClass!=null) {
                    if (strikeout) {
                        logFontClass
                            .getField("lfStrikeOut").set(logFont,
                                Byte.valueOf((byte)1));
                    }
                    if (underline) {
                        logFontClass
                            .getField("lfUnderline").set(logFont,
                                Byte.valueOf((byte)1));
                    }
                }
            }
            catch (Throwable e) {
                System.err
                    .println("Unable to set underline or strikeout"+
                        " (probably on a non-Windows platform). "+e);
            }
        }
        return new Font(Display.getCurrent(),fontData);
    }

    /**
     * Returns a bold version of the given {@link Font}.
     * 
     * @param baseFont the {@link Font} for which a bold version is desired
     * @return the bold version of the given {@link Font}
     */
    public static Font getBoldFont(Font baseFont) {

        FontData fontDatas[] = baseFont.getFontData();
        FontData data = fontDatas[0];
        return new Font(Display.getCurrent(),data.getName(),data.getHeight(),
            SWT.BOLD);
    }

    /**
     * Convert a SWT color to a AWT color
     * 
     * @param c color to convert
     * @return a AWT color
     * @since 2.2
     */
    public static java.awt.Color toAWTColor(Color c) {
        return new java.awt.Color(c.getRed(),c.getGreen(),c.getBlue());
    }

    /**
     * Return a AWT color from a system color
     * 
     * @param systemColor
     * @return
     * @since 2.2
     */
    public static java.awt.Color toAWTColor(int systemColor) {
        return toAWTColor(getColor(systemColor));
    }

    /**
     * Return a system color, use constants from SWT constants
     * 
     * {@docfacto.note for example getColor(SWT.COLOR_BLACK) }
     * 
     * @param systemColor
     * @return a system color
     * @since 2.2
     */
    public static Color getColor(int systemColor) {
        Display display = Display.getCurrent();
        return display.getSystemColor(systemColor);
    }

    /**
     * Return a color bases on red green blue values
     * 
     * @param r red
     * @param g green
     * @param b blue
     * @return a color
     * @since 2.1.1
     */
    public static Color getColor(int r,int g,int b) {
        return getColor(new RGB(r,g,b));
    }

    /**
     * Return a colour based on a RGB value
     * 
     * @param rgb
     * @return a color
     * @since 2.2
     */
    public static Color getColor(RGB rgb) {
        return new Color(Display.getCurrent(),rgb);
    }

    /**
     * Given an RGB converts it to a hex string
     * 
     * @param rgb The rgb to convert
     * @return A string hex colour
     * @since 2.4.4
     */
    public static String convertRGBToHex(RGB rgb,boolean showHash) {
        String hash = showHash ? "#" : "";

        return String.format(hash+"%02x%02x%02x",rgb.red,
            rgb.green,
            rgb.blue);
    }

    /**
     * Given a hex string, returns a Color
     * 
     * @param hex The string hex colour
     * @return An SWT Color based on the hex
     * @since 2.4.4
     */
    public static Color convertHexToColor(String hex) {
        try {
            java.awt.Color c = java.awt.Color.decode(hex);
            return getColor(c.getRed(),c.getGreen(),c.getBlue());
        }
        catch (NumberFormatException e) {
            return null;
        }
    }

    /**
     * Generate {@code FormData} from the provided format attachments
     * 
     * @param top
     * @param right
     * @param bottom
     * @param left
     * @return new FormData from the provided form attachments
     * @since 2.4
     */
    public static FormData getFormData(FormAttachment top,FormAttachment right,
    FormAttachment bottom,FormAttachment left) {
        FormData data = new FormData();
        data.top = top;
        data.right = right;
        data.bottom = bottom;
        data.left = left;
        return data;
    }

    /**
     * Run the runnable on the UI thread.
     * 
     * Determine if the current thread is the UI thread and if so run the
     * runnable synchronously, else add it to the runnable UI queue, which means
     * that it will be run async.
     * 
     * @param runnable to execute
     * @since 2.4
     */
    public static void UIThreadExec(Runnable runnable) {
        if (Thread.currentThread()==Display.getDefault().getThread()) {
            // I am in the UI thread
            runnable.run();
        }
        else {
            // OK, run in the background
            Display.getDefault().asyncExec(runnable);
        }
        
    }
    
    /**
     * Run the runnable on the UI thread and wait to return
     * 
     * Determine if the current thread is the UI thread and if so run the
     * runnable synchronously, else add it to the runnable UI queue, running synchronously, therefore waiting
     * 
     * @param runnable to execute
     * @since 2.5.1
     */
    public static void UIThreadExecAndWait(Runnable runnable){
        if (Thread.currentThread()==Display.getDefault().getThread()) {
            // I am in the UI thread
            runnable.run();
        }
        else {
            // OK, run in the background
            Display.getDefault().syncExec(runnable);
        }
    }

    /**
     * For given SWT key and mouse modifiers, translates to AWT
     * 
     * @param swtModifiers SWT modifiers to convert
     * @return the AWT modifier
     * @since 2.4
     */
    public static int translateSWTModifiers(int swtModifiers) {
        int awtModifiers = 0;
        if ((swtModifiers&SWT.CTRL)>0) {
            awtModifiers = awtModifiers|InputEvent.CTRL_MASK;
        }
        if ((swtModifiers&SWT.SHIFT)>0) {
            awtModifiers = awtModifiers|InputEvent.SHIFT_MASK;
        }
        if ((swtModifiers&SWT.ALT)>0) {
            awtModifiers = awtModifiers|InputEvent.ALT_MASK;
        }
        if ((swtModifiers&SWT.BUTTON1)>0) {
            awtModifiers = awtModifiers|InputEvent.BUTTON1_DOWN_MASK;
        }
        if ((swtModifiers&SWT.BUTTON2)>0) {
            awtModifiers = awtModifiers|InputEvent.BUTTON2_DOWN_MASK;
        }
        if ((swtModifiers&SWT.BUTTON3)>0) {
            awtModifiers = awtModifiers|InputEvent.BUTTON3_DOWN_MASK;
        }
        if ((swtModifiers&SWT.COMMAND)>0) {
            awtModifiers = awtModifiers|InputEvent.META_MASK;
        }
        return awtModifiers;
    }

    /**
     * For given AWT key and mouse modifiers, translates to SWT
     * 
     * @param awtModifiers the AWT modifiers
     * @return the SWT modifier
     * @since 2.4
     */
    public static int translateAWTModifiers(int awtModifiers) {
        int swtModifiers = 0;
        if ((awtModifiers&InputEvent.CTRL_MASK)>0) {
            swtModifiers = swtModifiers|SWT.CTRL;
        }
        if ((awtModifiers&InputEvent.SHIFT_MASK)>0) {
            swtModifiers = swtModifiers|SWT.SHIFT;
        }
        if ((awtModifiers&InputEvent.ALT_MASK)>0) {
            swtModifiers = swtModifiers|SWT.ALT;
        }
        if ((awtModifiers&InputEvent.BUTTON1_DOWN_MASK)>0) {
            swtModifiers = swtModifiers|SWT.BUTTON1;
        }
        if ((awtModifiers&InputEvent.BUTTON2_DOWN_MASK)>0) {
            swtModifiers = swtModifiers|SWT.BUTTON2;
        }
        if ((awtModifiers&InputEvent.BUTTON3_DOWN_MASK)>0) {
            swtModifiers = swtModifiers|SWT.BUTTON3;
        }
        if ((awtModifiers&InputEvent.META_MASK)>0) {
            swtModifiers = swtModifiers|SWT.COMMAND;
        }
        return swtModifiers;
    }
    
    /**
     * Returns an RGB that lies between the given foreground and background
     * colors using the given mixing factor. A <code>factor</code> of 1.0 will produce a
     * color equal to <code>fg</code>, while a <code>factor</code> of 0.0 will produce one
     * equal to <code>bg</code>.
     * @param bg the background color
     * @param fg the foreground color
     * @param factor the mixing factor, must be in [0,&nbsp;1]
     *
     * @return the interpolated color
     * @since 2.4
     */
    public static RGB blend(RGB bg, RGB fg, float factor) {
        float complement= 1f - factor;
        return new RGB(
                (int) (complement * bg.red + factor * fg.red),
                (int) (complement * bg.green + factor * fg.green),
                (int) (complement * bg.blue + factor * fg.blue)
        );
    }
    
    /**
     * Get a relative point based on the parent from a given mouse location and 
     * @param control The control thats producing the mouse location
     * @param mouseLocation The mouse location to relativize 
     * @return The point on a parents control given a child
     * @since 2.4.6
     */
    public static Point getRelativeLocation(Control control, Point mouseLocation){
        Composite parent = control.getParent();
        Point pointOnDisplay = control.toDisplay(mouseLocation);
        Point pointOnWidget =  parent.toControl(pointOnDisplay);
        
        return pointOnWidget;
    }
    
    /**
     * Adds a listener to the given control and all its children if any.
     * @param control The control to add listener
     * @param listener The KeyListner
     * @since 2.4.8
     */
    public static void addKeyListenerToChildren(Control control, KeyListener listener){
        control.addKeyListener(listener);
        if(control instanceof Composite){
            for(final Control c: ((Composite)control).getChildren()){
                addKeyListenerToChildren(c,listener);
            }
        }
    }
}
