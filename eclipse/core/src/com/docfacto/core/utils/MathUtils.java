package com.docfacto.core.utils;

import java.awt.geom.Point2D;

/**
 * Mathematical Utilities
 * 
 * @author kporter - created Oct 1, 2013
 * @since 2.4.5
 */
public class MathUtils {

    private MathUtils(){}
   
    public static Point2D.Float retainAspectRatio(float x, float y, float originalX, float originalY){
        float retainX = x;
        float retainY = y;
        
        if (originalX>originalY) {
            float ratio = originalX/originalY;
            if (Math.abs(retainX)>Math.abs(retainY)) {
                retainY = x/ratio;
            }
            else {
                retainX = y*ratio;
            }
        }
        else {
            float ratio = originalY/originalX;
            if (Math.abs(retainY)>Math.abs(retainX)) {
                retainX = y/ratio;
            }
            else {
                retainY = x*ratio;
            }
        }
        
        return new Point2D.Float(retainX,retainY);
    }
    
}
