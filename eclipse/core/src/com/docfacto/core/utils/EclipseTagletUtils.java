package com.docfacto.core.utils;

import java.util.List;

import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jface.text.IDocument;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.TagletUtils;
import com.docfacto.javadoc.TagParser;

/**
 * A collection a static methods that help with the processing of taglets
 * 
 * @author dhudson - created 26 Feb 2013
 * @since 2.2
 */
public class EclipseTagletUtils {

    /**
     * Constructor.
     */
    private EclipseTagletUtils() {
        // Stop constructing
    }

    /**
     * Process the current TagElement with the given document and retrieve the
     * text
     * 
     * @param tag to process
     * @param doc containing the tag
     * @return the Tag text string
     * @throws DocfactoException if unable to process the TagElement
     * @since 2.2
     */
    public static String getTagElementText(TagElement tag,IDocument doc)
    throws DocfactoException {
        int start = tag.getStartPosition();

        try {
            String text = TagletUtils.removeDocLineIntros(doc.get(start,tag.getLength()));

            // Need to strip the {@tagname from the text..

            boolean isInLine = text.startsWith("{");

            if (isInLine) {
                return text.substring(tag.getTagName().length()+2,
                    text.length()-1);
            }
            else {
                return text.substring(tag.getTagName().length()+1);
            }
        }
        catch (Exception ex) {
            throw new DocfactoException("Unable to process tag element "+ex);
        }

    }

    /**
     * Iterate the fragments and create a {@code TagParser}
     * 
     * @param element to process
     * @return A {@code TagParser} with the Tag text
     * @since 2.2
     */
    public static TagParser createTagParser(TagElement element) {
        final List frags = element.fragments();
        StringBuilder builder = new StringBuilder(20);
        for (Object frag:frags) {
            builder.append(frag.toString());
        }

        return new TagParser(builder.toString());
    }

    /**
     * Perform the check to see if there are any inline tags within this tag
     * 
     * @param element to check
     * @return true if the tag has nested inline tags
     * @since 2.2
     */
    public static boolean hasNestedInlineTags(TagElement element) {
        final List frags = element.fragments();
        for(Object frag : frags) {
            if(frag instanceof TagElement) {
                return true;
            }
        }
        
        return false;
    }

}
