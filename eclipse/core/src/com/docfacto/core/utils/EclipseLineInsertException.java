package com.docfacto.core.utils;

public class EclipseLineInsertException extends Exception {

	public EclipseLineInsertException(String message) {
		super(message);
	}
}
