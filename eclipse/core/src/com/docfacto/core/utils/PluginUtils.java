package com.docfacto.core.utils;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.activation.MimetypesFileTypeMap;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaModelMarker;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.internal.corext.util.JavaModelUtil;
import org.eclipse.jface.text.IRegion;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.internal.Workbench;
import org.osgi.framework.Bundle;
import org.osgi.framework.Version;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.core.DocfactoCorePlugin;

/**
 * General helper utils
 * 
 * @author dhudson - created 20 Feb 2013
 * @since 2.2
 */
@SuppressWarnings("restriction")
public class PluginUtils {

    /**
     * Collection of static methods, not to be instantiated
     */
    private PluginUtils() {
    }

    /**
     * Load a resource that is included with the plugin
     * 
     * @param pluginId of the plugin
     * @param path from the root of the plugin, so /taglets.css for example
     * @return the contents of the resource
     * @throws DocfactoException unable to find or load the resource
     * @since 2.2
     */
    public static String loadResource(String pluginId,String path)
    throws DocfactoException {

        Bundle bundle = Platform.getBundle(pluginId);
        URL resource = bundle.getEntry(path);

        if (resource==null) {
            throw new DocfactoException("Unload to find resource ["+
                pluginId+"] "+path);
        }

        return IOUtils.loadURL(resource);
    }

    /**
     * Load the resource
     * 
     * @param resource to process
     * @return contents of the resource
     * @throws DocfactoException if unable to load the resource
     * @since 2.2
     */
    public static String loadResource(IResource resource)
    throws DocfactoException {

        URL url;
        try {
            url = resource.getLocationURI().toURL();
            return IOUtils.loadURL(url);
        }
        catch (MalformedURLException ex) {
            throw new DocfactoException("Unable to convert resource to URL",ex);
        }
    }

    /**
     * Return the absolute file path of a bundle entry.
     * 
     * {@docfacto.note title="SWT Feature" If you are going to use this, then in
     * the feature set, the option to unpack the archive will be required }
     * 
     * @param pluginId of the plugin
     * @param path bundle path
     * @return A string to the disk path
     * @throws IOException if an error occurs during the conversion
     * @since 2.4
     */
    public static String getFileLocationOf(String pluginId,String path)
    throws IOException {
        return FileLocator.toFileURL(getResourceURL(pluginId,path)).getPath();
    }

    /**
     * Given a path relative to the plugin and the plugin id, will return the
     * full path.
     * <p>
     * This returns the full path to a resource.
     * </p>
     * 
     * @param symbolicName The symbolic name/ID of the plugin
     * @param pathToResource The path t
     * @return The full path to the resource or nothing if the plugin does not
     * exist
     * @since 2.1
     */
    public static URL getResourceURL(String symbolicName,String pathToResource) {
        Bundle bundle = Platform.getBundle(symbolicName);
        if (bundle!=null) {
            return bundle.getEntry(pathToResource);
        }

        return null;
    }

    /**
     * Creates a qualifier string based on the current time.
     * <p>
     * The qualifier is created to the nearest minute in the format of: Year
     * Month Day-Hour Minute. The Hour is in 24hour format. Example output:
     * 20130329-1353 Note a hyphen separates the timestamp from the date.
     * </p>
     * {@docfacto.system <p>
     * Months returned by the Calendar start at 0, but date does not. Therefore
     * one is added to month as it needs to be adjusted to fit 1-12. but date is
     * not adjusted. Hour/minute, can be 0 therefore not relevant.} </p>
     * <p>
     * As Calendar returns int's, the 0 for example in 01, is not represented.
     * This needs to be added in and is done with a series of ternary if
     * statements. If month/day/hour/minute less than 10, add "0" before it.
     * </p>
     * {@docfacto.note The Calendar instance used is the GregorianCalendar}
     * {@docfacto.note <p>
     * Why need a qualifier?
     * </p>
     * <p>
     * Documents that we generate, SVG/DITA for example may use versioning
     * 
     * {@literal <vrm>} in DITA or {@literal svg version="1.2.3.qualifier"} in
     * SVG} }
     * 
     * @return A string with the qualifier, a 'date-time stamp' to the nearest
     * minute.
     * @since 2.2
     */
    public static String getQualifier() {
        Calendar now = GregorianCalendar.getInstance();

        int monthNum = now.get(Calendar.MONTH)+1;
        int dayNum = now.get(Calendar.DATE);
        int hourNum = now.get(Calendar.HOUR_OF_DAY);
        int minuteNum = now.get(Calendar.MINUTE);

        String year = now.get(Calendar.YEAR)+"";
        String month = monthNum<10 ? "0"+monthNum : ""+monthNum;
        String day = dayNum<10 ? "0"+dayNum : ""+dayNum;
        String hour = hourNum<10 ? "0"+hourNum : ""+hourNum;
        String minute = minuteNum<10 ? "0"+minuteNum : ""+minuteNum;

        return (year+month+day+"-"+hour+minute);
    }

    /**
     * Return a list of open Java Projects
     * 
     * @return A list of open java projects
     * @since 2.2
     */
    public static List<IProject> getOpenJavaProjects() {
        ArrayList<IProject> results = new ArrayList<IProject>();

        IProject[] projects = ResourcesPlugin.getWorkspace().getRoot()
            .getProjects();

        for (IProject project:projects) {
            try {
                if (project.isOpen()&&project.hasNature(JavaCore.NATURE_ID)) {
                    results.add(project);
                }
            }
            catch (CoreException ex) {
                DocfactoCorePlugin.logException(
                    "PluginUtils: getOpenJavaProjects  "+
                        ex.getLocalizedMessage(),
                    ex);
            }
        }

        return results;
    }

    /**
     * Check to see if problem or task markers exist at a region
     * 
     * @param resource to check
     * @param region position
     * @return true if there are problem markers at the given position
     * @since 2.2
     */
    public static boolean hasProblemMarkers(IResource resource,IRegion region) {
        if (hasMarkersForRegion(IJavaModelMarker.JAVA_MODEL_PROBLEM_MARKER,
            resource,region)) {
            return true;
        }

        return hasMarkersForRegion(IJavaModelMarker.TASK_MARKER,resource,region);
    }

    /**
     * Check to see if there are any markers at the region for the resource
     * 
     * @param type of marker, or null for all
     * @param resource to search
     * @param region to look for the markers
     * @return true if the marks of that type are found in the region
     * @since 2.2
     */
    public static boolean hasMarkersForRegion(String type,IResource resource,
    IRegion region) {
        try {
            IMarker[] markers =
                resource.findMarkers(type,true,IResource.DEPTH_ZERO);
            for (IMarker marker:markers) {
                int position = (Integer)marker.getAttribute(IMarker.CHAR_START);

                if (region.getOffset()<=position
                    &&position<=region.getOffset()+region.getLength()) {
                    return true;
                }
            }
        }
        catch (CoreException ex) {
            DocfactoCorePlugin.logException("Unable to getMarkers for region",
                ex);
        }

        return false;
    }

    /**
     * Return a {@code File} from an {@code IFile}
     * 
     * @param file to convert
     * @return a java {@code File}
     * @since 2.4
     */
    public static File getFileFromIFile(IFile file) {
        return file.getRawLocation().makeAbsolute().toFile();
    }

    /**
     * Return the Eclipse version
     * 
     * @return the running eclipse version
     * @since 2.4
     */
    public static Version getEclipseVersion() {
        return Platform.getBundle("org.eclipse.platform").getVersion();
    }

    /**
     * Checks whether the file is an image through 3 steps.
     * 
     * @param file The file to check
     * @return true if file is an image
     * @since 2.4.7
     */
    public static boolean isFileAnImage(File file) {
        String filename = file.getName();
        String extension = filename.substring(filename.lastIndexOf('.'));
        // Skip the rest if we're dealing with an svg.
        if (extension.equals(".svg")) {
            return false;
        }

        // First attempt to get a file type
        String mimetypes = new MimetypesFileTypeMap().getContentType(file);
        String type = mimetypes.split("/")[0];

        if (!type.equals("image")) {
            // This doesnt seem to work for .png, so second attempt
            mimetypes =
                URLConnection.guessContentTypeFromName(file.getAbsolutePath());
            if (mimetypes==null||!mimetypes.startsWith("image")) {
                if (extension==null||!extension.equalsIgnoreCase(".jpeg")||
                    !extension.equalsIgnoreCase(".jpg")
                    ||!extension.equalsIgnoreCase(".png")) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Returns a Java Element, which could be class or package.
     * 
     * {@docfacto.note If its a Java Class then .java is not required }
     * 
     * @param resourceName to find
     * @return An IJaveElement if possible
     * @since 2.5
     */
    public static IJavaElement locateJavaElement(String resourceName) {

        // Remove the .java extension if there is one
        if (resourceName.endsWith(".java")) {
            resourceName = resourceName.substring(0,resourceName.length()-5);
        }

        List<IProject> projects = PluginUtils.getOpenJavaProjects();

        try {
            for (IProject project:projects) {

                IJavaProject javaProject = JavaCore.create(project);
                IType type = javaProject.findType(resourceName);

                if (type==null||type.isBinary()) {
                    // Its not in this project, or its in a jar file
                    continue;
                }

                return JavaModelUtil.findTypeContainer(javaProject,resourceName);
            }
        }
        catch (JavaModelException ex) {
            DocfactoCorePlugin.logException("Unable to locate resource "+resourceName,ex);
        }

        return null;
    }

    /**
     * Checkout an IFile in its workspace.
     * <p>
     * Call this before editing a file, and again before saving to a file.
     * </p>
     * <p>
     * Proceed only if return is true, this means its safe to edit or save as
     * the file has been checked out.
     * </p>
     * 
     * @param file The file to checkout.
     * @return true if the file has been checked out and its safe to edit.
     * @since 2.4.8
     */
    public static boolean checkoutFile(IFile file) {
        return checkoutFile(file,null);
    }

    /**
     * Checkout an IFile in its workspace.
     * <p>
     * Call this before editing a file, and again before saving to a file.
     * </p>
     * <p>
     * Proceed only if return is true, this means its safe to edit or save as
     * the file has been checked out.
     * </p>
     * <p>
     * By passing a UI context, the caller indicates that the VCM component may
     * contact the user to help decide how best to proceed. If no UI context is
     * provided, the VCM component will make its decision without additional
     * interaction with the user. If OK is returned, the caller can safely
     * assume that all of the given files haven been prepared for modification
     * and that there is good reason to believe that
     * <code>IFile.setContents</code> (or <code>appendContents</code>) would be
     * successful on any of them. If the result is not OK, modifying the given
     * files might not succeed for the reason(s) indicated.
     * </p>
     * <p>
     * If a shell is passed in as the context, the VCM component may bring up a
     * dialogs to query the user or report difficulties; the shell should be
     * used to parent any such dialogs; the caller may safely assume that the
     * reasons for failure will have been made clear to the user. If
     * {@link IWorkspace#VALIDATE_PROMPT} is passed as the context, this
     * indicates that the caller does not have access to a UI context but would
     * still like the user to be prompted if required. If <code>null</code> is
     * passed, the user should not be contacted; any failures should be reported
     * via the result; the caller may chose to present these to the user however
     * they see fit. The ideal implementation of this method is transactional;
     * no files would be affected unless the go-ahead could be given. (In
     * practice, there may be no feasible way to ensure such changes get done
     * atomically.)
     * </p>
     * 
     * @param file The file to checkout.
     * @param context either {@link IWorkspace#VALIDATE_PROMPT}, or the
     * <code>org.eclipse.swt.widgets.Shell</code> that is to be used to parent
     * any dialogs with the user, or <code>null</code> if there is no UI context
     * (declared as an <code>Object</code> to avoid any direct references on the
     * SWT component)
     * @return true if the file has been checked out and its safe to edit.
     * @since 2.4.8
     */
    public static boolean checkoutFile(IFile file,Object context) {
        IWorkspace workspace = file.getWorkspace();
        IStatus status = workspace.validateEdit(new IFile[] {
            file
        },context);

        return status.isOK();
    }

    /**
     * Get an IFile in the workspace.
     * 
     * @param fullPath The full path to the file.
     * @return The IFile at this path or null if it doesn't exist.
     * @since 2.5.0
     */
    public static IFile findIFileInWorkspace(String fullPath) {
        IPath targetPath = new Path(fullPath);
        IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
        IFile targetFile = workspaceRoot.getFileForLocation(targetPath);
        return targetFile;
    }

    /**
     * This method returns the workspace the plugin is running in.
     * 
     * @return the workspace
     * @since 2.5
     */
    public static IWorkspace getWorkspace() {
        return ResourcesPlugin.getWorkspace();
    }

    /**
     * Set a status line message in eclipse workbench
     * 
     * @param message The status to display
     * @since 2.5
     */
    public static void displayStatusLineMessage(String message) {
        Workbench workbench = Workbench.getInstance();
        IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();

        Assert.isNotNull(window);

        IEditorPart part = window.getActivePage().getActiveEditor();

        Assert.isNotNull(part);

        displayStatusLineMessage(part,message);
    }

    /**
     * Set a status line message in eclipse workbench
     * 
     * {@docfacto.system Use this if uninitialized editor}
     * 
     * @param message The status to display
     * @since 2.5
     */
    public static void displayStatusLineMessage(IEditorPart part,String message) {
        part.getEditorSite().getActionBars().getStatusLineManager().setMessage(message);
    }
}
