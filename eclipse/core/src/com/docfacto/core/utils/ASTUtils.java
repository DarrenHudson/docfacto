package com.docfacto.core.utils;

import java.io.File;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;

/**
 * A collection of AST utils.
 * 
 * @author dhudson - created 5 Mar 2013
 * @since 2.2
 */
public final class ASTUtils {

    private ASTUtils() {
    }

    /**
     * Create a {@code CompilationUnit} from an {@code ICompilationUnit}
     * 
     * @param cu
     * @return a compilation unit
     * @since 2.2
     */
    public static CompilationUnit getCompilationUnitFor(ICompilationUnit cu) {
        final ASTParser astParser = ASTParser.newParser(AST.JLS4);
        astParser.setKind(ASTParser.K_COMPILATION_UNIT);
        astParser.setSource(cu);

        return (CompilationUnit)astParser.createAST(null);
    }

    /**
     * Given the file, create a IFile from it and see if it is possible to
     * create a compilation unit for it.
     * 
     * @param file to locate the compilation unit
     * @return a compilation unit, or null
     * @since 2.2
     */
    public static CompilationUnit createCompilationUnitForFileInWorkspace(File file) {
        IFile ifile =
            ResourcesPlugin.getWorkspace().getRoot()
                .getFileForLocation(new Path(file.getPath()));

        return ASTUtils.getCompilationUnitFor(JavaCore
            .createCompilationUnitFrom(ifile));
    }
    
    /**
     * Return the compilation unit from the marker
     * 
     * @param marker get this marker's {@code ICompilationUnit}
     * @return the {@code ICompilationUnit} of marker
     * @since 2.2
     */
    public static ICompilationUnit getCompilationUnitFor(IMarker marker) {
        final IResource res = marker.getResource();
        if (res instanceof IFile&&res.isAccessible()) {
            final IJavaElement element = JavaCore.create((IFile)res);
            if (element instanceof ICompilationUnit)
                return (ICompilationUnit)element;
        }
        return null;
    }
    
    /**
     * Return the compilation unit for an external file
     * @param file to locate the compilation unit
     * @return a compilation unit, or null
     * @since 2.4
     */
    public static CompilationUnit getCompilationUnitForExternalFile(File file) {
    	char[] fileContents;
        try {
            fileContents = IOUtils.readFileAsString(file).toCharArray();
            return getCompilationUnitFromContents(fileContents);
        }
        catch (DocfactoException e) {
            return null;
        }
    }
    
    /**
     * Return the compilation unit from a character array of the file contents
     * @param contents of the file to get the compilation unit for
     * @return the compilation unit or null
     */
	private static CompilationUnit getCompilationUnitFromContents(char[] contents) {
	    ASTParser parser = ASTParser.newParser(AST.JLS4);
	    parser.setKind(ASTParser.K_COMPILATION_UNIT);
	    parser.setSource(contents);
	    parser.setResolveBindings(true);
	    CompilationUnit parse = (CompilationUnit) parser.createAST(null);
	 
	    return parse;
	}
}
