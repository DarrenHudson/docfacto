package com.docfacto.core.utils;

import org.eclipse.jdt.core.ISourceRange;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Comment;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.NodeFinder;
import org.eclipse.jface.text.IRegion;

/**
 * Complete node finder checks for comments, which {@code NodeFinder} doesn't.
 *
 * @author dhudson - created 9 Oct 2013
 * @since 2.4
 */
public class CompleteNodeFinder {

    /**
     * Maps a selection to a given ASTNode, where the selection is defined using a start and a length.
     * The result node is determined as follows:
     * <ul>
     *   <li>first the visitor tries to find a node with the exact <code>start</code> and <code>length</code></li>
     *   <li>if no such node exists then the node that encloses the range defined by
     *       <code>start</code> and <code>length</code> is returned.</li>
     *   <li>if the length is zero then also nodes are considered where the node's
     *       start or end position matches <code>start</code>.</li>
     *   <li>otherwise <code>null</code> is returned.</li>
     * </ul>
     *
     * @param root the root node from which the search starts
     * @param start the given start
     * @param length the given length
     *
     * @return the found node
     * @since 2.4
     */
    public static ASTNode perform(CompilationUnit root, int start, int length) {
        for(Object obj : root.getCommentList()) {
            Comment comment = (Comment) obj;
            if(selectionIsComment(comment, start, length)) {
            	return comment;
            }
        }
        
        // If we get this far then lets call the NodeFinder
        return NodeFinder.perform(root,start,length);
    }
    
    /**
     * Checks whether a selection is a comment
     * @param comment the comment to check
     * @param start the start of the selection
     * @param length the length of the selection
     * @return true if the selection is a comment and false otherwise
     */
    private static boolean selectionIsComment(Comment comment, int start, int length) {
    	int end = start + length;
        int commentEndPosition = comment.getStartPosition() + comment.getLength();
        return comment.getStartPosition() <= start && commentEndPosition >= end;
    }

    /**
     * Maps a selection to a given ASTNode, where the selection is defined using a source range.
     * It calls <code>perform(root, range.getOffset(), range.getLength())</code>.
     * 
     * @param root CompilationUnit
     * @param range the text range
     * @return the result node
     * @see #perform(ASTNode, int, int)
     * @since 2.4
     */
    public static ASTNode perform(CompilationUnit root, ISourceRange range) {
        return perform(root, range.getOffset(), range.getLength());
    }

    /**
     * Return an AST node at a given position.
     * 
     * @param root Compilation Unit
     * @param region to check
     * @return the node at the region
     * @since 2.5
     */
    public static ASTNode perform(CompilationUnit root, IRegion region) {
        return perform(root, region.getOffset(), region.getLength());
    }
}
