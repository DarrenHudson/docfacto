package com.docfacto.core.widgets;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

public class InternalFileDialog extends ElementTreeSelectionDialog {

    private Color theColor = DocfactoUIConstants.COLOR_SEPARATOR;

    public InternalFileDialog(Shell parent,String message) {
        super(parent,new WorkbenchLabelProvider(),new WorkbenchContentProvider());

        IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
        this.setAllowMultiple(false);
        this.setHelpAvailable(false);
        this.setInput(root);
        super.setMessage(message);
        super.setTitle("Choose a file");

    }

    public InternalFileDialog(Shell parent,String message,Color brandColor) {
        this(parent,message);
        theColor = brandColor;
    }

    @Override
    protected Control createDialogArea(Composite parent) {
        Composite comp = (Composite)super.createDialogArea(parent);
        parent.setBackground(DocfactoUIConstants.COLOR_WIDGET_BACKGROUND);
        parent.setBackgroundMode(SWT.INHERIT_FORCE);

        return comp;
    }

    @Override
    protected Label createMessageArea(Composite composite) {
        Label message = (Label)super.createMessageArea(composite);
        message.setForeground(theColor);
        return message;
    }
    
    public void setBrandColour(Color brandColour){
        theColor = brandColour;
    }

}
