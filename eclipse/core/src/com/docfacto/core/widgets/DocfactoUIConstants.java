package com.docfacto.core.widgets;

import org.eclipse.swt.graphics.Color;

import com.docfacto.core.utils.SWTUtils;

/**
 * Docfacto wide widget/UI constants
 * @author kporter - created Oct 14, 2013
 * @since 2.4.7
 */
public class DocfactoUIConstants {
    /**
     * Colour to use for separators
     */
    public final static Color COLOR_SEPARATOR = SWTUtils.getColor(190,190,190);
    
    /**
     * The lightest background colour, used for main/primary toolbars.
     */
    public final static Color COLOR_TOOLBAR_PRIMARY = SWTUtils.getColor(245,245,245);
    
    public final static Color COLOR_WIDGET_BACKGROUND = SWTUtils.getColor(235,235,240);

}
