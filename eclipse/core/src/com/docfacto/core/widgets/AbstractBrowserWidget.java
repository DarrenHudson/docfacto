package com.docfacto.core.widgets;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

/**
 * A widget with a textbox and two buttons for internal and external browser type dialogs.
 * @author kporter - created 3 Dec 2013
 * @since 2.5
 */
public abstract class AbstractBrowserWidget extends Composite {

    /**
     * Use this as a default colour if none other is set
     */
    public static Color COLOUR_TEXT_DEFAULT = DocfactoUIConstants.COLOR_SEPARATOR;
    
    Text theTextBox;
    String theFullPath;

    private ArrayList<Listener> theListeners;
    private final String theLabelText;

    private Label theLabel;

    /**
     * Constructor.
     * @param parent parent of the composite
     * @param style Style of the composite
     * @param labelText The text to display on the widget
     */
    public AbstractBrowserWidget(Composite parent,int style, String labelText) {
        super(parent, style);
        theLabelText = labelText;
        theListeners = new ArrayList<Listener>(1);
        layoutWidget();
    }

    /**
     * Layout the widget
     * 
     * @since 2.4
     */
    private void layoutWidget() {
        this.setLayout(new GridLayout(4, false));
    
        theLabel = new Label(this, SWT.NONE);
        theLabel.setText(theLabelText);
        GridData gd_label = new GridData(SWT.RIGHT,SWT.CENTER,false,false);
        gd_label.widthHint = 100;
        theLabel.setLayoutData(gd_label);
    
        theTextBox = new Text(this, SWT.SINGLE|SWT.BORDER);
        theTextBox.setText("");
        theTextBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
    
        final Label btnInternal = new Label(this, SWT.NONE);
        btnInternal.setImage(PlatformUI.getWorkbench().getSharedImages().getImage(IDE.SharedImages.IMG_OBJ_PROJECT));
        btnInternal.setToolTipText("Select a file in Eclipse Workspace");
    
        final Label btnExternal = new Label(this, SWT.NONE);
        btnExternal.setImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_FOLDER));
        btnInternal.setToolTipText("Select a file outside of Workspace");
    
        btnExternal.addMouseListener(getExternalMouseListener());
        
        btnInternal.addMouseListener(getInternalMouseListener());
        
        theTextBox.addFocusListener(new FocusAdapter() {
            /**
             * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
             */
            @Override
            public void focusLost(FocusEvent event) {
                fireChangeEvent();
            }
        });
    }
    
    /**
     * Check to see that a file has been selected and that it exists
     * 
     * @return true if the widget has a file path and the file exists
     * @since 2.4
     */
    public boolean validate() {
        if (theTextBox.getText().isEmpty()) {
            return false;
        }
    
        // Lets see if its a valid file
        if (theFullPath!=null) {
            return new File(theFullPath).exists();
        }
    
        return new File(theTextBox.getText()).exists();
    
    }

    /**
     * Register a Listener to the list of listeners.
     * 
     * @param listener the listener
     * @since 2.4
     */
    public void registerListener(Listener listener) {
        theListeners.add(listener);
    }
    
    /**
     * Remove a listener from the list of listeners
     * 
     * @param listener THe listener to remove
     * @since 2.5
     */
    public void removeListener(Listener listener){
        theListeners.remove(listener);
    }

    /**
     * Set the file path
     * 
     * @param path to set
     * @since 2.4
     */
    public void setFilePath(String path) {
        theTextBox.setText(path);
    }

    /**
     * Return the selected path of the widget
     * 
     * @return the path of the widget
     * @since 2.4
     */
    public String getSelectedPath() {
        if (theFullPath!=null&&!theFullPath.isEmpty()) {
            return theFullPath;
        }
        return theTextBox.getText();
    }

    /**
     * @see org.eclipse.swt.widgets.Widget#dispose()
     */
    @Override
    public void dispose() {
        theTextBox.dispose();
        theListeners = null;
        super.dispose();
    }

    /**
     * Notify the listeners that something has changed
     * 
     * @since 2.4
     */
    protected void fireChangeEvent() {
        for (Listener listener:theListeners) {
            Event event = new Event();
            event.widget = this;
            event.data = theTextBox.getText();
            listener.handleEvent(event);
        }
    }
    
    /**
     * Gets the displayed Label.
     * @return the Label displayed
     * @since 2.5
     */
    public Label getLabel(){
        return theLabel;
    }
    
    /**
     * Get the displayed text
     * @return The String of text that should be displayed
     * @since 2.5
     */
    public String getLabelText(){
        return theLabelText;
    }

    /**
     * Gets the MouseListener that is added to the external browser button
     * @return the MouseListener to use for the external browser button
     * @since 2.5
     */
    public abstract MouseListener getExternalMouseListener();
    
    /**
     * Gets the MouseListener that is added to the internal browser button
     * @return the MouseListener to use for the internal browser button
     * @since 2.5
     */
    public abstract MouseListener getInternalMouseListener();

}