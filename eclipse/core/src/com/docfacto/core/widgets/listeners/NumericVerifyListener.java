package com.docfacto.core.widgets.listeners;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;

/**
 * Verify Listener for only number input.
 * 
 * Add to any SWT component that allows text entry when numbers only are
 * required.
 * 
 * @author kporter - created Aug 16, 2013
 * @since 2.4.3
 */
public class NumericVerifyListener implements VerifyListener {

    /**
     * @see org.eclipse.swt.events.VerifyListener#verifyText(org.eclipse.swt.events.VerifyEvent)
     */
    @Override
    public void verifyText(VerifyEvent e) {
        switch (e.keyCode) {
            case SWT.BS: // Backspace
            case SWT.DEL: // Delete
            case SWT.HOME: // Home
            case SWT.END: // End
            case SWT.ARROW_LEFT: // Left arrow
            case SWT.ARROW_RIGHT: // Right arrow
                return;
        }

        char chars[] = e.text.toCharArray();
        for(char c: chars){
            if (!Character.isDigit(c)) {
                e.doit = false;
            }
        }
    }

}
