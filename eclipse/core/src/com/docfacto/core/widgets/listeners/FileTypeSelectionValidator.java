package com.docfacto.core.widgets.listeners;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;

import com.docfacto.core.DocfactoCorePlugin;

public class FileTypeSelectionValidator implements ISelectionStatusValidator {

    private String[] theExtensions;
    private String theValidMessage;
    private String theInvalidMessage;

    /**
     * Constructs a new Validator for file types.
     * 
     * @param extensions an array of the extensions acceptable
     * @param validMessage The message that appears when a valid file is chosen.
     * @param invalidMessage The message that appears when an invalid file type is chosen.
     */
    public FileTypeSelectionValidator(String[] extensions,String validMessage,String invalidMessage) {
        theExtensions = extensions;
        theInvalidMessage = invalidMessage;
        theValidMessage = validMessage;
    }

    /**
     * Constructs a validator that works for any file extension
     * 
     * @param validMessage The message that appears when a valid file is chosen.
     * @param invalidMessage The message that appears when an invalid file type is chosen.
     */
    public FileTypeSelectionValidator(String validMessage,String invalidMessage) {
        this(new String[] {"*"}, validMessage, invalidMessage);
    }

    @Override
    public IStatus validate(Object[] selection) {
        if (selection != null && selection.length > 0) {
            IResource res = (IResource)selection[0];
            if (res.getFileExtension() != null && !res.getFileExtension().isEmpty()) {
                String ext = res.getFileExtension();
                if (isValidExtension(ext)) {
                    return new Status(IStatus.OK, DocfactoCorePlugin.PLUGIN_ID, theValidMessage);
                }
            }
        }
        return new Status(IStatus.ERROR, DocfactoCorePlugin.PLUGIN_ID, theInvalidMessage);
    }

    private boolean isValidExtension(String checkExtension) {
        if (theExtensions[0].equals("*"))
            return true;

        for (String extension:theExtensions) {
            if (checkExtension.equalsIgnoreCase(extension))
                return true;
        }
        return false;
    }
}
