package com.docfacto.core.widgets.listeners;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;

/**
 * Verify that only HEX is entered.
 * @author kporter - created Sep 9, 2013
 * @since 2.4.4
 */
public class HexVerifyListener implements VerifyListener {

    /**
     * @see org.eclipse.swt.events.VerifyListener#verifyText(org.eclipse.swt.events.VerifyEvent)
     */
    @Override
    public void verifyText(VerifyEvent e) {
        switch (e.keyCode) {
            case SWT.BS: // Backspace
            case SWT.DEL: // Delete
            case SWT.HOME: // Home
            case SWT.END: // End
            case SWT.ARROW_LEFT: // Left arrow
            case SWT.ARROW_RIGHT: // Right arrow
                return;
        }
        
        char[] chars = e.text.toCharArray();
        for(char c: chars){
            c = Character.toUpperCase(c);
            if(!Character.isDigit(c) && c!='A' && c!='B' 
            && c!='C' && c!='D' && c!='E' && c!='F'){
                e.doit = false;
                return;
            }
            
        }
    }

}
