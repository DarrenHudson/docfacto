package com.docfacto.core.widgets;

import org.eclipse.core.resources.IResource;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;

/**
 * Widget to display a label, text and a browse button for a file dialog.
 * 
 * A listener can be registered so that events can be acted upon.
 * 
 * @author dhudson - created 18 Jul 2013
 * @since 2.4
 */
public class FileBrowserWidget extends AbstractBrowserWidget {

    private InternalFileDialog theInternalDialog;
    private FileDialog theExternalDialog;
    private Object theBaseResource;

    /**
     * Constructs a new FileBrowserWidget that opens the FileDialog at the default base path, and has no extension
     * filter.
     * 
     * @param parent The parent composite
     * @param style Addition style
     * @param labelText The label
     * @since 2.4
     */
    public FileBrowserWidget(Composite parent,int style,String labelText) {
        super(parent, style, labelText);
        theInternalDialog = new InternalFileDialog(getShell(), labelText, COLOUR_TEXT_DEFAULT);
        theExternalDialog = new FileDialog(getShell());
    }

    /**
     * Constructs a new FileBrowserWidget, setting a base path on the FileDialog.
     * 
     * {@docfacto.note It is recommended to call setInternalDialogProps(ISelectionStatusValidator, String, Color) to
     * fully customise the Dialog that pops up for internal files, otherwise the display the default colour, message,
     * and no validator}
     * 
     * @param parent The parent composite
     * @param style Addition style
     * @param labelText The label
     * @param basePath The base path to open when showing the FileDialog
     * @since 2.4
     */
    public FileBrowserWidget(Composite parent,int style,String labelText,String basePath) {
        this(parent, style, labelText);
        setBasePath(basePath);
    }

    /**
     * Constructs a new FileBrowserWidget, setting a filter on extensions to the FileDialog.
     * 
     * {@docfacto.note It is recommended to call setInternalDialogProps(ISelectionStatusValidator, String, Color) to
     * fully customise the Dialog that pops up for internal files, otherwise the display the default colour, message,
     * and no validator}
     * 
     * @param parent The parent composite
     * @param style Addition style
     * @param labelText The label
     * @param fileExtensions The file extensions to filter
     * @since 2.4
     */
    public FileBrowserWidget(Composite parent,int style,String labelText,String[] fileExtensions) {
        this(parent, style, labelText);
        theExternalDialog.setFilterExtensions(fileExtensions);
    }

    /**
     * Constructs a new FileBrowserWidget, setting a filter on extensions to the FileDialog.
     * 
     * @param parent The parent composite
     * @param style Addition style
     * @param labelText The label
     * @param fileExtensions The file extensions to filter
     * @param validator The {@link ISelectionStatusValidator} for the internal file dialog
     * @since 2.4.9
     */
    public FileBrowserWidget(Composite parent,int style,String labelText,String[] fileExtensions,
    ISelectionStatusValidator validator) {
        this(parent, style, labelText, fileExtensions);
        theInternalDialog.setValidator(validator);
    }

    /**
     * Constructs a new FileBrowserWidget, setting a base path and a filter on extensions to the FileDialog.
     * 
     * {@docfacto.note It is recommended to call setInternalDialogProps(ISelectionStatusValidator, String, Color) to
     * fully customise the Dialog that pops up for internal files, otherwise the display the default colour, message,
     * and no validator}
     * 
     * @param parent The parent composite
     * @param style Addition style
     * @param labelText The label
     * @param basePath The base path to open when showing the FileDialog
     * @param fileExtensions The file extensions to filter
     * @since 2.4
     */
    public FileBrowserWidget(Composite parent,int style,String labelText,String basePath,String[] fileExtensions) {
        this(parent, style, labelText, basePath);
        theExternalDialog.setFilterExtensions(fileExtensions);
    }

    /**
     * Constructs a new FileBrowserWidget, setting a filter on extensions to the FileDialog.
     * 
     * @param parent The parent composite
     * @param style Addition style
     * @param labelText The label
     * @param basePath The base path to open when showing the FileDialog
     * @param fileExtensions The file extensions to filter
     * @param validator The {@link ISelectionStatusValidator} for the internal file dialog
     * @since 2.4.9
     */
    public FileBrowserWidget(Composite parent,int style,String labelText,String basePath,String[] fileExtensions,
    ISelectionStatusValidator validator) {
        this(parent, style, labelText, basePath);
        theExternalDialog.setFilterExtensions(fileExtensions);
        theInternalDialog.setValidator(validator);
    }

    public void layout() {

    }

    /**
     * Set the validator to use for internal file dialogs
     * 
     * @param validator The validator
     * @since 2.4.9
     */
    public void setInternalValidator(ISelectionStatusValidator validator) {
        theInternalDialog.setValidator(validator);
    }

    @Override
    public MouseListener getInternalMouseListener() {
        return new MouseAdapter() {
            @Override
            public void mouseDown(MouseEvent e) {
                theInternalDialog.setInitialSelection(theBaseResource);
                if (theInternalDialog.open()==Dialog.OK) {
                    IResource res = (IResource)theInternalDialog.getFirstResult();
                    theTextBox.setText(res.getFullPath().toString());
                    theFullPath = res.getLocation().toString();
                    theBaseResource = theInternalDialog.getFirstResult();
                    setBasePath(theFullPath);
                    fireChangeEvent();
                }
            }
        };
    }

    @Override
    public MouseListener getExternalMouseListener() {
        return new MouseAdapter() {
            @Override
            public void mouseDown(MouseEvent e) {
                String path = theExternalDialog.open();
                if (path!=null) {
                    theTextBox.setText(path);
                    setBasePath(path);
                    // Fire change event
                    fireChangeEvent();
                }
            }
        };
    }

    /**
     * @see FileDialog#setBasePath(String)
     */
    public void setBasePath(String base) {
        theExternalDialog.setFilterPath(base);
    }

    /**
     * Set the brand colour for the dialogs.
     * 
     * @param brandColor A brand colour
     * @since 2.4.9
     */
    public void setBrandColor(Color brandColor) {
        theInternalDialog.setBrandColour(brandColor);
    }

}
