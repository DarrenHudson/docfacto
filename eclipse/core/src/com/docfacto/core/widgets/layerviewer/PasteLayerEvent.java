package com.docfacto.core.widgets.layerviewer;

/**
 * A paste layer event
 * @author kporter - created Oct 10, 2013
 * @since 2.4.7
 */
public class PasteLayerEvent extends LayerEvent {

    private AbstractLayer<?> pastePosition;

    /**
     * Constructor.
     * @param action
     */
    public PasteLayerEvent(AbstractLayer<?> layerToPasteUnder) {
        super(Action.PASTE);
        pastePosition = layerToPasteUnder;
    }
    
    /**
     * Returns pastePosition.
     *
     * @return the pastePosition
     */
    public AbstractLayer<?> getPastePosition() {
        return pastePosition;
    }

}
