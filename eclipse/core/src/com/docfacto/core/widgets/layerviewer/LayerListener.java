package com.docfacto.core.widgets.layerviewer;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DragDetectEvent;
import org.eclipse.swt.events.DragDetectListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Control;

import com.docfacto.core.utils.SWTUtils;

/**
 * The listeners for Layers
 * 
 * @author kporter - created Oct 7, 2013
 * @since 2.4.6
 */
public class LayerListener implements MouseMoveListener,DragDetectListener,MouseListener {
    private static LayerListener theInstance;

    private ILayerController theController;

    private boolean theDragState = false;
    private AbstractLayer<?> theHovered = null;

    private ArrayList<AbstractLayer<?>> theSelectedLayers;

    /**
     * Blocks construction
     */
    private LayerListener(ILayerController controller) {
        theController = controller;
        theSelectedLayers = theController.getSelectedLayers();

    }

    /**
     * Gets a singleton instance of the LayerListener
     * 
     * @param controller The ILayerController needed
     * @return An instance of this listener
     * @since 2.4.6
     */
    public static LayerListener getInstance(ILayerController controller) {
        if (theInstance==null) {
            theInstance = new LayerListener(controller);
        }
        return theInstance;
    }

    /**
     * @see org.eclipse.swt.events.DragDetectListener#dragDetected(org.eclipse.swt.events.DragDetectEvent)
     */
    @Override
    public void dragDetected(DragDetectEvent e) {
        theDragState = true;
    }

    /**
     * @see org.eclipse.swt.events.MouseMoveListener#mouseMove(org.eclipse.swt.events.MouseEvent)
     */
    @Override
    public void mouseMove(MouseEvent e) {
        if (theDragState) {
            Control control = (Control)e.widget;
            Point pointOnWidget = SWTUtils.getRelativeLocation(control, new Point(e.x, e.y));

            for (AbstractLayer<?> layer:theController.getLayers()) {
                layer.highlightLine(false);
                if (layer.getBounds().contains(pointOnWidget)) {
                    int halfway = (layer.getSize().y/2);

                    if (pointOnWidget.y>(layer.getLocation().y+halfway)+5) {
                        layer.setHighlightState(HighlightState.HIGHLIGHT_BOTTOM);
                        theHovered = layer;
                        break;
                    }
                    else if (pointOnWidget.y<(layer.getLocation().y+halfway)-3) {
                        layer.setHighlightState(HighlightState.NO_HIGHLIGHT);
                        theHovered = null;
                        break;

                    }
                    else {
                        System.out.println("mid highlight");
                        layer.setHighlightState(HighlightState.HIGHLIGHT_OVER);
                        theHovered = layer;
                        break;
                    }
                }
                else {
                    theHovered = null;
                }
            }
        }
    }

    /**
     * @see org.eclipse.swt.events.MouseListener#mouseUp(org.eclipse.swt.events.MouseEvent)
     */
    @Override
    public void mouseUp(MouseEvent e) {
        theDragState = false;

        if (theHovered!=null) {
            AbstractLayer<?> toMove = (AbstractLayer<?>)e.widget;
            switch (theHovered.getHighlightState()) {
            case HIGHLIGHT_BOTTOM:
                theController.processEvent(new MoveLayerEvent(toMove, theHovered));
                break;

            case HIGHLIGHT_OVER:
                theController.processEvent(new GroupLayerEvent(theHovered, toMove));
                break;

            case NO_HIGHLIGHT:
                // Nothing to do
                break;
            }
            theHovered.setHighlightState(HighlightState.NO_HIGHLIGHT);
        }
    }

    /**
     * @see org.eclipse.swt.events.MouseListener#mouseDoubleClick(org.eclipse.swt.events.MouseEvent)
     */
    @Override
    public void mouseDoubleClick(MouseEvent e) {
        // TODO Auto-generated method stub
    }

    /**
     * @see org.eclipse.swt.events.MouseListener#mouseDown(org.eclipse.swt.events.MouseEvent)
     */
    @Override
    public void mouseDown(MouseEvent e) {
        AbstractLayer<?> selected = (AbstractLayer<?>)e.widget;
        boolean state = selected.isSelected();
        // selected.getParent().getChildren()
        if (e.stateMask==SWT.COMMAND) {
            selected.setSelected(!state);
        }
        else {
            for (AbstractLayer<?> layer:theController.getLayers()) {
                layer.setSelected(false);
            }
            selected.setSelected(true);
        }
    }

}