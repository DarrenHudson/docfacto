package com.docfacto.core.widgets.layerviewer;

/**
 * TODO - Title.
 *<P>
 * TODO - Description.
 *
 * @author kporter - created Oct 9, 2013
 * @since n.n
 */
public class MoveLayerEvent extends LayerEvent {

    private AbstractLayer<?> theMovedLayer;
    private AbstractLayer<?> thePositionLayer;
    
    /**
     * Constructor.
     * @param action
     */
    public MoveLayerEvent(AbstractLayer<?> toInsert, AbstractLayer<?> position) {
        super(Action.MOVE);
        theMovedLayer = toInsert;
        thePositionLayer = position;
    }
    /**
     * Returns the layer that has been moved
     *
     * @return the layer to be moved
     */
    public AbstractLayer<?> getMovedLayer() {
        return theMovedLayer;
    }
    /**
     * Returns the layer in which position the other layer is to be inserted below
     *
     * @return the positional layer
     */
    public AbstractLayer<?> getPositionLayer() {
        return thePositionLayer;
    }
}
