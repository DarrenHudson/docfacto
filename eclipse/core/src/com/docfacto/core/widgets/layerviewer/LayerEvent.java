package com.docfacto.core.widgets.layerviewer;

/**
 * Events for layers.
 * 
 * @author kporter - created Oct 7, 2013
 * @since 2.4.7
 */
public class LayerEvent {

    /**
     * The actions {@docfacto.note add here to extend}
     */
    enum Action {

        /**
         * A move layer action
         */
        MOVE,

        /**
         * A group layer action
         */
        GROUP,

        /**
         * A copy layer action
         */
        COPY,

        /**
         * A paste layer action
         */
        PASTE,

        /**
         * A delete layer action
         */
        DELETE,
    }

    private final Action theAction;

    /**
     * Constructs a new LayerEvent
     * 
     * @param action The type of this event
     */
    public LayerEvent(Action action) {
        theAction = action;
    }

    /**
     * Gets the type of event this is
     * 
     * @return The type of action this event is
     * @since 2.4.7
     */
    public Action getAction() {
        return theAction;
    }

    /**
     * Check if this a move event
     * 
     * @return true if this is a move event
     * @since 2.4.7
     */
    public boolean isMoveEvent() {
        return theAction==Action.MOVE;
    }

    /**
     * Check if this a group event
     * 
     * @return true if this is a group event
     * @since 2.4.7
     */
    public boolean isGroupEvent() {
        return theAction==Action.GROUP;
    }

    /**
     * Check if this a copy event
     * 
     * @return true if this is a copy event
     * @since 2.4.7
     */
    public boolean isCopyEvent() {
        return theAction==Action.COPY;
    }

    /**
     * Check if this a copy event
     * 
     * @return true if this is a copy event
     * @since 2.4.7
     */
    public boolean isPasteEvent() {
        return theAction==Action.PASTE;
    }

}
