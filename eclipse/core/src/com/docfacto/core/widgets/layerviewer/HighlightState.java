package com.docfacto.core.widgets.layerviewer;

/**
 * The states a highlight on a layer.
 * 
 * @author kporter - created Oct 7, 2013
 * @since 2.4.7
 */
public enum HighlightState {
    HIGHLIGHT_OVER,

    HIGHLIGHT_BOTTOM,

    NO_HIGHLIGHT
}
