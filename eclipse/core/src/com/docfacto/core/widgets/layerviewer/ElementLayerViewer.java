package com.docfacto.core.widgets.layerviewer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.docfacto.core.utils.SWTUtils;

/**
 * Layer viewer for Element Layers
 * 
 * @author kporter - created Oct 3, 2013
 * @since 2.4.6
 */
public class ElementLayerViewer extends AbstractLayerViewer {

    private static final Color DEFAULT_BACKGROUND = SWTUtils.getColor(90, 90, 90);

    /**
     * Constructor.
     * 
     * @param parent
     * @param style
     */
    public ElementLayerViewer(Composite parent,int style) {
        super(parent, style, Orientation.NEW_LAYER_ON_BOTTOM);
        for (int i = 0;i<5;i++) {
            ElementLayer c = new ElementLayer(this);
            c.setLayoutData(getGridData());
            c.setText("group"+(i+1));
            addLayer(c);
        }
    }

    /**
     * @see com.docfacto.core.widgets.layerviewer.AbstractLayerViewer#createLayer()
     */
    @Override
    AbstractLayer<?> createLayer() {
        return new ElementLayer(this);
    }

    /**
     * @see com.docfacto.core.widgets.layerviewer.AbstractLayerViewer#getLayerPreview()
     */
    @Override
    public Image getLayerPreview() {
        return null;
    }

    /**
     * Launch the application.
     * 
     * @param args
     */
    public static void main(String args[]) {
        try {
            Display display = Display.getDefault();
            Shell shell = new Shell(display);
            shell.addPaintListener(new PaintListener() {
                @Override
                public void paintControl(PaintEvent e) {
                    e.gc.setBackground(SWTUtils.getColor(SWT.COLOR_WHITE));
                    e.gc.setAlpha(10);
                    e.gc.fillRectangle(0, 0, e.width, e.height);
                }
            });
            shell.setLayout(new FillLayout());
            @SuppressWarnings("unused")
            ElementLayerViewer layer = new ElementLayerViewer(shell, SWT.NONE);
            shell.open();
            shell.layout();
            shell.pack();
            while (!shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                    display.sleep();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @see com.docfacto.core.widgets.layerviewer.AbstractLayerViewer#doMove(com.docfacto.core.widgets.layerviewer.AbstractLayer,
     * com.docfacto.core.widgets.layerviewer.AbstractLayer)
     */
    @Override
    boolean doMove(AbstractLayer<?> toMove,AbstractLayer<?> position) {
        // TODO Auto-generated method stub
        return false;
    }

    /**
     * @see com.docfacto.core.widgets.layerviewer.AbstractLayerViewer#doGrouping(com.docfacto.core.widgets.layerviewer.AbstractLayer,
     * com.docfacto.core.widgets.layerviewer.AbstractLayer)
     */
    @Override
    void doGrouping(AbstractLayer<?> layerToGroup,AbstractLayer<?> group) {
        // TODO Auto-generated method stub

    }

    /**
     * @see com.docfacto.core.widgets.layerviewer.AbstractLayerViewer#copyLayer(com.docfacto.core.widgets.layerviewer.AbstractLayer)
     */
    @Override
    public void copyLayer(AbstractLayer<?> layerToCopy) {
        // TODO Auto-generated method stub

    }

    /**
     * @see com.docfacto.core.widgets.layerviewer.AbstractLayerViewer#doPaste(com.docfacto.core.widgets.layerviewer.AbstractLayer,
     * com.docfacto.core.widgets.layerviewer.AbstractLayer)
     */
    @Override
    boolean doPaste(AbstractLayer<?> layerToPaste,AbstractLayer<?> pasteLocation) {
        // TODO Auto-generated method stub
        return false;
    }

    /**
     * @see com.docfacto.core.widgets.layerviewer.AbstractLayerViewer#getBackgroundColour()
     */
    @Override
    public Color getBackgroundColour() {
        return DEFAULT_BACKGROUND;
    }
}
