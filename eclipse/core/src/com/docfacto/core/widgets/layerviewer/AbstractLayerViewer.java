package com.docfacto.core.widgets.layerviewer;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

/**
 * A layer viewer widget
 * 
 * @author kporter - created Oct 7, 2013
 * @since 2.4.7
 */
public abstract class AbstractLayerViewer extends Composite implements ILayerController {

    enum Orientation {
        NEW_LAYER_ON_TOP,

        NEW_LAYER_ON_BOTTOM
    }

    private ArrayList<AbstractLayer<?>> theLayers;
    private ArrayList<AbstractLayer<?>> theSelectedLayers;
    private Orientation theOrientation;

    /**
     * Constructor.
     * 
     * @param parent
     * @param style
     * @since
     */
    public AbstractLayerViewer(Composite parent,int style) {
        super(parent, style);
        theLayers = new ArrayList<AbstractLayer<?>>();
        theSelectedLayers = new ArrayList<AbstractLayer<?>>();
        theOrientation = Orientation.NEW_LAYER_ON_TOP;

        setBackground(getBackgroundColour());

        GridLayout gridLayout = new GridLayout(1, true);
        gridLayout.marginHeight = 2;
        gridLayout.marginWidth = 2;
        gridLayout.verticalSpacing = 1;
        setLayout(gridLayout);
    }

    public AbstractLayerViewer(Composite parent,int style,Orientation orientation) {
        this(parent, style);
        theOrientation = orientation;
    }

    public abstract Color getBackgroundColour();

    /**
     * @see com.docfacto.core.widgets.layerviewer.ILayerController#processEvent(com.docfacto.core.widgets.layerviewer.LayerEvent)
     */
    @Override
    public void processEvent(LayerEvent event) {
        if (event.isMoveEvent()) {
            MoveLayerEvent moveEvent = (MoveLayerEvent)event;
            moveLayer(moveEvent.getMovedLayer(), moveEvent.getPositionLayer());
        }
        else if (event.isGroupEvent()) {
            GroupLayerEvent groupEvent = (GroupLayerEvent)event;
            groupLayer(groupEvent.getLayerToGroup(), groupEvent.getGroup());
        }
        else if (event.isCopyEvent()) {
            CopyLayerEvent copyEvent = (CopyLayerEvent)event;
            copyLayer(copyEvent.getLayer());
        }
        else if (event.isPasteEvent()) {
            PasteLayerEvent pasteEvent = (PasteLayerEvent)event;
//             pasteLayer(layerToPaste,pasteLocation)
        }

    }

    public GridData getGridData() {
        GridData gd = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
        gd.widthHint = 196;
        gd.heightHint = 22;
        return gd;
    }

    public void addLayer() {
        addLayer(createLayer());
    }

    public void addLayer(AbstractLayer<?> layer) {
        doAdd(layer);
        afterAdd(layer);
    }

    abstract AbstractLayer<?> createLayer();

    private void doAdd(AbstractLayer<?> layer) {
        layer.setLayoutData(getGridData());
        if (theOrientation==Orientation.NEW_LAYER_ON_TOP) {
            if (!theLayers.isEmpty()) {
                layer.moveAbove(theLayers.get(theLayers.size()-1));
            }
        }
        theLayers.add(layer);
    }

    private void afterAdd(AbstractLayer<?> layer) {
        deselectLayers();
        selectLayer(layer);
    }

    public void moveLayer(AbstractLayer<?> toMove,AbstractLayer<?> position) {
        boolean move = doMove(toMove, position);
        if (move) {
            afterMove(toMove, position);
        }
    }

    abstract boolean doMove(AbstractLayer<?> toMove,AbstractLayer<?> position);

    private void afterMove(AbstractLayer<?> toMove,AbstractLayer<?> position) {
        System.out.println("move");
        toMove.moveBelow(position);
        this.layout();
    }

    public void groupLayer(AbstractLayer<?> layerToGroup,AbstractLayer<?> group) {
        doGrouping(layerToGroup, group);
        afterGrouping(layerToGroup, group);
    }

    abstract void doGrouping(AbstractLayer<?> layerToGroup,AbstractLayer<?> group);

    private void afterGrouping(AbstractLayer<?> layerToGroup,AbstractLayer<?> group) {
    }

    public abstract void copyLayer(AbstractLayer<?> layerToCopy);

    public void pasteLayer(AbstractLayer<?> pasteLocation) {
        boolean paste = doPaste(null, pasteLocation);
        if (paste) {
            afterPaste(null, pasteLocation);
        }
    }

    abstract boolean doPaste(AbstractLayer<?> layerToPaste,AbstractLayer<?> pasteLocation);

    private void afterPaste(AbstractLayer<?> layerToPaste,AbstractLayer<?> pasteLocation) {
        // TODO Auto-generated method stub
    }

    private void selectLayer(AbstractLayer<?> layer) {
        layer.setSelected(true);
        theSelectedLayers.add(layer);
    }

    private void deselectLayers() {
        for (AbstractLayer<?> layer:getSelectedLayers()) {
            layer.setSelected(false);
        }
        theSelectedLayers.clear();
    }

    /**
     * @see com.docfacto.core.widgets.layerviewer.ILayerController#getLayers()
     */
    @Override
    public ArrayList<AbstractLayer<?>> getLayers() {
        return theLayers;
    }

    /**
     * @see com.docfacto.core.widgets.layerviewer.ILayerController#getSelectedLayers()
     */
    @Override
    public ArrayList<AbstractLayer<?>> getSelectedLayers() {
        return theSelectedLayers;
    }

    public abstract Image getLayerPreview();

}