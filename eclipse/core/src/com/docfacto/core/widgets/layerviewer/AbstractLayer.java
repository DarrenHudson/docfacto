package com.docfacto.core.widgets.layerviewer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

/**
 * A Composite representing a layer and holding a piece of data.
 * 
 * @author kporter - created Oct 7, 2013
 * @param <T> The type of data this layer holds
 * @since 2.4.7
 */
public abstract class AbstractLayer<T> extends Composite {

    Label theHighlighter;
    Label theText;
    private HighlightState theState;
    private boolean isSelected = false;

    public abstract T getLayerData(T data);

    public abstract void setLayerData(T data);

    public abstract Color getNormalColor();

    public abstract Color getSelectedColor();

    public abstract Color getHighlightColor();

    public abstract void highlightLayer(boolean highlight);

    public abstract boolean isGrouper();

    public AbstractLayer(AbstractLayerViewer parent) {
        super(parent, SWT.NONE);
        theState = HighlightState.NO_HIGHLIGHT;
        addListeners(LayerListener.getInstance(parent));

        setBackground(getNormalColor());

        GridLayout layout = new GridLayout(1, false);
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        layout.marginTop = 2;
        layout.verticalSpacing = 0;
        layout.horizontalSpacing = 0;
        setLayout(layout);

        theText = new Label(this, SWT.NONE);
        GridData gdText = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gdText.horizontalIndent = 10;
        theText.setLayoutData(gdText);
        theText.setText("Group");
        theText.setEnabled(false);

        theHighlighter = new Label(this, SWT.NONE);
        GridData gdHighlighter = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        gdHighlighter.verticalIndent = 2;
        gdHighlighter.minimumHeight = 2;
        gdHighlighter.heightHint = 2;
        theHighlighter.setLayoutData(gdHighlighter);

    }

    protected void addListeners(LayerListener listener) {
        addMouseMoveListener(listener);
        addDragDetectListener(listener);
        addMouseListener(listener);
    }

    public HighlightState getHighlightState() {
        return theState;
    }

    public void setHighlightState(HighlightState state) {
        switch (state) {
        case HIGHLIGHT_BOTTOM:
            highlightLine(true);
            highlightLayer(false);
            theState = HighlightState.HIGHLIGHT_BOTTOM;
            break;
        case HIGHLIGHT_OVER:
            highlightLine(false);
            highlightLayer(true);
            theState = HighlightState.HIGHLIGHT_OVER;
            break;
        case NO_HIGHLIGHT:
            highlightLine(false);
            highlightLayer(false);
            theState = HighlightState.NO_HIGHLIGHT;
            break;
        }
    }

    public void setSelected(boolean selected) {
        System.out.println(selected);

        isSelected = selected;
        if (isSelected) {
            this.setBackground(getSelectedColor());
        }
        else {
            this.setBackground(getNormalColor());
        }
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setText(String text) {
        theText.setText(text);
    }

    public void highlightLine(boolean show) {
        if (show) {
            theHighlighter.setBackground(getHighlightColor());
        }
        else {
            if (theState==HighlightState.HIGHLIGHT_BOTTOM) {
                theHighlighter.setBackground(this.getBackground());
            }
        }
    }

    /**
     * @see org.eclipse.swt.widgets.Widget#toString()
     */
    @Override
    public String toString() {
        return theText.getText();
    }

}