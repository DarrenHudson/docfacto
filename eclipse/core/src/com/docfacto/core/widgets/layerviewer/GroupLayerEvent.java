package com.docfacto.core.widgets.layerviewer;

/**
 * A group layer event
 * 
 * @author kporter - created Oct 9, 2013
 * @since 2.4.7
 */
public class GroupLayerEvent extends LayerEvent {

    private AbstractLayer<?> theGroup;
    private AbstractLayer<?> theLayerToGroup;

    /**
     * Constructor.
     * 
     * @param action
     */
    public GroupLayerEvent(AbstractLayer<?> toGroup,AbstractLayer<?> groupLayer) {
        super(Action.GROUP);
        theLayerToGroup = toGroup;
        theGroup = groupLayer;
    }

    /**
     * Returns group.
     * 
     * @return the group
     */
    public AbstractLayer<?> getGroup() {
        return theGroup;
    }

    /**
     * Returns layerToGroup.
     * 
     * @return the layerToGroup
     */
    public AbstractLayer<?> getLayerToGroup() {
        return theLayerToGroup;
    }

}
