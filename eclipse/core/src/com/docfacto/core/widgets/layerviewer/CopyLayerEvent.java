package com.docfacto.core.widgets.layerviewer;

/**
 * A copy layer event
 * 
 * @author kporter - created Oct 10, 2013
 * @since 2.4.7
 */
public class CopyLayerEvent extends LayerEvent {

    private AbstractLayer<?> theLayer;

    /**
     * Constructor.
     * 
     * @param action
     */
    public CopyLayerEvent(AbstractLayer<?> layerToCopy) {
        super(Action.COPY);
        theLayer = layerToCopy;
    }

    /**
     * Returns the layer to copy
     * 
     * @return the layer to copy
     * @since 2.4.7
     */
    public AbstractLayer<?> getLayer() {
        return theLayer;
    }

}
