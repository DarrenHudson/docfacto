package com.docfacto.core.widgets.layerviewer;

import org.eclipse.swt.graphics.Color;
import org.w3c.dom.Element;

import com.docfacto.core.utils.SWTUtils;

public class ElementLayer extends AbstractLayer<Element> {
    public final static Color DEFAULT_HIGHLIGHT_COLOUR = SWTUtils.getColor(252, 185, 107);

    public final static Color DEFAULT_BACKGROUND_COLOUR = SWTUtils.getColor(223, 223, 223);

    public final static Color DEFAULT_SELECT_COLOUR = SWTUtils.getColor(179, 223, 236);

    /**
     * Constructor.
     */
    public ElementLayer(AbstractLayerViewer parent) {
        super(parent);
    }

    @Override
    public void highlightLayer(boolean highlight) {
        // nothing
    }

    /**
     * @see com.docfacto.core.widgets.layerviewer.ILayer#getLayerData(java.lang.Object)
     */
    @Override
    public Element getLayerData(Element data) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see com.docfacto.core.widgets.layerviewer.ILayer#setLayerData(java.lang.Object)
     */
    @Override
    public void setLayerData(Element data) {
        // TODO Auto-generated method stub
    }

    /**
     * @see com.docfacto.core.widgets.layerviewer.AbstractLayer#getNormalColor()
     */
    @Override
    public Color getNormalColor() {
        return DEFAULT_BACKGROUND_COLOUR;
    }

    /**
     * @see com.docfacto.core.widgets.layerviewer.AbstractLayer#getSelectedColor()
     */
    @Override
    public Color getSelectedColor() {
        return DEFAULT_SELECT_COLOUR;
    }

    /**
     * @see com.docfacto.core.widgets.layerviewer.AbstractLayer#getHighlightColor()
     */
    @Override
    public Color getHighlightColor() {
        return DEFAULT_HIGHLIGHT_COLOUR;
    }

    /**
     * @see com.docfacto.core.widgets.layerviewer.AbstractLayer#isGrouper()
     */
    @Override
    public boolean isGrouper() {
        return false;
    }

}
