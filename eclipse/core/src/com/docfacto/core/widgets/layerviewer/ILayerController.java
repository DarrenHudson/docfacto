package com.docfacto.core.widgets.layerviewer;

import java.util.ArrayList;

/**
 * TODO - Title.
 * <P>
 * TODO - Description.
 * 
 * @author kporter - created Oct 7, 2013
 * @since n.n
 */
public interface ILayerController {

    public void processEvent(LayerEvent event);

    public ArrayList<AbstractLayer<?>> getLayers();

    public ArrayList<AbstractLayer<?>> getSelectedLayers();

}
