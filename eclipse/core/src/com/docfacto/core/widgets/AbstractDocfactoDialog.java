package com.docfacto.core.widgets;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

/**
 * For uniformity all dialogs for docfacto should extend this.
 * 
 * @author kporter - created Oct 14, 2013
 * @since 2.4.7
 */
public abstract class AbstractDocfactoDialog extends Dialog {

    /**
     * Create the dialog.
     * @param shell
     */
    public AbstractDocfactoDialog(Shell shell) {
        super(shell);
    }

    /**
     * Create contents of the dialog.
     * @param parent The parent composite
     */
    @Override
    protected Control createDialogArea(Composite parent) {
        Composite container = (Composite)super.createDialogArea(parent);
        GridLayout layout = (GridLayout)container.getLayout();
        layout.verticalSpacing = 0;
        layout.marginWidth = 0;
        layout.marginTop = 0;
        layout.marginHeight = 0;
        
        Composite titleBar = new Composite(container,SWT.NONE);
        titleBar.setBackground(DocfactoUIConstants.COLOR_TOOLBAR_PRIMARY);

        GridData gdTitle = new GridData(SWT.FILL,SWT.CENTER,false,false);
        gdTitle.heightHint = 33;
        titleBar.setLayoutData(gdTitle);
        
        RowLayout rl_titleBar = new RowLayout(SWT.HORIZONTAL);
        rl_titleBar.marginTop = 10;
        rl_titleBar.marginRight = 0;
        rl_titleBar.marginLeft = 0;
        rl_titleBar.marginBottom = 0;
        rl_titleBar.fill = true;
        rl_titleBar.justify = true;
        titleBar.setLayout(rl_titleBar);
        
        Label title = new Label(titleBar, SWT.NONE);
        title.setText(getDialogTitle());
        title.setForeground(getBrandColor());
        
        Label spacer = new Label(container,SWT.NONE);
        GridData gdSpacer = new GridData(SWT.FILL,SWT.CENTER,true,false);
        gdSpacer.heightHint = 1;
        spacer.setLayoutData(gdSpacer);
        spacer.setBackground(getBrandComplementColor());
        
        Composite content = new Composite(container, SWT.NONE);
        content.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        content.setBackground(getBackgroundMain());
        GridLayout contentGrid = new GridLayout(1,false);
        contentGrid.marginBottom = 1;
        contentGrid.marginWidth = 0;
        content.setLayout(contentGrid);
        return content;
    }
    
    protected abstract String getDialogTitle();
    
    public abstract Color getBrandColor();
    
    public abstract Color getBrandComplementColor();
    
    public static Color getBackgroundMain(){
        return DocfactoUIConstants.COLOR_WIDGET_BACKGROUND;
    }
    
    public static Color getToolbarColor(){
        return DocfactoUIConstants.COLOR_TOOLBAR_PRIMARY;
    }
    
    /**
     * Create contents of the button bar.
     * @param parent
     */
    @Override
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent,IDialogConstants.OK_ID,IDialogConstants.OK_LABEL,true);
        createButton(parent,IDialogConstants.CANCEL_ID,IDialogConstants.CANCEL_LABEL,false);
    }
    
    /**
     * @see org.eclipse.jface.dialogs.Dialog#createButtonBar(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createButtonBar(Composite parent) {
        Composite composite = (Composite) super.createButtonBar(parent);
        GridLayout layout = (GridLayout)composite.getLayout();
        layout.marginHeight = 3;
        composite.setBackground(getBackgroundMain());
        return composite;
    }

}
