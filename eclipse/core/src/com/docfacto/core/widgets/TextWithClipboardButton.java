package com.docfacto.core.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

/**
 * A Component which as a text box and a copy to clipboard button.
 * 
 * @author dhudson - created 19 Jul 2013
 * @since 2.4
 */
public class TextWithClipboardButton extends Composite {

    private final String theLabelText;
    private Text theTextBox;
    private Clipboard theClipboard;

    /**
     * Constructor.
     * @param parent
     * @param labelText
     * @param style
     * @since 2.4
     */
    public TextWithClipboardButton(Composite parent,String labelText,int style) {
        super(parent,style);
        theLabelText = labelText;
        theClipboard = new Clipboard(Display.getCurrent());
        layoutWidget();
    }

    private void layoutWidget() {
        GridLayout gridLayout = new GridLayout();
        gridLayout.numColumns = 3;

        setLayout(gridLayout);

        Label label = new Label(this,SWT.NONE);
        label.setText(theLabelText);

        GridData gd = new GridData(GridData.FILL_HORIZONTAL);

        theTextBox = new Text(this,SWT.SINGLE|SWT.BORDER);
        theTextBox.setText("");

        gd = new GridData();
        gd.horizontalAlignment = SWT.FILL;
        gd.grabExcessHorizontalSpace = true;

        theTextBox.setLayoutData(gd);

        Button button = new Button(this,SWT.PUSH);
        button.setImage(PlatformUI.getWorkbench().getSharedImages()
            .getImage(ISharedImages.IMG_TOOL_COPY));

        gd = new GridData(SWT.CENTER,SWT.CENTER,false,false);
        button.setLayoutData(gd);

        button.addListener(SWT.Selection,new Listener() {
            /**
             * @see org.eclipse.swt.widgets.Listener#handleEvent(org.eclipse.swt.widgets.Event)
             */
            public void handleEvent(Event e) {
                String text = theTextBox.getText();

                if (text.length()>0) {
                    TextTransfer textTransfer = TextTransfer.getInstance();
                    theClipboard.setContents(new Object[] {text},
                        new Transfer[] {textTransfer});
                }
            }
        });
    }

    /**
     * @see org.eclipse.swt.widgets.Widget#dispose()
     */
    @Override
    public void dispose() {
        super.dispose();
        theTextBox.dispose();
        theClipboard.dispose();
    }
}
