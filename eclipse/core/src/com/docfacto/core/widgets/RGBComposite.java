package com.docfacto.core.widgets;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.docfacto.core.utils.SWTUtils;
import com.docfacto.core.widgets.listeners.HexVerifyListener;
import com.docfacto.core.widgets.listeners.NumericVerifyListener;

/**
 * A Composite for displaying RGB values.
 * 
 * If you need to listen for changes to the RGB, implement RGBListener.
 * 
 * @author kporter - created Sep 6, 2013
 * @since 2.4.4
 */
public class RGBComposite extends Composite {
    private Text theR;
    private Text theG;
    private Text theB;
    private Text theHex;
    private Label _lblG;
    private Label _lblR;
    private Label _lblB;
    private Label _lblHash;
    private ArrayList<RGBListener> listeners;

    /**
     * Create the composite.
     * @param parent The parent composite
     * @param style The style int
     * @since 2.4.4
     */
    public RGBComposite(Composite parent,int style) {
        super(parent,style);
        setLayout(new GridLayout(3, false));
        listeners = new ArrayList<RGBComposite.RGBListener>(1);
        
        _lblR = new Label(this, SWT.NONE);
        _lblR.setText("R");
        
        GridData gd_text_1 = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
        gd_text_1.widthHint = 27;
        theR = new Text(this, SWT.BORDER);
        theR.setLayoutData(gd_text_1);
        theR.setTextLimit(3);
        new Label(this, SWT.NONE);
        
        _lblG = new Label(this, SWT.NONE);
        _lblG.setText("G");
        
        theG = new Text(this, SWT.BORDER);
        theG.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        theG.setTextLimit(3);
        new Label(this, SWT.NONE);
        
        _lblB = new Label(this, SWT.NONE);
        _lblB.setText("B");
        
        GridData gd_text_2 = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
        theB = new Text(this, SWT.BORDER);
        theB.setLayoutData(gd_text_2);
        theB.setTextLimit(3);
        
        //Fillers for the gridlayout
        new Label(this, SWT.NONE);
        new Label(this, SWT.NONE);
        new Label(this, SWT.NONE);
        new Label(this, SWT.NONE);
        
        _lblHash = new Label(this, SWT.NONE);
        _lblHash.setText("#");
        
        theHex = new Text(this, SWT.BORDER);
        GridData gd_text_3 = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
        gd_text_3.widthHint = 50;
        theHex.setLayoutData(gd_text_3);
        theHex.setTextLimit(6);
        
        theHex.addVerifyListener(new HexVerifyListener());
        NumericVerifyListener verify = new NumericVerifyListener();
        theR.addVerifyListener(verify);
        theG.addVerifyListener(verify);
        theB.addVerifyListener(verify);
        
        RGBModifyListener modify = new RGBModifyListener();
        theR.addKeyListener(modify);
        theG.addKeyListener(modify);
        theB.addKeyListener(modify);
        
        theR.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                setVerifiedRGB((Text)e.getSource());
            }
        });
        theG.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                setVerifiedRGB((Text)e.getSource());
            }
        });
        theB.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                setVerifiedRGB((Text)e.getSource());
            }
        });
    }
    
    /**
     * Sets the RGB and 
     * @param color The colour to set
     * @since 2.4.4
     */
    public void setColours(Color color){
        if(color==null){
            color = new Color(Display.getCurrent(),0,0,0);
        }
        setRGB(color.getRGB());
        setHex(SWTUtils.convertRGBToHex(color.getRGB(),false));
    }
    
    /**
     * Sets the three Text boxes to the given RGB.
     * @param rgb The RGB
     * @since 2.4.4
     */
    public void setRGB(RGB rgb){
        theR.setText(Integer.toString(rgb.red));
        theG.setText(Integer.toString(rgb.green));
        theB.setText(Integer.toString(rgb.blue));            
    }
    
    /**
     * Sets the hex.
     * @param hex The hex 
     * @since 2.4.4
     */
    public void setHex(String hex){
        theHex.setText(hex);
    }
    
    /**
     * Calculates a hex from the RGB and sets it.
     * @param rgb The RGB to set
     * @since 2.4.4
     */
    public void setHex(RGB rgb){
        theHex.setText(SWTUtils.convertRGBToHex(rgb,false));
    }
    
    /**
     * Gets an RGB from the text box values.
     * 
     * @return The RGB 
     * @since 2.4.4
     */
    public RGB getRGB(){
        int r = 0;
        int g = 0;
        int b = 0;
        
        try{
            r = Integer.parseInt(theR.getText());
        }catch(NumberFormatException e){}
        try{
            g = Integer.parseInt(theG.getText());
        }catch(NumberFormatException e){}
        try{
            b = Integer.parseInt(theB.getText());
        }catch(NumberFormatException e){}
        
        return new RGB(r,g,b);
    }
    
    /**
     * Takes a Text and verifies its value to a valid RGB value.
     * @param text The textbox to verify
     * @since 2.4.4
     */
    public void setVerifiedRGB(Text text){
        int MAX = 255;
        int MIN = 0;
        
        try{
            int value = Integer.parseInt(text.getText());
            if(value > MAX){
                text.setText(Integer.toString(MAX));
            }
            else if(value < MIN){
                text.setText(Integer.toString(MIN));
            }
            else{
                text.setText(value+"");
            }
        }
        catch(NumberFormatException e){
            text.setText(Integer.toString(MIN));
        }
        setHex(getRGB());
    }
    
    /**
     * Sets the labels on this widget to the given foreground colour.
     * @param color The foreground Color to set
     * @since 2.4.4
     */
    public void setLabelForegroundColor(Color color){
        _lblB.setForeground(color);
        _lblG.setForeground(color);
        _lblR.setForeground(color);
        _lblHash.setForeground(color);
    }
    
    /**
     * A listener for modifications on a number text control that will update hex
     * @author kporter - created Sep 9, 2013
     * @since 2.4.4
     */
    public class RGBModifyListener implements KeyListener{

        /**
         * @see org.eclipse.swt.events.KeyListener#keyPressed(org.eclipse.swt.events.KeyEvent)
         */
        @Override
        public void keyPressed(KeyEvent e) {

        }

        /**
         * @see org.eclipse.swt.events.KeyListener#keyReleased(org.eclipse.swt.events.KeyEvent)
         */
        @Override
        public void keyReleased(KeyEvent e) {
            Text text = (Text)e.getSource();
            if(!text.getText().isEmpty()){
                int value = Integer.parseInt(text.getText());
                if(value>=0 && value <= 255){ 
                    setHex(getRGB());
                    notifyListeners(getRGB());
                }
            }  
        }
    }
    
    /**
     * Call this to notify listeners of a change
     * @param rgb the current RGB
     * @since 2.4.4
     */
    public void notifyListeners(RGB rgb){
        for(RGBListener listener: listeners){
            listener.colourChange(rgb);
        }
    }
    
    /**
     * Add a RGBListener to this widget.
     * 
     * @param listener The RGBListener to add
     * @since 2.4.4
     */
    public void addRGBListener(RGBListener listener){
        listeners.add(listener);
    }
    
    /**
     * Implement this listener to be notified of RGB changes.
     * @author kporter - created Sep 11, 2013
     * @since 2.4.4
     */
    public interface RGBListener{

        /**
         * Is called when a RGB changes value.
         * @param rgb The new RGB value
         * @since 2.4.4
         */
        public void colourChange(RGB rgb);
    }
    
    /**
     * Launch the application.
     * @param args
     * @docfacto.adam ignore
     */
    public static void main(String args[]) {
        try {
            Display display = Display.getDefault();
            Shell shell = new Shell(display);
            RGBComposite c = new RGBComposite(shell,SWT.NONE);
            c.setColours(SWTUtils.getColor(255,55,55));
            c.setBounds(10,10,300,300);
            shell.open();
            shell.layout();
            while (!shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                    display.sleep();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
