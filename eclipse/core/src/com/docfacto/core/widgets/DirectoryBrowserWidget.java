package com.docfacto.core.widgets;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;

/**
 * Widget to display a label, text and a browse button for a file dialog.
 * 
 * A listener can be registered so that events can be acted upon.
 * 
 * @author dhudson - created 18 Jul 2013
 * @since 2.4
 */
public class DirectoryBrowserWidget extends AbstractBrowserWidget {
    private DirectoryDialog theExternalDialog;
    private ContainerSelectionDialog theInternalDialog;

    /**
     * Constructs a new FileBrowserWidget that opens the FileDialog at the default base path, and has no extension
     * filter.
     * 
     * @param parent The parent composite
     * @param style Addition style
     * @param labelText The label
     * @since 2.4
     */
    public DirectoryBrowserWidget(Composite parent,int style,String labelText) {
        super(parent, style, labelText);
        theInternalDialog = new ContainerSelectionDialog(getShell(), null, false, labelText);
        theExternalDialog = new DirectoryDialog(getShell());
    }

    /**
     * Constructs a new FileBrowserWidget, setting a base path on the FileDialog.
     * 
     * @param parent The parent composite
     * @param style Addition style
     * @param labelText The label
     * @param basePath The base path to open when showing the FileDialog
     * @since 2.4
     */
    public DirectoryBrowserWidget(Composite parent,int style,String labelText,String basePath) {
        this(parent, style, labelText);
        theExternalDialog.setFilterPath(basePath);
    }

    @Override
    public MouseListener getExternalMouseListener() {
        return new MouseAdapter() {
            @Override
            public void mouseDown(MouseEvent e) {
                String path = theExternalDialog.open();
                if (path!=null) {
                    theTextBox.setText(path);
                    setBasePath(path, null);
                    // Fire change event
                    fireChangeEvent();
                }
            }
        };
    }

    @Override
    public MouseListener getInternalMouseListener() {
        return new MouseAdapter() {
            @Override
            public void mouseDown(MouseEvent e) {
                if (theInternalDialog.open()==Dialog.OK) {
                    if (theInternalDialog.getResult().length!=0) {
                        IPath path = (IPath)theInternalDialog.getResult()[0];
                        theTextBox.setText(path.toString());
                        IResource res = ResourcesPlugin.getWorkspace().getRoot().findMember(path);
                        theFullPath = res.getLocation().toString();
                        setBasePath(theFullPath, path);
                        fireChangeEvent();
                    }
                }
            }
        };
    }

    /**
     * Set a base path so that the next time this is opened, it opens to that path.
     * 
     * @param base The base path to the file selected
     * @param internal An IPath to an internal file, can be null
     * @since 2.5
     */
    private void setBasePath(String base,IPath internal) {
        theExternalDialog.setFilterPath(base);
        if (internal!=null) {
            theInternalDialog.setInitialSelections(new IPath[] {internal});
        }
    }
}
