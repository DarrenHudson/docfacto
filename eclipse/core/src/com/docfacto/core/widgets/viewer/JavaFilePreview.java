package com.docfacto.core.widgets.viewer;

import java.util.Hashtable;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.internal.ui.IJavaStatusConstants;
import org.eclipse.jdt.internal.ui.JavaPlugin;
import org.eclipse.jdt.internal.ui.preferences.formatter.JavaPreview;
import org.eclipse.jdt.internal.ui.text.java.JavaFormattingContext;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.formatter.FormattingContextProperties;
import org.eclipse.jface.text.formatter.IContentFormatter;
import org.eclipse.jface.text.formatter.IContentFormatterExtension;
import org.eclipse.jface.text.formatter.IFormattingContext;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;

/**
 * A java preview.
 * 
 * @author kporter - created Nov 8, 2013
 * @since 2.4.8
 */
@SuppressWarnings("restriction")
public class JavaFilePreview extends JavaPreview implements IPreviewer {
    private final StyledText theText;
    private String theContent = null;
    private Color theHighlightColor = null;

    /**
     * Constructor.
     * 
     * @param parent parent
     */
    @SuppressWarnings("unchecked")
    public JavaFilePreview(Composite parent) {
        super(new Hashtable<String,String>(), parent);

        System.out.println(JavaCore.getDefaultOptions().toString());
        theText = fSourceViewer.getTextWidget();
    }

    /**
     * Constructor.
     * 
     * @param parent parent
     * @param highlightColor colour to use as highlight
     */
    public JavaFilePreview(Composite parent,Color highlightColor) {
        this(parent);
        theHighlightColor = highlightColor;
    }

    /**
     * Set Highlight colour
     * 
     * @param highlight
     * @since 2.5
     */
    public void setHighlightColor(Color highlight) {
        theHighlightColor = highlight;
    }

    public void setContent(String content) {
        theContent = content;
    }

    public StyledText getText() {
        return theText;
    }

    /**
     * Get the current position of the caret as a line number.
     * 
     * @return the position of the caret as a line number
     * @since 2.4.8
     */
    public int getLine() {
        return theText.getLineAtOffset(theText.getCaretOffset());
    }

    /**
     * Set the viewer position to the line
     * 
     * @param lineNumber to set the viewer to
     * @since 2.5
     */
    public void setLineNumber(int lineNumber) {
        theText.setSelection(theText.getOffsetAtLine(lineNumber-1));
    }

    public void format(String content) {
        theContent = content;
        doFormatPreview();
    }

    public void format() {
        doFormatPreview();
    }

    /**
     * @see com.docfacto.core.widgets.viewer.IPreviewer#getViewer()
     */
    @Override
    public SourceViewer getViewer() {
        return fSourceViewer;
    }

    /**
     * @see org.eclipse.jdt.internal.ui.preferences.formatter.JavaPreview#doFormatPreview()
     */
    @Override
    protected void doFormatPreview() {
        if (theContent==null) {
            fPreviewDocument.set(""); //$NON-NLS-1$
            return;
        }

        if (theHighlightColor!=null) {
            theText.setLineBackground(0, 1, theHighlightColor);
        }

        fPreviewDocument.set(theContent);

        fSourceViewer.setRedraw(false);
        final IFormattingContext context = new JavaFormattingContext();
        try {
            final IContentFormatter formatter = fViewerConfiguration.getContentFormatter(fSourceViewer);
            if (formatter instanceof IContentFormatterExtension) {
                final IContentFormatterExtension extension = (IContentFormatterExtension)formatter;
                context.setProperty(FormattingContextProperties.CONTEXT_PREFERENCES, fWorkingValues);
                context.setProperty(FormattingContextProperties.CONTEXT_DOCUMENT, Boolean.valueOf(true));
//                extension.format(fPreviewDocument, context);
            }
            else
                formatter.format(fPreviewDocument, new Region(0, fPreviewDocument.getLength()));
        }
        catch (Exception e) {
            final IStatus status =
                new Status(IStatus.ERROR, JavaPlugin.getPluginId(), IJavaStatusConstants.INTERNAL_ERROR, "Error", e);
            JavaPlugin.log(status);
        }
        finally {
            context.dispose();
            fSourceViewer.setRedraw(true);
        }
    }
}
