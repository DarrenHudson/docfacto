package com.docfacto.core.widgets.viewer;

import org.eclipse.swt.custom.CaretEvent;
import org.eclipse.swt.custom.CaretListener;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;

public class LineHighlighter implements CaretListener {

    Color theColor;

    public LineHighlighter(Color color) {
        theColor = color;
    }

    /**
     * @see org.eclipse.swt.custom.CaretListener#caretMoved(org.eclipse.swt.custom.CaretEvent)
     */
    @Override
    public void caretMoved(CaretEvent event) {
        StyledText theText = (StyledText)event.getSource();
        int theLine = theText.getLineAtOffset(event.caretOffset);
        theText.setLineBackground(0,theText.getLineCount(),null);
        theText.setLineBackground(theLine,1,theColor);
        theText.redraw();
    }

}
