package com.docfacto.core.widgets.viewer;

import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.swt.custom.StyledText;

public interface IPreviewer {

    public SourceViewer getViewer();
    public StyledText getText();
    public int getLine();
    
    public void setContent(String content);
    
    public void format();
    
    public void format(String content);
    
}
