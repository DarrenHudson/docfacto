package com.docfacto.core.widgets.viewer;

import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;

import com.docfacto.core.editors.xmleditor.XMLConfiguration;

/**
 * XML viewer
 *
 * @author dhudson - created 26 Nov 2013
 * @since 2.5
 */
public class SimpleXmlViewer implements IPreviewer {
    
    private SourceViewer theViewer;
    private IDocument theDocument;
    private String theContent= "";
    private StyledText theText;
    private Color theHighlightColor;
    
    
    public SimpleXmlViewer(Composite parent){
        theViewer = new SourceViewer(parent,null,SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
        XMLConfiguration config = new XMLConfiguration();
        
        theViewer.configure(config);
        theViewer.setEditable(false);
        
        theDocument = new Document();
        
        theViewer.setDocument(theDocument);
        
        theText = theViewer.getTextWidget();
        
        theHighlightColor = null;
        
    }

    /**
     * @see com.docfacto.core.widgets.viewer.IPreviewer#getViewer()
     */
    @Override
    public SourceViewer getViewer() {
        return theViewer;
    }

    /**
     * @see com.docfacto.core.widgets.viewer.IPreviewer#getText()
     */
    @Override
    public StyledText getText() {
        return theText;
    }

    /**
     * Set the viewer position to the line
     * 
     * @param lineNumber to set the viewer to
     * @since 2.5
     */
    public void setLineNumber(int lineNumber) {
        theText.setSelection(theText.getOffsetAtLine(lineNumber-1));
    }
    
    /**
     * @see com.docfacto.core.widgets.viewer.IPreviewer#getLine()
     */
    @Override
    public int getLine() {
        return theText.getLineAtOffset(theText.getCaretOffset());
    }

    /**
     * @see com.docfacto.core.widgets.viewer.IPreviewer#setContent(java.lang.String)
     */
    @Override
    public void setContent(String content) {
        theContent=content;
    }

    /**
     * @see com.docfacto.core.widgets.viewer.IPreviewer#format()
     */
    @Override
    public void format() {
        if(theContent==null){
            theDocument.set("");
            return;
        }
        
        theDocument.set(theContent);
        theViewer.setRedraw(true);
    }

    /**
     * @see com.docfacto.core.widgets.viewer.IPreviewer#format(java.lang.String)
     */
    @Override
    public void format(String content) {
        if(theHighlightColor!=null){
            theText.setLineBackground(0,1,theHighlightColor);
        }
        
        theDocument.set(content);
        theViewer.setRedraw(true);
    }

    public void setVisible(boolean visible) {
        theViewer.setVisibleRegion(0,theDocument.getLength());
    }
    
    
    /**
     * Set the colour if this viewer highlights lines.
     * @since 2.4.8
     */
    public void setHighlightColor(Color color){
        theHighlightColor = color;
    }
}
