package com.docfacto.core.widgets;

import java.awt.AWTException;
import java.awt.Robot;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.core.swt.DefaultMouseTrackListener;
import com.docfacto.core.utils.ImageUtils;
import com.docfacto.core.utils.SWTUtils;
import com.docfacto.core.widgets.RGBComposite.RGBListener;

/**
 * A colour picker widget.
 * 
 * Use this widget to allow users to pick colours. {@docfacto.media uri="doc-files/Colour-Picker.svg"}
 * 
 * @author kporter
 * @docfacto.link key="colourPicker" uri="${doc-beermat}/controlbar/c_stroke_widget.dita" link-to="doc"
 */
public abstract class AbstractColorPicker implements RGBListener {

    public static final int[] DEFAULT_COLOURS = {
        SWT.COLOR_WHITE,SWT.COLOR_RED,SWT.COLOR_MAGENTA,
        SWT.COLOR_BLUE,SWT.COLOR_CYAN,SWT.COLOR_GREEN,
        SWT.COLOR_YELLOW,SWT.COLOR_GRAY,SWT.COLOR_BLACK};

    private Composite theComposite;
    private Shell theShell;
    private Label theColourLabel;
    private Composite theParent;

    private boolean theHasNoColour = true;

    /**
     * Used to hold the original colour in case a user closes the shell
     * 
     * @docfacto.link uri="${doc-beermat}/controlbar/c_stroke_widget.dita" key="original-colour" version="2.4.2"
     */
    private Color theOriginalColour;

    private RGBComposite theRGB;

    /**
     * Create the composite for this widget and lay it out.
     * 
     * @param parent The parent to create this in
     * @return The created Composite
     * @since 2.4.6
     */
    protected abstract Composite createCompositePart(Composite parent);

    /**
     * Create the label for the colour picker
     * 
     * @param parent The parent composite to create this in
     * @return The created Label
     * @since 2.4.6
     */
    protected abstract Label createColourLabel(Composite parent);

    /**
     * Set the colour chart visible if true.
     * 
     * @param vis true to show
     * @since 2.4
     */
    protected abstract void setVisible(boolean visible);

    /**
     * Sets the colour label to none. Define the behaviour for a null colour.
     * 
     * @since 2.4.6
     */
    protected abstract void setColorLabelToNone();

    /**
     * Sets a colour to the colour label. {@docfacto.note Colour must not be null}
     * 
     * @param color The colour to set
     * @since 2.4.6
     */
    protected abstract void setColourLabelToColour(Color color);

    /**
     * Get the foreground colour for the widget to use
     * 
     * @return The foreground Color
     * @since 2.4.6
     */
    public abstract Color getForegroundColour();

    /**
     * Get the background colour for the widget to use
     * 
     * @return The background Color
     * @since 2.4.6
     */
    public abstract Color getBackgroundColour();

    /**
     * Default constructor, none is not shown.
     * 
     * @param parent
     */
    public AbstractColorPicker(Composite parent) {
        this(parent, false);
    }

    /**
     * Constructor, given a boolean shows 'none'
     */
    public AbstractColorPicker(Composite parent,boolean noColorAllowed) {
        theComposite = createCompositePart(parent);
        theColourLabel = createColourLabel(theComposite);

        setColourLabel(null);

        theOriginalColour = null;

        theShell = new Shell(parent.getDisplay(), SWT.ON_TOP);
        theShell.setLayout(new FillLayout());

        theParent = new Composite(theShell, SWT.NONE);
        theParent.setBackground(getBackgroundColour());
        theParent.setLayout(new RowLayout(SWT.HORIZONTAL));

        final Label colorArray = new Label(theParent, SWT.NONE);
        colorArray.setImage(ImageUtils.getPluginImage(DocfactoCorePlugin.PLUGIN_ID, "icons/rgb.png"));
        addColourSelectorListeners(colorArray);

        new ColourStrip(theParent, SWT.NONE, noColorAllowed);

        theRGB = new RGBComposite(theParent, SWT.NONE);
        theRGB.setLayoutData(new RowData(SWT.DEFAULT, SWT.DEFAULT));
        theRGB.setBackground(getBackgroundColour());
        theRGB.setLabelForegroundColor(getForegroundColour());
        theRGB.addRGBListener(this);

        // If the label is clicked again
        theColourLabel.addMouseListener(new MouseAdapter() {
            /**
             * @see org.eclipse.swt.events.MouseAdapter#mouseDown(org.eclipse.swt.events.MouseEvent)
             */
            @Override
            public void mouseDown(MouseEvent e) {
                if (!getVisible()) {
                    setVisible(true);
                    theParent.forceFocus();
                }
                else {
                    setColourLabel(theOriginalColour);
                    setVisible(false);
                }
            }
        });

        theShell.setVisible(false);
        // setVisible(false);

        SWTUtils.addKeyListenerToChildren(theParent, new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.character==SWT.ESC) {
                    setColourLabel(theOriginalColour);
                    theShell.setVisible(false);
                }
            }
        });
    }

    /**
     * The RGB of the current mouse position
     * 
     * @return the RGB of the current mouse position
     * @since 2.4
     */
    private RGB getCurrentColour() {
        try {
            Robot robot = new Robot();
            Point pos = Display.getCurrent().getCursorLocation();
            java.awt.Color color = robot.getPixelColor(pos.x, pos.y);
            return new RGB(color.getRed(), color.getGreen(), color.getBlue());
        }
        catch (AWTException ex) {
            return null;
        }
    }

    /**
     * Set the colour
     * 
     * @param color to set
     * @param setRGB true if the rgb is to be set
     * @since 2.4
     */
    public void setColour(Color color,boolean setRGB) {
        setColourLabel(color);
        theOriginalColour = color;
        setHasNoColour(false);

        if (setRGB) {
            theRGB.setColours(color);
        }
    }

    /**
     * Helper method to set the background of the colour label {@docfacto.note Can take a colour or null, the relevant
     * method is called}
     * 
     * @param color to set the background, or null
     * @since 2.4
     */
    protected void setColourLabel(Color color) {
        if (color==null) {
            // None set..
            setColorLabelToNone();
            theHasNoColour = true;
        }
        else {
            // Setting the image to null, trashes the size info
            setColourLabelToColour(color);
        }
    }

    /**
     * Return the selected colour.
     * 
     * This will return null, if no colour selected
     * 
     * @return the colour in SVG format
     * @since 2.4
     */
    public String getCurrentSelectedColour() {
        if (!getHasNoColour()) {
            Color colour = theColourLabel.getBackground();
            return SWTUtils.convertRGBToHex(colour.getRGB(), true);
        }
        return null;
    }

    /**
     * @see com.docfacto.beermat.ui.widgets.RGBComposite.RGBListener#colourChange(org.eclipse.swt.graphics.RGB)
     */
    @Override
    public void colourChange(RGB rgb) {
        setColour(new Color(Display.getCurrent(), rgb), false);
    }

    /**
     * Set whether this widget has no colour.
     * 
     * @param noColour Set whether no colour is set
     * @since 2.4.6
     */
    public void setHasNoColour(boolean noColour) {
        theHasNoColour = noColour;
    }

    /**
     * Gets whether no colour is set
     * 
     * @return true if there is no colour set
     * @since 2.4.6
     */
    public boolean getHasNoColour() {
        return theHasNoColour;
    }

    /**
     * Check to see if the shell is visible
     * 
     * @return true if visible
     * @since 2.4
     */
    public boolean getVisible() {
        return theShell.getVisible();
    }

    /**
     * Gets the composite from this widget.
     * 
     * @return The composite held
     * @since 2.4.3
     */
    public Composite getComposite() {
        return theComposite;
    }

    /**
     * Gets the shell
     * 
     * @return This widgets shell
     * @since 2.4.5
     */
    protected Shell getShell() {
        return theShell;
    }

    /**
     * Returns theOriginalColour.
     * 
     * @return the theOriginalColour
     */
    public Color getOriginalColour() {
        return theOriginalColour;
    }

    /**
     * Sets theOriginalColour.
     * 
     * @param theOriginalColour the theOriginalColour value
     */
    public void setOriginalColour(Color originalColour) {
        theOriginalColour = originalColour;
    }

    /**
     * Gets the colour label.
     * 
     * @return the colour label
     * @since 2.4.6
     */
    public Label getColorLabel() {
        return theColourLabel;
    }

    /**
     * Add the listeners to the colour selector label
     * 
     * @param colourSelector
     * @since 2.4
     */
    private void addColourSelectorListeners(Label colourSelector) {
        colourSelector.addMouseListener(new MouseAdapter() {
            /**
             * @see org.eclipse.swt.events.MouseAdapter#mouseDown(org.eclipse.swt.events.MouseEvent)
             */
            @Override
            public void mouseDown(MouseEvent e) {
                RGB colour = getCurrentColour();
                setColourLabel(SWTUtils.getColor(colour));
                setHasNoColour(false);
                setVisible(false);
            }
        });

        colourSelector.addMouseMoveListener(new MouseMoveListener() {
            /**
             * @see org.eclipse.swt.events.MouseMoveListener#mouseMove(org.eclipse.swt.events.MouseEvent)
             */
            @Override
            public void mouseMove(MouseEvent e) {
                Color color = SWTUtils.getColor(getCurrentColour());
                setColourLabel(color);
                theRGB.setColours(color);
            }
        });
    }

    /**
     * Helper method to create the close widget
     * 
     * @since 2.4
     */
    private void createCloseLabel(Composite parent) {
        final Label close = new Label(parent, SWT.NONE);
        close.setLayoutData(new RowData(15, 15));
        close.setToolTipText("Close / Leave unchanged");
        close.setImage(ImageUtils.getPluginImage(DocfactoCorePlugin.PLUGIN_ID, "icons/progress_rem.gif"));
        close.addMouseListener(new MouseAdapter() {
            /**
             * @see org.eclipse.swt.events.MouseAdapter#mouseDown(org.eclipse.swt.events.MouseEvent)
             */
            public void mouseDown(MouseEvent e) {
                setColourLabel(theOriginalColour);
                setVisible(false);
            }
        });

        close.addMouseTrackListener(new DefaultMouseTrackListener() {
            /**
             * @see com.docfacto.core.swt.DefaultMouseTrackListener#mouseEnter(org.eclipse.swt.events.MouseEvent)
             */
            @Override
            public void mouseEnter(MouseEvent event) {
                setColourLabel(theOriginalColour);
            }
        });
    }

    /**
     * Helper method to create a 'none' widget
     * 
     * @since 2.4
     * @docfacto.link key="noColourFillWidget" uri="${doc-beermat}/controlbar/c_fill_widget.dita" link-to="doc"
     */
    private void createNoneLabel(Composite parent) {
        final Label none = new Label(parent, SWT.NONE);
        none.setBackground(SWTUtils.getColor(SWT.COLOR_WHITE));
        none.setToolTipText("None");

        none.setImage(ImageUtils.getPluginImage(DocfactoCorePlugin.PLUGIN_ID, "icons/close_view.gif"));
        none.addMouseListener(new MouseAdapter() {
            /**
             * @see org.eclipse.swt.events.MouseAdapter#mouseDown(org.eclipse.swt.events.MouseEvent)
             */
            public void mouseDown(MouseEvent e) {
                // Set it to none
                setColourLabel(null);
                setVisible(false);
            }
        });

        none.addMouseTrackListener(new DefaultMouseTrackListener() {
            /**
             * @see com.docfacto.core.swt.DefaultMouseTrackListener#mouseEnter(org.eclipse.swt.events.MouseEvent)
             */
            @Override
            public void mouseEnter(MouseEvent event) {
                setColourLabel(null);
            }
        });
    }

    /**
     * A Label which the background colour is the colour which will be selected.
     * 
     * @author dhudson - created 18 Jun 2013
     * @since 2.4
     */
    private class SimpleColourLabel {

        private static final int WIDTH = 15;
        private static final int HEIGHT = 15;

        // Subclassing not allowed, SWT thing..
        private final Label theLabel;

        /**
         * Constructor.
         * 
         * @param colour which the label represents
         */
        public SimpleColourLabel(Composite parent,final Color colour) {
            theLabel = new Label(parent, SWT.NONE);
            theLabel.setBackground(colour);
            theLabel.setSize(WIDTH, HEIGHT);
            theLabel.setLayoutData(new RowData(WIDTH, HEIGHT));

            theLabel.addMouseListener(new MouseAdapter() {
                /**
                 * @see org.eclipse.swt.events.MouseAdapter#mouseDown(org.eclipse.swt.events.MouseEvent)
                 */
                public void mouseDown(MouseEvent e) {
                    // Set the color selected label
                    setColourLabel(colour);
                    setHasNoColour(false);
                    // Close the display
                    setVisible(false);
                }
            });

            theLabel.addMouseTrackListener(new DefaultMouseTrackListener() {
                /**
                 * @see com.docfacto.core.swt.DefaultMouseTrackListener#mouseEnter(org.eclipse.swt.events.MouseEvent)
                 */
                @Override
                public void mouseEnter(MouseEvent event) {
                    setColourLabel(colour);
                    theRGB.setColours(colour);
                }
            });
        }
    }

    /**
     * A Composite holding a strip of the default colours Using a vertical rowlayout all the colour labels are added to
     * this. {@docfacto.media uri="doc-files/Colour-Picker.svg"}
     * 
     * @author kporter - created Sep 6, 2013
     * @since 2.4.4
     */
    private class ColourStrip extends Composite {
        public ColourStrip(Composite parent,int style,boolean showNone) {
            super(parent, style);
            setBackground(getBackgroundColour());

            RowLayout layout = new RowLayout(SWT.VERTICAL);
            layout.marginHeight = 1;
            layout.marginWidth = 1;
            layout.fill = true;
            layout.pack = false;
            layout.wrap = false;
            layout.spacing = 2;
            setLayout(layout);

            createCloseLabel(this);

            if (showNone) {
                createNoneLabel(this);
            }
            addDefaultColours(this);
        }

        /**
         * Utility method to create a column of simple colours
         * 
         * @since 2.4
         */
        private void addDefaultColours(Composite parent) {
            for (int color:DEFAULT_COLOURS) {
                new SimpleColourLabel(parent, SWTUtils.getColor(color));
            }
        }
    }

}