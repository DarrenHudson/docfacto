package com.docfacto.adam.preferences;


import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import com.docfacto.adam.plugin.EclipseAdamUtils;
import com.docfacto.config.generated.Severity;

/**
 * @author pemi
 * @since 2.1
 * 
 */
public class TagDialog extends Dialog {

	/**
	 * @wbp.nonvisual location=142,191
	 */
	private final AdamTag adamTag;
	private Button enableButton;
	private Text nameText;
	private Combo severityCombo;
	private Button button;
	private boolean edit = false;
	protected boolean severitySelected = false;
	protected boolean nameSelected = false;

	/**
	 * Constructor initializes values
	 * 
	 * Create the dialog.
	 * 
	 * @param parentShell
	 * @param scopeItems
	 * @param tags
	 * @since 2.1
	 */
	public TagDialog(Shell parentShell, AdamTag aTag,boolean editing) {
		super(parentShell);
		adamTag = aTag;
		edit = editing;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		if(edit){
			getShell().setText("Updating Tag: "+adamTag.getName());
		}else{
			getShell().setText("New tag in "+adamTag.getScope());
		}

		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(2, false));

		// create a 'Enable' check box
		new Label(container, SWT.NONE).setText("Enable:");

		enableButton = new Button(container, SWT.CHECK);
		enableButton.setSelection(true);
		enableButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false));

		// create 'Name' Field
		new Label(container, SWT.NONE).setText("Name:");

		nameText = new Text(container, SWT.BORDER | SWT.SINGLE);

		// enable OK button if all three fields are filled
		nameText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				if (nameText.getText().trim().length() > 0) {
					nameSelected = true;
					if (nameSelected && severitySelected) {
						button.setEnabled(true);
					}

				} else {
					nameSelected = false;
				}
				button.setEnabled(nameSelected && severitySelected);
			}
		});
		nameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		
		
		
		
		// Create 'Severity' field
		new Label(container, SWT.NONE).setText("Severity:");
		severityCombo = new Combo(container, SWT.READ_ONLY);
		if (edit) {
			severitySelected = true;
		}

		// enable OK button if all three fields are filled
		severityCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				severitySelected = true;
				button.setEnabled(nameSelected && severitySelected);
			}
		});
		
		// get combo box texts from store
		String[] severityComboItems = EclipseAdamUtils.SEVERITIES;
		severityCombo.setItems(severityComboItems);
		severityCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false));
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		return container;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse
	 * .swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		button = createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);

		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
		initDataBindings();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#getInitialSize()
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(289, 202);
	}

	/**
	 * @return <code>AdamTag</code> to be displayed
	 */
	public AdamTag getDialogTag() {
		AdamTag aTag = adamTag;
		return aTag;
	}

	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		IObservableValue observeTextNameTextObserveWidget = WidgetProperties
				.text(SWT.Modify).observe(nameText);
		IObservableValue nameAdamTagObserveValue = BeanProperties.value("name")
				.observe(adamTag);
		bindingContext.bindValue(observeTextNameTextObserveWidget,
				nameAdamTagObserveValue, null, null);
		//
		IObservableValue observeTextSeverityComboObserveWidget = WidgetProperties
				.text().observe(severityCombo);
		IObservableValue severityAdamTagObserveValue = BeanProperties.value(
				"severity").observe(adamTag);
		bindingContext.bindValue(observeTextSeverityComboObserveWidget,
				severityAdamTagObserveValue, null, null);
		//
		IObservableValue observeSelectionEnableButtonObserveWidget = WidgetProperties
				.selection().observe(enableButton);
		IObservableValue enableAdamTagObserveValue = BeanProperties.value(
				"enable").observe(adamTag);
		bindingContext.bindValue(observeSelectionEnableButtonObserveWidget,
				enableAdamTagObserveValue, null, null);
		//
		return bindingContext;
	}

}
