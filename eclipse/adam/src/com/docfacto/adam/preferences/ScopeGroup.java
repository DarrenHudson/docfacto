package com.docfacto.adam.preferences;

import java.util.ArrayList;
import java.util.List;

/**
 * A model to hold <code>AdamTag</code>s
 * @author pemi
 * @since 2.2
 *
 */
public class ScopeGroup extends AbstractModelObject{
	
	private final List<AdamTag> m_tags = new ArrayList<AdamTag>();
	private String m_scopeName;
	


	/**
	 * Default constructor
	 * 
	 */
	public ScopeGroup() {
        
    }
	
	/**
	 * @param name group name
	 */
	public ScopeGroup(String name) {
		m_scopeName = name;
	}

	/**
	 * @return name of scope
	 */
	public String getName() {
		return m_scopeName;
	}

	/**
	 * Sets the scope name
	 * @param name name of scope
	 */
	public void setName(String name) {
		String oldValue = m_scopeName;
		m_scopeName = name;
		firePropertyChange("scope", oldValue, m_scopeName);
	}

	/**
	 * Adds specified tag to scope
	 * @param aTag tag to add
	 */
	public void addAdamTag(AdamTag aTag) {
		String aTagID = aTag.getName()+aTag.getScope();
		boolean duplicate = false;
		
		for(AdamTag tag : m_tags){
			if(aTagID.equals(tag.getName()+tag.getScope())){
				duplicate = true;
			}
	
		}
		if(!duplicate){
			m_tags.add(aTag);
			firePropertyChange("tags", null, m_tags);
		}
		
		
	}

	/**
	 * Removes specified tag from scope
	 * @param aTag name of tag to remove'
	 */
	public void removeAdamTag(AdamTag aTag) {
		m_tags.remove(aTag);
		firePropertyChange("tags", null, m_tags);
	}

	/**
	 * Gets tags in xml
	 * @return a list of <code>AdamTag</code>s
	 */
	public List<AdamTag> getAdamTags() {
		return m_tags;
	}
	
	
	

}
