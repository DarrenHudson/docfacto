package com.docfacto.adam.preferences;


/**
 * This class provides some constants used in plug-in.

 * @author pemi
 * @since 2.1.1
 */
public class AdamStoreUtil {
	
	public static final String MISSING_DESCRIPTION = "Required tag description missing";
	public static final String TAGCOUNT = "tagCount";
	public static final String TAG = "tag";
	//public static final String CORE_AVAILABLE = "core_xml_available";
	public static final String [] A_TAG = {"Name","Scope","Severity","Enable"};
	public static final String SCOPE_COUNT = "ScopeCount";
	public static final String JAVADOC_CHECK = "Javadoc_check";
	//public static final String SCOPE_NAME = "ScopeName";
	public static final String[] SCOPE_ARRAY = new String[]{"packages","classes","methods","constructors","fields", "enums","interfaces"};
	public static final int NAME = 0;
	public static final int SCOPE = 1;
	public static final int SEVERITY = 2;
	public static final int ENABLE = 3;
	public static final int PACKAGES = 0;
	public static final int CLASSES = 1;
	public static final int METHODS = 2;
	public static final int CONSTRUCTORS = 3;
	public static final int FIELDS = 4;
	public static final int ENUMS = 5;
	public static final int INTERFACES = 6;
	
	
}
