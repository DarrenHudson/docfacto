package com.docfacto.adam.preferences;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.docfacto.adam.plugin.EclipseAdamUtils;

/**
 * A dialog UI dedicated to getting/setting textual warnings.
 * <p>
 * The dialog has two fields, the first field is a text field whose label
 * can be set by calling #setKey(). The second field is a read-only combo
 * for displaying the severity values (i.e warning, error, info). 
 * 
 * Usage example: Adam preference page, for setting the following in config file. * 
 * <doc severity="error">package.html</doc>
 * 
 * where "package.html" is the #getKey() 
 * and "error" is the severity.
 * 
 * </p>
 * @author pemi - created Mar 5, 2013 10:51:48 AM
 *
 * @since 2.2
 */
public class CmntWarnDialog extends Dialog {
    
	private Text keyWord;
	WarningBean warning;
	private Combo combo;
	private String title = "Add comment keyword";
	private String key = "Keyword";
	

    /**
     * Creates a new dialog with a given {@code Shell} and  {@code WaringBean} 
     * @param parentShell - the parent shell
     * @param warningItem - WarningBean to be used for data binding
     * @since 2.2
     */
	protected CmntWarnDialog(Shell parentShell, WarningBean warningItem) {
		super(parentShell);
		warning = warningItem;


	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		getShell().setText(title);
		
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FormLayout());
		
		Label lblKeyword = new Label(container, SWT.NONE);
		FormData fd_lblKeyword = new FormData();
		fd_lblKeyword.top = new FormAttachment(0, 13);
		fd_lblKeyword.left = new FormAttachment(0, 11);
		lblKeyword.setLayoutData(fd_lblKeyword);
		lblKeyword.setText(key);
		
		keyWord = new Text(container, SWT.BORDER);
		FormData fd_text = new FormData();
		fd_text.right = new FormAttachment(100, -10);
		fd_text.top = new FormAttachment(0, 11);
		fd_text.left = new FormAttachment(0, 67);
		keyWord.setLayoutData(fd_text);
		
		Label lblSeverity = new Label(container, SWT.NONE);
		FormData fd_lblSeverity = new FormData();
		fd_lblSeverity.bottom = new FormAttachment(0, 58);
		fd_lblSeverity.right = new FormAttachment(0, 61);
		fd_lblSeverity.top = new FormAttachment(0, 37);
		fd_lblSeverity.left = new FormAttachment(0, 11);
		lblSeverity.setLayoutData(fd_lblSeverity);
		lblSeverity.setText("Severity");
		
		combo = new Combo(container, SWT.READ_ONLY);
		FormData fd_combo = new FormData();
		fd_combo.top = new FormAttachment(0, 37);
		fd_combo.left = new FormAttachment(0, 67);
		fd_combo.right = new FormAttachment(100, -10);
		combo.setLayoutData(fd_combo);
		combo.setItems(EclipseAdamUtils.SEVERITIES);
		initDataBindings();
		
		
		return container;
		
	}
	
    /**
     * Method is run during the initialization of data binding objects
     * @return the {@code DataBindingContext}
     * @since 2.2
     */
	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		IObservableValue observeTextKeyWordObserveWidget = WidgetProperties.text(SWT.Modify).observe(keyWord);
		IObservableValue warningNameWarningObserveValue = BeanProperties.value("warningName").observe(warning);
		bindingContext.bindValue(observeTextKeyWordObserveWidget, warningNameWarningObserveValue, null, null);
		//
		IObservableValue observeSelectionComboObserveWidget = WidgetProperties.selection().observe(combo);
		IObservableValue warningSeverityWarningObserveValue = BeanProperties.value("warningSeverity").observe(warning);
		bindingContext.bindValue(observeSelectionComboObserveWidget, warningSeverityWarningObserveValue, null, null);
		//
		return bindingContext;
	}

    /**
     * Gets title of dialog window
     * @return the title
     * @since 2.2
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title of this dialog window
     * @param title the title to set
     * @since 2.2
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets the label for the text field
     * @return the comment key
     * @since 2.2
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the label for the text field
     * @param key the key to set
     * @since 2.2
     */
    public void setKey(String key) {
        this.key = key;
    }

    
}
