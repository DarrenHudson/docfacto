package com.docfacto.adam.preferences;



import com.docfacto.config.generated.CommentRegex;
import com.docfacto.config.generated.Severity;

public class WarningBean extends AbstractModelObject{
	
	private String warningName;
	private String warningSeverity;
	
	public WarningBean(CommentRegex warning) {
		this.warningName = warning.getValue();
		this.warningSeverity = warning.getSeverity().value();
	}
	public WarningBean(String warnName) {
		this.warningName = warnName;
	}
	/**
	 * @return the warningSeverity
	 */
	public String getWarningSeverity() {
		return warningSeverity;
	}
	/**
	 * @param warningSeverity the warningSeverity to set
	 */
	public void setWarningSeverity(String warningSeverity) {
		String oldValue = warningSeverity;
		firePropertyChange("warningSeverity", oldValue, this.warningSeverity);
		this.warningSeverity = warningSeverity;
	}
	/**
	 * @return the warningName
	 */
	public String getWarningName() {
		return warningName;
	}
	/**
	 * @param warningName the warningName to set
	 */
	public void setWarningName(String warningName) {
		String oldValue = warningName;
		firePropertyChange("warningName", oldValue, this.warningName);
		this.warningName = warningName;
	}
	
	public String[] getItems(){
		return new String[]{
				Severity.WARNING.value(),
				Severity.ERROR.value(),
				Severity.INFO.value() };
		
		
	}

	

}
