package com.docfacto.adam.preferences;

import java.util.ArrayList;

/**
 * @author pemi
 * @since 2.2
 *
 */
public class ScopeGroups extends AbstractModelObject {
	private final ArrayList<ScopeGroup> t_scopes = new ArrayList<ScopeGroup>();

	/**
	 * @param scopeGroup - add to <code>ScopeGroups</code>
	 * @since 2.2
	 */
	public void addScope(ScopeGroup scopeGroup) {
		t_scopes.add(scopeGroup);
		firePropertyChange("scopes", null, t_scopes);
	}

	/**
	 * @param tags - remove from <code>ScopeGroups</code>
	 * @since 2.2
	 */
	public void removeScope(ScopeGroup tags) {
		t_scopes.remove(tags);
		firePropertyChange("scopes", null, t_scopes);
	}

	/**
	 * @return <code>ArrayList</code> of <code>ScopeGroup</code>s
	 * @since 2.2
	 */
	public ArrayList<ScopeGroup> getScopes() {
		return t_scopes;
	}

}