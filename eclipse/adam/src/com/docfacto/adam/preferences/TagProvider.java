package com.docfacto.adam.preferences;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.Viewer;

import com.docfacto.config.generated.RequiredDocumentation;

/**
 * A model object which holds <code>AdamTag</code>s
 * @author pemi
 * @since 2.1
 *
 */
public class TagProvider extends AbstractModelObject implements IContentProvider {
	private final List<AdamTag> tags = new ArrayList<AdamTag>();
	
	private boolean enumConstantReq;
    private boolean interfaceMethodsCommented;
    private boolean CommentGetSet;
    private RequiredDocumentation requiredDocumentation;
	
	 
	/**
	 * Method adds <code>AdamTag</code> to TagProvider
	 * @param tag To be added to list
	 * @return true if tag already exists in list
	 */
	public boolean addTag(AdamTag tag) {
		boolean notDuplicate = true;
		String tagID = "";
		for (AdamTag atag : tags) {
			tagID = atag.getName() + atag.getScope();

			if (tagID.equals(tag.getName() + tag.getScope())) {
				System.out.println("duplicate found");
				notDuplicate = false;

			}

		}
		if (notDuplicate) {

			tags.add(tag);
			firePropertyChange("tags", null, tags);

		}

		return notDuplicate;

	}

	/**
	 * @param tag removes this <code>AdamTag</code> from <code>TagProvider</code>
	 */
	public void removeTag(AdamTag tag) {
		tags.remove(tag);
		firePropertyChange("tags", null, tags);
	}

	/**
	 * @return a <code>List</code> of <code>AdamTag</code>s
	 */
	public List<AdamTag> getTags() {

		return tags;
	}

	/**
	 * @param tagProvider a <code>TagProvider</code>
	 * @param deleteOld true if old <code>AdamTag</code>s should be discarded
	 */
	public void setTags(TagProvider tagProvider, boolean deleteOld) {
		if (deleteOld) {
			tags.clear();

		}

		if (tagProvider != null) {
			for (AdamTag aTag : tagProvider.getTags()) {
				String aTagID = aTag.getName() + aTag.getScope();
				boolean duplicate = false;
				if (!tags.isEmpty()) {
					for (AdamTag bTag : tags) {
						if (aTagID.equals(bTag.getName() + bTag.getScope())) {
							duplicate = true;
							System.out.println("Duplicate found");

						}

					}
				}
				if (!duplicate) {

					tags.add(aTag);
					firePropertyChange("tags", null, tags);
				}
			}

		}

	}

	public String getName() {
		return "";
	}

	public void setName(String name) {
	}

	public String getScope() {
		return "";
	}

	public void setScope(String scope) {
	}

	public String getSeverity() {
		return "";
	}

	public void setSeverity(String severity) {
	}

	public String getEnable() {
		return "";
	}

	public void setEnable(String enable) {
	}
    /**
     * @return the enumConstantReq
     */
    public boolean getEnumConstantReq() {
        return enumConstantReq;
    }

    /**
     * @param b the enumConstantReq to set
     */
    public void setEnumConstantReq(boolean b) {
        this.enumConstantReq = b;
    }

    /**
     * @return the interfaceMethodsCommented
     */
    public boolean getInterfaceMethodsCommented() {
        return interfaceMethodsCommented;
    }

    /**
     * @param interfaceMethodsCommented the interfaceMethodsCommented to set
     */
    public void setInterfaceMethodsCommented(
            boolean interfaceMethodsCommented) {
        this.interfaceMethodsCommented = interfaceMethodsCommented;
    }

    /**
     * @return the commentGetSet
     */
    public boolean getCommentGetSet() {
        return CommentGetSet;
    }

    /**
     * @param commentGetSet the commentGetSet to set
     */
    public void setCommentGetSet(boolean commentGetSet) {
        CommentGetSet = commentGetSet;
    }
    
    /**
     * @param requiredDocumentation
     */
    public void setRequiredDoc(RequiredDocumentation requiredDocumentation) {
        this.requiredDocumentation = requiredDocumentation;
    }
    
    /**
     * @return the requiredDocumentation
     */
    public RequiredDocumentation getRequiredDocumentation() {
        return requiredDocumentation;
    }

	@Override
	public void dispose() {
		// Not implemented

	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// not implemented

	}

	/**
	 * @return String array of scope names
	 */
	public String[] getTagScopes() {

		HashSet<String> items = new HashSet<String>();
		String[] scopeItems = null;
		if (tags != null) {
			List<AdamTag> taglist = getTags();
			for (AdamTag a : taglist) {
				items.add(a.getScope());
			}
			int x = 0;
			scopeItems = new String[items.size()];
			for (String a : items) {
				scopeItems[x] = a;
				x++;
			}
		}

		return scopeItems;
	}

    

    

}
