package com.docfacto.adam.preferences;



import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class GetXMLFilePath extends AbstractModelObject implements IContentProvider {
	
	private String filepath = "";
	private boolean delOld = true;

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
		
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		
		this.filepath = filepath;
		
	}

	public boolean getDelOld() {
		return delOld;
	}

	public void setDelOld(boolean delOld) {
		this.delOld = delOld;
	}
	
	
	

} 
