package com.docfacto.adam.preferences;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.docfacto.adam.plugin.DocfactoAdamPlugin;
import com.docfacto.common.DocfactoException;
import com.docfacto.config.XmlConfig;
import com.docfacto.config.generated.Adam;
import com.docfacto.config.generated.Docfacto;
import com.docfacto.config.generated.RequiredTag;
import com.docfacto.config.generated.RequiredTags;
import com.docfacto.config.generated.Severity;
import com.docfacto.core.DocfactoCorePlugin;

/**
 * Utility class for 'Docfacto.xml' extraction and manipulation
 * 
 * @author pemi
 * @since 2.1
 * 
 */
public class XMLtoAdamTag {
    private TagProvider tags;
    private Adam adam;
    private final String TAG_CLASS = AdamStoreUtil.SCOPE_ARRAY[AdamStoreUtil.CLASSES];
    private final String TAG_METHOD = AdamStoreUtil.SCOPE_ARRAY[AdamStoreUtil.METHODS];
    private final String TAG_CONSTRUCTOR = AdamStoreUtil.SCOPE_ARRAY[AdamStoreUtil.CONSTRUCTORS];
    private final String TAG_PACKAGE = AdamStoreUtil.SCOPE_ARRAY[AdamStoreUtil.PACKAGES];
    private final String TAG_FIELD = AdamStoreUtil.SCOPE_ARRAY[AdamStoreUtil.FIELDS];
    private final String TAG_ENUM = AdamStoreUtil.SCOPE_ARRAY[AdamStoreUtil.ENUMS];
    private final String TAG_INTERFACE = AdamStoreUtil.SCOPE_ARRAY[AdamStoreUtil.INTERFACES];
    private String[] SCOPE = AdamStoreUtil.SCOPE_ARRAY;
    private String tagEnable;

    /**
     * @param xmlpath String representing path of XML file
     * @return <code>TagProvider</code> extracted from specified XML file
     * @since 2.2
     */
    public TagProvider getTagsFromXML() {
        adam = DocfactoCorePlugin.getXmlConfig().getAdamConfig();

        List<RequiredTag> reqTags = null;
        tags = new TagProvider();

        tags.setEnumConstantReq(adam.getEnums().getConstantsRequireJavadoc().isValue());
        tags.setInterfaceMethodsCommented(adam.getMethods().getAllowNoCommentsForInterfaceMethods().isValue());
        tags.setCommentGetSet(adam.getMethods().getAllowNoDescriptionForGetterSetter().isValue());
        tags.setRequiredDoc(adam.getPackages().getRequiredDocumentation());

        for (String scope:SCOPE) {
            try {
                // packages
                if (scope.equals(TAG_PACKAGE)) {
                    reqTags = adam.getPackages().getRequiredTags().getRequiredTags();
                }

                // constructors
                else if (scope.equals(TAG_CONSTRUCTOR)) {
                    reqTags = adam.getConstructors().getRequiredTags().getRequiredTags();
                }

                // classes
                else if (scope.equals(TAG_CLASS)) {
                    reqTags = adam.getClasses().getRequiredTags().getRequiredTags();
                }

                // methods
                else if (scope.equals(TAG_METHOD)) {
                    reqTags = adam.getMethods().getRequiredTags().getRequiredTags();
                }

                // fields
                else if (scope.equals(TAG_FIELD)) {
                    reqTags = adam.getFields().getRequiredTags().getRequiredTags();
                }

                // enums
                else if (scope.equals(TAG_ENUM)) {
                    reqTags = adam.getEnums().getRequiredTags().getRequiredTags();
                }

                // interfaces
                else if (scope.equals(TAG_INTERFACE)) {
                    reqTags = adam.getInterfaces().getRequiredTags().getRequiredTags();
                }

                storeTagsfromConfig(reqTags, scope);
            }
            catch (NullPointerException e) {
                // do nothing; caused by empty scope
            }
        }

        return tags;
    }

    /**
     * @param reqTags list of <code>RequiredTag</code>s
     * @param scope name of scope
     * @since 2.2
     */
    private void storeTagsfromConfig(List<RequiredTag> reqTags,String scope) {
        String tagName = null;
        String tagSeverity = null;

        for (RequiredTag aReqTag:reqTags) {
            tagName = aReqTag.getValue();
            tagSeverity = aReqTag.getSeverity().value();
            tagEnable = Boolean.toString(aReqTag.isEnabled());
            tags.addTag(new AdamTag(tagName, scope, tagSeverity, tagEnable));
        }

    }

    /**
     * @param tagsave <code>TagProvider<code> to add to <code>Adam</code>
     * @since 2.2
     */
    private void updateAdam(Adam adam,TagProvider tagsave) {
        // remove existing <required-tags> nodes from adam
        for (String tagScope:SCOPE) {
            boolean requiredNodeFound = false;

            try {
                if (tagScope.equals(TAG_PACKAGE)) {
                    adam.getPackages().setRequiredTags(null);
                }
                else if (tagScope.equals(TAG_CLASS)) {
                    adam.getClasses().setRequiredTags(null);
                }
                else if (tagScope.equals(TAG_METHOD)) {
                    adam.getMethods().setRequiredTags(null);
                }
                else if (tagScope.equals(TAG_FIELD)) {
                    adam.getFields().setRequiredTags(null);
                }
                else if (tagScope.equals(TAG_CONSTRUCTOR)) {
                    adam.getConstructors().setRequiredTags(null);
                }
                else if (tagScope.equals(TAG_ENUM)) {
                    adam.getEnums().setRequiredTags(null);
                }
                else if (tagScope.equals(TAG_INTERFACE)) {
                    adam.getInterfaces().setRequiredTags(null);
                }
            }
            catch (Exception e) {
                System.out.println("error in loop "+tagScope);
            }

            // Add tags associated with current scope
            for (AdamTag a_tag:tagsave.getTags()) {
                RequiredTag reqTag = new RequiredTag();
                reqTag.setValue(a_tag.getName());
                reqTag.setSeverity(Severity.fromValue(a_tag.getSeverity()));
                reqTag.setEnabled(Boolean.valueOf(a_tag.getEnable()));

                if (a_tag.getScope().equals(TAG_PACKAGE)
                    &&a_tag.getScope().equals(tagScope)) {
                    if (!requiredNodeFound) {
                        adam.getPackages().setRequiredTags(new RequiredTags());
                        requiredNodeFound = true;

                    }

                    adam.getPackages().getRequiredTags().getRequiredTags().add(reqTag);

                }
                else if (a_tag.getScope().equals(TAG_CLASS)
                    &&a_tag.getScope().equals(tagScope)) {
                    if (!requiredNodeFound) {
                        adam.getClasses().setRequiredTags(new RequiredTags());
                        requiredNodeFound = true;
                    }

                    adam.getClasses().getRequiredTags().getRequiredTags().add(reqTag);
                }
                else if (a_tag.getScope().equals(TAG_METHOD)
                    &&a_tag.getScope().equals(tagScope)) {
                    if (!requiredNodeFound) {
                        adam.getMethods().setRequiredTags(new RequiredTags());
                        requiredNodeFound = true;
                    }

                    adam.getMethods().getRequiredTags().getRequiredTags().add(reqTag);

                }
                else if (a_tag.getScope().equals(TAG_FIELD)
                    &&a_tag.getScope().equals(tagScope)) {

                    if (!requiredNodeFound) {
                        adam.getFields().setRequiredTags(new RequiredTags());
                        requiredNodeFound = true;
                    }

                    adam.getFields().getRequiredTags().getRequiredTags().add(reqTag);
                }
                else if (a_tag.getScope().equals(TAG_CONSTRUCTOR)
                    &&a_tag.getScope().equals(tagScope)) {
                    if (!requiredNodeFound) {
                        adam.getConstructors().setRequiredTags(new RequiredTags());
                        requiredNodeFound = true;
                    }
                    adam.getConstructors().getRequiredTags().getRequiredTags().add(reqTag);

                }
                else if (a_tag.getScope().equals(TAG_ENUM)
                    &&a_tag.getScope().equals(tagScope)) {
                    if (!requiredNodeFound) {
                        adam.getEnums().setRequiredTags(new RequiredTags());
                        requiredNodeFound = true;
                    }
                    adam.getEnums().getRequiredTags().getRequiredTags().add(reqTag);

                }
                else if (a_tag.getScope().equals(TAG_INTERFACE)
                    &&a_tag.getScope().equals(tagScope)) {
                    if (!requiredNodeFound) {
                        adam.getInterfaces().setRequiredTags(new RequiredTags());
                        requiredNodeFound = true;
                    }
                    adam.getInterfaces().getRequiredTags().getRequiredTags().add(reqTag);
                }

            }
        }
        adam.getMethods().getAllowNoCommentsForInterfaceMethods().setValue(tagsave.getInterfaceMethodsCommented());
        adam.getMethods().getAllowNoDescriptionForGetterSetter().setValue(tagsave.getCommentGetSet());
        adam.getEnums().getConstantsRequireJavadoc().setValue(tagsave.getEnumConstantReq());
        adam.getPackages().setRequiredDocumentation(tagsave.getRequiredDocumentation());
    }

    /**
     * Save values to XML
     * 
     * @param tagProvider - the {@code TagProvider} to save
     * @since 2.2
     */
    public void saveTags(TagProvider tagProvider) {
        XmlConfig config = DocfactoCorePlugin.getXmlConfig();
        updateAdam(config.getAdamConfig(), tagProvider);

        try {
            config.save();
        }
        catch (DocfactoException e) {
            DocfactoAdamPlugin.logException("Could not save XML", e);
        }

    }

    /**
     * Export adam element to the specified path Method does not update the CORE xml file
     * 
     * @param tags2 - tags to be saved
     * @param folderpath - the folder path
     * @since 2.2
     */
    public void saveTags(Adam adam,TagProvider tags2,String folderpath) {
        Marshaller jaxbMarshaller = null;
        updateAdam(adam, tags2);
        Docfacto doc = new Docfacto();
        doc.setAdam(adam);

        try {
            File file = new File(folderpath);
            JAXBContext jaxbContext = JAXBContext.newInstance(Docfacto.class);
            jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(doc, file);

        }
        catch (JAXBException e) {
            e.printStackTrace();
        }

    }

}
