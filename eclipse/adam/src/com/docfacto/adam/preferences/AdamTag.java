package com.docfacto.adam.preferences;


/**
 * AdamTag is a java bean of a javadoc tag.
 * <p>
 * This bean is used in the preference page settings,
 * It extends {@code com.docfacto.adam.preferences.AbstracModelObject} 
 * which gives it its capability to notify its users of changes. 
 * </p>
 * 
 * @author pemi - created Jan 22, 2013 8:45:02 AM
 * @since 2.1
 *
 */
public class AdamTag extends AbstractModelObject{


	private String tname = "";
	private String tscope = "";
	private String tseverity = "";
	private String tenabled = "";
	
	/**
	 * Constructor initializes the necessary parameters
	 * @param name - Name of tag in bean
	 * @param scope - scope of this tag; one of (methods,packages, classes, enum, interfaces)
	 * @param severity - severity of this tag (as described in configuration)
	 * @param enable - rather than delete a tag, users can set the enable value to {@code false}
	 * @since 2.1
	 */
	public AdamTag(String name, String scope, String severity,
			String enable) {
		
		tname = name;
		tscope = scope;
		tseverity = severity;
		tenabled = enable;
	}

	
	/**
	 * Gets the name of this tag
	 * @return tag name
	 * @since 2.1
	 */
	public String getName() {
		return tname;
		
	}

	/**
	 * Sets name of tag
	 * @param name sets name of tag
	 * @since 2.1
	 */
	public void setName(String name) {
		String oldValue = name;
		tname = name;
		firePropertyChange("name", oldValue, tname);
	}
	
	/**
	 * Get scope of tag
	 * @return scope of tag
	 * @since 2.1
	 */
	public String getScope() {
		return tscope;
	}

	/**
	 * Set the scope of tag
	 * @param scope value to set scope of this tag
	 * @since 2.1
	 */
	public void setScope(String scope) {
		String oldValue = scope;
		tscope = scope;
		firePropertyChange("scope", oldValue, tscope);
	}
	
	/**
	 * Gets the severity of tag
	 * @return severity of this tag
	 * @since 2.1
	 */
	public String getSeverity() {
		return tseverity;
	}

	/**
	 * Sets the severity of tag
	 * @param severity value to set as new severity
	 * @since 2.1
	 */
	public void setSeverity(String severity) {
		String oldValue = severity;
		tseverity = severity;
		firePropertyChange("severity", oldValue, tseverity);
	}
	
	/**
	 * Returns "true" if tag is enabled
	 * @return String representation of tag state ( one of "true" or "false")
	 * @since 2.1
	 */
	public String getEnable() {
		return tenabled;
	}

	/**
	 * Sets the value of tag state.
	 * @param enable {@code String} 
	 * value "true" would enable tag otherwise tag is considered disabled
	 * @since 2.1
	 */
	public void setEnable(String enable) {
		String oldValue = enable;
		tenabled = enable;
		firePropertyChange("enable", oldValue, tenabled);
	}

}
