package com.docfacto.adam.preferences;

import java.io.File;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.core.databinding.beans.PojoProperties;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.ModifyEvent;

public class XMLImportDialog extends Dialog {
	private DataBindingContext m_bindingContext;
	private Button btnNewButton;
	private final GetXMLFilePath xmlFilePathBean;
	private Text text;
	private Button btnAddToCurrent;
	private Button button;
	/**
	 * Create the dialog.
	 * @param parentShell
	 * @param filepath 
	 */
	public XMLImportDialog(Shell parentShell, GetXMLFilePath filepath) {
		super(parentShell);
		xmlFilePathBean = filepath;
	}

	/**
	 * Create contents of the dialog.
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout gridLayout = (GridLayout) container.getLayout();
		gridLayout.numColumns = 4;
		new Label(container, SWT.NONE);
		
				
				Label lblLoadXmlFile = new Label(container, SWT.NONE);
				lblLoadXmlFile.setText("Load tags from XML");
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		
		text = new Text(container, SWT.BORDER);
		text.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				File file = new File(text.getText());
				boolean fileExists = file.exists()? true: false;
				if(fileExists){
					button.setEnabled(true);
				}else{
					button.setEnabled(false);
				}
				
				
			}
		});
		text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(container, SWT.NONE);
		
		btnNewButton = new Button(container, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

		        FileDialog dlg = new FileDialog(getShell());

		        // Set the initial filter path to user's selection
		        dlg.setFilterPath(text.getText());

		        // Change the title bar text
		        dlg.setText("Please select a Docfacto.xml file");
		        
		        //set file name
		        dlg.setFileName("Docfacto.xml");

		        // Calling open() will open and run the dialog.
		        // It will return the selected directory, or
		        // null if user cancels
		        xmlFilePathBean.setFilepath(dlg.open());
		        m_bindingContext.updateModels();
		        System.out.println("The text is :"+text.getText());
		        
		        System.out.println("The bean is :"+xmlFilePathBean.getFilepath());
		        


			}
		});
		btnNewButton.setText("Browse...");
				new Label(container, SWT.NONE);
		
				
				btnAddToCurrent = new Button(container, SWT.CHECK);
				btnAddToCurrent.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						m_bindingContext.updateModels();
						
						
					}
				});
				btnAddToCurrent.setText("discard old values");
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);


		return container;
	}

	/**
	 * Create contents of the button bar.
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		button = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		button.setEnabled(false);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
		m_bindingContext = initDataBindings();

	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(450, 300);
	}
	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		IObservableValue filepathXmlFilePathBeanObserveValue = BeanProperties.value("filepath").observe(xmlFilePathBean);
		IObservableValue observeTextTextObserveWidget = WidgetProperties.text(SWT.Modify).observe(text);
		bindingContext.bindValue(filepathXmlFilePathBeanObserveValue, observeTextTextObserveWidget, null, null);
		//
		IObservableValue selectionBtnAddToCurrentObserveValue = PojoProperties.value("selection").observe(btnAddToCurrent);
		IObservableValue delOldXmlFilePathBeanObserveValue = BeanProperties.value("delOld").observe(xmlFilePathBean);
		bindingContext.bindValue(selectionBtnAddToCurrentObserveValue, delOldXmlFilePathBeanObserveValue, null, null);
		//
		return bindingContext;
	}
}
