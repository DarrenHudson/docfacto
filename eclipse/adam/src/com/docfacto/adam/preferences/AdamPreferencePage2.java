package com.docfacto.adam.preferences;

import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.map.IObservableMap;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.databinding.viewers.ObservableMapLabelProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.dialogs.PreferencesUtil;

import com.docfacto.adam.plugin.AddMarkersToResources;
import com.docfacto.adam.plugin.DocfactoAdamPlugin;
import com.docfacto.adam.plugin.EclipseAdamUtils;
import com.docfacto.common.DocfactoException;
import com.docfacto.config.generated.Adam;
import com.docfacto.config.generated.CommentRegex;
import com.docfacto.config.generated.Comments;
import com.docfacto.config.generated.RuleWithSeverity;
import com.docfacto.config.generated.Severity;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.core.XmlChangeListener;

/**
 * Preference page - [comment settings] child node of adam preferences
 * 
 * @author pemi - created March 4, 2013 4:11:29 PM
 * 
 * @since 2.2
 */
public class AdamPreferencePage2 extends PreferencePage implements IWorkbenchPreferencePage,XmlChangeListener {
    private DataBindingContext m_bindingContext;

    private Button btnCheckComments;
    private Button throwsCheckBox;
    private Button meaninglessCheckBox;
    private Button paramCheckBox;
    private Button returnCheckBox;
    private Button deprecatedCheckBox;
    private Button desc2CheckBox;
    private Button htmlFLCheckBox;
    private Button htmlImbalanceCheckBox;
    private Button seeCheckBox;
    private Button malformedTagCheckBox;

    private Table table_1;
    private TableViewer tableViewer;
    private TableViewerColumn tableViewerColumn_1;
    private TableColumn tblclmnString;
    private TableColumn tblclmnSeverity;
    private String[] severityComboItems;
    private CommentWarning commentWarning;
    private WarningBean warnbean;
    private Comments comments;
    private Combo comboMeanSev;
    private Combo comboParamSev;
    private Combo comboThrowsSev;
    private Combo comboCommSev;
    private Combo comboReturnSev;
    private Combo comboDeprecatedSev;
    private Combo comboDeprecatedSev2;
    private Combo combohtmlFLSev;
    private Combo comboHtmlImbalance;
    private Combo comboSeeSev;
    private Combo comboMalformedTag;
    private Button btnCommentRequiredIn;
    private List<CommentRegex> warnings;
    private Button btnAdd;
    private Button btnEdit;
    private Button btnDelete;
    IPreferenceStore store = DocfactoAdamPlugin.getDefault().getPreferenceStore();
    private Group grpWarnIfJavadoc;

    private Composite composite;

    private Composite theContainer;

    private boolean hasChanged;

    /**
     * Constructor to create the preference page.
     */
    public AdamPreferencePage2() {
    }

    @Override
    public void xmlChanged() {
        // TODO Auto-generated method stub
    }

    @Override
    public void xmlReloaded() {
        if (theContainer!=null) {
            Composite parent = theContainer.getParent().getParent();
            super.createControl(parent);
        }
    }

    /*
     * Create contents of the preference page.
     * 
     * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse .swt.widgets.Composite)
     */
    @Override
    public Control createContents(Composite parent) {
        hasChanged = false;
        commentWarning = new CommentWarning();

        severityComboItems = new String[] {
            Severity.WARNING.value(),
            Severity.ERROR.value(),
            Severity.INFO.value()
        };

        theContainer = new Composite(parent, SWT.NONE);
        // Check if internal XML is being used
        if (DocfactoCorePlugin.getDefault().isUsingInternalXML()) {
            // display error message above page
            setMessage("Using default configuration settings", WARNING);

            Link link = new Link(theContainer, SWT.NONE);
            link.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    PreferencesUtil
                        .createPreferenceDialogOn(null,
                            "com.docfacto.core.preference.Docfacto", null, null)
                        .open();
                }
            });

            link.setText("<a>Set your configuration file</a>");

            return theContainer;

        }
        // Check if 'Docfacto.xml' has been validated
        else if (!DocfactoCorePlugin.getDefault().getPreferenceStore().getBoolean(DocfactoCorePlugin.KEY_VALIDATED)) {
            // display error message above page
            setErrorMessage("A valid docfacto configuration file is required");

            Label lblTagsInSelected = new Label(theContainer, SWT.WRAP|SWT.CENTER);
            lblTagsInSelected.setBounds(5, 5, 300, 150);
            lblTagsInSelected.setText(DocfactoCorePlugin.getDefault().getPreferenceStore()
                .getString(DocfactoCorePlugin.VALIDATION_ERROR)+
                "\n\nYou will have to relaunch this window before your changes take effect");
            return theContainer;
        }

        Adam adam = DocfactoCorePlugin.getXmlConfig().getAdamConfig();

        // get value & severity of configuration comment tags
        if (adam!=null) {
            System.out.println("Adam is null");
            if (adam.getAll()!=null) {
                System.out.println("Adam .getAll() is null");
                if (adam.getAll().getComments()!=null) {
                    System.out.println("Adam.getAll().getComments() is null");
                }
            }
        }

        // check box to enable/disable detailed comment processing
        btnCheckComments = new Button(theContainer, SWT.CHECK);
        btnCheckComments.setBounds(0, 10, 379, 18);
        btnCheckComments.setText("Enable detailed javadoc checking");
        btnCheckComments.addSelectionListener(new SelectionAdapter() {
            // enable or disable all elements based on selection
            @Override
            public void widgetSelected(SelectionEvent e) {
                returnCheckBox.setEnabled(btnCheckComments.getSelection());
                throwsCheckBox.setEnabled(btnCheckComments.getSelection());
                meaninglessCheckBox.setEnabled(btnCheckComments.getSelection());
                paramCheckBox.setEnabled(btnCheckComments.getSelection());
                deprecatedCheckBox.setEnabled(btnCheckComments.getSelection());
                desc2CheckBox.setEnabled(btnCheckComments.getSelection());
                htmlFLCheckBox.setEnabled(btnCheckComments.getSelection());
                htmlImbalanceCheckBox.setEnabled(btnCheckComments.getSelection());
                seeCheckBox.setEnabled(btnCheckComments.getSelection());
                malformedTagCheckBox.setEnabled(btnCheckComments.getSelection());
                btnCommentRequiredIn.setEnabled(btnCheckComments.getSelection());
                comboMeanSev.setEnabled(btnCheckComments.getSelection());
                comboParamSev.setEnabled(btnCheckComments.getSelection());
                combohtmlFLSev.setEnabled(btnCheckComments.getSelection());
                comboHtmlImbalance.setEnabled(btnCheckComments.getSelection());
                comboThrowsSev.setEnabled(btnCheckComments.getSelection());
                comboCommSev.setEnabled(btnCheckComments.getSelection());
                comboReturnSev.setEnabled(btnCheckComments.getSelection());
                comboDeprecatedSev.setEnabled(btnCheckComments.getSelection());
                comboDeprecatedSev2.setEnabled(btnCheckComments.getSelection());
                comboSeeSev.setEnabled(btnCheckComments.getSelection());
                comboMalformedTag.setEnabled(btnCheckComments.getSelection());
                table_1.setEnabled(btnCheckComments.getSelection());
                btnAdd.setEnabled(btnCheckComments.getSelection());
                btnEdit.setEnabled(btnCheckComments.getSelection());
                btnDelete.setEnabled(btnCheckComments.getSelection());
            }
        });
        composite = new Composite(theContainer, SWT.NONE);
        composite.setBounds(0, 34, 371, 295);

        comments = adam.getAll().getComments();
        RuleWithSeverity checkMeaning = comments.getCheckMeaninglessComment();
        RuleWithSeverity commentReq = comments.getCommentRequired();
        RuleWithSeverity paramDesc = comments.getParamTagRequireDescriptions();
        RuleWithSeverity throwDesc = comments.getThrowsTagRequireDescriptions();
        RuleWithSeverity returnDesc = comments.getReturnTagRequireDescription();
        RuleWithSeverity deprecatedDesc = comments.getDeprecatedTagRequireDescriptions();
        RuleWithSeverity deprecated2Desc = comments.getDeprecatedAnnotationRequiresTag();
        RuleWithSeverity invalidFLDesc = comments.getInvalidHtmlInFirstline();
        RuleWithSeverity htmlImbalanceDesc = comments.getHtmlImbalance();
        RuleWithSeverity seeInvalidDesc = comments.getInvalidSeeTag();
        RuleWithSeverity malformedTagDesc = comments.getMalformedTag();
        warnings = comments.getWarningIfContains();

        boolean meaningVal = checkMeaning.isValue();
        boolean commentVal = commentReq.isValue();
        boolean paramDescVal = paramDesc.isValue();
        boolean throwDescVal = throwDesc.isValue();
        boolean returnDescVal = returnDesc.isValue();
        boolean deprecatedDescVal = deprecatedDesc.isValue();
        boolean depre2DescVal = deprecated2Desc.isValue();
        boolean invalidFLVal = invalidFLDesc.isValue();
        boolean imbalanceHTMLVal = htmlImbalanceDesc.isValue();
        boolean seeValidVal = seeInvalidDesc.isValue();
        boolean malformedTagVal = malformedTagDesc.isValue();
        Severity commentReqdSev = commentReq.getSeverity();
        Severity throwsDescSev = throwDesc.getSeverity();
        Severity meaningSev = checkMeaning.getSeverity();
        Severity paramDescSev = paramDesc.getSeverity();
        Severity returnDescSev = returnDesc.getSeverity();
        Severity deprecatedDescSev = deprecatedDesc.getSeverity();
        Severity deprecatedDesc2Sev = deprecated2Desc.getSeverity();
        Severity invalidFLSev = invalidFLDesc.getSeverity();
        Severity imbalanceHTMLSev = htmlImbalanceDesc.getSeverity();
        Severity invalidSeeSev = seeInvalidDesc.getSeverity();
        Severity malformedTagSev = malformedTagDesc.getSeverity();
        if (store.getBoolean(AdamStoreUtil.JAVADOC_CHECK)) {
            btnCheckComments.setSelection(false);
        }
        else {
            btnCheckComments.setSelection(true);
        }

        // create check box label and combos
        // line 1: comment description required in javadoc
        btnCommentRequiredIn = new Button(composite, SWT.CHECK);
        btnCommentRequiredIn.setBounds(16, 20, 246, 20);
        btnCommentRequiredIn.setText("comment description required in javadoc");
        btnCommentRequiredIn.setSelection(commentVal);
        comboCommSev = new Combo(composite, SWT.READ_ONLY);
        comboCommSev.setBounds(268, 20, 93, 20);
        comboCommSev.setItems(severityComboItems);
        comboCommSev.select(EclipseAdamUtils.getIndex(severityComboItems, commentReqdSev.value()));

        // line 2: check meaningless comment
        malformedTagCheckBox = new Button(composite, SWT.CHECK);
        malformedTagCheckBox.setBounds(16, 45, 246, 20);
        malformedTagCheckBox.setText("check malformed tags");
        malformedTagCheckBox.setSelection(malformedTagVal);
        comboMalformedTag = new Combo(composite, SWT.READ_ONLY);
        comboMalformedTag.setBounds(268, 45, 93, 20);
        comboMalformedTag.setItems(severityComboItems);
        comboMalformedTag.select(EclipseAdamUtils.getIndex(severityComboItems, malformedTagSev.value()));

        // line 3: check meaningless comment
        meaninglessCheckBox = new Button(composite, SWT.CHECK);
        meaninglessCheckBox.setBounds(16, 70, 246, 20);
        meaninglessCheckBox.setText("check meaningless comment");
        meaninglessCheckBox.setSelection(meaningVal);
        comboMeanSev = new Combo(composite, SWT.READ_ONLY);
        comboMeanSev.setBounds(268, 70, 93, 20);
        comboMeanSev.setItems(severityComboItems);
        comboMeanSev.select(EclipseAdamUtils.getIndex(severityComboItems, meaningSev.value()));

        // line 4: invalid HTML in first line
        htmlFLCheckBox = new Button(composite, SWT.CHECK);
        htmlFLCheckBox.setBounds(16, 95, 246, 20);
        htmlFLCheckBox.setText("invalid first line in HTML");
        htmlFLCheckBox.setSelection(invalidFLVal);
        combohtmlFLSev = new Combo(composite, SWT.READ_ONLY);
        combohtmlFLSev.setBounds(268, 95, 93, 20);
        combohtmlFLSev.setItems(severityComboItems);
        combohtmlFLSev.select(EclipseAdamUtils.getIndex(severityComboItems, invalidFLSev.value()));

        // line 5: HTML imbalance line
        htmlImbalanceCheckBox = new Button(composite, SWT.CHECK);
        htmlImbalanceCheckBox.setBounds(16, 120, 246, 20);
        htmlImbalanceCheckBox.setText("check HTML imbalance");
        htmlImbalanceCheckBox.setSelection(imbalanceHTMLVal);
        comboHtmlImbalance = new Combo(composite, SWT.READ_ONLY);
        comboHtmlImbalance.setBounds(268, 120, 93, 20);
        comboHtmlImbalance.setItems(severityComboItems);
        comboHtmlImbalance.select(EclipseAdamUtils.getIndex(severityComboItems, imbalanceHTMLSev.value()));

        // line 6: @param tag requires description
        paramCheckBox = new Button(composite, SWT.CHECK);
        paramCheckBox.setBounds(16, 145, 246, 20);
        paramCheckBox.setText("@param tag requires description");
        paramCheckBox.setSelection(paramDescVal);
        comboParamSev = new Combo(composite, SWT.READ_ONLY);
        comboParamSev.setBounds(268, 145, 93, 20);
        comboParamSev.setItems(severityComboItems);
        comboParamSev.select(EclipseAdamUtils.getIndex(severityComboItems, paramDescSev.value()));

        // line 7: @throws tag requires description
        throwsCheckBox = new Button(composite, SWT.CHECK);
        throwsCheckBox.setBounds(16, 170, 246, 20);
        throwsCheckBox.setText("@throws tag requires description");
        throwsCheckBox.setSelection(throwDescVal);
        comboThrowsSev = new Combo(composite, SWT.READ_ONLY);
        comboThrowsSev.setBounds(268, 170, 93, 20);
        comboThrowsSev.setItems(severityComboItems);
        comboThrowsSev.select(EclipseAdamUtils.getIndex(severityComboItems, throwsDescSev.value()));

        // line 8: @return tag requires description"
        returnCheckBox = new Button(composite, SWT.CHECK);
        returnCheckBox.setBounds(16, 195, 246, 20);
        returnCheckBox.setText("@return tag requires description");
        returnCheckBox.setSelection(returnDescVal);
        comboReturnSev = new Combo(composite, SWT.READ_ONLY);
        comboReturnSev.setBounds(268, 195, 93, 20);
        comboReturnSev.setItems(severityComboItems);
        comboReturnSev.select(EclipseAdamUtils.getIndex(severityComboItems, returnDescSev.value()));

        // line 9: @deprecated tag requires description
        deprecatedCheckBox = new Button(composite, SWT.CHECK);
        deprecatedCheckBox.setBounds(16, 220, 246, 20);
        deprecatedCheckBox.setText("@deprecated tag requires description");
        deprecatedCheckBox.setSelection(deprecatedDescVal);
        comboDeprecatedSev = new Combo(composite, SWT.READ_ONLY);
        comboDeprecatedSev.setBounds(268, 220, 93, 20);
        comboDeprecatedSev.setItems(severityComboItems);
        comboDeprecatedSev.select(EclipseAdamUtils.getIndex(severityComboItems, deprecatedDescSev.value()));

        // line 10: @deprecated annotation requires tag
        desc2CheckBox = new Button(composite, SWT.CHECK);
        desc2CheckBox.setBounds(16, 245, 246, 20);
        desc2CheckBox.setText("@deprecated annotation requires tag");
        desc2CheckBox.setSelection(depre2DescVal);
        comboDeprecatedSev2 = new Combo(composite, SWT.READ_ONLY);
        comboDeprecatedSev2.setBounds(268, 245, 93, 20);
        comboDeprecatedSev2.setItems(severityComboItems);
        comboDeprecatedSev2.select(EclipseAdamUtils.getIndex(severityComboItems, deprecatedDesc2Sev.value()));

        // line 11: @see tag requires validity check
        seeCheckBox = new Button(composite, SWT.CHECK);
        seeCheckBox.setBounds(16, 270, 246, 20);
        seeCheckBox.setText("@see validity check");
        seeCheckBox.setSelection(seeValidVal);
        comboSeeSev = new Combo(composite, SWT.READ_ONLY);
        comboSeeSev.setBounds(268, 270, 93, 20);
        comboSeeSev.setItems(severityComboItems);
        comboSeeSev.select(EclipseAdamUtils.getIndex(severityComboItems, invalidSeeSev.value()));

        // save variable for CommentWarning
        for (CommentRegex warning:warnings) {
            warnbean = new WarningBean(warning);
            commentWarning.addWarning(warnbean);
        }

        grpWarnIfJavadoc = new Group(theContainer, SWT.NONE);
        grpWarnIfJavadoc.setText("Warn if javadoc comments contain any of the following keywords.");
        grpWarnIfJavadoc.setBounds(10, 335, 361, 107);

        // add button for warning-if-contains
        btnAdd = new Button(grpWarnIfJavadoc, SWT.NONE);
        btnAdd.setBounds(235, 10, 94, 20);
        btnAdd.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                final WarningBean warningItem = new WarningBean("");
                final CmntWarnDialog dialog = new CmntWarnDialog(getShell(), warningItem);
                if (dialog.open()==Window.OK) {
                    commentWarning.addWarning(warningItem);
                    m_bindingContext.updateModels();
                    tableViewer.refresh();
                    hasChanged = true;
                }
            }
        });
        btnAdd.setText("Add");

        // edit button for warning-if-contains
        btnEdit = new Button(grpWarnIfJavadoc, SWT.NONE);
        btnEdit.setBounds(235, 35, 94, 20);
        btnEdit.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                final IStructuredSelection selection = (IStructuredSelection)tableViewer.getSelection();
                final WarningBean warningItem = (WarningBean)selection.getFirstElement();
                final CmntWarnDialog dialog = new CmntWarnDialog(getShell(), warningItem);
                if (dialog.open()==Window.OK) {
                    m_bindingContext.updateModels();
                    tableViewer.refresh();
                    hasChanged = true;
                }
            }
        });
        btnEdit.setText("Edit");

        // delete button for warning-if-contains
        btnDelete = new Button(grpWarnIfJavadoc, SWT.NONE);
        btnDelete.setBounds(235, 58, 94, 20);
        btnDelete.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                final IStructuredSelection selection = (IStructuredSelection)tableViewer.getSelection();
                final WarningBean warningItem = (WarningBean)selection.getFirstElement();
                new CmntWarnDialog(getShell(), warningItem);
                boolean confirm = MessageDialog.openConfirm(getShell(), "Confirm Delete",
                    "Delete keyword '"+warningItem.getWarningName()+"'?");

                if (confirm) {
                    commentWarning.removeWarning(warningItem);
                    m_bindingContext.updateModels();
                    tableViewer.refresh();
                    hasChanged = true;
                }
            }
        });
        btnDelete.setText("Delete");

        // create table for keywords
        tableViewer = new TableViewer(grpWarnIfJavadoc, SWT.BORDER|SWT.FULL_SELECTION);
        table_1 = tableViewer.getTable();
        table_1.setBounds(10, 10, 221, 100);
        table_1.setLinesVisible(true);
        table_1.setHeaderVisible(true);

        TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
        tblclmnString = tableViewerColumn.getColumn();
        tblclmnString.setWidth(100);
        tblclmnString.setText("Keyword");

        tableViewerColumn_1 = new TableViewerColumn(tableViewer, SWT.NONE);
        tblclmnSeverity = tableViewerColumn_1.getColumn();
        tblclmnSeverity.setWidth(100);
        tblclmnSeverity.setText("Severity");

        // enable/disable check boxes depending on parent selection
        throwsCheckBox.setEnabled(btnCheckComments.getSelection());
        returnCheckBox.setEnabled(btnCheckComments.getSelection());
        meaninglessCheckBox.setEnabled(btnCheckComments.getSelection());
        htmlFLCheckBox.setEnabled(btnCheckComments.getSelection());
        htmlImbalanceCheckBox.setEnabled(btnCheckComments.getSelection());
        paramCheckBox.setEnabled(btnCheckComments.getSelection());
        deprecatedCheckBox.setEnabled(btnCheckComments.getSelection());
        desc2CheckBox.setEnabled(btnCheckComments.getSelection());
        seeCheckBox.setEnabled(btnCheckComments.getSelection());
        malformedTagCheckBox.setEnabled(btnCheckComments.getSelection());
        btnCommentRequiredIn.setEnabled(btnCheckComments.getSelection());
        comboMeanSev.setEnabled(btnCheckComments.getSelection());
        combohtmlFLSev.setEnabled(btnCheckComments.getSelection());
        comboHtmlImbalance.setEnabled(btnCheckComments.getSelection());
        comboParamSev.setEnabled(btnCheckComments.getSelection());
        comboThrowsSev.setEnabled(btnCheckComments.getSelection());
        comboCommSev.setEnabled(btnCheckComments.getSelection());
        comboReturnSev.setEnabled(btnCheckComments.getSelection());
        comboDeprecatedSev.setEnabled(btnCheckComments.getSelection());
        comboDeprecatedSev2.setEnabled(btnCheckComments.getSelection());
        comboSeeSev.setEnabled(btnCheckComments.getSelection());
        comboMalformedTag.setEnabled(btnCheckComments.getSelection());
        btnAdd.setEnabled(btnCheckComments.getSelection());
        btnEdit.setEnabled(btnCheckComments.getSelection());
        btnDelete.setEnabled(btnCheckComments.getSelection());
        table_1.setEnabled(btnCheckComments.getSelection());

        SelectionListener selectionListener = new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                hasChanged = true;
            }
        };
        comboCommSev.addSelectionListener(selectionListener);
        comboDeprecatedSev.addSelectionListener(selectionListener);
        comboDeprecatedSev2.addSelectionListener(selectionListener);
        combohtmlFLSev.addSelectionListener(selectionListener);
        comboHtmlImbalance.addSelectionListener(selectionListener);
        comboMalformedTag.addSelectionListener(selectionListener);
        comboMeanSev.addSelectionListener(selectionListener);
        comboParamSev.addSelectionListener(selectionListener);
        comboReturnSev.addSelectionListener(selectionListener);
        comboSeeSev.addSelectionListener(selectionListener);
        comboThrowsSev.addSelectionListener(selectionListener);

        m_bindingContext = initDataBindings();

        return theContainer;
    }

    /**
     * @see org.eclipse.jface.preference.PreferencePage#performOk()
     */
    @Override
    public boolean performOk() {
        // save javadoc processing state
        store.setValue(AdamStoreUtil.JAVADOC_CHECK, !btnCheckComments.getSelection());

        try {
            if (hasChanged) {
                // save data to XML
                // setting values for meaningless comments
                comments.getCheckMeaninglessComment().setValue(meaninglessCheckBox.getSelection());
                comments.getCheckMeaninglessComment().setSeverity(
                    Severity.fromValue(comboMeanSev.getItem(comboMeanSev.getSelectionIndex())));

                // setting values for comments required
                comments.getCommentRequired().setValue(btnCommentRequiredIn.getSelection());
                comments.getCommentRequired().setSeverity(
                    Severity.fromValue(comboCommSev.getItem(comboCommSev.getSelectionIndex())));

                // setting values for invalid firstline in HTML
                comments.getInvalidHtmlInFirstline().setValue(htmlFLCheckBox.getSelection());
                comments.getInvalidHtmlInFirstline().setSeverity(
                    Severity.fromValue(combohtmlFLSev.getItem(combohtmlFLSev.getSelectionIndex())));

                // setting values for HTML imbalance
                comments.getInvalidHtmlInFirstline().setValue(htmlImbalanceCheckBox.getSelection());
                comments.getInvalidHtmlInFirstline().setSeverity(
                    Severity.fromValue(comboHtmlImbalance.getItem(comboHtmlImbalance.getSelectionIndex())));

                // setting values for @param description checks
                comments.getParamTagRequireDescriptions().setValue(paramCheckBox.getSelection());
                comments.getParamTagRequireDescriptions().setSeverity(
                    Severity.fromValue(comboParamSev.getItem(comboParamSev.getSelectionIndex())));

                // setting values for throws check
                comments.getThrowsTagRequireDescriptions().setValue(throwsCheckBox.getSelection());
                comments.getThrowsTagRequireDescriptions().setSeverity(
                    Severity.fromValue(comboThrowsSev.getItem(comboThrowsSev.getSelectionIndex())));

                // setting values for return description checks
                comments.getReturnTagRequireDescription().setValue(returnCheckBox.getSelection());
                comments.getReturnTagRequireDescription().setSeverity(
                    Severity.fromValue(comboReturnSev.getItem(comboReturnSev.getSelectionIndex())));

                // setting values for @deprecated description checks
                comments.getDeprecatedTagRequireDescriptions().setValue(deprecatedCheckBox.getSelection());
                comments.getDeprecatedTagRequireDescriptions().setSeverity(
                    Severity.fromValue(comboDeprecatedSev.getItem(comboDeprecatedSev.getSelectionIndex())));

                // setting values for @deprecated annotation tag check
                comments.getDeprecatedAnnotationRequiresTag().setValue(desc2CheckBox.getSelection());
                comments.getDeprecatedAnnotationRequiresTag().setSeverity(
                    Severity.fromValue(comboDeprecatedSev2.getItem(comboDeprecatedSev2.getSelectionIndex())));

                // setting values for @see validity check
                comments.getInvalidSeeTag().setValue(seeCheckBox.getSelection());
                comments.getInvalidSeeTag().setSeverity(
                    Severity.fromValue(comboSeeSev.getItem(comboSeeSev.getSelectionIndex())));

                // setting values for malformed tags check
                comments.getInvalidSeeTag().setValue(malformedTagCheckBox.getSelection());
                comments.getInvalidSeeTag().setSeverity(
                    Severity.fromValue(comboMalformedTag.getItem(comboMalformedTag.getSelectionIndex())));

                // setting values for comment warnings
                warnings.clear();
                for (WarningBean abean:commentWarning.getWarnings()) {
                    CommentRegex commentRegex = new CommentRegex();
                    commentRegex.setSeverity(Severity.fromValue(abean.getWarningSeverity()));
                    commentRegex.setValue(abean.getWarningName());
                    warnings.add(commentRegex);
                }

                DocfactoCorePlugin.getXmlConfig().save();
                new AddMarkersToResources().refresh();
                hasChanged = false;
            }
        }
        catch (DocfactoException e) {
            DocfactoAdamPlugin.logException("Could not save these preferences to the XML", e);
        }
        return true;
    }

    /**
     * Initialize the preference page.
     * 
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    @Override
    public void init(IWorkbench workbench) {
        // Initialize the preference page
    }

    /**
     * Initialize data bindings
     * 
     * @return <code>DataBindingContext</code>
     */
    protected DataBindingContext initDataBindings() {
        DataBindingContext bindingContext = new DataBindingContext();
        //
        ObservableListContentProvider listContentProvider =
            new ObservableListContentProvider();
        IObservableMap[] observeMaps =
            BeansObservables.observeMaps(
                listContentProvider.getKnownElements(), WarningBean.class,
                new String[] {"warningName","warningSeverity"});
        tableViewer
            .setLabelProvider(new ObservableMapLabelProvider(observeMaps));
        tableViewer.setContentProvider(listContentProvider);
        //
        IObservableList warningsCommentWarningObserveList =
            BeanProperties.list("warnings").observe(commentWarning);
        tableViewer.setInput(warningsCommentWarningObserveList);
        //
        return bindingContext;
    }

}