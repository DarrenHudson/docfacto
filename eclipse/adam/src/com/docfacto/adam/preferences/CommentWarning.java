package com.docfacto.adam.preferences;

import java.util.ArrayList;
import java.util.List;

public class CommentWarning extends AbstractModelObject {
	private final List<WarningBean> warnings = new ArrayList<WarningBean>();
	
	
	
	public void addWarning(WarningBean warning) {
			warnings.add(warning);
		firePropertyChange("warnings", null, warnings);
		
		
	}


	public void removeWarning(WarningBean warning) {
		warnings.remove(warning);
		firePropertyChange("warnings", null, warnings);
	}
	
	
	public List<WarningBean> getWarnings() {
		return warnings;
	}
	


}
