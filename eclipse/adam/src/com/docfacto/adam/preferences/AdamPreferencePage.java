package com.docfacto.adam.preferences;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.map.IObservableMap;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.databinding.viewers.ObservableMapLabelProvider;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.PreferencesUtil;

import com.docfacto.adam.plugin.AddMarkersToResources;
import com.docfacto.adam.plugin.DocfactoAdamPlugin;
import com.docfacto.config.generated.Doc;
import com.docfacto.config.generated.Severity;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.core.XmlChangeListener;

/**
 * Creates a preference page to allow modification of Docfacto.xml
 * 
 * @author pemi - modified Aug 30, 2013 3:08:49 PM
 * 
 * @since 2.1
 */
public class AdamPreferencePage extends PreferencePage implements IWorkbenchPreferencePage,XmlChangeListener {

    /*
     * This class needs a rework. 1. There might be a number of unnecessary data binding objects which needs to be
     * checked for redundancy
     * 
     * 2. The XML elements need to be programmatically generated.
     * 
     * 3. A general clean up is required.
     * 
     * 4. Clear unused values in preference store
     */

    private DataBindingContext m_bindingContext;
    private TagProvider tagProvider;
    private ScopeGroup sgroup;
    private ScopeGroups s_groups;
    private String[] scopeItemsArray;
    private XMLtoAdamTag defTag;
    private boolean coreAvailable = false;
    private Table scopeTable;
    private Table tagTable;
    private TableViewer scopeTableViewer;
    private TableViewer tagTableViewer;
    private Button btnRemoveTag;
    private Button btnEditTag;
    private Button btnNewTag;
    private Group methodSettings;
    private Label lblScopes;
    protected String selectedGroup = "";
    private Group enumSettings;
    private Button btnAllowNoComment;
    private Button btnEnumConstantsRequire;
    private Button btnNoCommentsIn;
    private Group packageSettings;
    private ToolBar toolBar;
    private ToolItem tltmNew;
    private ToolItem tltmDelete;
    private CommentWarning docWarning;
    private Table table;
    private TableColumn tblclmnNewColumn_1;
    private TableViewerColumn tableViewerColumn;
    private TableColumn tblclmnNewColumn_2;
    private TableViewerColumn tableViewerColumn_1;
    private TableViewer tableViewer;
    private Composite theContainer;
    private boolean hasChanged = false;

    /**
     * Create the preference page.
     */
    public AdamPreferencePage() {
        scopeItemsArray = AdamStoreUtil.SCOPE_ARRAY;
        defTag = new XMLtoAdamTag();
        DocfactoCorePlugin.getDefault().addXmlChangeListener(this);
    }

    @Override
    public void xmlChanged() {
        // Do something...
    }

    @Override
    public void xmlReloaded() {
        /*
         * This is a pretty inefficient hack but its too messy to figure out a method otherwise, all the code is in
         * createcontrol so to refresh the contents i have to resort to recreating the whole control.
         */
        if (theContainer!=null) {
            // Recreating is only neccessary if the page was created and seen, if thecontainer is null, this preference
            // was
            // never opened and therefore does not need to be recreated.
            this.defTag = new XMLtoAdamTag();
            this.tagProvider = null;
            this.hasChanged = false;

            Composite parent = theContainer.getParent().getParent();
            AdamPreferencePage.super.createControl(parent);
        }
    }

    @Override
    public void dispose() {
        DocfactoCorePlugin.getDefault().removeXmlChangeListener(this);
        super.dispose();
    }

    /**
     * Create contents of the preference page.
     * 
     * @param parent
     */
    @Override
    public Control createContents(Composite parent) {
        theContainer = new Composite(parent, SWT.NULL);
        theContainer.setLayout(new FormLayout());

        sgroup = new ScopeGroup();
        s_groups = new ScopeGroups();

        // Check if internal XML is being used
        if (DocfactoCorePlugin.getDefault().isUsingInternalXML()) {
            // display error message above page
            setMessage("Using default configuration settings", WARNING);

            Link link = new Link(theContainer, SWT.NONE);
            link.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    PreferencesUtil.createPreferenceDialogOn(null, "com.docfacto.core.preference.Docfacto", null, null)
                        .open();
                }
            });

            link.setText("<a>Set your configuration file</a>");

            return theContainer;

        }
        // Check if 'Docfacto.xml' has been validated
        else if (!DocfactoCorePlugin.getDefault().getPreferenceStore().getBoolean(DocfactoCorePlugin.KEY_VALIDATED)) {
            // display error message above page
            setErrorMessage("A valid docfacto configuration file is required");

            Label lblTagsInSelected = new Label(theContainer, SWT.WRAP|SWT.CENTER);
            lblTagsInSelected.setBounds(5, 5, 300, 150);
            lblTagsInSelected.setText(DocfactoCorePlugin.getDefault().getPreferenceStore()
                .getString(DocfactoCorePlugin.VALIDATION_ERROR)+
                "\n\nYou will have to relaunch this window before your changes take effect");
            return theContainer;

        }
        else {
            coreAvailable = true;
        }

        tagProvider = defTag.getTagsFromXML();
        putTagsInScopes(tagProvider, true);

        Link link = new Link(theContainer, SWT.NONE);
        link.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                PreferencesUtil.createPreferenceDialogOn(null, "org.eclipse.ui.editors.preferencePages.Annotations",
                    null, null).open();
            }
        });
        FormData fd_link = new FormData();
        fd_link.top = new FormAttachment(100, -15);
        fd_link.bottom = new FormAttachment(100);
        fd_link.left = new FormAttachment(0);
        link.setLayoutData(fd_link);
        link.setText("<a>Adam marker appearance settings</a>");

        Composite composite = new Composite(theContainer, SWT.NONE);
        FormData fd_composite = new FormData();
        fd_composite.top = new FormAttachment(0, 39);
        fd_composite.right = new FormAttachment(100);
        composite.setLayoutData(fd_composite);
        composite.setLayout(new FormLayout());

        btnNewTag = new Button(composite, SWT.NONE);
        btnNewTag.addSelectionListener(new SelectionAdapter() {

            // New Item Button: on click
            @Override
            public void widgetSelected(final SelectionEvent e) {
                final IStructuredSelection scopeSelection = (IStructuredSelection)scopeTableViewer.getSelection();
                ScopeGroup scopeName = (ScopeGroup)scopeSelection.getFirstElement();

                final AdamTag newTag = new AdamTag("", scopeName.getName(), "", "true");

                final TagDialog dialog = new TagDialog(getShell(), newTag, false);
                if (dialog.open()==Window.OK) {

                    ArrayList<ScopeGroup> ss = s_groups.getScopes();
                    boolean scopeFound = false;
                    for (int sg = 0;sg<ss.size();sg++) {
                        if (ss.get(sg).getName().equals(newTag.getScope())) {
                            ss.get(sg).addAdamTag(newTag);
                            scopeFound = true;
                        }

                    }
                    if (!scopeFound) {
                        sgroup = new ScopeGroup(newTag.getScope());
                        sgroup.addAdamTag(newTag);
                        s_groups.addScope(sgroup);
                    }
                    tagProvider.addTag(newTag);
                    m_bindingContext.updateModels();
                    tagTableViewer.refresh();
                    hasChanged = true;
                }

            }
        });
        FormData fd_btnNewTag = new FormData();
        fd_btnNewTag.right = new FormAttachment(0, 104);
        fd_btnNewTag.top = new FormAttachment(0);
        fd_btnNewTag.left = new FormAttachment(0);
        btnNewTag.setLayoutData(fd_btnNewTag);
        btnNewTag.setText("New");

        btnEditTag = new Button(composite, SWT.NONE);
        FormData fd_btnEditTag = new FormData();
        fd_btnEditTag.right = new FormAttachment(0, 104);
        fd_btnEditTag.top = new FormAttachment(0, 23);
        fd_btnEditTag.left = new FormAttachment(0);
        btnEditTag.setLayoutData(fd_btnEditTag);
        btnEditTag.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                final IStructuredSelection selection = (IStructuredSelection)tagTableViewer
                    .getSelection();
                final AdamTag seltag = (AdamTag)selection.getFirstElement();
                final TagDialog dialog = new TagDialog(getShell(), seltag, true);
                if (dialog.open()==Window.OK) {
                    m_bindingContext.updateModels();
                    tagTableViewer.refresh();
                    hasChanged = true;

                }
            }
        });
        btnEditTag.setEnabled(false);
        btnEditTag.setText("Edit");

        btnRemoveTag = new Button(composite, SWT.NONE);
        FormData fd_btnDelete = new FormData();
        fd_btnDelete.right = new FormAttachment(0, 104);
        fd_btnDelete.top = new FormAttachment(0, 45);
        fd_btnDelete.left = new FormAttachment(0);
        btnRemoveTag.setLayoutData(fd_btnDelete);
        btnRemoveTag.addSelectionListener(new SelectionAdapter() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                IStructuredSelection groupSelection = (IStructuredSelection)scopeTableViewer.getSelection();
                IStructuredSelection tagSelection = (IStructuredSelection)tagTableViewer.getSelection();
                ScopeGroup group = (ScopeGroup)groupSelection.getFirstElement();
                AdamTag remTag = (AdamTag)tagSelection.getFirstElement();
                boolean confirm =
                    MessageDialog.openConfirm(getShell(), "Confirm Delete", "Delete tag '"+remTag.getName()+"' from "+
                        remTag.getScope()+"?");
                if (confirm) {
                    group.removeAdamTag(remTag);
                    tagProvider.removeTag(remTag);
                    m_bindingContext.updateModels();
                    tagTableViewer.refresh();
                    hasChanged = true;

                }
            }

        });
        btnRemoveTag.setEnabled(false);
        btnRemoveTag.setText("Delete");

        Composite composite_1 = new Composite(theContainer, SWT.NONE);
        fd_composite.bottom = new FormAttachment(composite_1, 0, SWT.BOTTOM);
        FormData fd_composite_1 = new FormData();
        fd_composite_1.left = new FormAttachment(0, 10);
        fd_composite_1.right = new FormAttachment(composite, -6);
        fd_composite_1.bottom = new FormAttachment(link, -129);
        fd_composite_1.top = new FormAttachment(0, 20);
        composite_1.setLayoutData(fd_composite_1);
        composite_1.setLayout(new FormLayout());

        lblScopes = new Label(composite_1, SWT.NONE);
        FormData fd_lblScopes = new FormData();
        fd_lblScopes.right = new FormAttachment(0, 59);
        fd_lblScopes.top = new FormAttachment(0);
        fd_lblScopes.left = new FormAttachment(0);
        lblScopes.setLayoutData(fd_lblScopes);
        lblScopes.setText("Scopes");

        Label lblTagsInSelected = new Label(composite_1, SWT.NONE);
        FormData fd_lblTagsInSelected = new FormData();
        fd_lblTagsInSelected.left = new FormAttachment(lblScopes, 28);
        fd_lblTagsInSelected.right = new FormAttachment(100, -14);
        fd_lblTagsInSelected.top = new FormAttachment(0);
        lblTagsInSelected.setLayoutData(fd_lblTagsInSelected);
        lblTagsInSelected.setText("Tags in selected scope");

        scopeTableViewer = new TableViewer(composite_1, SWT.BORDER
            |SWT.FULL_SELECTION);

        scopeTable = scopeTableViewer.getTable();
        FormData fd_table = new FormData();
        fd_table.left = new FormAttachment(0);
        fd_table.top = new FormAttachment(lblScopes, 6);
        fd_table.bottom = new FormAttachment(100);
        scopeTable.setLayoutData(fd_table);

        tagTableViewer = new TableViewer(composite_1, SWT.BORDER
            |SWT.FULL_SELECTION);

        // Disable delete and edit buttons when no item is selected
        tagTableViewer.addPostSelectionChangedListener(new ISelectionChangedListener() {
            @Override
            public void selectionChanged(SelectionChangedEvent event) {
                if (tagTableViewer.getSelection().isEmpty()) {
                    btnRemoveTag.setEnabled(false);
                    btnEditTag.setEnabled(false);
                }
            }

        });

        // Enable delete and edit buttons when an item is selected
        tagTableViewer.addSelectionChangedListener(new ISelectionChangedListener() {
            @Override
            public void selectionChanged(
            final SelectionChangedEvent event) {
                btnRemoveTag.setEnabled(true);
                btnEditTag.setEnabled(true);
            }
        });

        // Edit tags when item is double clicked
        tagTableViewer.addDoubleClickListener(new IDoubleClickListener() {

            @Override
            public void doubleClick(final DoubleClickEvent event) {
                final IStructuredSelection selection = (IStructuredSelection)tagTableViewer.getSelection();
                final AdamTag seltag = (AdamTag)selection.getFirstElement();
                final TagDialog dialog = new TagDialog(getShell(), seltag, true);
                if (dialog.open()==Window.OK) {
                    m_bindingContext.updateModels();
                    tagTableViewer.refresh();
                    hasChanged = true;

                }

            }

        });

        tagTable = tagTableViewer.getTable();
        tagTable.setLinesVisible(true);
        tagTable.setHeaderVisible(true);
        fd_table.right = new FormAttachment(tagTable, -7);
        FormData fd_table_1 = new FormData();
        fd_table_1.left = new FormAttachment(lblTagsInSelected, 7, SWT.LEFT);
        fd_table_1.top = new FormAttachment(lblTagsInSelected, 6);
        fd_table_1.bottom = new FormAttachment(100);
        fd_table_1.right = new FormAttachment(100);
        tagTable.setLayoutData(fd_table_1);
        // Insert 'Name' column in table of tags
        TableColumn tblclmnName = new TableColumn(tagTable, SWT.NONE);
        tblclmnName.setWidth(50);
        tblclmnName.setText("Name");

        // Insert 'Severity' column in table of tags
        TableColumn tblclmnSeverity = new TableColumn(tagTable, SWT.NONE);
        tblclmnSeverity.setWidth(55);
        tblclmnSeverity.setText("Severity");

        // Insert 'Enable' column in table of tags
        TableColumn tblclmnNewColumn = new TableColumn(tagTable, SWT.NONE);
        tblclmnNewColumn.setWidth(55);
        tblclmnNewColumn.setText("Enable");

        // setting values for package files
        docWarning = new CommentWarning();
        if (tagProvider.getRequiredDocumentation()!=null) {

            for (Doc doc:tagProvider.getRequiredDocumentation().getDocs()) {
                WarningBean warnBean = new WarningBean(doc.getValue());
                warnBean.setWarningSeverity(doc.getSeverity().value());
                docWarning.addWarning(warnBean);

            }
        }

        packageSettings = new Group(theContainer, SWT.NONE);
        packageSettings.setText("package documentation files");
        FormData fd_packageSettings = new FormData();
        fd_packageSettings.bottom = new FormAttachment(link, -21);
        fd_packageSettings.top = new FormAttachment(composite_1, 16);
        fd_packageSettings.left = new FormAttachment(composite_1, 0, SWT.LEFT);
        fd_packageSettings.right = new FormAttachment(composite_1, 0, SWT.RIGHT);
        packageSettings.setLayoutData(fd_packageSettings);
        packageSettings.setVisible(false);

        toolBar = new ToolBar(packageSettings, SWT.FLAT|SWT.VERTICAL);
        toolBar.setBounds(238, 10, 26, 55);

        tltmNew = new ToolItem(toolBar, SWT.PUSH);
        tltmNew.setImage(PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(ISharedImages.IMG_OBJ_ADD)
            .createImage());
        tltmNew.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                final WarningBean warningItem = new WarningBean("");
                final CmntWarnDialog dialog = new CmntWarnDialog(getShell(), warningItem);
                dialog.setKey("file name");
                dialog.setTitle("add file requirement");
                if (dialog.open()==Window.OK) {
                    docWarning.addWarning(warningItem);
                    m_bindingContext.updateModels();
                    tableViewer.refresh();
                    hasChanged = true;

                }

            }
        });

        tltmDelete = new ToolItem(toolBar, SWT.PUSH);
        tltmDelete.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                final IStructuredSelection selection = (IStructuredSelection)tableViewer.getSelection();
                final WarningBean warningItem = (WarningBean)selection.getFirstElement();
                new CmntWarnDialog(getShell(), warningItem);
                boolean confirm =
                    MessageDialog.openConfirm(getShell(), "Confirm Delete", "Remove documentation requirement for '"+
                        warningItem.getWarningName()+"'?");
                if (confirm) {
                    docWarning.removeWarning(warningItem);
                    m_bindingContext.updateModels();
                    tableViewer.refresh();
                    hasChanged = true;
                }
            }

        });
        tltmDelete.setImage(PlatformUI.getWorkbench().getSharedImages().
            getImageDescriptor(ISharedImages.IMG_ELCL_REMOVE).createImage());
        tltmDelete.setEnabled(false);

        tableViewer = new TableViewer(packageSettings, SWT.BORDER|SWT.FULL_SELECTION);
        tableViewer.addDoubleClickListener(new IDoubleClickListener() {
            public void doubleClick(DoubleClickEvent event) {
                final IStructuredSelection selection = (IStructuredSelection)event.getSelection();
                final WarningBean warningItem = (WarningBean)selection.getFirstElement();
                final CmntWarnDialog dialog = new CmntWarnDialog(getShell(), warningItem);
                dialog.setKey("file name");
                dialog.setTitle("edit file requirement");
                if (dialog.open()==Window.OK) {
                    m_bindingContext.updateModels();
                    tableViewer.refresh();
                    hasChanged = true;
                }
            }
        });

        // Disable delete and edit buttons when no item is selected
        tableViewer.addPostSelectionChangedListener(new ISelectionChangedListener() {
            @Override
            public void selectionChanged(SelectionChangedEvent event) {
                if (tableViewer.getSelection().isEmpty()) {
                    tltmDelete.setEnabled(false);
                }
            }

        });

        // Enable delete and edit buttons when an item is selected
        tableViewer.addSelectionChangedListener(new ISelectionChangedListener() {
            @Override
            public void selectionChanged(
            final SelectionChangedEvent event) {
                tltmDelete.setEnabled(true);
            }
        });

        table = tableViewer.getTable();
        table.setLinesVisible(true);
        table.setHeaderVisible(true);
        table.setBounds(10, 10, 229, 55);

        tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
        tblclmnNewColumn_1 = tableViewerColumn.getColumn();
        tblclmnNewColumn_1.setWidth(100);
        tblclmnNewColumn_1.setText("File");

        tableViewerColumn_1 = new TableViewerColumn(tableViewer, SWT.NONE);
        tblclmnNewColumn_2 = tableViewerColumn_1.getColumn();
        tblclmnNewColumn_2.setWidth(100);
        tblclmnNewColumn_2.setText("Severity");

        methodSettings = new Group(theContainer, SWT.NONE);
        methodSettings.setText("Additional settings for methods");
        FormData fd_grpAdditionalSettings = new FormData();
        fd_grpAdditionalSettings.bottom = new FormAttachment(link, -21);
        fd_grpAdditionalSettings.top = new FormAttachment(composite_1, 16);
        fd_grpAdditionalSettings.left = new FormAttachment(composite_1, 0,
            SWT.LEFT);
        fd_grpAdditionalSettings.right = new FormAttachment(composite_1, 0,
            SWT.RIGHT);
        methodSettings.setLayoutData(fd_grpAdditionalSettings);
        methodSettings.setVisible(false);

        enumSettings = new Group(theContainer, SWT.NONE);
        enumSettings.setText("Additional settings for enums");
        FormData fd_enumSettings = new FormData();
        fd_enumSettings.bottom = new FormAttachment(link, -21);
        fd_enumSettings.top = new FormAttachment(composite_1, 16);
        fd_enumSettings.left = new FormAttachment(composite_1, 0, SWT.LEFT);
        fd_enumSettings.right = new FormAttachment(composite_1, 0, SWT.RIGHT);
        enumSettings.setLayoutData(fd_enumSettings);
        enumSettings.setVisible(false);

        btnEnumConstantsRequire = new Button(enumSettings, SWT.CHECK);
        btnEnumConstantsRequire.setSelection(tagProvider.getEnumConstantReq());
        btnEnumConstantsRequire.setBounds(0, 10, 244, 18);
        btnEnumConstantsRequire.setText("Enum constants require documentation");

        btnNoCommentsIn = new Button(methodSettings, SWT.CHECK);
        btnNoCommentsIn
            .setSelection(tagProvider.getInterfaceMethodsCommented());
        btnNoCommentsIn.setBounds(0, 10, 244, 18);
        btnNoCommentsIn.setText("allow uncommented methods in interfaces");

        btnAllowNoComment = new Button(methodSettings, SWT.CHECK);
        btnAllowNoComment.setSelection(tagProvider.getCommentGetSet());
        btnAllowNoComment.setBounds(0, 30, 244, 18);
        btnAllowNoComment.setText("allow uncommented getters/setters");

        scopeTableViewer.addPostSelectionChangedListener(new ISelectionChangedListener() {
            @Override
            public void selectionChanged(SelectionChangedEvent event) {
                if (!scopeTableViewer.getSelection().isEmpty()) {
                    selectedGroup = scopeTable.getItem(scopeTableViewer.getTable().getSelectionIndex()).getText();

                    if (selectedGroup.equals(AdamStoreUtil.SCOPE_ARRAY[AdamStoreUtil.METHODS])) {
                        methodSettings.setVisible(true);

                    }
                    else {
                        methodSettings.setVisible(false);

                    }
                    if (selectedGroup.equals(AdamStoreUtil.SCOPE_ARRAY[AdamStoreUtil.ENUMS])) {
                        enumSettings.setVisible(true);

                    }
                    else {

                        enumSettings.setVisible(false);
                    }
                    if (selectedGroup.equals(AdamStoreUtil.SCOPE_ARRAY[AdamStoreUtil.PACKAGES])) {
                        packageSettings.setVisible(true);

                    }
                    else {
                        packageSettings.setVisible(false);

                    }
                }
            }

        });

        // initialize data binding
        m_bindingContext = initDataBindings();

        // Select the first item in scope table
        scopeTableViewer.setSelection(new StructuredSelection(s_groups
            .getScopes().get(0)));

        return theContainer;
    }

    private void putTagsInScopes(TagProvider tagProvider2,boolean b) {

        boolean duplicate = false;
        // get scopes of tags in TagProvider
        for (String tscope:scopeItemsArray) {

            for (ScopeGroup aScope:s_groups.getScopes()) {
                if (aScope.getName().equals(tscope)) {
                    duplicate = true;
                    sgroup = aScope;
                }

            }

            // add scope if non exists
            if (!duplicate) {
                sgroup = new ScopeGroup(tscope);
                s_groups.addScope(sgroup);

            }

            // store tags in their respective scope
            for (AdamTag aTag:tagProvider2.getTags()) {
                if (aTag.getScope().equals(tscope)) {
                    sgroup.addAdamTag(aTag);

                }
            }

        }

    }

    /**
     * Initialize the preference page.
     */
    @Override
    public void init(IWorkbench workbench) {
        // Initialize the preference page
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.preference.PreferencePage#performOk()
     */
    @Override
    public boolean performOk() {

        if (coreAvailable&&hasChanged) {
            // update the enum properties
            tagProvider.setEnumConstantReq(btnEnumConstantsRequire
                .getSelection());

            // update method properties
            tagProvider.setInterfaceMethodsCommented(btnNoCommentsIn
                .getSelection());
            tagProvider.setCommentGetSet(btnAllowNoComment.getSelection());

            // update package doc file properties
            List<Doc> newDocs = new ArrayList<Doc>();
            for (WarningBean warnbean:docWarning.getWarnings()) {

                Doc newDoc = new Doc();
                newDoc.setSeverity(Severity.fromValue(warnbean.getWarningSeverity()));
                newDoc.setValue(warnbean.getWarningName());
                newDocs.add(newDoc);

            }

            if (!newDocs.isEmpty()) {
                tagProvider.getRequiredDocumentation().getDocs().clear();
                tagProvider.getRequiredDocumentation().getDocs().addAll(newDocs);
            }

            defTag.saveTags(tagProvider);

            // save tags to store
            IPreferenceStore store = DocfactoAdamPlugin.getDefault().getPreferenceStore();

            store.setValue(AdamStoreUtil.TAGCOUNT, tagProvider.getTags().size());

            for (final AdamTag atag:tagProvider.getTags()) {
                store.setValue(atag.getName()+atag.getScope(), atag.getEnable());
                store.needsSaving();
            }
            new AddMarkersToResources().refresh();
            hasChanged = false;
        }

        return super.performOk();

    }

    protected DataBindingContext initDataBindings() {
        DataBindingContext bindingContext = new DataBindingContext();
        //
        ObservableListContentProvider listContentProvider = new ObservableListContentProvider();
        IObservableMap observeMap =
            BeansObservables.observeMap(listContentProvider.getKnownElements(), ScopeGroup.class, "name");
        scopeTableViewer.setLabelProvider(new ObservableMapLabelProvider(observeMap));
        scopeTableViewer.setContentProvider(listContentProvider);
        //
        IObservableList scopesS_groupObserveList = BeanProperties.list("scopes").observe(s_groups);
        scopeTableViewer.setInput(scopesS_groupObserveList);
        //
        ObservableListContentProvider listContentProvider_1 = new ObservableListContentProvider();
        IObservableMap[] observeMaps =
            BeansObservables.observeMaps(listContentProvider_1.getKnownElements(), AdamTag.class, new String[] {"name",
                "severity","enable"});
        tagTableViewer.setLabelProvider(new ObservableMapLabelProvider(observeMaps));
        tagTableViewer.setContentProvider(listContentProvider_1);
        //
        IObservableValue observeSingleSelectionScopeTableViewer =
            ViewerProperties.singleSelection().observe(scopeTableViewer);
        IObservableList scopeTableViewerAdamTagsObserveDetailList =
            BeanProperties.list(ScopeGroup.class, "adamTags", AdamTag.class).observeDetail(
                observeSingleSelectionScopeTableViewer);
        tagTableViewer.setInput(scopeTableViewerAdamTagsObserveDetailList);
        //
        ObservableListContentProvider listContentProvider_2 = new ObservableListContentProvider();
        IObservableMap[] observeMaps_1 =
            BeansObservables.observeMaps(listContentProvider_2.getKnownElements(), WarningBean.class, new String[] {
                "warningName","warningSeverity"});
        tableViewer.setLabelProvider(new ObservableMapLabelProvider(observeMaps_1));
        tableViewer.setContentProvider(listContentProvider_2);
        //
        IObservableList warningsDocWarningObserveList = BeanProperties.list("warnings").observe(docWarning);
        tableViewer.setInput(warningsDocWarningObserveList);
        //
        return bindingContext;
    }

}
