package com.docfacto.adam.report;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import org.eclipse.core.resources.IResource;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.NodeFinder;

import com.docfacto.adam.AdamConstants;
import com.docfacto.adam.AdamInvoker;
import com.docfacto.adam.AdamOutputProcessor;
import com.docfacto.adam.plugin.ClearAdamMarkers;
import com.docfacto.adam.plugin.DocfactoAdamPlugin;
import com.docfacto.adam.plugin.markers.AdamMarkerWrapper;
import com.docfacto.adam.plugin.markers.MarkerCreator;
import com.docfacto.common.DocfactoException;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.core.utils.ASTUtils;
import com.docfacto.output.generated.Result;

/**
 * Generates an adam report for java resources
 * 
 * @author pemi - created Mar 21, 2013 12:20:21 PM
 * 
 * @since 2.2
 */
public class AdamReporter implements IAdamReporter {

    /**
     * Used in the AST Report
     */
    private ASTNode theFoundNode;
    
    /**
     * Construct new Adam Reporter
     * 
     * @since 2.4
     */
    public AdamReporter() {
    }

    /**
     * @see com.docfacto.adam.report.IAdamReporter#getProjectReport(org.eclipse.jdt.core.IJavaProject)
     */
    @Override
    public AdamOutputProcessor getProjectReport(IJavaProject project) {

        AdamOutputProcessor projectProcessor = null;

        try {
            projectProcessor = new AdamOutputProcessor();

            IPackageFragment[] fragments = project.getPackageFragments();
            for (IPackageFragment packageFrag:fragments) {
                if (packageFrag.getKind()==IPackageFragmentRoot.K_SOURCE) {
                    getPackageReport(packageFrag,false);
                }
            }
        }
        catch (DocfactoException ex) {
            DocfactoAdamPlugin.logException(
                "AdamReporter: Unable to generate AdamOutputProcessor "
                    +ex.getMessage(),ex);
        }
        catch (JavaModelException ex) {
            DocfactoAdamPlugin.logException("AdamReporter: getProjectReport: "
                +ex.getMessage(),ex);
        }

        return projectProcessor;
    }

    /**
     * @see com.docfacto.adam.report.IAdamReporter#getPackageReport(org.eclipse.jdt.core.IPackageFragment,
     * boolean)
     */
    @Override
    public AdamOutputProcessor
    getPackageReport(IPackageFragment packageFragment,boolean recursive) {

        AdamOutputProcessor packageProcessor = null;

        try {
            packageProcessor = new AdamOutputProcessor();

            if (recursive) {
                IPackageFragment[] fragments =
                    packageFragment.getJavaProject().getPackageFragments();
                for (IPackageFragment packageFrag:fragments) {
                    if (packageFrag.getElementName().startsWith(
                        packageFragment.getElementName())) {

                        getPackageReport(packageFrag,false);
                    }
                }
            }

            else if (packageFragment.hasChildren()) {
                ICompilationUnit[] arrayCU = packageFragment
                    .getCompilationUnits();

                for (ICompilationUnit cu:arrayCU) {
                    getCompilationUnitReport(cu);
                }
            }
        }
        catch (JavaModelException ex) {
            DocfactoAdamPlugin.logException(
                "JavaModelException: AdamReporter: getPackageReport",ex);
        }
        catch (DocfactoException ex) {
            DocfactoAdamPlugin.logException(
                "AdamReporter: unable to create AdamOutputProcessor",ex);
        }

        return packageProcessor;
    }

    /**
     * @see com.docfacto.adam.report.IAdamReporter#getCompilationUnitReport(org.eclipse.jdt.core.ICompilationUnit)
     */
    @Override
    public AdamOutputProcessor getCompilationUnitReport(ICompilationUnit cu) {

        AdamOutputProcessor output = null;

        // Do nothing if ICompilationUnit no longer exists
        if (!cu.exists()) {
            return null;
        }
        
        try {
            // create a temp file from the ICompilationUnit
            String tempFile = cu.getElementName();
            File temp1 = File.createTempFile(tempFile,".java");
            File temp2 = new File(temp1.getParent(),tempFile);
            temp1.delete();

            PrintWriter p_out = new PrintWriter(temp2.getAbsolutePath());
            p_out.println(cu.getSource());
            p_out.close();

            String sourcepath = cu
                .getAncestor(IJavaElement.PACKAGE_FRAGMENT_ROOT)
                .getResource().getLocation().toString();

            AdamInvoker invoker = new AdamInvoker();

            // allow this thread to be cancelled when generating report
            if (!Thread.currentThread().isInterrupted()) {
                // get the output processor from AdamInvoker
                // Added the recommend here, but this will be an option
                output =
                    invoker.invoke(AdamConstants.RECOMMEND_OPTION,"-quiet",
                        "-sourcepath",sourcepath,"-config",
                        DocfactoCorePlugin.getDefault().getXMLConfigFile(),
                        temp2.getAbsolutePath());// Running in quiet
                // mode
            }
            temp2.delete();// delete the temp file
            IResource resource = cu.getResource();
            
            if (output!=null) {

                if (output.getResults()!=null) {

                    // We have new markers
                    ClearAdamMarkers.clearMarkers(resource);

                    // create markers from the result
                    for (final Result result:output.getResults()) {

                        try {
                            // get Compilation unit from ICompilationUnit
                            final CompilationUnit theCompilationUnit =
                                ASTUtils.getCompilationUnitFor(cu);

                            int elementPosition =
                                theCompilationUnit.getPosition(result
                                    .getPosition().getLine(),result.getPosition()
                                    .getColumn());

                            if(elementPosition == -1) {
                                theCompilationUnit.accept(new ASTVisitor() {

                                    /**
                                     * @see org.eclipse.jdt.core.dom.ASTVisitor#preVisit2(org.eclipse.jdt.core.dom.ASTNode)
                                     */
                                    @Override
                                    public boolean preVisit2(ASTNode node) {
                                        if(theCompilationUnit.getLineNumber(node.getStartPosition()) == result.getPosition().getLine()) {
                                            theFoundNode = node;
                                            return false;
                                        }
                                        return true;
                                    }
                                    
                                });
                            } else {
                                theFoundNode =
                                NodeFinder.perform(theCompilationUnit,
                                    elementPosition,1);
                            }
                            
                            //REMOVE:
//                            System.out.println("Result " +result.getRule().getMessage() + result
//                                    .getPosition().getLine()+" : "+result.getPosition()
//                                    .getColumn());

                            MarkerCreator
                                .createNewMarker(new AdamMarkerWrapper(
                                    resource,theFoundNode,result));
                        }
                        catch (Exception e) {
                            DocfactoCorePlugin.logException("Exception",e);
                        }
                    }
                }
            }
        }
        catch (JavaModelException ex) {
            DocfactoCorePlugin.logException("DocfactoException",ex);
        }
        catch (DocfactoException ex) {
            DocfactoCorePlugin.logException("DocfactoException",ex);
            if (ex.isInvalidConfigException()) {
                DocfactoAdamPlugin.getDefault().invalidConfig(ex);
            }
        }
        catch (IOException e) {
            DocfactoCorePlugin.logException("IOException",e);
        }

        return output;
    }

    /**
     * @see com.docfacto.adam.report.IAdamReporter#getSourceFolderReport(org.eclipse.jdt.core.IJavaElement)
     */
    @Override
    public AdamOutputProcessor getSourceFolderReport(IJavaElement javaElement) {

        IJavaProject project = javaElement.getJavaProject();
        AdamOutputProcessor sourcefolderProcessor = null;
        IPackageFragment[] fragments;
        try {
            sourcefolderProcessor = new AdamOutputProcessor();
            fragments = project.getPackageFragments();
            for (IPackageFragment packageFrag:fragments) {
                if (packageFrag.getKind()==IPackageFragmentRoot.K_SOURCE
                    &&packageFrag
                        .getAncestor(IJavaElement.PACKAGE_FRAGMENT_ROOT)
                        .getElementName()
                        .equals(javaElement.getElementName())) {

                    getPackageReport(packageFrag,false);
                }
            }
        }
        catch (JavaModelException e) {
            DocfactoAdamPlugin
                .logException(
                    "JavaModelException in AdamReporter: unable to process source folder",
                    e);

        }
        catch (DocfactoException e) {
            DocfactoAdamPlugin
                .logException(
                    "DocfactoException in AdamReporter: unable to process source folder",
                    e);
        }

        return sourcefolderProcessor;
    }

}
