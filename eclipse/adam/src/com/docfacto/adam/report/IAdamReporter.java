package com.docfacto.adam.report;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;

import com.docfacto.adam.AdamOutputProcessor;

/**
 * Interface describes methods which can be used to obtain data from an
 * underlying java resource.
 * 
 * @author pemi created 14/03/2013
 * @since 2.2
 * 
 */
public interface IAdamReporter {

    /**
     * Method returns <code>AdamOutputProcessor</code> with data containing an
     * Adam report of specified project
     * 
     * @param project <code>IJavaProject</code> of selected project
     * @return <code>AdamOutputProcessor</code>
     */
    public AdamOutputProcessor getProjectReport(IJavaProject project);

    /**
     * Method returns <code>AdamOutputProcessor</code> with data containing an
     * Adam report of specified folder
     * 
     * @param sourceFolder <code>IJavaElement</code> representing the source
     * folder
     * @return <code>AdamOutputProcessor</code>
     */
    public AdamOutputProcessor getSourceFolderReport(IJavaElement sourceFolder);

    /**
     * Method returns <code>AdamOutputProcessor</code> with data containing an
     * Adam report of specified package
     * 
     * @param packageFragment <code>IPackageFragment</code> of selected
     * packageFolder
     * @param recursive <code>true</code> if recursive processing of package is
     * required, else <code>false</code>
     * @return <code>AdamOutputProcessor</code>
     */
    public AdamOutputProcessor getPackageReport(
    IPackageFragment packageFragment,boolean recursive);

    /**
     * Method returns <code>AdamOutputProcessor</code> with data containing an
     * Adam report of specified compilation unit
     * 
     * @param cu <code>ICompilationUnit</code> of selected file
     * @return <code>AdamOutputProcessor</code>
     */
    public AdamOutputProcessor getCompilationUnitReport(ICompilationUnit cu);

}
