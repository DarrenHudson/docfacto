/**
 * 
 */
package com.docfacto.adam.report;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;

import com.docfacto.adam.AdamInvoker;
import com.docfacto.adam.AdamOutputProcessor;
import com.docfacto.adam.plugin.DocfactoAdamPlugin;
import com.docfacto.common.DocfactoException;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.output.generated.Result;
import com.docfacto.output.generated.Statistic;

/**
 * @author pemi - created Jun 6, 2013 1:55:59 PM
 * 
 * @since 2.2
 */
public class ReportWithoutMarkers implements IAdamReporter {

    private final String configPath;

    /**
     * Initialization
     */
    public ReportWithoutMarkers() {
        configPath = DocfactoCorePlugin.getDefault().getXMLConfigFile();
    }

    /**
     * @see com.docfacto.adam.report.IAdamReporter#getProjectReport(org.eclipse.jdt.core.IJavaProject)
     */
    @Override
    public AdamOutputProcessor getProjectReport(IJavaProject project) {
        AdamOutputProcessor projectProcessor = null;

        try {

            projectProcessor = new AdamOutputProcessor();

            IPackageFragment[] fragments = project.getPackageFragments();
            for (IPackageFragment packageFrag:fragments) {
                if (packageFrag.getKind()==IPackageFragmentRoot.K_SOURCE) {
                    addProcessor(projectProcessor,
                        getPackageReport(packageFrag,false));
                }
            }

        }
        catch (DocfactoException ex) {
            DocfactoAdamPlugin.logException(
                "AdamReporter: Unable to generate AdamOutputProcessor "
                    +ex.getMessage(),ex);
        }
        catch (JavaModelException ex) {
            DocfactoAdamPlugin.logException("AdamReporter: getProjectReport: "
                +ex.getMessage(),ex);
        }

        return projectProcessor;
    }

    /**
     * @param invokeArgs2
     * @return
     */
    private AdamOutputProcessor getAdamOutProcessor(List<String> invokeArgs2) {

        // System.out.println(invokeArgs2.toString());
        AdamOutputProcessor processor = null;
        if (!Thread.currentThread().isInterrupted()) {
            String[] a = invokeArgs2.toArray(new String[invokeArgs2.size()]);
            try {
                processor = new AdamInvoker().invoke(invokeArgs2.toArray(a));
            }

            catch (DocfactoException e) {
                DocfactoCorePlugin.logException(
                    "Error: getAdamOutputProcessor()",e);
                if (e.isInvalidConfigException()) {
                    DocfactoAdamPlugin.getDefault().invalidConfig(e);

                }

            }
        }
        return processor;
    }

    /**
     * @see com.docfacto.adam.report.IAdamReporter#getSourceFolderReport(org.eclipse.jdt.core.IJavaElement)
     */
    @Override
    public AdamOutputProcessor getSourceFolderReport(IJavaElement sourceFolder) {

        File file = sourceFolder.getResource().getLocation().toFile();
        File[] files = file.listFiles();
        StringBuilder contents = new StringBuilder();
        ArrayList<String> multiFiles = new ArrayList<String>();
        List<String> invokeArgs = new ArrayList<String>(5);
        for (File s:files) {
            try {
                if (!s.getName().startsWith(".")&&s.isDirectory()) {
                    if (contents.length()<=0) {
                        contents.append(s.getName());
                    }
                    else {
                        contents.append(":"+s.getName());
                    }
                }
                else if (s.isFile()&&s.getName().endsWith(".java")) {
                    multiFiles.add(s.getAbsolutePath());
                }
            }
            catch (Exception e) {
                DocfactoCorePlugin.logException("Exception",e);
            }

        }

        invokeArgs.add("-quiet");
        invokeArgs.add("-sourcepath");
        invokeArgs.add(sourceFolder.getResource().getLocation().toString());
        invokeArgs.add("-config");
        invokeArgs.add(configPath);

        // get the output processor from AdamInvoker
        if (contents.length()>0&&!multiFiles.isEmpty()) {
            invokeArgs.add("-subpackages");
            invokeArgs.add(contents.toString());
            invokeArgs.addAll(multiFiles);

        }
        else if (contents.length()==0&&!multiFiles.isEmpty()) {
            invokeArgs.addAll(multiFiles);

        }
        else if (contents.length()>0&&multiFiles.isEmpty()) {
            invokeArgs.add("-subpackages");
            invokeArgs.add(contents.toString());

        }

        return getAdamOutProcessor(invokeArgs);
    }

    /**
     * @see com.docfacto.adam.report.IAdamReporter#getPackageReport(org.eclipse.jdt.core.IPackageFragment,
     * boolean)
     */
    @Override
    public AdamOutputProcessor getPackageReport(
    IPackageFragment packageFragment,boolean recursive) {

        String sourcepath = packageFragment
            .getAncestor(IJavaElement.PACKAGE_FRAGMENT_ROOT)
            .getResource().getLocation().toString();

        List<String> invokeArgs = new ArrayList<String>(5);

        invokeArgs.add("-quiet");
        invokeArgs.add("-sourcepath");
        invokeArgs.add(sourcepath);
        invokeArgs.add("-config");
        invokeArgs.add(configPath);

        if (packageFragment.getElementName().length()>0) {
            if (recursive)
                invokeArgs.add("-subpackages");
            invokeArgs.add(packageFragment.getElementName());
        }

        else {

            // The following is an absurd situation that I would like to cater
            // for.
            // I suppose there are silly people in this world.
            // ...user is running report on a default package (i.e no package
            // name)
            ArrayList<String> elementList = new ArrayList<String>();
            try {
                IJavaElement[] children = packageFragment.getCompilationUnits();
                for (IJavaElement elem:children) {
                    elementList
                        .add(elem.getResource().getLocation().toString());
                }
            }
            catch (JavaModelException e) {
                DocfactoCorePlugin.logException("JavaModelException",e);

            }
            invokeArgs.addAll(elementList);

        }

        return getAdamOutProcessor(invokeArgs);
    }

    /**
     * @see com.docfacto.adam.report.IAdamReporter#getCompilationUnitReport(org.eclipse.jdt.core.ICompilationUnit)
     */
    @Override
    public AdamOutputProcessor getCompilationUnitReport(ICompilationUnit cu) {

        String sourcepath = cu
            .getAncestor(IJavaElement.PACKAGE_FRAGMENT_ROOT)
            .getResource().getLocation().toString();

        List<String> invokeArgs = new ArrayList<String>(5);

        invokeArgs.add("-quiet");
        invokeArgs.add("-sourcepath");
        invokeArgs.add(sourcepath);
        invokeArgs.add("-config");
        invokeArgs.add(configPath);
        invokeArgs.add(cu.getResource().getLocation().toString());

        return getAdamOutProcessor(invokeArgs);

    }

    /**
     * Adds <code>AdamOutputProcessor</code> to an existing one.
     * 
     * @param collector
     * @param newData
     * @since 2.2
     */
    private void addProcessor(AdamOutputProcessor collector,
    AdamOutputProcessor newData) {
        if (collector!=null&&newData!=null) {
            for (Statistic stat:newData.getStatistics()) {
                if (stat.getValue()>0) {
                    collector.addToStatistic(stat.getRule().getKey(),
                        stat.getValue());
                }
            }

            for (Result res:newData.getResults()) {
                collector.addResult(res);
            }
        }
    }

}
