package com.docfacto.adam.plugin;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.ui.IWorkingCopyManager;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.docfacto.adam.plugin.listeners.AdamElementChangedListener;
import com.docfacto.adam.plugin.listeners.AdamPartListener;
import com.docfacto.adam.plugin.listeners.AdamResourceListener;
import com.docfacto.adam.report.AdamReporter;

/**
 * A class for adding adam markers and listeners to resources
 * 
 * @author pemi - created 15/02/2013
 * @since 2.2
 * 
 */
public class AddMarkersToResources {

    private IWorkbench workBench;
    private IWorkbenchPage page;
    private IEditorInput editorInput2;
    IWorkbenchWindow[] windows = null;
    IFile currentFile = null;
    private AdamElementChangedListener listener;
    private AdamPartListener partlistener;
    private AdamResourceListener resourceListener;

    /**
     * Constructor creates new instances of the listeners
     */
    public AddMarkersToResources() {
        // create element listener
        listener = new AdamElementChangedListener();

        // create part listener
        partlistener = new AdamPartListener();

        // create the resource listener
        resourceListener = new AdamResourceListener();
    }

    /**
     * This method is used to turn off adam listeners
     * 
     * @since 2.2
     * 
     */
    public void turnOffListeners() {
        try {
            // remove markers from all open resources and turn off listeners
            final IEditorReference[] editorReferences =
                PlatformUI.getWorkbench()
                    .getActiveWorkbenchWindow().getActivePage()
                    .getEditorReferences();
            for (final IEditorReference ref:editorReferences) {

                IEditorPart editor = ref.getEditor(false);
                if (editor!=null) {
                    currentFile = (IFile)editor.getEditorInput()
                        .getAdapter(IFile.class);
                    if (currentFile!=null) {
                        ClearAdamMarkers.clearMarkers(
                            currentFile);// markers removed
                    }
                }

            }
        }
        catch (final NullPointerException ex) {
            DocfactoAdamPlugin.logException(
                "Unable to remove markers from resource ",ex);
        }

        // remove the listeners
        JavaCore.removeElementChangedListener(listener);
        ResourcesPlugin.getWorkspace().removeResourceChangeListener(
            resourceListener);

        if (windows!=null) {
            for (final IWorkbenchWindow window:windows) {

                try {
                    page = window.getActivePage();
                    page.removePartListener(partlistener);
                }
                catch (Exception e) {
                    DocfactoAdamPlugin.logException(
                        "Unable to remove part listener",e);
                }
            }
        }
    }

    /**
     * This method is used to turn on adam listeners
     * 
     * @since 2.2
     * 
     */
    public void turnOnListeners() {
        // Turn on listeners

        JavaCore.addElementChangedListener(listener);
        ResourcesPlugin.getWorkspace().addResourceChangeListener(
            resourceListener,IResourceChangeEvent.POST_CHANGE);

        workBench = null;

        int timeLapse = 0;
        final int waitSeconds = 60;

        // retry every second if part listener not successfully added
        while (timeLapse<waitSeconds) {

            IWorkbenchWindow[] windows = null;
            try {
                workBench = PlatformUI.getWorkbench();
                windows = workBench.getWorkbenchWindows();
            }
            catch (final Exception e) {
                System.out.println(e.getMessage());
            }

            if (windows!=null) {

                for (final IWorkbenchWindow window:windows) {
                    page = window.getActivePage();
                    if (page!=null) {
                        page.addPartListener(partlistener);// addPartListener

                        addMarkers();
                    }
                }
            }

            if (page==null) {
                try {
                    Thread.sleep(1000); // 1 second sleep
                    timeLapse++;
                }
                catch (final InterruptedException ignore) {
                }
            }
            else {
                break;
            }
        }

    }

    /**
     * This method adds Adam markers to resource
     * <p>
     * called when - 'OK' is performed on preference page
     * <p>
     * called when - <code>ActivePartListener</code> is turned on.
     * 
     * @since 2.2
     */
    public void addMarkers() {

        try {

            workBench = PlatformUI.getWorkbench();
            windows = workBench.getWorkbenchWindows();
        }
        catch (final Exception e) {
            System.out.println(e.getMessage());
        }
        if (windows!=null) {

            for (final IWorkbenchWindow window:windows) {

                page = window.getActivePage();
                if (page!=null) {

                    final IEditorPart activeEditor = page.getActiveEditor();
                    if (activeEditor!=null) {
                        editorInput2 = page.getActiveEditor().getEditorInput();
                        final IWorkingCopyManager manager = JavaUI
                            .getWorkingCopyManager();
                        try {
                            manager.connect(editorInput2);
                        }
                        catch (final CoreException e) {
                            e.printStackTrace(System.err);
                        }

                        final Object editorInput = manager
                            .getWorkingCopy(window.getActivePage()
                                .getActiveEditor().getEditorInput());
                        if (editorInput instanceof ICompilationUnit) {
                            final ICompilationUnit cu =
                                (ICompilationUnit)editorInput;
                            AdamReporter reporter = new AdamReporter();
                            reporter.getCompilationUnitReport(cu);

                        }
                    }
                }
            }
        }
    }

    /**
     * Method is used to refresh markers if plug-in is ACTIVE
     * 
     * @since 2.2
     */
    public void refresh() {
        // check plug-in state
        Boolean pluinEnabled = EclipseAdamUtils.getAdamState();

        if (pluinEnabled) {
            addMarkers();
        }
    }

}
