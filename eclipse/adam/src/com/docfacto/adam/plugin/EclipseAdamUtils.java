/*
 * @author dhudson -
 * Created 15 Jan 2013 : 13:52:17
 */

package com.docfacto.adam.plugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.State;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;

import com.docfacto.config.generated.Severity;

/**
 * Utils to help with AST Javadoc processing
 * 
 * @author dhudson - created 15 Jan 2013
 * @since 1.1
 */
public class EclipseAdamUtils {

    private static final String DOCFACTO_ADAM = "@docfacto.adam";
    
    public static final String[] SEVERITIES = new String[]{
		Severity.WARNING.value(),
		Severity.ERROR.value(),
		Severity.INFO.value() };

    /**
     * List containing modifier keywords in the order proposed by Java Language
     * specification, sections 8.1.1, 8.3.1 and 8.4.3.
     */
    private static final List<Object> TAGLET_ORDER = Arrays
        .asList(new Object[] {
            TagElement.TAG_PARAM,TagElement.TAG_RETURN,TagElement.TAG_THROWS,
            TagElement.TAG_EXCEPTION,TagElement.TAG_SINCE,
            TagElement.TAG_VERSION,TagElement.TAG_AUTHOR,
            TagElement.TAG_SERIALDATA,TagElement.TAG_SEE,
            TagElement.TAG_INHERITDOC
        });

    /**
     * Create a new instance of <code>AdamUtils</code>.
     */
    private EclipseAdamUtils() {
    }

    /**
     * Checks to see if the Javadoc should be processed.
     * 
     * @param javadoc to process
     * @return true if a block tag of <code>docfacto.adam</code> is found
     * @since 1.1
     */
    public static boolean hasAdamIgnoreTag(Javadoc javadoc) {
        return hasTag(DOCFACTO_ADAM,javadoc);
    }

    /**
     * Check to see if a tag is in the javadoc, tag names should start with @,
     * if not one will be pre-fixed
     * 
     * @param tagName to search for
     * @param javadoc to process
     * @return true if tag is found
     * @since 1.1
     */
    public static boolean hasTag(String tagName,Javadoc javadoc) {

        if (!tagName.startsWith("@")) {
            tagName = "@"+tagName;
        }

        for (final TagElement te:getTags(javadoc)) {
            // Name can be null, if a text element
            if (tagName.equals(te.getTagName())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Return a list of all the Javadoc tags
     * 
     * @param javadoc to process
     * @return a list of all of the tags
     * @since 1.1
     */
    public static List<TagElement> getTags(Javadoc javadoc) {
        return getTags(null,javadoc);
    }

    /**
     * Gets a list of <code>TagElements</code> from javadoc
     * 
     * @param tagName name of the tag, or null for all tags
     * @param javadoc to process
     * @return a list of required tags
     * @since 1.1
     */
    public static List<TagElement> getTags(String tagName,Javadoc javadoc) {
        if (tagName!=null) {
            if (!tagName.startsWith("@")) {
                tagName = "@"+tagName;
            }
        }

        final ArrayList<TagElement> tags = new ArrayList<TagElement>(3);
        for (final Object te:javadoc.tags()) {
            if (te instanceof TagElement) {
                if (tagName!=null) {
                    if (tagName.equals(((TagElement)te).getTagName())) {
                        tags.add((TagElement)te);
                    }
                }
                else {
                    // We want them all.
                    tags.add((TagElement)te);
                }
            }
        }
        return tags;
    }

    /**
     * Check to see if this is a inherit or see type javadoc comment
     * 
     * @param javadoc to process
     * @return return true if this is a inherit or see javadoc
     * @since 1.1
     */
    public static boolean hasSeeOrInheritTag(Javadoc javadoc) {
        final List<TagElement> tags = EclipseAdamUtils.getTags(javadoc);

        if (!(tags.size()>0)) {
            return false;
        }

        final TagElement te = tags.get(0);
        final String tagName = te.getTagName();

        if (tagName==null) {
            return false;
        }

        if (tagName.equals(TagElement.TAG_INHERITDOC)||
            tagName.equals(TagElement.TAG_SEE)) {
            return true;
        }

        return false;
    }

    /**
     * reorder javadoc tags in the correct order
     * 
     * @param javadoc to process
     * @since 1.1
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    public static void reorderTags(Javadoc javadoc) {

        final List<Object> copies = new ArrayList<Object>();
        final Iterator it = javadoc.tags().iterator();
        while (it.hasNext())
        {
            final ASTNode mod = (ASTNode)it.next();
            copies.add(ASTNode.copySubtree(mod.getAST(),mod));
        }

        // order tags to correct order
        Collections.sort(copies,new Comparator()
        {
            @Override
            public int compare(Object arg0,Object arg1)
            {
                if (!(arg0 instanceof TagElement)||
                    !(arg1 instanceof TagElement))
                {
                    return 0;
                }

                final TagElement te1 = (TagElement)arg0;
                final TagElement te2 = (TagElement)arg1;

                final int tagIndex1 = TAGLET_ORDER.indexOf(te1.getTagName());
                final int tagIndex2 = TAGLET_ORDER.indexOf(te2.getTagName());

                return new Integer(tagIndex1).compareTo(new Integer(tagIndex2));
            }
        });

        javadoc.tags().clear();
        javadoc.tags().addAll(copies);
    }

    /**
     * Method to obtain Adam state
     * 
     * @return true if Adam is enabled
     */
    public static boolean getAdamState() {
        ICommandService service =
            (ICommandService)PlatformUI.getWorkbench().getService(
                ICommandService.class);
        Command command =
            service.getCommand("adamPluginBeta.commands.toggleCommand");
        State state = command.getState("org.eclipse.ui.commands.toggleState");
        Boolean pluinEnabled = (Boolean)state.getValue();

        return pluinEnabled;
    }

	/**
	 * A method to obtain index of a string within an array.
	 * 
	 * @param stringArray
	 * @param value
	 * @return int value of index. Would return 0 if string is not found.
	 * @since 2.2
	 */
	public static int getIndex(String[] stringArray, String value) {
		int i = 0;
		for(i = 0; i < stringArray.length; i++){
			if(stringArray[i].equals(value)){
				return i;
			}
			
		}
		
		return 0;
	}

}
