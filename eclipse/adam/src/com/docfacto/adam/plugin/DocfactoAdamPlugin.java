package com.docfacto.adam.plugin;

import java.util.GregorianCalendar;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.State;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.services.ISourceProviderService;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import com.docfacto.adam.sourceprovider.CommandState;
import com.docfacto.common.DocfactoException;
import com.docfacto.config.XmlConfig;
import com.docfacto.config.generated.Product;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.licensor.Products;

/**
 * The DocfactoAdamPlugin class controls the plug-in life cycle.
 * 
 * As this is a singleton, use this as the controller as well.
 * @author pemi - Jan 7, 2013 11:11am
 * @since 2.1
 */
public class DocfactoAdamPlugin extends AbstractUIPlugin implements IStartup {
	/**
	 * Adam plug-in ID
	 */
	public static final String PLUGIN_ID = "com.docfacto.adam.plugin";

	// The shared instance
	private static DocfactoAdamPlugin thePlugin;

	private static AddMarkersToResources resourceMarker =
			new AddMarkersToResources();

	private AdamQueue syncAdam;


	/**
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		thePlugin = this;

		if (isAdamTurnedOn()) {
			DocfactoCorePlugin.registerPlugin("Adam",Products.ADAM);
			
		}
	}

	/**
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		thePlugin = null;
		super.stop(context);
	}

	/**
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#initializeImageRegistry(org.eclipse.jface.resource.ImageRegistry)
	 */
	@Override
	protected void initializeImageRegistry(ImageRegistry registry) {
		super.initializeImageRegistry(registry);

		final Bundle bundle = Platform.getBundle(PLUGIN_ID);

		for (String imageName:IDocfactoAdamPlugin.IMAGES) {
			final ImageDescriptor image =
					ImageDescriptor.createFromURL(FileLocator
							.find(bundle,new Path("icons/"+imageName),null));
			registry.put(imageName,image);
		}
	}

	/**
	 * Return a Docfacto image
	 * 
	 * @param key of the Image
	 * @return the cached image, or null if not found
	 * @since 2.1
	 */
	public static Image getImage(String key) {
		final ImageRegistry imageRegistry = thePlugin.getImageRegistry();
		return imageRegistry.get(key);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 * @since 2.2
	 */
	public static DocfactoAdamPlugin getDefault() {
		return thePlugin;
	}

	/**
	 * Log and exception to the console
	 * 
	 * @param message to display
	 * @param ex cause, can be null
	 * @since 1.1
	 */
	public static void logException(String message,Exception ex) {
		thePlugin.getLog().log(
				new Status(IStatus.ERROR,PLUGIN_ID,IStatus.ERROR,message,ex));
	}

	/**
	 * @see org.eclipse.ui.IStartup#earlyStartup()
	 */
	@Override
	public void earlyStartup() {
		boolean isConfValid = DocfactoCorePlugin.getDefault().getPreferenceStore().getBoolean(DocfactoCorePlugin.KEY_VALIDATED);
		if (isConfValid && isLicenceValid()) {
		    setPermDisable(false);
			if (isAdamTurnedOn()) {
				// add markers and turn on listener
				try {
					// turn on listeners
					addListeners();
				} catch (final Exception ex) {
					// Probably too early to log
					DocfactoCorePlugin.logException(
							"Issue with Adam early startup", ex);
				}
			}
			
			
		}else{
			setPermDisable(true);
		}
	}

	/**
     * Method checks if Grabit is properly licensed
     * 
     * @return <code>true</code> if license valid
     * @since 2.5
     */
    public boolean isLicenceValid() {

        XmlConfig xmlConfig = DocfactoCorePlugin.getXmlConfig();
        if (xmlConfig!=null) {
            Product adamProduct = xmlConfig.getProduct(Products.ADAM);
            boolean valid = GregorianCalendar.getInstance().before(adamProduct.getExpiryDate().toGregorianCalendar());
            if (adamProduct!=null && valid) {
                // License is valid
                return true;
            }
        }

        return false;

    }
    /**
	 * Check to see if Adam is turned on
	 * 
	 * 
	 * @return true if Adam is turned on
	 * @since 2.4
	 */
	private boolean isAdamTurnedOn() {

		return (Boolean) getAdamToggleState().getValue();
	}

	private State getAdamToggleState() {

		return getAdamToggleCommand().getState("org.eclipse.ui.commands.toggleState");
	}

	/**
	 * 
	 * Removes listeners from resources
	 * 
	 * @since 2.2
	 * 
	 */
	public void removeListeners() {
		// turn off listeners
		resourceMarker.turnOffListeners();
		
		
	}

	/**
	 * Adds listeners to resources
	 * 
	 * @since 2.2
	 * 
	 */
	public void addListeners() {
		// turn on listeners
		resourceMarker.turnOnListeners();
	}

	/**
	 * Method processes the Exception and updates the store with error values
	 * The user is prompted to enter a valid configuration file.
	 * <p>
	 * Method also toggles the adam command button and remove associated
	 * listeners.
	 * </p>
	 * 
	 * @param e - <code>DocfactoException</code> required for
	 * <code>Status</code> update
	 * @since 2.2
	 */
	public void invalidConfig(DocfactoException e) {

		// store error
		DocfactoCorePlugin.getDefault().getPreferenceStore()
		.putValue(DocfactoCorePlugin.VALIDATION_ERROR,e.getMessage());

		// create a pop-up message
		Status myStatus =
				new Status(IStatus.ERROR,DocfactoCorePlugin.PLUGIN_ID,
						"Docfacto.xml invalid",e);
		StatusManager.getManager().handle(myStatus,StatusManager.SHOW);

		// turn off adam
		turnOffAdam();

	}

	/**
	 * Method returns a non-null value of AdamQueue
	 * 
	 * @return <code>AdamQueue</code> a new one is created if none exists
	 * @since 2.2
	 */
	public AdamQueue getAdamQueue() {

		if (syncAdam==null) {
			syncAdam = new AdamQueue();
		}
		return syncAdam;
	}

	/**
	 * Method checks if the adam toggle button is enabled/disabled
	 * 
	 * 
	 * @return - <code>true</code> if button is disabled or service not found
	 * @since 2.5
	 */
    public boolean isPermDisable() {
        CommandState commandStateService = null;

        // Get the source provider service
        ISourceProviderService sourceProviderService =
            (ISourceProviderService)PlatformUI
                .getWorkbench().getService(ISourceProviderService.class);
        // get service command
        if (sourceProviderService!=null) {
            commandStateService = (CommandState)sourceProviderService
                .getSourceProvider(CommandState.STATE);
        }

        boolean buttonDisabled =
            commandStateService.getCurrentState().containsKey(
                CommandState.DISABLED) ? true : false;

        return buttonDisabled;
    }

	/**
	 * Permanently disabling Adam
	 * <p>
	 * Method would disable/enable Adam based on received argument.
	 * </p>
	 * @param disable - disables adam if <code>true</code>
	 * @since 2.5
	 */
	public void setPermDisable(boolean disable) {
			// disable/enable the adam toggle button
			// Get the source provider service
			ISourceProviderService sourceProviderService = (ISourceProviderService) PlatformUI
					.getWorkbench().getService(ISourceProviderService.class);
			// get service
			CommandState commandStateService = (CommandState) sourceProviderService
					.getSourceProvider(CommandState.STATE);
			commandStateService.setDisabled(disable);
	}
	

	/**
	 * Programmatically turn off Adam toggle button
	 * @since 2.5
	 */
	public void turnOffAdam() {

		if (isAdamTurnedOn()) {

			try {
				HandlerUtil.toggleCommandState(getAdamToggleCommand());
			} catch (ExecutionException ex) {
				DocfactoCorePlugin.logException("ExecutionException", ex);
			}
		}
		removeListeners();

	}

	
	private Command getAdamToggleCommand() {
		ICommandService service = (ICommandService) PlatformUI.getWorkbench()
				.getService(ICommandService.class);

		return service.getCommand("adamPluginBeta.commands.toggleCommand");

	}

}
