package com.docfacto.adam.plugin;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.ui.IWorkingCopyManager;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import com.docfacto.adam.plugin.markers.MarkerCreator;
import com.docfacto.core.DocfactoCorePlugin;

/**
 * Handler for clearing Adam markers
 * 
 * @author pemi - created Mar 21, 2013 9:21:02 AM
 * 
 * @since 2.2
 */
public class ClearAdamMarkers extends AbstractHandler {

    /**
     * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
     */
    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {

        // Get selected item
        IWorkbenchWindow window = HandlerUtil
            .getActiveWorkbenchWindowChecked(event);
        ISelection selection = window.getSelectionService().getSelection();

        // Check selection type
        if (selection instanceof IStructuredSelection) {

            IStructuredSelection ss = (IStructuredSelection)selection;
            Object firstElement = ss.getFirstElement();
            if (firstElement instanceof IJavaElement) {

                IJavaElement javaElement = (IJavaElement)firstElement;
                clearMarkers(javaElement.getResource());

            }
            else {
                MessageDialog.openInformation(window.getShell(),"Info",
                    "Please select a valid java resource");
            }

            return null;

        }
        // Selection is probably from editor input
        else if (selection!=null) {
            IWorkingCopyManager manager = JavaUI.getWorkingCopyManager();
            IEditorPart cuEditor = PlatformUI.getWorkbench()
                .getActiveWorkbenchWindow().getActivePage()
                .getActiveEditor();
            IEditorInput editorInput = cuEditor.getEditorInput();

            try {
                manager.connect(editorInput);
            }
            catch (CoreException e) {
                e.printStackTrace(System.err);
            }
            ICompilationUnit cu = manager.getWorkingCopy(cuEditor
                .getEditorInput());

            // clear markers from the resource
            clearMarkers(cu.getResource());

        }
        else {
            MessageDialog.openInformation(window.getShell(),"Info",
                "Unable clear markers from this resource");
            return null;

        }

        return null;
    }

    /**
     * Method deletes adam markers from specified resource
     * 
     * @param resource the resource to delete adam markers from
     * @since 2.2
     */
    public static void clearMarkers(IResource resource) {
        try {
            resource.deleteMarkers(MarkerCreator.ADAM_MARKER_ID,true,
                IResource.DEPTH_INFINITE);
        }
        catch (CoreException e) {
            DocfactoCorePlugin.logException("Marker deletion error:",e);
        }

    }
}
