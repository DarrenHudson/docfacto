/**
 * 
 */
package com.docfacto.adam.plugin;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.eclipse.jdt.core.ICompilationUnit;

import com.docfacto.adam.report.AdamReporter;

/**
 * Class makes use of the <code>ConcurrentLinkedQueue</code> for storing
 * compilation units
 * 
 * @author pemi - created Jun 17, 2013 10:34:09 AM
 * 
 * @since 2.2
 */
public class AdamQueue {

    private ConcurrentLinkedQueue<ICompilationUnit> theCU =
        new ConcurrentLinkedQueue<ICompilationUnit>();
    private ExecutorService service = Executors.newSingleThreadExecutor();

    /**
     * Method adds a compilation unit to the adam queue
     * 
     * @param cu the cu to to be added to the tail of queue
     * @since 2.2
     */
    public synchronized void queueCU(ICompilationUnit cu) {

        // check if there are elements waiting to be processed
        if (!theCU.isEmpty()) {

            String cuPath = cu.getResource().getRawLocation().toString();

            // check if resource of cu is same as previous element(s) in queue
            try {
                String previousCUPath =
                    theCU.peek().getResource().getRawLocation().toString();

                // clear the queue if cu path is different
                if (!previousCUPath.equals(cuPath)) {
                    theCU.clear();

                }
            }
            catch (NullPointerException e) {
                // do nothing; no previous path

            }
        }
        // Add compilation unit to queue
        theCU.add(cu);

        // create a runnable and submit to the executor service
        service.submit(new Runnable() {
            public void run() {

                /*
                 * Finds the first element in the queue of compilation units.
                 * Process element if the element is the last one. Otherwise,
                 * remove the element from the queue.
                 */
                AdamReporter reporter = new AdamReporter();
                ICompilationUnit queuedCU = theCU.poll();

                if (theCU.isEmpty()&&queuedCU.exists()) {
                    reporter.getCompilationUnitReport(queuedCU);
                }
            }
        });
    }

}
