/**
 * 
 */
package com.docfacto.adam.plugin.resolutions;

import java.util.Arrays;
import java.util.Comparator;

import org.eclipse.core.resources.IMarker;
import org.eclipse.ui.IMarkerResolution;
import org.eclipse.ui.IMarkerResolutionGenerator2;

import com.docfacto.adam.plugin.markers.MarkerCreator;

/**
 * Marker Resolution Generator
 * 
 * @author pemi - created Jun 18, 2013 11:59:01 AM
 * @since 2.2
 */
public class AdamResolutionGenerator implements IMarkerResolutionGenerator2 {

    private IMarkerResolution[] theResolutions;
    private static final IMarkerResolution[] NO_RESOLUTIONS =
        new IMarkerResolution[0];

    /**
     * @see org.eclipse.ui.IMarkerResolutionGenerator#getResolutions(org.eclipse.core.resources.IMarker)
     */
    @Override
    public IMarkerResolution[] getResolutions(IMarker marker) {
        IMarkerResolution[] res = getResolutionFromMarker(marker);
        return sortRes(res,marker);
    }

    /**
     * Get Resolutions from Marker
     * 
     * @param marker to process
     * @return an array of marker resolutions
     * @since 2.2
     */
    public IMarkerResolution[] getResolutionFromMarker(
    IMarker marker) {

        if (!hasResolutions(marker)) {
            return NO_RESOLUTIONS;
        }
        else {
            int resCount =
                marker.getAttribute(MarkerCreator.ADAM_RESOLUTION_COUNT,0);

            theResolutions = new IMarkerResolution[resCount];

            for (int i = 0;resCount>i;i++) {
                theResolutions[i] = new AdamResolution(marker,i);
            }
        }
        return theResolutions;
    }

    /**
     * @param resolutions
     */
    private IMarkerResolution[] sortRes(IMarkerResolution[] resArray,
    IMarker marker) {

        IMarkerResolution[] resolutions = resArray;
        int arrayLength = resArray.length;
        IMarkerResolution[] resWithIgnore;
        boolean addFixAll = false;
        if (arrayLength>1) {
            addFixAll = true;
            resWithIgnore = new IMarkerResolution[arrayLength+2];
        }
        else {
            resWithIgnore = new IMarkerResolution[arrayLength+1];

        }

        // create an anonymous comparator
        Comparator<IMarkerResolution> resComp =
            new Comparator<IMarkerResolution>() {

                @Override
                public int compare(IMarkerResolution o1,IMarkerResolution o2) {

                    String label0 = o1.getLabel();
                    String label1 = o2.getLabel();

                    if (!label0.equals(MarkerCreator.LABEL_IGNORE)&&
                        !label1.equals(MarkerCreator.LABEL_IGNORE)) {
                        return label0.compareTo(label1);
                    }

                    if (label0.equals(MarkerCreator.LABEL_IGNORE)) {
                        return 1;

                    }
                    else if (label1.equals(MarkerCreator.LABEL_IGNORE)) {
                        return -1;

                    }

                    return 0;
                }
            };

        Arrays.sort(resolutions,resComp);
        System.arraycopy(resolutions,0,resWithIgnore,0,arrayLength);
        if (addFixAll) {

            // store a fix all resolution in last but one slot
            resWithIgnore[resWithIgnore.length-2] =
                new AdamResolution(marker,-2);
        }

        // store a ignore resolution in last slot
        resWithIgnore[resWithIgnore.length-1] =
            new AdamResolution(marker,-1);

        return resWithIgnore;
    }

    /**
     * @see org.eclipse.ui.IMarkerResolutionGenerator2#hasResolutions(org.eclipse.core.resources.IMarker)
     */
    @Override
    public boolean hasResolutions(IMarker marker) {
        // This gets called a lot, so make it as light as possible
        return (marker.getAttribute(MarkerCreator.ADAM_RESOLUTION_COUNT,0)>0);
    }

}
