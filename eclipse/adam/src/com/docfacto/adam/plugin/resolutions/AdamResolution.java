/**
 *
 */
package com.docfacto.adam.plugin.resolutions;

import java.util.List;

import org.eclipse.core.filebuffers.FileBuffers;
import org.eclipse.core.filebuffers.ITextFileBuffer;
import org.eclipse.core.filebuffers.ITextFileBufferManager;
import org.eclipse.core.filebuffers.LocationKind;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.NodeFinder;
import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jdt.core.dom.TextElement;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.swt.graphics.Image;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.TextEdit;
import org.eclipse.ui.IMarkerResolution;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.views.markers.WorkbenchMarkerResolution;

import com.docfacto.adam.plugin.DocfactoAdamPlugin;
import com.docfacto.adam.plugin.IDocfactoAdamPlugin;
import com.docfacto.adam.plugin.markers.MarkerCreator;
import com.docfacto.core.utils.ASTUtils;

/**
 * Class that represents a new Adam Resolution
 * 
 * @author pemi - created Jun 18, 2013 11:52:58 AM
 * @since 2.2
 */
public class AdamResolution extends WorkbenchMarkerResolution {

    private String theLabel;
    private String theDescription;
    private String tagFragment;
    private String tagName;

    private int theResolutionType;

    /**
     * Constructor to create a resolution from marker using specified
     * position/type
     * 
     * @param marker the marker to process
     * @param pos position of marker's resolution attribute if value is -1, an
     * ignore resolution would be generated, if value is -2, a 'fix all'
     * resolution would be generated.
     */
    public AdamResolution(IMarker marker,int pos) {

        int adamType = 0;
        tagFragment = "";
        tagName = "";
        if (pos>=0) {

            String resKey = MarkerCreator.RESOLUTION_KEY+pos;
            adamType =
                marker
                    .getAttribute(resKey+MarkerCreator.ADAM_RESOLUTION_TYPE,0);
            tagFragment =
                marker.getAttribute(resKey+MarkerCreator.ADAM_TAG_FRAG_NAME,"");
            tagName =
                marker.getAttribute(resKey+MarkerCreator.ADAM_TAG_NAME,"");

        }
        else if (pos==-1) {
            adamType = MarkerCreator.RESOLUTION_IGNORE;

        }
        else if (pos==-2) {
            adamType = MarkerCreator.RESOLUTION_FIX_ALL;
        }
        theResolutionType = adamType;
        setLabel(adamType,tagName,tagFragment);
        setDescription(adamType,tagName);
    }

    /**
     * @see org.eclipse.ui.views.markers.WorkbenchMarkerResolution#findOtherMarkers(org.eclipse.core.resources.IMarker[])
     */
    @Override
    public IMarker[] findOtherMarkers(IMarker[] markers) {
        return null;
    }

    /**
     * @see org.eclipse.ui.IMarkerResolution2#getDescription()
     */
    @Override
    public String getDescription() {
        return theDescription;
    }

    /**
     * @see org.eclipse.ui.IMarkerResolution2#getImage()
     */
    @Override
    public Image getImage() {

        Image image = null;
        switch (theResolutionType) {

        case MarkerCreator.RESOLUTION_ADD:
            image = PlatformUI.getWorkbench().getSharedImages().
                getImageDescriptor(ISharedImages.IMG_OBJ_ADD).createImage();
            break;

        case MarkerCreator.RESOLUTION_REMOVE:
            image = PlatformUI.getWorkbench().getSharedImages().
                getImageDescriptor(ISharedImages.IMG_ELCL_REMOVE).createImage();
            break;

        case MarkerCreator.RESOLUTION_FIX_ALL:
            image =
                DocfactoAdamPlugin.getImage(
                    IDocfactoAdamPlugin.FIX_ALL);
            break;

        }

        return image;
    }

    private int getInsertPoint(Javadoc javadoc,String tagName) {

        @SuppressWarnings("unchecked")
        List<TagElement> tagList = javadoc.tags();
        String currentTagName = "";

        // initialize insert points candidates
        int sincePoint = -1;
        int returnPoint = -1;
        int point = -1;

        // check the values of tags in javadoc
        for (int i = tagList.size()-1;i>=0;i--) {

            // get tag name
            currentTagName = tagList.get(i).getTagName();

            if (currentTagName!=null) {

                // put @author at top of tags
                if (tagName.startsWith(TagElement.TAG_AUTHOR)) {
                    return i;
                }

                // put @param after last @param tag
                if (currentTagName.equals(TagElement.TAG_PARAM)
                    &&tagName.startsWith(TagElement.TAG_PARAM)) {
                    return i+1;
                }

                // put @throws after last @throws tag
                if (currentTagName.equals(TagElement.TAG_THROWS)
                    &&tagName.startsWith(TagElement.TAG_THROWS)) {
                    return i+1;
                }

                // put @since last
                if (tagName.startsWith(TagElement.TAG_SINCE)) {
                    return -1;
                }

                // save point of @since if one exists
                if (currentTagName.equals(TagElement.TAG_SINCE)) {
                    sincePoint = i;
                }

                // save point of @return if one exists
                if (currentTagName.equals(TagElement.TAG_RETURN)) {
                    returnPoint = i;
                }
            }

        }

        // determine the insert point
        if (sincePoint>=0) {
            point = sincePoint;
        }
        if (returnPoint>=0) {
            point = returnPoint;

        }

        return point;
    }

    /**
     * @see org.eclipse.ui.IMarkerResolution#getLabel()
     */
    @Override
    public String getLabel() {
        return theLabel;
    }

    /**
     * @see org.eclipse.ui.IMarkerResolution#run(org.eclipse.core.resources.IMarker)
     */
    @SuppressWarnings("unchecked")
    @Override
    public void run(IMarker marker) {

        final IMarker aMarker = marker;

        try {

            ITextFileBufferManager bufferManager = null;
            
            ICompilationUnit source = ASTUtils.getCompilationUnitFor(marker);
            
            final IPath path = source.getPath();

            bufferManager = FileBuffers.getTextFileBufferManager();
            bufferManager.connect(path,LocationKind.IFILE,null);
            final ITextFileBuffer textFileBuffer =
                bufferManager.getTextFileBuffer(path,LocationKind.IFILE);
            final IDocument document = textFileBuffer.getDocument();


            CompilationUnit result = ASTUtils.getCompilationUnitFor(source);

            // start record of the modifications
            result.recordModifications();

            ASTNode astNode =
                NodeFinder.perform(result,
                    marker.getAttribute(IMarker.CHAR_START,0),1);

            Javadoc node = null;
            if (astNode instanceof Javadoc) {
                node = (Javadoc)astNode;
            }
            else {
                // lets find the BodyDeclaration
                while (!(astNode instanceof BodyDeclaration)) {
                    astNode = astNode.getParent();
                }
                // Create a javadoc node and assign it to the element
                node = result.getAST().newJavadoc();
                BodyDeclaration decl = (BodyDeclaration)astNode;
                decl.setJavadoc(node);
            }
            switch (theResolutionType) {

            case MarkerCreator.RESOLUTION_ADD:

                TagElement tagElement = result.getAST().newTagElement();

                if (tagName.isEmpty()) {
                    // We are adding a text node
                    TextElement textElement =
                        node.getAST().newTextElement();
                    textElement.setText(tagFragment);
                    tagElement.fragments().add(textElement);

                    node.tags().add(tagElement);
                }
                else {
                    // We are creating a tag
                    // This is wrong, as this should have a SimpleName Fragment
                    tagElement.setTagName(tagName);

                    if (!tagFragment.isEmpty()) {
                        tagElement.fragments().add(
                            node.getAST().newSimpleName(tagFragment));
                    }

                    int insertPoint = getInsertPoint(node,tagName);

                    if (insertPoint>=0) {
                        node.tags().add(insertPoint,tagElement);
                    }
                    else {
                        node.tags().add(tagElement);
                    }
                }
                break;

            case MarkerCreator.RESOLUTION_REMOVE:
                final List<TagElement> tags = node.tags();

                if (!tags.isEmpty()) {

                    for (TagElement e:tags) {
                        if (aMarker.getAttribute(
                            IMarker.LINE_NUMBER,0)==result
                            .getLineNumber(e.getStartPosition())) {
                            // I have the one, remove this
                            // TagElement
                            node.tags().remove(e);
                            break;
                        }
                    }

                }
                break;

            case MarkerCreator.RESOLUTION_IGNORE:
                TagElement ignoreTag =
                    node.getAST().newTagElement();
                ignoreTag.setTagName("@docfacto.adam ignore");
                node.tags().add(ignoreTag);
                break;

            case MarkerCreator.RESOLUTION_FIX_ALL:
                IMarkerResolution[] resGen =
                    new AdamResolutionGenerator()
                        .getResolutionFromMarker(aMarker);
                for (IMarkerResolution aRes:resGen) {
                    aRes.run(aMarker);
                }
                break;

            default:
                System.out
                    .println("Dont know what to do with type: "+
                        theResolutionType);

                break;

            }

            final TextEdit edit =
                result.rewrite(document,source.getJavaProject()
                    .getOptions(true));
            edit.apply(document);

        }
        catch (CoreException ex) {
            DocfactoAdamPlugin.logException(
                "CoreException error in resolution",ex);
        }
        catch (MalformedTreeException ex) {
            DocfactoAdamPlugin.logException(
                "MalformedTreeException error in resolution",ex);
        }
        catch (BadLocationException ex) {
            DocfactoAdamPlugin.logException(
                "BadLocationException error in resolution",ex);
        }

    }

    /**
     * @param type
     * @param tagName
     */
    private void setDescription(int type,String tagName) {
        theDescription = "";
        switch (theResolutionType) {

        case MarkerCreator.RESOLUTION_ADD:
            theDescription = tagName
                +" would be added to this javadoc comment";
            break;

        case MarkerCreator.RESOLUTION_REMOVE:
            theDescription = tagName
                +" would be removed from this javadoc comment";
            break;

        case MarkerCreator.RESOLUTION_FIX_ALL:
            theDescription = " would fix issues with known resolutions";
            break;

        case MarkerCreator.RESOLUTION_IGNORE:
            theDescription =
                "This comment block would be excluded from further checks";
            break;
        }
    }

    /**
     * @param type
     * @param tagName
     */
    private void setLabel(int type,String tagName,String fragment) {

        theLabel = "";
        String tagWithFrag =
            fragment.length()>0 ? tagName+" "+fragment : tagName;

        switch (type) {

        case MarkerCreator.RESOLUTION_ADD:
            if (tagName.isEmpty()) {
                theLabel = "Suggestion: "+fragment;
            }
            else {
                theLabel = "Add missing "+tagWithFrag+" tag";
            }
            break;

        case MarkerCreator.RESOLUTION_REMOVE:
            theLabel = "Delete "+tagWithFrag+" from comment";
            break;

        case MarkerCreator.RESOLUTION_IGNORE:
            theLabel = "Ignore this comment block";
            break;

        case MarkerCreator.RESOLUTION_FIX_ALL:
            theLabel = "Fix all issues";
            break;
        }
    }

}
