package com.docfacto.adam.plugin.listeners;

import org.eclipse.jdt.core.ElementChangedEvent;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IElementChangedListener;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaElementDelta;

import com.docfacto.adam.plugin.DocfactoAdamPlugin;

/**
 * Eclipse Change Listener.
 * 
 * @author pemi - created 11 Jan 2013
 * @since 1.1
 */
public class AdamElementChangedListener implements IElementChangedListener {
    
    private ICompilationUnit cu;

    /**
     * Constructor for <code>AdamResourceListener</code>.
     */
    public AdamElementChangedListener() {
    }

    /**
     * @see org.eclipse.jdt.core.IElementChangedListener#elementChanged(org.eclipse.jdt.core.ElementChangedEvent)
     */
    @Override
    public void elementChanged(ElementChangedEvent event) {

        final IJavaElementDelta delta = event.getDelta();
        if (delta!=null) {

            switch (delta.getKind()) {
            case IJavaElementDelta.CHANGED:
                if ((delta.getFlags()&IJavaElementDelta.F_FINE_GRAINED)!=0) {

                    final IJavaElement elem = delta.getElement();
                    cu = (ICompilationUnit)elem.getAncestor(IJavaElement.COMPILATION_UNIT);
                    
                    // send cu to a queue in AdamQueue
                    DocfactoAdamPlugin.getDefault().getAdamQueue().queueCU(cu); 
     
                }

                /* Others flags can also be checked */

                break;
          
                
            }
        }
    }
}