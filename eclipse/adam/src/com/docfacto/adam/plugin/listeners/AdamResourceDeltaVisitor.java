/**
 * 
 */
package com.docfacto.adam.plugin.listeners;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaCore;

import com.docfacto.adam.plugin.DocfactoAdamPlugin;

/**
 * This Delta visitor implementation ensures java files are processed after a
 * save command.
 * 
 * @author pemi - created May 30, 2013 3:33:00 PM
 * 
 * @since 2.2
 */
public class AdamResourceDeltaVisitor implements IResourceDeltaVisitor {

    /**
     * @see org.eclipse.core.resources.IResourceDeltaVisitor#visit(org.eclipse.core.resources.IResourceDelta)
     */
    public boolean visit(final IResourceDelta delta) {

        switch (delta.getKind()) {

        case IResourceDelta.CHANGED:

            // Process if resource is a java file
            if (delta.getFullPath().toString().endsWith(".java")
                &&(delta.getFlags()&IResourceDelta.CONTENT)!=0) {

                IPath path = new Path(delta.getFullPath().toString());
                IFile ifile =
                    ResourcesPlugin.getWorkspace().getRoot().getFile(path);
                ICompilationUnit cu = JavaCore.createCompilationUnitFrom(ifile);

                // send cu to a queue in AdamQueue
                DocfactoAdamPlugin.getDefault().getAdamQueue().queueCU(cu);

            }
            break;
        }
        return true; // visit the children
    }
}
