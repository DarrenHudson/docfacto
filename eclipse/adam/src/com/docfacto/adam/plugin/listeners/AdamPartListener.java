package com.docfacto.adam.plugin.listeners;

import org.eclipse.core.resources.IResource;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IWorkbenchPartReference;

import com.docfacto.adam.plugin.DocfactoAdamPlugin;
import com.docfacto.adam.plugin.EclipseAdamUtils;
import com.docfacto.adam.plugin.markers.MarkerCreator;

/**
 * A listener for part lifecycle events.
 * 
 * <p>
 * If Adam is enabled, the listener is called at startup 
 * and added to a <code>IWorkbenchPage</code> 
 * 
 * <p>Listener is removed if the Adam command 
 * button is disabled in toolbar.
 * 
 * @author pemi - created 11 Jan 2013
 * @since 1.1
 * 
 */
public class AdamPartListener implements IPartListener2 {

    private IEditorReference editor;
    private IEditorInput editorInput;
    private Object obj;

    /**
     * This method is called when a part is visible
     * 
     * @see org.eclipse.ui.IPartListener2#partVisible(org.eclipse.ui.IWorkbenchPartReference)
     * 
     * 
     */
    @Override
    public void partVisible(IWorkbenchPartReference partRef) {


        if (EclipseAdamUtils.getAdamState()) {

            if (partRef.getTitle().endsWith(".java")) {
                try {
                    editor = (IEditorReference)partRef;
                    editorInput = editor.getEditor(false).getEditorInput();
                    obj = JavaUI.getWorkingCopyManager().getWorkingCopy(
                        editorInput);

                    if (obj instanceof ICompilationUnit) {
                        //run in thread to prevent display locking
                        ICompilationUnit cu = (ICompilationUnit)obj;
                        DocfactoAdamPlugin.getDefault().getAdamQueue().queueCU(cu);
                        
                    }
                }
                catch (Exception ex) {
                    DocfactoAdamPlugin.logException(
                        "Error in ActivePartListener:partVisible : "
                            +ex.getMessage(),ex);
                }
            }
        }
    
        
    }

    /**
     * @see org.eclipse.ui.IPartListener2#partActivated(org.eclipse.ui.IWorkbenchPartReference)
     * 
     */
    @Override
    public void partActivated(IWorkbenchPartReference partRef) {
    }

    /**
     * @see org.eclipse.ui.IPartListener2#partBroughtToTop(org.eclipse.ui.IWorkbenchPartReference)
     * 
     */
    @Override
    public void partBroughtToTop(IWorkbenchPartReference partRef) {
    }

    /**
     * @see org.eclipse.ui.IPartListener2#partClosed(org.eclipse.ui.IWorkbenchPartReference)
     * 
     */
    @Override
    public void partClosed(IWorkbenchPartReference partRef) {

        // remove markers from closed resources
        if (partRef.getTitle().endsWith(".java")) {
            try {
                editor = (IEditorReference)partRef;
                editorInput = editor.getEditor(false).getEditorInput();
                obj = JavaUI.getWorkingCopyManager().getWorkingCopy(
                    editorInput);

                if (obj instanceof ICompilationUnit) {
                    IResource resource = ((ICompilationUnit)obj).getResource();

                    // remove markers if resource exists
                    if(resource.exists()){
                        resource.deleteMarkers(MarkerCreator.ADAM_MARKER_ID,true,
                            IResource.DEPTH_INFINITE);// markers removed                        
                    }

                }
            }
            catch (Exception ex) {
                System.out.println("Error in ActivePartListener: closing resource");
            }
        }

    }

    /**
     * @see org.eclipse.ui.IPartListener2#partDeactivated(org.eclipse.ui.IWorkbenchPartReference)
     * 
     */
    @Override
    public void partDeactivated(IWorkbenchPartReference partRef) {
        
    }

    /**
     * @see org.eclipse.ui.IPartListener2#partOpened(org.eclipse.ui.IWorkbenchPartReference)
     * 
     */
    @Override
    public void partOpened(IWorkbenchPartReference partRef) {        
    }

    /**
     * @see org.eclipse.ui.IPartListener2#partHidden(org.eclipse.ui.IWorkbenchPartReference)
     * 
     */
    @Override
    public void partHidden(IWorkbenchPartReference partRef) {
    }

    /**
     * @see org.eclipse.ui.IPartListener2#partInputChanged(org.eclipse.ui.IWorkbenchPartReference)
     * 
     */
    @Override
    public void partInputChanged(IWorkbenchPartReference partRef) {
    }

}