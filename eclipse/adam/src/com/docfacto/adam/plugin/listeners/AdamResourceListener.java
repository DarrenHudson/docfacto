/**
 * 
 */
package com.docfacto.adam.plugin.listeners;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.runtime.CoreException;

/**
 * A listener for tracking changes made to workspace
 * (e.g Save action on an underlying resource)
 * @author pemi - created May 30, 2013 4:05:41 PM
 * 
 * @since 2.2
 */
public class AdamResourceListener implements IResourceChangeListener {
    
    
    /** 
     * @see org.eclipse.core.resources.IResourceChangeListener#resourceChanged(org.eclipse.core.resources.IResourceChangeEvent)
     */
    public void resourceChanged(IResourceChangeEvent event) {
        switch (event.getType()) {

        case IResourceChangeEvent.POST_CHANGE:
            try {
                event.getDelta().accept(new AdamResourceDeltaVisitor());
            }
            catch (CoreException e) {
                e.printStackTrace();
            }
            break;
        }
    }
}