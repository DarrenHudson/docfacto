/**
 * 
 */
package com.docfacto.adam.plugin.markers;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.texteditor.MarkerUtilities;

import com.docfacto.config.generated.Severity;

/**
 * Utility class for adam markers
 * 
 * @author pemi - created Jun 19, 2013 2:19:00 PM
 * 
 * @since 2.2
 */
public class MarkerCreator {

    /**
     * The adam marker ID
     * 
     * @see org.eclipse.core.resources.IMarker#getType()
     */
    public static final String ADAM_MARKER_ID =
        "org.eclipse.viatra2.loaders.adammarker";

    /**
     * Resolution type to indicate a fix for all known issues
     */
    public static final int RESOLUTION_FIX_ALL = 234;

    /**
     * Resolution type for add action
     */
    public static final int RESOLUTION_ADD = 233;

    /**
     * Resolution type for remove action
     */
    public static final int RESOLUTION_REMOVE = 232;

    /**
     * Resolution type for modify action
     */
    public static final int RESOLUTION_MODIFY = 231;

    /**
     * Resolution type to ignore comment block
     */
    public static final int RESOLUTION_IGNORE = 230;

    /**
     * Normally indicating no resolution available
     */
    public static final int RESOLUTION_NONE = 0;

    /**
     * Part of resolution attribute key
     */
    public static final String ADAM_RESOLUTION_TYPE = "com.docfacto.adam.type";

    /**
     * Part of resolution attribute key
     */
    public static final String ADAM_TAG_NAME = "com.docfacto.adam.tag.name";

    /**
     * Part of resolution attribute key
     */
    public static final String ADAM_TAG_FRAG_NAME = "adam.tag.fragment";

    /**
     * Part of resolution attribute key
     */
    public static final String ADAM_RESOLUTION_COUNT = "adam.resolution.count";

    /**
     * Part of resolution attribute key
     */
    public static final String ADAM_RESULT_COUNT = "adam.resut.count";

    /**
     * Part of resolution attribute key
     */
    public static final String RESOLUTION_KEY = "[RESOLUTION]";

    /**
     * Part of resolution attribute key
     */
    public static final String RESOLUTION_SUBKEY_ID = "[SUBKEY]";

    /**
     * Part of result attribute key
     */
    public static final String ADAM_RESULT_KEY = "[RESULT]";

    /**
     * Message displayed when multiple resolutions exists
     */
    public static String MULTIPLE_ISSUES = "multiple issues in comment block";

    /**
     * Description label for the ignore tag
     */
    public static final String LABEL_IGNORE = "Ignore this comment block";

    /**
     * Method creates a marker for the resource specified in the holder and
     * marker attributes are set with data stored in the
     * {@code AdamMarkerWrapper}.
     * 
     * @param wrapper the {@code AdamMarkerWrapper} containing data from
     * processed {@code Result}
     * @throws CoreException from {@code IMarker} handling
     */
    public static void createNewMarker(AdamMarkerWrapper wrapper)
    throws CoreException {

        int charEnd = wrapper.getStart()+wrapper.getLength();

        final HashMap<String,Object> attributes =
            new HashMap<String,Object>(7);
        attributes.put(IMarker.PRIORITY,IMarker.PRIORITY_NORMAL);
        attributes.put(IMarker.CHAR_START,wrapper.getStart());
        attributes.put(IMarker.CHAR_END,charEnd);
        attributes.put(IMarker.LINE_NUMBER,wrapper.getLineNumber());
        attributes.put(IMarker.MESSAGE,wrapper.getMessage());
        attributes.put(IMarker.USER_EDITABLE,new Boolean(false));

        MarkerUtilities.createMarker(wrapper.getResource(),
            getResAttributes(wrapper,attributes),ADAM_MARKER_ID);

    }

    /**
     * @param holder
     * @return
     */
    private static Map<String,Object> getResAttributes(
    AdamMarkerWrapper holder,Map<String,Object> mAttributes) {

        int results =
            mAttributes.get(ADAM_RESULT_COUNT)==null ? 0 : (Integer)mAttributes
                .get(ADAM_RESULT_COUNT);
        int resolutions =
            mAttributes.get(ADAM_RESOLUTION_COUNT)==null ? 0
                : (Integer)mAttributes.get(ADAM_RESOLUTION_COUNT);
        int oldSeverity =
            mAttributes.get(IMarker.SEVERITY)==null ? 0 : (Integer)mAttributes
                .get(IMarker.SEVERITY);

        String resolutionKeyVal =
            holder.getTagName()+" "+holder.getDescription();
        resolutionKeyVal = resolutionKeyVal.trim();

        // add resolution to marker
        if (holder.getResType()>0&&holder.getResType()!=RESOLUTION_IGNORE) {

            mAttributes.put(RESOLUTION_KEY+resolutions,resolutionKeyVal);
            mAttributes.put(RESOLUTION_KEY+resolutions+ADAM_TAG_NAME,
                holder.getTagName());
            mAttributes.put(RESOLUTION_KEY+resolutions+ADAM_TAG_FRAG_NAME,
                holder.getDescription());
            mAttributes.put(RESOLUTION_KEY+resolutions+ADAM_RESOLUTION_TYPE,
                holder.getResType());
            mAttributes.put(ADAM_RESOLUTION_COUNT,resolutions+1);

        }

        // update severity
        int newSeverity = getSeverityAsInt(holder.getSeverity());

        if (oldSeverity<newSeverity) {
            mAttributes.put(IMarker.SEVERITY,newSeverity);

        }

        // update message
        if (resolutions>0) {
            mAttributes.put(IMarker.MESSAGE,MULTIPLE_ISSUES);
        }
        else {
            mAttributes.put(IMarker.MESSAGE,holder.getMessage());
        }

        // add result to marker
        mAttributes.put((ADAM_RESULT_KEY+results),holder.getMessage());

        // update results count
        mAttributes.put(ADAM_RESULT_COUNT,results+1);
        return mAttributes;
    }

    /**
     * @param severity
     * @return the int value of severity
     */
    private static int getSeverityAsInt(String severity) {
        int severityAsInt = 0;
        if (severity.equalsIgnoreCase(Severity.ERROR.value())) {
            severityAsInt = IMarker.SEVERITY_ERROR;

        }
        else if (severity.equalsIgnoreCase(Severity.WARNING.value())) {
            severityAsInt = IMarker.SEVERITY_WARNING;

        }
        else {
            severityAsInt = IMarker.SEVERITY_INFO;

        }
        return severityAsInt;

    }

}
