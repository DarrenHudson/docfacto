/**
 * 
 */
package com.docfacto.adam.plugin.markers;

import java.util.List;

import org.eclipse.core.resources.IResource;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.EnumConstantDeclaration;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jdt.core.dom.TextElement;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import com.docfacto.common.StringUtils;
import com.docfacto.output.generated.Fix;
import com.docfacto.output.generated.FixType;
import com.docfacto.output.generated.Result;

/**
 * A wrapper class around the marker
 * 
 * @author pemi - created Jun 19, 2013 7:55:11 PM
 * @since 2.2
 */
public class AdamMarkerWrapper {

    private Integer theStartPosition;
    private Integer theLength;
    private int theResolutionType;
    private String theTagName;
    private String theDescription;
    private final Result theResult;
    private final IResource theResource;

    /**
     * Constructor.
     * 
     * @param resource source file
     * @param node AST Node
     * @param result from Adam Processor
     * @since 2.4
     */
    public AdamMarkerWrapper(IResource resource,ASTNode node,Result result) {
        theResult = result;
        theResource = resource;
        // Default length
        theLength = 1;

        Fix errorFix = result.getFix();

        // get the tag name if one exists
        if (errorFix!=null) {

            if (!StringUtils.nullOrEmpty(errorFix.getTagName())) {
                if (!errorFix.getTagName().startsWith("@")) {
                    theTagName = "@"+errorFix.getTagName();
                }
                else {
                    theTagName = errorFix.getTagName();
                }
            }
            else {
                theTagName = "";
            }

            // get description
            if (errorFix.getDescription()==null) {
                theDescription = "";
            }
            else {
                theDescription = errorFix.getDescription();
            }

            setResolutionTypeInt(errorFix.getType());
        }

        if (node==null) {
            theStartPosition = result.getPosition().getColumn();
            theLength = 1;
        }
        else {
            processNode(node);
        }
    }

    /**
     * Look at the node and try and find its start position
     * 
     * @param node to process
     * @since 2.4
     */
    private void processNode(ASTNode node) {
        theStartPosition = node.getStartPosition();

        if (node instanceof BodyDeclaration) {
            Javadoc javadoc = ((BodyDeclaration)node).getJavadoc();
            if (javadoc!=null) {
                theStartPosition = javadoc.getStartPosition();
                theLength = 3;
                return;
            }
        }

        switch (node.getNodeType()) {
        case ASTNode.JAVADOC:
            theLength = 3;
            break;

        case ASTNode.TAG_ELEMENT:
            TagElement tagElement = (TagElement)node;
            theLength = tagElement.getLength();
            theTagName = tagElement.getTagName();
            break;

        case ASTNode.TEXT_ELEMENT:
            TextElement textElement = (TextElement)node;
            theLength = textElement.getLength();
            break;

        case ASTNode.METHOD_DECLARATION:
            MethodDeclaration methodNode = (MethodDeclaration)node;
            theLength = methodNode.getName().getLength();
            break;

        case ASTNode.TYPE_DECLARATION:
            TypeDeclaration typeNode = (TypeDeclaration)node;
            theLength = typeNode.getName().getLength();
            break;

        case ASTNode.ENUM_DECLARATION:
            EnumDeclaration enumNode = (EnumDeclaration)node;
            theLength = enumNode.getName().getLength();
            break;

        case ASTNode.ENUM_CONSTANT_DECLARATION:
            EnumConstantDeclaration enumConstNode =
                (EnumConstantDeclaration)node;
            theStartPosition = enumConstNode.getName().getStartPosition();
            theLength = enumConstNode.getName().getLength();
            break;

        case ASTNode.FIELD_DECLARATION:
            FieldDeclaration fieldNode = (FieldDeclaration)node;
            List<?> tagFrags = fieldNode.fragments();
            Object obj = tagFrags.get(tagFrags.size()-1);

            if (obj instanceof VariableDeclarationFragment) {
                VariableDeclarationFragment lastElement =
                    (VariableDeclarationFragment)obj;
                theStartPosition = lastElement.getName().getStartPosition();
                theLength = lastElement.getName().getLength();
            }
            else {
                theStartPosition = fieldNode.getStartPosition();
                theLength = fieldNode.getLength();
            }
            break;

        case ASTNode.VARIABLE_DECLARATION_FRAGMENT:
            VariableDeclarationFragment varDecl =
                (VariableDeclarationFragment)node;
            theStartPosition = varDecl.getName().getStartPosition();
            theLength = varDecl.getName().getLength();
            break;

        case ASTNode.SIMPLE_NAME:
            if (node.getParent() instanceof BodyDeclaration) {
                processNode(node.getParent());
                break;
            }

            SimpleName simpleName = (SimpleName)node;
            theStartPosition = simpleName.getStartPosition();
            theLength = simpleName.getLength();
            break;

        case ASTNode.MODIFIER:
        case ASTNode.BLOCK:
            ASTNode parent = node.getParent();
            if (parent!=null) {
                processNode(node.getParent());
                break;
            }

            // Watch the fall through..

        default:
            // default length..
            theLength = 1;
            System.out.println("I don't know how to handle .."+
                node.getNodeType()+" "+node.getClass().getName()+
                " "+theResult.getPosition().getLine()+" "+node);
        }
    }

    /**
     * Get start position
     * 
     * @return the start
     * @since 2.4
     */
    public Integer getStart() {
        return theStartPosition;
    }

    /**
     * Get message
     * 
     * @return the message
     * @since 2.4
     */
    public String getMessage() {
        return theResult.getRule().getMessage();
    }

    /**
     * @return the lineNumber
     */
    public Integer getLineNumber() {
        return theResult.getPosition().getLine();
    }

    /**
     * @return the length
     */
    public Integer getLength() {
        return theLength;
    }

    /**
     * @return the resource
     */
    public IResource getResource() {
        return theResource;
    }

    /**
     * Return the severity
     * 
     * @return the severity
     * @since 2.2
     */
    public String getSeverity() {
        return theResult.getSeverity();
    }

    /**
     * Return the resolution type
     * 
     * @return the resolution type
     */
    public int getResType() {
        return theResolutionType;
    }

    /**
     * Method to get the integer value of resolution type for setting marker
     * attributes
     * 
     * @param fixType The FixType to be processed
     * <p>
     * would return 0 if FixType is null or MarkerCreator.RESOLUTION_MODIFY;
     * </p>
     */
    private void setResolutionTypeInt(FixType fixType) {

        switch (fixType) {
        case ADD:
            theResolutionType = MarkerCreator.RESOLUTION_ADD;
            break;

        case REMOVE:
            theResolutionType = MarkerCreator.RESOLUTION_REMOVE;
            break;

        default:
            theResolutionType = 0;
            break;
        }
    }

    /**
     * Return the tag name
     * 
     * @return the tagName
     * @since 2.2
     */
    public String getTagName() {
        return theTagName;
    }

    /**
     * Return the fix description
     * 
     * @return the tagFragment
     * @since 2.2
     */
    public String getDescription() {
        return theDescription;
    }

}
