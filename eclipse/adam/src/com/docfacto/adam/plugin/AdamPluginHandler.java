package com.docfacto.adam.plugin;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * This handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 * 
 * @author pemi
 * @since 2.1
 * 
 */
public class AdamPluginHandler extends AbstractHandler {

    
    /**
     * The constructor.
     */
    public AdamPluginHandler() {
    }

    /**
     * 
     * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
     * @docfacto.adam ignore
     */
    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
        // toggle Adam command
        Command command = event.getCommand();
        final boolean preExecutionState = HandlerUtil.toggleCommandState(command);
        
        // use the old value and perform the operation
        if (preExecutionState) {
        	//turn off listeners        	
        	DocfactoAdamPlugin.getDefault().removeListeners();
        }
        else {
            //turn on listeners
        	DocfactoAdamPlugin.getDefault().addListeners();
        }

        return null;
    }


}
