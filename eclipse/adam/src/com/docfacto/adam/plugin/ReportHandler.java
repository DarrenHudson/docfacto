package com.docfacto.adam.plugin;

import java.util.concurrent.TimeUnit;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.ui.IWorkingCopyManager;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.eclipse.ui.handlers.HandlerUtil;

import com.docfacto.adam.AdamOutputProcessor;
import com.docfacto.adam.report.AdamReporter;
import com.docfacto.adam.report.IAdamReporter;
import com.docfacto.adam.report.ReportWithoutMarkers;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.output.generated.Statistic;

/**
 * @author pemi - created Mar 21, 2013 12:11:31 PM
 * 
 * @since 2.2
 */
public class ReportHandler extends AbstractHandler {

    private IEditorPart cuEditor;
    private IWorkingCopyManager manager;
    private IEditorInput editorInput;
    private final String ADAM_CONSOLE = "Adam Report";
    private MessageConsoleStream out;
    private MessageConsole adamConsole;
    private AdamOutputProcessor adamMSG;
    private int selectionType;
    private String consoleMsg;
    private IJavaElement javaElement;
    private IAdamReporter adamReporter;
    private Job job;
    private boolean withMarkers;

    /**
     * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
     */
    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {

        //        System.out.println(event.getCommand().getId());
        
        


        consoleMsg = "";
        final IWorkbenchWindow window =
        HandlerUtil.getActiveWorkbenchWindowChecked(event);
        ISelection selection = window.getSelectionService().getSelection();

        String viewID = IConsoleConstants.ID_CONSOLE_VIEW;

        adamConsole = findConsole(ADAM_CONSOLE);
        adamConsole.clearConsole();
        out = adamConsole.newMessageStream();
        String commandID = event.getCommand().getId();
        
        adamReporter = new ReportWithoutMarkers();
        

        if(commandID.equals("com.docfacto.adam.report.markersless")){
            withMarkers = false;

        }else {
            withMarkers = true;
        }


        try {
            // Display the console view
            PlatformUI.getWorkbench()
            .getActiveWorkbenchWindow().getActivePage()
            .showView(viewID);

        }
        catch (PartInitException ex) {
            DocfactoAdamPlugin
            .logException("ReportHandler "+ex.getMessage(),ex);
        }

        // Check if selection is a structured selection
        if (selection instanceof IStructuredSelection) {

            IStructuredSelection ss = (IStructuredSelection)selection;
            Object firstElement = ss.getFirstElement();
            if (firstElement instanceof IJavaElement) {

                javaElement = (IJavaElement)firstElement;

            }
        }

        // Check if selection is an editor input
        else if (selection!=null) {
            manager = JavaUI.getWorkingCopyManager();
            cuEditor = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
            .getActivePage().getActiveEditor();
            editorInput = cuEditor.getEditorInput();

            try {
                manager.connect(editorInput);
            }
            catch (CoreException e) {
                e.printStackTrace(System.err);
            }

            javaElement = manager.getWorkingCopy(cuEditor.getEditorInput());

        }

        // specify selected element type
        if (javaElement!=null) {
            selectionType = javaElement.getElementType();
        }
        else {
            selectionType = 0;
        }

        // Generate report for the resource.
        // Create Job for report generation
        job = new Job("Generating adam report") {

            @Override
            protected IStatus run(IProgressMonitor monitor) {
                monitor.beginTask("Generating report",10);
                monitor.worked(1);
                Thread reporterThread;
                        
                        try {
                            reporterThread = new Thread() {
                                public void run() {

                                    /*
                                     * (My shit list)
                                     * ======================
                                     * 
                                     * 
                                     * At the moment, the AdamProcessor is run twice
                                     * First run gets the report and adds markers to the resource
                                     * Second run gets the processor without markers
                                     * 
                                     * This should be redesigned to run just once.  
                                     */
                                    switch (selectionType) {

                                    case IJavaElement.PACKAGE_FRAGMENT:
                                        consoleMsg = "Package Fragment";
                                        if(withMarkers){
                                            new AdamReporter().getPackageReport((IPackageFragment)javaElement,true);
                                        }
                                        adamMSG =
                                            adamReporter.getPackageReport((IPackageFragment)javaElement , true);
                                        break;

                                    case IJavaElement.COMPILATION_UNIT:
                                        consoleMsg = "Java File";
                                        if(withMarkers){
                                            new AdamReporter().getCompilationUnitReport((ICompilationUnit)javaElement);
                                        }
                                        adamMSG =
                                            adamReporter.getCompilationUnitReport((ICompilationUnit)javaElement);
                                        break;

                                    case IJavaElement.JAVA_PROJECT:
                                        consoleMsg = "Java Project";
                                        if(withMarkers){
                                            new AdamReporter().getProjectReport(javaElement.getJavaProject());
                                        }
                                        
                                        adamMSG =
                                            adamReporter.getProjectReport(javaElement.getJavaProject());
                                        break;

                                    case IJavaElement.PACKAGE_FRAGMENT_ROOT:
                                        consoleMsg = "Source Folder";
                                        if(withMarkers){
                                            new AdamReporter().getSourceFolderReport(javaElement);
                                        }
                                        
                                        adamMSG = adamReporter.getSourceFolderReport(javaElement);
                                        break;

                                    default:
                                        MessageDialog
                                            .openInformation(window.getShell(),
                                                "Info",
                                                "Unable to generate report for selected resource");

                                    }
                                 

                                    if(!Thread.interrupted()){
                                        
                                        //Print stats when the runnable is finished
                                        printStatistic(adamMSG);
                                    }
                                    
                                    
                                    
                                }
                                
                            };
                            
                         // Print out report header
                            out.println("Generating report for "+consoleMsg+": ["
                            +javaElement.getElementName()+"]");
                            
                        reporterThread.setName("runnable_reporter");
                        reporterThread.start();
                        
                        int i = 0;
                        while(job.getState()!=Job.NONE && reporterThread.isAlive()){
                            
                            if(monitor.isCanceled()){
                             // Obtain the Platform job manager
                                
                                    adamMSG = null;
                                    reporterThread.interrupt();
                                    out.println("...interrupted");
                                
                                job.cancel();
                               
                               
                                
                                return Status.CANCEL_STATUS; 
                            }else{
                                try {
                                    if (i<8)
                                        monitor.worked(1);
                                    i++;
                                    TimeUnit.SECONDS.sleep(1);

                                }
                                catch (InterruptedException e) {
                                    DocfactoCorePlugin.logException("InterruptedException",e);
                                }
                            }
                               
                        }
                
                }finally{
                   monitor.done(); 
                }
                
                return Status.OK_STATUS; 
            }                                        //end of Job#run()

        };                                           //end of Job block

        job.setUser(true);
        job.schedule();
        return null;

    }

    /**
     * Print a statistic to the console
     * 
     * @param adamProcessor print stats from <code>AdamOutputProcessor</code>
     * 
     */
    private void printStatistic(AdamOutputProcessor adamProcessor) {

        if (adamProcessor==null) {
            return;
        }

        for (Statistic stat:adamProcessor.getStatistics()) {

            if (stat.getValue()>0) {
                out.println("\t"+stat.getRule().getMessage()+" "+
                stat.getValue());
            }
        }
    }

    /**
     * Method gets the specified console
     * 
     * @param consoleName name of console
     * @return <code>MessageConsole</code>
     */
    public MessageConsole findConsole(String consoleName) {
        ConsolePlugin plugin = ConsolePlugin.getDefault();
        IConsoleManager conMan = plugin.getConsoleManager();
        IConsole[] existing = conMan.getConsoles();

        for (int i = 0;i<existing.length;i++) {
            if (consoleName.equals(existing[i].getName())) {
                return (MessageConsole)existing[i];
            }
        }

        // no console found, so create a new one
        MessageConsole msgConsole = new MessageConsole(consoleName,null);
        conMan.addConsoles(new IConsole[] {msgConsole});
        return msgConsole;

    }

}
