package com.docfacto.adam.plugin;

/**
 * Plugin Constants
 * 
 * @author dhudson - created 3 Oct 2013
 * @since 2.4
 */
public interface IDocfactoAdamPlugin {

    public static final String ADAM_ICON_12 = "AdamIcon12x12.png";

    public static final String ADAM_ICON_16 = "AdamIcon16x16.png";

    public static final String ADAM_ICON_24 = "AdamIcon24x24.png";

    public static final String FIX_ALL = "FixAll.png";

    // Add to this list of images so that the plugin can load them
    public static final String[] IMAGES = {ADAM_ICON_12,
        ADAM_ICON_16,ADAM_ICON_24,FIX_ALL};
}
