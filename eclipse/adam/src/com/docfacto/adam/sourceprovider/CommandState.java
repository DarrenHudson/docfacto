package com.docfacto.adam.sourceprovider;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.ui.AbstractSourceProvider;
import org.eclipse.ui.ISources;

public class CommandState extends AbstractSourceProvider {
	
	public final static String STATE = "com.docfacto.adam.sourceprovider.active";
    public final static String DISABLED = "DISABLED";
    public final static String ENABLED = "ENABLED";
    private boolean enabled = true;


	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Map getCurrentState() {
		Map map = new HashMap(1);
	    String value = enabled ? ENABLED : DISABLED;
	    map.put(STATE, value);
	    return map;
	  }

	@Override
	public String[] getProvidedSourceNames() {
		return new String[] {STATE};
	}
	
	public void setDisabled(boolean disable) {
	    enabled = !disable ;
	    String value = enabled ? ENABLED : DISABLED;
	    fireSourceChanged(ISources.WORKBENCH, STATE, value);
	  }

}