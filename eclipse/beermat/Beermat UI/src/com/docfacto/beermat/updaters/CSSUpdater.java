package com.docfacto.beermat.updaters;

import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;

/**
 * Upadte to create a style sheet instruction
 * 
 * @author dhudson - created 5 Dec 2013
 * @since 2.5
 */
public class CSSUpdater extends AbstractUpdater {
    private final String theCSSPath;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param path to ccs file
     * @since 2.5
     */
    public CSSUpdater(BeermatController controller,String path) {
        super(controller);
        theCSSPath = path;
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        StringBuilder builder = new StringBuilder(50);
        builder.append("type=\"text/css\" ");
        builder.append("href=\"");
        builder.append(theCSSPath);
        builder.append("\"");

        final SVGDocument doc = getController().getManager().getDocument();
        final ProcessingInstruction pi = doc.createProcessingInstruction
            ("xml-stylesheet",builder.toString());

        doc.insertBefore(pi,doc.getRootElement());

        // Might need a redraw here
        repaint();

        return true;
    }

}
