package com.docfacto.beermat.updaters;

import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler;
import com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData;

/**
 * Updater to handle shape dragging
 * 
 * @author dhudson - created 5 Dec 2013
 * @since 2.5
 */
public class ShapeUpdater extends AbstractUpdater {

    private final ShapeHandler theHandler;
    private final ShapeUpdateData theData;
    private final Element theElement;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param element to change
     * @param handler shape handler
     * @param data to use
     * @since 2.5
     */
    public ShapeUpdater(BeermatController controller,Element element,
    ShapeHandler handler,ShapeUpdateData data) {
        super(controller);
        theElement = element;
        theHandler = handler;
        theData = data;
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        theHandler.drag(theElement,theData);
        return true;
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#saveState()
     */
    @Override
    public boolean saveState() {
        return false;
    }

}
