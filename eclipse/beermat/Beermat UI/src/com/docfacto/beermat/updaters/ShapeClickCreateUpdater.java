package com.docfacto.beermat.updaters;

import java.util.ArrayList;

import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.listeners.ShapeToolInputListener;
import com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler;
import com.docfacto.common.NameValuePair;

/**
 * Create a shape the same size as the last one.
 * 
 * @author dhudson - created 20 Mar 2014
 * @since 2.4
 */
public class ShapeClickCreateUpdater extends AbstractUpdater {

    private ShapeHandler theHandler;
    private Point theLocation;
    private Element theLastElement;
    private final ShapeToolInputListener theInputListener;
    
    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param handler shape handler
     * @param loc location
     * @param lastElement last element created
     * @param inputListener shape tool input listener
     */
    public ShapeClickCreateUpdater(BeermatController controller,
    ShapeHandler handler,Point loc,Element lastElement,ShapeToolInputListener inputListener) {
        super(controller);
        theHandler = handler;
        theLocation = loc;
        theLastElement = lastElement;
        theInputListener = inputListener;
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        Element element = theHandler.mouseClick(theLocation,theLastElement);
        ArrayList<NameValuePair> attrs =
            getController().getView().getControlToolBar()
                .getElementAttributes(element);
        new SetAttributesAction(attrs).setAttributes(element);

        theInputListener.setElement(element);
        
        return true;
    }

}
