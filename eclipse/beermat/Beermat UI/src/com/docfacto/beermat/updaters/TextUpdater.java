package com.docfacto.beermat.updaters;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.svg.TextElement;

/**
 * Simple text updater
 * 
 * @author dhudson - created 5 Dec 2013
 * @since 2.5
 */
public class TextUpdater extends AbstractUpdater {

    private final TextElement theTextElement;
    private final String theText;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param element text element
     * @param text to set
     * @since 2.5
     */
    public TextUpdater(BeermatController controller,TextElement element,String text) {
        super(controller);
        theTextElement = element;
        theText = text;
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        theTextElement.setTextContents(theText);
        return true;
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#notifyHighlightManager()
     */
    @Override
    public boolean notifyHighlightManager() {
        return true;
    }

}
