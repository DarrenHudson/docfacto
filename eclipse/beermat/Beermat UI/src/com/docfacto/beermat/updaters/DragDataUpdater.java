package com.docfacto.beermat.updaters;

import java.util.List;

import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.svg.DragData;
import com.docfacto.beermat.svg.ShapeShifter;
import com.docfacto.beermat.ui.BeermatAWTCanvas;

/**
 * Dragging shape updater
 * 
 * @author dhudson - created 5 Dec 2013
 * @since 2.5
 */
public class DragDataUpdater extends AbstractUpdater {

    private final DragData theDragData;

    /**
     * Constructor.
     * 
     * Only queue the updates, if there is something to update.
     * 
     * @param controller beermat controller
     * @param data for shape shifter
     * @since 2.5
     */
    public DragDataUpdater(BeermatController controller,DragData data) {
        super(controller);
        theDragData = data;
        if (controller.getBeermatAWTCanvas().getHighlightManager().hasSelectedElements()) {
            enqueue();
        }
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {

        BeermatAWTCanvas canvas = getController().getBeermatAWTCanvas();
        List<Element> highlighted = canvas.getHighlightManager().getHighlightedElements();
        
        for (Element element:highlighted) {
            ShapeShifter.updateElement(element,theDragData);
        }

        ShapeShifter.updateAnchoredElements(canvas,highlighted,getController().getViewBoxTransform());

        repaint();
        return true;
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#saveState()
     */
    @Override
    public boolean saveState() {
        return false;
    }

}
