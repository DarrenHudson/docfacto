package com.docfacto.beermat.updaters;

import java.util.ArrayList;

import org.apache.batik.dom.svg.SVGOMGElement;
import org.apache.batik.dom.svg.SVGOMTextElement;
import org.w3c.dom.NodeList;

import com.docfacto.beermat.controller.BeermatController;

/**
 * Group Text updater.
 * 
 * @author dhudson - created 5 Dec 2013
 * @since 2.5
 */
public class SimpleGroupTextUpdater extends AbstractTextUpdater {

    private final SVGOMGElement theGroup;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param group group to process
     * @param lines to add
     * @since 2.5
     */
    public SimpleGroupTextUpdater(BeermatController controller,
    SVGOMGElement group,String[] lines) {
        super(controller,lines,null,null);
        theGroup = group;
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        
        ArrayList<SVGOMTextElement> elements =
            new ArrayList<SVGOMTextElement>();

        NodeList nl = theGroup.getChildNodes();
        for (int i = 0;i<nl.getLength();i++) {
            if (nl.item(i) instanceof SVGOMTextElement) {
                elements.add((SVGOMTextElement)nl.item(i));
            }
        }
        
        // new lines back in
        addTextLines(theGroup,elements,getLines());

        updateAnchorPosition();
        
        return true;
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#notifyHighlightManager()
     */
    @Override
    public boolean notifyHighlightManager() {
        return true;
    }

}
