package com.docfacto.beermat.updaters;

import java.util.Collections;
import java.util.List;

import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.utils.ElementMovement;
import com.docfacto.beermat.utils.ElementUtils;

/**
 * Handle moving elements on the canvas
 * 
 * @author dhudson - created 17 Aug 2013
 * @since 2.4
 * @docfacto.link
 * uri="${doc-beermat}/palettebar/c_bring_backFront_palette_item.dita"
 * key="layer-action" version="2.4.2"
 */
public class LayerUpdater extends AbstractUpdater {

    private final ElementMovement theDirection;

    /**
     * Constructor.
     * 
     * @param controller Beermat Controller
     * @param direction of movement
     * @since 2.4
     */
    public LayerUpdater(BeermatController controller,ElementMovement direction) {
        super(controller);
        theDirection = direction;
        if (controller.getBeermatAWTCanvas().getHighlightManager()
            .hasSelectedElements()) {
            enqueue();
        }
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        final List<Element> elements =
            getController().getBeermatAWTCanvas().getHighlightManager()
                .getHighlightedElements();

        // (Only effects multiple elements and only sending to back
        // Must be reversed otherwise when sending to back, the foremost element
        // becomes the rear most
        if (theDirection==ElementMovement.BACK) {
            Collections.reverse(elements);
        }
        for (Element element:elements) {
            ElementUtils.moveElement(element,theDirection);
        }
        return true;
    }

}
