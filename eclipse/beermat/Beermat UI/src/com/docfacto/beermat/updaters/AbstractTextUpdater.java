package com.docfacto.beermat.updaters;

import java.util.List;

import org.apache.batik.dom.svg.SVGOMTextElement;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.hotspot.HotSpot;
import com.docfacto.beermat.svg.ShapeShifter;
import com.docfacto.beermat.svg.TextHandler;
import com.docfacto.beermat.utils.ElementUtils;
import com.docfacto.common.NameValuePair;

/**
 * Abstract class that text updates should extend as this contains helper
 * methods
 * 
 * @author dhudson - created 5 Dec 2013
 * @since 2.5
 */
public abstract class AbstractTextUpdater extends AbstractUpdater {

    private final String[] theLines;
    private final HotSpot theHotSpot;
    private final Element theAnchorElement;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param lines the lines from the dialog box
     * @param hotSpot for the text
     * @param anchorElement text is anchored
     * @since 2.5
     */
    public AbstractTextUpdater(BeermatController controller,String[] lines,
    HotSpot hotSpot,Element anchorElement) {
        super(controller);
        theLines = lines;
        theHotSpot = hotSpot;
        theAnchorElement = anchorElement;
    }

    /**
     * Set Font attributes from the widget
     * 
     * @param element to set the attributes to.
     * @since 2.4
     */
    public void setWidgetAttributes(Element element) {
        List<NameValuePair> attrs =
            getController().getView().getControlToolBar().getFontWidget()
                .getAttributesForElement(element);
        for (NameValuePair attr:attrs) {
            // Set the attribute for the element
            element.setAttribute(attr.getName(),
                attr.getValue());
        }
    }

    /**
     * If text has been placed over a hot spot, then request a redraw to remove
     * the hot spot
     * 
     * @since 2.4
     */
    public void updateDirtyHotSpot() {
        if (theHotSpot!=null) {
            getController().getBeermatAWTCanvas().repaint(
                theHotSpot.getBounds());
        }
    }

    /**
     * Set the anchor and hot spot attributes on the element
     * 
     * @param element to set the attributes on
     * @since 2.4
     */
    public void setHotSpotAttributes(Element element) {
        TextHandler.setAnchorAttributes(element,theAnchorElement,theHotSpot
            .getLocation());
    }

    /**
     * Set the hot spot attributes and repaint the canvas for the hot spot
     * 
     * @param element to set the hot spot attributes for
     * @since 2.5
     */
    public void applyHotSpotAttributes(Element element) {
        if (theHotSpot!=null) {
            setAnchorAttribute(element);
            setHotSpotAttributes(element);
            updateDirtyHotSpot();
        }
    }

    /**
     * Adds element to the owner document
     * 
     * @param element to append
     * @since 2.5
     */
    public void addToDocument(Element element) {
        element.getOwnerDocument().getDocumentElement().appendChild(element);
    }

    /**
     * Depending on the hot spot, set the correct anchor attribute
     * 
     * @param element to set the anchor attribute
     * @since 2.4
     */
    public void setAnchorAttribute(Element element) {
        if (theHotSpot==null) {
            // Nothing to do
            return;
        }

        switch (theHotSpot.getLocation()) {
        case HotSpot.CENTRE:
        case HotSpot.NORTH:
        case HotSpot.SOUTH:
            element.setAttribute("text-anchor","middle");
            break;

        case HotSpot.EAST:
            element.setAttribute("text-anchor","start");
            break;

        case HotSpot.WEST:
            element.setAttribute("text-anchor","end");
            break;
        }
    }

    /**
     * Utility method to add an array of lines to a parent element
     * 
     * {@docfacto.note Only use if purely creating new text elements within the
     * given parent}
     * 
     * @param parent element
     * @param lines to add
     * @since 2.4
     */
    public void addTextLines(Element parent,String[] lines) {
        for (int i = 0;i<lines.length;i++) {
            int y = i*15;
            Element text =
                ElementUtils.createText(parent.getOwnerDocument(),lines[i]);
            text.setAttribute("y",y+"");
            parent.appendChild(text);
        }
    }

    /**
     * Add an array of line to the parent.
     * 
     * {@docfacto.note should always be used if the holding group contained text
     * elements already, these may contain titles or tspans, and even if the
     * lines of text have changed these should remain}
     * 
     * @param parent The parent to add lines to
     * @param existingText The existing lines of text
     * @param lines The lines to set
     * @since 2.5.1
     */
    public void addTextLines(Element parent,
    List<SVGOMTextElement> existingText,String[] lines) {

        int y = 0;
        for (int i = 0;i<lines.length;i++) {
            if (i<existingText.size()) {
                SVGOMTextElement text = existingText.get(i);
                text.setTextContent(lines[i]);
                y = Integer.parseInt(text.getAttribute("y"));
            }
            else {
                // Need to create a text element..
                Element text =
                    ElementUtils.createText(parent.getOwnerDocument(),lines[i]);
                text.setAttribute("y",Integer.toString(y));
                parent.appendChild(text);
                y = y+15;
            }
        }

        if (lines.length<existingText.size()) {
            // Remove the trailing text lines
            for (int i = lines.length;i<existingText.size();i++) {
                SVGOMTextElement text = existingText.get(i);
                parent.removeChild(text);
            }
        }
    }

    /**
     * Return the anchor element
     * 
     * @return the anchor element or null
     * @since 2.5
     */
    public Element getAnchorElement() {
        return theAnchorElement;
    }

    /**
     * Return the lines from the dialog
     * 
     * @return the text lines in the dialog
     * @since 2.5
     */
    public String[] getLines() {
        return theLines;
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#notifyHighlightManager()
     */
    @Override
    public boolean notifyHighlightManager() {
        return true;
    }

    /**
     * Return the hot spot or null
     * 
     * @return the hot spot or null
     * @since 2.5
     */
    public HotSpot getHotSpot() {
        return theHotSpot;
    }

    /**
     * Reposition the text if required.
     * 
     * @since 2.5
     */
    public void updateAnchorPosition() {
        if (theAnchorElement!=null) {
            ShapeShifter.findAndUpdateText(getController()
                .getBeermatAWTCanvas(),theAnchorElement,getController()
                .getViewBoxTransform());
        }
    }
}
