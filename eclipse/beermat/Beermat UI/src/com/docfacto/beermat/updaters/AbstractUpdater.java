package com.docfacto.beermat.updaters;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.HighlightManagerEvent;
import com.docfacto.beermat.events.SVGDirtyEvent;
import com.docfacto.beermat.plugin.BeermatUIPlugin;

/**
 * All updates to a live XML element must be done via an updater.
 * 
 * @author dhudson - created 4 Dec 2013
 * @since 2.5
 */
public abstract class AbstractUpdater implements Runnable {

    private final BeermatController theController;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @since 2.5
     */
    public AbstractUpdater(BeermatController controller) {
        theController = controller;
    }

    /**
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        try {
            if (applyUpdates()) {
                theController.processEvent(new SVGDirtyEvent(this));
                if (notifyHighlightManager()) {
                    theController.processEvent(new HighlightManagerEvent(this));
                }
            }
        }
        catch (Throwable t) {
            BeermatUIPlugin.logException(
                this.getClass().getName()+"failed "+t.getMessage(),t);
        }

    }

    /**
     * Repaint the canvas
     * 
     * @since 2.4
     */
    public void repaint() {
        theController.getBeermatAWTCanvas().immediateRepaint();
    }

    /**
     * Return the beermat controller
     * 
     * @return beermat controller
     * @since 2.5
     */
    public BeermatController getController() {
        return theController;
    }

    /**
     * Sumbit this class to the Update Thread Manager queue.
     * 
     * @since 2.5
     */
    public void enqueue() {
        theController.applyUpdates(this);
    }

    /**
     * This method is executed with the Update Manager thread.
     * 
     * @return true if there has been updates
     * @since 2.5
     */
    public abstract boolean applyUpdates();

    /**
     * Flag for undo / redo.
     * 
     * Set to true by this implementation, set to false if not required.
     * 
     * @return true if the state is required to be saved.
     * @since 2.5
     */
    public boolean saveState() {
        return true;
    }

    /**
     * Notify the highlight manager if applyUpdates return true.
     * 
     * Set to false by this implementation, override if required.
     * 
     * @return true if the highlight manager needs to be notified.
     * @since 2.5
     */
    public boolean notifyHighlightManager() {
        return false;
    }
}
