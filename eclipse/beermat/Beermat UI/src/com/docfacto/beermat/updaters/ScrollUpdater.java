package com.docfacto.beermat.updaters;

import java.awt.geom.AffineTransform;

import com.docfacto.beermat.controller.BeermatController;

public class ScrollUpdater extends AbstractUpdater {

    private final AffineTransform theTransform;
    
    public ScrollUpdater(BeermatController controller,AffineTransform transform) {
        super(controller);
        theTransform = transform;
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#saveState()
     */
    @Override
    public boolean saveState() {
        return false;
    }

    @Override
    public boolean applyUpdates() {
        getController().getBeermatAWTCanvas().setRenderingTransform(theTransform);
        return false;
    }

}
