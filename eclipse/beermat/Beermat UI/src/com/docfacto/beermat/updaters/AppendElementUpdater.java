package com.docfacto.beermat.updaters;

import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;

/**
 * General updater to append an element to the end of a document.
 * 
 * @author dhudson - created 4 Dec 2013
 * @since 2.5
 */
public class AppendElementUpdater extends AbstractUpdater {

    private final Element theElement;

    /**
     * Constructor.
     * 
     * @param controller beermat controller;
     * @param element to append
     * @since 2.5
     */
    public AppendElementUpdater(BeermatController controller,Element element) {
        super(controller);
        theElement = element;
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        theElement.getOwnerDocument().getDocumentElement()
            .appendChild(theElement);
        return true;
    }

}
