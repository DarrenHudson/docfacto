package com.docfacto.beermat.updaters;

import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGRect;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.hotspot.HotSpot;
import com.docfacto.beermat.utils.ElementSizer;
import com.docfacto.beermat.utils.SVGUtils;

/**
 * Updater used to create a group of text.
 * 
 * @author dhudson - created 5 Dec 2013
 * @since 2.5
 */
public class CreateTextGroupUpdater extends AbstractTextUpdater {

    private final Point2D theLocation;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param lines only the first line will be used
     * @param hotSpot for the connector
     * @param anchorElement the connector
     * @param location location of the mouse click
     * @since 2.5
     */
    public CreateTextGroupUpdater(BeermatController controller,String[] lines,
    HotSpot hotSpot,Element anchorElement,Point2D location) {
        super(controller,lines,hotSpot,anchorElement);
        theLocation = location;
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        // Get the attributes list and set them
        Element element = getController().getManager().getDocument()
            .createElementNS(SVGUtils.SVG_NAMESPACE,"g");

        element.setAttribute("class","textGroup");

        setWidgetAttributes(element);

        addTextLines(element,getLines());

        setTranslateTranform(element,getController()
            .getViewBoxTransform());

        // Set the anchor attribute
        setAnchorAttribute(element);

        addToDocument(element);

        applyHotSpotAttributes(element);
        return true;
    }

    /**
     * Add transform translate to a group of multi line text items
     * 
     * @param group to add the translate to
     * @param transform current viewbox transform
     * @since 2.4
     */
    public void setTranslateTranform(Element group,AffineTransform transform) {
        Point2D location = theLocation;

        if (getHotSpot()!=null) {
            SVGRect bbox =
                ElementSizer.INSTANCE
                    .getElementBounds(group);

            location =
                SVGUtils.getTextLocationWithHotSpot(group,getHotSpot(),
                    (int)bbox.getHeight(),transform);
        }else {
            location =
            SVGUtils.getTransformedPoint(
                theLocation.getX(),theLocation.getY(),
                getController()
                    .getViewBoxTransform());
        }

        SVGUtils.setTransformTranslate(group,location);
    }

}
