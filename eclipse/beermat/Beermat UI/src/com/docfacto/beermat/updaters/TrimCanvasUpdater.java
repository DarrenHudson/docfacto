package com.docfacto.beermat.updaters;

import java.awt.geom.Rectangle2D;
import java.util.List;

import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.SVGSizeEvent;
import com.docfacto.beermat.svg.DragData;
import com.docfacto.beermat.svg.ShapeShifter;
import com.docfacto.beermat.utils.BeermatUtils;
import com.docfacto.beermat.utils.SVGUtils;

/**
 * Trim Canvas logic which is run in the Batik update manager
 * 
 * @author dhudson - created 31 Aug 2013
 * @since 2.4
 */
public class TrimCanvasUpdater extends AbstractUpdater {

    /**
     * Constructor.
     * 
     * @param controller Beermat controller
     * @since 2.4
     */
    public TrimCanvasUpdater(BeermatController controller) {
        super(controller);
        enqueue();
    }

    /**
     * {@docfacto.media uri="doc-files/ShrinkToFit.png"}
     * 
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        // Get the graphics root node
        Rectangle2D rect =
            getController().getBeermatAWTCanvas().getCanvasGraphicsNode()
                .getBounds();

        int padding = 10;
        int offsetX = BeermatUtils.convertToInt(rect.getX());
        int offsetY = BeermatUtils.convertToInt(rect.getY());

        if (offsetX>padding) {
            offsetX -= padding;
        }

        if (offsetY>padding) {
            offsetY -= padding;
        }

        List<Element> elements =
            SVGUtils.getSVGElements(getController().getManager().getDocument());

        DragData d = new DragData();
        // Move all the elements by the negative offset
        d.setXDiff(-offsetX);
        d.setYDiff(-offsetY);
        
        // Move the elements
        for (Element element:elements) {
            ShapeShifter.updateElement(element,d);
        }

        // Move all of the anchorable elements
        ShapeShifter.updateAnchoredElements(getController()
            .getBeermatAWTCanvas(),elements,getController()
            .getViewBoxTransform());

        // Lets size the document
        int width = BeermatUtils.convertToInt(rect.getWidth())+(padding*2);
        int height = BeermatUtils.convertToInt(rect.getHeight())+(padding*2);

        SVGUtils.setSVGHeightWidth(getController().getManager().getDocument(),
            height,width);

        // Change the size widget
        getController().processEvent(
            new SVGSizeEvent(this,getController().getBeermatAWTCanvas()));

        // Lets repaint the image
        repaint();

        return true;
    }
}
