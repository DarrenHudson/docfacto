package com.docfacto.beermat.updaters;

import java.util.ArrayList;

import org.apache.batik.dom.svg.SVGOMTextElement;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.utils.ElementUtils;
import com.docfacto.beermat.utils.SVGUtils;

/**
 * Handle the complex updating of group text.
 * 
 * @author dhudson - created 5 Dec 2013
 * @since 2.5
 */
public class ComplexGroupTextUpdater extends AbstractTextUpdater {

    private final Element theGroup;
    private final boolean hadGroup;
    private final Element theOriginal;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param lines to set
     * @param group to set them to
     * @param didHaveGroup was already a group
     * @param original original element
     * @since 2.5
     */
    public ComplexGroupTextUpdater(BeermatController controller,String[] lines,
    Element group,boolean didHaveGroup,Element original) {
        super(controller,lines,null,null);
        theGroup = group;
        hadGroup = didHaveGroup;
        theOriginal = original;
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        if (hadGroup) {
            // Start from 1 to avoid duplicating the first
            // line
            for (int i = 1;i<getLines().length;i++) {
                addTextLineToNextPositionInGroup(theGroup,getLines()[i]);
            }
        }
        else {
            addTextLines(theGroup,getLines());
            theOriginal.getParentNode().replaceChild(theGroup,theOriginal);
        }

        setWidgetAttributes(theGroup);

        updateAnchorPosition();

        return true;
    }

    private void addTextLineToNextPositionInGroup(Element group,String line) {
        NodeList nl = group.getChildNodes();
        ArrayList<Element> children = new ArrayList<Element>(nl.getLength());
        for (int i = 0;i<nl.getLength();i++) {
            if (nl.item(i) instanceof SVGOMTextElement)
                children.add((Element)nl.item(i));
        }

        // get last element
        Element lastText = children.get(children.size()-1);
        int y = Integer.parseInt(lastText.getAttribute("y"))+15;

        Element newText =
            ElementUtils.createText(group.getOwnerDocument(),line);
        SVGUtils.setAttribute(newText,"y",y);
        group.appendChild(newText);
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#notifyHighlightManager()
     */
    @Override
    public boolean notifyHighlightManager() {
        return true;
    }

}
