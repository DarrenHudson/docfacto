package com.docfacto.beermat.updaters;

import org.apache.batik.dom.svg.SVGOMTextPathElement;

import com.docfacto.beermat.controller.BeermatController;

/**
 * Updater to change text path data.
 * 
 * @author dhudson - created 4 Dec 2013
 * @since 2.5
 */
public class ConnectorTextUpdater extends AbstractUpdater {

    private final String[] theLines;
    private final SVGOMTextPathElement theElement;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param textPath to modify
     * @param lines the lines
     * @since 2.5
     */
    public ConnectorTextUpdater(BeermatController controller,
    SVGOMTextPathElement textPath,String[] lines) {
        super(controller);
        theElement = textPath;
        theLines = lines;
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        theElement.setTextContent(theLines[0]);
        return true;
    }
}
