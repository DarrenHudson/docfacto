package com.docfacto.beermat.updaters;

import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.apache.batik.gvt.GraphicsNode;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.hotspot.HotSpot;
import com.docfacto.beermat.hotspot.NSEWHotSpots;
import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.beermat.svg.ConnectorHandler;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.DocfactoException;
import com.docfacto.common.NameValuePair;

/**
 * Create a connector between two selected elements.
 * 
 * @author dhudson - created 9 Apr 2014
 * @since 2.5
 */
public class CreateConnectorUpdater extends AbstractUpdater {

    /**
     * Constructor.
     * 
     * @param controller
     */
    public CreateConnectorUpdater(BeermatController controller) {
        super(controller);
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {

        List<Element> elements =
            getController().getBeermatAWTCanvas().getHighlightManager()
                .getHighlightedElements();

        if (elements.size()!=2) {
            return false;
        }

        Element start = elements.get(0);
        Element end = elements.get(1);

        Document doc = start.getOwnerDocument();
        Element path = doc.createElementNS(SVGUtils.SVG_NAMESPACE,"path");

        GraphicsNode startNode =
            getController().getBeermatAWTCanvas().getGraphicsNodeFor(start);
        GraphicsNode endNode =
            getController().getBeermatAWTCanvas().getGraphicsNodeFor(end);

        if (getTransformedPoint(startNode).getX()>getTransformedPoint(endNode)
            .getX()) {
            // Swap the start and end nodes..
            Element tmp = end;
            end = start;
            start = tmp;

            GraphicsNode tmpNode = endNode;
            endNode = startNode;
            startNode = tmpNode;
        }

        ConnectorHandler handler = new ConnectorHandler();

        try {
            NSEWHotSpots hotSpots =
                new NSEWHotSpots(getController().getBeermatAWTCanvas(),
                    startNode);

            HotSpot hotSpot = hotSpots.getHotSpotForLocation(HotSpot.EAST);

            Point2D newPoint =
                SVGUtils.getTransformedPoint(hotSpot.getBounds().getCenterX(),
                    hotSpot.getBounds().getCenterY(),
                    getController().getViewBoxTransform());

            handler.setStart(newPoint.getX(),newPoint.getY());
            handler.setStartConnectorAttributes(path,start,HotSpot.EAST);

            hotSpots =
                new NSEWHotSpots(getController().getBeermatAWTCanvas(),
                    endNode);

            hotSpot = hotSpots.getHotSpotForLocation(HotSpot.WEST);

            newPoint =
                SVGUtils.getTransformedPoint(hotSpot.getBounds().getCenterX(),
                    hotSpot.getBounds().getCenterY(),
                    getController().getViewBoxTransform());

            handler.setEnd(newPoint.getX(),newPoint.getY());
            handler.setEndConnectorAttributes(path,end,HotSpot.WEST);

            handler.updatePath(path);

            ArrayList<NameValuePair> attrs =
                getController().getView().getControlToolBar()
                    .getElementAttributes(path);
            new SetAttributesAction(attrs).setAttributes(path);

            path.setAttribute("class","connector");
            doc.getDocumentElement().appendChild(path);

            getController().getBeermatAWTCanvas().getHighlightManager()
                .removeHighlights();

        }
        catch (DocfactoException ex) {
            BeermatUIPlugin.logException("Unable to create connector",ex);
            return false;
        }

        return true;
    }

    /**
     * Return the transformed point of the graphics node
     * 
     * @param graphicsNode to process
     * @return the transformed point of the graphics node
     * @since 2.5
     */
    private Point2D getTransformedPoint(GraphicsNode graphicsNode) {
        AffineTransform elementsAt =
            graphicsNode.getGlobalTransform();
        AffineTransform at = getController().getViewBoxTransform();
        at.concatenate(elementsAt);

        Point2D point =
            new Point2D.Double(graphicsNode.getBounds().getX(),graphicsNode
                .getBounds().getY());
        return at.transform(point,null);
    }
}
