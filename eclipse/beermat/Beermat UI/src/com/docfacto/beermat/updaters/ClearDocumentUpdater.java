package com.docfacto.beermat.updaters;

import com.docfacto.beermat.controller.BeermatController;

/**
 * Updater to clear the document
 *
 * @author dhudson - created 4 Dec 2013
 * @since 2.5
 */
public class ClearDocumentUpdater extends AbstractUpdater {

    /**
     * Constructor.
     * @param controller beermat controller
     * @since 2.5
     */
    public ClearDocumentUpdater(BeermatController controller) {
        super(controller);
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        getController().getManager().clearDocument();
        return true;
    }

}
