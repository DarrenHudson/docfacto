package com.docfacto.beermat.updaters;

import com.docfacto.beermat.controller.BeermatController;

/**
 * Simple updater to delete selected items
 * 
 * @author dhudson - created 5 Dec 2013
 * @since 2.5
 */
public class DeleteHighlightedUpdater extends AbstractUpdater {

    private final boolean isDeleteAnchoredElements;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param deleteAnchoredElements set to true if any connected elements
     * should be deleted as well.
     * @since 2.5
     */
    public DeleteHighlightedUpdater(BeermatController controller,
    boolean deleteAnchoredElements) {
        super(controller);
        isDeleteAnchoredElements = deleteAnchoredElements;
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        getController().getBeermatAWTCanvas().getHighlightManager()
            .deleteSelectedElements(isDeleteAnchoredElements);
        return true;
    }

}
