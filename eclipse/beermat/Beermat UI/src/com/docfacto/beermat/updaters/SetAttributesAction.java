package com.docfacto.beermat.updaters;

import java.util.List;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;

import com.docfacto.common.NameValuePair;

/**
 * Sets given attributes on an Element or a list of elements.
 * 
 * @author dhudson - created 20 Mar 2014
 * @since 2.4
 */
public class SetAttributesAction {
    private List<NameValuePair> theAttrs;

    /**
     * Constructor.
     * 
     * @param attrs List of attributes
     * @since 2.4
     */
    public SetAttributesAction(List<NameValuePair> attrs) {
        theAttrs = attrs;
    }

    /**
     * Sets the attributes on the element.
     * 
     * @param element to set the attribute on
     * @since 2.4
     */
    public void setAttributes(Element element) {
        if (element==null) {
            return;
        }

        for (NameValuePair attr:theAttrs) {
            // Set the attribute for the element
            try {
                if ((attr.getName().equals("marker-start")||attr.getName()
                    .equals("marker-end"))&&
                    attr.getValue().isEmpty()) {
                    element.removeAttribute(attr.getName());
                }
                else {
                    element.setAttribute(attr.getName(),attr.getValue());
                }
            }
            catch (DOMException e) {
            }
        }
    }

    /**
     * Set the attributes to all of the elements.
     * 
     * @param elements list of elements to set
     * @since 2.4
     */
    public void setAttributes(List<Element> elements) {
        for (Element element:elements) {
            setAttributes(element);
        }
    }
}
