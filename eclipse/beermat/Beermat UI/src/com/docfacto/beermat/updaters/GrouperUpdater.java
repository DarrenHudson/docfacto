package com.docfacto.beermat.updaters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.batik.dom.svg.SVGOMGElement;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.GroupElementEvent;
import com.docfacto.beermat.events.UngroupElementEvent;
import com.docfacto.beermat.utils.ElementComparator;
import com.docfacto.beermat.utils.SVGUtils;

/**
 * A runnable that handles the grouping or grouping.
 * 
 * This will also schedule this class on the update manager queue
 * 
 * @author dhudson - created 17 Aug 2013
 * @since 2.4
 * @docfacto.link key="groupElementAction"
 * uri="${doc-beermat}/controlbar/c_group_tool_palette_item.dita" link-to="doc"
 */
public class GrouperUpdater extends AbstractUpdater {

    /**
     * Constructor.
     * 
     * @param controller Beermat controller
     * @since 2.4
     */
    public GrouperUpdater(BeermatController controller) {
        super(controller);
        if (controller.getBeermatAWTCanvas().getHighlightManager()
            .hasSelectedElements()) {
            enqueue();
        }
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        final List<Element> elements =
            getController().getBeermatAWTCanvas().getHighlightManager()
                .getHighlightedElements();

        final Document doc = getController().getManager().getDocument();

        if (elements.size()==1&&
            elements.get(0) instanceof SVGOMGElement) {
            // A single element is selected and its a group element,
            // so ungroup them.
            Element group = elements.get(0);
            NodeList nl = group.getChildNodes();
            List<Element> children =
                new ArrayList<Element>(nl.getLength());

            for (int i = 0;i<nl.getLength();i++) {
                if (nl.item(i).getNodeType()==Element.ELEMENT_NODE) {
                    children.add((Element)nl.item(i));
                }
            }

            for (int i = 0;i<children.size();i++) {
                doc.getDocumentElement().insertBefore(
                    children.get(i),null);
            }
            // remove group from dom.
            doc.getDocumentElement().removeChild(group);
            getController()
                .processEvent(
                    new UngroupElementEvent(this,children));
        }
        else if (elements.size()>1) {
            // Several Elements are selected, so group them.
            Element group =
                doc.createElementNS(SVGUtils.SVG_NAMESPACE,"g");
            Collections.sort(elements,new ElementComparator());

            for (Element e:elements) {
                // For each element, add it to the group
                group.appendChild(e);
            }
            // Append group to document
            doc.getDocumentElement().appendChild(group);

            // Lets fire a Grouped Event, so the Overlay can react
            // to it
            getController().processEvent(
                new GroupElementEvent(this,group));
        }

        return true;
    }
}
