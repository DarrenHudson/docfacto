package com.docfacto.beermat.updaters;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.HighlightManagerEvent;
import com.docfacto.common.NameValuePair;

/**
 * An update to set a list of attributes on an element(s).
 * 
 * Updates, savestate can be specified, but is true by default.
 * {@docfacto.note Should only be used when the user is actually changing/setting
 * attributes out of an updater, as this will cause a state to be saved, if you need to set attributes on an element in
 * another Updater run a SetAttributeAction}
 * 
 * @author dhudson - created 5 Dec 2013
 * @since 2.4
 */
public class SetAttributesUpdater extends AbstractUpdater {

    private final List<NameValuePair> theAttrs;
    private final List<Element> theElements;
    private boolean theSaveState = true;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param attrs Attributes to set
     * @param element to set them on
     * @since 2.5
     */
    public SetAttributesUpdater(BeermatController controller,List<NameValuePair> attrs,Element element) {
        super(controller);
        theAttrs = attrs;
        theElements = new ArrayList<Element>(1);
        theElements.add(element);
        enqueue();
    }
    
    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param attrs Attributes to set
     * @param element to set them on
     * @param saveState whether a save state is created
     * @since 2.5.1
     */
    public SetAttributesUpdater(BeermatController controller,List<NameValuePair> attrs,Element element, boolean saveState) {
        this(controller,attrs,element);
        theSaveState = saveState;
    }

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param attrs to set
     * @param elements to set the attributes on
     * @since 2.5
     */
    public SetAttributesUpdater(BeermatController controller,List<NameValuePair> attrs,List<Element> elements) {
        super(controller);
        theAttrs = attrs;
        theElements = elements;
        enqueue();
    }
    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param attrs to set
     * @param elements to set the attributes on
     * @param saveState whether this will create a state
     * @since 2.5.1
     */
    public SetAttributesUpdater(BeermatController controller,List<NameValuePair> attrs,List<Element> elements, boolean saveState){
        this(controller,attrs,elements);
        theSaveState = saveState;
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        if (theElements.size()==1) {
            new SetAttributesAction(theAttrs).setAttributes(theElements.get(0));
        }
        else {
            new SetAttributesAction(theAttrs).setAttributes(theElements);
        }

        // Notify the highlight manager that bounds may have changed
        getController().processEvent(new HighlightManagerEvent(this));
        return true;
    }
    
    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#saveState()
     */
    @Override
    public boolean saveState() {
        return theSaveState;
    }
}
