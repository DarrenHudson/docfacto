package com.docfacto.beermat.updaters;

import java.util.List;

import org.apache.batik.dom.svg.SVGOMGElement;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.utils.ElementAttributes;
import com.docfacto.beermat.utils.ElementUtils;
import com.docfacto.beermat.utils.SVGUtils;

/**
 * Takes the highlighted elements and converts each to a path, removing the old
 * elements from the svg.
 * 
 * @author kporter - created Sep 5, 2013
 * @since 2.4.4
 */
public class ToPathUpdater extends AbstractUpdater {

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @since 2.4
     */
    public ToPathUpdater(BeermatController controller) {
        super(controller);
        enqueue();
    }

    private void replace(Element element,Element parent) {
        Document document = getController().getManager().getDocument();
        ElementAttributes attr = ElementUtils.getElementAttributes(element);

        String path =
            SVGUtils.elementToPath(document,element,
                getController().getBeermatAWTCanvas());

        Element newElement = ElementUtils.createPath(document,null);
        ElementUtils.setElementAttributes(newElement,attr);
        newElement.setAttribute("d",path);
        parent.replaceChild(newElement,element);
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        List<Element> elements =
            getController().getBeermatAWTCanvas().getHighlightManager()
                .getHighlightedElements();
        for (Element element:elements) {
            if (element instanceof SVGOMGElement) {
                List<Element> children =
                    ElementUtils.getElementChildren(element);
                children =
                    ElementUtils
                        .filterElementType(children,SVGOMGElement.class);
                for (Element e:children) {
                    replace(e,element);
                }
            }
            else {
                replace(element,getController().getManager().getDocument()
                    .getDocumentElement());
            }
        }
        return true;
    }

}