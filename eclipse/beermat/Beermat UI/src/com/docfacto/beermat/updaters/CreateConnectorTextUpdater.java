package com.docfacto.beermat.updaters;

import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.hotspot.HotSpot;
import com.docfacto.beermat.svg.TextHandler;

/**
 * Updater to create a text path for a connector
 * 
 * @author dhudson - created 5 Dec 2013
 * @since 2.5
 */
public class CreateConnectorTextUpdater extends AbstractTextUpdater {

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param lines only the first line will be used
     * @param hotSpot for the connector
     * @param anchorElement the connector
     * @since 2.5
     */
    public CreateConnectorTextUpdater(BeermatController controller,
    String[] lines,HotSpot hotSpot,Element anchorElement) {
        super(controller,lines,hotSpot,anchorElement);
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        Element textElement =
            TextHandler.addConnectorText(
                getAnchorElement(),getLines()[0]);

        setWidgetAttributes(textElement);

        addToDocument(textElement);

        applyHotSpotAttributes(textElement);
        return true;
    }
}
