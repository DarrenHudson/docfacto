package com.docfacto.beermat.updaters;

import java.awt.geom.Point2D;
import java.util.ArrayList;

import org.apache.batik.dom.svg.SVGOMLineElement;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.hotspot.HotSpot;
import com.docfacto.beermat.hotspot.NSEWHotSpots;
import com.docfacto.beermat.listeners.LineToolInputListener;
import com.docfacto.beermat.svg.ConnectorHandler;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.NameValuePair;

/**
 * This will run on the Batik updating thread and position the line correctly.
 * 
 * @author dhudson - created 29 Oct 2013
 * @since 2.4
 */
public class LinePositionUpdater extends AbstractUpdater {

    private final HotSpot theStart;
    private final HotSpot theEnd;
    private final SVGOMLineElement theLine;
    private final NSEWHotSpots theStartNode;
    private final NSEWHotSpots theEndNode;
    private final Point2D theStartPoint;
    private final Point2D theEndPoint;
    private final LineToolInputListener theListener;
    
    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param element to change
     * @param startNode the node the line is connecting from
     * @param endNode the node the line is connecting to
     * @param start hot spot
     * @param end end hot spot
     * @param startPoint starting point (transformed)
     * @param endPoint ending point (transformed)
     * @param listener callback for the new element
     * @since 2.4
     */
    public LinePositionUpdater(BeermatController controller,
    SVGOMLineElement element,NSEWHotSpots startNode,
    NSEWHotSpots endNode,HotSpot start,HotSpot end,Point2D startPoint,
    Point2D endPoint,LineToolInputListener listener) {
        super(controller);
        theLine = element;
        theStart = start;
        theEnd = end;
        theStartNode = startNode;
        theEndNode = endNode;
        theStartPoint = startPoint;
        theEndPoint = endPoint;
        theListener = listener;
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {

        Point2D startPoint;
        if (theStart!=null) {
            startPoint =
                SVGUtils.getTransformedPoint(theStart.getBounds().getCenterX(),
                    theStart.getBounds().getCenterY(),
                    getController().getViewBoxTransform());
        }
        else {
            startPoint = theStartPoint;
        }

        Point2D endPoint;
        if (theEnd!=null) {
            endPoint =
                SVGUtils
                    .getTransformedPoint(theEnd.getBounds().getCenterX(),theEnd
                        .getBounds().getCenterY(),
                        getController().getBeermatAWTCanvas()
                            .getViewBoxTransform());
        }
        else {
            endPoint = theEndPoint;
        }

        Document doc = theLine.getOwnerDocument();

        Element path = doc.createElementNS(SVGUtils.SVG_NAMESPACE,"path");

        ConnectorHandler handler = new ConnectorHandler();

        handler.setStart(startPoint.getX(),startPoint.getY());
        handler.setEnd(endPoint.getX(),endPoint.getY());

        handler.updatePath(path);

        if (theStart!=null) {
            handler.setStartConnectorAttributes(path,theStartNode.getElement(),
                theStart.getLocation());
        }

        if (theEnd!=null) {
            handler.setEndConnectorAttributes(path,theEndNode.getElement(),
                theEnd.getLocation());
        }

        doc.getDocumentElement().replaceChild(path,theLine);

        theListener.setElement(path);
        
        ArrayList<NameValuePair> attrs =
            getController().getView().getControlToolBar()
                .getElementAttributes(path);
        new SetAttributesAction(attrs).setAttributes(path);

        if(theEnd != null && theStart != null) {
            // Double ended connector
            path.setAttribute("class","connector");
        }
        return true;
    }

}
