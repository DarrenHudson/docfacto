package com.docfacto.beermat.updaters;

import java.awt.geom.Point2D;

import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGRect;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.hotspot.HotSpot;
import com.docfacto.beermat.utils.ElementSizer;
import com.docfacto.beermat.utils.ElementUtils;
import com.docfacto.beermat.utils.SVGUtils;

/**
 * Update to create a text node and deal with anchoring if required
 * 
 * @author dhudson - created 5 Dec 2013
 * @since 2.5
 */
public class CreateTextUpdater extends AbstractTextUpdater {

    private final Point2D theLocation;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param lines only the first line will be used
     * @param hotSpot for the connector
     * @param anchorElement the connector
     * @param location location of the mouse click
     * @since 2.5
     */
    public CreateTextUpdater(BeermatController controller,String[] lines,
    HotSpot hotSpot,Element anchorElement,Point2D location) {
        super(controller,lines,hotSpot,anchorElement);
        theLocation = location;
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        Element textElement =
            ElementUtils.createText(getController().getManager().getDocument(),
                getLines()[0]);

        setWidgetAttributes(textElement);

        Point2D location = null;

        if (getHotSpot()!=null) {
            // Use the sizer to work out the size when it is
            // added to the doc
            SVGRect bbox =
                ElementSizer.INSTANCE
                    .getElementBounds(textElement);

            location =
                SVGUtils.getTextLocationWithHotSpot(
                    textElement,getHotSpot(),
                    (int)bbox.getHeight(),
                    getController().getViewBoxTransform());
        }
        else {
            location =
                SVGUtils.getTransformedPoint(
                    theLocation.getX(),theLocation.getY(),
                    getController()
                        .getViewBoxTransform());
        }

        SVGUtils.setElementLocation(textElement,
            location);

        // Finally add the text element
        addToDocument(textElement);

        applyHotSpotAttributes(textElement);

        return true;
    }

}
