package com.docfacto.beermat.updaters;

import java.util.List;

import org.apache.batik.dom.svg.SVGOMGElement;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.utils.ElementUtils;
import com.docfacto.beermat.utils.SVGUtils;

/**
 * Update multiple elements to paths
 * 
 * @author kporter - created Sep 9, 2013
 * @since 2.4
 */
public class MultiElementsToPathUpdater extends AbstractUpdater {

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @since 2.5
     */
    public MultiElementsToPathUpdater(BeermatController controller) {
        super(controller);
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        Document document = getController().getManager().getDocument();
        List<Element> elements =
            getController().getBeermatAWTCanvas().getHighlightManager()
                .getHighlightedElements();

        String finalPath = "";
        for (Element element:elements) {
            if (element instanceof SVGOMGElement) {
                List<Element> children =
                    ElementUtils.getElementChildren(element);
                children =
                    ElementUtils
                        .filterElementType(children,SVGOMGElement.class);
                for (Element child:children) {
                    String path =
                        SVGUtils.elementToPath(document,child,
                            getController().getBeermatAWTCanvas());
                    finalPath += path;
                }
            }
            else {
                String path = SVGUtils.elementToPath(document,element,
                    getController().getBeermatAWTCanvas());
                finalPath += path;
            }
            document.getDocumentElement().removeChild(element);
        }

        Element newElement = ElementUtils.createPath(document,null);
        newElement.setAttribute("d",finalPath);
        newElement.setAttribute(SVGUtils.FILL,"none");
        newElement.setAttribute(SVGUtils.STROKE_COLOUR,"black");

        return true;
    }
}
