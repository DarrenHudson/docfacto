package com.docfacto.beermat.updaters;

import org.eclipse.swt.graphics.Point;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.utils.SVGUtils;

/**
 * Used to update the size of the SVG Canvas
 * 
 * @author dhudson - created 5 Dec 2013
 * @since 2.5
 */
public class SVGSizeUpdater extends AbstractUpdater {

    private final Point theSize;

    /**
     * Constructor.
     * 
     * @param controller Beermat controller
     * @param size the size of the document
     * @since 2.5
     */
    public SVGSizeUpdater(BeermatController controller,Point size) {
        super(controller);
        theSize = size;
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {
        SVGUtils.setSVGSize(getController()
            .getManager().getDocument(),theSize);
        return true;
    }

}
