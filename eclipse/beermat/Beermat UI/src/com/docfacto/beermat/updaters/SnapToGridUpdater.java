package com.docfacto.beermat.updaters;

import java.awt.Rectangle;
import java.util.List;

import org.apache.batik.gvt.GraphicsNode;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.svg.DragData;
import com.docfacto.beermat.svg.ShapeShifter;
import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.beermat.utils.SVGUtils;

/**
 * Snap to grid updater will move SVG elements to the nearest grid position.
 * 
 * @author dhudson - created 20 Mar 2014
 * @since 2.5
 */
public class SnapToGridUpdater extends AbstractUpdater {

    private final int theGridSize;

    /**
     * Constructor.
     * 
     * @param controller Beermat controller
     */
    public SnapToGridUpdater(BeermatController controller) {
        super(controller);
        theGridSize = (int)controller.getBeermatAWTCanvas().getGridSize();
        enqueue();
    }

    /**
     * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
     */
    @Override
    public boolean applyUpdates() {

        List<Element> elements =
            SVGUtils.getSVGElements(getController().getManager().getDocument());

        BeermatAWTCanvas beermatCanvas = getController().getBeermatAWTCanvas();

        for (Element element:elements) {
            GraphicsNode graphicsNode =
                beermatCanvas.getGraphicsNodeFor(element);
            if (graphicsNode!=null&&graphicsNode.getBounds()!=null) {

                if (!SVGUtils.isAnchorableElement(element)) {
                    Rectangle bounds = graphicsNode.getBounds().getBounds();

                    DragData d = new DragData();
                    d.setXDiff(getOffset(bounds.x));
                    d.setYDiff(getOffset(bounds.y));

                    ShapeShifter.updateElement(element,d);
                }
            }
        }

        // Move all of the anchorable elements
        ShapeShifter.updateAnchoredElements(getController()
            .getBeermatAWTCanvas(),elements,getController()
            .getViewBoxTransform());

        return true;
    }

    /**
     * Calculate the offset.
     * 
     * @param position point
     * @return the offset from the grid
     * @since 2.5
     */
    private int getOffset(int position) {
        int offset = position%theGridSize;
        if (offset>=(theGridSize/2)) {
            offset = theGridSize-offset;
        }
        else {
            offset = -offset;
        }

        return offset;
    }
}
