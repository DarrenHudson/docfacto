package com.docfacto.beermat.svg;

import java.awt.Component;

import org.apache.batik.swing.svg.SVGUserAgentGUIAdapter;

import com.docfacto.beermat.plugin.BeermatUIPlugin;

/**
 * A User Agent handles errors from the JSVGCanvas, as we don't want AWT
 * Dialogs, this class will log the exceptions instead.
 * 
 * @author dhudson - created 22 Jun 2013
 * @since 2.4
 */
public class BeermatUserAgent extends SVGUserAgentGUIAdapter {

    /**
     * Constructor.
     * 
     * @param parent component
     * @since 2.4
     */
    public BeermatUserAgent(Component parent) {
        super(parent);
    }

    /**
     * @see org.apache.batik.swing.svg.SVGUserAgentGUIAdapter#displayError(java.lang.Exception)
     */
    @Override
    public void displayError(Exception ex) {
        // Lets not display it but log it instead
        BeermatUIPlugin.logException(ex.getMessage(),ex);
    }

    /**
     * @see org.apache.batik.swing.svg.SVGUserAgentGUIAdapter#displayError(java.lang.String)
     */
    @Override
    public void displayError(String error) {
        BeermatUIPlugin.logException(error,null);
    }

    /**
     * @see org.apache.batik.swing.svg.SVGUserAgentGUIAdapter#displayMessage(java.lang.String)
     */
    @Override
    public void displayMessage(String message) {
        BeermatUIPlugin.logMessage(message);
    }

    /**
     * @see org.apache.batik.swing.svg.SVGUserAgentGUIAdapter#showAlert(java.lang.String)
     */
    @Override
    public void showAlert(String message) {
        super.showAlert(message);
    }

    /**
     * @see org.apache.batik.swing.svg.SVGUserAgentGUIAdapter#showConfirm(java.lang.String)
     */
    @Override
    public boolean showConfirm(String message) {
        return super.showConfirm(message);
    }

    /**
     * @see org.apache.batik.swing.svg.SVGUserAgentGUIAdapter#showPrompt(java.lang.String,
     * java.lang.String)
     */
    @Override
    public String showPrompt(String message,String defaultValue) {
        return super.showPrompt(message,defaultValue);
    }

    /**
     * @see org.apache.batik.swing.svg.SVGUserAgentGUIAdapter#showPrompt(java.lang.String)
     */
    @Override
    public String showPrompt(String message) {
        return super.showPrompt(message);
    }

    @Override
    public void openLink(String link,boolean arg1) {
        //REMOVE:
        System.out.println("BeermatUserAgent: openLink : "+link);
    }

}
