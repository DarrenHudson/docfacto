package com.docfacto.beermat.svg;

import java.text.NumberFormat;

import org.apache.batik.dom.svg.SVGOMLineElement;
import org.apache.batik.parser.DefaultPathHandler;
import org.apache.batik.parser.ParseException;
import org.apache.batik.parser.PathParser;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;

import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.XMLUtils;

/**
 * A connector is a path which draws a line. So Mx1 y1 Lx2, y3
 * 
 * @author dhudson - created 28 Nov 2013
 * @since 2.5
 */
public class ConnectorHandler extends DefaultPathHandler {

    /**
     * {@value}
     */
    public static final String CONNECTOR_START_ATTRIBUTE = "connector-start";
    /**
     * {@value}
     */
    public static final String CONNECTOR_START_HOTSPOT_ATTRIBUTE =
        "connector-start-hotspot";

    /**
     * {@value}
     */
    public static final String CONNECTOR_END_ATTRIBUTE = "connector-end";
    /**
     * {@value}
     */
    public static final String CONNECTOR_END_HOTSPOT_ATTRIBUTE =
        "connector-end-hotspot";

    private double x1;
    private double x2;
    private double y1;
    private double y2;

    private PathParser thePathParser;

    /**
     * Constructor.
     * 
     * @since 2.5
     */
    public ConnectorHandler() {
        thePathParser = new PathParser();
        thePathParser.setPathHandler(this);
    }

    /**
     * parse the path data from the element
     * 
     * {@docfacto.note This will upgrade the old line connectors to path
     * connectors}
     * 
     * @param pathElement to parse
     * @since 2.5
     */
    public void parser(Element pathElement) {
        if (pathElement instanceof SVGOMLineElement) {
            // Upgrade
            SVGOMLineElement line = (SVGOMLineElement)pathElement;
            x1 = line.getX1().getBaseVal().getValue();
            x2 = line.getX2().getBaseVal().getValue();
            y1 = line.getY1().getBaseVal().getValue();
            y2 = line.getY2().getBaseVal().getValue();

            Document doc = pathElement.getOwnerDocument();
            Element path = doc.createElementNS(SVGUtils.SVG_NAMESPACE,"path");

            // Lets copy the attributes
            NamedNodeMap attrs = line.getAttributes();

            for (int i = 0;i<attrs.getLength();i++) {
                Attr node = (Attr)attrs.item(i);
                String attrName = node.getNodeName();
                if (attrName.equals("x1")||attrName.equals("x2")||
                    attrName.equals("y1")||attrName.equals("y2")) {
                    continue;
                }

                path.setAttributeNS(node.getNamespaceURI(),node.getName(),
                    node.getValue());
            }

            // Swap them out
            doc.getDocumentElement().replaceChild(path,pathElement);

            updatePath(path);
        }
        else {
            try {
                thePathParser.parse(pathElement.getAttribute("d"));
            }
            catch (ParseException ex) {
                // REMOVE:
                System.out.println("pathElement "+
                    pathElement.getAttribute("id"));
            }
        }

    }

    /**
     * Check to see if the line direction is from left to right
     * 
     * @return true if the connector is from left to right
     * @since 2.5
     */
    public boolean isLeftToRight() {
        return x1<x2;
    }

    /**
     * {@docfacto.note Start x and y }
     * 
     * @see org.apache.batik.parser.DefaultPathHandler#movetoAbs(float, float)
     */
    @Override
    public void movetoAbs(float paramFloat1,float paramFloat2)
    throws ParseException {
        x1 = paramFloat1;
        y1 = paramFloat2;
    }

    /**
     * {@docfacto.note End x and y }
     * 
     * @see org.apache.batik.parser.DefaultPathHandler#linetoAbs(float, float)
     */
    @Override
    public void linetoAbs(float paramFloat1,float paramFloat2)
    throws ParseException {
        x2 = paramFloat1;
        y2 = paramFloat2;
    }

    /**
     * Set the start point co-ords
     * 
     * @param x
     * @param y
     * @since 2.5
     */
    public void setStart(double x,double y) {
        x1 = x;
        y1 = y;
    }

    /**
     * Set the end point co-ords
     * 
     * @param x
     * @param y
     * @since 2.5
     */
    public void setEnd(double x,double y) {
        x2 = x;
        y2 = y;
    }

    /**
     * Update the path data on the given element to reflect the start and end
     * values of the line
     * 
     * @param connectorElement to update
     * @since 2.5
     */
    public void updatePath(Element connectorElement) {
        StringBuilder path = new StringBuilder(50);
        path.append("M");
        NumberFormat decimalFormat = SVGUtils.NUMBER_FORMAT.get();

        path.append(decimalFormat.format(x1));
        path.append(" ");
        path.append(decimalFormat.format(y1));
        path.append(" L");
        path.append(decimalFormat.format(x2));
        path.append(" ");
        path.append(decimalFormat.format(y2));

        connectorElement.setAttribute("d",path.toString());
    }

    /**
     * Check to see if the element is a connector.
     * 
     * @param element to check
     * @return true if the element is a connector
     * @since 2.4
     */
    public static boolean isConnector(Element element) {
        if (element==null) {
            return false;
        }

        return (SVGUtils
            .hasDocfactoAttribute(element,CONNECTOR_START_ATTRIBUTE)||SVGUtils
            .hasDocfactoAttribute(element,CONNECTOR_END_ATTRIBUTE));
    }

    /**
     * Check to see if the connector is for the node
     * 
     * @param node connected node
     * @param connector node connector
     * @return true if the connector is for the node
     * @since 2.4
     */
    public static boolean isConnectorFor(Element node,Element connector) {
        if (isConnector(connector)) {
            String nodeId = node.getAttribute("id");
            if (nodeId==null||nodeId.isEmpty()) {
                return false;
            }
            String startNode = SVGUtils.getDocfactoAttribute(connector,
                CONNECTOR_START_ATTRIBUTE);
            if (nodeId.equals(startNode)) {
                return true;
            }
            String endNode = SVGUtils.getDocfactoAttribute(connector,
                CONNECTOR_END_ATTRIBUTE);
            if (nodeId.equals(endNode)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Set the start attributes for the connector
     * 
     * @param path connector
     * @param startNode connecting from
     * @param hotspot location
     * @since 2.5
     */
    public void setStartConnectorAttributes(Element path,
    Element startNode,int hotspot) {
        SVGUtils.setDocfactoAttribute(path,
            CONNECTOR_START_ATTRIBUTE,
            SVGUtils.getOrCreateId(startNode));
        SVGUtils.setDocfactoAttribute(path,
            CONNECTOR_START_HOTSPOT_ATTRIBUTE,
            Integer.toString(hotspot));
    }

    /**
     * Set the end attributes for the connector
     * 
     * @param path connector
     * @param element connecting from
     * @param location of hot spot
     * @since 2.5
     */
    public void setEndConnectorAttributes(Element path,Element element,
    int location) {
        SVGUtils.setDocfactoAttribute(path,
            CONNECTOR_END_ATTRIBUTE,
            SVGUtils.getOrCreateId(element));
        SVGUtils.setDocfactoAttribute(path,
            CONNECTOR_END_HOTSPOT_ATTRIBUTE,
            Integer.toString(location));
    }

    /**
     * Return the start node for the connector
     * 
     * @param connector to check
     * @return null or the start node for the connector
     * @since 2.5
     */
    public static Element getStartNode(Element connector) {
        return XMLUtils.getElementByID(connector.getOwnerDocument(),
            SVGUtils.getDocfactoAttribute(connector,CONNECTOR_START_ATTRIBUTE));
    }

    /**
     * Return the end node for the connector
     * 
     * @param connector to check
     * @return null or the end node
     * @since 2.5
     */
    public static Element getEndNode(Element connector) {
        return XMLUtils.getElementByID(connector.getOwnerDocument(),
            SVGUtils.getDocfactoAttribute(connector,CONNECTOR_END_ATTRIBUTE));
    }

    public double getX1() {
        return x1;
    }

    public double getX2() {
        return x2;
    }

    public double getY1() {
        return y1;
    }

    public double getY2() {
        return y2;
    }
}
