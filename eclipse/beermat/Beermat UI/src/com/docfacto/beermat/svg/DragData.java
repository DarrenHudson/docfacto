package com.docfacto.beermat.svg;

import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import org.apache.batik.swing.JSVGCanvas;
import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Document;

import com.docfacto.beermat.hotspot.HotSpot;

/**
 * A collection of data used for dragging.
 * 
 * @author kporter - created Aug 5, 2013
 * @since 2.4.3
 */
public class DragData {
    
    /**
     * A position thats used for line dragging.
     * @author kporter - created 18 Dec 2013
     * @since 2.5
     */
    enum LinePosition{
        NONE,
        X1Y1,
        X2Y2
    }
    
    private AffineTransform theTransform;
    private HotSpot theHotspot;
    private float theXDiff;
    private float theYDiff;
    private Point theMouseLocation;
    /**
     * @deprecated Use LinePosition for line dragging.
     */
    private ArrayList<String> theDirectionVariables;
    private boolean theRetainAspectRatio;

    private JSVGCanvas theCanvas;
    private Document theDocument;
    
    private LinePosition linePosition = LinePosition.NONE;
    
    /**
     * Constructor.
     * @since I don't know as Kelvin can't be arsed to fill in something this simple
     */
    public DragData() {
        theDirectionVariables = new ArrayList<String>(0);
        theRetainAspectRatio = false;
    }

    /**
     * Constructor.
     * @param xdiff
     * @param ydiff
     * @param hotspot
     * @param transform
     * @param canvas
     * @param document
     * @since I don't know as Kelvin can't be arsed to fill in something this simple
     */
    public DragData(float xdiff,float ydiff,HotSpot hotspot,
    AffineTransform transform,JSVGCanvas canvas,Document document) {
        this();
        theXDiff = xdiff;
        theYDiff = ydiff;
        theHotspot = hotspot;
        theTransform = transform;
        theDocument = document;
        theCanvas = canvas;
    }
    
    /**
     * Get which position is being dragged
     * @return the LinePosition that is being dragged
     * @since 2.5
     */
    public LinePosition getLinePosition() {
        return linePosition;
    }

    /**
     * Set the line position being dragged
     * @param linePosition The position being dragged
     * @since 2.5
     */
    public void setLinePosition(LinePosition linePosition) {
        this.linePosition = linePosition;
    }
    

    /**
     * Returns Mouse Location.
     * 
     * @return the Mouse Location
     */
    public Point getMouseLocation() {
        return theMouseLocation;
    }

    /**
     * Sets the Mouse Location.
     * 
     * @param mouseLocation the Mouse Location value
     */
    public void setMouseLocation(Point mouseLocation) {
        theMouseLocation = mouseLocation;
    }

    /**
     * Returns transform.
     * 
     * @return the transform
     */
    public AffineTransform getTransform() {
        return theTransform;
    }

    /**
     * Sets transform.
     * 
     * @param transform the transform value
     */
    public void setTransform(AffineTransform transform) {
        theTransform = transform;
    }

    /**
     * Returns hotspot.
     * 
     * @return the hotspot
     */
    public HotSpot getHotspot() {
        return theHotspot;
    }

    /**
     * Sets hotspot.
     * 
     * @param hotspot the hotspot value
     */
    public void setHotspot(HotSpot hotspot) {
        theHotspot = hotspot;
    }

    /**
     * Returns xDiff.
     * 
     * @return the xDiff
     */
    public float getXDiff() {
        return theXDiff;
    }

    /**
     * Sets xDiff.
     * 
     * @param xDiff the xDiff value
     */
    public void setXDiff(float xDiff) {
        theXDiff = xDiff;
    }

    /**
     * Returns yDiff.
     * 
     * @return the yDiff
     */
    public float getYDiff() {
        return theYDiff;
    }

    /**
     * Sets yDiff.
     * 
     * @param yDiff the yDiff value
     */
    public void setYDiff(float yDiff) {
        theYDiff = yDiff;
    }

    /**
     * Gets the direction variables
     * 
     * @return the ArrayList of direction variables.
     */
    public ArrayList<String> getDirectionVariables() {
        return theDirectionVariables;
    }

    /**
     * Check to see if there are direction vars
     * 
     * @return true if there are direction vars
     * @since 2.5
     */
    public boolean hasDirectionVariables() {
        return theDirectionVariables.size() > 0;
    }
    
    /**
     * Add a direction var to the list.
     * 
     * @param var to add
     * @since 2.5
     */
    public void addDirectionVariable(String var) {
        theDirectionVariables.add(var);
    }
    
    /**
     * Sets whether to retain the aspect ratio
     * 
     * @param retain Set to true if element dragging should retain aspect ratio.
     * @since 2.4.4
     */
    public void setRetainAspect(boolean retain) {
        theRetainAspectRatio = retain;
    }

    /**
     * Gets the boolean for retaining aspect ratio
     * 
     * @return a boolean value whether to retain the aspect ratio
     * @since 2.4.4
     */
    public boolean getRetainAspect() {
        return theRetainAspectRatio;
    }

    /**
     * Returns theDocument.
     * 
     * @return the theDocument
     */
    public Document getDocument() {
        return theDocument;
    }

    /**
     * Sets theDocument.
     * 
     * @param document to set
     */
    public void setDocument(Document document) {
        theDocument = document;
    }

    /**
     * Returns the beermat canvas
     * 
     * @return the canvas
     * @since 2.4
     */
    public JSVGCanvas getCanvas() {
        return theCanvas;
    }

}
