package com.docfacto.beermat.svg;

import java.util.Collections;
import java.util.List;

import org.apache.batik.dom.svg.SVGOMTextPathElement;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.StringUtils;
import com.docfacto.common.XMLUtils;

/*  <text>
 <textPath baseline-shift="-100%" direction="rtl" glyph-orientation-horizontal="180" startOffset="50%" unicode-bidi="bidi-override" xlink:href="#connector">
 Hello
 </textPath>
 </text>
 */

/**
 * Class to handle anchored text.
 * 
 * @author dhudson - created 2 Dec 2013
 * @since 2.5
 */
public class TextHandler {

    public static final String HOTSPOT_ATTRIBUTE = "hotspot";
    public static final String ANCHOR_ATTRIBUTE = "anchor";

    public static final String BASELINE_SHIFT = "baseline-shift";
    public static final String BASELINE_SHIFT_VALUE = "-110%";
    public static final String BASELINE_SHIFT_INITIAL_VALUE = "10%";

    public static final String DIRECTION = "direction";
    public static final String DIRECTION_VALUE = "rtl";

    public static final String GLYPH_ORIENTATION_HORIZONTAL =
        "glyph-orientation-horizontal";
    public static final String GLYPH_ORIENTATION_HORIZONTAL_VALUE = "180";

    public static final String START_OFFSET = "startOffset";
    public static final String START_OFFSET_VALUE = "50%";

    public static final String UNICODE_BIDI = "unicode-bidi";
    public static final String UNICODE_BIDI_VALUE = "bidi-override";

    /**
     * Return a list of text elements which are anchored to the provided node.
     * 
     * @param element to check
     * @return a list, which may be empty of text elements anchored to the
     * element provided
     * @since 2.5
     */
    public static List<Element> getAnchoredElements(Element element) {
        String id = element.getAttribute("id");
        if (StringUtils.nullOrEmpty(id)) {
            return Collections.emptyList();
        }

        return XMLUtils.getElementsWithAttribute(element.getOwnerDocument(),
            SVGUtils.DOCFACTO_NAME_SPACE,
            ANCHOR_ATTRIBUTE,id);
    }

    /**
     * Set the attributes required for the text element.
     * 
     * @param text to set the attributes
     * @param node of which the text is anchored
     * @param hotSpotLocation location of the hot spot
     * @since 2.5
     */
    public static void setAnchorAttributes(Element text,Element node,
    int hotSpotLocation) {
        SVGUtils.setDocfactoAttribute(text,HOTSPOT_ATTRIBUTE,
            Integer.toString(hotSpotLocation));
        SVGUtils.setDocfactoAttribute(text,ANCHOR_ATTRIBUTE,
            SVGUtils.getOrCreateId(node));
    }

    /**
     * Generate a Text / Text Path elements for connector text
     * 
     * @param anchorElement connector
     * @param theLines array of text
     * @return the newly created Text element
     * @since 2.5
     */
    public static Element
    addConnectorText(Element anchorElement,String text) {

        String id = SVGUtils.getOrCreateId(anchorElement);

        Document doc = anchorElement.getOwnerDocument();
        Element textElement =
            doc.createElementNS(SVGUtils.SVG_NAMESPACE,"text");

        Element textPath =
            doc.createElementNS(SVGUtils.SVG_NAMESPACE,"textPath");

        Node content = doc.createTextNode(text);
        textPath.appendChild(content);
        setTextPathRef(textPath,id);

        textPath.setAttribute(START_OFFSET,START_OFFSET_VALUE);
        textPath.setAttribute(BASELINE_SHIFT,BASELINE_SHIFT_INITIAL_VALUE);

        rotateTextIfRequired(anchorElement,textPath);

        textElement.appendChild(textPath);
        return textElement;
    }

    /**
     * Set the path reference for the text path.
     * 
     * @param textPath
     * @param id of the path (connector)
     * @since 2.5
     */
    public static void setTextPathRef(Element textPath,String id) {
        textPath.setAttributeNS(SVGUtils.XLINK_NAME_SPACE,"xlink:href","#"+id);
    }

    /**
     * Depending on the connector direction, flip the text path.
     * 
     * @param connector to process
     * @param textPath to flip
     * @since 2.5
     */
    public static void rotateTextIfRequired(Element connector,Element textPath) {
        ConnectorHandler handler = new ConnectorHandler();
        handler.parser(connector);
        if (!handler.isLeftToRight()) {
            // Lets add all of the flip attributes
            textPath.setAttribute(BASELINE_SHIFT,BASELINE_SHIFT_VALUE);
            textPath.setAttribute(DIRECTION,DIRECTION_VALUE);
            textPath.setAttribute(GLYPH_ORIENTATION_HORIZONTAL,
                GLYPH_ORIENTATION_HORIZONTAL_VALUE);
            textPath.setAttribute(UNICODE_BIDI,UNICODE_BIDI_VALUE);
        }
        else {
            textPath.removeAttribute(DIRECTION);
            textPath.removeAttribute(GLYPH_ORIENTATION_HORIZONTAL);
            textPath.removeAttribute(UNICODE_BIDI);
            textPath.setAttribute(BASELINE_SHIFT,BASELINE_SHIFT_INITIAL_VALUE);
        }
    }

    /**
     * Check to see if the given element contains a SVGOMTextPathElement in its
     * children.
     * 
     * @param element to check
     * @return true if the element contains a text path element
     * @since 2.5
     */
    public static boolean hasTextPath(Element element) {

        NodeList childNodes = element.getChildNodes();
        for (int i = 0;i<childNodes.getLength();i++) {
            Node node = childNodes.item(i);
            if (node instanceof SVGOMTextPathElement) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return the text path element from a text element.
     * 
     * @param element to check, should be a SVGOMTextElement
     * @return a text path element or null
     * @since 2.5
     */
    public static SVGOMTextPathElement getTextPath(Element element) {
        NodeList childNodes = element.getChildNodes();
        for (int i = 0;i<childNodes.getLength();i++) {
            Node node = childNodes.item(i);
            if (node instanceof SVGOMTextPathElement) {
                return (SVGOMTextPathElement)node;
            }
        }
        return null;
    }

    /**
     * Make the text path element dirty so that it gets redrawn.
     * 
     * This invalidates the graphic node cache, so its gets redrawn. There must
     * be a better way of doing this.
     * 
     * @param element to update
     * @since 2.5
     */
    public static void updateTextPathNode(SVGOMTextPathElement element) {
        element.setTextContent(element.getTextContent());
    }
}
