package com.docfacto.beermat.svg;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.batik.dom.svg.SVGDOMImplementation;
import org.apache.batik.dom.util.DocumentFactory;
import org.apache.batik.transcoder.ErrorHandler;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.TranscodingHints;
import org.apache.batik.transcoder.image.ImageTranscoder;
import org.apache.batik.util.SVGConstants;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.w3c.dom.DOMImplementation;

import com.docfacto.beermat.utils.SVGUtils;

/**
 * A Transcoder to create a icon for a custom image
 * 
 * @author dhudson - created 23 Jul 2013
 * @since 2.4
 */
public class IconTranscoder extends ImageTranscoder implements ErrorHandler {

    private BufferedImage theImage = null;
    private Image theSWTImage;

    /**
     * Constructor.
     * 
     * @param width
     * @param height
     */
    public IconTranscoder(int width, int height) {
        // Setup the transcoding hints
        TranscodingHints hints = new TranscodingHints();
        hints
            .put(ImageTranscoder.KEY_WIDTH,new Float(width));
        hints.put(ImageTranscoder.KEY_HEIGHT,
            new Float(height));
        
        hints.put(ImageTranscoder.KEY_DOM_IMPLEMENTATION,
            SVGDOMImplementation.getDOMImplementation());
        hints.put(ImageTranscoder.KEY_DOCUMENT_ELEMENT_NAMESPACE_URI,
            SVGConstants.SVG_NAMESPACE_URI);
        hints.put(ImageTranscoder.KEY_DOCUMENT_ELEMENT_NAMESPACE_URI,
            SVGConstants.SVG_NAMESPACE_URI);
        hints
            .put(ImageTranscoder.KEY_DOCUMENT_ELEMENT,SVGConstants.SVG_SVG_TAG);
        hints.put(ImageTranscoder.KEY_XML_PARSER_VALIDATING,false);

        // hints.put(ImageTranscoder.KEY_BACKGROUND_COLOR, Color.white);

        setTranscodingHints(hints);
    }

    /**
     * @see org.apache.batik.transcoder.SVGAbstractTranscoder#createDocumentFactory(org.w3c.dom.DOMImplementation,
     * java.lang.String)
     */
    @Override
    protected DocumentFactory createDocumentFactory(DOMImplementation domImpl,
    String parserClassname) {
        return SVGUtils.theSVGDocumentFactory;
    }

    /**
     * @see org.apache.batik.transcoder.image.ImageTranscoder#createImage(int,
     * int)
     */
    public BufferedImage createImage(int w,int h) {
        return new BufferedImage(w,h,BufferedImage.TYPE_INT_ARGB);
    }

    /**
     * @see org.apache.batik.transcoder.image.ImageTranscoder#writeImage(java.awt.image.BufferedImage,
     * org.apache.batik.transcoder.TranscoderOutput)
     */
    public void writeImage(BufferedImage img,TranscoderOutput out) {
        theImage = img;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ImageIO.write(theImage,"png",bos);
            bos.flush();
            bos.close();

            theSWTImage =
                new Image(Display.getDefault(),new ByteArrayInputStream(
                    bos.toByteArray()));
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Return an AWT image
     * 
     * @return
     * @since 2.4
     */
    public BufferedImage getImage() {
        return theImage;
    }

    /**
     * TODO - Method Title
     * <p>
     * TODO - Method Description
     * </p>
     * 
     * @return
     * @since n.n
     */
    public Image getSWTImage() {
        return theSWTImage;
    }

    @Override
    public void error(TranscoderException ex) throws TranscoderException {
        ex.printStackTrace();
    }

    @Override
    public void fatalError(TranscoderException ex) throws TranscoderException {
        ex.printStackTrace();
    }

    @Override
    public void warning(TranscoderException ex) throws TranscoderException {
        ex.printStackTrace();

    }
}
