package com.docfacto.beermat.svg;

import org.w3c.dom.DOMException;
import org.w3c.dom.svg.SVGRect;

/**
 * Create a SVGRect with a feathered bounds
 * 
 * @author dhudson - created 29 Oct 2013
 * @since 2.4
 */
public class FeatheredSVGBounds implements SVGRect {

    private final SVGRect theRect;
    private final int theFeather;

    /**
     * Constructor.
     * 
     * @param rect to increase
     * @param feather the amount
     * @since 2.4
     */
    public FeatheredSVGBounds(SVGRect rect,int feather) {
        theRect = rect;
        theFeather = feather;
    }

    /**
     * @see org.w3c.dom.svg.SVGRect#getHeight()
     */
    @Override
    public float getHeight() {
        return theRect.getHeight()+(theFeather*2);
    }

    /**
     * @see org.w3c.dom.svg.SVGRect#getWidth()
     */
    @Override
    public float getWidth() {
        return theRect.getWidth()+(theFeather*2);
    }

    /**
     * @see org.w3c.dom.svg.SVGRect#getX()
     */
    @Override
    public float getX() {
        return theRect.getX()-theFeather;
    }

    /**
     * @see org.w3c.dom.svg.SVGRect#getY()
     */
    @Override
    public float getY() {
        return theRect.getY()-theFeather;
    }

    /**
     * @see org.w3c.dom.svg.SVGRect#setHeight(float)
     */
    @Override
    public void setHeight(float height) throws DOMException {
        // Read only
    }

    /**
     * @see org.w3c.dom.svg.SVGRect#setWidth(float)
     */
    @Override
    public void setWidth(float width) throws DOMException {
        // Read only
    }

    /**
     * @see org.w3c.dom.svg.SVGRect#setX(float)
     */
    @Override
    public void setX(float x) throws DOMException {
        // Read only
    }

    /**
     * @see org.w3c.dom.svg.SVGRect#setY(float)
     */
    @Override
    public void setY(float y) throws DOMException {
        // Read only
    }

}
