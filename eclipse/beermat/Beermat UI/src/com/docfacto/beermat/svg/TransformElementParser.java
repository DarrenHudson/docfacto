package com.docfacto.beermat.svg;

import java.awt.geom.Point2D;

import org.apache.batik.dom.svg.SVGGraphicsElement;
import org.apache.batik.dom.svg.SVGOMTransform;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGAnimatedTransformList;
import org.w3c.dom.svg.SVGMatrix;
import org.w3c.dom.svg.SVGTransform;
import org.w3c.dom.svg.SVGTransformList;

/**
 * A parser of the transform attribute.
 * 
 * This can have several values like scale(2) translate(x,y)
 * 
 * @author dhudson - created 3 Jul 2013
 * @since 2.4
 */
public class TransformElementParser {

    @SuppressWarnings("unused")
    private final Element theElement;
    private SVGTransformList theTransList;

    /**
     * Constructor.
     * 
     * @param element to parse
     * @since 2.4
     */
    public TransformElementParser(Element element) {
        theElement = element;
        SVGGraphicsElement gEl = (SVGGraphicsElement)element;
        SVGAnimatedTransformList list = gEl.getTransform();
        theTransList = list.getBaseVal();
    }

    /**
     * Return the list of transforms in order.
     * 
     * @return the transform list
     * @since 2.4
     */
    public SVGTransformList getTransformList() {
        return theTransList;
    }
    
    /**
     * Modifies the translate or creates a new one
     * 
     * @param x
     * @param y
     * @since 2.4
     */
    public void translate(float x,float y) {
        translate(x,y,false);
    }

    /**
     * Translates an element.
     * 
     * @param x the x translation change
     * @param y The y translation change
     * @param insertAtStart Whether to insert this translate as the first transformation on the list
     * @since 2.4.4
     */
    public void translate(float x,float y,boolean insertAtStart) {
        SVGTransform transform = locate(SVGTransform.SVG_TRANSFORM_TRANSLATE);
        if (transform!=null) {
            SVGMatrix matrix = transform.getMatrix();
            transform.setTranslate(matrix.getE()+x,matrix.getF()+y);
            if (insertAtStart) {
                theTransList.insertItemBefore(transform,0);
            }
        }
        else {
            transform = new SVGOMTransform();
            transform.setTranslate(x,y);
            if (insertAtStart) {
                theTransList.insertItemBefore(transform,0);
            }
            else {
                theTransList.appendItem(transform);
            }
        }

    }

    public void setTranslate(float x,float y,boolean insertAtStart) {
        SVGTransform transform = locate(SVGTransform.SVG_TRANSFORM_TRANSLATE);
        transform = transform==null ? new SVGOMTransform() : transform;
        transform.setTranslate(x,y);
        if (insertAtStart) {
            theTransList.insertItemBefore(transform,0);
        }
        else {
            theTransList.appendItem(transform);
        }

    }

    /**
     * Modifies the scale or sets a new one
     * 
     * @param x
     * @param y
     * @since 2.4
     */
    public void setScale(float x,float y) {
        SVGTransform transform = locate(SVGTransform.SVG_TRANSFORM_SCALE);
        if (transform!=null) {
            transform.setScale(x,y);
        }
        else {
            SVGOMTransform newTransform = new SVGOMTransform();
            newTransform.setScale(x,y);
            theTransList.appendItem(newTransform);
        }
    }

    /**
     * Sets an angle of rotation to the element.
     * 
     * @param angle The rotation to set
     * @param cx the centre of rotation x
     * @param cy The centre of rotation y
     * @since 2.4.3
     */
    public void setRotate(float rotate,float cx,float cy) {
        SVGTransform transform = locate(SVGTransform.SVG_TRANSFORM_ROTATE);
        if (transform!=null) {
            transform.setRotate(rotate,cx,cy);
        }
        else {
            SVGOMTransform newTransform = new SVGOMTransform();
            newTransform.setRotate(rotate,cx,cy);
            theTransList.appendItem(newTransform);
        }
    }

    /**
     * Adds an angle of rotation to the current rotation.
     * 
     * @param angle The rotation to add
     * @param cx the centre of rotation x
     * @param cy The centre of rotation y
     * @since 2.4.3
     */
    public void addRotate(float angle,float cx,float cy) {
        SVGTransform transform = locate(SVGTransform.SVG_TRANSFORM_ROTATE);
        if (transform!=null) {
            float m = transform.getAngle();
            transform.setRotate(m+angle,cx,cy);
        }
        else {
            SVGOMTransform newTransform = new SVGOMTransform();
            newTransform.setRotate(angle,cx,cy);
            theTransList.appendItem(newTransform);
        }
    }

    /**
     * Locate a transform from the list, or return null if not found
     * 
     * @param type to locate
     * @return the first transform of that type or null
     * @since 2.4
     */
    private SVGTransform locate(short type) {
        for (int i = 0;i<theTransList.getNumberOfItems();i++) {
            SVGTransform transform = theTransList.getItem(i);
            if (transform.getType()==type) {
                return transform;
            }
        }

        return null;
    }

    /**
     * Get the translation from the transform of this element
     * 
     * @return The translate
     * @since 2.4
     */
    public Point2D.Float getTranslate() {
        SVGTransform t = locate(SVGTransform.SVG_TRANSFORM_TRANSLATE);
        if (t!=null) {
            return new Point2D.Float(t.getMatrix().getE(),t.getMatrix().getF());
        }
        return new Point2D.Float(0,0);
    }

    /**
     * Get the scale from the transform of this element
     * 
     * @return the scale held in the transform as a point
     * @since 2.5.1
     */
    public Point2D.Float getScale() {
        SVGTransform t = locate(SVGTransform.SVG_TRANSFORM_SCALE);
        if (t!=null) {
            return new Point2D.Float(t.getMatrix().getA(),t.getMatrix().getD());
        }
        else {
            return new Point2D.Float(1f,1f);
        }
    }

}
