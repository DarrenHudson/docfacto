package com.docfacto.beermat.svg;

import java.util.ArrayList;

import org.apache.batik.dom.AbstractDocument;
import org.apache.batik.dom.GenericText;
import org.apache.batik.dom.svg.SVGOMTSpanElement;
import org.apache.batik.dom.svg.SVGOMTextElement;
import org.apache.batik.dom.svg.SVGOMTextPathElement;
import org.apache.batik.dom.svg.SVGOMTitleElement;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * When editing Text elements, always use this wrapper, and set text using this.
 * 
 * @author kporter - created 23 Jan 2014
 * @since 2.5.1
 */
public class TextElement {
    private SVGOMTextElement theText;
    private SVGOMTitleElement theTitle;
    private ArrayList<Fragment> theFragments;

    /**
     * Constructs a new TextElement, which wraps the {@link SVGOMTextElement}
     * 
     * @param element
     */
    public TextElement(SVGOMTextElement element) {
        theText = element;
        theFragments = new ArrayList<Fragment>();
        NodeList nl = element.getChildNodes();
        for (int i = 0;i<nl.getLength();i++) {
            Node node = nl.item(i);
            if (node instanceof SVGOMTSpanElement || node instanceof SVGOMTextPathElement) {
                theFragments.add(new Fragment(node.getTextContent(),node));
            }
            else if (node instanceof GenericText) {
                String text = node.getTextContent();
                text = text.trim();
                if (!text.isEmpty()&&!text.equals("\n")) {
                    theFragments.add(new Fragment(text,node));
                }
            }
            else if (node instanceof SVGOMTitleElement) {
                theTitle = (SVGOMTitleElement)node;
            }
        }
    }

    public void setTitle(String text) {
        theTitle.setTextContent(text);
    }

    public String getTitle() {
        if (theTitle!=null) {
            return theTitle.getTextContent();
        }
        return "";
    }

    /**
     * Get the wrapped element
     * 
     * @return The element that this is wrapping
     * @since 2.5.1
     */
    public Element getElement() {
        return theText;
    }

    /**
     * Gets the string of text content within the element
     * 
     * @return The string of text contents
     * @since 2.5.1
     */
    public String getTextContent() {
        StringBuilder multi = new StringBuilder();
        for (Fragment f:theFragments) {
            multi.append(f.content);
            multi.append(" ");
        }
        return multi.toString();
    }

    /**
     * Safely sets the text content belonging to the given text element.
     * 
     * Preserves the tspans, and title of the element
     * 
     * @param text The text to set
     * @since 2.5.1
     */
    public void setTextContents(String text) {
        String[] split = text.split(" ");
        int i = 0;
        for (Fragment f:theFragments) {
            if (i==split.length) {
                // There's no more text so set the current fragment to nothing and move on
                if (f.element instanceof GenericText) {
                    theText.removeChild(f.element);
                }
                else {
                    f.element.setTextContent("");
                }

                continue;
            }

            String[] fSplit = f.content.split(" ");

            if (fSplit.length==1) {
                if (!split[i].equals(f.content)) {
                    if (f.element instanceof GenericText) {
                        replaceGenericText(f.element," "+split[i]);
                    }
                    else {
                        f.element.setTextContent(split[i]);
                    }
                }

                // Add one as the first fragment as been matched/changed
                i++;
            }
            else {
                StringBuilder multi = new StringBuilder();
                for (int k = 0;k<fSplit.length;k++) {
                    if (i<split.length) {
                        multi.append(split[i]);
                        multi.append(" ");
                        i++;
                    }
                }
                String concat = multi.toString().trim();
                if (!concat.equals(f.content)) {
                    if (f.element instanceof GenericText) {
                        replaceGenericText(f.element," "+concat);
                    }
                    else {
                        f.element.setTextContent(concat);
                    }
                }
            }
        }

        for (;i<split.length;i++) {
            theText.appendChild(new GenericText(" "+split[i],(AbstractDocument)theText.getOwnerDocument()));
        }
    }

    private void replaceGenericText(Node replace,String text) {
        theText.insertBefore(new GenericText(text,(AbstractDocument)replace.getOwnerDocument()),replace);
        theText.removeChild(replace);
    }

    private class Fragment {
        private String content;
        private Node element;

        private Fragment(String content,Node element) {
            this.content = content;
            this.element = element;
        }

        @Override
        public String toString() {
            return content;
        }
    }

}
