package com.docfacto.beermat.svg;

import org.apache.batik.dom.svg.SVGOMSVGElement;
import org.w3c.dom.svg.SVGDocument;
import org.w3c.dom.svg.SVGLength;

/**
 * Wrapper class around the SVG Height and Width.
 * 
 * Size will be returned in pixels
 * 
 * @author dhudson - created 21 Jun 2013
 * @since 2.4
 */
public class SVGSize {

    private final SVGLength theLength;
    private final SVGLength theWidth;

    
    /**
     * Constructor.
     * 
     * @param doc
     * @since 2.4
     */
    public SVGSize(SVGDocument doc) {
        SVGOMSVGElement root = (SVGOMSVGElement)doc.getRootElement();

        theLength = root.getHeight().getBaseVal();
        theLength.convertToSpecifiedUnits(SVGLength.SVG_LENGTHTYPE_PX);

        theWidth = root.getWidth().getBaseVal();
        theWidth.convertToSpecifiedUnits(SVGLength.SVG_LENGTHTYPE_PX);
    }

    /**
     * Return the length
     * 
     * @return number of pixels
     * @since 2.4
     */
    public int getHeight() {
        return Math.round(theLength.getValueInSpecifiedUnits());
    }

    /**
     * Return the width
     * 
     * @return number of pixels
     * @since 2.4
     */
    public int getWidth() {
        return Math.round(theWidth.getValueInSpecifiedUnits());
    }

    /**
     * The height as a double in px
     * 
     * @return height in px
     * @since 2.4
     */
    public double getHeightAsDouble() {
        return (double) theLength.getValueInSpecifiedUnits();
    }
    
    /**
     * The width as a double
     * 
     * @return width in px
     * @since 2.4
     */
    public double getWidthAsDouble() {
        return (double) theWidth.getValueInSpecifiedUnits();
    }
    
    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ""+getHeight()+" px"+":"+getWidth()+" px";
    }

}
