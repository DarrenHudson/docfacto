package com.docfacto.beermat.svg;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.events.SVGSizeEvent;
import com.docfacto.beermat.listeners.BeermatEventListener;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.DocfactoException;
import com.docfacto.common.XMLUtils;

/**
 * An object that holds one Document only, and edits this document.
 * 
 * Contains methods to add elements, retrieve elements, or remove elements from
 * the SVG.
 * 
 * @author kporter - created Mar 6, 2013
 * @since 2.2
 * @docfacto.adam ignore
 */
public class SVGManager implements BeermatEventListener {

    public final static int SVG_DEFAULT_WIDTH = 1600;
    public final static int SVG_DEFAULT_HEIGHT = 1080;

    private final static String MAIN_DEFS_ID = "docfacto.defs";

    private Map<String,Element> theDefs = new HashMap<String,Element>(10);

    private SVGSize theSvgSize;

    private SVGDocument theDocument;

    private final BeermatController theController;

    /**
     * Constructs an SVGManager with
     * 
     * @since 2.2
     */
    public SVGManager(BeermatController controller) {
        this(controller,null);
    }

    /**
     * Constructs a new SVGManager using an existing Document.
     * 
     * @param document The document to copy in
     * @since 2.2
     */
    public SVGManager(BeermatController controller,SVGDocument document) {
        theController = controller;
        if (document==null) {
            theDocument =
                createNewDocument(SVG_DEFAULT_WIDTH,SVG_DEFAULT_HEIGHT);
        }
        else {
            theDocument = document;
        }

        theController.addListener(this);
    }

    /**
     * Creates a new blank Document.
     * 
     * @param width The width of the created SVG
     * @param height The height of the SVG
     * @return The new document
     * @since 2.2
     */
    private SVGDocument createNewDocument(int width,int height) {
        theDefs.clear();

        SVGDocument doc = SVGUtils.createSVGDocument(null);
        SVGUtils.setSVGHeightWidth(doc,height,width);

        return doc;
    }

    /**
     * Clears the current document, preserving defs.
     * 
     * @since 2.2
     */
    public void clearDocument() {
        theDocument = createNewDocument(SVG_DEFAULT_WIDTH,SVG_DEFAULT_WIDTH);
        theController.getView().getSVGComposite().setDocument(theDocument);
    }

    /**
     * Set the new SVG Document, removing the old one
     * 
     * @param doc new SVG Document
     * @since 2.4
     */
    public void setSVGDocument(SVGDocument doc) {
        theDocument = doc;
        theController.getView().getSVGComposite().setDocument(theDocument);
    }

    /**
     * Cleans the canvas from all the highlight elements that may have been
     * left... also removes blank "defs" elements (with no children).
     * 
     * @since 2.2
     */
    public void cleanDocument() {
        // NodeList all = theDocument.getDocumentElement().getChildNodes();
        // List<Node> removeNodes = new ArrayList<Node>(1);
        // Element def = getDefsMain();
        // NodeList defs = def.getChildNodes();
        // for (int i = 0;i<defs.getLength();i++) {
        // if (defs.item(i).getNodeType()==1) {
        // if (((Element)defs.item(i)).getAttribute("class").equals(
        // HighlightElement.HIGHLIGHT_CLASS)) {
        // System.out.println("found class beermathighlight");
        // def.removeChild(defs.item(i));
        // }
        // }
        // }
        //
        // for (int i = 0;i<all.getLength();i++) {
        // Node n = all.item(i);
        // if (n.getNodeType()==1) {
        //
        // boolean remove = false;
        // Element e = (Element)n;
        //
        // if (e.hasAttribute("class")) {
        // if (e.getAttribute("class").equals(
        // HighlightElement.HIGHLIGHT_CLASS)) {
        // remove = true;
        // }
        // }
        //
        // if (e.getNodeName().equals("defs")) {
        // if (!e.hasChildNodes()) {
        // remove = true;
        // }
        // }
        //
        // if (remove) {
        // removeNodes.add(e);
        // }
        // }
        // }
        //
        // for (Node n:removeNodes) {
        // theDocument.getDocumentElement().removeChild(n);
        // }
    }

    /**
     * Gets or creates a new def element and returns it.
     * <p>
     * <ul>
     * <li>If the document does not contain a def element, then a new def
     * element is created in the given document</li>
     * <li>If the document contains a def element, with the ID of main.defs then
     * that defs element is created</li>
     * </ul>
     * SVG can contain several def elements in one document, but at the moment
     * only a docfacto.defs is dealt with
     * </p>
     * 
     * @return The def element contained in the doc, or a new def element
     * created in the doc
     * @since 2.2.0
     */
    public Element getDefsMain() {
        if (theDocument.getElementById(MAIN_DEFS_ID)!=null) {

            return theDocument.getElementById(MAIN_DEFS_ID);
        }
        else if (theDocument.getElementsByTagName("defs").item(0)!=null) {
            NodeList nl = theDocument.getElementsByTagName("defs");
            Element defs = (Element)nl.item(0);
            defs.setAttribute("id",MAIN_DEFS_ID);
            return defs;
        }
        else {
            return createDefsMain();
        }
    }

    private Element createDefsMain() {
        Element defs =
            theDocument.createElementNS(SVGUtils.SVG_NAMESPACE,"defs");
        defs.setAttribute("id",MAIN_DEFS_ID);
        theDocument.getDocumentElement().appendChild(defs);
        return defs;
    }

    public void addDef(Element element) {
        
        if (!element.hasAttribute("id")) {
            element.setAttribute("id",XMLUtils.generateUnqiueID());
        }
        
        Element defs = getDefsMain();
        defs.appendChild(element);
        theDefs.put(element.getAttribute("id"),element);
    }

    public Set<String> getDefList() {
        return theDefs.keySet();
    }

    /**
     * Checks whether a given element's ID is contained in the defs.
     * 
     * @param ID The ID of the element to check in the defs.
     * @return True if the element is a def.
     * @since 2.2.0
     */
    public boolean hasDef(String ID) {
        if (theDocument.getElementById(ID)!=null) {
            return true;
        }
        else {
            return theDefs.containsKey(ID);
        }
    }

    /**
     * Returns the document held by this manager.
     * 
     * @return The Document held
     * @since 2.2.0
     */
    public SVGDocument getDocument() {
        return theDocument;
    }

    /**
     * @see com.docfacto.beermat.listeners.BeermatEventListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    @Override
    public void handleEvent(BeermatEvent event) {
        if (event.getAction()==SVGSizeEvent.SVG_SIZE_ACTION) {
            SVGSizeEvent sizeEvent = (SVGSizeEvent)event;
            theSvgSize = sizeEvent.getSize();
        }
    }

    /**
     * Return the Size of the SVG
     * 
     * @return the current size of the SVG Document
     * @since 2.4
     */
    public SVGSize getDocumentSize() {
        return theSvgSize;
    }

    /**
     * Return the current document in formatted text form
     * 
     * @return
     * @throws DocfactoException
     * @since 2.4
     */
    public String getFormattedDocument() throws DocfactoException {
        return XMLUtils.prettyFormat(theDocument);
    }
    
    /**
     * Free up the dom
     * 
     * @since 2.5
     */
    public void shutdown() {
        theDefs = null;
        theDocument = null;
    }
}
