package com.docfacto.beermat.svg;

import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.List;
import java.util.Set;

import org.apache.batik.dom.svg.SVGGraphicsElement;
import org.apache.batik.dom.svg.SVGOMCircleElement;
import org.apache.batik.dom.svg.SVGOMDocument;
import org.apache.batik.dom.svg.SVGOMEllipseElement;
import org.apache.batik.dom.svg.SVGOMLineElement;
import org.apache.batik.dom.svg.SVGOMRectElement;
import org.apache.batik.dom.svg.SVGOMSVGElement;
import org.apache.batik.dom.svg.SVGOMTextElement;
import org.apache.batik.dom.svg.SVGOMTextPathElement;
import org.apache.batik.dom.svg.SVGOMUseShadowRoot;
import org.apache.batik.gvt.GraphicsNode;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.svg.SVGTextElement;

import com.docfacto.beermat.hotspot.HotSpot;
import com.docfacto.beermat.hotspot.NSEWCHotSpots;
import com.docfacto.beermat.hotspot.NSEWHotSpots;
import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.beermat.svg.DragData.LinePosition;
import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.beermat.utils.BeermatUtils;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.DocfactoException;
import com.docfacto.core.utils.MathUtils;

/**
 * Utility class to handle shape movement
 * 
 * @author dhudson - created 1 Aug 2013
 * @since 2.4
 */
public class ShapeShifter {
    // Lets just have one of these, although we might want to
    // create a thread local one ..
    public static final ConnectorHandler theConnectorHandler =
        new ConnectorHandler();

    /**
     * Changes the element based on what type of element it is, and the drag.
     * 
     * All dragging actions should go through this method, after which the
     * correct method of action is called on the given element.
     * 
     * @param element to change
     * @param data The data for the drag.
     * @since 2.4
     */
    public static void updateElement(Element element,DragData data) {
        if (element==null) {
            // If element is null, then do nothing now.
            return;
        }

        if ((data.getXDiff()==0&&data.getYDiff()==0)) {
            // If there is no different in mouse position, do nothing.
            return;
        }

        if (SVGUtils.isAnchorableElement(element)) {
            return;
        }

        if (!(element instanceof SVGOMTextElement)) {
            // If its not a graphical element, like a marker, there is not much
            // that
            // can be done
            if (!(element instanceof SVGGraphicsElement)) {
                return;
            }
        }

        // If its a use node, grab the use element
        Node parentNode = element.getParentNode();
        if (parentNode instanceof SVGOMUseShadowRoot) {
            SVGOMUseShadowRoot useRoot = (SVGOMUseShadowRoot)parentNode;
            element = (Element)useRoot.getCSSParentNode();
        }

        try {
            // If the parent of the element is an svg element, but its
            // grandparent is not the document, this means its an embedded svg
            // so therefore move the whole svg parent
            if (element.getParentNode() instanceof SVGOMSVGElement&&
                !(element.getParentNode().getParentNode() instanceof SVGOMDocument)) {
                addToXY((SVGOMSVGElement)element.getParentNode(),
                    data.getXDiff(),data.getYDiff());
                return;
            }

            boolean hotspot = false;
            // Check if the hotspot has been pressed
            if (data.getHotspot()!=null) {
                hotspot = true;
            }

            if (hotspot&&data.getRetainAspect()) {
                setRetainedAspectRatio(element,data);
            }

            // Hotspot was not a rotate, or no hotspot was pressed
            // So check what type of element, and then perform relevant method.
            if (element instanceof SVGOMRectElement) {

                if (hotspot) {
                    dragRect(element,data);
                }
                else {
                    addToXY(element,data.getXDiff(),data.getYDiff());
                }
            }
            else if (element instanceof SVGTextElement) {
                addToXY(element,data.getXDiff(),data.getYDiff());
            }
            else if (element instanceof SVGOMCircleElement) {
                if (hotspot) {
                    dragCircle(element,data);
                }
                else {
                    addToCxCy(element,data.getXDiff(),data.getYDiff());
                }
            }
            else if (element instanceof SVGOMEllipseElement) {
                if (hotspot) {
                    dragEllipse(element,data);
                }
                else {
                    addToCxCy(element,data.getXDiff(),data.getYDiff());
                }
            }
            else if (element instanceof SVGOMLineElement) {
                if (hotspot) {
                    dragLine(element,data);
                }
                else {
                    addToLineXY(element,data.getXDiff(),data.getYDiff());
                }
            }
            else {
                // The element is a group, or a path, or another type of element
                // with no 'x' 'y' that can be modified, so use skews and
                // translates.
                if (hotspot) {
                    // Skew element.
                    dragAll(element,data);
                }
                else {
                    // Translate element
                    moveAnyElement(element,data.getXDiff(),data.getYDiff());
                }
            }
        }
        catch (Throwable ex) {
            BeermatUIPlugin.logException("drag error",ex);
        }

    }

    /**
     * Adapter for {@link #dragAll(Element, float, float, int, AffineTransform)}
     * 
     * @param element The element to stretch
     * @param data The DragData for this drag
     * @since 2.4.3
     * @see #dragAll(Element, float, float, int, AffineTransform)
     */
    public static void dragAll(Element element,DragData data) {
        dragAll(element,data.getXDiff(),data.getYDiff(),data.getHotspot()
            .getLocation(),data.getRetainAspect(),data.getTransform());
    }

    /**
     * This method can be used for any element as it relies on scaling.
     * 
     * The method gets the bounds from the element, calculates a scale based on
     * the drag difference, and adds this scale to this existing scale to
     * enlarge or reduce the element.
     * 
     * {@docfacto.system title="method logic"
     * <ol>
     * <li>Invert the x or y drag difference if the HotSpot is w/nw/n because
     * here dragging away from the shape means making it bigger</li>
     * <li>Only calculate a new scale if the bounds of the object holds a
     * width/height of 1 or greater. This is there to prevent infinity floats
     * (dividing by 0)</li>
     * <li>The old scale must be gotten and added to the calculated difference
     * in scale, as each drag we only calculate a difference in scale and not
     * the total scale</li></ol>}
     * 
     * {@docfacto.system title="infinity floats" Before settings this new scale
     * to the element we must check that scale is greater than 0.01, this does
     * limit elements ability to scale down even further, but when an element
     * had a scale of 1e-4 or less, the next scale was getting NaN or infinity
     * floats causing the element to disappear}
     * 
     * @param element The element to drag
     * @param xDiff The x difference in movement
     * @param yDiff The y difference in movement
     * @param hotspotLocation The direction of the drag
     * @param retainAspect true if the aspect ratio is to be retained
     * @param transform The transform applied on the canvas required to locate
     * elements
     * @since 2.4.3
     */
    public static void dragAll(Element element,float xDiff,float yDiff,
    int hotspotLocation,boolean retainAspect,AffineTransform transform) {
        // This is necessary to keep the drag movement 1:1 with the mouse.
        Point2D p = SVGUtils.getElementScale(element);
        float x = (float)(xDiff*p.getX());
        float y = (float)(yDiff*p.getY());

        Rectangle bounds = SVGUtils.getBounds(element,transform);

        // retain aspect needed if shift is down
        if (retainAspect) {
            Point2D.Float f =
                MathUtils.retainAspectRatio(x,y,bounds.width,bounds.height);
            x = f.x;
            y = f.y;
        }

        // inverse x/y depending on ne/se/sw/nw/etc.
        switch (hotspotLocation) {
        case HotSpot.NORTH_WEST: {
            x = xDiff<0 ? Math.abs(xDiff) : -xDiff;
            y = yDiff<0 ? Math.abs(yDiff) : -yDiff;
            break;
        }
        case HotSpot.WEST:
            y = 0;
        case HotSpot.SOUTH_WEST: {
            x = xDiff<0 ? Math.abs(xDiff) : -xDiff;
            break;
        }
        case HotSpot.NORTH:
            x = 0;
        case HotSpot.NORTH_EAST: {
            y = yDiff<0 ? Math.abs(yDiff) : -yDiff;
            break;
        }
        case HotSpot.SOUTH: {
            x = 0;
            break;
        }
        case HotSpot.EAST: {
            y = 0;
            break;
        }
        }

        Point2D oldScale = SVGUtils.getElementScale(element);

        double xScale = oldScale.getX();
        double yScale = oldScale.getY();

        if (bounds.width>=1) {
            xScale = x/bounds.width;
            xScale += oldScale.getX();
        }
        if (bounds.height>=1) {
            yScale = y/bounds.height;
            yScale += oldScale.getY();
        }

        TransformElementParser transformer =
            new TransformElementParser(element);

        if ((xScale>0.01&&yScale>0.01)&&
            (xScale!=Float.POSITIVE_INFINITY&&yScale!=Float.POSITIVE_INFINITY)) {
            transformer.setScale((float)xScale,(float)yScale);
        }
    }

    /**
     * Performs the hotspot drag action for a rect element only.
     * 
     * @param element The element to drag
     * @param data The drag data
     * @since 2.4.3
     */
    private static void dragRect(Element element,DragData data) {
        HotSpot hotspot = data.getHotspot();
        switch (hotspot.getLocation()) {
        case HotSpot.EAST: {
            // Change width
            dragRectWidth(element,data);
            break;
        }
        case HotSpot.SOUTH_EAST: {
            // change height and width
            dragRectWidth(element,data);
            dragRectheight(element,data);
            break;
        }
        case HotSpot.SOUTH: {
            // change height
            dragRectheight(element,data);
            break;
        }
        case HotSpot.SOUTH_WEST: {
            // change height and width, and deduct difference in width from
            // the x. y remains the same
            dragRectheight(element,data);
            dragRectWidthUpdateX(element,data);
            break;
        }
        case HotSpot.WEST: {
            // change width only, deducting difference from x
            dragRectWidthUpdateX(element,data);
            break;
        }
        case HotSpot.NORTH_WEST: {
            // change height and width, and amend x and y
            dragRectWidthUpdateX(element,data);
            dragRectHeightUpdateY(element,data);
            break;
        }
        case HotSpot.NORTH: {
            // change height, amend y
            dragRectHeightUpdateY(element,data);
            break;
        }
        case HotSpot.NORTH_EAST: {
            // change height and width, amend y only.
            dragRectWidth(element,data);
            dragRectHeightUpdateY(element,data);
            break;
        }
        }
    }

    private static void dragRectWidth(Element element,DragData data) {
        float width = Float.parseFloat(element.getAttribute("width"));
        if (width+data.getXDiff()>0) {
            SVGUtils.setAttribute(element,"width",width+data.getXDiff());
        }
    }

    private static void dragRectheight(Element element,DragData data) {
        float height = Float.parseFloat(element.getAttribute("height"));
        if (height+data.getYDiff()>0) {
            SVGUtils.setAttribute(element,"height",height+data.getYDiff());
        }
    }

    private static void dragRectWidthUpdateX(Element element,DragData data) {
        float xDiff =
            data.getXDiff()<0 ? Math.abs(data.getXDiff()) : -data.getXDiff();
        float x = Float.parseFloat(element.getAttribute("x"));
        float width = Float.parseFloat(element.getAttribute("width"));

        if (width+xDiff>0) {
            SVGUtils.setAttribute(element,"x",x+data.getXDiff());
            SVGUtils.setAttribute(element,"width",width+xDiff);
        }
    }

    private static void dragRectHeightUpdateY(Element element,DragData data) {
        float yDiff =
            data.getYDiff()<0 ? Math.abs(data.getYDiff()) : -data.getYDiff();

        float height = Float.parseFloat(element.getAttribute("height"));

        float y = Float.parseFloat(element.getAttribute("y"));

        if (height+yDiff>0) {
            SVGUtils.setAttribute(element,"height",height+yDiff);
            SVGUtils.setAttribute(element,"y",y+data.getYDiff());
        }
    }

    /**
     * Drag out the radius of a circles.
     * 
     * @param element The element to drag
     * @param data The drag data
     * @since 2.4.3
     */
    private static void dragCircle(Element element,DragData data) {
        float r = Float.parseFloat(element.getAttribute("r"));
        HotSpot hotspot = data.getHotspot();
        switch (hotspot.getLocation()) {
        case HotSpot.EAST: {
            if (r+data.getXDiff()>0) {
                SVGUtils.setAttribute(element,"r",r+data.getXDiff());
            }
            break;
        }
        case HotSpot.WEST: {
            float xDiff =
                data.getXDiff()<0 ? Math.abs(data.getXDiff()) : -data
                    .getXDiff();
            if (r+xDiff>0) {
                SVGUtils.setAttribute(element,"r",r+xDiff);
            }
            break;
        }
        case HotSpot.NORTH: {
            float yDiff =
                data.getYDiff()<0 ? Math.abs(data.getYDiff()) : -data
                    .getYDiff();
            if (r+yDiff>0) {
                SVGUtils.setAttribute(element,"r",r+yDiff);
            }
            break;
        }
        case HotSpot.SOUTH: {
            if (r+data.getYDiff()>0) {
                SVGUtils.setAttribute(element,"r",r+data.getYDiff());
            }
            break;
        }

        case HotSpot.NORTH_WEST: {
            float yDiff =
                data.getYDiff()<0 ? Math.abs(data.getYDiff()) : -data
                    .getYDiff();
            float xDiff =
                data.getXDiff()<0 ? Math.abs(data.getXDiff()) : -data
                    .getXDiff();
            float largest = Math.abs(xDiff)>Math.abs(yDiff) ? xDiff : yDiff;
            if (r+largest>0) {
                SVGUtils.setAttribute(element,"r",r+largest);
            }
            break;
        }
        case HotSpot.NORTH_EAST: {
            float yDiff =
                data.getYDiff()<0 ? Math.abs(data.getYDiff()) : -data
                    .getYDiff();
            float largest =
                Math.abs(data.getXDiff())>Math.abs(yDiff) ? data.getXDiff()
                    : yDiff;
            if (r+largest>0) {
                SVGUtils.setAttribute(element,"r",r+largest);
            }
            break;
        }
        case HotSpot.SOUTH_EAST: {
            float largest =
                Math.abs(data.getXDiff())>Math.abs(data.getYDiff()) ? data
                    .getXDiff() : data.getYDiff();
            if (r+largest>0) {
                SVGUtils.setAttribute(element,"r",r+largest);
            }
            break;
        }

        case HotSpot.SOUTH_WEST: {
            float xDiff =
                data.getXDiff()<0 ? Math.abs(data.getXDiff()) : -data
                    .getXDiff();
            float largest =
                Math.abs(xDiff)>Math.abs(data.getYDiff()) ? xDiff : data
                    .getYDiff();
            if (r+largest>0) {
                SVGUtils.setAttribute(element,"r",r+largest);
            }
            break;
        }
        }
    }

    /**
     * Drag an ellipse Has multiple radius attributes, drags the correct one
     * depending on the hotspot.
     * 
     * @param element The Element to drag
     * @param data The drag data
     * @since 2.4.3
     */
    private static void dragEllipse(Element element,DragData data) {
        HotSpot hotspot = data.getHotspot();
        float rx = Float.parseFloat(element.getAttribute("rx"));
        float ry = Float.parseFloat(element.getAttribute("ry"));

        switch (hotspot.getLocation()) {
        case HotSpot.EAST: {
            if (rx+data.getXDiff()>0) {
                SVGUtils.setAttribute(element,"rx",rx+data.getXDiff());
            }
            break;
        }
        case HotSpot.WEST: {
            float xDiff =
                data.getXDiff()<0 ? Math.abs(data.getXDiff()) : -data
                    .getXDiff();
            if (rx+xDiff>0) {
                SVGUtils.setAttribute(element,"rx",rx+xDiff);
            }
            break;
        }

        case HotSpot.NORTH: {
            float yDiff =
                data.getYDiff()<0 ? Math.abs(data.getYDiff()) : -data
                    .getYDiff();
            if (ry+yDiff>0) {
                SVGUtils.setAttribute(element,"ry",ry+yDiff);
            }
            break;
        }
        case HotSpot.SOUTH: {
            if (ry+data.getYDiff()>0) {
                SVGUtils.setAttribute(element,"ry",ry+data.getYDiff());
            }
            break;
        }

        case HotSpot.NORTH_WEST: {
            float yDiff =
                data.getYDiff()<0 ? Math.abs(data.getYDiff()) : -data
                    .getYDiff();
            float xDiff =
                data.getXDiff()<0 ? Math.abs(data.getXDiff()) : -data
                    .getXDiff();
            if (rx+xDiff>0) {
                SVGUtils.setAttribute(element,"rx",rx+xDiff);
            }
            if (ry+yDiff>0) {
                SVGUtils.setAttribute(element,"ry",ry+yDiff);
            }
            break;
        }
        case HotSpot.NORTH_EAST: {
            float yDiff =
                data.getYDiff()<0 ? Math.abs(data.getYDiff()) : -data
                    .getYDiff();
            if (rx+data.getXDiff()>0) {
                SVGUtils.setAttribute(element,"rx",rx+data.getXDiff());
            }
            if (ry+yDiff>0) {
                SVGUtils.setAttribute(element,"ry",ry+yDiff);
            }
            break;
        }
        case HotSpot.SOUTH_EAST: {
            if (rx+data.getXDiff()>0) {
                SVGUtils.setAttribute(element,"rx",rx+data.getXDiff());
            }
            if (rx+data.getYDiff()>0) {
                SVGUtils.setAttribute(element,"ry",ry+data.getYDiff());
            }
            break;
        }

        case HotSpot.SOUTH_WEST: {
            float xDiff =
                data.getXDiff()<0 ? Math.abs(data.getXDiff()) : -data
                    .getXDiff();
            if (rx+xDiff>0) {
                SVGUtils.setAttribute(element,"rx",rx+xDiff);
            }
            if (rx+data.getYDiff()>0) {
                SVGUtils.setAttribute(element,"ry",ry+data.getYDiff());
            }
            break;
        }
        }
    }

    /**
     * A method to drag a line
     * 
     * The method first calculates which x or y values are the closest. Then
     * sets them to the drag data. It then alters the correct attributes
     * 
     * @param element The Element to drag
     * @param data The drag data
     * @since 2.4.3
     */
    private static void dragLine(Element element,DragData data) {
        HotSpot hotspot = data.getHotspot();

        if (data.getLinePosition()!=LinePosition.NONE) {
            LinePosition pos = data.getLinePosition();
            if (pos==LinePosition.X1Y1) {
                float x = Float.parseFloat(element.getAttribute("x1"));
                float y = Float.parseFloat(element.getAttribute("y1"));
                SVGUtils.setAttribute(element,"x1",x+data.getXDiff());
                SVGUtils.setAttribute(element,"y1",y+data.getYDiff());
            }
            else if (pos==LinePosition.X2Y2) {
                float x = Float.parseFloat(element.getAttribute("x2"));
                float y = Float.parseFloat(element.getAttribute("y2"));
                SVGUtils.setAttribute(element,"x2",x+data.getXDiff());
                SVGUtils.setAttribute(element,"y2",y+data.getYDiff());
            }
        }
        else {
            float x1 = Float.parseFloat(element.getAttribute("x1"));
            float y1 = Float.parseFloat(element.getAttribute("y1"));
            float x2 = Float.parseFloat(element.getAttribute("x2"));
            float y2 = Float.parseFloat(element.getAttribute("y2"));

            int hotspotX = (int)hotspot.getShape().getBounds().getX();
            int hotspotY = (int)hotspot.getShape().getBounds().getY();
            // Get a total for the x and y for the hotspot
            int hotspotTotal = hotspotX+hotspotY;
            // Get the difference between the x1+y1 total and the hotspot
            // total..
            float closestTotal = Math.abs(hotspotTotal-(x1+y1));
            // Check if the difference between the x2+y2 total is smaller, if it
            // is, thats the dragged side.
            if (Math.abs(hotspotTotal-(x2+y2))<closestTotal) {
                data.setLinePosition(LinePosition.X2Y2);
            }
            else {
                // If not, x1+y1 was indeed closer.
                data.setLinePosition(LinePosition.X1Y1);
            }
        }
    }

    private static void addToXY(Element element,float diffx,float diffy) {
        // TransformElementParser t = new TransformElementParser(element);
        float x;
        if (element.getAttribute("x").isEmpty()) {
            x = 0;
        }
        else {
            x = Float.parseFloat(element.getAttribute("x"));
        }

        float y;
        if (element.getAttribute("y").isEmpty()) {
            y = 0;
        }
        else {
            y = Float.parseFloat(element.getAttribute("y"));
        }

        SVGUtils.setAttribute(element,"x",x+diffx);
        SVGUtils.setAttribute(element,"y",y+diffy);
    }

    private static void addToCxCy(Element element,float diffx,float diffy) {
        float cx = 0;
        float cy = 0;

        if (!element.getAttribute("cx").isEmpty()) {
            cx = Float.parseFloat(element.getAttribute("cx"));
        }
        if (!element.getAttribute("cy").isEmpty()) {
            cy = Float.parseFloat(element.getAttribute("cy"));
        }

        SVGUtils.setAttribute(element,"cx",cx+diffx);
        SVGUtils.setAttribute(element,"cy",cy+diffy);

    }

    private static void addToLineXY(Element element,float diffx,float diffy) {
        float x1 = Float.parseFloat(element.getAttribute("x1"));
        float y1 = Float.parseFloat(element.getAttribute("y1"));
        float x2 = Float.parseFloat(element.getAttribute("x2"));
        float y2 = Float.parseFloat(element.getAttribute("y2"));
        SVGUtils.setAttribute(element,"x1",x1+diffx);
        SVGUtils.setAttribute(element,"x2",x2+diffx);
        SVGUtils.setAttribute(element,"y1",y1+diffy);
        SVGUtils.setAttribute(element,"y2",y2+diffy);
    }

    private static void moveAnyElement(Element element,float diffx,float diffy) {
        TransformElementParser parser = new TransformElementParser(element);
        parser.translate(diffx,diffy);
    }

    /**
     * Rotates an element by a given amount
     * <p>
     * {@docfacto.system title="Calculating the degrees of rotation" The degrees
     * of rotation are calculated by the following steps.
     * <ul>
     * <li>Find the centre point of the element, this must be its point on the
     * canvas</li> <li>Get the location the mouse is currently at, and calculate
     * a radius from the centre to the mouse</li> <li>Calculate the imaginary 12
     * o'clock point, directly above the centre</li> <li>The degrees can then be
     * calculated, as we have an arc between 12oclock and mouse position</li>
     * <li>Set degrees of rotation to element</li>
     * </ul>}
     * </p>
     * 
     * @param element The element to rotate.
     * @param data The drag data to calculate the rotation.
     * @since 2.4.3
     */
    public static void rotateElement(Element element,DragData data) {

        // Getting the centre throws an exception, if the element doesn't have a
        // centre it can' t be rotated.
        // try {
        Point center =
            SVGUtils.getElementCentre(element,data.getCanvas()
                .getUpdateManager().getBridgeContext());

        float x = data.getMouseLocation().x;
        float y = data.getMouseLocation().y;

        double radius =
            Math.sqrt(Math.abs(x-center.x)*Math.abs(x-center.x)+
                Math.abs(y-center.y)*Math.abs(y-center.y));

        double p0x = center.x;
        double p0y = center.y-radius;

        int angle = (int)getAngleDegrees(p0x,p0y,x,y);
        // double theta = getAngleRads(p0x,p0y,x,y);

        TransformElementParser transformer =
            new TransformElementParser(element);
        transformer.setRotate(angle,center.x,center.y);

        // SVGOMElement e = (Si9 VGOMElement)element;
        // AffineTransform t1 = e.getSVGContext().getGlobalTransform();
        // AffineTransform t2 =
        // AffineTransformUtil.rotate(t1,theta,center.x,center.y);
        // }
        // catch (DocfactoException e) {
        // // Element does not have a centre point.
        // e.printStackTrace();
        // }

    }

    private static double getAngleDegrees(double p0x,double p0y,double p1x,
    double p1y) {
        double rads = Math.atan2(p1y-p0y,p1x-p0x)*2;
        return Math.toDegrees(rads);
    }

    /**
     * Takes a drag data and sets its diffx and diffy to new values with the
     * aspect ratio retained
     * 
     * {@docfacto.system last switch statement <b>required</b>, to
     * <em>invert</em> the sign, only if the direction of dragging is the north
     * east or south west (where one direction has positive movement, and the
     * other has negative, as we have multiplied one difference by the other,
     * the sign may have switched. I.E drag difference was 1,-2, after aspect
     * retaining, its now -1.5,-2, so -1.5 needs to be inverted}
     * 
     * @param element The element
     * @param data The drag data
     * @since 2.4.4
     */
    private static void setRetainedAspectRatio(Element element,DragData data) {
        Rectangle r = SVGUtils.getBounds(element,data.getTransform());
        float width = r.width;
        float height = r.height;
        float xDiff = data.getXDiff();
        float yDiff = data.getYDiff();

        Point2D.Float f = MathUtils.retainAspectRatio(xDiff,yDiff,width,height);
        float newX = f.x;
        float newY = f.y;

        switch (data.getHotspot().getLocation()) {
        case HotSpot.NORTH_EAST:
        case HotSpot.SOUTH_WEST:
            if (data.getYDiff()>=0&&data.getXDiff()<=0) {
                newY = Math.abs(newY);
                newX = newX>0 ? -newX : newX;
            }
            else {
                newX = Math.abs(newX);
                newY = newY>0 ? -newY : newY;
            }
            break;
        }

        data.setXDiff(newX);
        data.setYDiff(newY);
    }

    /**
     * Update any connectors if connected to something moving
     * 
     * @param canvas beermat canvas
     * @param highlightedElements moving elements
     * @param transform to use
     * @since 2.4
     */
    public static void updateAnchoredElements(BeermatAWTCanvas canvas,
    List<Element> highlightedElements,AffineTransform transform) {

        if (highlightedElements.size()>0) {
            Set<Element> connectors =
                BeermatUtils.getConnectors(highlightedElements.get(0)
                    .getOwnerDocument());

            // Lets see if this has a connector..
            findAndUpdateConnectors(canvas,transform,connectors);

            for (Element element:highlightedElements) {
                // Did it have text
                findAndUpdateText(canvas,element,transform);
            }
        }
    }

    /**
     * See if there are any connectors around the shape being moved, and if so
     * move them as well.
     * 
     * @param element
     * @param data
     * @since 2.4
     */
    private static void findAndUpdateConnectors(BeermatAWTCanvas canvas,
    AffineTransform transform,Set<Element> connectors) {

        for (Element connectorElement:connectors) {

            Element startNode = ConnectorHandler.getStartNode(connectorElement);
            if (startNode!=null) {

                try {
                    NSEWHotSpots hotSpots =
                        new NSEWHotSpots(canvas,
                            canvas.getGraphicsNodeFor(startNode));

                    // Move start ..
                    HotSpot hotSpot =
                        hotSpots
                            .getHotSpotForLocation(SVGUtils
                                .getDocfactoAttribute(
                                    connectorElement,
                                    ConnectorHandler.CONNECTOR_START_HOTSPOT_ATTRIBUTE));

                    changeConnectorEndpoint(connectorElement,hotSpot,
                        transform,false);
                }
                catch (DocfactoException ex) {
                    BeermatUIPlugin.logException(
                        "ShapeShifter: find and update connectors",ex);
                }
            }

            Element endNode = ConnectorHandler.getEndNode(connectorElement);

            if (endNode!=null) {

                try {
                    NSEWHotSpots hotSpots =
                        new NSEWHotSpots(canvas,
                            canvas.getGraphicsNodeFor(endNode));

                    // Move end ..
                    HotSpot hotSpot =
                        hotSpots
                            .getHotSpotForLocation(SVGUtils
                                .getDocfactoAttribute(
                                    connectorElement,
                                    ConnectorHandler.CONNECTOR_END_HOTSPOT_ATTRIBUTE));

                    changeConnectorEndpoint(connectorElement,hotSpot,
                        transform,true);
                }
                catch (DocfactoException ex) {
                    BeermatUIPlugin.logException(
                        "ShapeShifter: find and update connectors",ex);
                }
            }

        }
    }

    // private static void findAndUpdateConnectors(BeermatAWTCanvas canvas,
    // Element element,AffineTransform transform,Set<Element> connectors) {
    //
    // if(element instanceof SVGOMGElement) {
    // // Need to check for children as well.
    // SVGOMGElement group = (SVGOMGElement) element;
    //
    // NodeList childNodes = group.getChildNodes();
    // for (int i = 0;i<childNodes.getLength();i++) {
    // Node node = childNodes.item(i);
    //
    // }
    // }
    //
    // String elementId = element.getAttribute("id");
    //
    // if (StringUtils.nullOrEmpty(elementId)) {
    // // Nothing to do
    // return;
    // }
    //
    // for (Element connectorElement:connectors) {
    //
    // if (ConnectorHandler.isConnectorFor(element,connectorElement)) {
    //
    // try {
    // // At this point we know that its a line..
    // NSEWHotSpots hotSpots =
    // new NSEWHotSpots(canvas,
    // canvas.getGraphicsNodeFor(element));
    //
    // if (elementId.equals(SVGUtils.getDocfactoAttribute(
    // connectorElement,
    // ConnectorHandler.CONNECTOR_START_ATTRIBUTE))) {
    // // Move start ..
    // HotSpot hotSpot =
    // hotSpots
    // .getHotSpotForLocation(SVGUtils
    // .getDocfactoAttribute(
    // connectorElement,
    // ConnectorHandler.CONNECTOR_START_HOTSPOT_ATTRIBUTE));
    //
    // changeConnectorEndpoint(connectorElement,hotSpot,
    // transform,false);
    // }
    // else if (elementId.equals(SVGUtils.getDocfactoAttribute(
    // connectorElement,
    // ConnectorHandler.CONNECTOR_END_ATTRIBUTE))) {
    // // Move end ..
    // HotSpot hotSpot =
    // hotSpots
    // .getHotSpotForLocation(SVGUtils
    // .getDocfactoAttribute(
    // connectorElement,
    // ConnectorHandler.CONNECTOR_END_HOTSPOT_ATTRIBUTE));
    //
    // changeConnectorEndpoint(connectorElement,hotSpot,
    // transform,true);
    // }
    // }
    // catch (DocfactoException ignore) {
    // // Already null checked
    // ignore.printStackTrace();
    // }
    // }
    // }
    // }
    /**
     * Change the start or end of the connector.
     * 
     * @param connectorElement the connector
     * @param hotSpot location
     * @param transform to use
     * @param end true if its an end connector node
     * @since 2.5
     */
    private static void changeConnectorEndpoint(Element connectorElement,
    HotSpot hotSpot,AffineTransform transform,boolean end) {
        if (hotSpot!=null) {
            Point2D newPoint =
                SVGUtils.getTransformedPoint(hotSpot.getBounds().getCenterX(),
                    hotSpot.getBounds().getCenterY(),
                    transform);

            // Get Co-ordinates
            theConnectorHandler.parser(connectorElement);

            if (end) {
                theConnectorHandler.setEnd(newPoint.getX(),newPoint.getY());
            }
            else {
                theConnectorHandler.setStart(newPoint.getX(),newPoint.getY());
            }

            theConnectorHandler.updatePath(connectorElement);

            updateConnectorText(connectorElement);
        }
    }

    /**
     * Update the text path element of text
     * 
     * @param connector which is being moved
     * @since 2.5
     */
    private static void updateConnectorText(Element connector) {
        // There will only be one
        List<Element> elements = TextHandler.getAnchoredElements(connector);

        for (Element textElement:elements) {

            if (textElement instanceof SVGOMTextElement) {
                // We have been called from the connector logic need to repaint
                // the text for the path, as the path has moved.
                SVGOMTextPathElement textPath =
                    TextHandler.getTextPath(textElement);
                if (textPath!=null) {
                    TextHandler.updateTextPathNode(textPath);
                    TextHandler.rotateTextIfRequired(connector,textPath);
                }
            }
        }
    }

    /**
     * Check to see if the element being moved as any anchored text
     * 
     * @param canvas beermat canvas
     * @param element being moved
     * @param transform to work with
     * @since 2.4
     */
    public static void findAndUpdateText(BeermatAWTCanvas canvas,
    Element element,AffineTransform transform) {

        List<Element> elements = TextHandler.getAnchoredElements(element);

        for (Element textElement:elements) {

            SVGOMTextPathElement pathElement =
                TextHandler.getTextPath(textElement);

            if (pathElement!=null) {
                // Force the element to redraw
                TextHandler.updateTextPathNode(pathElement);

                // Don't move textpath elements
                continue;
            }

            try {
                NSEWCHotSpots hotSpots =
                    new NSEWCHotSpots(canvas,canvas.getGraphicsNodeFor(element));

                HotSpot hotSpot =
                    hotSpots.getHotSpotForLocation(SVGUtils
                        .getDocfactoAttribute(textElement,
                            TextHandler.HOTSPOT_ATTRIBUTE));
                if (hotSpot==null) {
                    continue;
                }

                // Get text element size
                GraphicsNode textGraphicsNode =
                    canvas.getGraphicsNodeFor(textElement);

                Point2D location =
                    SVGUtils.getTextLocationWithHotSpot(textElement,hotSpot,
                        (int)textGraphicsNode.getBounds()
                            .getHeight(),transform);

                if (textElement instanceof SVGOMTextElement) {
                    SVGUtils.setElementLocation(textElement,location);
                }
                else {
                    // Its a group
                    SVGUtils.setTransformTranslate(textElement,location);
                }
            }
            catch (DocfactoException ex) {
                // Probably a broken document at this point
            }
        }
    }

}
