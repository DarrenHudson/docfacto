package com.docfacto.beermat.highlight;

import java.awt.BasicStroke;
import java.awt.Shape;
import java.awt.Stroke;

/**
 * This Stroke implementation applies a BasicStroke to a shape twice. If you
 * draw with this Stroke, then instead of outlining the shape, you're outlining
 * the outline of the shape.
 * 
 * @author dhudson - created 13 Sep 2013
 * @since 2.4
 */
public class OutlineStroke implements Stroke {

    private final BasicStroke theStroke1;
    private final BasicStroke theStroke2; // the two strokes to use

    /**
     * Constructor.
     * 
     * @param width1 of the first stroke
     * @param width2 of the second stroke
     * @since 2.4
     */
    public OutlineStroke(float width1,float width2) {
        theStroke1 = new BasicStroke(width1); // Constructor arguments specify
        theStroke2 = new BasicStroke(width2); // the line widths for the strokes
    }

    /**
     * @see java.awt.Stroke#createStrokedShape(java.awt.Shape)
     */
    public Shape createStrokedShape(Shape s) {
        // Use the first stroke to create an outline of the shape
        Shape outline = theStroke1.createStrokedShape(s);
        // Use the second stroke to create an outline of that outline.
        // It is this outline of the outline that will be filled in
        return theStroke2.createStrokedShape(outline);
    }
}
