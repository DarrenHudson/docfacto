package com.docfacto.beermat.highlight;

import java.awt.BasicStroke;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;

/**
 * Create a random (hand drawn shape)
 * 
 * @author dhudson - created 13 Sep 2013
 * @since 2.4
 */
public class SloppyStroke implements Stroke {
    private final BasicStroke theStroke;
    private float theloppiness;

    /**
     * Constructor.
     * 
     * @param width of the stroke
     * @param sloppiness how much randomness
     * @since 2.4
     */
    public SloppyStroke(float width,float sloppiness) {
        theStroke = new BasicStroke(width); // Used to stroke modified shape
        theloppiness = sloppiness; // How sloppy should we be?
    }

    /**
     * @see java.awt.Stroke#createStrokedShape(java.awt.Shape)
     */
    public Shape createStrokedShape(Shape shape) {
        // Start with an empty shape
        GeneralPath newshape = new GeneralPath();

        // Iterate through the specified shape, perturb its coordinates, and
        // use them to build up the new shape.
        float[] coords = new float[6];
        for (PathIterator i = shape.getPathIterator(null);!i.isDone();i.next()) {
            int type = i.currentSegment(coords);
            switch (type) {
            case PathIterator.SEG_MOVETO:
                perturb(coords,2);
                newshape.moveTo(coords[0],coords[1]);
                break;
            case PathIterator.SEG_LINETO:
                perturb(coords,2);
                newshape.lineTo(coords[0],coords[1]);
                break;
            case PathIterator.SEG_QUADTO:
                perturb(coords,4);
                newshape.quadTo(coords[0],coords[1],coords[2],coords[3]);
                break;
            case PathIterator.SEG_CUBICTO:
                perturb(coords,6);
                newshape.curveTo(coords[0],coords[1],coords[2],coords[3],
                    coords[4],coords[5]);
                break;
            case PathIterator.SEG_CLOSE:
                newshape.closePath();
                break;
            }
        }

        // Finally, stroke the perturbed shape and return the result
        return theStroke.createStrokedShape(newshape);
    }

    // Randomly modify the specified number of coordinates, by an amount
    // specified by the sloppiness field.
    private void perturb(float[] coords,int numCoords) {
        for (int i = 0;i<numCoords;i++) {
            coords[i] += (float)((Math.random()*2-1.0)*theloppiness);
        }
    }
}
