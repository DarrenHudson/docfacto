package com.docfacto.beermat.highlight;

import java.awt.BasicStroke;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

/**
 * Creates a frame inside a shape
 * 
 * @author dhudson - created 13 Sep 2013
 * @since 2.4
 */
public class InsideFrameStroke implements Stroke {

    private BasicStroke theStroke;

    /**
     * Constructor.
     * 
     * @since 2.4
     */
    public InsideFrameStroke() {
        theStroke = new BasicStroke(1);
    }

    /**
     * @see java.awt.Stroke#createStrokedShape(java.awt.Shape)
     */
    @Override
    public Shape createStrokedShape(Shape shape) {
        if (shape==null) {
            return null;
        }

        Rectangle2D oldBounds = shape.getBounds2D();
        float witdh = theStroke.getLineWidth();

        // calcute a transformation for inside drawing
        // based on the stroke width
        AffineTransform at = new AffineTransform();
        if (oldBounds.getWidth()>0) {
            at.scale(
                (oldBounds.getWidth()-witdh)/
                    oldBounds.getWidth(),1.1);
        }

        if (oldBounds.getHeight()>0) {
            at.scale(1.1,
                (oldBounds.getHeight()-witdh)
                    /oldBounds.getHeight());
        }

        // recalculate shape and its oldBounds
        shape = at.createTransformedShape(shape);
        Rectangle2D newBounds = shape.getBounds2D();

        // move the shape to the old origin + the line width offset
        AffineTransform moveBackTransform =
            AffineTransform.getTranslateInstance(
                oldBounds.getX()-newBounds.getX()+witdh/2,
                oldBounds.getY()-newBounds.getY()+witdh/2);

        shape = moveBackTransform.createTransformedShape(shape);

        // outline the shape using the simple basic stroke
        return theStroke.createStrokedShape(shape);
    }
}
