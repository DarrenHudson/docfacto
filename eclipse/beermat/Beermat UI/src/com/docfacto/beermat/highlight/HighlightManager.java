package com.docfacto.beermat.highlight;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.events.ElementSelectChangeEvent;
import com.docfacto.beermat.events.GroupElementEvent;
import com.docfacto.beermat.events.UngroupElementEvent;
import com.docfacto.beermat.hotspot.HotSpot;
import com.docfacto.beermat.listeners.BeermatEventListener;
import com.docfacto.beermat.overlay.HighlightOverlay;
import com.docfacto.beermat.svg.ConnectorHandler;
import com.docfacto.beermat.svg.TextHandler;
import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.beermat.utils.BeermatUtils;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.DocfactoException;
import com.docfacto.common.StringUtils;

/**
 * The Highlight manager is used to control a number of selected items on a SVG
 * canvas
 * 
 * @author dhudson - created 1 Jul 2013
 * @since 2.4
 */
public class HighlightManager implements BeermatEventListener {

    private List<HighlightElement> theSelectedElements;
    private HighlightElement theHighlighted;

    private final BeermatController theController;

    private final BeermatAWTCanvas theBeermatCanvas;

    private final HighlightOverlay theOverlay;

    private boolean isDragging = false;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param canvas the beermat canvas
     * @since 2.4
     */
    public HighlightManager(BeermatController controller,BeermatAWTCanvas canvas) {
        theController = controller;
        theBeermatCanvas = canvas;

        // Create the new overlay
        theOverlay = new HighlightOverlay(this,canvas);

        // Add it to the canvas
        canvas.addOverlay(theOverlay);

        theSelectedElements = new ArrayList<HighlightElement>(5);

        controller.addListener(this);
    }

    /**
     * Return the highlighted element or null
     * 
     * @return the highlighted element or null
     * @since 2.4
     */
    public HighlightElement getHighlightedElement() {
        return theHighlighted;
    }

    /**
     * Check the mouse position and see if the mouse is on a hot spot
     * 
     * @param x mouse x
     * @param y mouse y
     * @return A hot spot or null
     * @since 2.4
     */
    public HotSpot checkForHotSpot(int x,int y) {

        for (HighlightElement element:theSelectedElements) {
            HotSpot hotSpot = element.isInHotSpot(x,y);
            if (hotSpot!=null) {
                return hotSpot;
            }
        }

        return null;
    }

    /**
     * Set the element highlighted, but not added to the list
     * 
     * {@docfacto.note A hightlight will only be added if the element is not
     * already selected }
     * 
     * @param element to highlight
     * @since 2.4
     */
    public void hightlight(Element element) {

        if (contains(element)) {
            // We are already selected
            return;
        }

        try {
            theHighlighted =
                new HighlightElement(theBeermatCanvas,element,false);
            // Just draw the highlight..
            theOverlay.paint(theBeermatCanvas.getOverlayGraphics(),false);
        }
        catch (DocfactoException ignore) {
        }
    }

    /**
     * This gets triggered on a mouse out event
     * 
     * @param element to unhighlight
     * @since 2.4
     */
    public void unhighlight(Element element) {
        if (contains(element)) {
            return;
        }

        if (theHighlighted!=null) {
            // Redraw the canvas now the box has gone
            theBeermatCanvas.repaint(BeermatUtils.outset(
                theHighlighted.getHighlightBox(),4));
        }
        theHighlighted = null;
    }

    /**
     * Send a repaint signal to the Beermat Canvas, this in turn will fire the
     * Overlay to redraw with out changes
     * 
     * @since 2.4
     */
    private void repaint() {
        theBeermatCanvas.repaint();
    }

    /**
     * This gets called when the user starts dragging.
     * 
     * The user may have a highlighted element and what to drag that as well as
     * the selected elements.
     * 
     * The highlighted paths are removed from the SVG and added again when
     * endDragging is called.
     * 
     * @param element current element
     * @since 2.4
     */
    public void startDragging(Element element) {
        // Are they currently over an element
        if (element!=null) {

            // Is the element highlighted?
            if (theHighlighted==null) {
                if (!contains(element)) {
                    try {
                        // We need to add the element as they want to drag it
                        theHighlighted =
                            new HighlightElement(theBeermatCanvas,element,true);
                        theSelectedElements.add(theHighlighted);
                    }
                    catch (DocfactoException ignore) {

                    }
                }
            }
            else if (!contains(theHighlighted.getHighlightedElement())) {
                theSelectedElements.add(theHighlighted);
            }
        }

        if (theHighlighted!=null) {
            theHighlighted.setVisible(false);
        }

        for (HighlightElement el:theSelectedElements) {
            el.setVisible(false);
        }

        theOverlay.startDragging();

        repaint();

        isDragging = true;
    }

    /**
     * The user has finished dragging so re-add the selected elements
     * 
     * @since 2.4
     */
    public void endDragging() {

        isDragging = false;

        theOverlay.endDragging();

        if (theHighlighted!=null) {
            theHighlighted.setVisible(true);
            theSelectedElements.remove(theHighlighted);
        }

        for (HighlightElement el:theSelectedElements) {
            el.setVisible(true);
        }

        repaint();
    }

    /**
     * Check to see if the user is currently dragging
     * 
     * @return true if currently dragging
     * @since 2.4
     */
    public boolean isDragging() {
        return isDragging;
    }

    /**
     * User has clicked an element, so lets select it if not already selected.
     * 
     * If already selected then unselect the element
     * 
     * @param element to select
     * @param multiple true if multiple select is allowed
     * @since 2.4
     */
    public void select(Element element,boolean multiple) {

        // User has pressed a mouse on the selected item
        if (contains(element)) {
            // Already there.. so they want to remove it
            HighlightElement highlight = getHighlightedElementFor(element);

            theHighlighted = highlight;
            theHighlighted.setSelected(false);

            theSelectedElements.remove(highlight);
        }
        else {
            if (!multiple) {
                // Only a single selection allowed
                theSelectedElements.clear();
            }
            try {
                theSelectedElements.add(new HighlightElement(
                    theBeermatCanvas,element,true));
            }
            catch (DocfactoException ignore) {
            }
        }

        if (theHighlighted!=null) {
            theController.processEvent(new ElementSelectChangeEvent(this,
                theHighlighted.getHighlightedElement()));
        }

        theHighlighted = null;

        repaint();
    }

    /**
     * Remove the selected elements from the SVG and the canvas
     * 
     * {@docfacto.note needs to be run via an updater}
     * 
     * @param deleteAnchoredElements true to delete the anchored element as
     * well.
     * @since 2.4
     */
    public void deleteSelectedElements(boolean deleteAnchoredElements) {
        // Lets not delete the highlighted one

        Set<Element> setToDelete = new HashSet<Element>();
        Set<Element> connectors = null;

        for (HighlightElement hEl:theSelectedElements) {

            if (deleteAnchoredElements) {
                String id = hEl.getElement().getAttribute("id");

                if (!StringUtils.nullOrEmpty(id)) {
                    if (connectors==null) {
                        connectors =
                            BeermatUtils.getConnectors(hEl.getElement()
                                .getOwnerDocument());
                    }

                    for (Element connector:connectors) {
                        if (id.equals(SVGUtils.getDocfactoAttribute(
                            connector,
                            ConnectorHandler.CONNECTOR_START_ATTRIBUTE))||
                            id.equals(SVGUtils.getDocfactoAttribute(
                                connector,
                                ConnectorHandler.CONNECTOR_END_ATTRIBUTE))) {
                            // Add the connector to the list
                            setToDelete.add(connector);
                            // Remove any text as well
                            setToDelete.addAll(TextHandler
                                .getAnchoredElements(connector));
                        }
                    }
                }
            }

            setToDelete.add(hEl.getElement());
        }

        for (Element element:setToDelete) {
            // Remove the element from the SVG
            element.getParentNode().removeChild(element);
        }

        removeHighlights();
    }

    /**
     * Remove the highlights from the SVG and clear the selected list
     * 
     * @since 2.4
     */
    public void removeHighlights() {
        theSelectedElements.clear();
        theHighlighted = null;
        repaint();
    }

    /**
     * Clear the highlighted element
     * 
     * @since 2.4
     */
    public void clearHighlighted() {
        if (theHighlighted!=null) {
            theHighlighted = null;
            repaint();
        }
    }

    /**
     * Check to see if there are selected elements, not including the
     * highlighted element
     * 
     * @return true if there are selected elements
     * @since 2.4
     */
    public boolean hasSelectedElements() {
        return !theSelectedElements.isEmpty();
    }

    /**
     * Return a list of selected elements
     * 
     * @return a list of elements that are selected
     * @since 2.4
     */
    public List<Element> getHighlightedElements() {
        List<Element> highlighted =
            new ArrayList<Element>(theSelectedElements.size());

        for (HighlightElement hElement:theSelectedElements) {
            highlighted.add(hElement.getHighlightedElement());
        }

        return highlighted;
    }

    /**
     * Return a list of the current highlights
     * 
     * @return the list of the highlights
     * @since 2.4
     */
    public List<HighlightElement> getHighlights() {
        return theSelectedElements;
    }

    /**
     * Check to see if the element is already in the selected list
     * 
     * @param element to check
     * @return true if the element is already in the selected list
     * @since 2.4
     */
    public boolean contains(Element element) {
        if (element==null) {
            return false;
        }
        for (HighlightElement hElement:theSelectedElements) {
            if (hElement.getHighlightedElement().equals(element)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Return the highlighted element for the given element if exists in the
     * list or null if its not
     * 
     * @param element
     * @return the {@code highlightElement} for the given element
     * @since 2.4
     */
    private HighlightElement getHighlightedElementFor(Element element) {
        for (HighlightElement hEl:theSelectedElements) {
            if (hEl.getHighlightedElement().equals(element)) {
                return hEl;
            }
        }

        return null;
    }

    /**
     * Add a list of elements to the selected items
     * 
     * @param elements to add
     * @since 2.4
     */
    public void add(List<Element> elements) {
        for (Element e:elements) {
            try {
                theSelectedElements.add(new HighlightElement(theBeermatCanvas,
                    e,
                    true));
            }
            catch (DocfactoException ignore) {
            }
        }

        repaint();
    }

    /**
     * @see com.docfacto.beermat.listeners.BeermatEventListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    @Override
    public void handleEvent(BeermatEvent event) {
        if (event.isGroupElementEvent()) {
            GroupElementEvent groupElementEvent = (GroupElementEvent)event;
            removeHighlights();
            try {
                theSelectedElements.add(new HighlightElement(theBeermatCanvas,
                    groupElementEvent.getGroup(),
                    true));
            }
            catch (DocfactoException ignore) {
                // Can't happen
            }
        }
        else if (event.isUngroupElementEvent()) {
            UngroupElementEvent ungroup = (UngroupElementEvent)event;
            removeHighlights();
            add(ungroup.getGroup());
        }
        else if (event.isHighlightManagerEvent()) {
            resizeSelectedTextElements();
        }
    }

    /**
     * Recalculate the transform bounds of the text as it may have changed size.
     * 
     * @since 2.5
     */
    private void resizeSelectedTextElements() {
        for (HighlightElement highlightElement:theSelectedElements) {
            if (SVGUtils.checkForTextEdit(highlightElement.getElement())) {
                highlightElement.calculateTransformedBounds();
                repaint();
            }
        }
    }
}
