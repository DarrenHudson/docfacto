package com.docfacto.beermat.highlight;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.apache.batik.dom.svg.SVGOMLineElement;
import org.w3c.dom.Element;

import com.docfacto.beermat.hotspot.CircleHotSpot;
import com.docfacto.beermat.hotspot.HotSpot;
import com.docfacto.beermat.hotspot.HotSpotCollection;
import com.docfacto.beermat.hotspot.SquareHotSpot;
import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.beermat.svg.ConnectorHandler;
import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.beermat.utils.BeermatUtils;
import com.docfacto.common.DocfactoException;

/**
 * This class represents a highlight of another element.
 * 
 * 
 * @author dhudson - created 1 Jul 2013
 * @since 2.4
 * @docfacto.link key="elementHighlightStyle"
 * uri="${doc-beermat}/selecting/c_selecting.dita" link-to="doc"
 */
public class HighlightElement extends HotSpotCollection {

    private static final int MARGIN_D = 5;

    private static final BasicStroke OUTLINE_STROKE = new BasicStroke(
        MARGIN_D*2);

    private boolean isSelected;

    private Shape theHighlight;

    private boolean isVisible = true;

    /**
     * What type of highlight is it
     * 
     * @author dhudson - created 17 Dec 2013
     * @since 2.5
     */
    private enum Type {
        CONNECTOR,
        LINE,
        BOUNDED
    }

    private Type theType;

    /**
     * Constructor.
     * 
     * @param canvas beermat canvas
     * @param elementToHighight element to highlight
     * @param selected true if a selected item rather than just a hover
     * @throws DocfactoException if the element isn't visible or have a graphics
     * node
     * @since 2.4
     */
    public HighlightElement(BeermatAWTCanvas canvas,
    Element elementToHighight,boolean selected) throws DocfactoException {
        super(canvas,canvas.getGraphicsNodeFor(elementToHighight),
            elementToHighight);

        isSelected = selected;
    }

    /**
     * Return the highlight box
     * 
     * @return the Rectangle shape of the highlight
     * @since 2.4
     */
    public Rectangle getHighlightBox() {
        return theHighlight.getBounds();
    }

    /**
     * Render the highlights and hot spots on the overlay panel
     * 
     * @param g2d 2D graphics
     * @since 2.4
     */
    public void renderHighlight(Graphics2D g2d) {
        if (!isVisible) {
            // Nothing to do
            return;
        }

        // It might have moved, or is being dragged
        createHotSpots();

        g2d.setStroke(BeermatUtils.HIGHLIGHT_STROKE);

        if (isSelected) {
            g2d.setColor(HotSpot.HOTSPOT_HIGHLIGHT_COLOUR);

            // Draw the hot spots
            for (int i = 0;i<getNumberOfHotSpots();i++) {
                g2d.fill(theHotSpots[i].getShape());
            }
        }
        else {
            g2d.setColor(HotSpot.HOTSPOT_COLOUR);
        }

        g2d.draw(theHighlight);
    }

    /**
     * Set the visible flag for the highlight
     * 
     * @param visible true if visible
     * @since 2.4
     */
    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    /**
     * Toggle the style of the element from hover to selected
     * 
     * @param selected true for selected
     * @since 2.4
     */
    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    /**
     * Check to see if the element is selected
     * 
     * @return true if the element is showing selected
     * @since 2.4
     */
    public boolean isSelected() {
        return isSelected;
    }

    /**
     * Return the SVG Element that is being highlighted
     * 
     * @return element that is being highlighted
     * @since 2.4
     */
    public Element getHighlightedElement() {
        return getElement();
    }

    /**
     * @see com.docfacto.beermat.hotspot.HotSpotCollection#getNumberOfHotSpots()
     * 
     */
    @Override
    public int getNumberOfHotSpots() {
        if (theType==null) {
            getConnectorType();
        }

        switch (theType) {
        case CONNECTOR:
            return 0;

        case LINE:
            return 2;

        default:
            return 8;
        }

    }

    /**
     * Only do this once as its expensive
     * 
     * @since 2.5
     */
    private void getConnectorType() {

        if (ConnectorHandler.isConnector(getElement())) {
            theType = Type.CONNECTOR;
        }
        else if (getElement() instanceof SVGOMLineElement) {
            theType = Type.LINE;
        }
        else {
            theType = Type.BOUNDED;
        }
    }

    /**
     * @see com.docfacto.beermat.hotspot.HotSpotCollection#createHotSpots()
     */
    @Override
    public void createHotSpots() {

        // Create the highlight..
        if (theType.equals(Type.CONNECTOR)||theType.equals(Type.LINE)) {
            theHighlight =
                OUTLINE_STROKE.createStrokedShape(calculateTransformedShape());
            // ConnectorHandler connectorHandler = new ConnectorHandler();
            // // Get the x1,y1 - x2, y2 of the connector
            // connectorHandler.parser(getElement());
        }
        else {
            calculateTransformedBounds();
            theHighlight = BeermatUtils.outset(getTransformedBounds(),MARGIN_D);
        }

        if (theType.equals(Type.LINE)) {
            SVGOMLineElement line = (SVGOMLineElement)getElement();

            Point2D location;
            try {
                location = getTransformedPoint(line.getX1().getBaseVal()
                    .getValue(),line.getY1().getBaseVal().getValue());

                int x1 = (int)location.getX();
                int y1 = (int)location.getY();

                location =
                    getTransformedPoint(line.getX2().getBaseVal()
                        .getValue(),line.getY2().getBaseVal().getValue());

                int x2 = (int)location.getX();
                int y2 = (int)location.getY();

                // There has to be a better way of doing this
                if (x1<x2) {
                    // Right to left..
                    x1 -= 4;
                }
                else {
                    x2 -= 4;
                }

                if (y1<y2) {
                    y2 += 4;
                    x1 -= 4;
                    y1 -= 4;
                }
                else if (y1>y2) {
                    y2 -= 4;
                    x1 -= 4;
                }
                else {
                    y1 -= 4;
                    y2 -= 4;
                    x1 -= 4;
                }

                theHotSpots[0] =
                    new CircleHotSpot(HotSpot.WEST,x1,y1,8);

                theHotSpots[1] =
                    new CircleHotSpot(HotSpot.EAST,x2,y2,8);
            }
            catch (NoninvertibleTransformException ex) {
                BeermatUIPlugin.logException(
                    "Unable to create transform : Highlight Element",ex);
            }
        }

        if (theType.equals(Type.BOUNDED)) {

            Rectangle2D highlightBounds = theHighlight.getBounds2D();

            int hotSpotSize = 8;
            if (highlightBounds.getWidth()<100||highlightBounds.getHeight()<100) {
                hotSpotSize = 4;
            }

            // Create hot spots
            for (int i = 0;i<getNumberOfHotSpots();i++) {
                theHotSpots[i] =
                    new SquareHotSpot(i,highlightBounds,hotSpotSize);
            }
        }
    }
}
