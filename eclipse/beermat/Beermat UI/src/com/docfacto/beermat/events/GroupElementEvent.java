package com.docfacto.beermat.events;

import org.w3c.dom.Element;

/**
 * This event is fired when elements are grouped togther
 *
 * @author dhudson - created 31 Jul 2013
 * @since 2.4
 */
public class GroupElementEvent extends BeermatEvent {

    /**
     * {@value}
     */
    private static final long serialVersionUID = 1L;

    private final Element theGroup;
    
    /**
     * Constructor.
     * @param source of the event
     * @param group newly created group
     * @since 2.4
     */
    public GroupElementEvent(Object source,Element group) {
        super(source,GROUP_ELEMENT_ACTION);
        theGroup = group;
    }
    
    /**
     * Return the newly created group
     * 
     * @return the newly created group
     * @since 2.4
     */
    public Element getGroup() {
        return theGroup;
    }
}
