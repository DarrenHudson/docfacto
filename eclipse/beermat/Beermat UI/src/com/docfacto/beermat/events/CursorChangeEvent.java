package com.docfacto.beermat.events;

/**
 * This event is fired when a change of Cursor is required.
 *
 * @author dhudson - created 26 Jul 2013
 * @since 2.4
 */
public class CursorChangeEvent extends BeermatEvent {

    /**
     * {@value}
     */
    private static final long serialVersionUID = 1L;
    
    private final int theCursorType;
    
    /**
     * Constructor.
     * @param source
     * @param cursorType
     */
    public CursorChangeEvent(Object source,int cursorType) {
        super(source, CURSOR_CHANGE_ACTION);
        theCursorType = cursorType;
    }
    
    /**
     * Get the required cursor type
     * 
     * @return the required cursor type
     * @since 2.4
     */
    public int getCursorType() {
        return theCursorType;
    }
}
