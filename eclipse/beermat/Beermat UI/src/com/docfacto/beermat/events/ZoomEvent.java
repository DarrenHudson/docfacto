package com.docfacto.beermat.events;

import javax.swing.Action;
import javax.swing.ActionMap;

import org.apache.batik.swing.JSVGCanvas;

/**
 * This event is fired upon change of zoom
 * 
 * @author dhudson - created 21 Jun 2013
 * @since 2.4
 */
public class ZoomEvent extends BeermatEvent implements CanvasAction {

    private static final long serialVersionUID = 1L;

    /**
     * Zoom in
     */
    public static final int IN = 0;
    /**
     * Zoom out
     */
    public static final int OUT = 1;
    /**
     * Reset zoom
     */
    public static final int RESET = 2;

    private final int theZoomAction;

    /**
     * Constructor.
     * 
     * @param source of the event
     * @param action action to take
     * @since 2.4
     */
    public ZoomEvent(Object source,int action) {
        super(source,ZOOM_ACTION);
        theZoomAction = action;
    }

    /**
     * Return the zoom action
     * 
     * @return the zoom action
     * @since 2.4
     */
    public int getZoomAction() {
        return theZoomAction;
    }

    /**
     * @see com.docfacto.beermat.events.CanvasAction#performAction(org.apache.batik.util.gui.resource.ActionMap)
     */
    @Override
    public void performAction(ActionMap actionMap) {
        Action action = null;

        switch (getZoomAction()) {
        case ZoomEvent.IN:
            action =
                actionMap.get(JSVGCanvas.ZOOM_IN_ACTION);
            break;

        case ZoomEvent.OUT:
            action =
                actionMap.get(JSVGCanvas.ZOOM_OUT_ACTION);
            break;

        case ZoomEvent.RESET:
            action =
                actionMap.get(
                    JSVGCanvas.RESET_TRANSFORM_ACTION);
            break;
        }

        if (action!=null) {
            action.actionPerformed(null);
        }
    }
}
