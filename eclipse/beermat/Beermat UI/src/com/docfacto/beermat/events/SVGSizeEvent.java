package com.docfacto.beermat.events;

import com.docfacto.beermat.svg.SVGSize;
import com.docfacto.beermat.ui.BeermatAWTCanvas;

/**
 * This Event is fired when a document size changes, or if a document has been
 * created or loaded
 * 
 * @author dhudson - created 21 Jun 2013
 * @since 2.4
 */
public class SVGSizeEvent extends BeermatEvent {

    private static final long serialVersionUID = 1L;

    private final SVGSize theSize;
    
    /**
     * Constructor.
     * @param source event source
     * @param canvas
     * @since 2.4
     */
    public SVGSizeEvent(Object source,BeermatAWTCanvas canvas) {
        super(source,SVG_SIZE_ACTION);
        theSize = new SVGSize(canvas.getSVGDocument());
    }
    
    /**
     * Return the size attributes
     * 
     * @return the size
     * @since 2.4
     */
    public SVGSize getSize() {
        return theSize;
    }
}
