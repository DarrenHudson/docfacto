package com.docfacto.beermat.events;

import java.awt.event.MouseEvent;

import org.apache.batik.gvt.GraphicsNode;
import org.w3c.dom.Element;

/**
 * Handle SVG Element mouse actions
 * 
 * @author dhudson - created 27 Jun 2013
 * @since 2.4
 */
public class SVGElementMouseEvent extends BeermatEvent {

    private static final long serialVersionUID = 1L;

    private final MouseEvent theMouseEvent;
    private final EVENT_TYPE theEventType;

    private Element theSVGElement;
    private GraphicsNode theGraphicsNode;

    /**
     * Mouse Event Types
     * 
     * @since 2.4
     */
    public enum EVENT_TYPE {
        /**
         * Mouse out
         */
        MOUSE_OUT,
        /**
         * Mouse over
         */
        MOUSE_OVER
    }

    /**
     * Constructor.
     * 
     * @param source event source
     * @param mouseEvent AWT mouse event
     * @param type Out or Over
     * @since 2.4
     */
    public SVGElementMouseEvent(Object source,MouseEvent mouseEvent,
    EVENT_TYPE type) {
        super(source,SVG_MOUSE_EVENT_ACTION);
        theMouseEvent = mouseEvent;
        theEventType = type;
    }

    /**
     * Return the Dom element that reported the event
     * 
     * @return the Element
     * @since 2.4
     */
    public Element getElement() {
        return theSVGElement;
    }

    /**
     * Sets the SVG Element
     * 
     * @param element SVG Element
     * @since 2.4
     */
    public void setElement(Element element) {
        theSVGElement = element;
    }

    /**
     * Gets the graphics node
     * 
     * @return the graphics node
     * @since 2.4
     */
    public GraphicsNode getGraphicsNode() {
        return theGraphicsNode;
    }

    /**
     * Set the graphics node
     * 
     * @param node graphics node
     * @since 2.4
     */
    public void setGraphicsNode(GraphicsNode node) {
        theGraphicsNode = node;
    }

    /**
     * Check to see if mouse out event
     * 
     * @return true if the event is mouse out
     * @since 2.4
     */
    public boolean isMouseOutEvent() {
        return theEventType.equals(EVENT_TYPE.MOUSE_OUT);
    }

    /**
     * Check to see if mouse over event
     * 
     * @return true if mouse over event
     * @since 2.4
     */
    public boolean isMouseOverEvent() {
        return theEventType.equals(EVENT_TYPE.MOUSE_OVER);
    }

    /**
     * Gets the mouse event
     * 
     * @return the mouse event
     * @since 2.4
     */
    public MouseEvent getMouseEvent() {
        return theMouseEvent;
    }
}
