package com.docfacto.beermat.events;

import java.util.List;

import org.w3c.dom.Element;

/**
 * An event to notify for ungrouping elements.
 *
 * @author kporter - created Aug 1, 2013
 * @since 2.4.3
 */
public class UngroupElementEvent extends BeermatEvent {

    /**
     * {@value}
     */
    private static final long serialVersionUID = 1L;

    private final List<Element> theGroup;
    
    /**
     * Constructor.
     * @param source of the event
     * @param group The ungrouped elements to highlight
     * @since 2.4.3
     */
    public UngroupElementEvent(Object source,List<Element> group) {
        super(source,UNGROUP_ELEMENT_ACTION);
        theGroup = group;
    }
    
    /**
     * Return the list of un-grouped elements.
     * 
     * @return the list of elements that were ungrouped
     * @since 2.4
     */
    public List<Element> getGroup() {
        return theGroup;
    }
}
