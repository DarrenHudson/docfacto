package com.docfacto.beermat.events;

import javax.swing.ActionMap;


/**
 * Interface to describe Batik Canvas Actions
 *
 * @author dhudson - created 11 Aug 2013
 * @since 2.4
 */
public interface CanvasAction {

    /**
     * Depending on the event, process the correct action depending on the {@code ActionMap}
     * 
     * @param actionMap that holds the actions
     * @since 2.4
     */
    public void performAction(ActionMap actionMap);
}
