package com.docfacto.beermat.events;

/**
 * An event for when zoom has changed.
 * @author kporter - created Aug 20, 2013
 * @since 2.4.3
 */
public class ZoomChangeEvent extends BeermatEvent {

    /**
     * TODO - Field Description
     */
    private static final long serialVersionUID = 1L;
    private double theScale;

    /**
     * Constructs a zoomchange event
     * @param source The source of the event
     * @param scale The new zoom scale
     */
    public ZoomChangeEvent(Object source, double scale) {
        super(source,ZOOM_CHANGE_EVENT_ACTION);
        theScale = scale;
    }
    
    /**
     * Returns theScale.
     *
     * @return the theScale
     */
    public double getScale() {
        return theScale;
    }

}
