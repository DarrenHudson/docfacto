package com.docfacto.beermat.events;

/**
 * AWT Action to prompt for save.
 * 
 * @author dhudson - created 23 Jul 2013
 * @since 2.4
 */
public class SaveEvent extends BeermatEvent {

    /**
     * Constant {@value}
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor.
     * 
     * @param source
     */
    public SaveEvent(Object source) {
        super(source,SAVE_ACTION);
    }
}
