package com.docfacto.beermat.events;

import org.w3c.dom.Element;


/**
 * An event for when element selection has changed.
 * @author kporter - created Aug 29, 2013
 * @since 2.4.4
 */
public class ElementSelectChangeEvent extends BeermatEvent {
    
    private static final long serialVersionUID = 1L;
    private Element theElement;
    /**
     * Constructor.
     * @param source
     * @param action
     */
    public ElementSelectChangeEvent(Object source, Element element) {
        super(source,ELEMENT_SELECT_EVENT_ACTION);
        theElement = element;
    }
    public Element getElement() {
        return theElement;
    }

}
