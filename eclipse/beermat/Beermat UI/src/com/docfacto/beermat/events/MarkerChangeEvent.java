package com.docfacto.beermat.events;

/**
 * An event to notify components of a marker change.
 * @author kporter - created Sep 18, 2013
 * @since 2.4.4
 */
public class MarkerChangeEvent extends BeermatEvent {
    private static final long serialVersionUID = 1L;
    /**
     * Represents the marker change position which has changed
     * @since 2.4.4
     */
    public enum Position{
        /**
         * the marker-start
         */
        START,
        /**
         * the marker-mid
         */
        END,
        
        /**
         * Both markers
         */
        BOTH
    }
    
    private Position thePosition;
    private String theID;
    
    /**
     * Constructor.
     * @param source The source of the event
     * @param position The position of the marker to change to.
     * @param id The id of the marker to change to
     * @since 2.4.5
     */
    public MarkerChangeEvent(Object source,Position position, String id) {
        super(source, MARKER_CHANGE_EVENT_ACTION);
        thePosition = position;
        theID = id;
    }
    
    /**
     * Returns the position of the marker to change
     * @return the Position
     * @since 2.4.5
     */
    public Position getPosition(){
        return thePosition;
    }
    
    /**
     * Gets the marker id
     * @return The markers id
     * @since 2.4.5
     */
    public String getID(){
        return theID;    
    }

}
