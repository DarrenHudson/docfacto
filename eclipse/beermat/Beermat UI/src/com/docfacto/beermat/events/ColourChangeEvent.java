package com.docfacto.beermat.events;

import org.eclipse.swt.graphics.Color;

/**
 * The event of a colour change
 * 
 * @author kporter - created Aug 16, 2013
 * @since 2.4.3
 */
public class ColourChangeEvent extends BeermatEvent {
    /**
     * TODO - Field Description
     */
    private static final long serialVersionUID = 1L;

    private Color theColor;

    /**
     * Constructor.
     * 
     * @param source
     * @param action
     */
    public ColourChangeEvent(Object source,Color newColour) {
        super(source,COLOUR_CHANGE_ACTION);
        theColor = newColour;
    }

    /**
     * Gest the colour
     * 
     * @return The colour
     * @since 2.4.3
     */
    public Color getColor() {
        return theColor;
    }

}
