package com.docfacto.beermat.events;

/**
 * This event is fired when a widget on the control bar is changed which could
 * impact selected items
 * 
 * @author dhudson - created 25 Jul 2013
 * @since 2.4
 */
public class AttributeChangeEvent extends BeermatEvent {

    /**
     * {@value}
     */
    private static final long serialVersionUID = 1L;

    private final boolean isFontAttribute;

    /**
     * Constructor.
     * 
     * @param source
     * @param fontAttribute true if the change effects text
     */
    public AttributeChangeEvent(Object source,boolean fontAttribute) {
        super(source,ATTRIBUTE_CHANGE_ACTION);
        isFontAttribute = fontAttribute;
    }

    /**
     * Constructor.
     * 
     * Font attribute will set to false
     * 
     * @param source
     */
    public AttributeChangeEvent(Object source) {
        this(source,false);
    }

    /**
     * Check to see if this is for fonts
     * 
     * @return true if this event was fired in respect to Font Dialog
     * @since 2.4
     */
    public boolean isFontAttribute() {
        return isFontAttribute;
    }
}
