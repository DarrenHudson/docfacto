package com.docfacto.beermat.events;

/**
 * Event is fired when the UI is complete
 * 
 * @author dhudson - created 20 Jun 2013
 * @since 2.4
 */
public class UICompleteEvent extends BeermatEvent {

    private static final long serialVersionUID = 1L;

    /**
     * Constructor.
     * @param source
     */
    public UICompleteEvent(Object source) {
        super(source,UI_COMPLETE_ACTION);
    }

}
