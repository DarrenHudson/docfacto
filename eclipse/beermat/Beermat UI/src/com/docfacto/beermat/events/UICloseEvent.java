package com.docfacto.beermat.events;

/**
 * Fired when the page is closed
 * 
 * @author dhudson - created 5 Jul 2013
 * @since 2.4
 */
public class UICloseEvent extends BeermatEvent {

    private static final long serialVersionUID = 1L;

    /**
     * Constructor.
     * 
     * @param source
     * @since 2.4
     */
    public UICloseEvent(Object source) {
        super(source,UI_CLOSE_ACTION);
    }
}
