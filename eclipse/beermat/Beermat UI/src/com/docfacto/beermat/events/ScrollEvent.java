package com.docfacto.beermat.events;

import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.ActionMap;

import org.apache.batik.swing.JSVGCanvas;

/**
 * Pan / Scroll the current document
 * 
 * @author dhudson - created 11 Aug 2013
 * @since 2.4
 */
public class ScrollEvent extends BeermatEvent implements CanvasAction {

    /**
     * {@value}
     */
    private static final long serialVersionUID = 1L;

    private final KeyEvent theKeyEvent;

    /**
     * Constructor.
     * 
     * @param source
     * @param event
     * @since 2.4
     */
    public ScrollEvent(Object source,KeyEvent event) {
        super(source,SCROLL_ACTION);
        theKeyEvent = event;
    }

    /**
     * @see com.docfacto.beermat.events.CanvasAction#performAction(org.apache.batik.util.gui.resource.ActionMap)
     */
    public void performAction(ActionMap actionMap) {
        Action action = null;

        switch (theKeyEvent.getKeyCode()) {

        case KeyEvent.VK_KP_RIGHT:
        case KeyEvent.VK_RIGHT:
            if (theKeyEvent.isControlDown()) {
                action =
                    actionMap.get(JSVGCanvas.FAST_SCROLL_RIGHT_ACTION);
            }
            else {
                action = actionMap.get(JSVGCanvas.SCROLL_RIGHT_ACTION);
            }
            break;

        case KeyEvent.VK_LEFT:
        case KeyEvent.VK_KP_LEFT:
            if (theKeyEvent.isControlDown()) {
                action =
                    actionMap.get(JSVGCanvas.FAST_SCROLL_LEFT_ACTION);
            }
            else {
                action = actionMap.get(JSVGCanvas.SCROLL_LEFT_ACTION);
            }
            break;

        case KeyEvent.VK_UP:
        case KeyEvent.VK_KP_UP:
            if (theKeyEvent.isControlDown()) {
                action = actionMap.get(JSVGCanvas.FAST_SCROLL_UP_ACTION);
            }
            else {
                action = actionMap.get(JSVGCanvas.SCROLL_UP_ACTION);
            }
            break;

        case KeyEvent.VK_DOWN:
        case KeyEvent.VK_KP_DOWN:
            if (theKeyEvent.isControlDown()) {
                action =
                    actionMap.get(JSVGCanvas.FAST_SCROLL_DOWN_ACTION);
            }
            else {
                action = actionMap.get(JSVGCanvas.SCROLL_DOWN_ACTION);
            }
            break;
        }

        if (action!=null) {
            action.actionPerformed(null);
        }
    }
}
