package com.docfacto.beermat.events;

import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

import com.docfacto.beermat.utils.BeermatUtils;

/**
 * Fired when a mouse moves on the SVG Canvas
 * 
 * @author dhudson - created 5 Jul 2013
 * @since 2.4
 */
public class CanvasMouseMoveEvent extends BeermatEvent {

    /**
     * {@value}
     */
    private static final long serialVersionUID = 1L;

    private final MouseEvent theEvent;

    private int theSVGX;
    private int theSVGY;

    /**
     * Constructor.
     * 
     * @param source of the event
     * @param event mouse event
     * @param transform the viewbox transform
     * @since 2.4
     */
    public CanvasMouseMoveEvent(Object source,MouseEvent event,
    AffineTransform transform) {
        super(source,MOUSE_EVENT_ACTION);
        theEvent = event;

        Point2D canvasPoint = BeermatUtils.getTranformedMousePoint(event,
            transform);

        if (canvasPoint!=null) {
            theSVGX = (int)Math.round(canvasPoint.getX());
            theSVGY = (int)Math.round(canvasPoint.getY());
        }
        else {
            theSVGX = 0;
            theSVGY = 0;
        }
    }

    /**
     * Get SVG X
     * 
     * @return the SVG X
     * @since 2.4
     */
    public int getSVGX() {
        return theSVGX;
    }

    /**
     * Get SVG Y
     * 
     * @return the SVG Y
     * @since 2.4
     */
    public int getSVGY() {
        return theSVGY;
    }

    /**
     * The X location
     * 
     * @return X
     * @since 2.4
     */
    public int getMouseX() {
        return theEvent.getX();
    }

    /**
     * The Y location
     * 
     * @return Y
     * @since 2.4
     */
    public int getMouseY() {
        return theEvent.getY();
    }
}
