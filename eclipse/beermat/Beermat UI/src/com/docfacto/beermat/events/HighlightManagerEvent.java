package com.docfacto.beermat.events;

/**
 * Notify the Highlight manager that some action needs to be taken
 * 
 * @author dhudson - created 24 Jan 2014
 * @since 2.5
 */
public class HighlightManagerEvent extends BeermatEvent
{

    /**
     * @serial
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor.
     * 
     * @param source of the event
     */
    public HighlightManagerEvent(Object source) {
        super(source,HIGHLIGHT_MANAGER_EVENT_ACTION);
    }

}
