package com.docfacto.beermat.events;

/**
 * Handles the grid required events
 * 
 * @author dhudson - created 24 Jun 2013
 * @since 2.4
 */
public class GridEvent extends BeermatEvent {

    private static final long serialVersionUID = 1L;

    private final boolean isGridRequired;

    /**
     * Constructor.
     * 
     * @param source
     * @param required
     */
    public GridEvent(Object source,boolean required) {
        super(source,GRID_ACTION);
        isGridRequired = required;
    }

    /**
     * True is the grid is to be displayed
     * 
     * @return true if a grid is required
     * @since 2.4
     */
    public boolean isGridRequired() {
        return isGridRequired;
    }

}
