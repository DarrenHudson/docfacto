package com.docfacto.beermat.events;

import java.util.EventObject;


/**
 * A Beermat event.
 * 
 * @author dhudson - created 20 Jun 2013
 * @since 2.4
 */
public class BeermatEvent extends EventObject {

    /**
     * {@value}
     */
    private static final long serialVersionUID = -1L;

    public static final int SVG_DIRTY_ACTION = 1;

    public static final int UI_COMPLETE_ACTION = 2;

    public static final int CONTROL_TOOLBAR_CHANGE_ACTION = 3;

    public static final int PALETTEBAR_CHANGE_ACTION = 4;

    public static final int SVG_SIZE_ACTION = 5;

    /**
     * {@value}
     */
    public static final int ZOOM_ACTION = 6;

    public static final int GRID_ACTION = 7;

    /**
     * {@value}
     */
    public static final int SVG_MOUSE_EVENT_ACTION = 8;

    public static final int MOUSE_EVENT_ACTION = 9;

    public static final int UI_CLOSE_ACTION = 11;

    public static final int SAVE_ACTION = 12;

    public static final int ATTRIBUTE_CHANGE_ACTION = 13;
    
    public static final int CURSOR_CHANGE_ACTION = 15;

    public static final int GROUP_ELEMENT_ACTION = 16;

    public static final int UNGROUP_ELEMENT_ACTION = 17;

    public static final int SCROLL_ACTION = 18;

    public static final int COLOUR_CHANGE_ACTION = 19;

    public static final int GRID_SIZE_EVENT_ACTION = 20;
    
    public static final int ZOOM_CHANGE_EVENT_ACTION = 21;
    
    public static final int ELEMENT_SELECT_EVENT_ACTION = 22;
    
    public static final int MARKER_CHANGE_EVENT_ACTION = 23;
    
    public static final int HIGHLIGHT_MANAGER_EVENT_ACTION = 24;
    

    /**
     * What type of event it is
     */
    private final int theAction;

    /**
     * Abstract class for BeermatEvent
     * 
     * @param source of where the event was fired
     * @param action type of event
     */
    public BeermatEvent(Object source,int action) {
        super(source);
        theAction = action;
    }

    /**
     * Return the action value
     * 
     * @return the action
     * @since 2.4
     */
    public int getAction() {
        return theAction;
    }

    /**
     * Check to see if its an SVG Dirty event
     * 
     * @return true if its and SVG dirty event
     * @since 2.4
     */
    public boolean isSVGDirtyEvent() {
        return theAction==SVG_DIRTY_ACTION;
    }

    /**
     * Check to see if its a UI complete event
     * 
     * @return true if its a UI complete event
     * @since 2.4
     */
    public boolean isUICompleteEvent() {
        return theAction==UI_COMPLETE_ACTION;
    }

    /**
     * Check to see if its a control toolbar change event
     * 
     * @return true if its a control toolbar change event
     * @since 2.4
     */
    public boolean isControlToolbarChangeEvent() {
        return theAction==CONTROL_TOOLBAR_CHANGE_ACTION;
    }

    /**
     * Check to see if its a palette toolbar change event
     * 
     * @return true if a palette toolbar change event
     * @since 2.4
     */
    public boolean isPaletteToolbarChangeEvent() {
        return theAction==PALETTEBAR_CHANGE_ACTION;
    }

    /**
     * Check to see if its a size event
     * 
     * @return true if its a size event
     * @since 2.4
     */
    public boolean isSVGSizeEvent() {
        return theAction==SVG_SIZE_ACTION;
    }

    /**
     * Check to see if its a zoom event
     * 
     * @return true if a zoom event
     * @since 2.4
     */
    public boolean isZoomEvent() {
        return theAction==ZOOM_ACTION;
    }

    /**
     * Check to see if its a grid event
     * 
     * @return true if its a grid event
     * @since 2.4
     */
    public boolean isGridEvent() {
        return theAction==GRID_ACTION;
    }

    /**
     * Check to see if its a SVG Mouse event
     * 
     * @return true if its a SVG mouse event
     * @since 2.4
     */
    public boolean isSVGElementMouseEvent() {
        return theAction==SVG_MOUSE_EVENT_ACTION;
    }

    /**
     * Check to see if the event is a canvas mouse event
     * 
     * @return true if its a mouse move event
     * @since 2.4
     */
    public boolean isCanvasMouseMoveEvent() {
        return theAction==MOUSE_EVENT_ACTION;
    }

    /**
     * Check to see if the page is closing
     * 
     * @return true if its a page close
     * @since 2.4
     */
    public boolean isUICloseEvent() {
        return theAction==UI_CLOSE_ACTION;
    }

    /**
     * Check to see if this is a save action
     * 
     * @return true if this is a save action
     * @since 2.4
     */
    public boolean isSaveEvent() {
        return theAction==SAVE_ACTION;
    }

    /**
     * Check to see if this as a attribute change event
     * 
     * @return true if this event is an attribute change event
     * @since 2.4
     */
    public boolean isAttributeChangeEvent() {
        return theAction==ATTRIBUTE_CHANGE_ACTION;
    }

    /**
     * Check to see if its a cursor change event
     * 
     * @return true if the event is a cursor change event
     * @since 2.4
     */
    public boolean isCursorChangeEvent() {
        return theAction==CURSOR_CHANGE_ACTION;
    }

    /**
     * Check to see if its a group element event
     * 
     * @return true if its a group element event
     * @since 2.4
     */
    public boolean isGroupElementEvent() {
        return theAction==GROUP_ELEMENT_ACTION;
    }

    /**
     * Check if action is an ungroup element action.
     * 
     * @return true if its an ungroup element event.
     * @since 2.4.2
     */
    public boolean isUngroupElementEvent() {
        return theAction==UNGROUP_ELEMENT_ACTION;
    }

    /**
     * Check if action is a colour change event.
     * 
     * @return true if action is a colour change event
     * @since 2.4.3
     */
    public boolean isColourChangeEvent() {
        return theAction==COLOUR_CHANGE_ACTION;
    }

    /**
     * Check if action is a grid size change
     * 
     * @return true if action is a grid size change event
     * @since 2.4.3
     */
    public boolean isGridSizeEvent() {
        return theAction==GRID_SIZE_EVENT_ACTION;
    }
    
    /**
     * Check if action is a zoom change event
     * 
     * @return true if action is a zoom change event
     * @since 2.4.3
     */
    public boolean isZoomChangeEvent() {
        return theAction==ZOOM_CHANGE_EVENT_ACTION;
    }
    
    /**
     * Check if action is an element change event
     * 
     * @return true if action is an element change
     * @since 2.4.5
     */
    public boolean isElementSelectChangeEvent(){
        return theAction==ELEMENT_SELECT_EVENT_ACTION;
    }
    
    /**
     * Check if action is a marker change event
     * 
     * @return true if action is a marker change
     * @since 2.4.5
     */
    public boolean isMarkerChangeEvent(){
        return theAction==MARKER_CHANGE_EVENT_ACTION;
    }

    /**
     * Check to see if the action is a highlight manager event.
     * 
     * @return true if the action is a highlight manager event
     * @since 2.5
     */
    public boolean isHighlightManagerEvent() {
        return theAction==HIGHLIGHT_MANAGER_EVENT_ACTION;
    }
}
