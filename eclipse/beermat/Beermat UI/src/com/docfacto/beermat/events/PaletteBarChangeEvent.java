package com.docfacto.beermat.events;

import com.docfacto.beermat.ui.toolbar.PaletteItem;
import com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler;

/**
 * This event is fired when the palette bar has changed
 * 
 * @author dhudson - created 21 Jun 2013
 * @since 2.4
 */
public class PaletteBarChangeEvent extends BeermatEvent {

    private static final long serialVersionUID = 1L;

    private final PaletteItem thePaletteItem;
    private final ShapeHandler theShapeHandler;

    /**
     * Constructor.
     * 
     * @param source
     * @param paletteItem
     * @param shapeHandler
     */
    public PaletteBarChangeEvent(Object source,PaletteItem paletteItem,
    ShapeHandler shapeHandler) {
        super(source,PALETTEBAR_CHANGE_ACTION);
        thePaletteItem = paletteItem;
        theShapeHandler = shapeHandler;
    }

    /**
     * Return the currently selected palette item
     * 
     * @return the palette item
     * @since 2.4
     */
    public PaletteItem getPaletteItem() {
        return thePaletteItem;
    }

    /**
     * Return a shape handler or null
     * 
     * @return a shape handler, or null
     * @since 2.4
     */
    public ShapeHandler getShapeHandler() {
        return theShapeHandler;
    }
}
