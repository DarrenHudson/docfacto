package com.docfacto.beermat.events;

/**
 * An event for when grid size has changed.
 * 
 * @author kporter - created Aug 16, 2013
 * @since 2.4.3
 */
public class GridSizeEvent extends BeermatEvent {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private int theSize;

    /**
     * Constructs a new Event for what grid size has changed.
     * 
     * @param source The event source
     * @param size grid size
     * @since 2.4
     */
    public GridSizeEvent(Object source,int size) {
        super(source,GRID_SIZE_EVENT_ACTION);
        theSize = size;
    }

    /**
     * Returns size.
     * 
     * @return the size
     * @since 2.4
     */
    public int getSize() {
        return theSize;
    }

    /**
     * Sets theSize.
     * 
     * @param size the size value
     * @since 2.4
     */
    public void setSize(int size) {
        theSize = size;
    }

}
