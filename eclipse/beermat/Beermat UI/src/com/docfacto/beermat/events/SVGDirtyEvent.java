package com.docfacto.beermat.events;

/**
 * This event is fired when the SVG has been rendered dirty
 *
 * @author dhudson - created 20 Jun 2013
 * @since 2.4
 */
public class SVGDirtyEvent extends BeermatEvent {

    private static final long serialVersionUID = 9169055113681626225L;
    
    /**
     * Constructor.
     * 
     * @param source event source
     * @since 2.4
     */
    public SVGDirtyEvent(Object source) {
        super(source,SVG_DIRTY_ACTION);
    }
    
}
