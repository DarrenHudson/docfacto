package com.docfacto.beermat.events;

/**
 * Event to signify that something has changed in the Control Toolbar.
 *
 * @author dhudson - created 21 Jun 2013
 * @since 2.4
 */
public class ControlToolbarChangeEvent extends BeermatEvent {

    private static final long serialVersionUID = 1L;
    
    /**
     * Constructor.
     * @param source
     * @since 2.4
     */
    public ControlToolbarChangeEvent(Object source) {
        super(source, CONTROL_TOOLBAR_CHANGE_ACTION);
    }
}
