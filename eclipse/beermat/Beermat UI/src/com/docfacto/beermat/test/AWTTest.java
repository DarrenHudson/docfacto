package com.docfacto.beermat.test;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGLocatable;
import org.w3c.dom.svg.SVGRect;

import com.docfacto.beermat.svg.IconTranscoder;
import com.docfacto.beermat.svg.TransformElementParser;
import com.docfacto.beermat.ui.BeermatViewConstants;
import com.docfacto.beermat.utils.ElementUtils;
import com.docfacto.beermat.utils.SVGUtils;

public class AWTTest {

    public Image theSWTImage;

    AWTTest() {
        JFrame frame = new JFrame("Test");
        frame.setPreferredSize(new Dimension(100,100));
        frame.setSize(100,100);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        final Container cp = frame.getContentPane();
        final GridBagConstraints gBC = new GridBagConstraints();
        cp.setLayout(new GridBagLayout());
        
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());

        gBC.gridx = 0;
        gBC.gridy = 0;
        gBC.insets = new Insets(2,2,2,2);
        gBC.anchor = GridBagConstraints.NORTHWEST;
        gBC.fill = GridBagConstraints.BOTH;
        gBC.weightx = 0.5;
        gBC.weighty = 0.5;
        
        cp.add(panel,gBC);

        IconTranscoder transcoder = new IconTranscoder(BeermatViewConstants.ICON_SIZE,BeermatViewConstants.ICON_SIZE);
        
        Document document = SVGUtils.createSVGDocument(null);
        
        JSVGCanvas canvas = new JSVGCanvas();
        canvas.setDocumentState(JSVGCanvas.ALWAYS_DYNAMIC);
        canvas.setDocument(document);
        
        Element element = ElementUtils.createPath(document,new Point(0,0));
        element.setAttribute("d","m3,41l56,-38l56,38l-56,38l-56,-38zz");
        element.setAttribute("fill","none");
        element.setAttribute("stroke","#000000");
        element.setAttribute("stroke-width","5");
        
        //Remove this, add load listener to canvas, and do the following after document has been rendered
        try {
            Thread.sleep(500);
        }
        catch (InterruptedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        
        
        SVGLocatable locatable = (SVGLocatable)element;
        SVGRect box = locatable.getBBox();
        
        float x = 18;
        float y = 18;
        
        float xScale = x / box.getWidth();
        float yScale = y / box.getHeight();
        
        TransformElementParser transformer = new TransformElementParser(element);
        transformer.setScale(xScale,yScale);
        document.getDocumentElement().setAttribute("width",x+"");
        document.getDocumentElement().setAttribute("height",y+"");
        
        
        // DOMImplementation impl = SVGDOMImplementation.getDOMImplementation();


        TranscoderInput ti = new TranscoderInput(document);

        try {
            transcoder.transcode(ti,null);
            // panel.addanzad(new JLabel("Hello World"));
            panel.add(new JLabel(new ImageIcon(transcoder.getImage())),gBC);
            frame.pack();
            frame.setVisible(true);
            
            theSWTImage = transcoder.getSWTImage();
            
//             transcoder.getSWTImage();
        }
        catch (TranscoderException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
       AWTTest a =  new AWTTest();
        
        try {
            Display display = Display.getDefault();
            Shell shell = new Shell(display);
            shell.setBounds(500,500,100,100);
            Label label = new Label(shell,SWT.NONE);
            label.setBounds(0,0,100,100);
            label.setImage(a.theSWTImage);
            shell.open();
            shell.layout();
            while (!shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                    display.sleep();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}
