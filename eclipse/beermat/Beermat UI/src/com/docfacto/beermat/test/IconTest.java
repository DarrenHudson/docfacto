package com.docfacto.beermat.test;

import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.batik.dom.svg.SVGContext;
import org.apache.batik.dom.svg.SVGDOMImplementation;
import org.apache.batik.dom.svg.SVGOMElement;
import org.apache.batik.dom.svg.SVGOMMarkerElement;
import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.swing.svg.GVTTreeBuilderAdapter;
import org.apache.batik.swing.svg.GVTTreeBuilderEvent;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.TranscodingHints;
import org.apache.batik.transcoder.image.ImageTranscoder;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.batik.util.SVGConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.BeermatLoader;
import com.docfacto.beermat.generated.ElementDef;
import com.docfacto.beermat.generated.Palette;
import com.docfacto.beermat.generated.SvgDef;
import com.docfacto.beermat.generated.Type;
import com.docfacto.beermat.svg.DragData;
import com.docfacto.beermat.svg.IconTranscoder;
import com.docfacto.beermat.svg.ShapeShifter;
import com.docfacto.beermat.svg.TransformElementParser;
import com.docfacto.beermat.utils.BeermatUtils;
import com.docfacto.beermat.utils.ElementUtils;
import com.docfacto.beermat.utils.PluginManager;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.core.utils.SWTUtils;

public class IconTest {


    /**
     * Change this
     */
    static String USER_PATH = "/Users/kporter/development/";
    
    static String BEERMAT_PATH = "docfacto/main/eclipse/beermat/Beermat UI/";
    
    static String BEERMAT_XML_PATH = BEERMAT_PATH+"resources/beermat.xml";
    /**
     * path to the output in the beermat folder. has leading /
     */
    static String BEERMAT_SAVE_FOLDER_PATH = BEERMAT_PATH+"icons/palettes/";
    
    static int ICON_SIZE = 18;
    
//    private static String TEST_XML =
//    "/Users/dhudson/development/docfacto/main/eclipse/beermat/Beermat UI/src/com/docfacto/beermat/test/resources/Beermat.xml";
    
    private static String LIVE_XML = USER_PATH + BEERMAT_XML_PATH;
    
    IconTest() {
        try {
            Display display = Display.getDefault();
            Shell shell = new Shell(display);
            shell.setBounds(500,500,200,200);
            shell.setLayout(new FillLayout());

            BeermatLoader loader = new BeermatLoader(new File(LIVE_XML));

            List<Palette> palettes = loader.getRootPalettes();

            Composite comp = new Composite(shell,SWT.NONE);
            comp.setLayout(new RowLayout());
            comp.setBackground(SWTUtils.getColor(SWT.COLOR_WHITE));

            for (Palette palette:palettes) {
                if (palette.getType().equals(Type.TOOLBAR) && palette.getName().equals("UML")) {
                    List<SvgDef> defs = PluginManager.getSVGDefs(palette);
                    for (SvgDef def:defs) {
                        Label label = new Label(comp,SWT.NONE);
                        label.setImage(generateICON(palette.getName(),def));
                        label.setToolTipText(def.getDescription());
                    }
                    
                    List<ElementDef> edefs = PluginManager.getElementDefs(palette);
                    for(ElementDef def: edefs){
                        Image i = generateIconFromElement(SVGUtils.getGroupElementFromElementDef(def),palette.getName(),def.getName());
                        Label label = new Label(comp,SWT.NONE);
                        label.setImage(i);
                        label.setToolTipText(def.getName()); 
                    }
                    
                }
//                if(palette.getType().equals(Type.MARKER)){
//                    ElementDef def = (ElementDef) palette.getPalettesAndSvgDevesAndElementDeves().get(0);
//                    Document allMarkers = SVGUtils.getSVGFromElementDef(def);
//                    SVGDocument reverseMarkers = SVGUtils.createSVGDocument(null);
//                    SVGUtils.setSVGSize(reverseMarkers,new Point(100,100));
//                    
//                    
//                    NodeList children = allMarkers.getDocumentElement().getChildNodes();
//                    for(int i=0;i<children.getLength();i++){
//                        if(children.item(i) instanceof SVGOMMarkerElement){
//                            SVGOMMarkerElement marker = ((SVGOMMarkerElement)children.item(i));
//                            generateMarkerIcons("markers",marker,false);
//                            
//                            String reverseID = ElementUtils.addMarker(reverseMarkers,marker,marker.getAttribute("id"),true);
//                            System.out.println(reverseID);
//                        }
//                    }
//                    
//                    NodeList reverseChildren = reverseMarkers.getElementsByTagName("marker");
//                    for(int i=0;i<reverseChildren.getLength();i++){                        
//                        generateMarkerIcons("markers",(SVGOMMarkerElement)reverseChildren.item(i),true);
//                    }
//                }
            }

            // Save the new paths
//            loader.save();

            shell.open();
            // shell.layout();
            while (!shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                    display.sleep();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
     
    
    public void generateIconsForMarkers(String paletteName, SVGOMMarkerElement marker, boolean reverse){
        SVGDocument document = SVGUtils.createSVGDocument(null);
        SVGUtils.setSVGSize(document,new Point(ICON_SIZE,ICON_SIZE));
        
        JSVGCanvas canvas = new JSVGCanvas();
        canvas.setDocumentState(JSVGCanvas.ALWAYS_DYNAMIC);
        
        final Object theLock = new Object();

        canvas.addGVTTreeBuilderListener(new GVTTreeBuilderAdapter() {
            /**
             * @see org.apache.batik.swing.svg.GVTTreeBuilderAdapter#gvtBuildCompleted(org.apache.batik.swing.svg.GVTTreeBuilderEvent)
             */
            @Override
            public void gvtBuildCompleted(GVTTreeBuilderEvent e) {
                // Let the processing commence
                synchronized (theLock) {
                    System.out.println("build complete..");
                    theLock.notify();
                }
            }
        });
        
        SVGOMMarkerElement newMarker = (SVGOMMarkerElement) document.importNode(marker,true);
        String id = newMarker.getAttribute("id");
        
        if(newMarker.getAttribute("fill").isEmpty() || !newMarker.getAttribute("fill").equals("none")){
            newMarker.setAttribute("fill","#E3A03A");             
        }
        newMarker.setAttribute("stroke","#E3A03A");
        
        document.getDocumentElement().appendChild(newMarker);      
        
        Element element = ElementUtils.createPath(document,new Point(0,0));
        
        element.setAttribute("stroke","#f9af40");
        element.setAttribute("stroke-width","1.3");
        if(reverse){
            element.setAttribute("d","M12 9 18 9");
            element.setAttribute("marker-start","url(#"+id+")");
        }else{
            element.setAttribute("d","M0 9 6 9");            
            element.setAttribute("marker-end","url(#"+id+")");
        }
        
     // Wait for it to be built
        canvas.setSVGDocument(document);

        try {
            // Wait for the build completed event
            synchronized (theLock) {
                theLock.wait();
            }
        }
        catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String directory = USER_PATH+BEERMAT_SAVE_FOLDER_PATH+paletteName;
        
        transcodeToPng(document,directory,id);
        
        
    }
    
    private Image generateIconFromElement(Element oldElement, String paletteName, String elementName){
        SVGDocument document = SVGUtils.createSVGDocument(null);
        SVGUtils.setSVGSize(document,new Point(200,200));
        JSVGCanvas canvas = new JSVGCanvas();
        canvas.setDocumentState(JSVGCanvas.ALWAYS_DYNAMIC);

        final Object theLock = new Object();
        canvas.addGVTTreeBuilderListener(new GVTTreeBuilderAdapter() {
            /**
             * @see org.apache.batik.swing.svg.GVTTreeBuilderAdapter#gvtBuildCompleted(org.apache.batik.swing.svg.GVTTreeBuilderEvent)
             */
            @Override
            public void gvtBuildCompleted(GVTTreeBuilderEvent e) {
                // Let the processing commence
                synchronized (theLock) {
                    System.out.println("build complete..");
                    theLock.notify();
                }
            }
        });
        
        Element element = (Element) document.importNode(oldElement,true);
        document.getDocumentElement().appendChild(element);
        // Wait for it to be built
        canvas.setSVGDocument(document);

        try {
            // Wait for the build completed event
            synchronized (theLock) {
                theLock.wait();
            }
        }
        catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        element = scaleImage(element,canvas);

        centreImage(element);

//        if (style==null||style.isEmpty()) {
            // These are the default settings
            element.setAttribute("fill","none");
            element.setAttribute("stroke","#E3A03A");
            element.setAttribute("stroke-width","4");
//        }
//        else {
//            element.setAttribute("style",style);
//        }

        document.getDocumentElement().setAttribute("width","66");
        document.getDocumentElement().setAttribute("height","66");

        String path =
        USER_PATH+BEERMAT_SAVE_FOLDER_PATH+
            paletteName;
        
        transcodeToPng(document,path,elementName);
       
        IconTranscoder transcoder = new IconTranscoder(64,64);

        try {
            transcoder.transcode(new TranscoderInput(document),null);
        }
        catch (TranscoderException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return transcoder.getSWTImage();

    }

    private Image generateICON(String paletteName,SvgDef def) {
        // REMOVE:
        System.out.println("--> "+def.getName());

        SVGDocument document = SVGUtils.createSVGDocument(null);
        SVGUtils.setSVGSize(document,new Point(200,200));

        JSVGCanvas canvas = new JSVGCanvas();
        canvas.setDocumentState(JSVGCanvas.ALWAYS_DYNAMIC);

        final Object theLock = new Object();

        canvas.addGVTTreeBuilderListener(new GVTTreeBuilderAdapter() {
            /**
             * @see org.apache.batik.swing.svg.GVTTreeBuilderAdapter#gvtBuildCompleted(org.apache.batik.swing.svg.GVTTreeBuilderEvent)
             */
            @Override
            public void gvtBuildCompleted(GVTTreeBuilderEvent e) {
                // Let the processing commence
                synchronized (theLock) {
                    System.out.println("build complete..");
                    theLock.notify();
                }
            }
        });

        Element element = ElementUtils.createPath(document,new Point(0,0));
        element.setAttribute("d",def.getPath().getValue());

        // May contain stroke width info
        if (def.getPath().getStyle()!=null) {
            element.setAttribute("style",def.getPath().getStyle());
        }

        document.getDocumentElement().appendChild(element);

        // Wait for it to be built
        canvas.setSVGDocument(document);

        try {
            // Wait for the build completed event
            synchronized (theLock) {
                theLock.wait();
            }
        }
        catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        element = scaleImage(element,canvas);

        centreImage(element);

        // SVGLocatable locatable = (SVGLocatable)element;
        // SVGRect box = locatable.getBBox();
        //
        // DragData dragData = new DragData();
        // int imageX = BeermatUtils.convertToInt(box.getX());
        // int imageY = BeermatUtils.convertToInt(box.getY()); // ,
        // // newY;
        // System.out.println("bounds "+box.getX()+" : "+box.getY()+" : "+
        // box.getWidth()+" : "+box.getHeight());
        //
        // imageX = (imageX*-1)+1;
        // imageY = (imageY*-1)+1;
        //
        // dragData.setXDiff(imageX);
        // dragData.setYDiff(imageY);
        //
        // System.out.println("moving "+imageX+" : "+imageY);
        // // Move the element
        // ShapeShifter.updateElement(element,dragData);

        document.getDocumentElement().setAttribute("width","66");
        document.getDocumentElement().setAttribute("height","66");

        // Set the new path
        def.getPath().setValue(SVGUtils.elementToPath(document,element,canvas));

        String style = def.getPath().getStyle();

        if (style==null||style.isEmpty()) {
            // These are the default settings
            element.setAttribute("fill","#E3A03A");
            element.setAttribute("stroke","#E3A03A");
        }
        else {
            element.setAttribute("style",style);
        }

        String path =
        USER_PATH+BEERMAT_SAVE_FOLDER_PATH+
            paletteName;
        
        transcodeToPng(document,path,def.getName());
       
        IconTranscoder transcoder = new IconTranscoder(64,64);

        try {
            transcoder.transcode(new TranscoderInput(document),null);
        }
        catch (TranscoderException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return transcoder.getSWTImage();
    }
    
    private void transcodeToPng(Document document, String directory, String filename){
        PNGTranscoder pngTranscoder = new PNGTranscoder();
        TranscodingHints hints = new TranscodingHints();
        hints
            .put(ImageTranscoder.KEY_WIDTH,new Float(18));
        hints.put(ImageTranscoder.KEY_HEIGHT,
            new Float(18));

        hints.put(ImageTranscoder.KEY_DOM_IMPLEMENTATION,
            SVGDOMImplementation.getDOMImplementation());
        hints.put(ImageTranscoder.KEY_DOCUMENT_ELEMENT_NAMESPACE_URI,
            SVGConstants.SVG_NAMESPACE_URI);
        hints.put(ImageTranscoder.KEY_DOCUMENT_ELEMENT_NAMESPACE_URI,
            SVGConstants.SVG_NAMESPACE_URI);
        hints
            .put(ImageTranscoder.KEY_DOCUMENT_ELEMENT,SVGConstants.SVG_SVG_TAG);
        hints.put(ImageTranscoder.KEY_XML_PARSER_VALIDATING,false);

        pngTranscoder.setTranscodingHints(hints);

        File folder = new File(directory);
        if (!folder.exists()) {
            folder.mkdir();
        }

        try {
            File icon = new File(folder,filename+".png");
            FileOutputStream outputStream = new FileOutputStream(icon);
            pngTranscoder.transcode(new TranscoderInput(document),
                new TranscoderOutput(outputStream));
            outputStream.flush();
            outputStream.close();
        }
        catch (TranscoderException ex) {
            ex.printStackTrace();
        }
        catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    private void centreImage(final Element element) {

        final SVGOMElement svgEl = (SVGOMElement)element;
        SVGContext svgContext = svgEl.getSVGContext();
        Rectangle2D box = svgContext.getBBox();

        DragData dragData = new DragData();
        int imageX = BeermatUtils.convertToInt(box.getX());
        int imageY = BeermatUtils.convertToInt(box.getY());

        System.out.println("bounds "+box.getX()+" : "+box.getY()+" : "+
            box.getWidth()+" : "+box.getHeight()+" : "+box.getCenterX()+" : "+
            box.getCenterY());

        int xOffset = 1;
        int yOffset = 1;

        // Try and centre the image in middle of the canvas

        xOffset = 32-(BeermatUtils.convertToInt(box.getWidth())/2);
        yOffset = 32-(BeermatUtils.convertToInt(box.getHeight()/2));

        imageX = (imageX*-1)+xOffset;
        imageY = (imageY*-1)+yOffset;

        dragData.setXDiff(imageX);
        dragData.setYDiff(imageY);

        System.out.println("moving "+imageX+" : "+imageY);
        // Move the element
        ShapeShifter.updateElement(element,dragData);
    }

    private Element scaleImage(final Element element,JSVGCanvas canvas) {
        // SVGLocatable locatable = (SVGLocatable)element;
        // SVGRect box = locatable.getBBox();

        final SVGOMElement svgEl = (SVGOMElement)element;
        SVGContext svgContext = svgEl.getSVGContext();

        Rectangle2D box = svgContext.getBBox();

        if (box==null) {
            return element;
        }

        System.out.println("box "+box.getX()+" : "+box.getY()+" : "+
            box.getWidth()+" : "+box.getHeight());

        // Lets preserve the aspect ratio
        float x = 63;
        float y = 63;

        if (box.getWidth()>box.getHeight()) {
            y = 63*(float)(box.getHeight()/box.getWidth());
        }
        else {
            x = 63*(float)(box.getWidth()/box.getHeight());
        }

        float xScale = x/(float)box.getWidth();
        float yScale = y/(float)box.getHeight();

        //If its close enough to its original size, don't resize it? why? then some objects which are 1.1 size are cut off.
//        if (Math.round(xScale)==1&&Math.round(yScale)==1) {
//            // Nothing to do
//            return element;
//        }

        System.out.println("Scale factor "+xScale+" : "+yScale);

        TransformElementParser transformer =
            new TransformElementParser(element);
        transformer.setScale(xScale,yScale);

        SVGDocument document = (SVGDocument)element.getOwnerDocument();

        String newPath = SVGUtils.elementToPath(document,element,canvas);
        element.setAttribute("d",newPath);
        
        System.out.println(newPath);

        document.getDocumentElement().removeChild(element);

        Element newElement = ElementUtils.createPath(document,new
            Point(0,0));
        newElement.setAttribute("d",newPath);

        return newElement;
    }

    /**
     * 
     * @param args none
     * @since 2.4
     */
    public static void main(String[] args) {
        new IconTest();
    }
}
