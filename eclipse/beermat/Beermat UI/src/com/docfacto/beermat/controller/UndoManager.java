package com.docfacto.beermat.controller;

import java.util.EmptyStackException;
import java.util.Stack;

import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.common.DocfactoException;

/**
 * Holds state to undo and redo
 * 
 * @author kporter - created 21 Jan 2014
 * @since 2.5.1
 */
public class UndoManager {
    private final static int INITIAL_SIZE = 10;

    private Stack<String> theUndoStack = new Stack<String>();
    private Stack<String> theRedoStack = new Stack<String>();

    /**
     * Constructor.
     */
    public UndoManager() {
        theUndoStack.setSize(INITIAL_SIZE);
        theRedoStack.setSize(INITIAL_SIZE);
    }

    /**
     * Get the last state and store the current state.
     * 
     * @param current The current state that will be added to the 'redo' stack
     * @return The undone state
     * @since 2.5.1
     */
    public String undo(String current) {
        try {
            String undo = theUndoStack.pop();

            theRedoStack.push(current);

            return undo;
        }
        catch (EmptyStackException ex) {
            return null;
        }
    }

    /**
     * Get the redo state and store the current state in the stack
     * 
     * @param current The current state that will be added to the 'redo' stack
     * @return the redone state
     * @since 2.5.1
     */
    public String redo(String current) {
        try {
            String redo = theRedoStack.pop();

            theUndoStack.push(current);

            return redo;
        }
        catch (EmptyStackException ex) {
            return null;
        }
    }

    /**
     * Add a state to the stack
     * 
     * @param state the state to add to the stack for undoing
     * @since 2.5.1
     */
    public void addState(String state) {
        theRedoStack.clear();
        theUndoStack.push(state);
    }

    /**
     * Get the current state from the given controller, then add that state to the stack.
     * 
     * @param controller The beermat controller
     * @since 2.5.1
     */
    public void saveCurrentState(BeermatController controller) {
        try {
            String state = controller.getManager().getFormattedDocument();
            addState(state);
        }
        catch (DocfactoException ex) {
           BeermatUIPlugin.logException("UndoManager exception",ex);
        }
    }

}
