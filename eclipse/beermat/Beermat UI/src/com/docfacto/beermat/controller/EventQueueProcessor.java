package com.docfacto.beermat.controller;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;

import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.listeners.BeermatEventListener;
import com.docfacto.beermat.plugin.BeermatUIPlugin;

/**
 * Handle the Beermat events in a separate thread.
 * 
 * This stop thread deadlock when an event tries to process another event
 * 
 * @author dhudson - created 5 Jul 2013
 * @since 2.4
 */
public class EventQueueProcessor extends Thread {

    // Synchronised blocking queue
    private final LinkedBlockingQueue<BeermatEvent> theEventQueue;

    // Use copy on write as reads far outweighs the inserts and deletes making
    // it thread safe
    private final CopyOnWriteArrayList<BeermatEventListener> theEventListeners;
    private boolean isRunning;

    private static int theInstanceCount = 0;

    /**
     * Constructor.
     * 
     * @since 2.4
     */
    public EventQueueProcessor() {
        super("BeermatEventQueueProcessor-"+theInstanceCount++);
        theEventListeners = new CopyOnWriteArrayList<BeermatEventListener>();
        theEventQueue = new LinkedBlockingQueue<BeermatEvent>();
        isRunning = true;
    }

    /**
     * Add an event listener
     * 
     * @param eventListener to add
     * @since 2.4
     */
    public void addListener(BeermatEventListener eventListener) {
        theEventListeners.add(eventListener);
    }

    /**
     * Remove a event listener
     * 
     * @param eventListener to remove
     * @since 2.4
     */
    public void removeListener(BeermatEventListener eventListener) {
        theEventListeners.remove(eventListener);
    }

    /**
     * Add an event to the queue
     * 
     * @param event to add to the queue
     * @since 2.4
     */
    public void addToQueue(BeermatEvent event) {
        theEventQueue.offer(event);
    }

    /**
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        while (isRunning) {

            BeermatEvent event;
            try {
                event = theEventQueue.take();

                for (BeermatEventListener listener:theEventListeners) {
                    try {
                        listener.handleEvent(event);
                    }
                    catch (Throwable t) {
                        BeermatUIPlugin.logException(
                            "EventQueueProcessor ",t);
                    }
                }

                Thread.yield();
            }
            catch (InterruptedException ignore) {

            }
        }
    }

    /**
     * Stop the thread
     * 
     * @since 2.4
     */
    public void finish() {
        isRunning = false;
        theEventQueue.clear();
        theEventListeners.clear();
        // Wake up the take
        synchronized(theEventQueue) {
            theEventQueue.notifyAll();
        }
    }

}
