package com.docfacto.beermat.controller;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;

/**
 * Manages the clipboard, for cut copy paste.
 * 
 * @author kporter - created 22 Jan 2014
 * @since 2.5.1
 */
public class ClipboardManager {

    private ArrayList<Element> theClipboard;
    boolean theCut = false;

    /**
     * Construct a clipboard manager.
     */
    public ClipboardManager() {
        theClipboard = new ArrayList<Element>(0);
    }

    /**
     * Copy a list of elements to the clipboard
     * 
     * @param elements The Element's to copy
     * @since 2.5.1
     */
    public void copy(List<Element> elements) {
        theClipboard.clear();

        for (Element element:elements) {
            theClipboard.add((Element)element.cloneNode(true));
        }
        theCut = false;
    }

    /**
     * Cut a list of elements, this is the same as copy but sets cut to true
     * 
     * This is identical to copy except the boolean flag 'isCut' is now true, this is useful for when pasting, if paste
     * is from a copy there may be a displacement, but if paste is from a cut, its usually required in the same place
     * 
     * @param elements The list of elements to cut
     * @since 2.5.1
     */
    public void cut(List<Element> elements) {
        copy(elements);
        theCut = true;
    }

    /**
     * Returns the list of elements to paste
     * 
     * @return The list of Elements to paste
     * @since 2.5.1
     */
    public ArrayList<Element> paste() {
        return theClipboard;
    }

    /**
     * Returns true if last action was a cut, if a copy false.
     * @return True if the last action was a cut
     * @since 2.5.1
     */
    public boolean isCut() {
        return theCut;
    }

    public void setCut(boolean cut) {
        theCut = cut;
    }

}
