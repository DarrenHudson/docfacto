package com.docfacto.beermat.overlay;

import java.awt.Graphics2D;

import org.apache.batik.gvt.GraphicsNode;
import org.w3c.dom.Element;

import com.docfacto.beermat.hotspot.ConnectorHotSpot;
import com.docfacto.beermat.hotspot.HotSpot;
import com.docfacto.beermat.hotspot.HotSpotHighlightCollection;
import com.docfacto.beermat.hotspot.NSEWCHotSpots;
import com.docfacto.beermat.svg.ConnectorHandler;
import com.docfacto.beermat.svg.TextHandler;
import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.common.DocfactoException;

/**
 * Overlay for the text tool
 * 
 * @author dhudson - created 18 Oct 2013
 * @since 2.4
 */
public class TextOverlay implements BeermatOverlay {

    private HotSpotHighlightCollection theTextNode;
    private final BeermatAWTCanvas theCanvas;

    /**
     * Constructor.
     * 
     * @param canvas beermat canvas
     * @since 2.4
     */
    public TextOverlay(BeermatAWTCanvas canvas) {
        theCanvas = canvas;
    }

    /**
     * @see com.docfacto.beermat.overlay.BeermatOverlay#paint(java.awt.Graphics2D,
     * boolean)
     */
    @Override
    public void paint(Graphics2D g2d,boolean recalcHostSpots) {

        if (theTextNode!=null) {
            try {
                theTextNode.render(g2d);
            }
            catch (Exception ignore) {
                // Sometimes when painting text paths, a NPE is thrown
            }
        }
    }

    /**
     * Set a hover element to so the hot spots
     * 
     * @param element
     * @since 2.4
     */
    public void setHoverElement(Element element) {
        if (element!=null) {
            try {

                GraphicsNode node = theCanvas.getGraphicsNodeFor(element);

                // Connector hot spot for connectors
                if (ConnectorHandler.isConnector(element)) {
                    // Check to see if it already has text
                    if (TextHandler.getAnchoredElements(element).size()==0) {
                        theTextNode = new ConnectorHotSpot(theCanvas,node);
                        paint(theCanvas.getOverlayGraphics(),false);
                    }
                }
                else {
                    // Might be too small
                    if (node.getBounds().getHeight()>10&&
                        node.getBounds().getWidth()>10) {
                        theTextNode =
                            new NSEWCHotSpots(theCanvas,node);
                        paint(theCanvas.getOverlayGraphics(),false);
                    }
                }
            }
            catch (DocfactoException ignore) {
                // All ready null checked
            }
        }
        else {
            // Repaint the area that had the highlight to remove the hot spots
            if (theTextNode!=null) {
                theCanvas.repaint(theTextNode.getDirtyBounds());
            }

            theTextNode = null;
        }
    }

    /**
     * Update the overlay with the mouse position.
     * 
     * @param x location
     * @param y location
     * @since 2.4
     */
    public void updateHotspotPosition(int x,int y) {
        if (theTextNode!=null) {
            if (theTextNode.updatePosition(x,y)) {
                paint(theCanvas.getOverlayGraphics(),false);
            }
        }
    }

    /**
     * Return the highlighted hot spot or null
     * 
     * @return the highlighted hot spot or null
     * @since 2.4
     */
    public HotSpot getHotSpot() {
        if (theTextNode==null) {
            return null;
        }

        return theTextNode.getHighlightedHotSpot();
    }

    /**
     * Return the SVG element for this node, or null if not set
     * 
     * @return the SVG element for this node
     * @since 2.4
     */
    public Element getTextNodeElement() {
        if (theTextNode==null) {
            return null;
        }

        return theTextNode.getElement();
    }
}
