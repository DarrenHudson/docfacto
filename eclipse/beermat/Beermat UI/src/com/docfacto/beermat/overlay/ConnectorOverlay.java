package com.docfacto.beermat.overlay;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.hotspot.HotSpot;
import com.docfacto.beermat.hotspot.NSEWHotSpots;
import com.docfacto.beermat.utils.BeermatUtils;

/**
 * Overlay for the connectors
 * 
 * @author dhudson - created 18 Oct 2013
 * @since 2.4
 */
public class ConnectorOverlay implements BeermatOverlay {

    private final BeermatController theController;

    private NSEWHotSpots theConnectingNode;

    private HotSpot theStartHotSpot;

    private NSEWHotSpots theStartNode;

    private Point2D theStartPoint;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @since 2.4
     */
    public ConnectorOverlay(BeermatController controller) {
        theController = controller;
    }

    /**
     * @see com.docfacto.beermat.overlay.BeermatOverlay#paint(java.awt.Graphics2D,
     * boolean)
     */
    @Override
    public void paint(Graphics2D g2d,boolean recalcHostSpots) {

        if (theConnectingNode!=null) {
            theConnectingNode.calculateTransformedBounds();
            g2d.setStroke(new BasicStroke(2f));
            theConnectingNode.render(g2d);
        }

        if (theStartHotSpot!=null) {
            if (recalcHostSpots) {
                int location = theStartHotSpot.getLocation();
                theStartNode.calculateTransformedBounds();
                theStartNode.createHotSpots();
                theStartNode.setHighlightedHotSpot(location);
                theStartHotSpot = theStartNode.getHighlightedHotSpot();
            }

            g2d.setColor(HotSpot.HOTSPOT_HIGHLIGHT_COLOUR);
            g2d.fill(theStartHotSpot.getShape());
        }
    }

    /**
     * Set an edge node to highlight
     * 
     * @param connectingNode an edge node
     * @since 2.4
     */
    public void setConnectingNode(NSEWHotSpots connectingNode) {
        if (connectingNode==null&&theConnectingNode==null) {
            return;
        }

        Rectangle bounds;

        if (connectingNode==null) {
            bounds = theConnectingNode.getTransformedBounds();
        }
        else {
            bounds = connectingNode.getTransformedBounds();
        }

        theConnectingNode = connectingNode;

        theController.getBeermatAWTCanvas().repaint(
            BeermatUtils.outset(bounds,10));
    }

    /**
     * Return the connector start node.
     * 
     * @return the connector start node
     * @since 2.4
     */
    public NSEWHotSpots getStartNode() {
        return theStartNode;
    }

    /**
     * Return the connector end node.
     * 
     * @return the end node
     * @since 2.4
     */
    public NSEWHotSpots getEndNode() {
        return theConnectingNode;
    }

    /**
     * Return the start hot spot
     * 
     * @return the start hot spot or null
     * @since 2.4
     */
    public HotSpot getStartHotSpot() {
        return theStartHotSpot;
    }

    /**
     * Reset the connecting hot spots and repaint if required
     * 
     * @since 2.4
     */
    public void resetConnectingHotSpots() {
        if (theStartHotSpot!=null) {
            theController.getBeermatAWTCanvas().repaint(
                BeermatUtils.outset(theStartHotSpot.getBounds(),10));
            theStartHotSpot = null;
        }
    }

    /**
     * Update the overlay with the mouse position.
     * 
     * @param x location
     * @param y location
     * @since 2.4
     */
    public void updateHotspotPosition(int x,int y) {
        if (theConnectingNode!=null) {
            if (theConnectingNode.updatePosition(x,y)) {
                paint(theController.getBeermatAWTCanvas().getOverlayGraphics(),
                    false);
            }
        }
    }

    /**
     * Return the highlighted hot spot or null
     * 
     * @return the highlighted hot spot or null
     * @since 2.4
     */
    public HotSpot getHotSpot() {
        if (theConnectingNode==null) {
            return null;
        }

        return theConnectingNode.getHighlightedHotSpot();
    }

    /**
     * If there is a node highlighted, retain the hot spot
     * 
     * @param startPoint transformed mouse position
     * @since 2.5
     */
    public void startedDragging(Point2D startPoint) {
        if (theConnectingNode!=null) {
            theStartHotSpot = theConnectingNode.getHighlightedHotSpot();
        }
        theStartNode = theConnectingNode;
        theStartPoint = startPoint;
    }

    /**
     * Return the start point
     * 
     * @return the transformed start point
     * @since 2.5
     */
    public Point2D getStartPoint() {
        return theStartPoint;
    }
}
