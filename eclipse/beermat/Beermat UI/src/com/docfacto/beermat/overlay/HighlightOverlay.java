package com.docfacto.beermat.overlay;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;

import com.docfacto.beermat.highlight.HighlightElement;
import com.docfacto.beermat.highlight.HighlightManager;
import com.docfacto.beermat.hotspot.CentreHotSpot;
import com.docfacto.beermat.hotspot.HotSpot;
import com.docfacto.beermat.hotspot.LocatingHotSpot;
import com.docfacto.beermat.svg.ConnectorHandler;
import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.DocfactoException;

/**
 * This class is in charge of drawing the highlighted and selected elements
 * 
 * @author dhudson - created 26 Jul 2013
 * @since 2.4
 */
public class HighlightOverlay implements BeermatOverlay {

    private final HighlightManager theHighlightManager;
    private final BeermatAWTCanvas theCanvas;
    private final List<CentreHotSpot> theHotSpots;
    private final List<LocatingHotSpot> theLocatingHotSpots;

    private boolean isDragging = false;

    /**
     * Constructor.
     * 
     * @param highlightManager Highlight manager
     * @param canvas beermat canvas
     * @since 2.4
     */
    public HighlightOverlay(HighlightManager highlightManager,
    BeermatAWTCanvas canvas) {
        theHighlightManager = highlightManager;
        theCanvas = canvas;
        theHotSpots = new ArrayList<CentreHotSpot>(3);
        theLocatingHotSpots = new ArrayList<LocatingHotSpot>(3);
    }

    /**
     * @see com.docfacto.beermat.overlay.BeermatOverlay#paint(java.awt.Graphics2D, boolean)
     */
    @Override
    public void paint(Graphics2D g2d,boolean recalcHostSpots) {        
        g2d.setStroke(new BasicStroke(2f));

        if (isDragging) {
            // This can sometimes throw runtime exceptions, due to
            // synchronisation
            try {
                // Draw hot spots ..
                // Note draw these first, as this will re calculate the position
                // of the moving element
                for (LocatingHotSpot hotSpot:theLocatingHotSpots) {
                    hotSpot.render(g2d);
                }

                for (CentreHotSpot hotSpot:theHotSpots) {
                    if (recalcHostSpots) {
                        // The canvas may have scrolled, so lets recalc the hot
                        // spot
                        hotSpot.calculateTransformedBounds();
                        hotSpot.createHotSpots();
                    }
                    
                    hotSpot.render(g2d,hotSpotHit(hotSpot.getHotSpot()));
                }
            }
            catch (Throwable ignore) {

            }
        }
        else {
            if (theHighlightManager.getHighlightedElement()!=null) {
                theHighlightManager.getHighlightedElement()
                    .renderHighlight(g2d);
            }

            List<HighlightElement> clone = new ArrayList<HighlightElement>(theHighlightManager.getHighlights());
            
            for (HighlightElement element:clone) {
                element.renderHighlight(g2d);
            }
        }
    }

    /**
     * Check to see if the centre of the hot spot hits a locating line
     * 
     * @param hotSpot to check
     * @return true if the hot spot hits a locating line
     * @since 2.4
     */
    private boolean hotSpotHit(HotSpot hotSpot) {
        for (LocatingHotSpot locating:theLocatingHotSpots) {
            if (locating.isInHotSpot(hotSpot)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Notify the overlay that dragging is about to start
     * 
     * @since 2.4
     */
    public void startDragging() {
        List<Element> elements =
            SVGUtils.getSVGElements(theCanvas.getSVGDocument());

        for (Element element:elements) {
            if (!theHighlightManager.contains(element)) {
                // Its not one of the highlighted elements,
                // anchored text, because we don't
                // want one of them
                if (SVGUtils.isAnchorableText(element)) {
                    continue;
                }

                if (ConnectorHandler.isConnector(element)) {
                    Element node = ConnectorHandler.getStartNode(element);
                    if (theHighlightManager.contains(node)) {
                        continue;
                    }

                    node = ConnectorHandler.getEndNode(element);
                    if (theHighlightManager.contains(node)) {
                        continue;
                    }
                }

                // Nope, so lets create a hot spot
                try {
                    theHotSpots.add(new CentreHotSpot(theCanvas,element));
                }
                catch (DocfactoException ex) {
                    // The element can't have a hot spot, like markers
                }
            }
            else {
                // Its something that we want the positional hot spot for
                try {
                    theLocatingHotSpots.add(new LocatingHotSpot(theCanvas,
                        element));
                }
                catch (DocfactoException ignore) {
                    // Its already passed this test
                }
            }
        }
        isDragging = true;
    }

    /**
     * Notify the overlay that dragging has ended
     * 
     * @since 2.4
     */
    public void endDragging() {
        isDragging = false;
        theHotSpots.clear();
        theLocatingHotSpots.clear();
    }
}
