package com.docfacto.beermat.overlay;

import java.awt.Graphics2D;

/**
 * Beermat version of Overlays
 * 
 * @author dhudson - created 13 Dec 2013
 * @since 2.5
 */
public interface BeermatOverlay {

    /**
     * Draw overlay
     * 
     * @param g2d graphics
     * @param recalcHostSpots true if zoomed or scrolled since last paint
     * @since 2.5
     */
    public abstract void paint(Graphics2D g2d,boolean recalcHostSpots);

}
