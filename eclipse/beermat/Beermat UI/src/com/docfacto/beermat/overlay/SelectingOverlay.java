package com.docfacto.beermat.overlay;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.apache.batik.swing.gvt.JGVTComponent;
import org.eclipse.swt.graphics.Point;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.listeners.BeermatEventListener;
import com.docfacto.beermat.utils.BeermatUtils;

/**
 * This overlay is responsible for drawing the selecting box
 * 
 * @author dhudson - created 19 Aug 2013
 * @since 2.4
 */
public class SelectingOverlay implements BeermatOverlay,BeermatEventListener {

    private Rectangle2D theRect;
    private Point theStart;
    private Point theOrigin;
    
    private boolean isRequiringIntersect = false;

    private final BeermatController theController;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @since 2.5
     */
    public SelectingOverlay(BeermatController controller) {
        theController = controller;
        theController.addListener(this);
    }

    /**
     * Set the beginning of the mouse drag.
     * 
     * {@docfacto.note Set to null to reset}
     * 
     * @param start point the mouse drag was started
     * @since 2.4
     */
    public void setStartingPoint(Point start) {

        if (start==null) {
            // Reset
            theRect = null;
            isRequiringIntersect = false;
            theStart = null;
        }
        else {
            theStart = new Point(start.x,start.y);
            theOrigin = new Point(start.x,start.y);
        }
    }

    /**
     * The alt key was down, so intersect required
     * 
     * @since 2.4
     */
    public void setRequiringIntersect() {
        isRequiringIntersect = true;
    }

    /**
     * Returns true if intersect is required
     * 
     * @return true if intersect required
     * @since 2.4
     */
    public boolean isIntersetRequired() {
        return isRequiringIntersect;
    }

    /**
     * User still dragging, set the new x and y
     * 
     * @param event Beermat Mouse Event
     * @since 2.4
     */
    public void setNewPoint(MouseEvent event) {
        JGVTComponent c = (JGVTComponent)event.getSource();

        int xCurrent =  event.getX();
        int yCurrent =  event.getY();

        if (theRect!=null) {
            // Need to take into account the stroke of the highlight
            // Repaint the area to get rid of the old box
            c.repaint(BeermatUtils.outset(theRect.getBounds(),2));
        }

        // Constrain rectangle to window's Aspect Ratio.
        int xMin, yMin, width, height;
        if (theStart.x<xCurrent) {
            xMin = theStart.x;
            width = xCurrent-theStart.x;
        }
        else {
            xMin = xCurrent;
            width = theStart.x-xCurrent;
        }
        if (theStart.y<yCurrent) {
            yMin = theStart.y;
            height = yCurrent-theStart.y;
        }
        else {
            yMin = yCurrent;
            height = theStart.y-yCurrent;
        }

        if (theRect==null) {
            theRect = new Rectangle2D.Double(xMin,yMin,width,height);
        }
        else {
            theRect.setRect(xMin,yMin,width,height);
        }

        Graphics2D g2d = (Graphics2D)c.getGraphics();
        paint(g2d,false);
    }

    /**
     * Get the selected bounds of the highlight
     * 
     * @return the selected bounds, or null
     * @since 2.4
     */
    public Rectangle getSelectedBounds() {
        if (theRect!=null) {
            return theRect.getBounds();
        }

        return null;
    }

    /**
     * @see com.docfacto.beermat.overlay.BeermatOverlay#paint(java.awt.Graphics2D,
     * boolean)
     */
    @Override
    public void paint(Graphics2D g2d,boolean recalcHostSpots) {

        if (theRect!=null) {
            g2d.setXORMode(Color.white);
            g2d.setColor(Color.red);
            g2d.setStroke(BeermatUtils.HIGHLIGHT_STROKE);

            g2d.draw(theRect);
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.BeermatEventListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    @Override
    public void handleEvent(BeermatEvent event) {
        if (theRect!=null) {
            if (event.getAction()==BeermatEvent.SCROLL_ACTION) {
                Point2D transformedPoint;
                    transformedPoint = theController.getViewBoxTransform().transform(
                        new Point2D.Double(theOrigin.x,theOrigin.y),null);
                    theStart =
                    new Point((int)transformedPoint.getX(),
                        (int)transformedPoint.getY());
            }
        }
    }
}
