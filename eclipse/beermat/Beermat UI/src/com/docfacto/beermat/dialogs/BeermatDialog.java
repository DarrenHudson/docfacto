package com.docfacto.beermat.dialogs;

import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.docfacto.beermat.ui.BeermatViewConstants;
import com.docfacto.core.widgets.AbstractDocfactoDialog;

/**
 * A beermat dialog.
 * {@docfacto.note All dialog popups in beermat should extend this Dialog}
 * 
 * @author kporter - created Oct 14, 2013
 * @since 2.4.7
 */
public class BeermatDialog extends AbstractDocfactoDialog {

    private String theTitle;

    /**
     * Constructs a new beermat dialog
     * 
     * @param shell The parent
     * @param dialogTitle the title text display in the title bar
     * @since 2.4.7
     */
    public BeermatDialog(Shell shell,String dialogTitle) {
        super(shell);
        theTitle = dialogTitle;
    }

    /**
     * @see com.docfacto.core.widgets.AbstractDocfactoDialog#getDialogTitle()
     */
    @Override
    protected String getDialogTitle() {
        return theTitle;
    }

    /**
     * @see com.docfacto.core.widgets.AbstractDocfactoDialog#getBrandColor()
     */
    @Override
    public Color getBrandColor() {
        return BeermatViewConstants.COLOR_FOREGROUND_BRAND;
    }

    /**
     * @see com.docfacto.core.widgets.AbstractDocfactoDialog#getBrandComplementColor()
     */
    @Override
    public Color getBrandComplementColor() {
        return BeermatViewConstants.COLOR_FOREGROUND_COMPLEMENTARY;
    }

    /**
     * Traps the return key.
     * 
     * When the user presses return {@code okPressed} is called
     * 
     * {@docfacto.note used for connector annotations}
     * @param text to add the listener to
     * @since 2.5
     */
    public void addKeyboardListener(Text text) {
        text.addKeyListener(new KeyAdapter() {
            /**
             * @see org.eclipse.swt.events.KeyListener#keyReleased(org.eclipse.swt.events.KeyEvent)
             */
            @Override
            public void keyReleased(KeyEvent e) {
                if(e.keyCode == 13) {
                    okPressed();
                }
            }
        });        
    }
}
