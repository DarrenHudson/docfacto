package com.docfacto.beermat.dialogs;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * Allows for simple setting of attributes on the SVG.
 * 
 * If the values are set, then a combo of possible values are shown
 * 
 * @author dhudson - created 9 Dec 2013
 * @since 2.5
 */
public abstract class AbstractAttributeDialog extends BeermatDialog {

    private Text theText;

    private String theAttributeValue;

    private List<String> theValues;

    private Combo theCombo;

    /**
     * Constructor.
     * 
     * @param shell parent shell
     * @param dialogTitle title
     * @since 2.5
     */
    public AbstractAttributeDialog(Shell shell,String dialogTitle) {
        super(shell,dialogTitle);
    }

    /**
     * @see com.docfacto.core.widgets.AbstractDocfactoDialog#createDialogArea(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createDialogArea(Composite parent) {
        Composite container = (Composite)super.createDialogArea(parent);
        GridLayout gridLayout = (GridLayout)container.getLayout();
        final ColorDialog c = new ColorDialog(getShell());
        c.getParent().setBackground(getBackgroundMain());

        gridLayout.verticalSpacing = 12;
        gridLayout.numColumns = 1;
        gridLayout.marginBottom = 5;
        gridLayout.marginTop = 10;
        gridLayout.numColumns = 2;

        GridData gd_text = new GridData(SWT.CENTER,SWT.CENTER,true,false,2,1);
        gd_text.widthHint = 100;

        theText = new Text(container,SWT.BORDER);
        theText.setLayoutData(gd_text);

        if (theAttributeValue!=null) {
            theText.setText(theAttributeValue);
            theText.setSelection(theText.getText().length());
        }

        if (theValues!=null||theValues.size()>0) {

            theCombo = new Combo(container,SWT.NONE);
            GridData comboData =
                new GridData(SWT.CENTER,SWT.CENTER,true,false,2,1);
            comboData.widthHint = 120;

            theCombo.setLayoutData(comboData);
            for (String className:theValues) {
                theCombo.add(className);
            }

            theCombo.addSelectionListener(new SelectionListener() {

                /**
                 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
                 */
                @Override
                public void widgetSelected(SelectionEvent e) {
                    theText.setText(theCombo.getText());
                }

                /**
                 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
                 */
                @Override
                public void widgetDefaultSelected(SelectionEvent e) {
                }

            });

            // Empty the values for next time as this is a shared dialog
            theValues = null;
        }

        addKeyboardListener(theText);

        return container;
    }

    /**
     * @see org.eclipse.jface.dialogs.Dialog#okPressed()
     */
    @Override
    protected void okPressed() {
        theAttributeValue = theText.getText();
        super.okPressed();
    }

    /**
     * Set the current value of the attribute foe the user to edit
     * 
     * @param value current value of the attribute
     * @since 2.5
     */
    public void setAttributeValue(String value) {
        theAttributeValue = value;
    }

    /**
     * Gets the new vaue of the attribute
     * 
     * @return the value of the attribute
     * @since 2.5
     */
    public String getAttributeValue() {
        return theAttributeValue;
    }

    /**
     * Set the current values
     * 
     * @param values to display in the combo box
     * @since 2.5
     */
    public void setValues(List<String> values) {
        theValues = values;
    }
}
