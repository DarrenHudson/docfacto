package com.docfacto.beermat.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.docfacto.beermat.ui.BeermatViewConstants;
import com.docfacto.core.widgets.FileBrowserWidget;
import com.docfacto.core.widgets.listeners.FileTypeSelectionValidator;

/**
 * A CSS Browser Dialog, which allows users to select a css file
 * 
 * @author dhudson - created 10 Sep 2013
 * @since 2.4
 */
public class CSSBrowserDialog extends BeermatDialog {

    private static final String[] EXTENSIONS = {"*.css"};

    private FileBrowserWidget theFileBrowser;
    private String theBasePath = null;
    private String thePath;

    /**
     * Constructor.
     * 
     * @param parent shell
     * @since 2.4
     */
    public CSSBrowserDialog(Shell parent) {
        super(parent, "Select a CSS file");
    }

    /**
     * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createDialogArea(final Composite parent) {
        Composite container = (Composite)super.createDialogArea(parent);
        container.getShell().setText("Insert CSS");

        GridLayout gridLayout = (GridLayout)container.getLayout();
        gridLayout.numColumns = 2;

        Label lblImageLocation = new Label(container, SWT.NONE);
        lblImageLocation.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        lblImageLocation.setForeground(getBrandColor());
        lblImageLocation.setText("CSS Location");

        // Give the widget some room
        GridData gd = new GridData();
        gd.widthHint = 300;
        gd.verticalAlignment = SWT.CENTER;
        gd.horizontalAlignment = SWT.FILL;
        gd.grabExcessHorizontalSpace = true;

        theFileBrowser = new FileBrowserWidget(container, SWT.NONE, "", theBasePath, EXTENSIONS);
        theFileBrowser.setLayoutData(gd);
        theFileBrowser.setBackground(getBackgroundMain());
        theFileBrowser.setInternalValidator(new FileTypeSelectionValidator(new String[] {"css"}, "Valid css file",
            "Choose a valid css file"));
        theFileBrowser.setBrandColor(BeermatViewConstants.COLOR_FOREGROUND_BRAND);

        return container;
    }

    /**
     * @see org.eclipse.jface.dialogs.Dialog#okPressed()
     */
    @Override
    protected void okPressed() {
        thePath = theFileBrowser.getSelectedPath();
        super.okPressed();
    }

    /**
     * Sets the base path.
     * 
     * @param path The base path to set.
     * @since 2.4.2
     */
    public void setBasePath(String path) {
        theBasePath = path;
    }

    /**
     * The selected file path
     * 
     * @return the selected file path
     * @since 2.4
     */
    public String getPath() {
        return thePath;
    }
}
