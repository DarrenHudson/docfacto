package com.docfacto.beermat.dialogs;

import org.eclipse.swt.widgets.Shell;

/**
 * Allows the user to set the filter attribute
 * 
 * @author dhudson - created 9 Dec 2013
 * @since 2.5
 */
public class FilterAttributeDialog extends AbstractAttributeDialog {

    /**
     * Constructor.
     * 
     * @param shell parent
     * @since 2.5
     */
    public FilterAttributeDialog(Shell shell) {
        super(shell,"Add / Modify filter attribute");
    }

    /**
     * @see com.docfacto.beermat.dialogs.AbstractAttributeDialog#setAttributeValue(java.lang.String)
     */
    @Override
    public void setAttributeValue(String value) {
        // Remove the url stuff from the attribute
        if (value.startsWith("url(#")) {
            String id = value.substring(5,value.length()-1);
            super.setAttributeValue(id);
        }
        else {
            super.setAttributeValue(value);
        }
    }

}
