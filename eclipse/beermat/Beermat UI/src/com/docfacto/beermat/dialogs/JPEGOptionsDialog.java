package com.docfacto.beermat.dialogs;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.docfacto.beermat.ui.BeermatViewConstants;

public class JPEGOptionsDialog extends AbstractRasterDialog {
    private final static int DEFAULT_QUALITY = 8;
    
    private Text theScaleText;
    private Scale theScale;
    private int theQuality;
   

    public JPEGOptionsDialog(Shell shell) {
        super(shell,"JPEG Options");
    }

    @Override
    protected Control createDialogArea(Composite parent) {
        Composite container = (Composite)super.createDialogArea(parent);

        // getSeparator(container, 5);
        Label sep = new Label(container,SWT.NONE);
        sep.setBackground(BeermatViewConstants.COLOR_SEPARATOR);
        GridData gd = new GridData(SWT.FILL,SWT.CENTER,true,false,4,1);
        gd.heightHint = 1;
        gd.verticalIndent = 5;
        sep.setLayoutData(gd);

        super.getSizeArea(container);
        super.getSeparator(container);
        super.getSeparator(container,10);

        Label lblQuality = new Label(container,SWT.NONE);
        GridData gd_lblQuality = new GridData(SWT.LEFT,SWT.CENTER,false,false,4,1);
        gd_lblQuality.horizontalIndent = 5;
        lblQuality.setLayoutData(gd_lblQuality);
        lblQuality.setText("Quality");

        theScale = new Scale(container,SWT.NONE);
        GridData gd_scale = new GridData(SWT.LEFT,SWT.CENTER,false,false,1,1);
        gd_scale.horizontalIndent = 10;
        theScale.setLayoutData(gd_scale);
        theScale.setMaximum(10);
        theScale.setMinimum(1);
        theScale.setSelection(DEFAULT_QUALITY);
        theScale.setIncrement(1);
        theScale.setPageIncrement(1);

        theScale.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                theQuality = theScale.getSelection();
                theScaleText.setText(Integer.toString(theScale.getSelection()));
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });

        theScaleText = new Text(container,SWT.BORDER);
        theScaleText.setEditable(false);
        GridData gd_theScaleText = new GridData(SWT.FILL,SWT.CENTER,false,false,1,1);
        gd_theScaleText.widthHint = 25;
        theScaleText.setLayoutData(gd_theScaleText);
        
        theScaleText.setText(Integer.toString(DEFAULT_QUALITY));
        theQuality = DEFAULT_QUALITY;
        
        super.getSeparator(container,2);

        return container;
    }
    
    public int getQuality(){
        return theQuality;
    }

    public static void main(String[] args) {
        try {
            Display display = Display.getDefault();
            Shell shell = new Shell(display);
            JPEGOptionsDialog d = new JPEGOptionsDialog(shell);
            d.setOriginalSize(1900,1200);
            if (d.open()==Window.OK) {

            }

            while (!shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                    display.sleep();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}
