package com.docfacto.beermat.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.docfacto.beermat.ui.BeermatViewConstants;
import com.docfacto.core.utils.SWTUtils;
import com.docfacto.core.widgets.listeners.NumericVerifyListener;

public abstract class AbstractRasterDialog extends BeermatDialog {
    private Text theWidthTxt;
    private Text theHeightTxt;
    private int theOriginalHeight =-1;
    private int theOriginalWidth =-1;
    private boolean isSizeModified;
    private Point theNewSize;
    
    /**
     * Constructor.
     * @see com.docfacto.beermat.dialogs.BeermatDialog
     */
    public AbstractRasterDialog(Shell shell,String dialogTitle) {
        super(shell, dialogTitle);
    }
    
    /**
     * Set an original size to appear in the size area.
     * 
     * @param width The width on the original size in size area if created
     * @param height The height on the original size in size area if created
     * @since 2.5
     */
    public void setOriginalSize(int width, int height){
        theOriginalWidth = width;
        theOriginalHeight = height;
    }
    
    /**
     * Check whether the input size is different from the original size.
     * @return true if the size is different to the original size
     * @since 2.5
     */
    public boolean getSizeModified(){
        return isSizeModified;
    }
    
    private boolean checkSizeModified(){
        int width = Integer.parseInt(theWidthTxt.getText());
        int height = Integer.parseInt(theHeightTxt.getText());
        
        return (theOriginalHeight!=height) & (theOriginalWidth!=width);
    }
    
    private Point getSize(){
        return new Point(Integer.parseInt(theWidthTxt.getText()), Integer.parseInt(theHeightTxt.getText()));
    }
    
    public Point getNewSize(){
        return theNewSize;
    }
    
    @Override
    protected void okPressed() {
        isSizeModified = checkSizeModified();
        theNewSize = getSize();
        super.okPressed();
    }
    
    @Override
    protected Control createDialogArea(Composite parent) {
        Composite c =(Composite) super.createDialogArea(parent);
        GridLayout gridLayout = (GridLayout) c.getLayout();
        gridLayout.verticalSpacing = 2;
        gridLayout.numColumns = 4;
        return c;
    }

    /**
     * Create a size area to show an original size and allow users to give a different output size.
     * The original size here is set to the size held by this dialog set with setOriginalSize(width,height)
     * 
     * @param parent The parent of the created composite
     * @return a Composite of a size area using the original width and height
     * @since 2.5
     * @see com.docfacto.beermat.dialogs.AbstractRasterDialog.setOriginalSize(int, int)
     */
    protected Composite getSizeArea(Composite parent){
        return getSizeArea(parent, theOriginalWidth, theOriginalHeight);
    }
    
    /**
     * Create a size area to show an original size and allow users to give a different output size.
     * The original size here is set to the parameters given to this method.
     * 
     * @param parent The parent of the size area
     * @param width The 'original' width to set
     * @param height The 'original' height to set
     * @return A size area.
     * @since 2.5
     */
    protected Composite getSizeArea(Composite parent,int width,int height) {
        Composite sizeArea = new Composite(parent, SWT.NONE);
        sizeArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 4, 1));
        sizeArea.setBackground(getBackgroundMain());
        
        GridLayout gl_sizeArea = new GridLayout(2, false);
        gl_sizeArea.horizontalSpacing = 3;
        gl_sizeArea.marginWidth = 3;
        gl_sizeArea.marginHeight = 0;
        gl_sizeArea.verticalSpacing = 0;
        sizeArea.setLayout(gl_sizeArea);
                
        Label lblSize = new Label(sizeArea, SWT.NONE);
        GridData gd_lblSize = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
        gd_lblSize.horizontalIndent = 3;
        lblSize.setLayoutData(gd_lblSize);
        lblSize.setSize(26, 14);
        lblSize.setText("Size");
        
        RGB bkg = new RGB(230,230,235);
        Color darkerBackground = new Color(this.getShell().getDisplay(), bkg);
        
        Composite newSizeComposite = new Composite(sizeArea, SWT.NONE);
        newSizeComposite.setBackground(darkerBackground);
        
        GridLayout gl_newSizeComposite = new GridLayout(3, false);
        gl_newSizeComposite.marginRight = 5;
        gl_newSizeComposite.marginTop = 5;
        gl_newSizeComposite.marginHeight = 0;
        gl_newSizeComposite.marginLeft = 10;
        gl_newSizeComposite.marginWidth = 0;
        gl_newSizeComposite.marginBottom = 2;
        newSizeComposite.setLayout(gl_newSizeComposite);
        
        Label lblNew = new Label(newSizeComposite, SWT.NONE);
        lblNew.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 3, 1));
        lblNew.setText("New");
        
        Label lblWidth = new Label(newSizeComposite, SWT.NONE);
        lblWidth.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblWidth.setText("Width");
        
        theWidthTxt = new Text(newSizeComposite, SWT.BORDER);
        theWidthTxt.addVerifyListener(new NumericVerifyListener());
        theWidthTxt.setBackground(SWTUtils.getColor(SWT.COLOR_WHITE));
        GridData gd_txtWidth = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        gd_txtWidth.widthHint = 43;
        theWidthTxt.setLayoutData(gd_txtWidth);
        
        Label lblPx = new Label(newSizeComposite, SWT.NONE);
        lblPx.setText("px");
        
        Label lblHeight = new Label(newSizeComposite, SWT.NONE);
        lblHeight.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblHeight.setText("Height");
        
        theHeightTxt = new Text(newSizeComposite, SWT.BORDER);
        theHeightTxt.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        theHeightTxt.setBackground(SWTUtils.getColor(SWT.COLOR_WHITE));
        theHeightTxt.addVerifyListener(new NumericVerifyListener());
        
        Label lblOx = new Label(newSizeComposite, SWT.NONE);
        lblOx.setText("px");
        
        Composite oldSizeComposite = new Composite(sizeArea, SWT.NONE);
        oldSizeComposite.setLayout(new GridLayout(2, false));
        oldSizeComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        oldSizeComposite.setBackground(darkerBackground);
        
        Label lblOriginal = new Label(oldSizeComposite, SWT.NONE);
        lblOriginal.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1));
        lblOriginal.setText("Original");
        
        Label lblW = new Label(oldSizeComposite, SWT.NONE);
        lblW.setText("W");
        
        Label oldWidth = new Label(oldSizeComposite, SWT.NONE);
        oldWidth.setText(Integer.toString(width));
        
        Label lblH = new Label(oldSizeComposite, SWT.NONE);
        lblH.setText("H");
        
        Label oldHeight = new Label(oldSizeComposite, SWT.NONE);
        oldHeight.setText(Integer.toString(height));
        
        theHeightTxt.setText(Integer.toString(height));
        theWidthTxt.setText(Integer.toString(width));
        
        return sizeArea;
    }
    
    /**
     * Creates a separator, 1 pixel high, which spans across the whole widget.
     * @param parent The parent composite of this separator.
     * @return A separator
     * @since 2.5
     */
    protected Label getSeparator(Composite parent){
        return getSeparator(parent,0);
    }
   
    /**
     * Creates a separator, 1 pixel high, which spans across the whole widget and has a vertical indent.
     * 
     * @param parent the parent composite
     * @param indent The vertical indent of this separator
     * @return A separator
     * @since 2.5
     */
    protected Label getSeparator(Composite parent, int indent){
        Label sep = new Label(parent, SWT.NONE);
        sep.setBackground(BeermatViewConstants.COLOR_SEPARATOR);
        GridData gd = new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1);
        gd.heightHint = 1;
        gd.verticalIndent = indent;
        sep.setLayoutData(gd);
        return sep;
    }

}