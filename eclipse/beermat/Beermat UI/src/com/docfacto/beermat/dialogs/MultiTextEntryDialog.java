package com.docfacto.beermat.dialogs;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.ui.BeermatViewConstants;
import com.docfacto.beermat.ui.widgets.TabbedTextComposite;
import com.docfacto.beermat.ui.widgets.data.TabData;

/**
 * Allow for Multi Language Text Entry
 * 
 * @author kporter - created Jul 1, 2013
 * @since 2.4
 */
public class MultiTextEntryDialog extends AbstractTextEntryDialog {

    private ArrayList<TabData> theTabs;
    private TabbedTextComposite theTextComposite;

    /**
     * Constructor.
     * 
     * @param parentShell The parent
     * @param controller beermat controller
     * @since 2.4
     */
    public MultiTextEntryDialog(Shell parentShell,BeermatController controller) {
        super(parentShell,controller);
        setReturnCode(OK);
    }

    /**
     * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createDialogArea(Composite parent) {
        parent
            .setBackground(BeermatViewConstants.COLOR_BACKGROUND);
        Composite container = (Composite)super.createDialogArea(parent);
        GridLayout gl_container = new GridLayout(2,false);
        gl_container.marginRight = 0;
        gl_container.marginLeft = 0;
        container.setLayout(gl_container);
        container
            .setBackground(BeermatViewConstants.COLOR_BACKGROUND);
        theTextComposite = new TabbedTextComposite(container,SWT.NONE);
        new Label(container,SWT.NONE);

        return container;
    }

    /**
     * @see org.eclipse.jface.dialogs.Dialog#okPressed()
     */
    @Override
    protected void okPressed() {
        theTabs = theTextComposite.getTabData();
        super.okPressed();
    }

    /**
     * Get the tab data list
     * 
     * @return The list of tabs
     * @since 2.4
     */
    public ArrayList<TabData> getTabData() {
        return theTabs;
    }

    /**
     * Set tabs to this entry
     * 
     * @param tabs The tabs to set to this text entry
     * @since 2.4
     */
    public void setTabData(ArrayList<TabData> tabs) {
        theTabs = tabs;
    }

    /**
     * @see com.docfacto.beermat.dialogs.AbstractTextEntryDialog#processText(org.w3c.dom.svg.SVGDocument,
     * org.eclipse.swt.graphics.Point)
     */
    @Override
    public boolean processText(SVGDocument document) {
//        Element fontGroup =
//            document.createElementNS(SVGUtils.SVG_NAMESPACE,"g");
//        // TODO: Change this line to use the new method of setting a translate.
//        // 'setTranslate'
//
//        setTranslateTranform(fontGroup,getController().getViewBoxTransform());
//
//        setFontAttributes(fontGroup);
//
//        Element switchGroup =
//            document.createElementNS(SVGUtils.SVG_NAMESPACE,"switch");
//
//        // Get the tabs, remove and store default tab as we do not want this
//        // sorted
//        ArrayList<TabData> tabs = getTabData();
//        TabData defaultTab = tabs.get(0);
//        tabs.remove(defaultTab);
//
//        // Sort the List in reverse order so the general locale is always last
//        // i.e. en_GB then en
//        Collections.sort(tabs,Collections.reverseOrder());
//
//        for (int i = 0;i<tabs.size();i++) {
//            TabData current = tabs.get(i);
//            Element localeGroup =
//                ElementUtils.createSystemLocaleGroup(document,
//                    current.getLocale());
//            addTextLines(document,localeGroup,current.getText());
//            switchGroup.appendChild(localeGroup);
//        }
//
//        // Element defText =
//        // ElementUtils.createText(document,defaultTab.getText());
//        // defaultGroup.appendChild(defText);
//        Element defaultGroup =
//            document.createElementNS(SVGUtils.SVG_NAMESPACE,"g");
//        addTextLines(document,defaultGroup,defaultTab.getText());
//
//        switchGroup.appendChild(defaultGroup);
//        fontGroup.appendChild(switchGroup);
//        document.getDocumentElement().appendChild(fontGroup);

        return true;
    }

    /**
     * @see com.docfacto.beermat.dialogs.AbstractTextEntryDialog#updateElement()
     */
    @Override
    public boolean updateElement() {
        return false;
    }
}
