package com.docfacto.beermat.dialogs;

import java.awt.geom.Point2D;

import org.apache.batik.dom.svg.SVGOMGElement;
import org.apache.batik.dom.svg.SVGOMTextElement;
import org.apache.batik.dom.svg.SVGOMTextPathElement;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.svg.ConnectorHandler;
import com.docfacto.beermat.svg.TextElement;
import com.docfacto.beermat.svg.TextHandler;
import com.docfacto.beermat.updaters.ComplexGroupTextUpdater;
import com.docfacto.beermat.updaters.ConnectorTextUpdater;
import com.docfacto.beermat.updaters.CreateConnectorTextUpdater;
import com.docfacto.beermat.updaters.CreateTextGroupUpdater;
import com.docfacto.beermat.updaters.CreateTextUpdater;
import com.docfacto.beermat.updaters.SimpleGroupTextUpdater;
import com.docfacto.beermat.updaters.TextUpdater;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.Platform;

/**
 * Simple Text Entry Dialog
 * 
 * @author dhudson - created 7 Aug 2013
 * @since 2.4
 */
public class SingleTextEntryDialog extends AbstractTextEntryDialog {

    private Text theTextArea;
    private String[] theLines;
    private boolean isInEditMode;

    /**
     * Constructor.
     * 
     * @param parentShell parent shell
     * @param controller beermat controller
     * @since 2.4
     */
    public SingleTextEntryDialog(Shell parentShell,BeermatController controller) {
        super(parentShell,controller);
    }

    /**
     * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createDialogArea(Composite parent) {

        Composite container = parent;
        GridData gd = new GridData(GridData.FILL,GridData.FILL,true,true);
        gd.widthHint = 200;
        gd.heightHint = 30;
        gd.horizontalAlignment = SWT.FILL;
        gd.grabExcessHorizontalSpace = true;

        // Removed SWT.WRAP
        theTextArea =
            new Text(container,SWT.BORDER|SWT.H_SCROLL|SWT.V_SCROLL|SWT.CANCEL|
                SWT.MULTI);
        theTextArea.setLayoutData(gd);
        theTextArea.addListener(SWT.KeyDown,new Listener() {

            /**
             * @see org.eclipse.swt.widgets.Listener#handleEvent(org.eclipse.swt.widgets.Event)
             */
            @Override
            public void handleEvent(Event event) {
                if ((event.stateMask&SWT.CTRL)!=0) {
                    if (event.keyCode==SWT.CR) {
                        // Consume the event
                        event.doit = false;
                        okPressed();
                    }
                }
            }

        });

        if (getElement()!=null) {
            // We have been supplied an element, it but it text

            // We are in edit mode
            if (getElement() instanceof SVGOMGElement) {
                // Loop through the group and pick out the text elements
                StringBuilder builder = new StringBuilder();
                NodeList children = getElement().getChildNodes();
                for (int i = 0;i<children.getLength();i++) {
                    Node child = children.item(i);
                    if (child instanceof SVGOMTextElement) {
                        SVGOMTextElement line = (SVGOMTextElement)child;
                        builder.append(new TextElement(line).getTextContent());
                        builder.append(Platform.LINE_SEPARATOR);
                    }
                }

                // remove the last LINE_SEPARATOR
                String content = builder.toString();
                if (content.endsWith(Platform.LINE_SEPARATOR)) {
                    content = content.substring(0,content.length()-1);
                }

                theTextArea.setText(content);
            }
            else if (getElement() instanceof SVGOMTextElement) {
                // Simple text element
                SVGOMTextElement textElement = (SVGOMTextElement)getElement();

                theTextArea.setText(new TextElement(textElement)
                    .getTextContent());

                if (TextHandler.getTextPath(getElement())!=null) {
                    // Editing a connector limit the input
                    addSingleTextEntryListener();
                }
            }

            isInEditMode = true;

            // Set the cursor at the end of the string
            theTextArea.setSelection(theTextArea.getText().length());
        }
        else {
            isInEditMode = false;

            if (ConnectorHandler.isConnector(getAnchorElement())) {
                addSingleTextEntryListener();
            }
        }

        return container;
    }

    private void addSingleTextEntryListener() {
        theTextArea.addKeyListener(new KeyAdapter() {
            private final long startTime = System.currentTimeMillis();

            /**
             * @see org.eclipse.swt.events.KeyListener#keyReleased(org.eclipse.swt.events.KeyEvent)
             */
            @Override
            public void keyReleased(KeyEvent e) {
                // For some reason if the return key is pressed to
                // open the dialog, AWT and SWT get it ..
                // This stops the dialog opening and closing again.
                if ((System.currentTimeMillis()-startTime)<1000) {
                    e.doit = false;
                   return;
                }

                if (e.keyCode==13) {
                    okPressed();
                }
            }
        });
    }

    /**
     * @see org.eclipse.jface.dialogs.Dialog#okPressed()
     */
    @Override
    protected void okPressed() {
        theLines = getLinesFromText(theTextArea);
        super.okPressed();
    }

    /**
     * @see com.docfacto.beermat.dialogs.AbstractTextEntryDialog#processText(org.w3c.dom.svg.SVGDocument,
     * org.eclipse.swt.graphics.Point)
     */
    @Override
    public boolean processText(final SVGDocument document) {

        if (isInEditMode) {
            return updateElement();
        }
        else {
            return createElement(document);
        }
    }

    /**
     * Create a text element if required
     * 
     * @param document to modify
     * @return true if created an element
     * @since 2.4
     */
    public boolean createElement(final SVGDocument document) {
        // Nothing to do
        if (theLines.length==0) {
            return false;
        }

        if (ConnectorHandler.isConnector(getAnchorElement())) {
            if (!theLines[0].trim().isEmpty()) {
                new CreateConnectorTextUpdater(getController(),theLines,
                    getHotSpot(),getAnchorElement());
                return true;
            }
            else {
                return false;
            }

        }

        if (theLines.length==1) {
            if (!theLines[0].trim().isEmpty()) {
                new CreateTextUpdater(getController(),theLines,getHotSpot(),
                    getAnchorElement(),getLocation());
            }
            else {
                return false;
            }
        }
        else {
            new CreateTextGroupUpdater(getController(),theLines,getHotSpot(),
                getAnchorElement(),getLocation());
        }

        return true;
    }

    /**
     * @see com.docfacto.beermat.dialogs.AbstractTextEntryDialog#updateElement()
     */
    @Override
    public boolean updateElement() {
        if (getElement() instanceof SVGOMTextElement) {
            SVGOMTextPathElement textPath =
                TextHandler.getTextPath(getElement());

            // We are dealing with TextPath
            if (textPath!=null) {
                new ConnectorTextUpdater(getController(),textPath,theLines);
                return true;
            }

            if (theLines.length==1&&getElement() instanceof SVGOMTextElement) {
                TextElement wrapper =
                    new TextElement((SVGOMTextElement)getElement());
                new TextUpdater(getController(),wrapper,theLines[0]);
                return true;
            }

            final Document document = getElement().getOwnerDocument();

            // Deal with group text
            final Element group;
            final boolean hadGroup;
            if (getElement().getParentNode() instanceof SVGOMGElement) {
                group = (Element)getElement().getParentNode();
                hadGroup = true;
            }
            else {
                group = document.createElementNS(SVGUtils.SVG_NAMESPACE,"g");
                group.setAttribute("class","textGroup");
                String x =
                    getElement().getAttribute("x").isEmpty() ? "0"
                        : getElement().getAttribute("x");
                String y =
                    getElement().getAttribute("y").isEmpty() ? "0"
                        : getElement().getAttribute("y");
                
                SVGUtils
                    .setTransformTranslate(
                        group,
                        new Point2D.Double(Float.parseFloat(x),Float
                            .parseFloat(y)));

                // group.setAttribute("transform","translate("+x+","+y+")");

                if (SVGUtils.hasDocfactoAttribute(getElement(),
                    TextHandler.ANCHOR_ATTRIBUTE)) {
                    // Copy anchor attributes
                    SVGUtils.setDocfactoAttribute(group,
                        TextHandler.ANCHOR_ATTRIBUTE,
                        SVGUtils.getDocfactoAttribute(getElement(),
                            TextHandler.ANCHOR_ATTRIBUTE));
                    SVGUtils.setDocfactoAttribute(group,
                        TextHandler.HOTSPOT_ATTRIBUTE,
                        SVGUtils.getDocfactoAttribute(getElement(),
                            TextHandler.HOTSPOT_ATTRIBUTE));
                    group.setAttribute("text-anchor",
                        getElement().getAttribute("text-anchor"));
                }
                hadGroup = false;
            }

            new ComplexGroupTextUpdater(getController(),theLines,group,
                hadGroup,getElement());
            return true;

        }
        else if (getElement() instanceof SVGOMGElement) {
            new SimpleGroupTextUpdater(getController(),
                (SVGOMGElement)getElement(),theLines);
        }

        return false;
    }

}