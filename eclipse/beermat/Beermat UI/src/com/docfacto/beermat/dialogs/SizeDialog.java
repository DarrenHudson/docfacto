package com.docfacto.beermat.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import com.docfacto.beermat.ui.BeermatViewConstants;
import com.docfacto.beermat.ui.widgets.SizeSelectionWidget;
import com.docfacto.common.DocfactoException;

/**
 * A dialog to select a size.
 * 
 * @author kporter - created Jul 29, 2013
 * @since 2.4.2
 */
public class SizeDialog extends BeermatDialog {

    private SizeSelectionWidget theWidget;
    private Point theSize;
    private Point thePreviousSize;

    /**
     * Create the dialog.
     * 
     * @param parentShell the parent
     * @since 2.4.2
     */
    public SizeDialog(Shell parentShell) {
        super(parentShell,"Set SVG Size");
    }

    /**
     * @see com.docfacto.core.widgets.AbstractDocfactoDialog#createDialogArea(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createDialogArea(Composite parent) {
        Composite container = (Composite)super.createDialogArea(parent);

        theWidget = new SizeSelectionWidget(container,SWT.NONE);
        theWidget.setSVGSize(thePreviousSize);

        GridData gd_theWidget = new GridData(SWT.FILL,SWT.FILL,false,false,1,1);
        theWidget.setLayoutData(gd_theWidget);

        theWidget.setForeground(getBrandColor());
        theWidget.setBackground(getBackgroundMain());
        theWidget.setColourToLabels(BeermatViewConstants.COLOR_FOREGROUND_BRAND);
        return container;
    }

    /**
     * Gets the set size
     * 
     * @return The size
     * @throws DocfactoException if size is invalid
     * @since 2.4
     */
    public Point getSize() throws DocfactoException {
        theSize =
            new Point(theWidget.getWidthAsPixels(),
                theWidget.getHeightAsPixels());
        if (theSize.x<=0||theSize.y<=0) {
            throw new DocfactoException("SVG Size cannot be 0 or less");
        }
        else {
            return theSize;
        }
    }

    /**
     * Sets a size which will appear in the widget.
     * 
     * @param size The size to set
     * @since 2.4.3
     */
    public void setSVGSize(Point size) {
        thePreviousSize = size;
    }
}
