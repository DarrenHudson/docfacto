package com.docfacto.beermat.dialogs;

import org.eclipse.swt.widgets.Shell;

/**
 * Allow users to assign a class to selected items
 * 
 * @author dhudson - created 6 Dec 2013
 * @since 2.5
 */
public class ClassAttributeDialog extends AbstractAttributeDialog  {

    /**
     * Constructor.
     * 
     * @param shell parent shell
     * @since 2.5
     */
    public ClassAttributeDialog(Shell shell) {
        super(shell,"Add / Modify class attribute");
    }

}
