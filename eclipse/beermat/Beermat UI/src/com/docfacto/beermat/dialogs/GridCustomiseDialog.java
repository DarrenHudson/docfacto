package com.docfacto.beermat.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.docfacto.beermat.ui.BeermatViewConstants;
import com.docfacto.core.widgets.listeners.NumericVerifyListener;

/**
 * A dialog to customise the grid.
 * 
 * @author kporter - created Aug 16, 2013
 * @since 2.4.3
 */
public class GridCustomiseDialog extends BeermatDialog {
    private final Color FOREGROUND = getBrandColor();

    private Text text;
    private int theSize = -1;
    private Color theColour;

    /**
     * Constructor.
     * 
     * @param parentShell The parent shell
     * @since 2.4
     */
    public GridCustomiseDialog(Shell parentShell) {
        super(parentShell,"Customize the Grid");
    }

    /**
     * @see com.docfacto.core.widgets.AbstractDocfactoDialog#createDialogArea(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createDialogArea(Composite parent) {
        Composite container = (Composite)super.createDialogArea(parent);
        GridLayout gridLayout = (GridLayout)container.getLayout();
        final ColorDialog c = new ColorDialog(getShell());
        c.getParent().setBackground(getBackgroundMain());

        gridLayout.verticalSpacing = 12;
        gridLayout.numColumns = 2;
        gridLayout.marginBottom = 5;
        gridLayout.marginTop = 10;

        Label lblSize = new Label(container,SWT.NONE);
        lblSize
            .setLayoutData(new GridData(SWT.RIGHT,SWT.CENTER,false,false,1,1));
        lblSize.setText("Grid size");
        lblSize.setForeground(FOREGROUND);

        GridData gd_text = new GridData(SWT.RIGHT,SWT.CENTER,false,false,1,1);
        gd_text.widthHint = 53;
        text = new Text(container,SWT.BORDER);
        text.setLayoutData(gd_text);
        if (theSize>0) {
            text.setText(Integer.toString(theSize));
            text.selectAll();
        }
        text.addVerifyListener(new NumericVerifyListener());
        text.addModifyListener(new ModifyListener() {

            @Override
            public void modifyText(ModifyEvent e) {
                try {
                    theSize = Integer.parseInt(text.getText());
                }
                catch (NumberFormatException nfe) {
                    // Do nothing
                }
            }
        });

        Label lblColour = new Label(container,SWT.NONE);
        lblColour.setText("Grid Colour");
        lblColour.setForeground(FOREGROUND);

        GridData gd_label = new GridData(SWT.RIGHT,SWT.FILL,false,false,1,1);
        gd_label.widthHint = 58;
        final Label colour = new Label(container,SWT.BORDER);
        colour.setLayoutData(gd_label);

        if (theColour!=null) {
            colour.setBackground(theColour);
            c.setRGB(theColour.getRGB());
        }
        else {
            colour.setBackground(BeermatViewConstants.BEERMAT_GRID_COLOR);
            c.setRGB(BeermatViewConstants.BEERMAT_GRID_COLOR.getRGB());
        }

        colour.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseDown(MouseEvent e) {
                RGB rgb = c.open();
                if (rgb!=null) {
                    theColour = new Color(getShell().getDisplay(),rgb);
                    colour.setBackground(theColour);
                }
            }
        });

        return container;
    }

    /**
     * Sets the grid size
     * 
     * @param i The size to set
     * @since 2.4.3
     */
    public void setGridSize(int i) {
        theSize = i;
    }

    /**
     * Get the grid size which was set.
     * 
     * @return The grid size
     * @since 2.4
     */
    public int getGridSize() {
        return theSize;
    }

    /**
     * Returns colour.
     * 
     * @return the colour
     * @since 2.4
     */
    public Color getColour() {
        return theColour;
    }

    /**
     * Sets colour.
     * 
     * @param colour the colour value
     * @since 2.4
     */
    public void setColour(Color colour) {
        theColour = colour;
    }

}
