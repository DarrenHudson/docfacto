package com.docfacto.beermat.dialogs;

import java.awt.geom.Point2D;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.hotspot.HotSpot;
import com.docfacto.common.Platform;

/**
 * Abstract class for Text Entry
 * 
 * @author dhudson - created 8 Aug 2013
 * @since 2.4
 */
public abstract class AbstractTextEntryDialog extends BeermatDialog {

    private final BeermatController theController;
    private Point2D theLocation;
    private Element theElement;
    private HotSpot theHotSpot;
    private Element theAnchorElement;

    /**
     * Constructor.
     * 
     * @param parentShell parent shell
     * @param controller beermat controller
     * @since 2.4
     */
    public AbstractTextEntryDialog(Shell parentShell,
    BeermatController controller) {
        super(parentShell,"Insert Text on canvas");
        theController = controller;
    }

    /**
     * Get the Beermat controller
     * 
     * @return the beermat controller
     * @since 2.4
     */
    public BeermatController getController() {
        return theController;
    }

    /**
     * Set the location of the mouse click.
     * 
     * This will set the location of the shell, so this needs to be called before {@code open}
     * 
     * @param x of the mouse click
     * @param y of the mouse click
     * @since 2.4
     */
    public void setLocation(int x,int y) {
        theLocation = new Point2D.Double(x,y);
    }

    /**
     * If a hot spot is set, then alignment information can be calculated
     * 
     * @param hotSpot for alignment information, or can be null
     * @since 2.4
     */
    public void setHotSpot(HotSpot hotSpot) {
        theHotSpot = hotSpot;
    }

    /**
     * Return the hot spot
     * 
     * @return the hot spot or null
     * @since 2.4
     */
    public HotSpot getHotSpot() {
        return theHotSpot;
    }

    /**
     * Set the anchor element
     * 
     * @param anchorElement to which the text will be anchored to
     * @since 2.4
     */
    public void setAnchorElement(Element anchorElement) {
        theAnchorElement = anchorElement;
    }

    /**
     * Return the anchor element or null if not set
     * 
     * @return the anchor element or null
     * @since 2.4
     */
    public Element getAnchorElement() {
        return theAnchorElement;
    }

    /**
     * Get mouse click location
     * 
     * @return the mouse click location
     * @since 2.4
     */
    public Point2D getLocation() {
        return theLocation;
    }

    /**
     * Sets the element to update
     * 
     * @param element to update
     * @since 2.4
     */
    public void setElement(Element element) {
        theElement = element;
    }

    /**
     * The element to update
     * 
     * @return the element if set
     * @since 2.4
     */
    public Element getElement() {
        return theElement;
    }

    /**
     * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
     */
    protected void configureShell(Shell shell) {
        super.configureShell(shell);
        shell.setText("Text Entry");
    }

    /**
     * Get the text from the text control and then build an array out of it
     * 
     * @param text to process
     * @return an array of strings
     * @since 2.4
     */
    public String[] getLinesFromText(Text text) {
        return text.getText().split(Platform.LINE_SEPARATOR);
    }

    /**
     * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent,IDialogConstants.OK_ID,IDialogConstants.OK_LABEL,
            true);
        createButton(parent,IDialogConstants.CANCEL_ID,
            IDialogConstants.CANCEL_LABEL,false);
    }

    /**
     * @see org.eclipse.jface.window.Window#constrainShellSize()
     */
    @Override
    protected void constrainShellSize() {
        super.constrainShellSize();

        Composite parent = getShell().getParent();

        Monitor monitor = getShell().getDisplay().getPrimaryMonitor();
        if (parent!=null) {
            monitor = parent.getMonitor();
        }

        Rectangle monitorBounds = monitor.getClientArea();

        Point newLocation = new Point(monitorBounds.x,monitorBounds.y);

        Rectangle bounds = getShell().getBounds();

        if (bounds!=null) {
            newLocation.x += getLocation().getX()+40+bounds.width;
            newLocation.y += getLocation().getY()+bounds.height;
        }

        // Using the mouse location, display the dialog on the correct monitor
        // and in the correct position
        getShell().setLocation(newLocation);
    }

    /**
     * @see com.docfacto.beermat.dialogs.BeermatDialog#addKeyboardListener(org.eclipse.swt.widgets.Text)
     */
    @Override
    public void addKeyboardListener(Text text) {
        // Do Nothing..
    }

    /**
     * @see org.eclipse.jface.window.Window#getShellStyle()
     */
    @Override
    protected int getShellStyle() {
        return super.getShellStyle()|SWT.RESIZE;
    }

    /**
     * At this point the user has pressed OK and the text should be processed
     * 
     * @param document to append text to
     * @return true if text added to the document
     * @since 2.4
     */
    public abstract boolean processText(SVGDocument document);

    /**
     * Update an existing element
     * 
     * @return true if the element was updates
     * @since 2.4
     */
    public abstract boolean updateElement();

}
