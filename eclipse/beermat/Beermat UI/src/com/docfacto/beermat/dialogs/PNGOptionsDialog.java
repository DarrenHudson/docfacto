package com.docfacto.beermat.dialogs;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.docfacto.core.utils.SWTUtils;

/**
 */
public class PNGOptionsDialog extends AbstractRasterDialog {

    private Label theMatteLabel;
    private Combo theComboMatte;

    private RGB theMatteColour;

    private boolean theTransparency;
    private ColorDialog theColourDialog;

    /**
     * Create the dialog.
     * 
     * @param parentShell
     */
    public PNGOptionsDialog(Shell parentShell) {
        super(parentShell, "PNG Options");
        theColourDialog = new ColorDialog(parentShell);
        theColourDialog.getParent().setBackground(getBackgroundMain());
    }

    /**
     * Create contents of the dialog.
     * 
     * @param parent
     */
    @Override
    protected Control createDialogArea(Composite parent) {
        Composite container = (Composite)super.createDialogArea(parent);
        
        getSeparator(container, 5);
        
        final Button btnTransparency = new Button(container, SWT.CHECK);
        GridData gd_btnTransparency = new GridData(SWT.LEFT, SWT.CENTER, false, false, 4, 1);
        gd_btnTransparency.horizontalIndent = 5;
        gd_btnTransparency.verticalIndent = 2;
        btnTransparency.setLayoutData(gd_btnTransparency);
        btnTransparency.setSelection(true);
        btnTransparency.setText("Transparency");
        btnTransparency.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                switchEnabledMatte();
                theTransparency = btnTransparency.getSelection();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
        Label spacer = new Label(container, SWT.NONE);
        GridData gd_spacer = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_spacer.horizontalIndent = 30;
        spacer.setLayoutData(gd_spacer);

        Label matte = new Label(container, SWT.NONE);
        matte.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        matte.setText("Matte");

        theComboMatte = new Combo(container, SWT.READ_ONLY);
        theComboMatte.setItems(new String[] {"White","Black","Other"});
        theComboMatte.setVisibleItemCount(3);
        theComboMatte.select(INDEX_MATTE_COMBO_WHITE);
        theComboMatte.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                switch (theComboMatte.getSelectionIndex()) {
                case INDEX_MATTE_COMBO_WHITE:
                    setMatteColour(new RGB(255, 255, 255));
                    break;
                case INDEX_MATTE_COMBO_BLACK:
                    setMatteColour(new RGB(0, 0, 0));
                    break;
                case INDEX_MATTE_COMBO_OTHER:
                    theColourDialog.open();
                    RGB rgb = theColourDialog.getRGB();
                    if (rgb!=null) {
                        setMatteColour(rgb);
                    }
                    break;
                }
            }
        });
        
        GridData gd_combo = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
        gd_combo.widthHint = 75;
        theComboMatte.setLayoutData(gd_combo);
        
        Composite colourComposite = new Composite(container, SWT.NONE);
        colourComposite.setBackground(SWTUtils.getColor(SWT.COLOR_BLACK));
        FillLayout fl_colourComposite = new FillLayout(SWT.HORIZONTAL);
        fl_colourComposite.marginWidth = 1;
        fl_colourComposite.marginHeight = 1;
        colourComposite.setLayout(fl_colourComposite);
        GridData gd_colourComposite = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_colourComposite.heightHint = 16;
        gd_colourComposite.widthHint = 16;
        colourComposite.setLayoutData(gd_colourComposite);

        theMatteLabel = new Label(colourComposite, SWT.NONE);
        theMatteLabel.setBackground(SWTUtils.getColor(SWT.COLOR_WHITE));

        theMatteLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseDown(MouseEvent e) {
                theColourDialog.open();
                RGB rgb = theColourDialog.getRGB();
                if (rgb!=null) {
                    setMatteColour(rgb);
                    theComboMatte.select(2);
                }
            }
        });

        getSeparator(container);

        getSeparator(container, 10);
        
        getSizeArea(container);
        
        getSeparator(container, 2);
        
        setEnabledMatte(false);
        theTransparency = true;
        return container;
    }

    private void setEnabledMatte(boolean enable) {
        if (!enable) {
            theMatteLabel.setForeground(SWTUtils.getColor(130, 130, 130));
            theComboMatte.setEnabled(false);
        }
        else {
            theMatteLabel.setForeground(SWTUtils.getColor(SWT.COLOR_BLACK));
            theComboMatte.setEnabled(true);
        }
    }

    private void switchEnabledMatte() {
        setEnabledMatte(!theComboMatte.getEnabled());
    }

    private void setMatteColour(RGB rgb) {
        theMatteColour = rgb;
        theMatteLabel.setBackground(new Color(getShell().getDisplay(), rgb));
    }

    /**
     * Get the user choice for transparency or not.
     * 
     * @return True if user has selected that they want transparency
     * @since 2.5
     */
    public boolean getTransparency() {
        return theTransparency;
    }

    /**
     * This method returns a RGB of the users chosen colour matte, or null when transparency is enabled.
     * @return An RGB of the chosen colour matte
     * @since 2.5
     */
    public RGB getMatte() {
        return theMatteColour;
    }
    
    private final static int INDEX_MATTE_COMBO_WHITE = 0;
    private final static int INDEX_MATTE_COMBO_BLACK = 1;
    private final static int INDEX_MATTE_COMBO_OTHER = 2;
    
    public static void main(String[]args){        
        try {
            Display display = Display.getDefault();
            Shell shell = new Shell(display);
            PNGOptionsDialog d = new PNGOptionsDialog(shell);
            d.setOriginalSize(1900, 1200);
            if(d.open()==Window.OK){
                System.out.println(d.getTransparency());
                System.out.println(d.getMatte());  
                System.out.println(d.getSizeModified());
                System.out.println(d.getNewSize());            
            }
            
            while (!shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                    display.sleep();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }   
    }
}



