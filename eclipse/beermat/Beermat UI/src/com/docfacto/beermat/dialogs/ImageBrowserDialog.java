package com.docfacto.beermat.dialogs;

import static com.docfacto.beermat.plugin.PluginConstants.ICON_FOLDER;
import static com.docfacto.beermat.plugin.PluginConstants.MORE_BLUE;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.beermat.ui.BeermatViewConstants;
import com.docfacto.beermat.ui.widgets.HiderButton;
import com.docfacto.beermat.ui.widgets.SimpleLocationWidget;
import com.docfacto.beermat.utils.PluginManager;
import com.docfacto.core.widgets.FileBrowserWidget;
import com.docfacto.core.widgets.listeners.FileTypeSelectionValidator;

/**
 * A dialog for the FileBrowserWidget
 * 
 * @author kporter - created Jul 22, 2013
 * @since 2.4.2
 */
public class ImageBrowserDialog extends BeermatDialog {

    /**
     * Represents a selection of 'actual size'
     */
    public final static int SIZE_ACTUAL = 1;
    /**
     * Represents a selection of 'Fit to element'
     */
    public final static int SIZE_FIT = 2;
    /**
     * Represents a selection of 'custom size'
     */
    public final static int SIZE_CUSTOM = 3;

    /**
     * The background colour of all the composites in this dialog. {@docfacto.system Change this one constant to change
     * all of the background colours}
     */
    private final static Color BACKGROUND_COLOUR =
        BeermatViewConstants.COLOR_BACKGROUND;

    /**
     * The foreground colour of all the text. {@docfacto.system Change this one constant to change all of the text
     * colour}
     */
    private final static Color FOREGROUND_COLOUR =
        BeermatViewConstants.COLOR_FOREGROUND_BRAND;

    private static final String[] EXTENSIONS = {"*.png","*.jpg;*.jpeg","*.gif",
        "*.svg"};

    private FileBrowserWidget theFileBrowser;
    private SimpleLocationWidget theLocationWidget;
    private SimpleLocationWidget theSizeWidget;

    private String thePath;
    private String theBasePath = null;
    private Point theLocation;
    private Point theSize;

    private int theSizeInt = SIZE_ACTUAL;

    private boolean theElementsSelected = false;

    /**
     * Create the dialog.
     * 
     * @param parent The parent shell
     * @since 2.4.2
     */
    public ImageBrowserDialog(Shell parent) {
        super(parent, "Insert an Image");
    }

    /**
     * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createDialogArea(final Composite parent) {
        Composite container = (Composite)super.createDialogArea(parent);
        container.getShell().setText("Insert Image");
        GridLayout gridLayout = (GridLayout)container.getLayout();
        gridLayout.numColumns = 2;

        Label lblImageLocation = new Label(container, SWT.NONE);
        lblImageLocation.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        lblImageLocation.setForeground(FOREGROUND_COLOUR);
        lblImageLocation.setText("Image Location");

        // Give the widget some room
        GridData gd = new GridData();
        gd.widthHint = 360;
        gd.verticalAlignment = SWT.CENTER;
        gd.horizontalAlignment = SWT.FILL;
        gd.grabExcessHorizontalSpace = true;
        theFileBrowser =
            new FileBrowserWidget(container, SWT.NONE, "", theBasePath, EXTENSIONS);
        theFileBrowser.setLayoutData(gd);
        theFileBrowser.setInternalValidator(new FileTypeSelectionValidator(new String[] {"jpg","jpeg","png"},
            "Valid image format.", "Choose an image file"));
        theFileBrowser.setBrandColor(BeermatViewConstants.COLOR_FOREGROUND_BRAND);

        GridData gd_advancedButton = new GridData(SWT.LEFT, SWT.BOTTOM, false, false, 1, 1);
        gd_advancedButton.heightHint = 15;
        gd_advancedButton.widthHint = 79;

        final Image image = PluginManager.getImage(ICON_FOLDER+MORE_BLUE);
        HiderButton advancedButton = new HiderButton(container, SWT.NONE, "Advanced", image);
        advancedButton.setLayoutData(gd_advancedButton);
        advancedButton.getLabelText().setForeground(FOREGROUND_COLOUR);
        new Label(container, SWT.NONE);

        Label lblImageSize = new Label(container, SWT.NONE);
        lblImageSize.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        lblImageSize.setToolTipText("If no size is set, the actual size of the image is used");
        lblImageSize.setText("Image Size");
        lblImageSize.setForeground(BeermatViewConstants.COLOR_FOREGROUND_BRAND);

        Composite sizeButtonsComposite = (Composite)createSizeButtons(container);
        GridData gd_sizeButtons = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_sizeButtons.heightHint = 31;
        gd_sizeButtons.widthHint = 351;
        sizeButtonsComposite.setLayoutData(gd_sizeButtons);
        sizeButtonsComposite.setBackground(BACKGROUND_COLOUR);

        new Label(container, SWT.NONE);

        GridData gd_sizeWidget = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_sizeWidget.widthHint = 262;
        theSizeWidget = new SimpleLocationWidget(container, SWT.HORIZONTAL, "Width", "Height");
        theSizeWidget.setLayoutData(gd_sizeWidget);
        theSizeWidget.setToolTipText("If no size is set, the actual size of the image is used");

        // Disable the text boxes by default as 'actual size' is selected
        theSizeWidget.setEnabledText(false);

        new Label(container, SWT.NONE);
        new Label(container, SWT.NONE);

        Label lblImagePos = new Label(container, SWT.NONE);
        lblImagePos.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        lblImagePos.setText("Image Position");
        lblImagePos.setForeground(BeermatViewConstants.COLOR_FOREGROUND_BRAND);

        GridData gd_locationWidget = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
        gd_locationWidget.horizontalIndent = 23;
        gd_locationWidget.widthHint = 246;
        gd_locationWidget.heightHint = 29;
        theLocationWidget = new SimpleLocationWidget(container, SWT.HORIZONTAL);
        theLocationWidget.setLayoutData(gd_locationWidget);
        theLocationWidget.setToolTipText("If no location is set, image is inserted at the default 0,0");
        theLocationWidget.setBackground(BACKGROUND_COLOUR);
        theLocationWidget.setLabelColour(FOREGROUND_COLOUR);

        advancedButton.addToHideAndHide(sizeButtonsComposite);
        advancedButton.addToHideAndHide(theSizeWidget);
        advancedButton.addToHideAndHide(theLocationWidget);
        advancedButton.addToHideAndHide(lblImagePos);
        advancedButton.addToHideAndHide(lblImageSize);

        parent.setBackground(BACKGROUND_COLOUR);
        container.setBackground(BACKGROUND_COLOUR);
        theFileBrowser.setBackground(BACKGROUND_COLOUR);
        theSizeWidget.setBackground(BACKGROUND_COLOUR);
        advancedButton.setBackground(BACKGROUND_COLOUR);
        theSizeWidget.setLabelColour(FOREGROUND_COLOUR);
        // attempt to fix case 317
        container.redraw();
        return container;
    }

    /**
     * @see org.eclipse.jface.dialogs.Dialog#okPressed()
     */
    @Override
    protected void okPressed() {
        thePath = theFileBrowser.getSelectedPath();

        int x;
        int y;

        try {
            x = Integer.parseInt(theLocationWidget.getX());
        }
        catch (NumberFormatException ex) {
            BeermatUIPlugin.logMessage("Invalid location input, set to 0");
            x = 0;
        }

        try {
            y = Integer.parseInt(theLocationWidget.getY());

        }
        catch (NumberFormatException ex) {
            BeermatUIPlugin.logMessage("Invalid location input, set to 0");
            y = 0;
        }
        try {
            int width = Integer.parseInt(theSizeWidget.getX());
            int height = Integer.parseInt(theSizeWidget.getY());
            theSize = new Point(width, height);
        }
        catch (NumberFormatException ex) {
            // Do nothing, size stays null
        }

        theLocation = new Point(x, y);

        super.okPressed();
    }

    /**
     * @see org.eclipse.jface.dialogs.Dialog#isResizable()
     */
    protected boolean isResizable() {
        return true;
    }

    /**
     * The selected file path
     * 
     * @return the selected file path
     * @since 2.4
     */
    public String getPath() {
        return thePath;
    }

    /**
     * Sets the base path.
     * 
     * @param path The base path to set.
     * @since 2.4.2
     */
    public void setBasePath(String path) {
        theBasePath = path;
    }

    /**
     * Get a location Point.
     * 
     * @return The Point holding the location
     * @since 2.4.2
     */
    public Point getLocation() {
        return theLocation;
    }

    /**
     * Get a size Point
     * 
     * @return The Point holding size.
     * @since 2.4.2
     */
    public Point getSize() {
        return theSize;
    }

    /**
     * Returns the selection int of size.
     * 
     * @return An int to switch over the selection of the user
     * @see com.docfacto.beermat.ui.widgets.ImageBrowserDialog.SIZE_ACTUAL
     * @see com.docfacto.beermat.ui.widgets.ImageBrowserDialog.SIZE_FIT
     * @see com.docfacto.beermat.ui.widgets.ImageBrowserDialog.SIZE_CUSTOM
     * @since 2.4.2
     */
    public int getSizeInt() {
        return theSizeInt;
    }

    private Composite createSizeButtons(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        RowLayout rl_composite = new RowLayout(SWT.HORIZONTAL);
        rl_composite.marginTop = 5;
        rl_composite.spacing = 20;
        composite.setLayout(rl_composite);

        Button btnImageSize = new Button(composite, SWT.RADIO);
        btnImageSize.setText("Actual Size");
        // This is the default enabled behaviour.
        btnImageSize.setSelection(true);

        Button btnShrink = new Button(composite, SWT.RADIO);
        btnShrink.setText("Fit Selected Element");
        // This is set to the dialog before opening, by default its false, but
        // if an element is selected it will be true
        btnShrink.setEnabled(theElementsSelected);

        Button btnCustomSize = new Button(composite, SWT.RADIO);
        btnCustomSize.setText("Custom Size");

        btnImageSize.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                theSizeWidget.setEnabledText(false);
                theLocationWidget.setEnabledText(true);
                theSizeInt = SIZE_ACTUAL;
            }
        });
        btnShrink.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                theSizeWidget.setEnabledText(false);
                theLocationWidget.setEnabledText(false);
                theSizeInt = SIZE_FIT;
            }
        });
        btnCustomSize.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                theSizeWidget.setEnabledText(true);
                theLocationWidget.setEnabledText(true);
                theSizeInt = SIZE_CUSTOM;
            }
        });
        return composite;
    }

    /**
     * Notify this whether an element is selected.
     * 
     * This is to allow the user to have the relevant size options if an element is selected.
     * 
     * {@docfacto.note title="Set before open" Remember, the Dialog checks the boolean whether an element is selected
     * every time open() is called, so this method should be called before open()}
     * 
     * @param selected boolean if an element is selected
     * @since 2.4.2
     */
    public void setElementSelected(boolean selected) {
        theElementsSelected = selected;
    }

}
