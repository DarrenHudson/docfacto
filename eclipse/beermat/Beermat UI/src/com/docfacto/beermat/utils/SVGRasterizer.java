package com.docfacto.beermat.utils;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.ImageTranscoder;
import org.apache.batik.transcoder.image.JPEGTranscoder;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Document;

/**
 * Exports Document's to raster formats.
 * 
 * PNG, JPEG, and TIFF
 * 
 * @author kporter - created Jul 12, 2013
 * @since 2.4
 */
public class SVGRasterizer {

    public static void exportToJPG(Document document,String path,String filename) throws IOException,
    TranscoderException {
        exportToJPG(document,new File(path+filename+".jpg"));
    }

    public static void exportToJPG(Document document,File file) throws FileNotFoundException, TranscoderException {
        JPEGTranscoder t = new JPEGTranscoder();
        t.addTranscodingHint(JPEGTranscoder.KEY_QUALITY,new Float(.8));
        transcode(t,document,file);
    }
    
    public static void exportToJPG(Document document,File file, Point size, int quality) throws FileNotFoundException, TranscoderException {
        JPEGTranscoder t = new JPEGTranscoder();
        float f = (quality/10f);
        Float q = new Float(f);
        
        t.addTranscodingHint(JPEGTranscoder.KEY_QUALITY,q);
        
        if(size!=null){
            t.addTranscodingHint(JPEGTranscoder.KEY_WIDTH, size.x+0f);
            t.addTranscodingHint(JPEGTranscoder.KEY_HEIGHT, size.y+0f);
        }
        
        transcode(t,document,file);
    }

    public static void exportToPNG(Document document,String path,String filename) throws FileNotFoundException,
    TranscoderException {
        exportToPNG(document,new File(path+filename+".png"));
    }

    public static void exportToPNG(Document document,File file) throws FileNotFoundException, TranscoderException {
        exportToPNG(document, file, true, null,null);
    }
    
    /**
     * Rasterizes a document to a PNG image.
     * 
     * @param document The document to draw.
     * @param file The File to write to
     * @param transparency True for transparent output, false otherwise
     * @param matte The colour of the matte or null for default white (not needed for transparent)
     * @param size The output size or null for default
     * @throws FileNotFoundException
     * @throws TranscoderException
     * @since 2.5
     */
    public static void exportToPNG(Document document,File file, boolean transparency, Color matte, Point size) throws FileNotFoundException, TranscoderException {
        PNGTranscoder t = new PNGTranscoder();
        if(transparency){
            t.addTranscodingHint(PNGTranscoder.KEY_FORCE_TRANSPARENT_WHITE,true);            
        }
        else{
            if(matte==null){
                //If non-transparent and no colour was passed in, assume white.
                matte = new Color(255, 255, 255);
            }
            t.addTranscodingHint(PNGTranscoder.KEY_FORCE_TRANSPARENT_WHITE,false);
            t.addTranscodingHint(PNGTranscoder.KEY_BACKGROUND_COLOR, matte);
        }
        
        if(size!=null){
            t.addTranscodingHint(PNGTranscoder.KEY_WIDTH, size.x+0f);
            t.addTranscodingHint(PNGTranscoder.KEY_HEIGHT, size.y+0f);
        }
        
        transcode(t,document,file);
    }

    private static void transcode(final ImageTranscoder transcoder,final Document document,final File file)
    throws FileNotFoundException, TranscoderException {

        TranscoderInput input = new TranscoderInput(document);

        OutputStream ostream = new FileOutputStream(file);
        TranscoderOutput output = new TranscoderOutput(ostream);
        transcoder.transcode(input,output);
        try {
            if (ostream!=null) {
                ostream.close();
                ostream.flush();
            }
        }
        catch (IOException e) {
            // do something, or nothing
        }
    }

}
