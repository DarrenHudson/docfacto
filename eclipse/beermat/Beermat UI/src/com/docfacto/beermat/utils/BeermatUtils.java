package com.docfacto.beermat.utils;

import java.awt.BasicStroke;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.batik.css.engine.CSSEngine;
import org.apache.batik.css.engine.CSSStyleSheetNode;
import org.apache.batik.css.engine.Rule;
import org.apache.batik.css.engine.StyleRule;
import org.apache.batik.css.engine.StyleSheet;
import org.apache.batik.css.engine.sac.CSSClassCondition;
import org.apache.batik.css.engine.sac.CSSConditionalSelector;
import org.w3c.css.sac.SelectorList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.svg.ConnectorHandler;
import com.docfacto.beermat.svg.SVGSize;
import com.docfacto.common.XMLUtils;

/**
 * Collection of static util methods for Beermat
 * 
 * @author dhudson - created 7 Aug 2013
 * @since 2.4
 */
public class BeermatUtils {

    private static final float dash1[] = {2.0f};

    /**
     * {@value}
     */
    public static final BasicStroke HIGHLIGHT_STROKE = new BasicStroke(1.0f,
        BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER,10.0f,dash1,0.0f);

    /**
     * Constructor.
     */
    private BeermatUtils() {
        // I don't think so
    }

    /**
     * Convert a double to an int
     * 
     * @param number to convert
     * @return an int
     * @since 2.3
     */
    public static int convertToInt(double number) {
        return (int)Math.round(number);
    }

    /**
     * Increases the given rectangle area for a given amount
     * 
     * @param r The given rectangle
     * @param amount The given amount of units
     * @return <code>r</code>
     * @since 2.4
     */
    public static Rectangle outset(Rectangle r,int amount) {
        Rectangle rect = new Rectangle(r.x-amount,r.y-amount,r.width
            +(2*amount),r.height+(2*amount));
        return rect;
    }

    /**
     * Check the mouse position and see if the point is on the canvas
     * 
     * @param event mouse event
     * @param controller beermat controller
     * @return true if the mouse position is on the canvas
     * @since 2.4
     */
    public static boolean isMouseOnCanvas(MouseEvent event,
    BeermatController controller) {
        // Check to see if the mouse in the the bounds of the document
        SVGSize docSize = controller.getManager().getDocumentSize();

        if(docSize == null) {
            return false;
        }
        
        Point2D canvasPoint = getTranformedMousePoint(event,
            controller.getViewBoxTransform());

        if (canvasPoint!=null) {
            if (canvasPoint.getX()<0
                ||canvasPoint.getX()>docSize.getWidth()) {
                // Mouse isn't on canvas
                return false;
            }

            if (canvasPoint.getY()<0
                ||canvasPoint.getY()>docSize.getHeight()) {
                // Mouse isn't on canvas
                return false;
            }

            return true;
        }
        return false;
    }

    /**
     * Return the transformed mouse point.
     * 
     * @param event mouse event which the point will be taken from
     * @param viewbox transform
     * @return a relative point, regardless of zoom and scroll
     * @since 2.4
     */
    public static Point2D getTranformedMousePoint(MouseEvent event,
    AffineTransform viewbox) {
        try {
            // Lets see if the point is on the canvas
            AffineTransform transform = viewbox.createInverse();

            return transform.transform(
                new Point2D.Double(event.getX(),event.getY()),null);
        }
        catch (NoninvertibleTransformException ignore) {
            return null;
        }
    }

    /**
     * Return a list of style sheet classes
     * 
     * @param cssEngine to process
     * @return a list of class styles, may be an empty list
     * @since 2.5
     */
    public static List<String> getStyleSheetNodes(CSSEngine cssEngine) {

        ArrayList<String> classList = new ArrayList<String>(5);

        List<?> styleSheetsList = cssEngine.getStyleSheetNodes();

        for (int j = 0;j<styleSheetsList.size();j++) {
            CSSStyleSheetNode cssNode =
                (CSSStyleSheetNode)styleSheetsList.get(j);

            StyleSheet styleSheet = cssNode.getCSSStyleSheet();

            int numRules = styleSheet.getSize();

            for (int ruleIndex = 0;ruleIndex<numRules;ruleIndex++)
            {
                Rule rule = styleSheet.getRule(ruleIndex);

                if (rule instanceof StyleRule)
                {
                    StyleRule sr = ((StyleRule)rule);

                    SelectorList styleList = sr.getSelectorList();
                    for (int i = 0;i<styleList.getLength();i++) {
                        Object obj = styleList.item(i);
                        if (obj instanceof CSSConditionalSelector) {
                            CSSConditionalSelector conSelector =
                                (CSSConditionalSelector)obj;
                            CSSClassCondition condition =
                                (CSSClassCondition)conSelector.getCondition();
                            if ("class".equals(condition.getLocalName())) {
                                classList.add(condition.getValue());
                            }
                        }
                    }
                }
            }
        }
        return classList;
    }

    public static void dumpRect(Rectangle2D rect) {
        System.out.println("Rect "+convertToInt(rect.getX())+" : "
            +convertToInt(rect.getY())+" : "
            +convertToInt(convertToInt(rect.getWidth()))+" : "
            +convertToInt(rect.getHeight()));
    }

    /**
     * Return a list of filter id's.
     * 
     * @param document to search
     * @return a List, possible empty of filter id's
     * @since 2.5
     */
    public static List<String> getFilters(SVGDocument document) {
        ArrayList<String> filters = new ArrayList<String>(5);

        NodeList entries = document.getElementsByTagName("filter");
        for (int i = 0;i<entries.getLength();++i) {
            Node node = entries.item(i);
            if (Node.ELEMENT_NODE==node.getNodeType()) {
                Element element = (Element)node;
                if (element.hasAttribute("id")) {
                    filters.add(element.getAttribute("id"));
                }
            }
        }
        return filters;
    }
    
    /**
     * Return a set of connectors.
     * 
     * @param document to process
     * @return A Set of connectors
     * @since 2.5
     */
    public static Set<Element> getConnectors(Document document) {
        LinkedHashSet<Element> connectors = new LinkedHashSet<Element>(5);
        connectors.addAll(XMLUtils.getElementsWithAttribute(
            document,
            SVGUtils.DOCFACTO_NAME_SPACE,
            ConnectorHandler.CONNECTOR_START_ATTRIBUTE,"*"));
        connectors.addAll(XMLUtils.getElementsWithAttribute(
            document,
            SVGUtils.DOCFACTO_NAME_SPACE,
            ConnectorHandler.CONNECTOR_END_ATTRIBUTE,"*"));

        return connectors;
    }
}
