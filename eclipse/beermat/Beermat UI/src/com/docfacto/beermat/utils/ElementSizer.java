package com.docfacto.beermat.utils;

import org.apache.batik.bridge.BridgeContext;
import org.apache.batik.bridge.DocumentLoader;
import org.apache.batik.bridge.GVTBuilder;
import org.apache.batik.bridge.UserAgent;
import org.apache.batik.bridge.UserAgentAdapter;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;
import org.w3c.dom.svg.SVGLocatable;
import org.w3c.dom.svg.SVGRect;

/**
 * Calculate the size of an element, without using the rendering panel
 * 
 * @author dhudson - created 25 Sep 2013
 * @since 2.4
 */
public enum ElementSizer {

    /**
     * ENUM singleton pattern
     */
    INSTANCE;

    private BridgeContext theContext;
    private GVTBuilder theBuilder;
    private SVGDocument theDocument;
    
    /**
     * Constructor.
     */
    private ElementSizer() {
        
        // Create standard doc
        theDocument = SVGUtils.createSVGDocument(null);
        
        // Reuse the context and the builder
        UserAgent userAgent = new UserAgentAdapter();
        theContext = new BridgeContext(userAgent, new DocumentLoader(userAgent));
        theContext.setDynamicState(BridgeContext.DYNAMIC);

        theBuilder = new GVTBuilder();
    }

    /**
     * Calculate the size of an element
     * 
     * @param element to process, this element should not be active (I.E. on the
     * live DOM)
     * @return the graphical bounds of the element
     * @since 2.4
     */
    public SVGRect getElementBounds(Element element) {
        Element clone = (Element)theDocument.importNode(element,true);
        theDocument.getDocumentElement().appendChild(clone);
        theBuilder.build(theContext,theDocument);
        SVGRect box = ((SVGLocatable)clone).getBBox();
        // Make sure its ready for the next call
        // theDocument.getDocumentElement().removeChild(clone);
        return box;
    }
}
