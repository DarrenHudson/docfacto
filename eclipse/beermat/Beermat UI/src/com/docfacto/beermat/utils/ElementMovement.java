package com.docfacto.beermat.utils;

/**
 * Enum to encapsulate the movement of element(s) in the order of the SVG
 * 
 * @author dhudson - created 17 Aug 2013
 * @since 2.4
 */
public enum ElementMovement {
    /**
     * Element to front will mean that the element is at the end of the
     * document, so it is rendered last
     */
    FRONT,
    /**
     * Move the element down the list of elements
     */
    FORWARD,
    /**
     * Element to back will mean that the element will be the first element in
     * the document
     */
    BACK,
    /**
     * Move the element towards the beginning of the document
     */
    BACKWARD
}
