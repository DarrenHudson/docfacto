package com.docfacto.beermat.utils;

import java.util.Comparator;

import org.w3c.dom.Element;

/**
 * Compares two elements.
 * 
 * @author kporter - created Aug 23, 2013
 * @since 2.4.4
 */
public class ElementComparator implements Comparator<Element> {

    /**
     * {@docfacto.system For some odd reason the Element compare returns 2 when the comparing element is before the compared}
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(Element o1,Element o2) {
        int comparison = o1.compareDocumentPosition(o2);
        return comparison == 2 ? -1 : 1 ;
    }

}
