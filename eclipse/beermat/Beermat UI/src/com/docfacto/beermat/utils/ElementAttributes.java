package com.docfacto.beermat.utils;

import java.util.ArrayList;
import java.util.List;

import com.docfacto.common.NameValuePair;

/**
 * A class to hold elementAttributes
 * 
 * @author kporter - created Aug 29, 2013
 * @since 2.4.4
 */
public class ElementAttributes {

    private String theStrokeColour;
    private String theStrokeWidth;
    private String theFill;
    private String theDash;
    
    private String theMarkerStart;
    private String theMarkerEnd;

    /**
     * Makes a new ElementAttributes with no properties set.
     * 
     * @since 2.4.4
     */
    public ElementAttributes() {
    }

    /**
     * Constructs a new ElementAttributes with properties set.
     * 
     * @param strokeColour The stroke colour
     * @param strokeWidth The stroke width
     * @param fill The fill
     * @param dash The stroke-dasharray
     * @since 2.4.4
     */
    public ElementAttributes(String strokeColour,String strokeWidth,
    String fill,String dash) {
        setStrokeColour(strokeColour);
        setStrokeWidth(strokeWidth);
        setFill(fill);
        setDash(dash);
        theMarkerEnd = "";
        theMarkerStart = "";
    }

    /**
     * Get the stroke colour
     * 
     * @return The stroke colour
     * @since 2.4.4
     */
    public String getStrokeColour() {
        return theStrokeColour;
    }

    /**
     * Sets the stroke colour
     * 
     * @param strokeColour The stroke colour
     * @since 2.4.4
     */
    public void setStrokeColour(String strokeColour) {
        strokeColour = colourCheck(strokeColour);
        theStrokeColour = strokeColour;
    }

    /**
     * Get the stroke width
     * 
     * @return the stroke width
     * @since 2.4.4
     */
    public String getStrokeWidth() {
        return theStrokeWidth;
    }

    /**
     * Set a stroke width
     * 
     * If strokeWidth is empty or null, the width is assumed to be 1
     * 
     * @param strokeWidth the stroke width to set
     * @since 2.4.4
     */
    public void setStrokeWidth(String strokeWidth) {
        if (strokeWidth.isEmpty()) {
            theStrokeWidth = "1";
        }
        else {
            theStrokeWidth = strokeWidth;
        }
    }

    /**
     * Get the fill
     * 
     * @return the fill
     * @since 2.4.4
     */
    public String getFill() {
        return theFill;
    }

    /**
     * Sets the fill The string is checked to see if it a worded colour;
     * black,red,blue,green,yellow,white, if so, changes it to a hex colour.
     * 
     * @param fill The fill
     * @since 2.4.4
     */
    public void setFill(String fill) {
        fill = colourCheck(fill);
        theFill = fill;
    }

    /**
     * Gets the stroke dash
     * 
     * @return The stroke-dasharray
     * @since 2.4.4
     */
    public String getDash() {
        return theDash;
    }

    /**
     * Sets a stroke-dasharray.
     * 
     * If the given parameter is empty, "none" for stroke-dasharray is assumed.
     * 
     * @param dash The dash array to set
     * @since 2.4.4
     */
    public void setDash(String dash) {
        if (dash.isEmpty()) {
            theDash = "none";
        }
        else {
            theDash = dash;

        }
    }

    /**
     * Checks that the given string is a hex value
     * 
     * {@docfacto.todo still doesn't check half the worded colours}
     * 
     * @param colour The hex colour
     * @return A string holding the hex colour
     * @since 2.4.4
     */
    public String colourCheck(String colour) {
        if (colour.equals("red")) {
            return "#FF0000";
        }
        else if (colour.equals("green")) {
            return "#00FF00";
        }
        else if (colour.equals("blue")) {
            return "#0000FF";
        }
        else if (colour.equals("white")) {
            return "#FFFFFF";
        }
        else if (colour.equals("black")) {
            return "#000000";
        }
        else if (colour.isEmpty()||!colour.startsWith("#")) {
            return "none";
        }
        else {
            return colour;
        }
    }

    /**
     * Returns theMarkerStart.
     *
     * @return the theMarkerStart
     * @since 2.4.5
     */
    public String getMarkerStart() {
        return theMarkerStart;
    }

    /**
     * Sets theMarkerStart.
     *
     * @param markerStart the start marker value
     */
    public void setMarkerStart(String markerStart) {
        theMarkerStart = markerStart;
    }

    /**
     * Returns theMarkerEnd.
     *
     * @return the theMarkerEnd
     * @since 2.4.5
     */
    public String getMarkerEnd() {
        return theMarkerEnd;
    }

    /**
     * Sets theMarkerEnd.
     *
     * @param markerEnd the end marker value
     * @since 2.4.5
     */
    public void setMarkerEnd(String markerEnd) {
        theMarkerEnd = markerEnd;
    }

    /**
     * Puts all attributes in a list of nvp.
     * 
     * @return The list of NameValuePair's of all attributes
     * @since 2.4.4
     * @see com.docfacto.common.NameValuePair
     */
    public List<NameValuePair> getAllAttributes() {
        List<NameValuePair> attrList = new ArrayList<NameValuePair>(3);

        attrList.add(new NameValuePair("stroke-width",theStrokeWidth));
        attrList.add(new NameValuePair("stroke",theStrokeColour));
        attrList.add(new NameValuePair("fill",theFill));
        attrList.add(new NameValuePair("stroke-dashArray",theDash));
        
        if(!theMarkerEnd.isEmpty()){
            attrList.add(new NameValuePair("marker-end",theMarkerEnd));
        }
        if(!theMarkerStart.isEmpty()){
            attrList.add(new NameValuePair("marker-start",theMarkerStart));
        }
        return attrList;
    }

}
