package com.docfacto.beermat.utils;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.swt.graphics.Image;

import com.docfacto.beermat.BeermatLoader;
import com.docfacto.beermat.generated.ElementDef;
import com.docfacto.beermat.generated.Palette;
import com.docfacto.beermat.generated.SvgDef;
import com.docfacto.beermat.generated.Type;
import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.common.DocfactoException;
import com.docfacto.core.utils.ImageUtils;
import com.docfacto.core.utils.PluginUtils;

/**
 * Manages loading, caching of images, and xml loading.
 * 
 * @author kporter - created Apr 10, 2013
 * @since 2.2
 */
public class PluginManager {
    /**
     * Field has leading "/"
     */
    private static HashMap<String,Image> theImageCache =
        new HashMap<String,Image>();
    private static List<Palette> thePalettes;

    /**
     * A method to cache and retrieve images.
     * <p>
     * Will first check the cache of images, using the path as a key in the map,
     * if non-existent then the image is retrieved added to the cache, and
     * returned.
     * </p>
     * 
     * @param path The path to the image
     * @return The image at the given path
     * @since 2.2
     */
    public static Image getImage(String path) {
        Image image = theImageCache.get(path);
        if (image==null) {
            // Have tried both ResourceManager and PluginUtils, neither shows a
            // toolbar on windows.
            image =
                ImageUtils.getPluginImage(BeermatUIPlugin.PLUGIN_ID,path);
            theImageCache.put(path,image);
        }
        return image;
    }

    /**
     * A method to retrieve Palette's from the Beermat XML and cache them.
     * <p>
     * A method that will return all the Palette's from the XML if not cached.
     * </p>
     * 
     * @return All the Palette's from the XML
     * @since 2.2
     */
    private static List<Palette> getPalettesFromXML() {
        if (thePalettes!=null) {
            return thePalettes;
        }
        else {
            try {
                URL u =
                    PluginUtils.getResourceURL(BeermatUIPlugin.PLUGIN_ID,
                        "resources/beermat.xml");
                BeermatLoader loader = new BeermatLoader(u);
                thePalettes = loader.getRootPalettes();
            }
            catch (DocfactoException ex) {
                BeermatUIPlugin.logException("Unable to load Beermat.xml",ex);
            }
            return thePalettes;
        }
    }

    /**
     * Returns palettes of a given type.
     * 
     * @param type The type needed
     * @param palettes The list of palettes in which to sort
     * @return The palettes of a given type.
     * @since 2.2
     */
    public static List<Palette> getPaletteType(
    com.docfacto.beermat.generated.Type type) {
        List<Palette> palettes = getPalettesFromXML();
        List<Palette> typePalettes = new ArrayList<Palette>(palettes.size()/2);

        for (Palette p:palettes) {
            if (p.getType()==type) {
                typePalettes.add(p);
            }
        }
        return typePalettes;
    }

    /**
     * {@docfacto.note Not to be used as part of the plugin code }
     * 
     * @param type
     * @param file
     * @return a list of palette items of a given type
     * @throws DocfactoException if unable to load the XML file
     * @since 2.4
     */
    public static List<Palette> getPaletteType(Type type,File file)
    throws DocfactoException {
        BeermatLoader loader = new BeermatLoader(file);
        List<Palette> typePalettes =
            new ArrayList<Palette>(loader.getRootPalettes().size()/2);

        for (Palette p:loader.getRootPalettes()) {
            if (p.getType()==type) {
                typePalettes.add(p);
            }
        }
        return typePalettes;
    }

    /**
     * Returns only the SVGDefs from a given palette.
     * 
     * As a palette can contain both SvgDefs and Palette's, this will only
     * return the SvgDefs.
     * 
     * @param palette The palette in which to retrieve the SvgDefs
     * @return The SvgDefs
     * @since 2.2
     */
    public static List<SvgDef> getSVGDefs(Palette palette) {
        List<SvgDef> svgDefs = new ArrayList<SvgDef>(0);
        for (Object o:palette.getPalettesAndSvgDevesAndElementDeves()) {
            if (o instanceof SvgDef) {
                svgDefs.add((SvgDef)o);
            }
        }
        return svgDefs;
    }
    
    public static List<ElementDef> getElementDefs(Palette palette){
        List<ElementDef> elements = new ArrayList<ElementDef>(0);
        for(Object o: palette.getPalettesAndSvgDevesAndElementDeves()){
            if (o instanceof ElementDef){
                elements.add((ElementDef)o);
            }
        }
        return elements;
    }
}
