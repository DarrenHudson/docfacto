package com.docfacto.beermat.utils;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.batik.bridge.BridgeContext;
import org.apache.batik.dom.svg.SAXSVGDocumentFactory;
import org.apache.batik.dom.svg.SVGDOMImplementation;
import org.apache.batik.dom.svg.SVGOMGElement;
import org.apache.batik.dom.svg.SVGOMPoint;
import org.apache.batik.dom.svg.SVGOMTextElement;
import org.apache.batik.gvt.GraphicsNode;
import org.apache.batik.svggen.SVGGeneratorContext;
import org.apache.batik.svggen.SVGPath;
import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.util.SVGConstants;
import org.apache.batik.util.XMLResourceDescriptor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.svg.SVGDocument;
import org.w3c.dom.svg.SVGGElement;
import org.w3c.dom.svg.SVGLocatable;
import org.w3c.dom.svg.SVGMatrix;
import org.w3c.dom.svg.SVGPoint;
import org.w3c.dom.svg.SVGRect;
import org.w3c.dom.svg.SVGSVGElement;

import com.docfacto.beermat.generated.ElementDef;
import com.docfacto.beermat.hotspot.HotSpot;
import com.docfacto.beermat.svg.ConnectorHandler;
import com.docfacto.beermat.svg.TextHandler;
import com.docfacto.beermat.svg.TransformElementParser;
import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.common.XMLUtils;

/**
 * Util class to help with SVG and DOM
 * 
 * @author kporter - created Feb 18, 2013
 * @since 2.2
 */
public class SVGUtils {

    /**
     * Thread safe decimal format
     */
    public static final ThreadLocal<NumberFormat> NUMBER_FORMAT =
        new ThreadLocal<NumberFormat>() {
            /**
             * @see java.lang.ThreadLocal#initialValue()
             */
            @Override
            protected NumberFormat initialValue() {
                // The decimal point is required
                NumberFormat format = NumberFormat.getInstance(Locale.ENGLISH);
                // Only two decimal places please.
                format.setMaximumFractionDigits(2);
                format.setMinimumFractionDigits(0);
                format.setGroupingUsed(false);
                return format;
            }
        };

    /**
     * The SAX Document Factory for SVG.
     */
    public static SAXSVGDocumentFactory theSVGDocumentFactory =
        new SAXSVGDocumentFactory(
            XMLResourceDescriptor.getXMLParserClassName());

    /**
     * DocType Public ID
     */
    public static final String SVG_PUBLIC_ID_11 = "-//W3C//DTD SVG 1.1//EN";

    /**
     * DocType System ID
     */
    public static final String SVG_SYSTEM_ID =
        "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd";

    /**
     * DocType Qualified Name
     */
    public static final String SVG_QUALIFIED_NAME = "svg";

    /**
     * Docfacto qualified name
     */
    public static final String DOCFACTO_QUALIFIED_NAME = "docfacto";

    /**
     * Docfacto Name Space {@value}
     */
    public static final String DOCFACTO_NAME_SPACE =
        "http://www.docfacto.com/beermat";

    /**
     * XLink name space {@value}
     */
    public static final String XLINK_NAME_SPACE =
        "http://www.w3.org/1999/xlink";

    /**
     * XLink qualified name
     */
    public static final String XLINK_QUALIFIED_NAME = "xlink";

    /**
     * {@value}
     */
    public final static String SVG_NAMESPACE =
        SVGDOMImplementation.SVG_NAMESPACE_URI;

    /**
     * The attribute name for stroke dasharray
     */
    public final static String DASH_ARRAY = "stroke-dashArray";

    /**
     * The attribute name for stroke colour
     */
    public final static String STROKE_COLOUR = "stroke";

    /**
     * The attribute name for stroke width
     */
    public final static String STROKE_WIDTH = "stroke-width";

    /**
     * Attribute name for fill.
     */
    public final static String FILL = "fill";

    private SVGUtils() {
    }

    /**
     * A method to check whether the SVGDocument set to this Widget has a
     * viewBox attribute set.
     * 
     * @param doc to check
     * @return <code>true</code> if this SVG has a viewBox attribute set.
     * @since 2.1
     */
    public static boolean hasViewBox(SVGDocument doc) {
        Element root = doc.getDocumentElement();
        return root.hasAttribute("viewBox");
    }

    /**
     * Creates doc DOM of the URI.
     * 
     * The URI should point to a SVG
     * 
     * @param uri to load from
     * @return newly created Document
     * @throws IOException if unable to load the document
     * @since 2.1
     */
    public static SVGDocument getSVGDocument(URI uri) throws IOException {
        return getSVGDocument(uri.toString());
    }

    /**
     * Creates an SVGDocument based on the given uri as a string.
     * 
     * @param uri in string format
     * @return a newly created SVG document
     * @throws IOException if unable to load the file
     * @since 2.1
     */
    public static SVGDocument getSVGDocument(String uri) throws IOException {
        return theSVGDocumentFactory.createSVGDocument(uri);
    }

    /**
     * Create a SVGDocument from a baseURI and String of contents
     * 
     * @param baseURI The URI
     * @param contents XML contents
     * @return a SVGDocument with the baseURI set
     * @throws IOException not going to happen, as its a string
     * @since 2.4
     */
    public static SVGDocument getSVGDocument(String baseURI,String contents)
    throws IOException {
        StringReader reader = new StringReader(contents);
        return theSVGDocumentFactory.createSVGDocument(baseURI,reader);
    }

    /**
     * Create an empty SVG Document
     * 
     * @param type Document Type to create the document with, if null, then a
     * standard SVG 1.1 DocType will be created
     * @return and empty svg document
     * @throws DOMException if unable to create the document
     * @since 2.4
     */
    public static SVGDocument createSVGDocument(DocumentType type)
    throws DOMException {
        DOMImplementation dom = SVGDOMImplementation.getDOMImplementation();
        if (type==null) {
            type = dom.createDocumentType(SVG_QUALIFIED_NAME,SVG_PUBLIC_ID_11,
                SVG_SYSTEM_ID);
        }
        else {
            type = dom.createDocumentType(SVG_QUALIFIED_NAME,
                type.getPublicId(),type.getSystemId());
        }

        SVGDocument doc = (SVGDocument)dom.createDocument(SVG_NAMESPACE,
            SVG_QUALIFIED_NAME,type);

        Element svgRoot = doc.getDocumentElement();

        // Add the xlink name space
        // xmlns:xlink="http://www.w3.org/1999/xlink"
        svgRoot.setAttributeNS("http://www.w3.org/2000/xmlns/","xmlns:xlink",
            SVGUtils.XLINK_NAME_SPACE);

        // Add docfacto namespace
        injectDocfactoNamespace(doc);

        return doc;
    }

    /**
     * Add the docfacto name space so that connectors et al will work.
     * 
     * {@docfacto.note xmlns:docfacto="http://www.docfacto.com/beermat" }
     * 
     * @param doc to add the name space
     * @since 2.4
     */
    public static void injectDocfactoNamespace(SVGDocument doc) {
        Element svgRoot = doc.getDocumentElement();
        svgRoot.setAttributeNS("http://www.w3.org/2000/xmlns/","xmlns:"
            +DOCFACTO_QUALIFIED_NAME,DOCFACTO_NAME_SPACE);
    }

    /**
     * Set the height and width attributes of a SVG Document
     * 
     * @param doc to set the attributes
     * @param height to set
     * @param width to set
     * @since 2.4
     */
    public static void setSVGHeightWidth(SVGDocument doc,int height,int width) {
        Element svgRoot = doc.getDocumentElement();
        svgRoot.setAttributeNS(null,SVGConstants.SVG_WIDTH_ATTRIBUTE,
            Integer.toString(width));
        svgRoot.setAttributeNS(null,SVGConstants.SVG_HEIGHT_ATTRIBUTE,
            Integer.toString(height));
    }

    /**
     * Create a new document, put preserve the document type and height and
     * width attributes from the old document.
     * 
     * @param doc to copy form
     * @return a new document, with the height and width set if possible
     * @since 2.4
     */
    public static SVGDocument createDocumentFrom(SVGDocument doc) {
        DocumentType type = doc.getDoctype();
        SVGDocument newDoc = createSVGDocument(type);
        Element svgRoot = doc.getDocumentElement();

        String height = svgRoot.getAttribute(SVGConstants.SVG_HEIGHT_ATTRIBUTE);
        String width = svgRoot.getAttribute(SVGConstants.SVG_WIDTH_ATTRIBUTE);

        if (!height.isEmpty()&&!width.isEmpty()) {
            Element newSVGRoot = newDoc.getDocumentElement();
            newSVGRoot.setAttribute(SVGConstants.SVG_HEIGHT_ATTRIBUTE,height);
            newSVGRoot.setAttribute(SVGConstants.SVG_WIDTH_ATTRIBUTE,width);
        }

        return newDoc;
    }

    /**
     * Clear a documents visible elements, while preserving its defs.
     * <p>
     * This method is for when a document visible elements should be cleared,
     * but all its defs should be preserved. The height/width of the document is
     * preserved.
     * </p>
     * 
     * @param document The SVG Document
     * @return A document cleared of its visible elements
     * @since 2.4
     */
    public static Document clearDocument(SVGDocument document) {
        SVGDocument freshDocument = createDocumentFrom(document);
        NodeList nl = document.getElementsByTagName("defs");
        for (int i = 0;i<nl.getLength();i++) {
            Element def = (Element)nl.item(i);
            Element newDef = (Element)freshDocument.importNode(def,true);
            freshDocument.getDocumentElement().appendChild(newDef);
        }

        return freshDocument;
    }

    /**
     * Return the parent group, if possible.
     * 
     * An element may belong to a parent group, find the highest level group
     * 
     * @param element child element, which may belong to a group
     * @return the topmost group element, or the same element if it doesn't
     * belong to a group
     * @since 2.4
     */
    public static Element findParentGroup(Element element) {
        Node node = element;
        Element parentGroup = element;
        while ((node = node.getParentNode())!=null) {
            if (node instanceof Element) {
                Element parent = (Element)node;
                if (parent instanceof SVGOMGElement) {
                    parentGroup = parent;
                }
            }
        }

        return parentGroup;
    }

    /**
     * Return the first parent that is a group
     * 
     * @param element to search
     * @return first parent group node
     * @since 2.4
     */
    public static Element findFirstGroup(Element element) {
        Node node = element;
        while ((node = node.getParentNode())!=null) {
            if (node instanceof Element) {
                Element parent = (Element)node;
                if (parent instanceof SVGOMGElement) {
                    return parent;
                }
            }
        }
        return element;
    }

    /**
     * Get a path from a given element
     * <p>
     * A path is returned, with its translation taken in to account.
     * {@docfacto.system Done by first getting a Shape of the element (with its
     * transform applied) the shape is then converted to a path}
     * </p>
     * 
     * @param doc The document holding the element.
     * @param element The element to be converted to a path
     * @param canvas THe canvas.
     * @return The path as string
     * @since 2.4
     */
    public static String elementToPath(Document doc,Element element,
    JSVGCanvas canvas) {
        Shape path = elementToShape(element,canvas);
        SVGGeneratorContext gc = SVGGeneratorContext.createDefault(doc);
        return shapeToPath(path,gc);
    }

    /**
     * Gets a Shape from a given element.
     * <p>
     * The shape returned is the element with its transform applied.
     * </p>
     * 
     * @param element The element to get a shape from.
     * @param canvas The canvas in which the element exists.
     * @return The Shape for this element.
     * @since 2.4
     */
    public static Shape elementToShape(Element element,JSVGCanvas canvas) {
        BridgeContext b = canvas.getUpdateManager().getBridgeContext();
        GraphicsNode gn = b.getGraphicsNode(element);
        AffineTransform a = gn.getTransform();
        return a.createTransformedShape(gn.getOutline());
    }

    /**
     * Get a path from a Shape.
     * <p>
     * Using the given SVGGeneratorContext a path that follows this Shape is
     * calculated.
     * </p>
     * 
     * @param shape The shape to convert.
     * @param gc The SVGGeneratorContext needed to convert the shape.
     * @return A path
     * @since 2.4
     */
    public static String shapeToPath(Shape shape,SVGGeneratorContext gc) {
        return SVGPath.toSVGPathData(shape,gc);
    }

    /**
     * Calculate the bounds of an element, relative to the SVG and not what is
     * being viewed.
     * 
     * @param element to calculate the bounds
     * @param viewTransform The view AffineTransform
     * @return bounds of an element relative to the SVG
     * @since 2.4
     */
    public static Rectangle getBounds(Element element,
    AffineTransform viewTransform) {

        SVGLocatable locatable = (SVGLocatable)element;
        SVGRect box = locatable.getBBox();
        SVGMatrix matrix = locatable.getCTM(); //getScreenCTM(); // getCTM();

        AffineTransform shapeTransform = null;
        try {
            shapeTransform = viewTransform.createInverse();
            shapeTransform.concatenate(new AffineTransform(matrix.getA(),
                matrix.getB(),matrix.getC(),matrix.getD(),matrix.getE(),
                matrix.getF()));
        }
        catch (NoninvertibleTransformException e) {
            e.printStackTrace();
        }

        Point2D point = shapeTransform.transform(new Point2D.Float(box.getX(),
            box.getY()),null);

        return new Rectangle((int)point.getX(),(int)point.getY(),
            (int)(box.getWidth()*shapeTransform.getScaleX()),
            (int)(box.getHeight()*shapeTransform.getScaleY()));
    }

    /**
     * Return a point on the canvas taking into account the viewbox transform.
     * 
     * @param x location
     * @param y location
     * @param transform normally viewport
     * @return a point relative to canvas disregarding scrolling and zooming
     * @since 2.5
     */
    public static Point2D getTransformedPoint(double x,double y,
    AffineTransform transform) {
        try {
            return transform.inverseTransform(new Point2D.Double(x,y),
                null);
        }
        catch (NoninvertibleTransformException e) {
            return null;
        }
    }

    /**
     * Return the transform top most point of the element
     * 
     * @param element to calculate the start point
     * @return a point of the translated x,y
     * @since 2.4
     */
    public static Point getElementTranslate(Element element) {
        SVGLocatable locatable = (SVGLocatable)element;
        SVGMatrix matrix = locatable.getCTM();
        SVGOMPoint pt = new SVGOMPoint();
        SVGPoint point = pt.matrixTransform(matrix);
        return new Point((int)point.getX(),(int)point.getY());
    }

    /**
     * Convert a affine transform to a matrix string
     * 
     * @param at transform to process
     * @return an SVG String of a matrix
     * @since 2.2
     */
    public static String affineTransformToString(final AffineTransform at) {
        double[] matrix = {0.0,0.0,0.0,0.0,0.0,0.0};
        at.getMatrix(matrix);
        return matrixArrayToString(matrix);
    }

    /**
     * Covert an array of doubles (size of 6 to represent a matrix) into a SVG
     * Matrix string
     * 
     * @param vals to convert
     * @return a SVG String representation of a matix
     * @since 2.2
     */
    public static String matrixArrayToString(double[] vals) {
        return new StringBuilder("matrix(").append(vals[0]).append(" ")
            .append(vals[1]).append(" ").append(vals[2]).append(" ")
            .append(vals[3]).append(" ").append(vals[4]).append(" ")
            .append(vals[5]).append(") ").toString();
    }

    /**
     * Gets the scale of an element
     * <p>
     * {@docfacto.system Elements A and D of the matrix represent the scale. A
     * is scaleX and D is scaleY.}
     * </p>
     * 
     * @param element The element to get the scale from
     * @return The scale as a Double Point2D
     * @since 2.4
     */
    public static Point2D getElementScale(Element element) {
        SVGLocatable loctable = (SVGLocatable)element;
        SVGMatrix matrix = loctable.getCTM();
        return new Point2D.Double(matrix.getA(),matrix.getD());
    }

    /**
     * Gets the transform from an element
     * 
     * @param element The element from which to get the transform
     * @return The translate of the Element
     * @since 2.4
     */
    public static Point getElementTranslateA(Element element) {
        SVGLocatable loctable = (SVGLocatable)element;
        SVGMatrix matrix = loctable.getCTM();
        AffineTransform shapeTransform = new AffineTransform(matrix.getA(),
            matrix.getB(),matrix.getC(),matrix.getD(),matrix.getE(),
            matrix.getF());
        return new Point(
            (int)(shapeTransform.getTranslateX()/shapeTransform
                .getScaleX()),
            (int)(shapeTransform.getTranslateY()/shapeTransform
                .getScaleY()));
        // return new Point((int)shapeTransform.getTranslateX(),
        // (int)shapeTransform.getTranslateY());
        // System.out.println("Affine: "+shapeTransform);
        // System.out.println("Matrix: "+matrix.getA()+", "+matrix.getB()+", "+matrix.getC()+", "+matrix.getD()+", "+matrix.getE()+", "+matrix.getF());
        // System.out.println("transX,Y:"+shapeTransform.getTranslateX()+","+shapeTransform.getTranslateY()+". Scale x,y:"+shapeTransform.getScaleX()
        // +","+shapeTransform.getScaleY()+". Shear x,y: "+shapeTransform.getShearX()+","+shapeTransform.getShearY());
    }

    /**
     * Change the size set to a Document.
     * 
     * @param document The document to change size
     * @param size The size to set the SVG to
     * @since 2.4.2
     */
    public static void setSVGSize(SVGDocument document,Point size) {
        document.getDocumentElement().setAttribute("width",size.x+"");
        document.getDocumentElement().setAttribute("height",size.y+"");
    }

    /**
     * Set the X and Y on an element
     * 
     * {@docfacto.note This should not be used on live elements unless its in
     * the update manager thread}
     * 
     * @param element to set the location
     * @param location containing the new location
     * @since 2.4
     */
    public static void setElementLocation(Element element,Point2D location) {
        setAttribute(element,"x",location.getX());
        setAttribute(element,"y",location.getY());
    }

    /**
     * See if the element is text editable.
     * 
     * @param element to process
     * @return true if the group or element can be edited by a text editor
     * @since 2.4
     */
    public static boolean checkForTextEdit(Element element) {
        if (element==null) {
            return false;
        }

        if (element instanceof SVGOMTextElement) {
            return true;
        }

        if (element instanceof SVGOMGElement) {
            // Lets see the children see if they are all text?
            NodeList children = element.getChildNodes();
            for (int i = 0;i<children.getLength();i++) {
                Node node = children.item(i);
                if (node.getNodeType()==Node.ELEMENT_NODE) {
                    if (!(node instanceof SVGOMTextElement)) {
                        return false;
                    }
                }
            }
            return true;
        }

        return false;
    }

    /**
     * Gets the centre point of the element.
     * 
     * {@docfacto.system This uses instanceof to check the object type, and gets
     * the centre based upon its attributes. This way is preferred to
     * calculating the centre based on the element bounds, as when change an
     * elements rotation, its bounds may change, and therefore the centre point
     * changes too, causing an elements centre to be all over the place}
     * 
     * @param element The element to get a centre from
     * @param bridge The bridgecontext
     * @return The centre point of an element.
     * @since 2.4.2
     */
    public static Point getElementCentre(Element element,BridgeContext bridge) {
        GraphicsNode gn = bridge.getGraphicsNode(element);
        Rectangle2D r = gn.getPrimitiveBounds();
        return new Point((int)r.getCenterX(),(int)r.getCenterY());
    }

    /**
     * Return a list of elements under the root node
     * 
     * @param doc Document to process
     * @return A list of SVG elements under the root node
     * @since 2.4
     */
    public static List<Element> getSVGElements(SVGDocument doc) {
        SVGSVGElement root = doc.getRootElement();

        NodeList children = root.getChildNodes();
        List<Element> elements = new ArrayList<Element>(children.getLength());
        for (int i = 0;i<children.getLength();i++) {
            if (children.item(i) instanceof Element) {
                elements.add((Element)children.item(i));
            }
        }

        return elements;
    }

    /**
     * Gets all the elements from an element def and makes an svg around it.
     * 
     * @param def The def to get elements from
     * @return A new Document containing all the data within the element def
     * @throws IOException will not happen as its a string
     * @since 2.4.5
     */
    public static Document getSVGFromElementDef(ElementDef def)
    throws IOException {
        String svgContents = def.getValue();
        svgContents.replace("//<![CDATA[","");
        svgContents.replace("//]]>","");

        StringBuilder builder = new StringBuilder(100);
        builder.append("<svg xmlns=\"http://www.w3.org/2000/svg\">");
        builder.append(svgContents);
        builder.append("</svg>");

        StringReader reader = new StringReader(builder.toString());
        try {
            return theSVGDocumentFactory.createSVGDocument("",reader);
        }
        finally {
            reader.close();
        }
    }

    /**
     * Extracts the contents of an ElementDef to return a group Element.
     * 
     * @param def The elementdef to extract contents from
     * @return A group element containing the contents of the elementdef
     * @throws IOException Thrown when creating a file for the SVG Parsing
     * @since 2.4.5
     */
    public static Element getGroupElementFromElementDef(ElementDef def)
    throws IOException {
        Document doc = getSVGFromElementDef(def);
        NodeList children = doc.getDocumentElement().getChildNodes();

        ArrayList<Element> elements = new ArrayList<Element>(
            children.getLength());

        for (int i = 0;i<children.getLength();i++) {
            if (children.item(i).getNodeType()==Node.ELEMENT_NODE) {
                elements.add((Element)children.item(i));
            }
        }

        Element element;
        if (elements.get(0) instanceof SVGOMGElement) {
            element = elements.get(0);
        }
        else {
            element = doc.createElement("g");
            for (int i = 0;i<children.getLength();i++) {
                element.appendChild(children.item(i));
            }
        }

        return element;
    }

    /**
     * Builds a new SVG String with an embedded image
     * 
     * @param relativePath The path to the image
     * @param width The width of the image
     * @param height The height of the image
     * @return An SVG in string
     * @since 2.4.7
     */
    public static String createSVGWithImage(String relativePath,int width,
    int height) {
        StringBuilder builder = new StringBuilder();
        builder.append("<?xml version=\"1.0\" standalone=\"no\" ?> ");
        builder.append("<svg");
        builder.append(" width=\""+width+"\" height=\""+height+"\"");
        builder.append(" xmlns=\"http://www.w3.org/2000/svg\" "
            +"xmlns:xlink=\"http://www.w3.org/1999/xlink\" >");

        builder
            .append("\n <image xmlns:xlink=\"http://www.w3.org/1999/xlink\"");
        builder.append(" width=\""+width+"\"");
        builder.append(" height=\""+height+"\"");
        builder.append(" x=\"0\" y=\"0\"");
        builder.append(" xlink:href=\""+relativePath+"\"></image> \n");

        builder.append("</svg>");

        return builder.toString();
    }

    /**
     * Returns the element id.
     * 
     * If no id is present, then one is created.
     * 
     * @param element to check
     * @return an Id
     * @since 2.4
     */
    public static String getOrCreateId(Element element) {
        String id = element.getAttribute("id");
        if (id.isEmpty()) {
            id = XMLUtils.generateUnqiueID();
            element.setAttribute("id",id);
        }

        return id;
    }

    /**
     * Set a docfacto name spaced attribute on an element
     * 
     * @param element set set the attributes on
     * @param name attribute name
     * @param value attribute value
     * @since 2.4
     */
    public static void setDocfactoAttribute(Element element,String name,
    String value) {
        element.setAttributeNS(DOCFACTO_NAME_SPACE,DOCFACTO_QUALIFIED_NAME
            +":"+name,value);
    }

    /**
     * Return the docfacto name spaced attribute value
     * 
     * @param element to check
     * @param name of the attribute
     * @return the attribute value, if any
     * @since 2.4
     */
    public static String getDocfactoAttribute(Element element,String name) {
        return element.getAttributeNS(DOCFACTO_NAME_SPACE,name);
    }

    /**
     * Check to see if a docfacto name spaced attribute exists
     * 
     * @param element to check
     * @param name of attribute
     * @return true if there is an attribute within the docfacto name space
     * @since 2.4
     */
    public static boolean hasDocfactoAttribute(Element element,String name) {
        return element.hasAttributeNS(DOCFACTO_NAME_SPACE,name);
    }

    /**
     * Remove a attribute from the docfacto name space
     * 
     * @param element to remove the attribute
     * @param name of the attribute
     * @since 2.4
     */
    public static void removeDocfactoAttribute(Element element,String name) {
        element.removeAttributeNS(DOCFACTO_NAME_SPACE,name);
    }

    /**
     * Remove all of the docfacto name space attributes
     * 
     * @param element to remove all of the elements
     * @since 2.4
     */
    public static void removeDocfactoAttributes(Element element) {
        NamedNodeMap attrs = element.getAttributes();
        for (int i = 0;i<attrs.getLength();i++) {
            Attr attr = (Attr)attrs.item(i);
            if (DOCFACTO_NAME_SPACE.equals(attr.getNamespaceURI())) {
                removeDocfactoAttribute(element,attr.getLocalName());
            }
        }
    }

    /**
     * Set the attribute using NumberFormat
     * 
     * @param element to set the attribute on
     * @param attributeName to set
     * @param value to set
     * @since 2.5
     */
    public static void setAttribute(Element element,String attributeName,
    float value) {
        element.setAttribute(attributeName,NUMBER_FORMAT.get().format(value));
    }

    /**
     * Set the attribute using NumberFormat
     * 
     * @param element to set the attribute on
     * @param attributeName to set
     * @param value to set
     * @since 2.5
     */
    public static void setAttribute(Element element,String attributeName,
    double value) {
        element.setAttribute(attributeName,NUMBER_FORMAT.get().format(value));
    }

    /**
     * Set the attribute using NumberFormat
     * 
     * @param element to set the attribute on
     * @param attributeName to set
     * @param value to set
     * @since 2.5
     */
    public static void setAttribute(Element element,String attributeName,
    int value) {
        element.setAttribute(attributeName,Integer.toString(value));
    }

    /**
     * Set a translate on a transform attribute
     * 
     * @param element to set the transform
     * @param location for the translate
     * @since 2.5
     */
    public static void setTransformTranslate(Element element,Point2D location) {
        StringBuilder builder = new StringBuilder(50);
        builder.append("translate(");
        builder.append(NUMBER_FORMAT.get().format(location.getX()));
        builder.append(", ");
        builder.append(NUMBER_FORMAT.get().format(location.getY()));
        builder.append(")");

        element.setAttribute("transform",builder.toString());
    }

    /**
     * Set the location of group or text depending on the hot spot location
     * 
     * {@docfacto.note text height needs to be supplied, as this might not be a
     * live element and therefore have no bounds }
     * 
     * @param element to set to location for
     * @param hotSpot the hot spot
     * @param textHeight height of the text (bounds height)
     * @param transform to use
     * @return the location of the new position
     * @since 2.4
     */
    public static Point2D getTextLocationWithHotSpot(Element element,
    HotSpot hotSpot,int textHeight,AffineTransform transform) {

        int halfTextHeight = textHeight/2;

        Point2D newPoint =
            SVGUtils.getTransformedPoint(hotSpot.getBounds().getCenterX(),
                hotSpot.getBounds().getCenterY(),transform);

        if (element instanceof SVGGElement) {
            // Group positions are top right
            return new Point2D.Double(newPoint.getX(),
                newPoint.getY()-(halfTextHeight/2));
        }

        // Text positions are bottom left
        if (hotSpot.getLocation()==HotSpot.NORTH) {
            return new Point2D.Double(newPoint.getX(),
                newPoint.getY()-(halfTextHeight/2));
        }

        if (hotSpot.getLocation()==HotSpot.SOUTH) {
            return new Point2D.Double(newPoint.getX(),
                newPoint.getY()+textHeight);
        }

        return new Point2D.Double(newPoint.getX(),newPoint.getY()
            +halfTextHeight);
    }

    /**
     * Check to see if the element is anchored text
     * 
     * @param element to check
     * @return true if the text is anchored to something
     * @since 2.5
     */
    public static boolean isAnchorableText(Element element) {
        return hasDocfactoAttribute(element,TextHandler.ANCHOR_ATTRIBUTE);
    }

    /**
     * Check to see if the element is an connector / anchored text
     * 
     * @param element to check
     * @return true if its a connector or anchored text
     * @since 2.4
     */
    public static boolean isAnchorableElement(Element element) {
        return (hasDocfactoAttribute(element,
            ConnectorHandler.CONNECTOR_START_ATTRIBUTE)||isAnchorableText(element));
    }

    /**
     * Calculate the transformed shape
     * 
     * @param canvas beermat canvas
     * @param g graphics node
     * @return the transformed shape
     * @since 2.5
     */
    public static java.awt.Rectangle calculateTransformedBounds(GraphicsNode g,
    final BeermatAWTCanvas canvas) {
        AffineTransform elementsAt = g.getGlobalTransform();
        Shape selectionHighlight = g.getOutline();
        AffineTransform at = canvas.getViewBoxTransform();
        at.concatenate(elementsAt);
        return at.createTransformedShape(selectionHighlight).getBounds();
    }
    
    /**
     * Apply the scale from the old element to the new element.
     * 
     * @param newElement to apply the scale
     * @param oldElement which has the scale
     * @since 2.5
     */
    public static void retainScale(Element newElement, Element oldElement) {
        if(oldElement != null) {
            // Retain scale of last element
            TransformElementParser parser = new TransformElementParser(oldElement);
            Point2D.Float scale = parser.getScale();
            
            parser = new TransformElementParser(newElement);
            parser.setScale(scale.x,scale.y);
        }
    }
}
