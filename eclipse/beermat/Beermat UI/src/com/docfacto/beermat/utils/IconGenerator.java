package com.docfacto.beermat.utils;

import org.apache.batik.swing.JSVGCanvas;
import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGLocatable;
import org.w3c.dom.svg.SVGRect;

import com.docfacto.beermat.svg.TransformElementParser;
import com.docfacto.beermat.ui.toolbar.shapes.CustomShapeHandler;

/**
 * Creates icons from SVG.
 * 
 * @author kporter - created Jul 12, 2013
 * @since 2.4.2
 */
public class IconGenerator {    
    /**
     * The padding to set around a path..
     * By default set to 5.
     */
//    public int padding = 5;
    /**
     * The fill of the shape in this icon. By default set to no fill.
     */
    public String fill = "none";
    
    /**
     * The strokes colour. Default is black.
     */
    public String stroke = "#000000";
    
    /**
     * The stroke set to the path. By default set to 5.
     */
    public String strokeWidth = "5";
        
    /**
     * Create an icon from a svg path.
     * 
     * Given a valid SVG path, creates an icon to the specified size and path, using the SVGRasterizer.
     * 
     * @param elementPath The path of the element to iconify
     * @param iconSize The size of the icon created in pixels
     * @param outputLocation The location of the icon file, include the leading /
     * @param filename The name of the icon file.
     * @throws Exception IO
     * @since 2.4.2
     */
    public void pathToIcon(String elementPath, Point iconSize, String outputLocation, String filename) throws Exception{
        Document document = SVGUtils.createSVGDocument(null);
        
        JSVGCanvas canvas = new JSVGCanvas();
        canvas.setDocumentState(JSVGCanvas.ALWAYS_DYNAMIC);
        canvas.setDocument(document);
        
        Element element = ElementUtils.createPath(document,new Point(0,0));
        element.setAttribute("d",elementPath);
        element.setAttribute("fill",fill);
        element.setAttribute("stroke",stroke);
        element.setAttribute("stroke-width",strokeWidth);
        
        //Remove this, add load listener to canvas, and do the following after document has been rendered
        Thread.sleep(500);
        
        
        SVGLocatable locatable = (SVGLocatable)element;
        SVGRect box = locatable.getBBox();
        
        float x = iconSize.x;
        float y = iconSize.y;
        
        float xScale = x / box.getWidth();
        float yScale = y / box.getHeight();
        
        TransformElementParser transformer = new TransformElementParser(element);
        transformer.setScale(xScale,yScale);
        document.getDocumentElement().setAttribute("width",x+"");
        document.getDocumentElement().setAttribute("height",y+"");
        SVGRasterizer.exportToPNG(document, outputLocation, filename);   
    }
    
    /**
     * Creates an icon from a shapehandler.
     * 
     * @param pathShape The CustomShapeHandler from which to get the path and filename from.
     * @param size The size of the icon created in pixels
     * @param outputLocation The location of the icon file, include the leading /
     * @throws Exception IO
     * @since 2.4.2
     */
    public void shapeToIcon(CustomShapeHandler pathShape, Point size, String outputLocation) throws Exception{
        pathToIcon(pathShape.getPath(),size,outputLocation, pathShape.getName());
    }
    
}
