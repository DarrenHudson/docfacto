package com.docfacto.beermat.utils;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.graphics.Point;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.docfacto.common.NameValuePair;

/**
 * Utils for modify Element's.
 * 
 * @author kporter - created Jun 21, 2013
 * @since 2.4
 */
public class ElementUtils {
    public final static String ID_ARROW_HEAD = "arrow.head";
    public final static String ID_ARROW_TAIL = "arrow.tail";

    /**
     * Creates an arrow and adds it to the document. Can be bidirectional
     * 
     * @since 2.2.0
     */
    public static Element createArrow(Document document,Point position,
    boolean biDirectional) {
        Element line = createLine(document,position);
        Element marker = createArrowMarker(document,false);
        document.getDocumentElement().appendChild(marker);
        line.setAttribute("marker-end","url(#"+marker.getAttribute("id")+")");

        if (biDirectional) {
            Element tailMarker = createArrowMarker(document,true);
            document.getDocumentElement().appendChild(tailMarker);

            line.setAttribute("marker-start",
                "url(#"+tailMarker.getAttribute("id")+")");
        }

        document.getDocumentElement().appendChild(line);
        return line;
    }

    public static void addMarkers(Document document,Element line,
    boolean biDirectional) {

        Element marker = createArrowMarker(document,false);
        document.getDocumentElement().appendChild(marker);
        line.setAttribute("marker-end","url(#"+marker.getAttribute("id")+")");

        if (biDirectional) {
            Element tailMarker = createArrowMarker(document,true);
            document.getDocumentElement().appendChild(tailMarker);

            line.setAttribute("marker-start",
                "url(#"+tailMarker.getAttribute("id")+")");
        }

    }

    /**
     * Appends this marker to the DOM or does nothing if it already exists.
     * 
     * @param document The Document
     * @param marker The element
     * @param id ID of the marker
     * @return the ID of the marker if it has changed.
     * @since 2.4.5
     */
    public static String addMarker(Document document,Element marker,String id,
    boolean reverseMarker) {
        if (document.getElementById(id)==null) {
            if (reverseMarker) {
                Element reversedMarker;
                try {
                    reversedMarker = (Element)marker.cloneNode(true);
                    document.getDocumentElement().appendChild(reversedMarker);
                }
                catch (DOMException e) {
                    reversedMarker = (Element)document.importNode(marker,true);
                    document.getDocumentElement().appendChild(reversedMarker);
                }
                NodeList children = reversedMarker.getChildNodes();
                for (int i = 0;i<children.getLength();i++) {
                    // Check if element type
                    if (children.item(i).getNodeType()==Node.ELEMENT_NODE) {
                        Element e = (Element)children.item(i);
                        String transform = e.getAttribute("transform");
                        e.setAttribute("transform",transform+" rotate(180)");
                    }
                }
                reversedMarker.setAttribute("id",id);
            }
            else {
                document.importNode(marker,true);
                document.getDocumentElement().appendChild(marker);
            }
        }
        return id;
    }

    /**
     * Creates an arrow marker. {@docfacto.note does not add the marker to the
     * document}
     * 
     * With the boolean set to true, an arrow head facing reverse is created
     * 
     * @param reverseArrowHead Whether to create a normal arrow head or reversed
     * @return The created arrowhead.
     * @since 2.2.0
     */
    private static Element createArrowMarker(Document document,boolean reverse) {
        if ((document.getElementById(ID_ARROW_HEAD)!=null)&&!reverse) {
            return document.getElementById(ID_ARROW_HEAD);
        }
        else if (document.getElementById(ID_ARROW_TAIL)!=null) {
            return document.getElementById(ID_ARROW_TAIL);
        }
        Element marker =
            document.createElementNS(SVGUtils.SVG_NAMESPACE,"marker");
        marker.setAttribute("overflow","visible");
        marker.setAttribute("refX","0");
        marker.setAttribute("refY","0");
        marker.setAttribute("orient","auto");
        marker.setAttribute("markerUnits","strokeWidth");

        Element path =
            document.createElementNS(SVGUtils.SVG_NAMESPACE,"path");
        path.setAttribute("d","M 0,0 L 5,-5 L -10,0 L 5,5 L 0,0 z");
        path.setAttribute("fill","black");

        if (!reverse) {
            marker.setAttribute("id",""+ID_ARROW_HEAD);
            path.setAttribute("transform",
                "scale(0.7) rotate(180)");
        }
        else {
            marker.setAttribute("id",""+ID_ARROW_TAIL);
            path.setAttribute("transform","scale(0.7) translate(5,0)");
        }
        marker.appendChild(path);
        return marker;
    }

    /**
     * Creates a circle Element in the given Document, of unknown radius.
     * <p>
     * Creates a circle, settings its cx and cy (the centre coordinates of the
     * circle) to the given position. {@docfacto.note This method adds the
     * element to the document for convenience.}
     * </p>
     * 
     * @param document The document to create the element in
     * @param position The starting position (centre).
     * @since 2.4
     */
    public static Element createCircle(Document document,Point position) {
        return createCircle(document,position,1);
    }

    /**
     * Creates a circle Element with a known radius
     * <p>
     * Creates a circle, sets its cx and cy to the position given, and sets its
     * radius.
     * </p>
     * <p>
     * {@docfacto.note This method adds the element to the document for
     * convenience.}
     * </p>
     * 
     * @param document The document to create the element in
     * @param position The starting position (centre).
     * @param radius The radius of the circle.
     * @since 2.4.3
     */
    public static Element createCircle(Document document,Point position,
    float radius) {
        Element circle =
            document.createElementNS(SVGUtils.SVG_NAMESPACE,"circle");
        SVGUtils.setAttribute(circle,"r",radius);
        SVGUtils.setAttribute(circle,"cx",position.x);
        SVGUtils.setAttribute(circle,"cy",position.y);
        document.getDocumentElement().appendChild(circle);
        return circle;
    }

    /**
     * Creates an ellipse Element in the given Document
     * <p>
     * Creates an ellipse, settings its cx and cy (the centre coordinates of the
     * ellipse) to the given position. {@docfacto.note This method adds the
     * element to the document for convenience.}
     * </p>
     * 
     * @param document The document to create the element in
     * @param position The starting position (centre).
     * @return The created Ellipse Element
     * @since 2.4
     */
    public static Element createEllipse(Document document,Point position) {
        return createEllipse(document,position,1F,1F);
    }

    /**
     * Creates an ellipse Element of known radius size.
     * <p>
     * Creates an ellipse, settings its cx and cy (the centre coordinates of the
     * ellipse) to the given position, as well as setting its rx and ry values
     * </p>
     * <p>
     * {@docfacto.note This method adds the element to the document for
     * convenience.}
     * </p>
     * 
     * @param document The document to create the element in
     * @param position The starting position (centre).
     * @param radiusX The rx to set
     * @param radiusY The ry to set
     * @return The created Ellipse Element
     * @since 2.4.3
     */
    public static Element createEllipse(Document document,Point position,
    float radiusX,float radiusY) {
        Element ellipse =
            document.createElementNS(SVGUtils.SVG_NAMESPACE,"ellipse");
        SVGUtils.setAttribute(ellipse,"cx",position.x);
        SVGUtils.setAttribute(ellipse,"cy",position.y);
        SVGUtils.setAttribute(ellipse,"rx",radiusX);
        SVGUtils.setAttribute(ellipse,"ry",radiusY);
        document.getDocumentElement().appendChild(ellipse);
        return ellipse;
    }

    /**
     * Creates an Line Element in the given Document
     * <p>
     * Sets its x1 and y1 (the start coordinates of the line) to the given
     * position. {@docfacto.note This method adds the element to the document
     * for convenience.}
     * </p>
     * 
     * @param document The document to create the element in
     * @param position The starting position of the line.
     * @return The created Line Element
     * @since 2.4
     */
    public static Element createLine(Document document,Point position) {
        Element element =
            document.createElementNS(SVGUtils.SVG_NAMESPACE,"line");
        SVGUtils.setAttribute(element,"x1",position.x);
        SVGUtils.setAttribute(element,"y1",position.y);
        SVGUtils.setAttribute(element,"x2",position.x);
        SVGUtils.setAttribute(element,"y2",position.y);
        document.getDocumentElement().appendChild(element);
        return element;
    }

    /**
     * Creates a path element on the given document.
     * <p>
     * Creates a path starting at the given position on the document.
     * </p>
     * 
     * @param document The Document in which to create the path on.
     * @param position The starting position of this path.
     * @return The created path element.
     * @since 2.4
     */
    public static Element createPath(Document document,Point position) {
        Element path =
            document.createElementNS(SVGUtils.SVG_NAMESPACE,"path");
        if (position==null) {
            position = new Point(0,0);
        }
        path.setAttribute("d","m0 0");
        path.setAttribute("transform","translate("+position.x+" "+position.y+
            ")");
        document.getDocumentElement().appendChild(path);
        return path;
    }

    /**
     * Creates a new rectangle Element in the given Document with an unknown
     * size.
     * <p>
     * The method creates a 'rect' Element in the given SVG Document. Using the
     * Point position to give it a starting x and y. {@docfacto.note This method
     * adds the element to the document for convenience.}
     * </p>
     * 
     * @param document The document to create this element in.
     * @param position The x and y Point to create the element
     * @return The element that was created.
     * @since 2.4
     */
    public static Element createRectangle(Document document,Point position) {
        return createRectangle(document,position,1,1);
    }

    /**
     * Creates a new rectangle Element in the given Document with a known size.
     * 
     * 
     * 
     * <p>
     * The method creates a 'rect' Element in the given SVG Document. Using the
     * Point position to give it a starting x and y, and the width and height
     * floats for sizing. {@docfacto.note This method adds the element to the
     * document for convenience.}
     * </p>
     * 
     * @param document The document to create this element in.
     * @param position The x and y Point to create the element
     * @param width The width to set
     * @param height The height to set
     * @return The element that was created.
     * @since 2.4.3
     */
    public static Element createRectangle(Document document,Point position,
    float width,float height) {
        Element rect = document.createElementNS(SVGUtils.SVG_NAMESPACE,"rect");
        SVGUtils.setAttribute(rect,"x",position.x);
        SVGUtils.setAttribute(rect,"y",position.y);
        SVGUtils.setAttribute(rect,"width",width);
        SVGUtils.setAttribute(rect,"height",height);
        document.getDocumentElement().appendChild(rect);
        return rect;
    }

    /**
     * Creates a group and sets an attribute of systemLocale to a given string.
     * 
     * @param doc The document in which to create this group
     * @param locale The locale to set to group
     * @return The created group
     * @since 2.4
     */
    public static Element createSystemLocaleGroup(Document doc,String locale) {
        Element group = doc.createElementNS(SVGUtils.SVG_NAMESPACE,"g");
        group.setAttribute("systemLanguage",locale);
        return group;
    }

    /**
     * A method to create a text element and append it to the SVG.
     * {@docfacto.note As text is usually added to a group this will not append
     * element to SVG}
     * 
     * @param document The document to create the element in.
     * @param contents The text to add to the SVG
     * @return The Text element created.
     * @since 2.2.3
     */
    public static Element createText(Document document,String contents) {
        Element text =
            document.createElementNS(SVGUtils.SVG_NAMESPACE,"text");
        Node theText = document.createTextNode(contents);
        text.appendChild(theText);
        return text;
    }

    /**
     * Updates a circle to the largest difference.
     * 
     * The method takes both x and y difference's but checks the largest radius
     * before setting it.
     * 
     * @param circle The element to update.
     * @param radius The difference that defines the radius
     * @since 2.2.0
     */
    public static void updateCircle(Element circle,Point radius) {
        int xDiff = radius.x;
        int yDiff = radius.y;
        int r = Math.abs(xDiff)>Math.abs(yDiff) ? Math.abs(xDiff) : Math
            .abs(yDiff);
        SVGUtils.setAttribute(circle,"r",r);
    }

    /**
     * Updates an ellipse.
     * 
     * @param radius The difference in x(width) and y(height) between the start
     * position of the ellipse.
     * @param ellipse The ellipse to update.
     * @since 2.2.0
     */
    public static void updateEllipse(Element ellipse,Point radius) {
        int xDiff = Math.abs(radius.x);
        int yDiff = Math.abs(radius.y);
        SVGUtils.setAttribute(ellipse,"rx",xDiff);
        SVGUtils.setAttribute(ellipse,"ry",yDiff);
    }

    /**
     * Updates the end position of a line.
     * 
     * @param element The line element to modify
     * @param last The end position for the line
     * @since 2.4
     */
    public static void updateLine(Element element,Point last) {
        SVGUtils.setAttribute(element,"x2",last.x);
        SVGUtils.setAttribute(element,"y2",last.y);
    }

    /**
     * Update a path by adding to the end of it.
     * <p>
     * The difference given should be the number of pixels representing the
     * difference between the last point on the path and where the path should
     * move to.
     * </p>
     * 
     * @param path The path to update
     * @param diff The difference in relative position to add to this path.
     * @since 2.4
     */
    public static void updatePath(Element path,Point2D diff) {
        String currentPath = path.getAttribute("d");
        if (!currentPath.endsWith(" ")) {
            currentPath = currentPath+" ";
        }
        path.setAttribute("d",currentPath+diff.getX()+","
            +diff.getY()+" ");
    }

    /**
     * Updates a rectangles positional data (or square with constrained set).
     * 
     * First calculate whether the drag is in the inverse direction, if so the
     * startPoint is recalculated and the height+width are calculated using this
     * new start position and the original drag point.
     * <p>
     * If constrained is set, then the height and width will always equal the
     * longest factor
     * </p>
     * 
     * @param element The element to update.
     * @param startPoint The original position the rect was dragged from.
     * @param lastPoint The last position the rect was dragged to.
     * @param constrained Whether the rect should have constrained width/height
     * @since 2.2.1
     */
    public static void updateRect(final Element element,Point startPoint,
    Point lastPoint,boolean constrained) {
        boolean inverseX = false;
        boolean inverseY = false;
        if (lastPoint.x<startPoint.x) {
            int temp = startPoint.x;
            startPoint = new Point(lastPoint.x,startPoint.y);
            lastPoint = new Point(temp,lastPoint.y);
            inverseX = true;
        }
        if (lastPoint.y<startPoint.y) {
            int temp = startPoint.y;
            startPoint = new Point(startPoint.x,lastPoint.y);
            lastPoint = new Point(lastPoint.x,temp);
            inverseY = true;
        }
        int width = (lastPoint.x-startPoint.x);
        int height = (lastPoint.y-startPoint.y);
        if (constrained) {
            if (width<height) {
                width = height;
                if (inverseX) {
                    startPoint = new Point(lastPoint.x-width,startPoint.y);
                }
            }
            if (height<width) {
                height = width;
                if (inverseY) {
                    startPoint = new Point(startPoint.x,lastPoint.y-height);
                }
            }
        }
        updateRect(element,startPoint.x,startPoint.y,width,height);
    }

    /**
     * Set the rect to a known x,y,width and height.
     * 
     * @param element The rect element that is being set.
     * @param x The x position to set
     * @param y The y position to set
     * @param width The width to set
     * @param height The height to set
     * @since 2.4
     */
    public static void updateRect(Element element,int x,int y,int width,
    int height) {
        SVGUtils.setAttribute(element,"x",x);
        SVGUtils.setAttribute(element,"y",y);
        SVGUtils.setAttribute(element,"width",width);
        SVGUtils.setAttribute(element,"height",height);
    }

    /**
     * Brings an element forward or backwards by a single layer.
     * 
     * Takes a given direction, and moves the element forward or backwards, or
     * to the start.
     * 
     * @param document
     * @param element
     * @param direction The direction of movement
     * @since 2.4.2
     */
    public static void moveElement(Element element,
    ElementMovement direction) {
        // If position of element is unknown
        // First find element to move within the children, get its index
        // Then get the element before -1 or after +2
        // Checking that element is not first or last
        // Then insertBefore that element.

        // For methods of moveElementToStart
        // Or moveElementToEnd,
        // Just push element before the 0 child, or insertBefore with null ref
        // to append to end
        Document document = element.getOwnerDocument();

        NodeList all = document.getDocumentElement().getChildNodes();
        ArrayList<Element> elements = new ArrayList<Element>(all.getLength());

        for (int i = 0;i<all.getLength();i++) {
            if (all.item(i).getNodeType()==Node.ELEMENT_NODE) {
                elements.add((Element)all.item(i));
            }
        }

        if (direction==ElementMovement.FRONT) {
            document.getDocumentElement().appendChild(element);
            return;
        }

        if (direction==ElementMovement.BACK) {
            document.getDocumentElement().insertBefore(element,elements.get(0));
            return;
        }

        int position = -1;
        Element insertBefore = null;

        for (int i = 0;i<elements.size();i++) {
            if (element==elements.get(i)) {
                position = i;
            }
        }

        if ((position==0&&direction==ElementMovement.BACKWARD)
            ||(position==elements.size()-1&&direction==ElementMovement.FORWARD)
            ||position==-1) {
            return;
        }
        else if (position==elements.size()-2&&
            direction==ElementMovement.FORWARD) {
            document.getDocumentElement().insertBefore(element,null);
            return;
        }

        if (direction==ElementMovement.BACKWARD) {
            insertBefore = elements.get(position-1);
        }
        else if (direction==ElementMovement.FORWARD) {
            insertBefore = elements.get(position+2);
        }

        document.getDocumentElement().insertBefore(element,insertBefore);
    }

    /**
     * Gets attribute from an Element and makes an ElementAttributes
     * 
     * @param element The element to get attributes from
     * @return An ElementAttributes object
     * @since 2.4.4
     */
    public static ElementAttributes getElementAttributes(Element element) {
        ElementAttributes attr = new ElementAttributes();
        attr.setFill(element.getAttribute(SVGUtils.FILL));
        attr.setStrokeColour(element.getAttribute(SVGUtils.STROKE_COLOUR));
        attr.setStrokeWidth(element.getAttribute(SVGUtils.STROKE_WIDTH));
        attr.setDash(element.getAttribute(SVGUtils.DASH_ARRAY));

        if (attr.getDash().isEmpty()) {
            attr.setDash("solid");
        }
        if (attr.getFill().isEmpty()) {
            attr.setFill("none");
        }

        return attr;
    }

    /**
     * Set all the attributes in an ElementAttribute object to the given
     * element. {@docfacto.note Must be run in a runnable on the updateablequeue
     * in the update manager to show results on canvas without refresh}
     * 
     * @param element The element to set attributes to
     * @param attr The element attributes to set
     * @since 2.4.4
     */
    public static void setElementAttributes(Element element,
    ElementAttributes attr) {
        List<NameValuePair> attrs = attr.getAllAttributes();
        for (NameValuePair pair:attrs) {
            element.setAttribute(pair.getName(),pair.getValue());
        }
    }

    /**
     * Filter multiple types from a list of elements. {@docfacto.note
     * <b>Constructive</b> function. }
     * 
     * @param elements The list of elements to filter
     * @param types The types to filter out
     * @return The filtered list
     * @since 2.4.4
     */
    public static List<Element> filterElementType(List<Element> elements,
    List<Class<?>> types) {
        List<Element> newList = new ArrayList<Element>(elements.size());
        for (Element e:elements) {
            newList.add(e);
        }
        for (Class<?> type:types) {
            ElementUtils.filterElementTypeDestructive(newList,type);
        }
        return newList;
    }

    /**
     * Filters out elements of a given type from a list of elements
     * 
     * {@docfacto.note This method works <b>destructively</b> , thus removing
     * elements from the given list}
     * 
     * @param elements The list of elements to filter
     * @param type The type to filter out
     * @return A list of elements without the given type
     * @since 2.4.4
     */
    public static List<Element> filterElementTypeDestructive(
    List<Element> elements,Class<?> type) {
        List<Element> removals = new ArrayList<Element>(elements.size()/2);
        for (Element e:elements) {
            if (type.isInstance(e)) {
                removals.add(e);
            }
        }
        elements.removeAll(removals);
        return elements;
    }

    /**
     * Filters out elements of a given type from a list of elements
     * 
     * {@docfacto.note This method works <b>constructively</b>, returning a new
     * List and not destroying the given list}
     * 
     * @param elements The list of elements to filter
     * @param type The type to filter out
     * @return A list of elements without the given type
     * @since 2.4.4
     */
    public static List<Element> filterElementType(List<Element> elements,
    Class<?> type) {
        ArrayList<Element> newList = new ArrayList<Element>(elements.size()/2);
        for (Element e:elements) {
            if (!type.isInstance(e)) {
                newList.add(e);
            }
        }
        return newList;
    }

    /**
     * Recursively populate a list with all its children.
     * 
     * {@docfacto.note title="Returns element itself" The method always returns
     * the element itself in the list. So at least a list of 1 will always be
     * returned}
     * 
     * {@docfacto.system title="Recursive steps"
     * <ol>
     * <li>First check if element has no children, if true, add element to the
     * list, and return the list</li>
     * <li>If false, get the elements list of children, and loop through them,
     * calling this method for each child</li>
     * <li>At end of loop, add this element itself to the list</li>
     * <li>Return populated list</li></ol>}
     * 
     * @param element The element to recursively check
     * @param list The list to populate the children into
     * @return The populated list of an element and its children
     * @since 2.4.4
     */
    public static List<Element> getElementChildren(Element element,
    List<Element> list) {
        if (!element.hasChildNodes()) {
            list.add(element);
        }
        else {
            NodeList nl = element.getChildNodes();
            for (int i = 0;i<nl.getLength();i++) {
                if (nl.item(i).getNodeType()==1) {
                    getElementChildren((Element)nl.item(i),list);
                }
            }
            list.add(element);
        }

        return list;
    }

    /**
     * Get a list of all the children, and their children, from an element.
     * Convenience method for getAllChildren(Element, List<Element>)
     * 
     * @since 2.4.4
     */
    public static List<Element> getElementChildren(Element element) {
        List<Element> list =
            getElementChildren(element,new ArrayList<Element>(1));
        return list;
    }

}