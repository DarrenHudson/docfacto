package com.docfacto.beermat.hotspot;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import org.w3c.dom.Element;

import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.beermat.utils.BeermatUtils;
import com.docfacto.common.DocfactoException;

/**
 * This is the hot spot that makes its easier to align with other elements.
 * 
 * {@docfacto.note Line2D has no bounds, so hit detection is done via the isOn..
 * Methods}
 * 
 * @author dhudson - created 31 Oct 2013
 * @since 2.4
 */
public class LocatingHotSpot extends CentreHotSpot {

    private Line2D theNorth;
    private Line2D theSouth;
    private Line2D theEast;
    private Line2D theWest;

    private static final int MARGIN = 5;

    /**
     * Constructor.
     * 
     * @param canvas beermat canvas
     * @param element to highlight
     * @since 2.4
     */
    public LocatingHotSpot(BeermatAWTCanvas canvas,Element element) throws DocfactoException {
        super(canvas,element);
        calculateLines();
    }

    /**
     * Check to see if the centre of the hot spot hits one of the lines
     * 
     * @param hotSpot to check
     * @return true if the centre of the hot spot hits one of the lines
     * @since 2.4
     */
    public boolean isInHotSpot(HotSpot hotSpot) {
        int x = (int)hotSpot.getBounds().getCenterX();
        int y = (int)hotSpot.getBounds().getCenterY();

        if (isOnVerticalLine(theNorth,x,y)) {
            return true;
        }

        if (isOnVerticalLine(theSouth,x,y)) {
            return true;
        }

        if (isOnHorizontalLine(theEast,x,y)) {
            return true;
        }

        if (isOnHorizontalLine(theWest,x,y)) {
            return true;
        }

        return false;
    }

    /**
     * Check to see if the point is on the line
     * 
     * @param line to check
     * @param x point x
     * @param y point y
     * @return true if the x,y hits the line
     * @since 2.4
     */
    private boolean isOnVerticalLine(Line2D line,int x,int y) {
        int lineX = (int)line.getX1();

        if (x!=lineX) {
            // Can't be ..
            return false;
        }

        if (line.getY1()>line.getY2()) {
            return (y>=line.getY2()&&y<=line.getY1());
        }

        return (y>=line.getY1()&&y<=line.getY2());

    }

    /**
     * Check to see if the point hits the horizontal line
     * 
     * @param line to check
     * @param x point x
     * @param y point y
     * @return true if the x,y hits the line
     * @since 2.4
     */
    private boolean isOnHorizontalLine(Line2D line,int x,int y) {

        int lineY = (int)line.getY1();

        if (y!=lineY) {
            // Can't be
            return false;
        }

        if (line.getX1()>line.getX2()) {
            return (x>=line.getX2()&&x<=line.getX1());
        }

        return (x>=line.getX1()&&x<=line.getX2());
    }

    /**
     * Draw the 4 lines for the locating hot spot
     * 
     * @param g2d to draw on
     * @since 2.4
     */
    public void render(Graphics2D g2d) {

        g2d.setStroke(BeermatUtils.HIGHLIGHT_STROKE);
        g2d.setColor(HotSpot.HOTSPOT_COLOUR);

        // It moves, calculate the bounds
        calculateTransformedBounds();

        calculateLines();

        g2d.draw(theEast);
        g2d.draw(theNorth);
        g2d.draw(theWest);
        g2d.draw(theSouth);
    }

    /**
     * Calculate the lines to be drawn
     * 
     * @since 2.4
     */
    private void calculateLines() {
        Rectangle2D bounds = getTransformedBounds();
        Rectangle visRect = getBeermatCanvas().getVisibleRect();

        theWest = new Line2D.Double(visRect.x,bounds.getCenterY(),
            bounds.getCenterX()-MARGIN,bounds.getCenterY());

        theNorth = new Line2D.Double(bounds.getCenterX(),bounds.getCenterY()-
            MARGIN,bounds.getCenterX(),0);

        theEast = new Line2D.Double(bounds.getCenterX()+MARGIN,
            bounds.getCenterY(),visRect.width,
            bounds.getCenterY());

        theSouth = new Line2D.Double(bounds.getCenterX(),bounds.getCenterY()+
            MARGIN,bounds.getCenterX(),visRect.height);
    }
}
