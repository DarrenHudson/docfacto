package com.docfacto.beermat.hotspot;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;

import org.apache.batik.gvt.GraphicsNode;
import org.w3c.dom.Element;

import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.beermat.utils.BeermatUtils;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.DocfactoException;

/**
 * An Abstract class which handles the highlighting of a hot spot it the cursor
 * is over it.
 * 
 * @author dhudson - created 28 Oct 2013
 * @since 2.4
 */
public abstract class HotSpotHighlightCollection extends HotSpotCollection {

    private HotSpot theHighlighted;
    private Rectangle theDirtyBounds;

    /**
     * Constructor.
     * 
     * @param canvas beermat canvas
     * @param node graphics node
     * @throws DocfactoException if node not visible
     * @since 2.4
     */
    public HotSpotHighlightCollection(BeermatAWTCanvas canvas,GraphicsNode node)
    throws DocfactoException {
        super(canvas,node);
    }

    /**
     * Return a slightly bigger area than the graphics node as hot spots make
     * the area bigger.
     * 
     * @return a rectangle of a dirty area
     * @since 2.4
     */
    public Rectangle getDirtyBounds() {
        if (theDirtyBounds==null) {
            theDirtyBounds = BeermatUtils.outset(getTransformedBounds(),10);
        }

        return theDirtyBounds;
    }

    /**
     * Check to see if a hot spot needs updating
     * 
     * @param x location
     * @param y location
     * @return true if the hot spot has changed
     * @since 2.4
     */
    public boolean updatePosition(int x,int y) {
        HotSpot hotSpot = isInHotSpot(x,y);

        if (hotSpot!=null) {
            return updateHighlighted(hotSpot);
        }

        // Its not in any hot spot
        if (theHighlighted==null) {
            // It was already null
            return false;
        }
        else {
            // Used to be a hot spot
            theHighlighted = null;
            return true;
        }
    }

    /**
     * Draw the hot spots on the graphics
     * 
     * @param g2d graphics
     * @since 2.4
     */
    public void render(Graphics2D g2d) {

        calculateTransformedBounds();
        createHotSpots();

        int location = -1;
        if (getHighlightedHotSpot()!=null) {
            location = getHighlightedHotSpot().getLocation();
        }

        g2d.setColor(HotSpot.HOTSPOT_COLOUR);

        for (HotSpot hotSpot:theHotSpots) {

            Rectangle bounds = hotSpot.getBounds();

            int x = (int)bounds.getCenterX();
            int y = (int)bounds.getCenterY();

            switch (hotSpot.getLocation()) {
            // End of the hot spot range
            case HotSpot.EAST:
                x = bounds.x+bounds.width;
                break;

            // Beginning of the hot spot range
            case HotSpot.WEST:
                x = bounds.x;
                break;
            }

            GraphicsNode node = getBeermatCanvas().getGraphicsNodeAt(x,y,0);
            boolean isPainting = true;
            // Have we found the node that we are highlighting?
            if (node!=getGraphicsNode()) {
                // Bounds need to be translated when dealing with elements
                Point2D newPoint =
                    SVGUtils.getTransformedPoint(x,y,getBeermatCanvas()
                        .getViewBoxTransform());

                Element element =
                    getBeermatCanvas().getElementAt((int)newPoint.getX(),
                        (int)newPoint.getY());
                isPainting = canPlaceHotSpotOver(element);
            }

            if (isPainting) {
                if (hotSpot.getLocation()==location) {
                    // Its a highlight
                    g2d.setColor(HotSpot.HOTSPOT_HIGHLIGHT_COLOUR);
                    g2d.fill(hotSpot.getShape());
                    g2d.setColor(HotSpot.HOTSPOT_COLOUR);
                }
                else {
                    g2d.fill(hotSpot.getShape());
                }
            }
        }
    }

    /**
     * Check to see if the hot spot can be drawn as there is something else at
     * the hot spot position.
     * 
     * @param element which is in the location of the hot spot
     * @return true if the hot spot is allowed to be drawn
     * @since 2.4
     */
    public abstract boolean canPlaceHotSpotOver(Element element);

    /**
     * Update the highlighted hot spot
     * 
     * @param hotSpot
     * @return true if the hot spot has changed
     * @since 2.4
     */
    private boolean updateHighlighted(HotSpot hotSpot) {
        if (theHighlighted==null) {
            theHighlighted = hotSpot;
            return true;
        }
        else {
            if (theHighlighted.getLocation()!=hotSpot.getLocation()) {
                theHighlighted = hotSpot;
                return true;
            }
        }

        return false;
    }

    /**
     * Sets the highlighted hot spot for a location
     * 
     * @param location of the hot spot
     * @since 2.5
     */
    public void setHighlightedHotSpot(int location) {
        theHighlighted = getHotSpotForLocation(location);
    }

    /**
     * Return the highlighted hot spot
     * 
     * @return the highlighted hot spot or null
     * @since 2.4
     */
    public HotSpot getHighlightedHotSpot() {
        return theHighlighted;
    }
}
