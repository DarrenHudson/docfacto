package com.docfacto.beermat.hotspot;

import java.awt.Rectangle;

import org.apache.batik.gvt.GraphicsNode;
import org.w3c.dom.Element;

import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.beermat.utils.BeermatUtils;
import com.docfacto.common.DocfactoException;

/**
 * This class represents a node that its connected via connectors (arrows)
 * 
 * @author dhudson - created 16 Oct 2013
 * @since 2.4
 */
public class NSEWHotSpots extends HotSpotHighlightCollection {

    private static final int HOTSPOT_SIZE = 10;

    /**
     * Constructor.
     * 
     * @param canvas beermat canvas
     * @param node to draw the hot spots around
     * @since 2.4
     */
    public NSEWHotSpots(BeermatAWTCanvas canvas,GraphicsNode node) throws DocfactoException {
        super(canvas,node);
    }
    
    /**
     * @see com.docfacto.beermat.hotspot.HotSpotCollection#getNumberOfHotSpots()
     */
    @Override
    public int getNumberOfHotSpots() {
        return 4;
    }

    /**
     * @see com.docfacto.beermat.hotspot.HotSpotCollection#createHotSpots()
     */
    @Override
    public void createHotSpots() {
        Rectangle bounds = BeermatUtils.outset(getTransformedBounds(),5);

        theHotSpots[0] = new CircleHotSpot(HotSpot.NORTH,bounds,HOTSPOT_SIZE);
        theHotSpots[1] = new CircleHotSpot(HotSpot.EAST,bounds,HOTSPOT_SIZE);
        theHotSpots[2] = new CircleHotSpot(HotSpot.SOUTH,bounds,HOTSPOT_SIZE);
        theHotSpots[3] = new CircleHotSpot(HotSpot.WEST,bounds,HOTSPOT_SIZE);
    }

    /**
     * @see com.docfacto.beermat.hotspot.HotSpotHighlightCollection#canPlaceHotSpotOver(org.w3c.dom.Element)
     */
    @Override
    public boolean canPlaceHotSpotOver(Element element) {
        return true;
    }
}
