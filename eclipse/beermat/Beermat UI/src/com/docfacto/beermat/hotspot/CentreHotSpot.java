package com.docfacto.beermat.hotspot;

import java.awt.Graphics2D;

import org.apache.batik.gvt.GraphicsNode;
import org.w3c.dom.Element;

import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.common.DocfactoException;

/**
 * Create a central hot spot for a graphics node
 * 
 * @author dhudson - created 24 Oct 2013
 * @since 2.4
 */
public class CentreHotSpot extends HotSpotCollection {

    /**
     * Constructor.
     * 
     * @see HotSpotCollection#HotSpotCollection(BeermatAWTCanvas, Element)
     * @since 2.4
     */
    public CentreHotSpot(BeermatAWTCanvas canvas,Element element) throws DocfactoException {
        super(canvas,element);
    }

    /**
     * Constructor.
     *
     * @param canvas beermat canvas
     * @param node graphics node
     * @throws DocfactoException if unable to create the hot spot
     * @since 2.5
     */
    public CentreHotSpot(BeermatAWTCanvas canvas,GraphicsNode node) throws DocfactoException {
        super(canvas,node);
    }
    
    /**
     * @see com.docfacto.beermat.hotspot.HotSpotCollection#getNumberOfHotSpots()
     */
    @Override
    public int getNumberOfHotSpots() {
        return 1;
    }

    /**
     * @see com.docfacto.beermat.hotspot.HotSpotCollection#createHotSpots()
     */
    @Override
    public void createHotSpots() {
        theHotSpots[0] =
            new CircleHotSpot(HotSpot.CENTRE,getTransformedBounds(),6);
    }

    /**
     * Return the single hot spot
     * 
     * @return the only hot spot
     * @since 2.4
     */
    public HotSpot getHotSpot() {
        return theHotSpots[0];
    }

    /**
     * Render a hot spot
     * 
     * @param g2d graphics to draw on
     * @param highlighted true if to be drawn highlighted
     * @since 2.4
     */
    public void render(Graphics2D g2d,boolean highlighted) {
        if (highlighted) {
            g2d.setColor(HotSpot.HOTSPOT_HIGHLIGHT_COLOUR);
        }
        else {
            g2d.setColor(HotSpot.HOTSPOT_COLOUR);
        }

        g2d.fill(getHotSpot().getShape());
    }
}
