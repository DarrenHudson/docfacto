package com.docfacto.beermat.hotspot;

import java.awt.Shape;
import java.awt.geom.Rectangle2D;

/**
 * Square hot spot
 * 
 * @author dhudson - created 18 Oct 2013
 * @since 2.4
 */
public class SquareHotSpot extends HotSpot {

    /**
     * Constructor.
     * 
     * @param location N/E/S/W location
     * @param highlight element
     * @param size of hot spot
     * @since 2.4
     */
    public SquareHotSpot(int location,Rectangle2D highlight,int size) {
        super(location,highlight,size,size);
    }

    /**
     * @see com.docfacto.beermat.hotspot.HotSpot#createHotSpotShape(double,
     * double)
     */
    @Override
    public Shape createHotSpotShape(double x,double y) {
        return new Rectangle2D.Double(x,y,getSize(),
            getSize());
    }
}
