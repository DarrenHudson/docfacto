package com.docfacto.beermat.hotspot;

import org.apache.batik.gvt.GraphicsNode;
import org.w3c.dom.Element;

import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.DocfactoException;

/**
 * A connector hot spot can change position depending on what position the line is in
 *
 * @author dhudson - created 27 Nov 2013
 * @since 2.5
 */
public class ConnectorHotSpot extends HotSpotHighlightCollection {

    /**
     * Constructor.
     * 
     * @param canvas the canvas
     * @param node graphics node to highlight
     * @throws DocfactoException if node not visible
     * @since 2.5
     */
    public ConnectorHotSpot(BeermatAWTCanvas canvas,GraphicsNode node) throws DocfactoException {
        super(canvas,node);
    }

    /**
     * @see com.docfacto.beermat.hotspot.HotSpotCollection#getNumberOfHotSpots()
     */
    @Override
    public int getNumberOfHotSpots() {
        return 1;
    }

    /**
     * @see com.docfacto.beermat.hotspot.HotSpotCollection#createHotSpots()
     */
    @Override
    public void createHotSpots() {
        theHotSpots[0] =
            new CircleHotSpot(HotSpot.CENTRE,getTransformedBounds(),6);
    }

    /**
     * Return the single hot spot
     * 
     * @return the only hot spot
     * @since 2.5
     */
    public HotSpot getHotSpot() {
        return theHotSpots[0];
    }
    
    /**
     * @see com.docfacto.beermat.hotspot.HotSpotHighlightCollection#canPlaceHotSpotOver(org.w3c.dom.Element)
     */
    @Override
    public boolean canPlaceHotSpotOver(Element element) {
        return !SVGUtils.checkForTextEdit(element);
    }
}
