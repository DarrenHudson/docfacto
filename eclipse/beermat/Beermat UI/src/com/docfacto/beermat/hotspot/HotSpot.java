package com.docfacto.beermat.hotspot;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import com.docfacto.beermat.ui.AbstractCursorManager;
import com.docfacto.beermat.ui.BeermatViewConstants;
import com.docfacto.beermat.utils.BeermatUtils;
import com.docfacto.core.utils.SWTUtils;

/**
 * A highlighted item will have 8 hot spots
 * 
 * @author dhudson - created 26 Jul 2013
 * @since 2.4
 */
public abstract class HotSpot {

    public static final int NORTH = 0;
    public static final int NORTH_EAST = 1;
    public static final int EAST = 2;
    public static final int SOUTH_EAST = 3;
    public static final int SOUTH = 4;
    public static final int SOUTH_WEST = 5;
    public static final int WEST = 6;
    public static final int NORTH_WEST = 7;
    public static final int CENTRE = 8;

    public static final Color HOTSPOT_COLOUR = SWTUtils
        .toAWTColor(BeermatViewConstants.COLOR_FOREGROUND_BRAND);
    public static final Color HOTSPOT_HIGHLIGHT_COLOUR = SWTUtils
        .toAWTColor(BeermatViewConstants.COLOR_FOREGROUND_COMPLEMENTARY);

    private final int theLocation;
    private Shape theShape;

    private Rectangle2D theHighlight;
    private final Rectangle theHotRegion;

    private int theDefaultCursor;

    private final int theHotSpotSize;
    private final int theHotSpotHalf;

    /**
     * Constructor.
     * 
     * @param location one of the constants
     * @param highlight the outline rect
     * @param size of the hot spot
     * @param feathering size of the region that will trigger the hot spot
     * @since 2.4
     */
    public HotSpot(int location,Rectangle2D highlight,int size,int feathering) {
        theLocation = location;
        theHighlight = highlight;
        theHotSpotSize = size;
        theHotSpotHalf = size/2;
        createShape();
        theHotRegion =
            BeermatUtils.outset(theShape.getBounds().getBounds(),feathering);
    }

    /**
     * Create a hot spot at a given location.
     * 
     * @param location one of the constant locations
     * @param x location
     * @param y location
     * @param size of hot spot
     * @param feathering size of the region that will trigger the hot spot
     */
    public HotSpot(int location,double x,double y,int size,int feathering) {
        theLocation = location;
        theHotSpotSize = size;
        theHotSpotHalf = size/2;
        theShape = createHotSpotShape(x,y);
        theHotRegion =
            BeermatUtils.outset(theShape.getBounds().getBounds(),feathering);
        theDefaultCursor = AbstractCursorManager.HAND;
    }

    /**
     * Return the size of the hot spot
     * 
     * @return the size of the hot spot
     * @since 2.4
     */
    public int getSize() {
        return theHotSpotSize;
    }

    /**
     * Return the hot spot shape
     * 
     * @return the hot spot shape
     * @since 2.4
     */
    public Shape getShape() {
        return theShape;
    }

    /**
     * Return the bounds of the hot spot
     * 
     * @return the hot spot bounds
     * @since 2.4
     */
    public Rectangle getBounds() {
        return theShape.getBounds();
    }

    /**
     * Depending on location, create the correct regions
     * 
     * @since 2.4
     */
    private void createShape() {

        switch (theLocation) {
        case NORTH:
            theShape =
                createHotSpotShape(theHighlight.getX()+
                    (theHighlight.getWidth()/2)-
                    theHotSpotHalf,
                    theHighlight.getY()-theHotSpotHalf);

            theDefaultCursor = AbstractCursorManager.N_RESIZE;
            break;

        case NORTH_EAST:
            theShape =
                createHotSpotShape(theHighlight.getX()+
                    theHighlight.getWidth()-theHotSpotHalf,
                    theHighlight.getY()-theHotSpotHalf);

            theDefaultCursor = AbstractCursorManager.NE_RESIZE;
            break;

        case EAST:
            theShape =
                createHotSpotShape(theHighlight.getX()+
                    (theHighlight.getWidth()-theHotSpotHalf),
                    theHighlight.getY()+(theHighlight.getHeight()/2)-
                        theHotSpotHalf);

            theDefaultCursor = AbstractCursorManager.E_RESIZE;
            break;

        case SOUTH_EAST:
            theShape =
                createHotSpotShape(theHighlight.getX()+
                    theHighlight.getWidth()-
                    theHotSpotHalf,(theHighlight.getY()+
                    theHighlight.getHeight())-
                    theHotSpotHalf);

            theDefaultCursor = AbstractCursorManager.SE_RESIZE;
            break;

        case SOUTH:
            theShape =
                createHotSpotShape(theHighlight.getX()+
                    (theHighlight.getWidth()/2)-theHotSpotHalf,
                    theHighlight.getY()+theHighlight.getHeight()-
                        theHotSpotHalf);

            theDefaultCursor = AbstractCursorManager.S_RESIZE;
            break;

        case SOUTH_WEST:
            theShape =
                createHotSpotShape(theHighlight.getX()-theHotSpotHalf,
                    theHighlight.getY()+theHighlight.getHeight()-
                        theHotSpotHalf);

            theDefaultCursor = AbstractCursorManager.SW_RESIZE;
            break;

        case WEST:
            theShape =
                createHotSpotShape(theHighlight.getX()-theHotSpotHalf,
                    theHighlight.getY()+(theHighlight.getHeight()/2)-
                        theHotSpotHalf);

            theDefaultCursor = AbstractCursorManager.W_RESIZE;
            break;

        case NORTH_WEST:
            theShape = createHotSpotShape(theHighlight.getX()-theHotSpotHalf,
                theHighlight.getY()-theHotSpotHalf);

            theDefaultCursor = AbstractCursorManager.NW_RESIZE;
            break;

        case CENTRE:
            theShape =
                createHotSpotShape(theHighlight.getCenterX()-theHotSpotHalf,
                    theHighlight.getCenterY()-theHotSpotHalf);

            theDefaultCursor = AbstractCursorManager.DEFAULT;
            break;
        }
    }

    /**
     * Return the shape of the hotspot given the x and the y
     * 
     * @param x location
     * @param y location
     * @return the shape to display as the hotspot
     * @since 2.4
     */
    public abstract Shape createHotSpotShape(double x,double y);

    /**
     * Check to see if either the move point rotate point has been hit
     * 
     * @param point mouse point
     * @return true if the point is
     * @since 2.4
     */
    public boolean isHit(Point2D.Double point) {
        return theHotRegion.contains(point);
    }

    /**
     * The location of the hot spot
     * 
     * @return the location
     * @since 2.4
     */
    public int getLocation() {
        return theLocation;
    }

    /**
     * Return the correct cursor type
     * 
     * @return the cursor type
     * @since 2.4
     */
    public int getCursorType() {
        return theDefaultCursor;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "HotSpot location ["+theLocation+"] bounds ["+getBounds()+"]";
    }
}
