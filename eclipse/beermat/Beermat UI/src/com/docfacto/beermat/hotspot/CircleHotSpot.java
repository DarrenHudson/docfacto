package com.docfacto.beermat.hotspot;

import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

/**
 * Circle Hot Spot
 * 
 * @author dhudson - created 18 Oct 2013
 * @since 2.4
 */
public class CircleHotSpot extends HotSpot {

    private static final int FEATHER = 10;

    /**
     * Constructor.
     * 
     * @param location N/E/S/W location
     * @param highlight element to highlight
     * @param size of hot spot
     * @since 2.4
     */
    public CircleHotSpot(int location,Rectangle2D highlight,int size) {
        super(location,highlight,size,FEATHER);
    }

    /**
     * Constructor.
     * 
     * @param location N/S/E?W location
     * @param x location
     * @param y location
     * @param size of the hot spot
     * @since 2.5
     */
    public CircleHotSpot(int location,double x,double y,int size) {
        super(location,x,y,size,FEATHER);
    }

    /**
     * @see com.docfacto.beermat.hotspot.HotSpot#createHotSpotShape(double,
     * double)
     */
    @Override
    public Shape createHotSpotShape(double x,double y) {
        return new Ellipse2D.Double(x,y,getSize(),getSize());
    }

}
