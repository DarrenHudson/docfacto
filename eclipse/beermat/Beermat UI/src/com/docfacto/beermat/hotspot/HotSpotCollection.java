package com.docfacto.beermat.hotspot;

import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;

import org.apache.batik.gvt.GraphicsNode;
import org.w3c.dom.Element;

import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.common.DocfactoException;

/**
 * Abstract class that represents a collection of Hot Spots
 * 
 * @author dhudson - created 24 Oct 2013
 * @since 2.4
 */
public abstract class HotSpotCollection {

    private final BeermatAWTCanvas theCanvas;
    private final GraphicsNode theGraphicsNode;
    private final Element theElement;

    /**
     * An array of hot spots
     */
    public HotSpot[] theHotSpots;
    private Rectangle theTransformedBounds;

    /**
     * Constructor.
     * 
     * @param canvas beermat canvas
     * @param node graphics node
     * @param element which has hot spots
     * @throws DocfactoException if node or element are null
     */
    public HotSpotCollection(BeermatAWTCanvas canvas,GraphicsNode node,
    Element element) throws DocfactoException {
        theCanvas = canvas;
        theGraphicsNode = node;
        theElement = element;

        if (element==null||node==null) {
            throw new DocfactoException("Invalid element / node, is null");
        }

        theHotSpots = new HotSpot[getNumberOfHotSpots()];

        calculateTransformedBounds();

        createHotSpots();
    }

    /**
     * Constructor.
     * 
     * @param canvas beermat canvas
     * @param node graphics node for the hot spots
     * @throws DocfactoException if the graphics node is null
     * 
     * @since 2.4
     */
    public HotSpotCollection(BeermatAWTCanvas canvas,GraphicsNode node)
    throws DocfactoException {
        this(canvas,node,canvas.getElementFor(node));
    }

    /**
     * Constructor.
     * 
     * @param canvas
     * @param element
     */
    public HotSpotCollection(BeermatAWTCanvas canvas,Element element)
    throws DocfactoException {
        this(canvas,canvas.getGraphicsNodeFor(element),element);
    }

    /**
     * Calculate the position on the screen
     * 
     * @since 2.4
     */
    public void calculateTransformedBounds() {
        theTransformedBounds = calculateTransformedShape().getBounds();
    }

    /**
     * Calculate the transformed shape
     * 
     * @return the transformed shape
     * @since 2.5
     */
    public Shape calculateTransformedShape() {
        AffineTransform elementsAt =
            theGraphicsNode.getGlobalTransform();
        Shape selectionHighlight = theGraphicsNode.getOutline();
        AffineTransform at = theCanvas.getViewBoxTransform(); // theCanvas.getRenderingTransform();
        at.concatenate(elementsAt);

        return at.createTransformedShape(selectionHighlight);
    }

    public Point2D getTransformedPoint(double x, double y) throws NoninvertibleTransformException {
        AffineTransform elementsAt =
        theGraphicsNode.getGlobalTransform();
    AffineTransform at = theCanvas.getViewBoxTransform();
    at.concatenate(elementsAt);
    return at.transform(new Point2D.Double(x,y),
        null);
    }
    
    /**
     * Check to see if the Point (normally mouse position) is in a hot spot.
     * 
     * This could be a positional hot spot or a rotate hot spot
     * 
     * @param x mouse x
     * @param y mouse y
     * @return true if the point is in a hot spot
     * @since 2.4
     */
    public HotSpot isInHotSpot(int x,int y) {
        Point2D.Double location = new Point2D.Double(x,y);
        for (int i = 0;i<getNumberOfHotSpots();i++) {
            if (theHotSpots[i].isHit(location)) {
                return theHotSpots[i];
            }
        }

        return null;
    }

    /**
     * Returns the bounds of the graphical element, which has been transformed
     * so its in the correct location.
     * 
     * @return the bounds of the transformed graphical element
     * @since 2.4
     */
    public Rectangle getTransformedBounds() {
        return theTransformedBounds;
    }

    /**
     * The number of hot spots for this graphics node.
     * 
     * {@docfacto.note the hot spot array will be created of this size}
     * 
     * @return the number of hot spots for this graphics node.
     * @since 2.4
     */
    public abstract int getNumberOfHotSpots();

    /**
     * Called during construction. Although the hot spot array will be created
     * of the correct size, it will be empty.
     * 
     * @since 2.4
     */
    public abstract void createHotSpots();

    /**
     * Return the beermat canvas
     * 
     * @return the canvas
     * @since 2.4
     */
    public BeermatAWTCanvas getBeermatCanvas() {
        return theCanvas;
    }

    /**
     * Return the graphics node
     * 
     * @return the graphics node
     * @since 2.4
     */
    public GraphicsNode getGraphicsNode() {
        return theGraphicsNode;
    }

    /**
     * The SVG element for the graphics node
     * 
     * @return the element for the graphics node
     * @since 2.4
     */
    public Element getElement() {
        return theElement;
    }

    /**
     * Return the hot spot for that location or null it not found.
     * 
     * @param location to search for
     * @return the hot spot for that location
     * @since 2.4
     */
    public HotSpot getHotSpotForLocation(int location) {
        for (HotSpot hotSpot:theHotSpots) {
            if (hotSpot.getLocation()==location) {
                return hotSpot;
            }
        }

        return null;
    }

    /**
     * Return a hot spot for that location, or null if not found.
     * 
     * @param location to search for
     * @return hot spot for that location or null
     * @since 2.4
     */
    public HotSpot getHotSpotForLocation(String location) {
        try {
            return getHotSpotForLocation(Integer.parseInt(location));
        }
        catch (NumberFormatException ex) {

        }
        return null;
    }
}
