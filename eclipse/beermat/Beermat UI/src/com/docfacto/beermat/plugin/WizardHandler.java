package com.docfacto.beermat.plugin;

import org.eclipse.ui.INewWizard;

import com.docfacto.beermat.plugin.wizards.NewSvgWizard;
import com.docfacto.core.swt.AbstractWizardHandler;

/**
 * Handle the opening of the Beermat Wizard
 * 
 * @author dhudson - created 15 Oct 2013
 * @since 2.4
 */
public class WizardHandler extends AbstractWizardHandler {

    /**
     * @see com.docfacto.core.swt.AbstractWizardHandler#getWizard()
     */
    @Override
    public INewWizard getWizard() {
        return new NewSvgWizard();
    }

}
