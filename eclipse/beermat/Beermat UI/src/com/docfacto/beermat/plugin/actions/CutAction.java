package com.docfacto.beermat.plugin.actions;

import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.texteditor.ITextEditorActionConstants;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.ClipboardManager;
import com.docfacto.beermat.plugin.BeermatEditor;
import com.docfacto.beermat.plugin.BeermatMultiPage;
import com.docfacto.beermat.updaters.DeleteHighlightedUpdater;

/**
 * A cut action
 * 
 * @author kporter - created 22 Jan 2014
 * @since 2.5.1
 */
public class CutAction extends Action {

    private final BeermatMultiPage theEditor;

    /**
     * Constructs a new cut action.
     * 
     * @param editor The beermat editor
     */
    public CutAction(BeermatMultiPage editor) {
        theEditor = editor;
    }

    /**
     * @see org.eclipse.jface.action.Action#run()
     */
    @Override
    public void run() {
        if (theEditor.getActivePage()==1) {
            // Its the XML Editor, so lets call the Text Editor Print Action
            theEditor.getXMLEditor().getAction(ITextEditorActionConstants.CUT).run();
            return;
        }
        
        BeermatEditor editor = (BeermatEditor)theEditor.getBeertmatEditor();
        ClipboardManager clipboard = editor.getController().getClipboard();
        List<Element> elements =
            editor.getController().getBeermatAWTCanvas().getHighlightManager().getHighlightedElements();
        if(!elements.isEmpty()){
            clipboard.cut(elements);
            new DeleteHighlightedUpdater(editor.getController(),false);            
        }

    }
}
