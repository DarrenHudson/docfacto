package com.docfacto.beermat.plugin;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.GregorianCalendar;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IPathEditorInput;
import org.eclipse.ui.IURIEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.MultiPageEditorPart;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.osgi.framework.Version;
import org.w3c.dom.DOMException;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.dialogs.JPEGOptionsDialog;
import com.docfacto.beermat.dialogs.PNGOptionsDialog;
import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.events.UICompleteEvent;
import com.docfacto.beermat.listeners.BeermatEventListener;
import com.docfacto.beermat.svg.SVGManager;
import com.docfacto.beermat.svg.SVGSize;
import com.docfacto.beermat.ui.xmleditor.XMLEditor;
import com.docfacto.beermat.utils.SVGRasterizer;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.common.Java;
import com.docfacto.common.Platform;
import com.docfacto.common.XMLUtils;
import com.docfacto.config.XmlConfig;
import com.docfacto.config.generated.Product;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.core.utils.PluginUtils;
import com.docfacto.core.utils.SWTUtils;
import com.docfacto.licensor.Products;

/**
 * Handles the multi page format.
 * 
 * Currently two pages of a beermatAWTcanvas and a text editor
 * 
 * @author kporter - created Jun 20, 2013
 * @since 2.4
 */
public class BeermatMultiPage extends MultiPageEditorPart implements
IResourceChangeListener,BeermatEventListener {

    public final static String ID =
        "com.docfacto.beermat.plugin.BeermatMultiPage";
    private final BeermatController theController;

    private IBeermatEditor theBeermatEditor;
    private int theBeermatEditorIndex;

    private XMLEditor theXMLEditor;
    private int theXMLEditorIndex;

    /**
     * Flag to denote that the document requires saving
     */
    private boolean isDirty = false;

    private File theFile;
    private IFile theInternalFile;

    private final static int ERROR_DEFAULT = 101;
    private final static int ERROR_PARSING = 102;
    private final static int ERROR_IO = 103;

    private enum ValidLicence {
        VALID, INVALID
    }

    private ValidLicence theLicenceValid;

    /**
     * Constructor.
     * 
     * @since 2.4
     */
    public BeermatMultiPage() {
        // Create the controller
        theController = new BeermatController();
        theController.setMultiPageEditor(this);
        ResourcesPlugin.getWorkspace().addResourceChangeListener(this);
    }

    /**
     * @see org.eclipse.ui.part.MultiPageEditorPart#init(org.eclipse.ui.IEditorSite,
     * org.eclipse.ui.IEditorInput)
     * @docfacto.link key=" runningBeermatProblems"
     * uri="${doc-beermat}/general/c_running.dita" link-to="doc"
     */
    @Override
    public void init(IEditorSite site,IEditorInput input)
    throws PartInitException {
        if (input instanceof IFileEditorInput) {
            File file =
                ((IFileEditorInput)input).getFile().getRawLocation()
                    .makeAbsolute().toFile();

            if (PluginUtils.isFileAnImage(file)) {
                try {
                    // Do image stuff
                    theInternalFile =
                        createImageSVG(((IFileEditorInput)input).getFile());
                    input = new FileEditorInput(theInternalFile);
                }
                catch (CoreException e) {
                    e.printStackTrace();
                    throw new PartInitException(
                        "Could not open the SVG associated with this image");
                }
            }
            else {
                theInternalFile = ((IFileEditorInput)input).getFile();
            }

            // Convert to file
            theFile = theInternalFile.getRawLocation().makeAbsolute().toFile();
        }
        else if (input instanceof IPathEditorInput) {
            // FInd the file in the work space
            IWorkspace ws = ResourcesPlugin.getWorkspace();
            IWorkspaceRoot wsr = ((IResource)ws).getWorkspace().getRoot();
            IPath location = ((IPathEditorInput)input).getPath();
            theInternalFile = wsr.getFileForLocation(location);
            theFile =
                wsr.getFileForLocation(location).getRawLocation()
                    .makeAbsolute().toFile();
        }
        else if (input instanceof IURIEditorInput) {
            URI uri = ((IURIEditorInput)input).getURI();
            theFile = new File(uri);
        }

        setSite(site);
        setInput(input);

        checkLicence();
    }

    private void checkLicence() {
        theLicenceValid = ValidLicence.INVALID;

        XmlConfig config = DocfactoCorePlugin.getXmlConfig();
        if (config!=null) {
            Product beermat = config.getProduct(Products.BEERMAT);
            if (beermat!=null) {
                if (GregorianCalendar.getInstance().before(
                    beermat.getExpiryDate().toGregorianCalendar())) {
                    theLicenceValid = ValidLicence.VALID;
                }
            }
        }
    }

    private void showLicenceErrorDialog() {
        // TODO: When Lee-Ann makes a licence webpage, link to that page.
        MessageDialog
            .openWarning(
                this.getEditorSite().getShell(),
                "Valid Licence required to save",
                "To save works created with Beermat a valid docfacto Beermat licence is required");
    }

    private void showLicenceErrorStatusLine() {
        PluginUtils
            .displayStatusLineMessage(this,
                "Beermat save functionality will not work until a valid licence is activated");
    }

    /**
     * @docfacto.link key="openingRasters"
     * uri="${doc-beermat}/general/c_opening_beermat.dita" link-to="doc"
     */
    private IFile createImageSVG(IFile image) throws CoreException {
        String originalName = image.getName();
        String name =
            image.getName().replace('.'+image.getFileExtension(),".svg");

        IPath folderpath = image.getParent().getProjectRelativePath();
        IFile svg;
        if (folderpath.isEmpty()) {
            // THe file is located in the project itself.
            svg = image.getProject().getFile(name);
        }
        else {
            IFolder folder = image.getProject().getFolder(folderpath);
            svg = folder.getFile(name);
        }

        if (!svg.exists()) {
            ImageLoader r = new ImageLoader();
            ImageData d =
                r.load(image.getRawLocation().makeAbsolute().toString())[0];
            String svgString =
                SVGUtils.createSVGWithImage(originalName,d.width,d.height);
            svg.create(new ByteArrayInputStream(svgString.getBytes()),false,
                null);
        }

        return svg;
    }

    /**
     * @see org.eclipse.ui.part.MultiPageEditorPart#createPages()
     */
    @Override
    protected void createPages() {
        // Create everything required
        boolean error = false;
        try {
            setupUI();
            // At this point, we have passed all of the construction logic,
            // lets fire a UICompelete Event
            theController.processEvent(new UICompleteEvent(this));
        }
        catch (DOMException e) {
            setupErrorPage(
                "The SVG Could not be parsed",
                "The SVG was invalid and could not be parsed "+"\n\n"+
                    e.getLocalizedMessage());
            error = true;
        }
        catch (IOException e) {
            setupErrorPage(ERROR_IO);
            error = true;
        }

        try {
            theBeermatEditorIndex = addPage(theBeermatEditor,getEditorInput());

            if (error) {
                setPageText(theBeermatEditorIndex,"Beermat Editor");
            }
            else {
                theXMLEditorIndex = addPage(theXMLEditor,getEditorInput());
                setPageText(theBeermatEditorIndex,"SVG Editor");
                setPageText(theXMLEditorIndex,"XML Editor");

                if (theLicenceValid==ValidLicence.INVALID) {
                    // Disable editing in the xml (copy still works)
                    theXMLEditor.disableCopy();
                    // show the status line
                    // getEditorSite().getActionBars().getStatusLineManager().setMessage("poop");
                    showLicenceErrorStatusLine();
                }
            }
        }
        catch (PartInitException ex) {
            BeermatUIPlugin
                .logException("Unable to create multipart editor",ex);
        }
    }

    private void setupErrorPage(String title,String message) {
        theBeermatEditor = new BeermatFileErrorPage(this,title,message);
        theXMLEditor = new XMLEditor(new BeermatController());
    }

    /**
     * Create the editors if and only if theFile is null or a valid svg.
     * Otherwise will throw exceptions for an error page to be created instead.
     * 
     * @since 2.4
     */
    private void setupUI() throws IOException {
        SVGManager manager = null;
        if (theFile!=null) {
            SVGDocument doc = SVGUtils.getSVGDocument(theFile.toURI());
            manager = new SVGManager(theController,doc);
            setPartName(theFile.getName());
        }
        else {
            manager = new SVGManager(theController);
            setPartName("New SVG");
        }

        // Note the order is important here
        theController.setSVGManager(manager);
        theController.addListener(this);

        if (isVersionCorrect()) {
            theBeermatEditor = new BeermatEditor(theController);
        }
        else {
            theBeermatEditor = new BeermatErrorPage(this);
        }
        theXMLEditor = new XMLEditor(theController);

    }

    private void setupErrorPage(final int error) {
        switch (error) {
        case ERROR_DEFAULT:
            setupErrorPage("An error occured while opening Beermat",
                "Beermat encountered an error while opening");
            break;
        case ERROR_PARSING:
            setupErrorPage("Beermat parsing error",
                "The SVG was invalid and could not be parsed");
            break;
        case ERROR_IO:
            setupErrorPage(
                "Beermat cannot open this type of file",
                "The file could not be opened with Beermat. "
                    +"Beermat currently supports opening of SVG files and raster image files,'PNG','JPEG'"
                    +"\n\n"
                    +"Press New SVG to create a blank SVG file or close this editor");
            break;
        }
    }

    /**
     * @see org.eclipse.ui.part.MultiPageEditorPart#pageChange(int)
     */
    @Override
    protected void pageChange(int newPageIndex) {

        // super.pageChange(newPageIndex);
        if (newPageIndex==theXMLEditorIndex) {
            try {
                if (theBeermatEditor.hasChanged()) {
                    // Need to get the Document out of the SVG and give it to
                    // the editor
                    SVGDocument doc = theController.getManager().getDocument();
                    String formatted = XMLUtils.prettyFormat(doc);
                    IDocumentProvider dp = theXMLEditor.getDocumentProvider();
                    IDocument textdoc =
                        dp.getDocument(theXMLEditor.getEditorInput());

                    textdoc.set(formatted);
                }
            }
            catch (DocfactoException ex) {

            }
            // Listen for any new changes
            theXMLEditor.resetHasChanged();
        }
        else {
            if (theXMLEditor.hasChanged()) {
                SVGManager svgManager = theController.getManager();
                SVGDocument oldDocument = svgManager.getDocument();

                String contents = theXMLEditor.getEditorContents();

                try {
                    SVGDocument newDocument =
                        SVGUtils.getSVGDocument(oldDocument.getBaseURI(),
                            contents);
                    svgManager.setSVGDocument(newDocument);
                }
                catch (IOException ex) {
                    MessageDialog
                        .openError(getEditorSite().getShell(),"Invalid XML",
                            "Invalid XML : "+ex.getLocalizedMessage());
                    this.setActivePage(theXMLEditorIndex);
                    BeermatUIPlugin.logException(
                        "Unable to set the SVG document",ex);
                }
            }

            theBeermatEditor.setFocus();
            theBeermatEditor.resetHasChanged();
        }
    }

    private static boolean isVersionCorrect() {
        if (Platform.isMac()) {
            Java java = new Java();
            Version version = PluginUtils.getEclipseVersion();

            // Eclipse is not kepler, and java 7+ is being used!
            if (version.getMajor()==4&&version.getMinor()<3) {
                if (java.getMinorVersion()>=7) {
                    return false;
                }
            }

            // Kepler is being used... But java version is lower than u40
            if (java.getMinorVersion()==7&&java.getBuildVersion()<40) {
                // Incorrect version of Java so grumble
                return false;
            }
        }
        return true;
    }

    /**
     * @see org.eclipse.ui.part.EditorPart#isDirty()
     */
    public boolean isDirty() {
        return isDirty;
    }

    /**
     * Set a flag to see if the SVG is dirty.
     * 
     * @param dirty to set
     * @since 2.4
     */
    public void setDirty(boolean dirty) {
        // Only need to do anything if we're actually changing from not dirty to
        // dirty...
        // This prevents dirty from being fired over and over needlessly.
        if (isDirty!=dirty) {
            if (dirty&&theInternalFile!=null) {
                PluginUtils.checkoutFile(theInternalFile,this.getSite()
                    .getShell());
            }
            isDirty = dirty;
            firePropertyChange(IEditorPart.PROP_DIRTY);
        }
    }

    /**
     * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public void doSave(IProgressMonitor progressMonitor) {
        if (theLicenceValid==ValidLicence.VALID) {
            if (theInternalFile!=null) {
                PluginUtils.checkoutFile(theInternalFile,this.getSite()
                    .getShell());
            }

            if (theFile==null) {
                // New document, so no file name..
                doSaveAs();
            }
            else {
                saveContents(theFile,false);
            }

        }
        else {
            showLicenceErrorDialog();
        }
    }

    /**
     * @see org.eclipse.ui.part.EditorPart#doSaveAs()
     * @docfacto.link key="saving" uri="${doc-beermat}/workspace/c_saving.dita"
     * link-to="doc"
     */
    @Override
    public void doSaveAs() {
        if (theLicenceValid==ValidLicence.VALID) {
            FileDialog dialog = new FileDialog(getSite().getShell(),SWT.SAVE);

            String[] filterNames =
                new String[] {"SVG (Scalable Vector Graphics)","PNG Image",
                    "JPEG Image"};
            String[] filterExtensions = new String[] {"*.svg","*.png",".jpg"};

            dialog.setFilterNames(filterNames);
            dialog.setFilterExtensions(filterExtensions);
            // dialog.setOverwrite(true);

            if (theFile!=null) {
                dialog.setFilterPath(theFile.getParent());
            }

            dialog.open();

            if (!dialog.getFileName().isEmpty()) {
                File file =
                    new File(dialog.getFilterPath(),dialog.getFileName());
                int i = file.getName().lastIndexOf(".");
                String ext = file.getName().substring(i);
                if (ext.equalsIgnoreCase(".svg")) {
                    saveContents(file,true);
                }
                else if (ext.equalsIgnoreCase(".png")) {
                    // Here create a PNG dialog to ask for PNG options
                    PNGOptionsDialog pngOptions =
                        new PNGOptionsDialog(getSite().getShell());
                    SVGSize s = theController.getManager().getDocumentSize();
                    int w = s.getWidth();
                    int h = s.getHeight();
                    pngOptions.setOriginalSize(w,h);

                    if (pngOptions.open()==Window.OK) {
                        final boolean t = pngOptions.getTransparency();
                        Point size = null;
                        if (pngOptions.getSizeModified()) {
                            size = pngOptions.getNewSize();
                        }
                        try {
                            if (t) {
                                SVGRasterizer.exportToPNG(theController
                                    .getManager().getDocument(),file,t,null,
                                    size);
                            }
                            else {
                                RGB rgb = pngOptions.getMatte();
                                java.awt.Color c =
                                    new Color(rgb.red,rgb.green,rgb.blue);
                                SVGRasterizer.exportToPNG(theController
                                    .getManager().getDocument(),file,t,c,size);
                            }
                        }
                        catch (Exception e) {
                            BeermatUIPlugin.logException(
                                "Could not save to PNG",e);
                        }
                    }

                }
                else if (ext.equalsIgnoreCase(".jpg")||
                    ext.equalsIgnoreCase(".jpeg")) {
                    JPEGOptionsDialog jpgDialog =
                        new JPEGOptionsDialog(getSite().getShell());
                    SVGSize size = theController.getManager().getDocumentSize();
                    int width = size.getWidth();
                    int height = size.getHeight();
                    jpgDialog.setOriginalSize(width,height);

                    if (jpgDialog.open()==Window.OK) {
                        Point jpgSize = null;
                        int quality = jpgDialog.getQuality();

                        if (jpgDialog.getSizeModified()) {
                            jpgSize = jpgDialog.getNewSize();
                        }

                        try {
                            SVGRasterizer.exportToJPG(theController
                                .getManager().getDocument(),file,jpgSize,
                                quality);
                        }
                        catch (Exception e) {
                            BeermatUIPlugin.logException(
                                "Could not save to JPG",e);
                        }
                    }
                }
                IFile ifile =
                    PluginUtils.findIFileInWorkspace(file.getAbsolutePath());
                if (ifile!=null) {
                    refresh(ifile,IResource.DEPTH_ONE);
                }

                refreshInternal();
            }

        }
        else {
            showLicenceErrorDialog();
        }
    }

    /**
     * Save the contents of the xml.
     * 
     * @param file to save the file to
     * @param saveAs true if this is a save as function
     * @throws DocfactoException if unable to format the XML or save the file
     * @since 2.4
     */
    private void saveContents(File file,boolean saveAs) {
        try {
            String contents = null;

            if (getActivePage()==0) {
                contents =
                    XMLUtils.prettyFormat(theController.getManager()
                        .getDocument());
            }
            else {
                contents = theXMLEditor.getEditorContents();
            }

            IOUtils.writeFile(file,contents);
            if (saveAs) {
                IWorkbenchPage page =
                    PlatformUI.getWorkbench().getActiveWorkbenchWindow()
                        .getActivePage();
                IDE.openEditor(page,file.toURI(),ID,true);
            }
            else {
                // Lets refresh the resource to stop the refresh error coming up
                refreshInternal();
                setDirty(false);
            }
        }
        catch (IOException ex) {
            BeermatUIPlugin.logException(
                "Save failed, couldn't write to file.",ex);
        }
        catch (DocfactoException ex) {
            BeermatUIPlugin.logException(
                "Unable to format Document pre saving",ex);
        }
        catch (PartInitException e) {
            BeermatUIPlugin.logException(
                "Unable to initialise a new editor part",e);
        }
    }

    /**
     * Refresh the resource as its been modified outside of Eclipse
     * 
     * @param depth IResource.DEPTH_ZERO, IResource.DEPTH_ONE,
     * IResource.DEPTH_INFINITE
     * @since 2.4
     */
    public void refresh(IFile file,int depth) {
        if (file!=null) {
            try {
                file.getProject().refreshLocal(IResource.DEPTH_ONE,null);
                file.refreshLocal(depth,null);
            }
            catch (CoreException ex) {
                BeermatUIPlugin.logException("Unable to refresh resource",ex);
            }
        }
    }

    /**
     * Refresh the resource as its been modified outside of Eclipse
     * 
     * @since 2.4.7
     */
    public void refreshInternal() {
        refresh(theInternalFile,IResource.DEPTH_ZERO);
    }

    public void refreshInternal(int depth) {
        refresh(theInternalFile,depth);
    }

    /**
     * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
     */
    @Override
    public boolean isSaveAsAllowed() {
        return true;
    }

    /**
     * @see org.eclipse.ui.part.MultiPageEditorPart#dispose()
     */
    public void dispose() {
        ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);

        // Shutdown the controller
        theController.shutdown();

        super.dispose();
    }

    /**
     * Returns the XML Editor part
     * 
     * @return the XML Editor
     * @since 2.4
     */
    public XMLEditor getXMLEditor() {
        return theXMLEditor;
    }

    /**
     * Returns the current beermat editor, this could be an error page.
     * 
     * @return the current beermat editor
     * @since 2.5
     */
    public IBeermatEditor getBeertmatEditor() {
        return theBeermatEditor;
    }

    /**
     * @see org.eclipse.core.resources.IResourceChangeListener#resourceChanged(org.eclipse.core.resources.IResourceChangeEvent)
     */
    @Override
    public void resourceChanged(final IResourceChangeEvent event) {
        if (event.getType()==IResourceChangeEvent.PRE_CLOSE) {
            SWTUtils.UIThreadExec(new Runnable() {
                public void run() {
                    IWorkbenchPage[] pages =
                        getSite().getWorkbenchWindow().getPages();
                    for (int i = 0;i<pages.length;i++) {
                        if (((FileEditorInput)theXMLEditor.getEditorInput())
                            .getFile().getProject().equals(event.getResource())) {
                            IEditorPart editorPart =
                                pages[i].findEditor(theXMLEditor
                                    .getEditorInput());
                            pages[i].closeEditor(editorPart,true);
                        }
                    }
                }
            });
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.BeermatEventListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    @Override
    public void handleEvent(BeermatEvent event) {
        if (event.isSVGDirtyEvent()) {
            SWTUtils.UIThreadExec(new Runnable() {
                /**
                 * @see java.lang.Runnable#run()
                 */
                public void run() {
                    setDirty(true);
                }
            });
        }

        if (event.isSaveEvent()) {
            SWTUtils.UIThreadExec(new Runnable() {
                /**
                 * @see java.lang.Runnable#run()
                 */
                public void run() {
                    if (isDirty) {
                        doSave(new NullProgressMonitor());
                    }
                }
            });
        }
    }

    /**
     * Get the file
     * 
     * @return the file which is opened
     * @since 2.4
     */
    public File getFile() {
        return theFile;
    }

}