package com.docfacto.beermat.plugin.wizards;

import org.eclipse.core.resources.IFile;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.IDE;

import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.common.DocfactoException;
import com.docfacto.core.utils.PluginUtils;
import com.docfacto.xsd2common.XSDArgumentParser;
import com.docfacto.xsd2svg.XSD2SVG;

/**
 * A wizard specifically for creating new SVG files from XSD files
 * @author kporter - created 10 Jan 2014
 * @since 2.5.1
 */
public class XsdNewWizard extends AbstractXsdWizard implements INewWizard {

    public XsdNewWizard() {
        super(WizardType.NEW);
    }

    @Override
    public boolean performFinish() {
        XSDArgumentParser parser = new XSDArgumentParser(theData.constructArguments());

        try {
            parser.validate();
            new XSD2SVG(parser);

            IFile file = PluginUtils.findIFileInWorkspace(parser.getOutputFile());
            if (file!=null) {
                try {
                    IDE.openEditor(theData.workbench.getActiveWorkbenchWindow().getActivePage(),file);
                }
                catch (PartInitException e) {
                    BeermatUIPlugin.logMessage("XSD Created, but could not open in beermat");
                }
            }

            return true;
        }
        catch (final DocfactoException ex) {
            System.out.println("Arguments not valid: cause ["+ex.getLocalizedMessage()+"]");
            return false;
        }
    }

}
