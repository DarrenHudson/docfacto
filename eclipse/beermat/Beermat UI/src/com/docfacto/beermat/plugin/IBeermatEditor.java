package com.docfacto.beermat.plugin;

import org.eclipse.ui.IEditorPart;

/**
 * All Beermat Editors implement this.
 * 
 * @author kporter - created Oct 25, 2013
 * @since 2.4
 */
public interface IBeermatEditor extends IEditorPart {
    
    /**
     * Check to see if the document has been changed
     * 
     * @return true if the document has changed since the last rest
     * @since 2.4
     */
    public boolean hasChanged();
    
    /**
     * Reset the has changed flag
     * 
     * @since 2.4
     */
    public void resetHasChanged();
}
