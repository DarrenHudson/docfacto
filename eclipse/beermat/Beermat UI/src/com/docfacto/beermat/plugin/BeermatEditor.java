package com.docfacto.beermat.plugin;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.listeners.BeermatEventListener;
import com.docfacto.beermat.ui.BeermatView;
import com.docfacto.core.utils.SWTUtils;

/**
 * The EditorPart for the Beermat Plugin
 * 
 * {@docfacto.note Although this extends EditorPart, all of implementation is
 * handled within the multipage editor }
 * 
 * {@docfacto.media uri="doc-files/BeermatMVC.svg" }
 * 
 * @author kporter - created March, 2013
 * @since 2.2
 */
public class BeermatEditor extends EditorPart implements BeermatEventListener,
IBeermatEditor {

    /**
     * The ID of this editor.
     */
    public final static String ID = "com.docfacto.beermat.editor";

    private final BeermatController theController;
    private BeermatView theBeermatView;

    /**
     * Flag to denote that the document requires saving
     */
    private boolean isDirty = false;

    /**
     * Flag to see if the SVG Document has changed since last rest. Used for the
     * tab switching in the multi part editor
     */
    private boolean hasChanged = false;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @since 2.4
     */
    public BeermatEditor(BeermatController controller) {
        theController = controller;
    }

    /**
     * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createPartControl(Composite parent) {
        theController.addListener(this);

        theBeermatView = new BeermatView(theController,parent);
        theController.setView(theBeermatView);
        theBeermatView.getSVGComposite().getBeermatAWTCanvas()
            .setDocument(theController.getManager().getDocument());
    }

    /**
     * Returns the beermat controller
     * 
     * @return the beermat controller
     * @since 2.5
     */
    public BeermatController getController() {
        return theController;
    }

    /**
     * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
     */
    @Override
    public void setFocus() {
        // theController.getBeermatAWTCanvas().requestFocus();
    }

    /**
     * @see org.eclipse.ui.part.EditorPart#isDirty()
     */
    public boolean isDirty() {
        return isDirty;
    }

    /**
     * Set a flag to see if the SVG is dirty.
     * 
     * @param dirty to set
     * @since 2.4
     */
    public void setDirty(boolean dirty) {
        isDirty = dirty;
        firePropertyChange(IEditorPart.PROP_DIRTY);
    }

    /**
     * @see com.docfacto.beermat.listeners.BeermatEventListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    @Override
    public void handleEvent(BeermatEvent event) {
        if (event.isSVGDirtyEvent()) {
            hasChanged = true;
            SWTUtils.UIThreadExec(new Runnable() {
                /**
                 * @see java.lang.Runnable#run()
                 */
                public void run() {
                    setDirty(true);
                }
            });
        }
    }

    /**
     * @see com.docfacto.beermat.plugin.IBeermatEditor#hasChanged()
     */
    public boolean hasChanged() {
        return hasChanged;
    }

    /**
     * Reset the has changed flag
     * 
     * @since 2.4
     */
    public void resetHasChanged() {
        hasChanged = false;
    }

    /**
     * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public void doSave(IProgressMonitor arg0) {
    }

    /**
     * @see org.eclipse.ui.part.EditorPart#doSaveAs()
     */
    @Override
    public void doSaveAs() {
    }

    /**
     * @see org.eclipse.ui.part.EditorPart#init(org.eclipse.ui.IEditorSite,
     * org.eclipse.ui.IEditorInput)
     */
    @Override
    public void init(IEditorSite site,IEditorInput editorInput)
    throws PartInitException {
    }

    /**
     * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
     */
    @Override
    public boolean isSaveAsAllowed() {
        return true;
    }

    /**
     * @see org.eclipse.ui.part.WorkbenchPart#dispose()
     */
    @Override
    public void dispose() {
        theBeermatView.dispose();
        super.dispose();
    }

}
