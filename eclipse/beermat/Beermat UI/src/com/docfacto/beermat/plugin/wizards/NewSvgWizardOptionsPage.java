package com.docfacto.beermat.plugin.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.docfacto.core.widgets.DocfactoUIConstants;
import com.docfacto.core.widgets.listeners.NumericVerifyListener;

/**
 * A Wizard page for new svg options.
 * 
 * @author kporter - created 28 Jan 2014
 * @since 2.5.1
 */
public class NewSvgWizardOptionsPage extends WizardPage {
    private Text theHeight;
    private Text theWidth;
    private Text theMinx;
    private Text theMiny;
    private Text theViewBoxHeight;
    private Text theViewBoxWidth;

    private NewWizardData theData;

    /**
     * Create the wizard.
     * 
     * @param data The {@link NewWizardData}
     * @since 2.5.1
     */
    public NewSvgWizardOptionsPage(NewWizardData data) {
        super("SVG Options");
        setTitle("SVG Options");
        setDescription("Set the initial SVG Options. Size is required, if left empty, an svg with a default size of "+
            "1000 x 600 will be created. All other options here are optional attributes");

        theData = data;
    }

    /**
     * Create contents of the wizard.
     * 
     * @param parent The parent composite
     * @since 2.5.1
     */
    public void createControl(Composite parent) {
        Composite container = new Composite(parent,SWT.NULL);

        setControl(container);
        GridLayout gl_container = new GridLayout(1,false);
        gl_container.verticalSpacing = 3;
        container.setLayout(gl_container);

        Label lblSize = new Label(container,SWT.NONE);
        GridData gd_lblSize = new GridData(SWT.LEFT,SWT.CENTER,false,false,1,1);
        gd_lblSize.horizontalIndent = 5;
        lblSize.setLayoutData(gd_lblSize);
        lblSize.setText("Size");

        Composite sizeArea = new Composite(container,SWT.BORDER);
        sizeArea.setBackground(DocfactoUIConstants.COLOR_WIDGET_BACKGROUND);
        sizeArea.setLayout(new GridLayout(3,false));

        Label lblHeight = new Label(sizeArea,SWT.NONE);
        lblHeight.setLayoutData(new GridData(SWT.RIGHT,SWT.CENTER,false,false,1,1));
        lblHeight.setText("Height");

        theHeight = new Text(sizeArea,SWT.BORDER);
        GridData gd_theHeightText = new GridData(SWT.FILL,SWT.CENTER,true,false,1,1);
        gd_theHeightText.widthHint = 59;
        theHeight.setLayoutData(gd_theHeightText);
        theHeight.addVerifyListener(new NumericVerifyListener());

        Label lblPx = new Label(sizeArea,SWT.NONE);
        GridData gd_lblPx = new GridData(SWT.LEFT,SWT.CENTER,false,false,1,1);
        gd_lblPx.horizontalIndent = -4;
        lblPx.setLayoutData(gd_lblPx);
        lblPx.setText("px");

        Label lblWidth = new Label(sizeArea,SWT.NONE);
        lblWidth.setLayoutData(new GridData(SWT.RIGHT,SWT.CENTER,false,false,1,1));
        lblWidth.setText("Width");

        theWidth = new Text(sizeArea,SWT.BORDER);
        theWidth.setLayoutData(new GridData(SWT.FILL,SWT.CENTER,true,false,1,1));
        theWidth.addVerifyListener(new NumericVerifyListener());

        Label lblpx2 = new Label(sizeArea,SWT.NONE);
        GridData gd_lblpx2 = new GridData(SWT.LEFT,SWT.CENTER,false,false,1,1);
        gd_lblpx2.horizontalIndent = -4;
        lblpx2.setLayoutData(gd_lblpx2);
        lblpx2.setText("px");

        Label lblViewbox = new Label(container,SWT.NONE);
        GridData gd_lblViewbox = new GridData(SWT.LEFT,SWT.CENTER,false,false,1,1);
        gd_lblViewbox.horizontalIndent = 5;
        lblViewbox.setLayoutData(gd_lblViewbox);
        lblViewbox.setText("Viewbox");

        Composite composite = new Composite(container,SWT.BORDER);
        composite.setLayout(new GridLayout(4,false));
        composite.setBackground(DocfactoUIConstants.COLOR_WIDGET_BACKGROUND);

        Label lblMnx = new Label(composite,SWT.NONE);
        lblMnx.setLayoutData(new GridData(SWT.RIGHT,SWT.CENTER,false,false,1,1));
        lblMnx.setText("min-x");

        theMinx = new Text(composite,SWT.BORDER);
        theMinx.setLayoutData(new GridData(SWT.FILL,SWT.CENTER,true,false,1,1));

        Label lblMiny = new Label(composite,SWT.NONE);
        lblMiny.setLayoutData(new GridData(SWT.RIGHT,SWT.CENTER,false,false,1,1));
        lblMiny.setText("min-y");

        theMiny = new Text(composite,SWT.BORDER);
        theMiny.setLayoutData(new GridData(SWT.FILL,SWT.CENTER,true,false,1,1));

        Label lblWidth_1 = new Label(composite,SWT.NONE);
        lblWidth_1.setLayoutData(new GridData(SWT.RIGHT,SWT.CENTER,false,false,1,1));
        lblWidth_1.setText("width");

        theViewBoxWidth = new Text(composite,SWT.BORDER);
        theViewBoxWidth.setLayoutData(new GridData(SWT.FILL,SWT.CENTER,true,false,1,1));

        Label lblHeight_1 = new Label(composite,SWT.NONE);
        lblHeight_1.setLayoutData(new GridData(SWT.RIGHT,SWT.CENTER,false,false,1,1));
        lblHeight_1.setText("height");

        theViewBoxHeight = new Text(composite,SWT.BORDER);
        theViewBoxHeight.setLayoutData(new GridData(SWT.FILL,SWT.CENTER,true,false,1,1));

        theViewBoxHeight.addFocusListener(new ChangeListener());
        theViewBoxWidth.addFocusListener(new ChangeListener());
        theMinx.addFocusListener(new ChangeListener());
        theMiny.addFocusListener(new ChangeListener());
        theHeight.addFocusListener(new ChangeListener());
        theWidth.addFocusListener(new ChangeListener());

        theViewBoxHeight.addModifyListener(new ChangeListener());
        theViewBoxWidth.addModifyListener(new ChangeListener());
        theMinx.addModifyListener(new ChangeListener());
        theMiny.addModifyListener(new ChangeListener());
        theHeight.addModifyListener(new ChangeListener());
        theWidth.addModifyListener(new ChangeListener());

    }

    /**
     * Get the size set here if both width and height were filled in
     * 
     * @return Get the size
     * @since 2.5.1
     */
    public Point getSize() {
        if (theHeight!=null&&theWidth!=null) {
            return new Point(Integer.parseInt(theWidth.getText()),Integer.parseInt(theHeight.getText()));
        }
        return null;

    }

    /**
     * Constructs a viewbox value from the users input
     * 
     * @return the viewbox as a string
     * @since 2.5.1
     */
    public String getViewbox() {
        if (theMinx.getText().isEmpty()||theMiny.getText().isEmpty()||theViewBoxHeight.getText().isEmpty()||
            theViewBoxWidth.getText().isEmpty()) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        builder.append(theMinx.getText());
        builder.append(" ");
        builder.append(theMiny.getText());
        builder.append(" ");
        builder.append(theViewBoxWidth.getText());
        builder.append(" ");
        builder.append(theViewBoxHeight.getText());

        return builder.toString();
    }

    private void updateData() {
        if (!theWidth.getText().isEmpty()) {
            theData.width = Integer.parseInt(theWidth.getText());
        }
        if (!theHeight.getText().isEmpty()) {
            theData.height = Integer.parseInt(theHeight.getText());
        }

        theData.viewbox = getViewbox();
    }

    private class ChangeListener implements FocusListener,ModifyListener {

        @Override
        public void focusGained(FocusEvent e) {
            updateData();
        }

        @Override
        public void focusLost(FocusEvent e) {
            updateData();
        }

        @Override
        public void modifyText(ModifyEvent e) {
            updateData();
        }

    }
}
