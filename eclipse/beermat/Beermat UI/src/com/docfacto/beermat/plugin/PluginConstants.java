package com.docfacto.beermat.plugin;

/**
 * Plugin wide constants.
 * @author kporter - created Jul 9, 2013
 * @since 2.4.2
 */
public final class PluginConstants {
    /**
     * Location of the folder with icons, has leading "/"
     */
    public final static String ICON_FOLDER = "icons/";
    
    public final static String ARROW_GRAY = "arrow.png";
    public final static String ARROW_ORANGE = "arrowOrange.png";
        
    public final static String BI_ARROW_GRAY = "biarrowGray.png"; 
    public final static String BI_ARROW_ORANGE = "biarrowOrange.png";
    
    public final static String CIRCLE_GRAY = "circle.png";
    public final static String CIRCLE_ORANGE ="circleOrange.png";
    
    public final static String CURSOR = "cursor.png";
    
    public final static String CLEAR = "trash.png";
    
    public final static String COPY_ATTRIBUTES = "eyedropper-a.png";
    
    public final static String CUSTOM_GRAY = "custom.png";
    public final static String CUSTOM_ORANGE = "customOrange.png";
    
    public final static String ELLIPSE_GRAY = "ellipse.png";
    public final static String ELLIPSE_ORANGE = "ellipseOrange.png";
    
    public final static String ERROR = "error.png";
    
    public final static String EYEDROPPER_A = "eyedropper-a.png";
    
    public final static String FRAME = "frame.png";
    
    public final static String GROUPER_GRAY = "grouper.png";
    public final static String GROUPER_ORANGE = "grouperOrange.png";
    
    public final static String LAYER_DOWN_GRAY = "layer-down.png";
    public final static String LAYER_DOWN_ORANGE = "bringToFront.png";
    
    public final static String LAYER_UP_GRAY = "layer-up.png";
    public final static String LAYER_UP_ORANGE ="bringToBack.png";
    
    public final static String LINE_GRAY = "line.png";
    public final static String LINE_ORANGE = "lineOrange.png";
    
    public final static String MORE_ORANGE = "moreOrange.png";
    public final static String MORE_BLUE = "moreBlue.png";
    public final static String MORE_BLUE_THIN = "moreBlueThin.png";
    public final static String MORE_BLUE_ROT = "moreBlueRot.png";
    public final static String MORE_WHITE = "moreWhite.png";
    
    public final static String PENCIL = "freehand.png";
    public final static String PENCIL_ORANGE = "pencilOrange.png";
    
    public final static String PROGRESS_REM = "progress_rem.gif";
    
    public final static String RECT_GRAY = "rect.png";
    public final static String RECT_ORANGE = "rectOrange.png";
    
    public final static String ROTATE = "rotate.gif";

    public final static String SHORTCUT_ARROW = "shortcut1Arrow.png";
    public final static String SHORTCUT_LINE = "shortcutLine.png";
    
    public final static String SQUARE_GRAY = "square.png";
    public final static String SQUARE_ORANGE = "squareOrange.png";
    
    
    public final static String TEXT_GRAY = "text.png";
    public final static String TEXT_ORANGE ="textOrange.png";
    
    public final static String ZOOM_IN = "zoominOrange.png";
    public final static String ZOOM_IN_BLUE = "zoominBlue.png";
    
    public final static String ZOOM_OUT = "zoomoutOrange.png";
    public final static String ZOOM_OUT_BLUE = "zoomoutBlue.png";
    
}
