package com.docfacto.beermat.plugin.wizards;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

import com.docfacto.common.DocfactoException;
import com.docfacto.core.widgets.AbstractBrowserWidget;
import com.docfacto.core.widgets.DirectoryBrowserWidget;
import com.docfacto.core.widgets.DocfactoUIConstants;
import com.docfacto.core.widgets.FileBrowserWidget;
import com.docfacto.core.widgets.listeners.FileTypeSelectionValidator;
import com.docfacto.xsdtree.XSDTreeTraverser;

/**
 * The XSD Wizard page.
 * 
 * @author kporter
 * @since 2.5.1
 */
public class XsdWizardPage extends WizardPage implements SelectionListener,
Listener {
    private WizardType theType;
    private XsdWizardData theData;
    private FileBrowserWidget theXsdBrowser;
    private AbstractBrowserWidget theSvgBrowser;
    private Combo theRootnodeCombo;
    private XSDTreeTraverser theTraverser = null;

    private boolean theRootnodeSelected = false;

    /**
     * Constructs a new wizard page, differs depending on the type of wizard
     * 
     * @param data The data to write to
     * @param type The type of wizard this wizard page is being created into
     */
    public XsdWizardPage(XsdWizardData data,WizardType type) {
        super("XSD Location");
        theType = type;
        theData = data;
        setTitle("Choose the XSD Location");
        setDescription("Choose an XSD Location and the root node then finish to generate an SVG");

        setPageComplete(false);
    }

    /**
     * Create contents of the wizard.
     * 
     * @param parent
     */
    public void createControl(Composite parent) {
        Composite container = new Composite(parent,SWT.NULL);
        container.setLayout(new GridLayout(1,false));

        String basepath =
            ResourcesPlugin.getWorkspace().getRoot().getLocation().toString();

        if (theType==WizardType.NEW) {
            theSvgBrowser =
                new DirectoryBrowserWidget(container,0,"Choose Output folder",
                    basepath);
            theSvgBrowser.getLabel().setText("Svg Folder");
        }
        else if (theType==WizardType.IMPORT) {
            FileTypeSelectionValidator validator =
                new FileTypeSelectionValidator(new String[] {"svg"},
                    "SVG Selected","Select an SVG");
            theSvgBrowser =
                new FileBrowserWidget(container,0,"Choose a SVG",basepath,
                    new String[] {"*.svg"},validator);
        }

        if (theType!=WizardType.INSERT) {
            theSvgBrowser.setLayoutData(new GridData(SWT.FILL,SWT.CENTER,false,
                false,1,1));
            theSvgBrowser.registerListener(this);
        }

        theXsdBrowser =
            new FileBrowserWidget(container,0,"Choose XSD",basepath,
                new String[] {"*.xsd"},
                new FileTypeSelectionValidator(new String[] {"xsd"},
                    "Valid XSD","Choose an XSD"));

        theXsdBrowser.getLabel().setText("XSD Location");
        theXsdBrowser.setLayoutData(new GridData(SWT.FILL,SWT.CENTER,true,
            false,1,1));

        setControl(container);

        Label sep = new Label(container,SWT.NONE);
        sep.setBackground(DocfactoUIConstants.COLOR_SEPARATOR);
        GridData gd_sep = new GridData(SWT.FILL,SWT.CENTER,true,false);
        gd_sep.heightHint = 1;
        sep.setLayoutData(gd_sep);

        Composite composite = new Composite(container,SWT.NONE);
        composite
            .setLayoutData(new GridData(SWT.LEFT,SWT.CENTER,true,false,1,1));
        composite.setLayout(new GridLayout(1,false));

        Label lblRootNode = new Label(composite,SWT.NONE);
        lblRootNode.setText("Root Node");

        theRootnodeCombo = new Combo(composite,SWT.READ_ONLY);
        GridData gd_combo = new GridData(SWT.RIGHT,SWT.CENTER,false,false,1,1);
        gd_combo.horizontalIndent = 10;
        gd_combo.widthHint = 480;
        theRootnodeCombo.setLayoutData(gd_combo);
        theRootnodeCombo.addSelectionListener(this);

        theXsdBrowser.registerListener(this);
    }

    private String getXsdPath() {
        return theXsdBrowser.getSelectedPath();
    }

    private void checkPageComplete() {
        boolean svgValid = true;
        if (theType!=WizardType.INSERT) {
            svgValid = theSvgBrowser.validate();
        }

        if (theXsdBrowser.validate()&&svgValid&&theRootnodeSelected) {
            if (theType==WizardType.NEW) {
                theData.outputpath = theSvgBrowser.getSelectedPath();
            }
            else if (theType==WizardType.IMPORT) {
                theData.svgpath = theSvgBrowser.getSelectedPath();
            }

            theData.xsdpath = getXsdPath();

            QName q = QName.valueOf(theRootnodeCombo.getText());
            theData.namespace = q.getNamespaceURI();
            theData.rootnode = q.getLocalPart();

            setPageComplete(true);
        }
        else {
            setPageComplete(false);
        }
    }

    /**
     * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
     */
    @Override
    public void widgetSelected(SelectionEvent e) {
        theRootnodeSelected = true;
        checkPageComplete();
    }

    /**
     * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
     */
    @Override
    public void widgetDefaultSelected(SelectionEvent e) {
    }

    /**
     * @see org.eclipse.swt.widgets.Listener#handleEvent(org.eclipse.swt.widgets.Event)
     */
    @Override
    public void handleEvent(Event event) {
        if (event.widget==theSvgBrowser) {
        }
        else {
            fillRootNodes();
        }
        checkPageComplete();
    }

    /**
     * If there is a path to the xsd set, then the combo box is filled with root
     * nodes.
     * 
     * @since 2.5.1
     */
    private void fillRootNodes() {
        theRootnodeCombo.removeAll();

        String xsd = theXsdBrowser.getSelectedPath();
        if (xsd==null||xsd.isEmpty()) {
            return;
        }

        theTraverser = new XSDTreeTraverser();
        try {
            theTraverser.visit(new File(xsd));

            List<QName> qnames = theTraverser.getElementNames();
            ArrayList<String> strings = new ArrayList<String>(qnames.size());
            for (QName node:qnames) {
                strings.add(node.toString());
                // theRootnodeCombo.add(node.toString());
            }

            String[] names = strings.toArray(new String[0]);
            Arrays.sort(names);
            theRootnodeCombo.setItems(names);
        }
        catch (DocfactoException e) {
            e.printStackTrace();
        }
    }
}
