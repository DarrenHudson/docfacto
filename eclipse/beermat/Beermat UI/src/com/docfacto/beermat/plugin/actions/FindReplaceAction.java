package com.docfacto.beermat.plugin.actions;

import java.util.ResourceBundle;

import org.eclipse.ui.IWorkbenchPart;

public class FindReplaceAction extends org.eclipse.ui.texteditor.FindReplaceAction {

    public FindReplaceAction(ResourceBundle bundle,String prefix,
    IWorkbenchPart workbenchPart) {
        super(bundle,prefix,workbenchPart);
    }

}
