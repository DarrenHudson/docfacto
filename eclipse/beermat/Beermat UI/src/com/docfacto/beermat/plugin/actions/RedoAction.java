package com.docfacto.beermat.plugin.actions;

import java.io.IOException;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.texteditor.ITextEditorActionConstants;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.plugin.BeermatEditor;
import com.docfacto.beermat.plugin.BeermatMultiPage;
import com.docfacto.beermat.svg.SVGManager;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.DocfactoException;

/**
 * A redo action
 * 
 * @author kporter - created 22 Jan 2014
 * @since 2.5.1
 */
public class RedoAction extends Action {

    private BeermatMultiPage theEditor;

    /**
     * Constructor.
     * 
     * @param editor The beermat editor
     */
    public RedoAction(BeermatMultiPage editor) {
        theEditor = editor;
    }

    @Override
    public void run() {
        if (theEditor.getActivePage()==1) {
            // Its the XML Editor, so lets call the Text Editor Print Action
            theEditor.getXMLEditor().getAction(ITextEditorActionConstants.REDO).run();
            return;
        }
        
        try {
            BeermatEditor editor = (BeermatEditor)theEditor.getBeertmatEditor();
            SVGManager manager = editor.getController().getManager();
            String currentState = manager.getFormattedDocument();

            String undoState = editor.getController().getUndoManager().redo(currentState);
            if (undoState!=null) {
                SVGDocument undoDom = SVGUtils.getSVGDocument(null,undoState);
                editor.getController().getManager().setSVGDocument(undoDom);
                theEditor.refreshInternal();
            }

        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (DocfactoException e) {
            e.printStackTrace();
        }

    }
}
