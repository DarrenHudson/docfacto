package com.docfacto.beermat.plugin.wizards;

/**
 * Data for a 'New Wizard'
 * @author kporter - created Jan 29, 2014
 * @since 2.5.1
 */
public class NewWizardData {

    /**
     * The height of the new SVG.
     */
    public int height;
    
    /**
     * The width of the new SVG.
     */
    public int width;
    
    /**
     * The String of the viewbox
     */
    public String viewbox;
    
    /**
     * Constructs a new wizard data with this width and height as a default
     * 
     * @since 2.5.1
     */
    public NewWizardData(int w,int h) {
        width = w;
        height = h;
    }

    public NewWizardData() {
        width = -1;
        height = -1;
    }

}
