package com.docfacto.beermat.plugin;

import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.ide.IDEActionFactory;
import org.eclipse.ui.part.MultiPageEditorActionBarContributor;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.texteditor.ITextEditorActionConstants;

import com.docfacto.beermat.plugin.actions.CopyAction;
import com.docfacto.beermat.plugin.actions.CutAction;
import com.docfacto.beermat.plugin.actions.PasteAction;
import com.docfacto.beermat.plugin.actions.PrintAction;
import com.docfacto.beermat.plugin.actions.RedoAction;
import com.docfacto.beermat.plugin.actions.RefreshAction;
import com.docfacto.beermat.plugin.actions.UndoAction;

/**
 * Manages the installation/de-installation of global actions for multi-page
 * editors. Responsible for the redirection of global actions to the active
 * editor. Multi-page contributor replaces the contributors for the individual
 * editors in the multi-page editor.
 * 
 * @author dhudson - created 10 Jul 2013
 * @since 2.4
 */
public class BeermatMultiPageEditorContributor extends MultiPageEditorActionBarContributor {

    /**
     * Creates a multi-page contributor.
     */
    public BeermatMultiPageEditorContributor() {

    }

    /**
     * @see org.eclipse.ui.part.MultiPageEditorActionBarContributor#setActiveEditor(org.eclipse.ui.IEditorPart)
     */
    @Override
    public void setActiveEditor(IEditorPart part) {
        IActionBars actionBars = getActionBars();
        if (actionBars!=null) {
            // This can ErrorEditorPart if it all goes Pete Tong
            if (part instanceof BeermatMultiPage) {
                BeermatMultiPage multiEditor = (BeermatMultiPage)part;

                ITextEditor editor = multiEditor.getXMLEditor();

                actionBars.setGlobalActionHandler(
                    ActionFactory.DELETE.getId(),editor.getAction(ITextEditorActionConstants.DELETE));
                actionBars.setGlobalActionHandler(
                    ActionFactory.UNDO.getId(),editor.getAction(ITextEditorActionConstants.UNDO));
                actionBars.setGlobalActionHandler(
                    ActionFactory.REDO.getId(),editor.getAction(ITextEditorActionConstants.REDO));
                actionBars.setGlobalActionHandler(
                    ActionFactory.CUT.getId(),editor.getAction(ITextEditorActionConstants.CUT));
                actionBars.setGlobalActionHandler(
                    ActionFactory.COPY.getId(),editor.getAction(ITextEditorActionConstants.COPY));
                actionBars.setGlobalActionHandler(
                    ActionFactory.PASTE.getId(),editor.getAction(ITextEditorActionConstants.PASTE));
                actionBars.setGlobalActionHandler(
                    ActionFactory.SELECT_ALL.getId(),editor.getAction(ITextEditorActionConstants.SELECT_ALL));
                
                //REMOVE:
                System.out.println("Find Action ..." +editor.getAction(ITextEditorActionConstants.FIND));
                
                actionBars.setGlobalActionHandler(
                    ActionFactory.FIND.getId(),editor.getAction(ITextEditorActionConstants.FIND));
                actionBars.setGlobalActionHandler(
                    IDEActionFactory.BOOKMARK.getId(),editor.getAction(IDEActionFactory.BOOKMARK.getId()));

                // Add in refresh and print
                actionBars.setGlobalActionHandler(ActionFactory.REFRESH.getId(),new RefreshAction(multiEditor));
                // editor.getAction(ITextEditorActionConstants.REFRESH));

                actionBars.setGlobalActionHandler(ActionFactory.PRINT.getId(),new PrintAction(multiEditor));
                // editor.getAction(ITextEditorActionConstants.PRINT));

                actionBars.setGlobalActionHandler(ActionFactory.UNDO.getId(),new UndoAction(multiEditor));
                
                actionBars.setGlobalActionHandler(ActionFactory.REDO.getId(),new RedoAction(multiEditor));
                
                actionBars.setGlobalActionHandler(ActionFactory.COPY.getId(),new CopyAction(multiEditor));
                
                actionBars.setGlobalActionHandler(ActionFactory.PASTE.getId(),new PasteAction(multiEditor));
                
                actionBars.setGlobalActionHandler(ActionFactory.CUT.getId(),new CutAction(multiEditor));

                actionBars.updateActionBars();
            }
        }
    }

    /**
     * @see org.eclipse.ui.part.MultiPageEditorActionBarContributor#setActivePage(org.eclipse.ui.IEditorPart)
     */
    public void setActivePage(IEditorPart part) {
    }

    // private void createActions() {
    // sampleAction = new Action() {
    // public void run() {
    // MessageDialog.openInformation(null,"XMLEditor",
    // "Sample Action Executed");
    // }
    // };
    // sampleAction.setText("Sample Action");
    // sampleAction.setToolTipText("Sample Action tool tip");
    // sampleAction.setImageDescriptor(PlatformUI.getWorkbench()
    // .getSharedImages().
    // getImageDescriptor(IDE.SharedImages.IMG_OBJS_TASK_TSK));
    // }
    //
    // public void contributeToMenu(IMenuManager manager) {
    // IMenuManager menu = new MenuManager("Editor &Menu");
    // manager.prependToGroup(IWorkbenchActionConstants.MB_ADDITIONS,menu);
    // menu.add(sampleAction);
    // }
    //
    // public void contributeToToolBar(IToolBarManager manager) {
    // manager.add(new Separator());
    // manager.add(sampleAction);
    // }
}
