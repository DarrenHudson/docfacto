package com.docfacto.beermat.plugin.actions;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.swing.SwingUtilities;

import org.apache.batik.ext.awt.RenderingHintsKeyExt;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.action.Action;
import org.eclipse.ui.texteditor.ITextEditorActionConstants;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.plugin.BeermatEditor;
import com.docfacto.beermat.plugin.BeermatMultiPage;
import com.docfacto.beermat.plugin.IBeermatEditor;
import com.docfacto.beermat.svg.IconTranscoder;

/**
 * Print Action
 * 
 * {@docfacto.system PrintTranscoder }
 * 
 * @author dhudson - created 24 Aug 2013
 * @since 2.4
 */
public class PrintAction extends Action {

    private final BeermatMultiPage theEditor;

    /**
     * Constructor.
     * 
     * @param multiPage
     */
    public PrintAction(BeermatMultiPage beermatMultiPage) {
        theEditor = beermatMultiPage;
    }

    /**
     * @see org.eclipse.jface.action.Action#run()
     */
    @Override
    public void run() {
        if (theEditor.getActivePage()==1) {
            // Its the XML Editor, so lets call the Text Editor Print Action
            theEditor.getXMLEditor().getAction(ITextEditorActionConstants.PRINT).run();
            return;
        }

        final IBeermatEditor editor = theEditor.getBeertmatEditor();
        if (editor instanceof BeermatEditor) {
            // Need to execute in AWT thread
            SwingUtilities.invokeLater(new Runnable() {
                /**
                 * @see java.lang.Runnable#run()
                 */
                @Override
                public void run() {
                    BeermatController controller = ((BeermatEditor)editor).getController();

                    // Build a PrintTranscoder to handle printing
                    // of the svgDocument object
                    // //
                    // PrintTranscoder pt = new p2();
                    // pt.addTranscodingHint(PrintTranscoder.KEY_SHOW_PAGE_DIALOG,Boolean.TRUE);
                    // pt.addTranscodingHint(PrintTranscoder.KEY_SHOW_PRINTER_DIALOG,Boolean.TRUE);
                    // pt.addTranscodingHint(PrintTranscoder.KEY_LANGUAGE,"en");
                    //
                    // pt.transcode(new
                    // TranscoderInput(controller.getManager().getDocument()),null);

                    // try {
                    // pt.print();
                    // }
                    // catch (PrinterException ex) {
                    // BeermatUIPlugin.logException("Unable to print SVG",ex);
                    // }

                    int width = controller.getManager().getDocumentSize().getWidth();
                    int height = controller.getManager().getDocumentSize().getHeight();

                    IconTranscoder transcoder = new IconTranscoder(width,height);
                    BufferedImage image = null;
                    try {
                        transcoder.transcode(new TranscoderInput(controller.getManager().getDocument()),null);
                        image = transcoder.getImage();
                    }
                    catch (TranscoderException e) {
                        e.printStackTrace();
                    }

                    PrinterJob printJob = PrinterJob.getPrinterJob();
                    PageFormat format = printJob.defaultPage();

                    Paper paper = format.getPaper();
                    paper.setImageableArea(0,0,width,height);

                    format = printJob.validatePage(format);
                    format = printJob.pageDialog(printJob.defaultPage());

                    if (!printJob.printDialog()) {
                        return;
                    }
                    Assert.isNotNull(image);
                    printJob.setPrintable(new ImagePrintable(image),format);

                    try {
                        printJob.print();
                    }
                    catch (PrinterException prt) {
                        prt.printStackTrace();
                    }
                }
            });
        }
    }

    class ImagePrintable implements Printable {
        private BufferedImage _image;

        public ImagePrintable(BufferedImage image) {
            _image = image;
        }

        @Override
        public int print(Graphics _g,PageFormat pageFormat,int pageIndex) throws PrinterException {
            if (pageIndex==0) {
                int x = (int)pageFormat.getImageableX();
                int y = (int)pageFormat.getImageableY();
                int width = (int)pageFormat.getImageableWidth();
                int height = (int)pageFormat.getImageableHeight();

                Graphics2D g = (Graphics2D)_g;
                g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
                g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                g.setRenderingHint(RenderingHintsKeyExt.KEY_TRANSCODING,RenderingHintsKeyExt.VALUE_TRANSCODING_PRINTING);

                g.drawImage(_image,x,y,width,height,null);
                return PAGE_EXISTS;
            }
            else {
                return NO_SUCH_PAGE;
            }
        }

    }
}
