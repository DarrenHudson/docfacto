package com.docfacto.beermat.plugin.actions;

import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.texteditor.ITextEditorActionConstants;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.ClipboardManager;
import com.docfacto.beermat.plugin.BeermatEditor;
import com.docfacto.beermat.plugin.BeermatMultiPage;

/**
 * A copy action
 * 
 * @author kporter - created 22 Jan 2014
 * @since 2.5.1
 */
public class CopyAction extends Action {

    private BeermatMultiPage theEditor;

    /**
     * Constructs a new copy action.
     * 
     * @param editor The beermat editor
     */
    public CopyAction(BeermatMultiPage editor) {
        theEditor = editor;
    }

    @Override
    public void run() {
        if (theEditor.getActivePage()==1) {
            // Its the XML Editor, so lets call the Text Editor Print Action
            theEditor.getXMLEditor().getAction(ITextEditorActionConstants.COPY).run();
            return;
        }
        
        BeermatEditor editor = (BeermatEditor)theEditor.getBeertmatEditor();
        ClipboardManager clipboard = editor.getController().getClipboard();
        List<Element> elements =
            editor.getController().getBeermatAWTCanvas().getHighlightManager().getHighlightedElements();

        if (!elements.isEmpty()) {
            clipboard.copy(elements);
        }
    }
}
