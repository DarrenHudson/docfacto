package com.docfacto.beermat.plugin.wizards;

import java.io.IOException;

import org.w3c.dom.Document;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.SVGDirtyEvent;
import com.docfacto.beermat.events.SVGSizeEvent;
import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.beermat.updaters.AbstractUpdater;
import com.docfacto.common.DocfactoException;
import com.docfacto.core.utils.PluginUtils;
import com.docfacto.core.utils.SWTUtils;

/**
 * A wizard to insert an XSD into an open SVG
 * 
 * @author kporter - created Jan 2014
 * @since 2.5.1
 */
public class XsdInsertWizard extends XsdImportWizard {

    private BeermatController theController;
    private Document theDocument;

    /**
     * A wizard that inserts selected XSD files into the given document.
     * 
     * @param document The document that will have an xsd inserted
     */
    public XsdInsertWizard(BeermatController controller) {
        super(WizardType.INSERT);
        theController = controller;
        theDocument = theController.getManager().getDocument();
    }

    /**
     * @see com.docfacto.beermat.plugin.wizards.XsdImportWizard#performFinish()
     */
    @Override
    public boolean performFinish() {
        try {
            final Document newDoc = getDocumentFromXSD();
            theController.applyUpdates(new AbstractUpdater(theController) {
                /**
                 * @see com.docfacto.beermat.updaters.AbstractUpdater#applyUpdates()
                 */
                @Override
                public boolean applyUpdates() {
                    SWTUtils.UIThreadExec(new Runnable() {

                        /**
                         * @see java.lang.Runnable#run()
                         */
                        @Override
                        public void run() {
                            PluginUtils
                                .displayStatusLineMessage(
                                    theController.getMultiPageEditor(),
                                    "");
                        }
                    });

                    
                    insertSVGintoDoc(theDocument,newDoc,theController
                        .getManager().getDocumentSize());

                    theController.processEvent(new SVGDirtyEvent(this));
                    theController.processEvent(new SVGSizeEvent(this,
                        theController.getBeermatAWTCanvas()));

                    SWTUtils.UIThreadExec(new Runnable() {

                        /**
                         * @see java.lang.Runnable#run()
                         */
                        @Override
                        public void run() {
                            PluginUtils
                                .displayStatusLineMessage(
                                    theController.getMultiPageEditor(),
                                    "XSD Inserted");
                        }
                    });

                    return true;
                }
            });
            return true;
        }
        catch (DocfactoException e) {
            BeermatUIPlugin.logException("Unable to import schema",e);
            return false;
        }
        catch (IOException e) {
            BeermatUIPlugin.logException("Unable to import schema",e);
            return false;
        }
    }
}
