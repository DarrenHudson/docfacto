package com.docfacto.beermat.plugin.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.wizard.WizardDialog;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.plugin.BeermatMultiPage;
import com.docfacto.beermat.plugin.wizards.XsdInsertWizard;
import com.docfacto.core.utils.SWTUtils;

/**
 * Opens a XsdInsertWizard, and inserts the chosen xsd into this SVG
 * 
 * @author kporter - created 3 Feb 2014
 * @since 2.5.2
 */
public class InsertXsdAction extends Action {

    private BeermatController theController;

    /**
     * Constructor.
     * 
     * @param controller Beermat Controller
     * @since 2.5
     */
    public InsertXsdAction(BeermatController controller) {
        theController = controller;
    }

    /**
     * @see org.eclipse.jface.action.Action#run()
     */
    @Override
    public void run() {
        XsdInsertWizard wizard = new XsdInsertWizard(theController);
        BeermatMultiPage editor = theController.getMultiPageEditor();
        wizard.init(editor.getEditorSite().getWorkbenchWindow().getWorkbench(),
            null);

        final WizardDialog wd =
            new WizardDialog(editor.getSite().getShell(),wizard);

        // For when called from the AWT thread.
        SWTUtils.UIThreadExec(new Runnable() {
            @Override
            public void run() {
                wd.open();
            }
        });
    }

}
