package com.docfacto.beermat.plugin.wizards;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWizard;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.IDE;

import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.core.utils.PluginUtils;

/**
 * An abstract wizard for getting xsd's into svg's
 * 
 * @author kporter - created 10 Jan 2014
 * @since 2.5.1
 */
public abstract class AbstractXsdWizard extends Wizard implements IWorkbenchWizard {
    XsdWizardData theData;
    XsdWizardPage thePage;

    /**
     * Constructor.
     * 
     * @param type The type of wizard
     */
    public AbstractXsdWizard(WizardType type) {
        theData = new XsdWizardData();

        switch (type) {
        case IMPORT:
            setWindowTitle("Import XSD in to the chosen SVG");
            break;
        case NEW:
            setWindowTitle("Create new SVG from XSD");
            break;
        case INSERT:
            setWindowTitle("Insert XSD in to the current SVG");
            break;
        }

        thePage = new XsdWizardPage(theData,type);
    }

    @Override
    public void init(IWorkbench workbench,IStructuredSelection selection) {
        theData.workbench = workbench;
        theData.selection = selection;

        addPage(thePage);
    }

    /**
     * Opens a new file that has recently been created.
     * 
     * @param outputLocation The location of the newly created file
     * @return True if the new file created was opened
     * @since 2.5.1
     */
    public boolean openFile(String outputLocation) {
        IFile file = PluginUtils.findIFileInWorkspace(outputLocation);
        if (file!=null) {
            try {
                IDE.openEditor(theData.workbench.getActiveWorkbenchWindow().getActivePage(),file);
                return true;
            }
            catch (PartInitException e) {
                BeermatUIPlugin.logMessage("XSD Created, but could not open in beermat");
            }
        }
        return false;
    }

}
