package com.docfacto.beermat.plugin;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.part.MultiPageEditorPart;

import com.docfacto.beermat.ui.BeermatViewConstants;
import com.docfacto.beermat.ui.widgets.listeners.ColourLabelOnMouseOver;
import com.docfacto.core.utils.SWTUtils;

/**
 * An error page.
 * 
 * {@docfacto.todo TODO: Make abstract so that other plugin's can use.}
 * 
 * @author kporter - created Oct 24, 2013
 * @since 2.4.7
 */
public class BeermatFileErrorPage extends EditorPart implements IBeermatEditor {

    public static final String ID = "com.docfacto.beermat.plugin.BeermatFileErrorPage"; //$NON-NLS-1$

    private String theTitle;
    private String theMessage;
    private MultiPageEditorPart theParentEditor;

    public BeermatFileErrorPage(MultiPageEditorPart parentEditor,String title,String message) {
        theTitle = title;
        theMessage = message;
        theParentEditor = parentEditor;
    }

    private static Color FOREGROUND = BeermatViewConstants.COLOR_FOREGROUND_COMPLEMENTARY;
    private static Color COMPLEMENT = BeermatViewConstants.COLOR_FOREGROUND_BRAND;
    private Composite theContainer;

    private Label theCloseButton;

    private Label theNewButton;
    
    private boolean theNewButtonHidden = false;

    /**
     * Create contents of the editor part.
     * 
     * @param parent
     */
    @Override
    public void createPartControl(Composite parent) {
        theContainer = new Composite(parent,SWT.NONE);
        theContainer.setBackground(BeermatViewConstants.COLOR_TOOLBAR_PRIMARY);
        GridLayout gl_container = new GridLayout(1,false);
        gl_container.marginTop = 5;
        gl_container.marginHeight = 0;
        gl_container.verticalSpacing = 0;
        gl_container.horizontalSpacing = 0;
        theContainer.setLayout(gl_container);

        Composite titleArea = new Composite(theContainer,SWT.NONE);
        titleArea.setBackground(BeermatViewConstants.COLOR_TOOLBAR_PRIMARY);
        RowLayout rl_titleArea = new RowLayout(SWT.HORIZONTAL);
        rl_titleArea.marginTop = 13;
        rl_titleArea.center = true;
        rl_titleArea.wrap = false;
        titleArea.setLayout(rl_titleArea);
        GridData gd_titleArea = new GridData(SWT.FILL,SWT.CENTER,true,false,1,1);
        gd_titleArea.heightHint = 60;
        gd_titleArea.widthHint = 800;
        titleArea.setLayoutData(gd_titleArea);

        Label lblErrorIcon = new Label(titleArea,SWT.NONE);
        Image errorIcon = parent.getDisplay().getSystemImage(SWT.ICON_ERROR);
        lblErrorIcon.setImage(errorIcon);

        Label errorTitle = new Label(titleArea,SWT.NONE);
        errorTitle.setFont(SWTUtils.getFont("Lucida Grande",13,SWT.BOLD));
        errorTitle.setText(theTitle);
        errorTitle.setForeground(FOREGROUND);
        if (errorTitle.getText().length()>120) {
            errorTitle.setFont(SWTUtils.getFont("Lucida Grande",12,SWT.BOLD));
        }

        createSeparator(theContainer);

        createSpacer(theContainer);

        createSeparator(theContainer);

        createMessageArea(theContainer);

        createSeparator(theContainer);

        createButtonArea(theContainer);

        createSeparator(theContainer);

        createSpacer(theContainer);

    }

    protected Composite createMessageArea(Composite parent) {
        Composite container = new Composite(parent,SWT.NONE);
        container.setBackground(BeermatViewConstants.COLOR_TOOLBAR_SECONDARY);
        container.setLayoutData(new GridData(SWT.FILL,SWT.FILL,false,true,1,1));

        GridLayout layout_container = new GridLayout(1,false);
        layout_container.verticalSpacing = 0;
        layout_container.marginWidth = 0;
        layout_container.marginHeight = 0;
        layout_container.horizontalSpacing = 0;
        container.setLayout(layout_container);

        Composite contents = new Composite(container,SWT.NONE);
        contents.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true,1,1));

        GridLayout layout = new GridLayout(1,false);
        layout.marginWidth = 8;
        layout.marginHeight = 8;
        contents.setLayout(layout);

        Label errorMessage = new Label(contents,SWT.WRAP);
        errorMessage.setLayoutData(new GridData(SWT.LEFT,SWT.TOP,true,false,1,1));
        errorMessage.setText(theMessage);
        return contents;
    }

    protected Composite createButtonArea(Composite parent) {
        Composite composite = new Composite(parent,SWT.NONE);
        composite.setBackground(SWTUtils.getColor(SWT.COLOR_WHITE));
        GridLayout gl_composite = new GridLayout(5,false);
        gl_composite.horizontalSpacing = 0;
        gl_composite.verticalSpacing = 0;
        gl_composite.marginHeight = 0;
        composite.setLayout(gl_composite);
        GridData gd = new GridData(SWT.FILL,SWT.CENTER,false,false,1,1);
        gd.heightHint = 25;
        composite.setLayoutData(gd);

        Label lblSpace = new Label(composite,SWT.NONE);
        lblSpace.setLayoutData(new GridData(SWT.LEFT,SWT.CENTER,true,false,1,1));

        Label lblGridHorizontalSep = new Label(composite,SWT.NONE);
        GridData gd_lblSep3 = new GridData(SWT.LEFT,SWT.FILL,false,false,1,1);
        gd_lblSep3.widthHint = 1;
        lblGridHorizontalSep.setLayoutData(gd_lblSep3);
        lblGridHorizontalSep.setBackground(BeermatViewConstants.COLOR_SEPARATOR);

        theNewButton = new Label(composite,SWT.NONE);
        theNewButton.setAlignment(SWT.CENTER);
        theNewButton.setBackground(SWTUtils.getColor(SWT.COLOR_WHITE));
        theNewButton.setText("New SVG");
        theNewButton.addMouseTrackListener(new ColourLabelOnMouseOver(FOREGROUND,COMPLEMENT));
        theNewButton.setForeground(FOREGROUND);
        GridData gd_newButton = new GridData(SWT.LEFT,SWT.CENTER,false,false,1,1);
        gd_newButton.widthHint = 80;
        gd_newButton.exclude = theNewButtonHidden;
        theNewButton.setLayoutData(gd_newButton);

        Label lblGridHorizontalSep1 = new Label(composite,SWT.NONE);
        GridData gd_lblSep4 = new GridData(SWT.CENTER,SWT.FILL,false,false,1,1);
        gd_lblSep4.widthHint = 1;
        lblGridHorizontalSep1.setLayoutData(gd_lblSep4);
        lblGridHorizontalSep1.setBackground(BeermatViewConstants.COLOR_SEPARATOR);

        theCloseButton = new Label(composite,SWT.NONE);
        theCloseButton.setBackground(SWTUtils.getColor(SWT.COLOR_WHITE));
        theCloseButton.setAlignment(SWT.CENTER);
        GridData gd_closeButton = new GridData(SWT.LEFT,SWT.CENTER,false,true,1,1);
        gd_closeButton.widthHint = 80;
        theCloseButton.setLayoutData(gd_closeButton);
        theCloseButton.setText("Close Editor");
        theCloseButton.addMouseTrackListener(new ColourLabelOnMouseOver(FOREGROUND,COMPLEMENT));
        theCloseButton.setForeground(FOREGROUND);

        theCloseButton.addMouseListener(new MouseAdapter() {
            /**
             * @see org.eclipse.swt.events.MouseAdapter#mouseDown(org.eclipse.swt.events.MouseEvent)
             */
            @Override
            public void mouseDown(MouseEvent e) {
                theParentEditor.getSite().getPage().closeEditor(theParentEditor,false);
            }
        });
        theNewButton.addMouseListener(new MouseAdapter() {
            /**
             * @see org.eclipse.swt.events.MouseAdapter#mouseDown(org.eclipse.swt.events.MouseEvent)
             */
            @Override
            public void mouseDown(MouseEvent e) {
                try {
                    new WizardHandler().execute(new ExecutionEvent());
                    theParentEditor.getSite().getPage().closeEditor(theParentEditor,false);
                }
                catch (ExecutionException e1) {
                }
            }
        });

        return composite;
    }
    
    public void setHideNewButton(boolean visible){
        theNewButtonHidden = visible;
        
    }

    private static Label createSeparator(Composite parent) {
        Label sep = new Label(parent,SWT.NONE);
        sep.setBackground(BeermatViewConstants.COLOR_SEPARATOR);
        GridData gd = new GridData(SWT.FILL,SWT.CENTER,false,false,1,1);
        gd.heightHint = 1;
        sep.setLayoutData(gd);
        return sep;
    }

    private static Label createSpacer(Composite parent) {
        Label spacer = new Label(parent,SWT.NONE);
        GridData gd = new GridData(SWT.LEFT,SWT.CENTER,false,false,1,1);
        gd.heightHint = 10;
        spacer.setLayoutData(gd);
        return spacer;
    }

    @Override
    public void init(IEditorSite site,IEditorInput input) throws PartInitException {
    }

    @Override
    public void setFocus() {
    }

    @Override
    public boolean isDirty() {
        return false;
    }

    @Override
    public boolean isSaveAsAllowed() {
        return false;
    }

    @Override
    public void doSave(IProgressMonitor monitor) {
    }

    @Override
    public void doSaveAs() {
    }
    
    @Override
    public boolean hasChanged() {
        return false;
    }

    @Override
    public void resetHasChanged() {
    }
}
