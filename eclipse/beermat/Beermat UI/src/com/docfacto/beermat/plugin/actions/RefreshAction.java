package com.docfacto.beermat.plugin.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.texteditor.ITextEditorActionConstants;

import com.docfacto.beermat.plugin.BeermatMultiPage;

/**
 * Handle the refresh action
 * 
 * @author dhudson - created 24 Aug 2013
 * @since 2.4
 */
public class RefreshAction extends Action {

    private final BeermatMultiPage theEditor;

    /**
     * Constructor.
     * 
     * @param beermatMultiPage
     */
    public RefreshAction(BeermatMultiPage beermatMultiPage) {
        theEditor = beermatMultiPage;
    }

    /**
     * @see org.eclipse.jface.action.Action#run()
     */
    @Override
    public void run() {
        if (theEditor.getActivePage()==1) {
            // Its the XML Editor, so lets call the Text Editor Print Action
            theEditor.getXMLEditor().getAction(ITextEditorActionConstants.REFRESH).run();
        }
        theEditor.refreshInternal();
    }

}
