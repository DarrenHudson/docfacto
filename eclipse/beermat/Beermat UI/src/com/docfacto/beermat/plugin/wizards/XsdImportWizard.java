package com.docfacto.beermat.plugin.wizards;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.IDE;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.svg.SVGDocument;
import org.w3c.dom.svg.SVGRect;

import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.beermat.svg.SVGSize;
import com.docfacto.beermat.utils.ElementSizer;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.common.StringUtils;
import com.docfacto.common.XMLUtils;
import com.docfacto.core.utils.PluginUtils;
import com.docfacto.xsd2common.XSDArgumentParser;
import com.docfacto.xsd2svg.XSD2SVG;

/**
 * A wizard for importing XSD files into an existing SVG image.
 * 
 * @author kporter - created 10 Jan 2014
 * @since 2.5.1
 */
public class XsdImportWizard extends AbstractXsdWizard implements IImportWizard {

    /**
     * Constructs a XsdImportWizard
     */
    public XsdImportWizard() {
        super(WizardType.IMPORT);
    }

    /**
     * Constructs an Import Wizard but with a different type set.
     * 
     * @param type The wizard type to set
     */
    protected XsdImportWizard(WizardType type) {
        super(type);
    }

    /**
     * @see org.eclipse.jface.wizard.Wizard#performFinish()
     */
    @Override
    public boolean performFinish() {
        try {
            Document newDoc = getDocumentFromXSD();

            File file = new File(theData.svgpath);
            Document doc = SVGUtils.getSVGDocument(file.toURI());

            insertSVGintoDoc(doc,newDoc);

            String contents = XMLUtils.prettyFormat(doc);
            IFile ifile =
                PluginUtils.findIFileInWorkspace(file.getAbsolutePath());

            if (ifile!=null) {
                // File is in the workspace
                if (!PluginUtils.checkoutFile(ifile)) {
                    // File could not be checked out
                    MessageDialog.openError(getShell(),
                        "Could not check out file",
                        "Cannot check out the SVG file");
                    return false;
                }
            }

            IOUtils.writeFile(file,contents);

            try {
                IDE.openEditor(theData.workbench.getActiveWorkbenchWindow()
                    .getActivePage(),ifile);
            }
            catch (PartInitException ex) {
                BeermatUIPlugin.logException(
                    "XsdImportWizard: PartInitException Exception ",ex);
                return false;
            }

            return true;
        }
        catch (DocfactoException ex) {
            BeermatUIPlugin.logException(
                "XsdImportWizard: Docfacto Exception ",ex);
            return false;
        }
        catch (IOException ex) {
            BeermatUIPlugin.logException(
                "XsdImportWizard: IOException Exception ",ex);
            return false;
        }
    }

    /**
     * Get a SVGDocument from the XSD
     * 
     * @return A SVG constructed from the XSD
     * @throws DocfactoException
     * @throws IOException If uri is invalid/cannot create SVGDocument
     * @since 2.5.1
     */
    public Document getDocumentFromXSD() throws DocfactoException, IOException {
        XSDArgumentParser parser =
            new XSDArgumentParser(theData.constructArguments());
        parser.validate();

        String svg = new XSD2SVG(parser).toString();

        return SVGUtils.getSVGDocument(null,svg);
    }

    /**
     * Given two documents, this will insert the second document into the first
     * document.
     * 
     * @param doc The original doc that will receive a new svg inside of it
     * @param svgToInsert The svg to insert into the side of the original doc
     * @since 2.5.1
     */
    public static void insertSVGintoDoc(Document doc,Document svgToInsert) {
        ElementSizer sizer = ElementSizer.INSTANCE;

        SVGRect docSize = sizer.getElementBounds(doc.getDocumentElement());

        SVGRect newDocSize =
            sizer.getElementBounds(svgToInsert.getDocumentElement());

        int tallestHeight =
            docSize.getHeight()>newDocSize.getHeight() ? (int)docSize
                .getHeight() : (int)newDocSize.getHeight();

        Point newTotalSize =
            new Point((int)(docSize.getWidth()+newDocSize.getWidth()+1),
                tallestHeight);

        SVGUtils.setSVGSize((SVGDocument)doc,newTotalSize);

        SVGUtils.setAttribute(svgToInsert.getDocumentElement(),"x",
            (int)docSize.getWidth()+1);
        SVGUtils.setAttribute(svgToInsert.getDocumentElement(),"y",0);

        Node root = doc.importNode(svgToInsert.getDocumentElement(),true);

        doc.getDocumentElement().appendChild(root);
    }

    /**
     * Given two documents, this will insert the second document into the first
     * document.
     * 
     * {@docfacto.system added this method because ElementSizer kept returning
     * null when trying to get the size for the Document doc}
     * 
     * @param doc The document in which the 'svgToInsert' will be inserted
     * @param svgToInsert The SVG of the XSD to insert
     * @param originalSize The size of the 'doc'
     * @since 2.5.1
     */
    public static void insertSVGintoDoc(Document doc,Document svgToInsert,
    SVGSize originalSize) {

        String height = svgToInsert.getDocumentElement().getAttribute("height");

        if (!StringUtils.nullOrEmpty(height)) {
            int newHeight = Integer.parseInt(height);

            if (originalSize.getHeight()<newHeight) {
                Point newTotalSize =
                    new Point((int)originalSize.getWidth(),newHeight);

                SVGUtils.setSVGSize((SVGDocument)doc,newTotalSize);
            }
        }

        SVGUtils.setAttribute(svgToInsert.getDocumentElement(),"x",
            20);
        SVGUtils.setAttribute(svgToInsert.getDocumentElement(),"y",20);

        Node root = doc.importNode(svgToInsert.getDocumentElement(),true);
        doc.getDocumentElement().appendChild(root);

    }
}
