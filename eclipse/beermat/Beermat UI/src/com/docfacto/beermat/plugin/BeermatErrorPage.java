package com.docfacto.beermat.plugin;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.MultiPageEditorPart;

import com.docfacto.common.Java;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.core.IDocfactoCorePlugin;
import com.docfacto.core.utils.PluginUtils;

/**
 * Display an error page grumbling about the Java version on OSX
 * 
 * @author dhudson - created 7 Oct 2013
 * @since 2.4
 */
public class BeermatErrorPage extends BeermatFileErrorPage implements IBeermatEditor {
    public BeermatErrorPage(MultiPageEditorPart parent) {
        super(parent,"Cannot open Beermat due to invalid Java version","Cannot open");
        super.setHideNewButton(true);
    }

    private static final String ORACLE_DOWNLOAD_SITE =
        "http://www.oracle.com/technetwork/java/javase/downloads/index.html";

    private static final String ECLIPSE_DOWNLOAD_SITE =
        "http://www.eclipse.org/downloads/";

    /**
     * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public void doSave(IProgressMonitor monitor) {
        // Never
    }

    /**
     * @see org.eclipse.ui.part.EditorPart#doSaveAs()
     */
    @Override
    public void doSaveAs() {
        // Never
    }

    /**
     * @see com.docfacto.beermat.plugin.BeermatEditor#init(org.eclipse.ui.IEditorSite,
     * org.eclipse.ui.IEditorInput)
     */
    @Override
    public void init(IEditorSite site,IEditorInput input)
    throws PartInitException {
        // Do nothing
    }

    /**
     * @see org.eclipse.ui.part.EditorPart#isDirty()
     */
    @Override
    public boolean isDirty() {
        return false;
    }

    /**
     * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
     */
    @Override
    public boolean isSaveAsAllowed() {
        return false;
    }

    /**
     * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Composite createMessageArea(Composite parent) {
        Composite container = new Composite(parent,SWT.FILL);
        GridData d = new GridData(SWT.FILL,SWT.FILL,true,true);
        container.setLayoutData(d);

        GridLayout layout = new GridLayout();
        layout.makeColumnsEqualWidth = false;
        layout.numColumns = 2;

        container.setLayout(layout);

        Label image = new Label(container,SWT.NONE);
        image.setImage(DocfactoCorePlugin
            .getImage(IDocfactoCorePlugin.DOCFACTO_IMAGE));
        GridData gridData = new GridData();
        gridData.grabExcessHorizontalSpace = false;

        image.setLayoutData(gridData);

        Label label = new Label(container,SWT.NONE);
        label.setText("I'm sorry, Beermat is unable to run due to : -");
        FontData fontData = label.getFont().getFontData()[0];
        Font font =
            new Font(Display.getCurrent(),new FontData(fontData.getName(),
                fontData
                    .getHeight()+4,SWT.BOLD));
        label.setFont(font);
        gridData = new GridData();
        gridData.horizontalAlignment = SWT.LEFT;
        label.setLayoutData(gridData);

        // Spacer
        gridData = new GridData();
        gridData.horizontalSpan = 2;
        new Label(container,SWT.NONE).setLayoutData(gridData);

        Java java = new Java();
        if (java.getMinorVersion()==7&&java.getBuildVersion()<40) {
            Link link = new Link(container,SWT.NONE);
            link.setText("A bug with OSX and Java 1.7, Beermat only runs with Java version 1.7 build 40 or above, which can be found <a href=\""+
                ORACLE_DOWNLOAD_SITE+"\">here</a>");
            gridData = new GridData();
            gridData.horizontalSpan = 2;
            gridData.horizontalAlignment = SWT.CENTER;

            link.setLayoutData(gridData);

            link.addSelectionListener(new SelectionListener() {
                /**
                 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
                 */
                @Override
                public void widgetDefaultSelected(SelectionEvent event) {
                }

                /**
                 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
                 */
                @Override
                public void widgetSelected(SelectionEvent event) {
                    // Launch the default OS browser ..
                    org.eclipse.swt.program.Program
                        .launch(ORACLE_DOWNLOAD_SITE);
                }
            });
        }

        // Spacer
        gridData = new GridData();
        gridData.horizontalSpan = 2;
        new Label(container,SWT.NONE).setLayoutData(gridData);

        if (PluginUtils.getEclipseVersion().getMinor()<3) {
            Link link = new Link(container,SWT.NONE);
            link.setText("A bug with OSX and Eclipse, Beermat will only run with Eclipse Kepler when using Java 7, Kepler can be downloaded <a href=\""+
                ECLIPSE_DOWNLOAD_SITE+"\">here</a>");
            gridData = new GridData();
            gridData.horizontalSpan = 2;
            gridData.horizontalAlignment = SWT.CENTER;

            link.setLayoutData(gridData);

            link.addSelectionListener(new SelectionListener() {
                /**
                 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
                 */
                @Override
                public void widgetDefaultSelected(SelectionEvent event) {
                }

                /**
                 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
                 */
                @Override
                public void widgetSelected(SelectionEvent event) {
                    // Launch the default OS browser ..
                    org.eclipse.swt.program.Program.launch(ECLIPSE_DOWNLOAD_SITE);
                }
            });
        }

        // Spacer
        gridData = new GridData();
        gridData.horizontalSpan = 2;
        new Label(container,SWT.NONE).setLayoutData(gridData);

        gridData = new GridData();
        gridData.horizontalSpan = 2;
        gridData.horizontalAlignment = SWT.CENTER;

        Label text = new Label(container,SWT.NONE);
        text.setText("The good news is that the other features will still work, so please check out :");
        text.setLayoutData(gridData);
        text.setFont(new Font(Display.getCurrent(),new FontData(fontData
            .getName(),
            fontData
                .getHeight()+2,SWT.BOLD|SWT.ITALIC)));

        // Spacer
        gridData = new GridData();
        gridData.horizontalSpan = 2;
        new Label(container,SWT.NONE).setLayoutData(gridData);

        image = new Label(container,SWT.NONE);
        image.setImage(DocfactoCorePlugin
            .getImage(IDocfactoCorePlugin.ADAM_ICON_32));

        text = new Label(container,SWT.NONE);
        text.setText("Adam, Javadoc processor.");

        image = new Label(container,SWT.NONE);
        image.setImage(DocfactoCorePlugin
            .getImage(IDocfactoCorePlugin.TAGLETS_ICON_32));

        text = new Label(container,SWT.NONE);
        text.setText("Taglets, Add additional information to standard Javadoc.");

        image = new Label(container,SWT.NONE);
        image.setImage(DocfactoCorePlugin
            .getImage(IDocfactoCorePlugin.GRABIT_ICON_32));

        text = new Label(container,SWT.NONE);
        text.setText("Grabit, Capture screen shots with Eclipse.");

        return container;
    }

    /**
     * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
     */
    @Override
    public void setFocus() {
        // Never
    }

    /**
     * @see com.docfacto.beermat.plugin.IBeermatEditor#hasChanged()
     */
    public boolean hasChanged() {
        return false;
    }

    /**
     * @see com.docfacto.beermat.plugin.BeermatEditor#dispose()
     */
    @Override
    public void dispose() {
        // Nothing to do here.
    }

    /**
     * @see com.docfacto.beermat.plugin.IBeermatEditor#resetHasChanged()
     */
    @Override
    public void resetHasChanged() {
        // TODO Auto-generated method stub

    }

}
