package com.docfacto.beermat.plugin.actions;

import java.awt.geom.AffineTransform;
import java.io.IOException;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.texteditor.ITextEditorActionConstants;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.UndoManager;
import com.docfacto.beermat.plugin.BeermatEditor;
import com.docfacto.beermat.plugin.BeermatMultiPage;
import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.beermat.svg.SVGManager;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.DocfactoException;

/**
 * An Undo action
 * 
 * @author kporter - created 22 Jan 2014
 * @since 2.5.1
 */
public class UndoAction extends Action {

    private BeermatMultiPage theEditor;

    /**
     * Constructor.
     * 
     * @param editor The beermat editor
     * @since 2.5
     */
    public UndoAction(BeermatMultiPage editor) {
        theEditor = editor;
    }

    /**
     * @see org.eclipse.jface.action.Action#run()
     */
    @Override
    public void run() {
        if (theEditor.getActivePage()==1) {
            // Its the XML Editor, so lets call the Text Editor Print Action
            theEditor.getXMLEditor().getAction(ITextEditorActionConstants.UNDO).run();
            return;
        }
        
        try {
            BeermatEditor editor = (BeermatEditor)theEditor.getBeertmatEditor();
            SVGManager manager = editor.getController().getManager();
            UndoManager undoManager = editor.getController().getUndoManager();
            String currentState = manager.getFormattedDocument();

            String undoState = undoManager.undo(currentState);

            if (undoState!=null) {
                
                AffineTransform paint = editor.getController().getBeermatAWTCanvas().getPaintingTransform();
                AffineTransform rend = editor.getController().getBeermatAWTCanvas().getRenderingTransform();
                
                SVGDocument undoDom = SVGUtils.getSVGDocument(null,undoState);
                editor.getController().getManager().setSVGDocument(undoDom);

                editor.getController().getBeermatAWTCanvas().setPaintingTransform(paint);
                editor.getController().getBeermatAWTCanvas().setRenderingTransform(rend);
                
                editor.getController().getBeermatAWTCanvas()
                    .getHighlightManager().removeHighlights();
            }
        }
        catch (IOException ex) {
            BeermatUIPlugin.logException("Unable to undo",ex);
        }
        catch (DocfactoException ex) {
            BeermatUIPlugin.logException("Unable to undo",ex);
        }

    }
}
