package com.docfacto.beermat.plugin;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.licensor.Products;

/**
 * This class is the main Activator class for the plugin.
 * <p>
 * Extends AbstractUIPlugin to define this plugin.
 * </p>
 * 
 * @author kporter - created Jan 3, 2013
 * @since 2.1
 */
public class BeermatUIPlugin extends AbstractUIPlugin {

    public static final String BEERMAT_LOGO_64 = "beermat64x64.png";

    public static final String BEERMAT_LOGO_24 = "beermat24.png";
    
    public static final String BEERMAT_LOGO_20 = "beermat20.png";

    public final static String PLUGIN_ID = "com.docfacto.beermat.ui";

    public final static String PLUGIN_EDITOR_ID = "com.docfacto.beermat.editor";

    /**
     * Field used for debugging purposes across the plugin
     */
    public static final boolean isDebugging = true;

    /**
     * Holds an instance of itself, singleton.
     */
    private static BeermatUIPlugin thePlugin;

    /**
     * Constructor for the plugin.
     */
    public BeermatUIPlugin() {
    }

    /**
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
     */
    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);
        thePlugin = this;
        DocfactoCorePlugin.registerPlugin("Beermat",Products.BEERMAT);
    }

    /**
     * A method to get the plugin instance.
     * <p>
     * This method returns the instance of the plugin; as this is a singleton
     * there is only one instance.
     * </p>
     * 
     * @return the single instance of this plugin
     * @since 2.1
     */
    public static BeermatUIPlugin getDefault() {
        return thePlugin;
    }

    /**
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#initializeImageRegistry(org.eclipse.jface.resource.ImageRegistry)
     */
    @Override
    protected void initializeImageRegistry(ImageRegistry registry) {
        super.initializeImageRegistry(registry);

        final Bundle bundle = Platform.getBundle(PLUGIN_ID);

        registry.put(BEERMAT_LOGO_64,ImageDescriptor.createFromURL(FileLocator
            .find(bundle,new Path("icons/beermat64x64.png"),null)));

        registry.put(BEERMAT_LOGO_24,ImageDescriptor.createFromURL(FileLocator
            .find(bundle,new Path("icons/links24.png"),null)));
    }

    /**
     * Return a Docfacto image
     * 
     * @param key of the Image
     * @return the cached image, or null if not found
     * @since 2.1
     */
    public static Image getImage(String key) {
        final ImageRegistry imageRegistry = thePlugin.getImageRegistry();
        return imageRegistry.get(key);
    }

    /**
     * Log and exception to the console
     * 
     * @param message to display
     * @param ex cause, can be null
     * @since 1.1
     */
    public static void logException(String message,Throwable ex) {
        thePlugin.getLog().log(
            new Status(Status.ERROR,PLUGIN_ID,Status.ERROR,message,ex));
    }

    /**
     * Log an message.
     * 
     * This will only log a message if debugging is switched on.
     * 
     * @param message to log
     * @since 2.4
     */
    public static void logMessage(String message) {
        if (isDebugging) {
            thePlugin.getLog().log(
                new Status(Status.INFO,PLUGIN_ID,Status.INFO,message,null));
        }
    }
}