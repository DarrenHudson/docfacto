package com.docfacto.beermat.plugin.wizards;

/**
 * The type of wizard
 * 
 * @author kporter - created 14 Jan 2014
 * @since 2.5.1
 */
public enum WizardType {
    IMPORT, NEW, INSERT
}
