package com.docfacto.beermat.plugin.wizards;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;

import com.docfacto.beermat.plugin.BeermatUIPlugin;

/**
 * A wizard page for new SVG files.
 * 
 * Use this page for a default SVG file.
 * 
 * @author kporter - created Apr 2, 2013
 * @since 2.2.3
 */
public class NewSvgWizardPage extends WizardNewFileCreationPage {

    private NewWizardData theData;

    /**
     * Constructor.
     * 
     * @param selection The current resource selection.
     * @param theData
     */
    public NewSvgWizardPage(IStructuredSelection selection,NewWizardData data) {
        super("NewSvgWizardPage",selection);

        setTitle("New SVG");
        setDescription("Creates a new blank SVG");
        setFileExtension("svg");
        setImageDescriptor(ImageDescriptor.createFromImage(BeermatUIPlugin.getImage(BeermatUIPlugin.BEERMAT_LOGO_64)));

        theData = data;
    }

    /**
     * @see org.eclipse.ui.dialogs.WizardNewFileCreationPage#getInitialContents()
     */
    @Override
    protected InputStream getInitialContents() {
        String width = "1000";
        String height = "600";

        if (theData.width!=-1) {
            width = Integer.toString(theData.width);
        }
        if (theData.height!=-1) {
            height = Integer.toString(theData.height);
        }

        StringBuilder template = new StringBuilder();
        template.append("<svg ");
        template.append(" width=\"");
        template.append(width+"\" \n");
        template.append("  height=\"");
        template.append(height+"\" \n");
        
        if(theData.viewbox!=null){
            template.append("viewBox=\""+theData.viewbox+"\" \n");
        }
        
        template.append("  xmlns=\"http://www.w3.org/2000/svg\" \n >"
            +"  </svg>");
        
        return new ByteArrayInputStream(template.toString().getBytes());
    }

}
