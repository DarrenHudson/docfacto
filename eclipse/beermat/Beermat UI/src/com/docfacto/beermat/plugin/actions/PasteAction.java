package com.docfacto.beermat.plugin.actions;

import java.util.ArrayList;

import org.apache.batik.dom.svg.SVGOMGElement;
import org.apache.batik.dom.svg.SVGOMSVGElement;
import org.eclipse.jface.action.Action;
import org.eclipse.ui.texteditor.ITextEditorActionConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.controller.ClipboardManager;
import com.docfacto.beermat.highlight.HighlightManager;
import com.docfacto.beermat.plugin.BeermatEditor;
import com.docfacto.beermat.plugin.BeermatMultiPage;
import com.docfacto.beermat.svg.DragData;
import com.docfacto.beermat.svg.ShapeShifter;
import com.docfacto.beermat.updaters.AbstractUpdater;

/**
 * A paste action
 * 
 * @author kporter - created 22 Jan 2014
 * @since 2.5.1
 */
public class PasteAction extends Action {

    private BeermatMultiPage theEditor;
    
    private ArrayList<Element> pasteElements;
    private HighlightManager hightlightManager;
    private ClipboardManager clipboard;
    private Element root;
    private BeermatController controller;

    /**
     * Constructs a new paste action
     * 
     * @param editor The beermat editor
     */
    public PasteAction(BeermatMultiPage editor) {
        theEditor = editor;
    }

    @Override
    public void run() {
        if (theEditor.getActivePage()==1) {
            // Its the XML Editor, so lets call the Text Editor Print Action
            theEditor.getXMLEditor().getAction(ITextEditorActionConstants.PASTE).run();
            return;
        }
        
        controller = ((BeermatEditor)theEditor.getBeertmatEditor()).getController();
        
        clipboard = controller.getClipboard();
        hightlightManager = controller.getBeermatAWTCanvas().getHighlightManager();
        
        Document dom = controller.getManager().getDocument();

        root = dom.getDocumentElement();
        
        if (hightlightManager.getHighlightedElement()!=null) {
            Element highlighted = hightlightManager.getHighlightedElement().getHighlightedElement();
            if (highlighted instanceof SVGOMGElement||highlighted instanceof SVGOMSVGElement) {
                root = highlighted;
            }
        }

        pasteElements = new ArrayList<Element>();
        
        for (Element element:clipboard.paste()) {
            pasteElements.add((Element)dom.importNode(element,true));
        }

        hightlightManager.removeHighlights();

        if (!clipboard.isCut()) {
            DragData data = new DragData();
            data.setXDiff(10);
            data.setYDiff(10);

            for (Element element:pasteElements) {
                ShapeShifter.updateElement(element,data);
            }

            clipboard.copy(pasteElements);
        }

        new PasteUpdate();

    }

    private class PasteUpdate extends AbstractUpdater {

        public PasteUpdate() {
            super(controller);
            enqueue();
        }

        @Override
        public boolean applyUpdates() {
            for (Element element:pasteElements) {
                root.appendChild(element);
                hightlightManager.select(element,true);
            }
            clipboard.setCut(false);
            return true;
        }

    }
}
