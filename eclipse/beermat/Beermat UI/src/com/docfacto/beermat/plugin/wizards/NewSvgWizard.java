package com.docfacto.beermat.plugin.wizards;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

import com.docfacto.beermat.plugin.BeermatUIPlugin;

/**
 * A wizard to create new SVG Files.
 * 
 * Use this wizard to create SVG Files.
 * 
 * @author kporter - created Apr 4, 2013
 * @since 2.2.3
 * @docfacto.link key="newSVGWizard"
 * uri="${doc-beermat}/general/c_opening_beermat.dita" link-to="doc"
 */
public class NewSvgWizard extends Wizard implements INewWizard {

    /**
     * The ID
     */
    public static String ID = "com.docfacto.beermat.svgwizard";

    private NewSvgWizardPage theMainPage;
    private IWorkbench theWorkbench;
    private NewWizardData theData;
    private NewSvgWizardOptionsPage theOptionsPage;

    /**
     * Constructor.
     * 
     * @since 2.2
     */
    public NewSvgWizard() {
        theData = new NewWizardData();
        BeermatUIPlugin.logMessage("New SVG Wizard");
    }

    /**
     * Return the window title
     * 
     * @return Window Title
     * @since 2.4
     */
    @Override
    public String getWindowTitle() {
        return "Create new Beermat Diagram";
    }

    /**
     * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench,
     * org.eclipse.jface.viewers.IStructuredSelection)
     */
    @Override
    public void init(IWorkbench workbench,IStructuredSelection selection) {
        theWorkbench = workbench;

        BeermatUIPlugin.logMessage("New SVG Wizard init start..");
        
        if (selection.isEmpty()) {
            try {
                IWorkbenchWindow window =
                    PlatformUI.getWorkbench().getActiveWorkbenchWindow();
                IStructuredSelection possibleSelection =
                    (IStructuredSelection)window.getSelectionService()
                        .getSelection("org.eclipse.jdt.ui.PackageExplorer");
                
                if(possibleSelection != null) {
                    selection = possibleSelection;
                }
            }
            catch (Throwable t) {
                BeermatUIPlugin
                    .logException(
                        "NewSvgWizard: unable to get selection from project explorer ",
                        t);
            }
        }

        theMainPage = new NewSvgWizardPage(selection,theData);
        theOptionsPage = new NewSvgWizardOptionsPage(theData);
        addPage(theMainPage);
        addPage(theOptionsPage);
        
        BeermatUIPlugin.logMessage("New SVG Wizard init end..");
    }

    /**
     * @see org.eclipse.jface.wizard.Wizard#performFinish()
     */
    @Override
    public boolean performFinish() {
        IFile file = theMainPage.createNewFile();
        // Here do something so that the editor is opened with this new file?
        boolean result = file!=null;
        if (result) {
            try {
                IDE.openEditor(theWorkbench.getActiveWorkbenchWindow()
                    .getActivePage(),file);
            }
            catch (PartInitException ex) {
                BeermatUIPlugin.logException("Unable to create new Beermat",ex);
            }
        }
        return result;
    }

}
