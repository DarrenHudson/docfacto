package com.docfacto.beermat.plugin.wizards;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbench;

import com.docfacto.xsd2common.XSDArgumentParser;

/**
 * A data object to use with the xsd wizards
 * @author kporter - created 10 Jan 2014
 * @since 2.5.1
 */
public class XsdWizardData {
    public IStructuredSelection selection;
    public IWorkbench workbench;
    public String xsdpath;
    public String outputpath;
    public String rootnode;
    public String namespace;
    public String title;
    public String svgpath;

    /**
     * Using all the data stored in this object, constructs a string array to use as arguments.
     * @return A string array of arguments
     * @since 2.5.1
     */
    public String[] constructArguments() {
        ArrayList<String> argList = new ArrayList<String>(0);

        argList.add(XSDArgumentParser.XSD_OPTION);
        argList.add(xsdpath);

        argList.add(XSDArgumentParser.ROOT_NODE_OPTION);
        argList.add(rootnode);

        if(outputpath!=null){
            String name = new File(xsdpath).getName();
            name = name.substring(0,name.lastIndexOf('.'));
            
            argList.add(XSDArgumentParser.OUTPUT_FILE_OPTION);
            argList.add(outputpath+"/"+name+"-"+rootnode+".svg");            
        }
        else{
            argList.add(XSDArgumentParser.OUTPUT_FILE_OPTION);
            argList.add("null");
        }

        if (namespace!=null&&!namespace.isEmpty()) {
            argList.add(XSDArgumentParser.NAME_SPACE_OPTION);
            argList.add(namespace);
        }
        if (title!=null&&!title.isEmpty()) {
            argList.add(XSDArgumentParser.TITLE_OPTION);
            argList.add(title);
        }

        String[] argArray = (String[])argList.toArray(new String[argList.size()]);

        return argArray;
    }
}
