package com.docfacto.beermat.listeners;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.Point2D;

import org.apache.batik.gvt.GraphicsNode;
import org.eclipse.jface.bindings.Trigger;
import org.eclipse.jface.bindings.TriggerSequence;
import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.IWorkbenchCommandConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.keys.IBindingService;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.events.CanvasMouseMoveEvent;
import com.docfacto.beermat.events.CursorChangeEvent;
import com.docfacto.beermat.events.PaletteBarChangeEvent;
import com.docfacto.beermat.events.SVGElementMouseEvent;
import com.docfacto.beermat.events.SaveEvent;
import com.docfacto.beermat.events.ScrollEvent;
import com.docfacto.beermat.plugin.actions.CopyAction;
import com.docfacto.beermat.plugin.actions.CutAction;
import com.docfacto.beermat.plugin.actions.PasteAction;
import com.docfacto.beermat.plugin.actions.RedoAction;
import com.docfacto.beermat.plugin.actions.UndoAction;
import com.docfacto.beermat.ui.AbstractCursorManager;
import com.docfacto.beermat.ui.AbstractPaletteItemToolbar;
import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.beermat.ui.BeermatScrollPane;
import com.docfacto.beermat.ui.CursorPopupMenu;
import com.docfacto.beermat.ui.toolbar.CursorPaletteItem;
import com.docfacto.beermat.ui.toolbar.PaletteItem;
import com.docfacto.beermat.ui.toolbar.shapes.LineShapeHandler;
import com.docfacto.beermat.ui.toolbar.shapes.PathShapeHandler;
import com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler;
import com.docfacto.beermat.ui.toolbar.shapes.TextShapeHandler;
import com.docfacto.beermat.utils.BeermatUtils;
import com.docfacto.core.utils.SWTUtils;

/**
 * This delegate will delegate all mouse and keyboard events to the correct
 * Input Listener depending on the tools selected
 * 
 * @author dhudson - created 25 Jun 2013
 * @since 2.4
 */
public class BeermatInputListenerDelegate extends
AbstractBeermatAWTInputListener {

    private AbstractBeermatAWTInputListener theSelectedListener;
    private final ShapeToolInputListener theShapeInputListener;
    private final CursorToolInputListener theCursorInputListener;
    private final TextToolInputListener theTextInputListner;
    private final LineToolInputListener theLineToolListener;
    private final PathToolInputListener thePathToolInputListener;

    private final ToolTipListener theToolTipListener;

    private KeyStroke theSaveKeyStroke;

    private GraphicsNode theLastHit;

    private CursorPopupMenu thePopupMenu;

    /**
     * Send Mouse Wheel events to the scroll pane
     */
    private BeermatScrollPane theScrollPane;
    private KeyStroke theUndoKeyStroke;
    private KeyStroke theRedoKeyStroke;
    private KeyStroke theCutKeyStroke;
    private KeyStroke theCopyKeyStroke;
    private KeyStroke thePasteKeyStroke;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param beermatCanvas beermat canvas
     * @since 2.4
     */
    public BeermatInputListenerDelegate(BeermatController controller,
    BeermatAWTCanvas beermatCanvas) {
        super(controller,beermatCanvas);

        theShapeInputListener =
            new ShapeToolInputListener(controller,beermatCanvas);
        theCursorInputListener =
            new CursorToolInputListener(controller,beermatCanvas);
        theTextInputListner =
            new TextToolInputListener(controller,beermatCanvas);
        thePathToolInputListener =
            new PathToolInputListener(controller,beermatCanvas);
        theLineToolListener =
            new LineToolInputListener(controller,beermatCanvas);
        theSelectedListener = theCursorInputListener;
        theToolTipListener = new ToolTipListener(controller,beermatCanvas);

        thePopupMenu = new CursorPopupMenu(controller);

        controller.addListener(this);
    }

    /**
     * Sets the scroll pane so that the scroll pane can handle mouse wheel
     * events
     * 
     * @param scrollPane to set
     * @since 2.4
     */
    public void setScrollPane(BeermatScrollPane scrollPane) {
        theScrollPane = scrollPane;
    }

    /**
     * Get from the Eclipse key bindings the key stroke for save This needs to
     * be done lazily, as binding are not declared when this get constructed
     * 
     * @since 2.4
     */
    private void getKeyStrokes() {
        IBindingService bindingService =
            (IBindingService)PlatformUI.getWorkbench().getAdapter(
                IBindingService.class);

        TriggerSequence sequence =
            bindingService.getBestActiveBindingFor("org.eclipse.ui.file.save");
        Trigger[] trigger = sequence.getTriggers();
        theSaveKeyStroke = (KeyStroke)trigger[0];

        sequence =
            bindingService
                .getBestActiveBindingFor(IWorkbenchCommandConstants.EDIT_UNDO);
        System.out.println(sequence);
        theUndoKeyStroke = (KeyStroke)sequence.getTriggers()[0];

        sequence =
            bindingService
                .getBestActiveBindingFor(IWorkbenchCommandConstants.EDIT_REDO);
        theRedoKeyStroke = (KeyStroke)sequence.getTriggers()[0];

        sequence =
            bindingService
                .getBestActiveBindingFor(IWorkbenchCommandConstants.EDIT_CUT);
        theCutKeyStroke = (KeyStroke)sequence.getTriggers()[0];

        sequence =
            bindingService
                .getBestActiveBindingFor(IWorkbenchCommandConstants.EDIT_COPY);
        theCopyKeyStroke = (KeyStroke)sequence.getTriggers()[0];

        sequence =
            bindingService
                .getBestActiveBindingFor(IWorkbenchCommandConstants.EDIT_PASTE);
        thePasteKeyStroke = (KeyStroke)sequence.getTriggers()[0];
    }

    /**
     * @see com.docfacto.beermat.listeners.BeermatEventListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    @Override
    public void handleEvent(BeermatEvent event) {
        if (event.getAction()==PaletteBarChangeEvent.PALETTEBAR_CHANGE_ACTION) {
            // Handle the swapping of the input listeners
            PaletteBarChangeEvent paletteEvent = (PaletteBarChangeEvent)event;

            PaletteItem item = paletteEvent.getPaletteItem();
            ShapeHandler handler = paletteEvent.getShapeHandler();

            if (thePopupMenu.isVisible()) {
                thePopupMenu.setVisible(false);
            }

            theSelectedListener.swapOut();

            if (item instanceof CursorPaletteItem) {
                theSelectedListener = theCursorInputListener;
            }
            else if (handler instanceof TextShapeHandler) {
                theSelectedListener = theTextInputListner;
            }
            else if (handler instanceof PathShapeHandler) {
                theSelectedListener = thePathToolInputListener;
            }
            else if (handler instanceof LineShapeHandler) {
                theSelectedListener = theLineToolListener;
            }
            else {
                theSelectedListener = theShapeInputListener;
            }

            // Change the cursor
            getController().processEvent(
                new CursorChangeEvent(this,theSelectedListener.getCursor()));

            theSelectedListener.swapIn();

        }

        // Pass the beermat event to the selected listener
        // Event should be passed on regardless of whether the palettebar has
        // changed or not
        theSelectedListener.handleEvent(event);
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseClicked(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseClicked(MouseEvent event) {
        // Show right click popup menu
        if (event.getButton()==3) {
            // Lets see if any key is currently being pressed, if so its not for
            // us
            if (event.isAltDown()||event.isControlDown()||event.isShiftDown()) {
                // Not for us then
                return;
            }

            thePopupMenu.show(getBeermatCanvas(),event.getX(),event.getY());
        }
        else {
            theSelectedListener.mouseClicked(event);
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseDragged(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseDragged(MouseEvent event) {
        getController().processEvent(
            new CanvasMouseMoveEvent(this,event,getController()
                .getViewBoxTransform()));

        BeermatScrollPane scrollPane =
            (BeermatScrollPane)getBeermatCanvas().getParent();
        Dimension viewPort = scrollPane.getViewport();

        if (event.getY()<0) {
            KeyEvent keyEvent =
                new KeyEvent(event.getComponent(),1,System.currentTimeMillis(),
                    0,KeyEvent.VK_KP_UP,' ');
            getController().processEvent(new ScrollEvent(this,keyEvent));
        }

        if (event.getY()>viewPort.height) {
            KeyEvent keyEvent =
                new KeyEvent(event.getComponent(),1,System.currentTimeMillis(),
                    0,KeyEvent.VK_KP_DOWN,' ');
            getController().processEvent(new ScrollEvent(this,keyEvent));
        }

        if (event.getX()<0) {
            KeyEvent keyEvent =
                new KeyEvent(event.getComponent(),1,System.currentTimeMillis(),
                    0,KeyEvent.VK_KP_LEFT,' ');
            getController().processEvent(new ScrollEvent(this,keyEvent));
        }

        if (event.getX()>viewPort.width) {
            KeyEvent keyEvent =
                new KeyEvent(event.getComponent(),1,System.currentTimeMillis(),
                    0,KeyEvent.VK_KP_RIGHT,' ');
            getController().processEvent(new ScrollEvent(this,keyEvent));
        }

        theSelectedListener.mouseDragged(event);
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseEntered(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseEntered(MouseEvent event) {
        theSelectedListener.mouseEntered(event);
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseExited(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseExited(MouseEvent event) {
        theSelectedListener.mouseExited(event);
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseMoved(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseMoved(MouseEvent event) {
        // Send a Mouse Moved Event for the mouse widget
        getController().processEvent(
            new CanvasMouseMoveEvent(this,event,getController()
                .getViewBoxTransform()));

        if (theSelectedListener.requiresMouseElementEvents()) {
            // Lets calculate if its a mouse over, mouse exit event
            processMouseEvents(event);
        }

        // Let the selected listener know
        theSelectedListener.mouseMoved(event);
        theToolTipListener.mouseMoved(event);
    }

    /**
     * Publish mouse out or mouse over events if required.
     * 
     * @param event to process
     * @since 2.4
     */
    private void processMouseEvents(MouseEvent event) {

        Point2D canvasPoint =
            BeermatUtils.getTranformedMousePoint(event,getController()
                .getViewBoxTransform());

        if (canvasPoint==null) {
            return;
        }

        int x = (int)Math.round(canvasPoint.getX());
        int y = (int)Math.round(canvasPoint.getY());

        // Specify a feather edge to make it easier to select
        GraphicsNode node =
            getBeermatCanvas().getGraphicsNodeAt(x,y,
                theSelectedListener.getSelectionFeathering());

        if (theLastHit!=node) {
            // post a MOUSE_OUT event
            if (theLastHit!=null) {

                SVGElementMouseEvent svgMouseEvent =
                    new SVGElementMouseEvent(this,event,
                        SVGElementMouseEvent.EVENT_TYPE.MOUSE_OUT);
                svgMouseEvent.setElement(getController().getBeermatAWTCanvas()
                    .getElementFor(theLastHit));
                svgMouseEvent.setGraphicsNode(node);

                theSelectedListener.mouseOutElement(svgMouseEvent);
                theToolTipListener.mouseOutElement(svgMouseEvent);
            }

            // post a MOUSE_OVER event
            if (node!=null) {
                SVGElementMouseEvent svgMouseEvent =
                    new SVGElementMouseEvent(this,event,
                        SVGElementMouseEvent.EVENT_TYPE.MOUSE_OVER);
                svgMouseEvent.setElement(getController().getBeermatAWTCanvas()
                    .getElementFor(node));
                svgMouseEvent.setGraphicsNode(node);

                theSelectedListener.mouseOverElement(svgMouseEvent);
                theToolTipListener.mouseOverElement(svgMouseEvent);
            }

            theLastHit = node;
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mousePressed(java.awt.event.MouseEvent)
     */
    @Override
    public void mousePressed(MouseEvent event) {
        // Show right click popup menu
        if (event.getButton()==3) {
            // Lets see if any key is currently being pressed, if so its not for
            // us
            if (event.isAltDown()||event.isControlDown()||event.isShiftDown()) {
                // Not for us then
                return;
            }

            thePopupMenu.show(getBeermatCanvas(),event.getX(),event.getY());
        }
        else {
            theSelectedListener.mousePressed(event);
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseReleased(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseReleased(MouseEvent event) {
        theSelectedListener.mouseReleased(event);
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseWheelMoved(java.awt.event.MouseWheelEvent)
     */
    @Override
    public void mouseWheelMoved(MouseWheelEvent event) {
        theSelectedListener.mouseWheelMoved(event);

        if (theScrollPane!=null) {
            theScrollPane.mouseWheelMoved(event);
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#keyPressed(java.awt.event.KeyEvent)
     */
    @Override
    public void keyPressed(KeyEvent event) {
        if (theSaveKeyStroke==null) {
            getKeyStrokes();
        }

        int modifier = event.getModifiers();

        if (modifier==SWTUtils.translateSWTModifiers(theSaveKeyStroke
            .getModifierKeys())) {
            // Process the save key...
            int lower = Character.toLowerCase(theSaveKeyStroke.getNaturalKey());
            if (event.getKeyChar()==lower) {
                getController().processEvent(new SaveEvent(this));
            }
        }

        if (modifier==SWTUtils.translateSWTModifiers(theUndoKeyStroke
            .getModifierKeys())) {
            int lower = Character.toLowerCase(theUndoKeyStroke.getNaturalKey());
            if (event.getKeyChar()==lower) {
                new UndoAction(getController().getMultiPageEditor()).run();
            }
        }

        if (modifier==SWTUtils.translateSWTModifiers(theRedoKeyStroke
            .getModifierKeys())) {
            int lower = Character.toLowerCase(theRedoKeyStroke.getNaturalKey());
            if (event.getKeyChar()==lower) {
                new RedoAction(getController().getMultiPageEditor()).run();
            }
        }

        if (modifier==SWTUtils.translateSWTModifiers(theCopyKeyStroke
            .getModifierKeys())) {
            int lower = Character.toLowerCase(theCopyKeyStroke.getNaturalKey());
            if (event.getKeyChar()==lower) {
                new CopyAction(getController().getMultiPageEditor()).run();
            }
        }

        if (modifier==SWTUtils.translateSWTModifiers(theCutKeyStroke
            .getModifierKeys())) {
            int lower = Character.toLowerCase(theCutKeyStroke.getNaturalKey());
            if (event.getKeyChar()==lower) {
                new CutAction(getController().getMultiPageEditor()).run();
            }
        }

        if (modifier==SWTUtils.translateSWTModifiers(thePasteKeyStroke
            .getModifierKeys())) {
            int lower =
                Character.toLowerCase(thePasteKeyStroke.getNaturalKey());
            if (event.getKeyChar()==lower) {
                new PasteAction(getController().getMultiPageEditor()).run();
            }
        }

        // Is it shortcut key
        final PaletteItem paletteItem =
            getController().getView().getPaletteToolBar().getShortcutKeys()
                .get(new Integer(event.getKeyChar()));

        if (paletteItem!=null) {
            ShapeHandler shapeHandler = null;
            if (paletteItem instanceof ShapeHandler) {
                shapeHandler = (ShapeHandler)paletteItem;
            }

            PaletteBarChangeEvent changeEvent =
                new PaletteBarChangeEvent(this,paletteItem,
                    shapeHandler);

            // This will call back to this class and others
            getController().processEvent(changeEvent);

            SWTUtils.UIThreadExec(new Runnable() {

                /**
                 * @see java.lang.Runnable#run()
                 */
                @Override
                public void run() {
                    ToolItem[] items =
                        getController().getView().getPaletteToolBar()
                            .getToolBar()
                            .getItems();

                    for (ToolItem item:items) {
                        if (item
                            .getData(AbstractPaletteItemToolbar.PALETTE_ITEM_DATA_KEY)==paletteItem) {
                            item.setSelection(true);
                        }
                        else {
                            item.setSelection(false);
                        }
                    }
                }
            });
        }
        else {
            theSelectedListener.keyPressed(event);
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#keyReleased(java.awt.event.KeyEvent)
     */
    @Override
    public void keyReleased(KeyEvent event) {
        // Is it the delete key
        if (event.getKeyCode()==KeyEvent.VK_DELETE||
            event.getKeyCode()==KeyEvent.VK_BACK_SPACE) {
            thePopupMenu.deleteSelectedItems(event.isShiftDown());
            return;
        }

        theSelectedListener.keyReleased(event);
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#keyTyped(java.awt.event.KeyEvent)
     */
    @Override
    public void keyTyped(KeyEvent event) {
        theSelectedListener.keyTyped(event);
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#getCursor()
     */
    @Override
    public int getCursor() {
        return AbstractCursorManager.CROSSHAIR;
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#getSelectionFeathering()
     */
    @Override
    public int getSelectionFeathering() {
        return 10;
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseOverElement(com.docfacto.beermat.events.SVGElementMouseEvent)
     */
    @Override
    public void mouseOverElement(SVGElementMouseEvent event) {
        // As the this is the delegator nothing to do
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseOutElement(com.docfacto.beermat.events.SVGElementMouseEvent)
     */
    @Override
    public void mouseOutElement(SVGElementMouseEvent event) {
        // As the this is the delegator nothing to do
    }
}
