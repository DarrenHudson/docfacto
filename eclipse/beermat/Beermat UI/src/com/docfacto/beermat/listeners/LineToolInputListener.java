package com.docfacto.beermat.listeners;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.List;

import org.apache.batik.dom.svg.SVGOMLineElement;
import org.apache.batik.gvt.GraphicsNode;
import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.events.MarkerChangeEvent;
import com.docfacto.beermat.events.SVGElementMouseEvent;
import com.docfacto.beermat.hotspot.ConnectorHotSpot;
import com.docfacto.beermat.hotspot.HotSpot;
import com.docfacto.beermat.hotspot.NSEWHotSpots;
import com.docfacto.beermat.overlay.ConnectorOverlay;
import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.beermat.svg.ConnectorHandler;
import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.beermat.ui.toolbar.MarkerPaletteItem;
import com.docfacto.beermat.updaters.LinePositionUpdater;
import com.docfacto.beermat.utils.BeermatUtils;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.DocfactoException;

/**
 * Line tool listener, which will handle the connectors
 * 
 * @author dhudson - created 16 Oct 2013
 * @since 2.4
 */
public class LineToolInputListener extends ShapeToolInputListener {

    private ConnectorOverlay theConnectorOverlay;
    private GraphicsNode theLastHit;
    private String theMarkerStartID = MarkerPaletteItem.NONE;
    private String theMarkerEndID = MarkerPaletteItem.NONE;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param beermatCanvas beermat canvas
     * @since 2.4
     */
    public LineToolInputListener(BeermatController controller,
    BeermatAWTCanvas beermatCanvas) {
        super(controller,beermatCanvas);
        theConnectorOverlay = new ConnectorOverlay(controller);
        beermatCanvas.addOverlay(theConnectorOverlay);
    }

    /**
     * Process the highlighting
     * 
     * @param event to process
     * @since 2.4
     */
    private void processHighlighting(SVGElementMouseEvent event) {
        if (event.isMouseOverEvent()) {

            MouseEvent mouseEvent = event.getMouseEvent();

            // Check to see if the mouse in the the bounds of the document
            if (!isMouseOnCanvas(mouseEvent)) {
                // Nothing to do
                return;
            }

            Element hoverElement = event.getElement();

            // Is it a connector, and if so ignore it
            if (ConnectorHandler.isConnector(hoverElement)) {
                return;
            }

            if (hoverElement!=null) {

                if (hoverElement==getElement()) {
                    // Its the element being dragged
                    return;
                }

                // If the alt key not pressed
                if (!mouseEvent.isAltDown()) {
                    hoverElement =
                        SVGUtils.findParentGroup(hoverElement);
                }

                try {
                    theConnectorOverlay.setConnectingNode(new NSEWHotSpots(
                        getBeermatCanvas(),getBeermatCanvas()
                            .getGraphicsNodeFor(hoverElement)));
                }
                catch (DocfactoException ignore) {
                    // Will not happen, as already null checked
                }
            }
        }
        else {
            theConnectorOverlay.setConnectingNode(null);
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.ShapeToolInputListener#mouseReleased(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseReleased(MouseEvent event) {

        HotSpot startHot = theConnectorOverlay.getStartHotSpot();
        HotSpot endHot = theConnectorOverlay.getHotSpot();

        // Note. this will trash start and last points..
        super.mouseReleased(event);

        if (startHot!=null||endHot!=null) {

            // This should be a line element
            Element element = getElement();

            if (element!=null) {
                SVGOMLineElement lineElement = (SVGOMLineElement)element;

                // This is a live element so need to run in the updater ..
                // This will also convert the line to a path so that text can
                // follow it

                new LinePositionUpdater(getController(),lineElement,
                    theConnectorOverlay.getStartNode(),
                    theConnectorOverlay.getEndNode(),startHot,
                    endHot,theConnectorOverlay.getStartPoint(),
                    BeermatUtils.getTranformedMousePoint(event,
                        getBeermatCanvas()
                            .getViewBoxTransform()),this);
            }
        }
        theConnectorOverlay.resetConnectingHotSpots();
    }

    /**
     * @see com.docfacto.beermat.listeners.ShapeToolInputListener#startedDragging()
     */
    @Override
    public void startedDragging(MouseEvent event) {
        theConnectorOverlay.startedDragging(BeermatUtils
            .getTranformedMousePoint(event,getBeermatCanvas()
                .getViewBoxTransform()));
    }

    /**
     * @see com.docfacto.beermat.listeners.ShapeToolInputListener#mouseDragged(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseDragged(MouseEvent event) {
        super.mouseDragged(event);

        processMouseEvents(event);
    }

    /**
     * Publish mouse out or mouse over events if required.
     * 
     * @param event to process
     * @since 2.4
     */
    private void processMouseEvents(MouseEvent event) {

        if (isMouseOnCanvas(event)) {
            theConnectorOverlay
                .updateHotspotPosition(event.getX(),event.getY());
        }

        Point2D canvasPoint =
            BeermatUtils.getTranformedMousePoint(event,getBeermatCanvas()
                .getViewBoxTransform());

        if (canvasPoint==null) {
            return;
        }

        int x = (int)Math.round(canvasPoint.getX());
        int y = (int)Math.round(canvasPoint.getY());

        // Specify a feather edge to make it easier to select
        // Note this gets a list of nodes, as you can have many connectors
        // connecting
        // to a hot spot
        List<GraphicsNode> nodes =
            getBeermatCanvas().getGraphicsNodesAt(
                new Point2D.Double(x,y),
                getSelectionFeathering(),
                getElement());

        GraphicsNode node = null;

        if (nodes.isEmpty()) {
            node = null;
        }
        else if (nodes.size()==1) {
            node = nodes.get(0);
        }
        else {
            for (GraphicsNode hit:nodes) {
                if (ConnectorHandler.isConnector(getBeermatCanvas()
                    .getElementFor(hit))) {
                    // Dont want connectors
                    continue;
                }
                // Stop after the first one as this is in order of painting
                node = hit;
                break;
            }
        }
        if (theLastHit!=node) {
            // post a MOUSE_OUT event
            if (theLastHit!=null) {

                SVGElementMouseEvent svgMouseEvent =
                    new SVGElementMouseEvent(this,event,
                        SVGElementMouseEvent.EVENT_TYPE.MOUSE_OUT);
                svgMouseEvent.setElement(getController().getBeermatAWTCanvas()
                    .getElementFor(theLastHit));
                svgMouseEvent.setGraphicsNode(node);

                mouseOutElement(svgMouseEvent);
            }

            // post a MOUSE_OVER event
            if (node!=null) {
                SVGElementMouseEvent svgMouseEvent =
                    new SVGElementMouseEvent(this,event,
                        SVGElementMouseEvent.EVENT_TYPE.MOUSE_OVER);
                svgMouseEvent.setElement(getController().getBeermatAWTCanvas()
                    .getElementFor(node));
                svgMouseEvent.setGraphicsNode(node);

                mouseOverElement(svgMouseEvent);
            }

            theLastHit = node;
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#keyPressed(java.awt.event.KeyEvent)
     */
    @Override
    public void keyPressed(KeyEvent event) {
        switch (event.getKeyCode()) {
        case KeyEvent.VK_ENTER:
            if (getElement()!=null) {
                GraphicsNode graphicsNode =
                    getBeermatCanvas().getGraphicsNodeFor(getElement());
                if (graphicsNode!=null) {
                    try {
                        ConnectorHotSpot hotSpot =
                            new ConnectorHotSpot(getBeermatCanvas(),
                                graphicsNode);

                        Point location =
                            new Point((int)hotSpot.getHotSpot().getBounds()
                                .getCenterX(),(int)hotSpot.getHotSpot()
                                .getBounds().getCenterY());

                        openTextDialog(location,null,hotSpot.getHotSpot(),
                            getElement());
                    }
                    catch (DocfactoException ex) {
                        BeermatUIPlugin
                            .logException(
                                "LineToolInputListener: Unable to open text dialog",
                                ex);
                    }
                }
            }
            break;
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    @Override
    public void handleEvent(BeermatEvent event) {
        if (event.getSource()==this) {
            // Its the swapIn event..
            return;
        }

        // Listen for marker events so that we can reset them
        if (event.isMarkerChangeEvent()) {

            MarkerChangeEvent markerEvent = (MarkerChangeEvent)event;
            switch (markerEvent.getPosition()) {
            case BOTH:
                theMarkerStartID = markerEvent.getID();
                theMarkerEndID = markerEvent.getID();
                break;
            case START:
                theMarkerStartID = markerEvent.getID();
                break;
            case END:
                theMarkerEndID = markerEvent.getID();
                break;
            }
        }
    }
    
    /**
     * @see com.docfacto.beermat.listeners.ShapeToolInputListener#mouseClicked(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseClicked(MouseEvent event) {
        // Don't want this behaviour here
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#swapIn()
     */
    @Override
    public void swapIn() {
        // Reset the markers to what we had ..
        getController().processEvent(
            new MarkerChangeEvent(this,MarkerChangeEvent.Position.START,
                theMarkerStartID));
        getController().processEvent(
            new MarkerChangeEvent(this,MarkerChangeEvent.Position.END,
                theMarkerEndID));
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#swapOut()
     */
    @Override
    public void swapOut() {
        theConnectorOverlay.resetConnectingHotSpots();
        theConnectorOverlay.setConnectingNode(null);
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseMoved(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseMoved(MouseEvent event) {
        processMouseEvents(event);
    }

    /**
     * @see com.docfacto.beermat.listeners.ShapeToolInputListener#mouseOverElement(com.docfacto.beermat.events.SVGElementMouseEvent)
     */
    @Override
    public void mouseOverElement(SVGElementMouseEvent event) {
        processHighlighting(event);
    }

    /**
     * @see com.docfacto.beermat.listeners.ShapeToolInputListener#mouseOutElement(com.docfacto.beermat.events.SVGElementMouseEvent)
     */
    @Override
    public void mouseOutElement(SVGElementMouseEvent event) {
        processHighlighting(event);
    }

    /**
     * @see com.docfacto.beermat.listeners.ShapeToolInputListener#getSelectionFeathering()
     */
    @Override
    public int getSelectionFeathering() {
        return 20;
    }

}
