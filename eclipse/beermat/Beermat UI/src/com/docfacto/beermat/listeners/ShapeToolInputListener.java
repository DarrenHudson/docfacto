package com.docfacto.beermat.listeners;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.batik.gvt.GraphicsNode;
import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.SVGElementMouseEvent;
import com.docfacto.beermat.hotspot.CentreHotSpot;
import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.beermat.ui.AbstractCursorManager;
import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler;
import com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData;
import com.docfacto.beermat.updaters.SetAttributesUpdater;
import com.docfacto.beermat.updaters.ShapeClickCreateUpdater;
import com.docfacto.beermat.updaters.ShapeUpdater;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.DocfactoException;
import com.docfacto.common.NameValuePair;

/**
 * Input Listener (Key and Mouse) for the shape tools
 * 
 * {@docfacto.system please note that this implementation is over-ridden }
 * 
 * @author dhudson - created 25 Jun 2013
 * @since 2.4
 */
public class ShapeToolInputListener extends AbstractBeermatAWTInputListener {

    private Element theElement;
    private Point theStart;
    private Point theLast;
    
    // Remember the size for different shape handlers
    private HashMap<ShapeHandler,Element> theLastElements;

    private enum Dragging {
        DRAGGING, STOPPED;
    }

    private Dragging dragged;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param beermatCanvas beermat canvas
     * @since 2.4
     */
    public ShapeToolInputListener(BeermatController controller,
    BeermatAWTCanvas beermatCanvas) {
        super(controller,beermatCanvas);
        dragged = Dragging.STOPPED;
        theLastElements = new HashMap<ShapeHandler,Element>();
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseDragged(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseDragged(MouseEvent event) {
        final ShapeHandler handler =
            getController().getView().getShapeHandler();
        if (handler==null) {
            return;// Nothing to do
        }

        final ShapeUpdateData updateData =
            new ShapeUpdateData(getController().getViewBoxTransform());

        if (event.isShiftDown()) {
            updateData.setShiftDown(true);
        }
        if (dragged==Dragging.STOPPED) {
            dragged = Dragging.DRAGGING;
            theStart = new Point(event.getX(),event.getY());
            theLast = new Point(event.getX(),event.getY());
            theElement = null;
            startedDragging(event);
        }

        if (dragged==Dragging.DRAGGING) {
            // MUST be calculated before theLast is re-set.
            final Point diff =
                new Point(event.getX()-theLast.x,event.getY()-theLast.y);
            theLast = new Point(event.getX(),event.getY());

            updateData.setStart(theStart);
            updateData.setLast(theLast);
            updateData.setDiff(diff);
            updateData.setStartToEndDiff(new Point((theLast.x-theStart.x),
                (theLast.y-theStart.y)));

            if (theElement==null) {
                if (diff.x!=0||diff.y!=0) {
                    theElement =
                        handler.createShape(getController().getManager()
                            .getDocument(),updateData.getStart());
                    ArrayList<NameValuePair> attrs =
                        getController().getView().getControlToolBar()
                            .getElementAttributes(theElement);
                    new SetAttributesUpdater(getController(),attrs,theElement,
                        false);
                }
            }
            else {
                // Updates always need to be done in the update manager thread
                new ShapeUpdater(getController(),theElement,handler,updateData);
            }
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#keyPressed(java.awt.event.KeyEvent)
     */
    @Override
    public void keyPressed(KeyEvent event) {
        switch (event.getKeyCode()) {
        case KeyEvent.VK_ENTER:
            if (theElement!=null) {
                GraphicsNode graphicsNode =
                    getBeermatCanvas().getGraphicsNodeFor(theElement);
                if (graphicsNode!=null) {
                    try {
                        CentreHotSpot centreHotSpot =
                            new CentreHotSpot(getBeermatCanvas(),theElement);
                        Point location =
                            new Point((int)centreHotSpot.getHotSpot()
                                .getBounds().getCenterX(),(int)centreHotSpot
                                .getHotSpot().getBounds().getCenterY());

                        openTextDialog(location,null,
                            centreHotSpot.getHotSpot(),theElement);
                    }
                    catch (DocfactoException ex) {
                        BeermatUIPlugin.logException(
                            "ShapeToolListener: Unable to open text dialog",ex);
                    }
                }
            }
        }
    }

    /**
     * The creating / created element
     * 
     * @return the element being created
     * @since 2.4
     */
    public Element getElement() {
        return theElement;
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseClicked(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseClicked(MouseEvent event) {
        // Only do something if the mouse is on the canvas
        if (!isMouseOnCanvas(event)) {
            // Nothing to do
            return;
        }

        final ShapeHandler handler =
            getController().getView().getShapeHandler();

        if (handler!=null) {
            Point2D transformedPoint =
                SVGUtils.getTransformedPoint(event.getX(),event.getY(),
                    getController().getViewBoxTransform());
            Point loc =
                new Point((int)transformedPoint.getX(),
                    (int)transformedPoint.getY());

            // Save the state before drawing a shape.
            getController().getUndoManager().saveCurrentState(getController());

            Element lastElement = theLastElements.get(handler);

            // Don't copy anything from the last element
            if (event.isShiftDown()) {
                lastElement = null;
            }

            new ShapeClickCreateUpdater(getController(),handler,loc,
                lastElement,this);
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseReleased(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseReleased(MouseEvent event) {
        if (dragged==Dragging.DRAGGING) {
            dragged = Dragging.STOPPED;
            if (theElement!=null) {
                ShapeHandler handler =
                    getController().getView().getShapeHandler();
                theLastElements.put(handler,theElement);
            }

            theStart = null;
            theLast = null;

        }
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#getCursor()
     */
    @Override
    public int getCursor() {
        return AbstractCursorManager.CROSSHAIR;
    }

    /**
     * The dragging start point.s
     * 
     * @return the point of where the user started dragging
     * @since 2.4
     */
    public Point getStartDraggingPoint() {
        return theStart;
    }

    /**
     * The dragging end point.
     * 
     * @return the point of where the user finished dragging
     * @since 2.4
     */
    public Point getLastDraggingPoint() {
        return theLast;
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#getSelectionFeathering()
     */
    @Override
    public int getSelectionFeathering() {
        return 10;
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#requiresMouseElementEvents()
     */
    @Override
    public boolean requiresMouseElementEvents() {
        return false;
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseOverElement(com.docfacto.beermat.events.SVGElementMouseEvent)
     */
    @Override
    public void mouseOverElement(SVGElementMouseEvent event) {
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseOutElement(com.docfacto.beermat.events.SVGElementMouseEvent)
     */
    @Override
    public void mouseOutElement(SVGElementMouseEvent event) {
    }

    /**
     * This method is called when dragging has started.
     * 
     * {@docfacto.note Implementations should override}
     * 
     * @param event mouse event of when the dragging started
     * 
     * @since 2.4
     */
    public void startedDragging(MouseEvent event) {
    }

    /**
     * Called via the updater for the newly created element.
     * 
     * @param element newly created element
     * @since 2.5
     */
    public void setElement(Element element) {
        theElement = element;
    }
}
