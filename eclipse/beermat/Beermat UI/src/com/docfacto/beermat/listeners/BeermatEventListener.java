package com.docfacto.beermat.listeners;

import com.docfacto.beermat.events.BeermatEvent;


/**
 * Listeners to Beermat events will need to implement this interface
 *
 * @author dhudson - created 20 Jun 2013
 * @since 2.4
 */
public interface BeermatEventListener {

    /**
     * handle the event if required
     *
     * @param event to process
     * @since 2.4
     */
    public void handleEvent(BeermatEvent event);
}
