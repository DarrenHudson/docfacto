package com.docfacto.beermat.listeners;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.ui.AbstractCursorManager;
import com.docfacto.beermat.ui.BeermatAWTCanvas;

/**
 * This class is used to provide a different cursor to normal shapes.
 * 
 * @author dhudson - created 16 Oct 2013
 * @since 2.4
 */
public class PathToolInputListener extends ShapeToolInputListener {

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param beermatCanvas canvas
     * 
     * @since 2.4
     */
    public PathToolInputListener(BeermatController controller,
    BeermatAWTCanvas beermatCanvas) {
        super(controller,beermatCanvas);
    }

    /**
     * @see com.docfacto.beermat.listeners.ShapeToolInputListener#getCursor()
     */
    @Override
    public int getCursor() {
        return AbstractCursorManager.PENCIL;
    }
}
