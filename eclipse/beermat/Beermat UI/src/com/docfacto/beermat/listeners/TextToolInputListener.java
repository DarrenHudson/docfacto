package com.docfacto.beermat.listeners;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.events.CursorChangeEvent;
import com.docfacto.beermat.events.SVGElementMouseEvent;
import com.docfacto.beermat.overlay.TextOverlay;
import com.docfacto.beermat.ui.AbstractCursorManager;
import com.docfacto.beermat.ui.BeermatAWTCanvas;

/**
 * Special processing for the text tool
 * 
 * @author dhudson - created 9 Aug 2013
 * @since 2.4
 */
public class TextToolInputListener extends AbstractBeermatAWTInputListener {

    private Element theCurrentElement;
    private MouseEvent theLastMouseEvent;
    private TextOverlay theTextOverlay;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param beermatCanvas beermat canvas
     * @since 2.4
     */
    public TextToolInputListener(BeermatController controller,
    BeermatAWTCanvas beermatCanvas) {
        super(controller,beermatCanvas);
        theTextOverlay = new TextOverlay(beermatCanvas);
        beermatCanvas.addOverlay(theTextOverlay);
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#keyPressed(java.awt.event.KeyEvent)
     */
    @Override
    public void keyPressed(KeyEvent event) {
        switch (event.getKeyCode()) {
        case KeyEvent.VK_ENTER:
            // Shortcut key to present a text entry dialog
            Point location =
                new Point(theLastMouseEvent.getX(),theLastMouseEvent.getY());

            openTextDialog(location,theCurrentElement,
                theTextOverlay.getHotSpot(),theTextOverlay.getTextNodeElement());
            break;
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseMoved(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseMoved(MouseEvent event) {
        theLastMouseEvent = event;
        if (isMouseOnCanvas(event)) {
            theTextOverlay.updateHotspotPosition(event.getX(),event.getY());
            if (isText(theCurrentElement)) {
                getController().processEvent(
                    new CursorChangeEvent(this,AbstractCursorManager.TEXT));
            }
            else {
                getController().processEvent(
                    new CursorChangeEvent(this,AbstractCursorManager.ADD_TEXT));
            }
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseClicked(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseClicked(final MouseEvent event) {

        // Do we have anything to do
        if (!isMouseOnCanvas(event)) {
            return;
        }

        if(event.isShiftDown()) {
            // User focus request
            return;
        }
        
        Element editable = null;

        if (isText(event)) {
            editable = theCurrentElement;
        }

        Point location = new Point(event.getX(),event.getY());
        openTextDialog(location,editable,theTextOverlay.getHotSpot(),
            theTextOverlay.getTextNodeElement());
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    public void handleEvent(BeermatEvent event) {
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#getCursor()
     */
    @Override
    public int getCursor() {
        return AbstractCursorManager.ADD_TEXT;
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#getSelectionFeathering()
     */
    @Override
    public int getSelectionFeathering() {
        return 40;
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#swapOut()
     */
    @Override
    public void swapOut() {
        super.swapOut();
        theTextOverlay.setHoverElement(null);
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseOverElement(com.docfacto.beermat.events.SVGElementMouseEvent)
     */
    @Override
    public void mouseOverElement(SVGElementMouseEvent event) {
        theCurrentElement = event.getElement();
        if (!isText(theCurrentElement)) {
            theTextOverlay.setHoverElement(theCurrentElement);
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseOutElement(com.docfacto.beermat.events.SVGElementMouseEvent)
     */
    @Override
    public void mouseOutElement(SVGElementMouseEvent event) {
        theCurrentElement = null;
        theTextOverlay.setHoverElement(theCurrentElement);
    }
}
