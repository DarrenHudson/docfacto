package com.docfacto.beermat.listeners;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.Rectangle2D;

import org.apache.batik.bridge.UpdateManagerEvent;
import org.apache.batik.bridge.UpdateManagerListener;
import org.apache.batik.swing.gvt.GVTTreeRendererEvent;
import org.apache.batik.swing.gvt.GVTTreeRendererListener;
import org.apache.batik.swing.gvt.JGVTComponentListener;
import org.apache.batik.swing.svg.GVTTreeBuilderEvent;
import org.apache.batik.swing.svg.GVTTreeBuilderListener;

import com.docfacto.beermat.ui.BeermatScrollPane;

/**
 * Class to handle the scroll bar interactions with document changes
 *
 * @author dhudson - created 7 Aug 2013
 * @since 2.4
 */
public class ScrollListener extends ComponentAdapter
implements JGVTComponentListener,GVTTreeBuilderListener,
GVTTreeRendererListener,UpdateManagerListener
{
    private boolean isReady = false;
    private final BeermatScrollPane theScrollPane;
    
    /**
     * Constructor.
     * @param scrollPane beermat scrollpane
     * @since 2.4
     */
    public ScrollListener(BeermatScrollPane scrollPane) {
        theScrollPane = scrollPane;
    }
    
    /**
     * @see org.apache.batik.swing.gvt.JGVTComponentListener#componentTransformChanged(java.awt.event.ComponentEvent)
     */
    public void componentTransformChanged(ComponentEvent evt)
    {
        if (isReady) {
            theScrollPane.resizeScrollBars();
        }
    }

    /**
     * @see java.awt.event.ComponentAdapter#componentResized(java.awt.event.ComponentEvent)
     */
    public void componentResized(ComponentEvent evt)
    {
        if (isReady) {
            theScrollPane.resizeScrollBars();
        }
    }

    /**
     * @see org.apache.batik.swing.svg.GVTTreeBuilderListener#gvtBuildStarted(org.apache.batik.swing.svg.GVTTreeBuilderEvent)
     */
    public void gvtBuildStarted(GVTTreeBuilderEvent e) {
        isReady = false;
        // Start by assuming we won't need them.
       theScrollPane.updateScrollbarState(false,false);
    }

    /**
     * @see org.apache.batik.swing.svg.GVTTreeBuilderListener#gvtBuildCompleted(org.apache.batik.swing.svg.GVTTreeBuilderEvent)
     */
    public void gvtBuildCompleted(GVTTreeBuilderEvent e)
    {
        isReady = true;
        // new document forget old viewBox if any.
        theScrollPane.resetViewbox();
    }

    /**
     * @see org.apache.batik.swing.gvt.GVTTreeRendererListener#gvtRenderingCompleted(org.apache.batik.swing.gvt.GVTTreeRendererEvent)
     */
    public void gvtRenderingCompleted(GVTTreeRendererEvent e) {
        if (theScrollPane.getViewbox()==null) {
            theScrollPane.resizeScrollBars();
            return;
        }

        Rectangle2D newview = theScrollPane.getViewBoxRect();
        Rectangle2D viewBox = theScrollPane.getViewbox();
        
        if ((newview.getX()!=viewBox.getX())||
            (newview.getY()!=viewBox.getY())||
            (newview.getWidth()!=viewBox.getWidth())||
            (newview.getHeight()!=viewBox.getHeight())) {
            theScrollPane.setViewbox(newview);
            theScrollPane.resizeScrollBars();
        }
    }

    /**
     * @see org.apache.batik.bridge.UpdateManagerListener#updateCompleted(org.apache.batik.bridge.UpdateManagerEvent)
     */
    public void updateCompleted(UpdateManagerEvent e) {
        if (theScrollPane.getViewbox()==null) {
            theScrollPane.resizeScrollBars();
            return;
        }
        
        Rectangle2D viewBox = theScrollPane.getViewbox();
        Rectangle2D newview =theScrollPane.getViewBoxRect();
        if ((newview.getX()!=viewBox.getX())||
            (newview.getY()!=viewBox.getY())||
            (newview.getWidth()!=viewBox.getWidth())||
            (newview.getHeight()!=viewBox.getHeight())) {
            theScrollPane.setViewbox(newview);
            theScrollPane.resizeScrollBars();
        }
    }

    /**
     * @see org.apache.batik.swing.svg.GVTTreeBuilderListener#gvtBuildCancelled(org.apache.batik.swing.svg.GVTTreeBuilderEvent)
     */
    public void gvtBuildCancelled(GVTTreeBuilderEvent e) {
    }

    /**
     * @see org.apache.batik.swing.svg.GVTTreeBuilderListener#gvtBuildFailed(org.apache.batik.swing.svg.GVTTreeBuilderEvent)
     */
    public void gvtBuildFailed(GVTTreeBuilderEvent e) {
    }

    /**
     * @see org.apache.batik.swing.gvt.GVTTreeRendererListener#gvtRenderingPrepare(org.apache.batik.swing.gvt.GVTTreeRendererEvent)
     */
    public void gvtRenderingPrepare(GVTTreeRendererEvent e) {
    }

    /**
     * @see org.apache.batik.swing.gvt.GVTTreeRendererListener#gvtRenderingStarted(org.apache.batik.swing.gvt.GVTTreeRendererEvent)
     */
    public void gvtRenderingStarted(GVTTreeRendererEvent e) {
    }

    /**
     * @see org.apache.batik.swing.gvt.GVTTreeRendererListener#gvtRenderingCancelled(org.apache.batik.swing.gvt.GVTTreeRendererEvent)
     */
    public void gvtRenderingCancelled(GVTTreeRendererEvent e) {
    }

    /**
     * @see org.apache.batik.swing.gvt.GVTTreeRendererListener#gvtRenderingFailed(org.apache.batik.swing.gvt.GVTTreeRendererEvent)
     */
    public void gvtRenderingFailed(GVTTreeRendererEvent e) {
    }

    /**
     * @see org.apache.batik.bridge.UpdateManagerListener#managerStarted(org.apache.batik.bridge.UpdateManagerEvent)
     */
    public void managerStarted(UpdateManagerEvent e) {
    }

    /**
     * @see org.apache.batik.bridge.UpdateManagerListener#managerSuspended(org.apache.batik.bridge.UpdateManagerEvent)
     */
    public void managerSuspended(UpdateManagerEvent e) {
    }

    /**
     * @see org.apache.batik.bridge.UpdateManagerListener#managerResumed(org.apache.batik.bridge.UpdateManagerEvent)
     */
    public void managerResumed(UpdateManagerEvent e) {
    }

    /**
     * @see org.apache.batik.bridge.UpdateManagerListener#managerStopped(org.apache.batik.bridge.UpdateManagerEvent)
     */
    public void managerStopped(UpdateManagerEvent e) {
    }

    /**
     * @see org.apache.batik.bridge.UpdateManagerListener#updateStarted(org.apache.batik.bridge.UpdateManagerEvent)
     */
    public void updateStarted(UpdateManagerEvent e) {
    }

    /**
     * @see org.apache.batik.bridge.UpdateManagerListener#updateFailed(org.apache.batik.bridge.UpdateManagerEvent)
     */
    public void updateFailed(UpdateManagerEvent e) {
    }
}
