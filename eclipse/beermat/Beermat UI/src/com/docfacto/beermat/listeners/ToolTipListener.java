package com.docfacto.beermat.listeners;

import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import javax.swing.ToolTipManager;

import org.apache.batik.util.SVGConstants;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.SVGElementMouseEvent;
import com.docfacto.beermat.svg.ConnectorHandler;
import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.common.StringUtils;

/**
 * A Listener which handles tooltips.
 * 
 * {@docfacto.note Tooltips are displayed if mouse over elements have
 * description or title child elements}
 * 
 * @author dhudson - created 12 Dec 2013
 * @since 2.5
 */
public class ToolTipListener extends AbstractBeermatAWTInputListener {

    private final ToolTipManager theToolTipManager;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param beermatCanvas beermat canvas
     */
    public ToolTipListener(BeermatController controller,
    BeermatAWTCanvas beermatCanvas) {
        super(controller,beermatCanvas);
        theToolTipManager = ToolTipManager.sharedInstance();
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#getCursor()
     */
    @Override
    public int getCursor() {
        return 0;
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#getSelectionFeathering()
     */
    @Override
    public int getSelectionFeathering() {
        return 0;
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseMoved(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseMoved(MouseEvent event) {
        theToolTipManager.mouseMoved(event);
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseOverElement(com.docfacto.beermat.events.SVGElementMouseEvent)
     */
    @Override
    public void mouseOverElement(final SVGElementMouseEvent event) {
        processToolTip(event.getElement());
        theToolTipManager.mouseEntered(event.getMouseEvent());
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseOutElement(com.docfacto.beermat.events.SVGElementMouseEvent)
     */
    @Override
    public void mouseOutElement(SVGElementMouseEvent event) {
        getBeermatCanvas().setToolTipText(null);
        theToolTipManager.mouseExited(event.getMouseEvent());
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseClicked(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseClicked(MouseEvent event) {
        theToolTipManager.mouseClicked(event);
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mousePressed(java.awt.event.MouseEvent)
     */
    @Override
    public void mousePressed(MouseEvent event) {
        theToolTipManager.mousePressed(event);
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseReleased(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseReleased(MouseEvent event) {
        theToolTipManager.mouseReleased(event);
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseWheelMoved(java.awt.event.MouseWheelEvent)
     */
    @Override
    public void mouseWheelMoved(MouseWheelEvent event) {
        theToolTipManager.mouseWheelMoved(event);
    }

    /**
     * Check to see if there is a title element, and if so, set the tooltip
     * 
     * @param element to process
     * @since 2.5
     */
    private void processToolTip(Element element) {
        if (ConnectorHandler.isConnector(element)) {
            getBeermatCanvas().setToolTipText(formatConnectorToolTip(element));
            return;
        }

        String tip = getTitleFromElement(element);

        if (tip!=null) {
            tip = formatToolTip(tip);
        }

        getBeermatCanvas().setToolTipText(tip);
    }

    /**
     * Use the id's of the start and end connecting elements to build a tooltip.
     * 
     * @param connector to process
     * @return A Tooltip string that represents a connector
     * @since 2.5
     */
    private String formatConnectorToolTip(Element connector) {
        StringBuilder builder = new StringBuilder(100);
        builder.append("<html><b>Connector<b> :<p><i>");

        builder
            .append(getTextForNode(ConnectorHandler.getStartNode(connector)));
        builder.append(" - ");
        builder.append(getTextForNode(ConnectorHandler.getEndNode(connector)));

        builder.append("</i></p></html>");

        return builder.toString();
    }

    /**
     * Search the node for a title element, failing that use the id
     * 
     * @param node to process
     * @return title or id for the connector
     * @since 2.5
     */
    private String getTextForNode(Element node) {

        if (node!=null) {
            String tip = getTitleFromElement(node);
            if (tip!=null) {
                return StringUtils.escapeXML(tip);
            }
            return node.getAttribute("id");
        }

        return "";
    }

    /**
     * Wrap text in html if too long
     * 
     * @param text to process
     * @return HTML formatted text if the text is too long
     * @since 2.5
     */
    private String formatToolTip(String text) {
        if (text.length()<40) {
            return text;
        }

        StringBuilder builder = new StringBuilder(100);
        builder.append("<html><p width=\"100px\">");
        builder.append(text);
        builder.append("</p></html>");

        return builder.toString();
    }

    /**
     * Return the contents of the title element if present.
     * 
     * @param element to process
     * @return the contents of the title element or null
     * @since 2.5
     */
    private String getTitleFromElement(Element element) {
        if (element.hasChildNodes()) {
            NodeList children = element.getChildNodes();
            for (int i = 0;i<children.getLength();i++) {
                Node node = children.item(i);
                if (node.getNodeType()==Node.ELEMENT_NODE&&
                    SVGConstants.SVG_TITLE_TAG.equals(node.getLocalName())) {
                    node.normalize();
                    return StringUtils.escapeXML(node
                        .getFirstChild()
                        .getNodeValue());
                }
            }
        }

        return null;
    }
}
