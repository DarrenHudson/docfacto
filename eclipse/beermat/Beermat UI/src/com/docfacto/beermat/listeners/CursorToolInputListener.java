package com.docfacto.beermat.listeners;

import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.svg.SVGDocument;
import org.w3c.dom.svg.SVGRect;
import org.w3c.dom.svg.SVGSVGElement;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.events.CursorChangeEvent;
import com.docfacto.beermat.events.SVGElementMouseEvent;
import com.docfacto.beermat.events.ScrollEvent;
import com.docfacto.beermat.highlight.HighlightManager;
import com.docfacto.beermat.hotspot.CentreHotSpot;
import com.docfacto.beermat.hotspot.HotSpot;
import com.docfacto.beermat.overlay.SelectingOverlay;
import com.docfacto.beermat.svg.DragData;
import com.docfacto.beermat.ui.AbstractCursorManager;
import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.beermat.updaters.DragDataUpdater;
import com.docfacto.beermat.utils.BeermatUtils;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.DocfactoException;

/**
 * This tool handles the mouse and keyboard events if the Cursor (Select) tool
 * is selected
 * 
 * @author dhudson - created 25 Jun 2013
 * @since 2.4
 */
public class CursorToolInputListener extends AbstractBeermatAWTInputListener {

    private Point theStart;

    private final HighlightManager theHighlightManager;

    private Element theCurrentElement;

    private boolean isDragging = false;

    private HotSpot theHotSpot;

    private DragData theDragData;

    private ArrayList<Element> theElements;

    private int theLastCursor;

    private enum State {
        /**
         * We are in highlight, element dragging mode
         */
        ELEMENT_DRAGGING,
        /**
         * We are in selecting elements mode
         */
        SELECTING_ELEMENTS
    }

    private State theState = State.ELEMENT_DRAGGING;

    private SelectingOverlay theSelectingOverlay;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param beermatCanvas beermat canvas
     * @since 2.4
     */
    public CursorToolInputListener(BeermatController controller,
    BeermatAWTCanvas beermatCanvas) {
        super(controller,beermatCanvas);

        theHighlightManager = beermatCanvas.getHighlightManager();

        theSelectingOverlay = new SelectingOverlay(controller);
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#keyPressed(java.awt.event.KeyEvent)
     */
    @Override
    public void keyPressed(KeyEvent event) {
        switch (event.getKeyCode()) {
        case KeyEvent.VK_ESCAPE:
            theHighlightManager.removeHighlights();
            break;

        case KeyEvent.VK_KP_RIGHT:
        case KeyEvent.VK_RIGHT:
            if (theHighlightManager.hasSelectedElements()) {
                updateElements(1,0);
            }
            else {
                getController().processEvent(new ScrollEvent(this,event));
            }
            break;

        case KeyEvent.VK_LEFT:
        case KeyEvent.VK_KP_LEFT:
            if (theHighlightManager.hasSelectedElements()) {
                updateElements(-1,0);
            }
            else {
                getController().processEvent(new ScrollEvent(this,event));
            }
            break;

        case KeyEvent.VK_UP:
        case KeyEvent.VK_KP_UP:
            if (theHighlightManager.hasSelectedElements()) {
                updateElements(0,-1);
            }
            else {
                getController().processEvent(new ScrollEvent(this,event));
            }
            break;

        case KeyEvent.VK_DOWN:
        case KeyEvent.VK_KP_DOWN:
            if (theHighlightManager.hasSelectedElements()) {
                updateElements(0,1);
            }
            else {
                getController().processEvent(new ScrollEvent(this,event));
            }

            break;
        }
    }

    /**
     * Move the selected shapes in a direction.
     * 
     * Params are plus for a positive direction, minus for a negative direction,
     * 0 for no change
     * 
     * @param x to change
     * @param y to change
     * @since 2.4
     */
    private void updateElements(final int x,final int y) {
        DragData data = new DragData();
        data.setXDiff(x);
        data.setYDiff(y);
        getController().getUndoManager().saveCurrentState(getController());
        new DragDataUpdater(getController(),data);
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseClicked(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseClicked(final MouseEvent event) {
        // Handle Double click
        if (event.getClickCount()==2) {
            if (theCurrentElement!=null) {
                Point2D canvasPoint =
                    BeermatUtils.getTranformedMousePoint(event,
                        getBeermatCanvas().getViewBoxTransform());

                if (canvasPoint==null) {
                    // Nothing we can do here ..
                    return;
                }

                Point location = new Point(event.getX(),event.getY());

                if (isText(event)) {
                    // Open the text editor in edit mode
                    openTextDialog(
                        location,
                        getBeermatCanvas().getElementAt(
                            (int)canvasPoint.getX(),(int)canvasPoint.getY()),
                        null,null);
                }
                else {
                    try {
                        // Let them add text at the centre of the item
                        CentreHotSpot centre =
                            new CentreHotSpot(getBeermatCanvas(),
                                theCurrentElement);
                        openTextDialog(location,null,centre.getHotSpot(),
                            theCurrentElement);
                    }
                    catch (DocfactoException ex) {
                        // Can't create a hot spot, but allow for text entry
                        openTextDialog(
                            location,
                            getBeermatCanvas()
                                .getElementAt((int)canvasPoint.getX(),
                                    (int)canvasPoint.getY()),null,null);
                    }
                }
            }

            return;
        }

        // Select the current element
        if (theCurrentElement!=null) {
            theHighlightManager.select(theCurrentElement,event.isShiftDown()||
                event.isAltDown());
        }
        else {
            theHighlightManager.removeHighlights();
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseDragged(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseDragged(MouseEvent event) {
        if (theStart==null) {
            theStart = new Point(event.getX(),event.getY());
        }
        if (event.isMetaDown()) {
            // Its not for us..
            return;
        }

        if (!isDragging) {
            // We are here for the first time so lets work out the state..
            if (theCurrentElement==null&&
                theHighlightManager.checkForHotSpot(event.getX(),event.getY())==null) {
                // Nothing is selected, so we must be selecting
                theState = State.SELECTING_ELEMENTS;
                if (!event.isShiftDown()) {
                    theHighlightManager.removeHighlights();
                }
            }
            else {
                theState = State.ELEMENT_DRAGGING;
                theElements = new ArrayList<Element>();
                if (theHighlightManager.getHighlightedElements().size()==1) {
                    Element e =
                        theHighlightManager.getHighlightedElements().get(0);
                    List<Element> elements =
                        SVGUtils.getSVGElements(getController().getManager()
                            .getDocument());
                    for (Element el:elements) {
                        if (!el.equals(e)) {
                            theElements.add(el);
                        }
                    }
                }
            }
        }

        // Process dragging depending on the state
        switch (theState) {
        case SELECTING_ELEMENTS:
            processSelectingDragging(event);
            break;
        case ELEMENT_DRAGGING:
            processElementDragging(event);
            break;
        }

        isDragging = true;
    }

    private void processSelectingDragging(MouseEvent event) {
        if (!isDragging) {
            // First time we are here ..
            theSelectingOverlay.setStartingPoint(theStart);
            if (event.isAltDown()) {
                theSelectingOverlay.setRequiringIntersect();
            }
            getBeermatCanvas().addOverlay(theSelectingOverlay);
        }

        theSelectingOverlay.setNewPoint(event);
    }

    private void processElementDragging(MouseEvent event) {
        AffineTransform transform = getController().getViewBoxTransform();

        // TODO: Sort out rounding
        final Point2D.Float diff =
            new Point2D.Float(
                (float)((event.getX()-theStart.x)/transform.getScaleX()),
                (float)((event.getY()-theStart.y)/transform.getScaleY()));

        if (!isDragging) {
            theHotSpot =
                theHighlightManager.checkForHotSpot(event.getX(),event.getY());

            if (theHotSpot==null) {
                // We are here for the first time
                theHighlightManager.startDragging(theCurrentElement);
            }
            else {
                theHighlightManager.clearHighlighted();
                theHighlightManager.startDragging(null);
            }

            theDragData =
                new DragData(diff.x,diff.y,theHotSpot,transform,getController()
                    .getBeermatAWTCanvas(),
                    getController().getManager().getDocument());

            // save the state before the drag
            getController().getUndoManager().saveCurrentState(getController());

        }

        if (event.isShiftDown()) {
            theDragData.setRetainAspect(true);
        }
        theDragData.setXDiff(diff.x);
        theDragData.setYDiff(diff.y);
        theDragData.setMouseLocation(new Point(event.getX(),event.getY()));

        // Go do the updating..
        // Rectangle r =
        // SVGUtils.calculateTransformedBounds(getController().getBeermatAWTCanvas().getGraphicsNodeFor(theCurrentElement),
        // getBeermatCanvas());
        //
        // int x = (int)((int)r.getCenterX() + diff.x);
        //
        // int y = (int)(r.getCenterY() + diff.y);

        // Element closestX = snap(theCurrentElement,x,1);
        // if(closestX!=null){
        // int close =
        // SVGUtils.calculateTransformedBounds(getBeermatCanvas().getGraphicsNodeFor(closestX),getBeermatCanvas()).x;
        // int movement = close - (int)r.getX();
        // theDragData.setXDiff(movement);
        // }
        //
        // Element closestY = snap(theCurrentElement,y,2);
        //
        // if(closestY!=null){
        // Rectangle r2 =
        // SVGUtils.calculateTransformedBounds(getBeermatCanvas().getGraphicsNodeFor(closestY),getBeermatCanvas());
        // int close = r2.y;
        // int movement = close - (int)r.getY();
        // theDragData.setYDiff(movement);
        // }

        new DragDataUpdater(getController(),theDragData);

        theStart = new Point(event.getX(),event.getY());
    }

    // private Element snap(Element dragged,int futurePosition,int axis) {
    // int range = 20;
    //
    // Element closestNode = null;
    // int closestDistance = Integer.MAX_VALUE;
    //
    // for (Element
    // node:SVGUtils.getSVGElements(getController().getManager().getDocument()))
    // {
    // if (!node.equals(dragged)) {
    // GraphicsNode g = getBeermatCanvas().getGraphicsNodeFor(node);
    //
    // Rectangle r = SVGUtils.calculateTransformedBounds(g,getBeermatCanvas());
    //
    // int pos;
    // if (axis==1) {
    // pos = (int)r.getCenterX();
    // }
    // else {
    // pos = (int)r.getCenterY();
    // }
    //
    // int distance = Math.abs(pos-futurePosition);
    //
    // if (distance<closestDistance) {
    // closestNode = node;
    // closestDistance = distance;
    // }
    // }
    // }
    //
    // if (closestDistance<range) {
    // return closestNode;
    // }
    // return null;
    // }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseReleased(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseReleased(MouseEvent event) {
        if (isDragging) {
            switch (theState) {
            case ELEMENT_DRAGGING:
                // Lets check to see if what was being dragged was anchorable
                // elements
                boolean allAnchorable = true;

                for (Element element:theHighlightManager
                    .getHighlightedElements()) {
                    if (!SVGUtils.isAnchorableElement(element)) {
                        allAnchorable = false;
                    }
                }

                if (allAnchorable) {
                    for (Element element:theHighlightManager
                        .getHighlightedElements()) {
                        SVGUtils.removeDocfactoAttributes(element);
                    }
                }

                theHighlightManager.endDragging();
                theHotSpot = null;

                break;

            case SELECTING_ELEMENTS:
                processSelectedItems();
                theSelectingOverlay.setStartingPoint(null);
                getBeermatCanvas().removeOverlay(theSelectingOverlay);
                break;
            }

            isDragging = false;
            theStart = null;
        }
    }

    /**
     * Analysis the selected area and see if any elements are captured
     * 
     * @since 2.4
     */
    private void processSelectedItems() {
        Rectangle bounds = theSelectingOverlay.getSelectedBounds();
        if (bounds==null) {
            // Nothing to do
            return;
        }

        SVGDocument doc =
            (SVGDocument)getController().getManager().getDocument();
        SVGSVGElement root = doc.getRootElement();

        AffineTransform transform;
        try {

            transform =
                getBeermatCanvas().getViewBoxTransform().createInverse();
            Point2D newPoint =
                transform.transform(
                    new Point2D.Double(bounds.getX(),bounds.getY()),null);

            SVGRect svgRect = root.createSVGRect();
            svgRect.setX(BeermatUtils.convertToInt(newPoint.getX()));
            svgRect.setY(BeermatUtils.convertToInt(newPoint.getY()));
            svgRect.setHeight(bounds.height);
            svgRect.setWidth(bounds.width);

            NodeList list;

            if (theSelectingOverlay.isIntersetRequired()) {
                list = root.getIntersectionList(svgRect,null);
            }
            else {
                list = root.getEnclosureList(svgRect,null);
            }

            List<Element> selected = new ArrayList<Element>(list.getLength());
            for (int i = 0;i<list.getLength();i++) {
                if (list.item(i) instanceof Element) {

                    Element group =
                        SVGUtils.findFirstGroup((Element)list.item(i));

                    if (group==null) {
                        // It doesn't belong a group, so just add it
                        selected.add((Element)list.item(i));
                    }
                    else {
                        // Have we already added the group
                        if (selected.contains(group)) {
                            continue;
                        }

                        selected.add(group);
                    }
                }
            }

            theHighlightManager.add(selected);
        }
        catch (NoninvertibleTransformException ignore) {
        }

    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseMoved(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseMoved(MouseEvent event) {
        HotSpot hotSpot =
            theHighlightManager.checkForHotSpot(event.getX(),event.getY());

        if (hotSpot!=null) {
            fireCursorChangeEvent(hotSpot.getCursorType());
        }
        else if (isText(event)) {
            fireCursorChangeEvent(AbstractCursorManager.TEXT);
        }
        else {
            fireCursorChangeEvent(AbstractCursorManager.HAND);
        }
    }

    /**
     * Fire a cursor event if different from the last one
     * 
     * @param cursor to change
     * @since 2.4
     */
    private void fireCursorChangeEvent(int cursor) {
        if (cursor!=theLastCursor) {
            getController().processEvent(new CursorChangeEvent(this,cursor));
            theLastCursor = cursor;
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    @Override
    public void handleEvent(BeermatEvent event) {
        // Change attributes for selected items
        if (event.isAttributeChangeEvent()) {
            if (theHighlightManager.hasSelectedElements()) {
                // Get list of highlighted elements
                List<Element> elements =
                    theHighlightManager.getHighlightedElements();
                for (Element element:elements) {
                    // If this element is a text element, change font attributes
                    if (isText(element)) {
                        getController().getView().setFontAttributesOnElement(
                            element);
                    }
                    else {
                        // else graphical element; if element is a group
                        // children are modified too
                        getController().getView().setAttributesOnElement(
                            element);
                    }
                }
            }
        }
    }

    /**
     * Calculate if highlights should happen
     * 
     * @param event mouse event
     * @since 2.4
     */
    private void processHighlighting(SVGElementMouseEvent event) {
        if (isDragging) {
            // Nothing to do
            return;
        }

        if (event.isMouseOverEvent()) {
            MouseEvent mouseEvent = event.getMouseEvent();

            // Check to see if the mouse in the the bounds of the document
            if (!isMouseOnCanvas(mouseEvent)) {
                // Nothing to do
                return;
            }

            Element hoverElement = event.getElement();

            if (hoverElement!=null) {
                // If the alt key not pressed
                if (!mouseEvent.isAltDown()) {
                    hoverElement = SVGUtils.findParentGroup(hoverElement);
                }

                theHighlightManager.hightlight(hoverElement);
                theCurrentElement = hoverElement;
            }
        }
        else {
            if (theCurrentElement!=null) {
                theHighlightManager.unhighlight(theCurrentElement);
                theCurrentElement = null;
            }
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#swapOut()
     */
    @Override
    public void swapOut() {
        theHighlightManager.removeHighlights();
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#getCursor()
     */
    @Override
    public int getCursor() {
        return AbstractCursorManager.HAND;
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#getSelectionFeathering()
     */
    @Override
    public int getSelectionFeathering() {
        return 10;
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseOverElement(com.docfacto.beermat.events.SVGElementMouseEvent)
     */
    @Override
    public void mouseOverElement(SVGElementMouseEvent event) {
        processHighlighting(event);
    }

    /**
     * @see com.docfacto.beermat.listeners.AbstractBeermatAWTInputListener#mouseOutElement(com.docfacto.beermat.events.SVGElementMouseEvent)
     */
    @Override
    public void mouseOutElement(SVGElementMouseEvent event) {
        processHighlighting(event);
    }

}
