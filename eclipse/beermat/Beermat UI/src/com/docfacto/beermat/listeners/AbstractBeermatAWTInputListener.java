package com.docfacto.beermat.listeners;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.Point2D;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.dialogs.AbstractTextEntryDialog;
import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.events.SVGElementMouseEvent;
import com.docfacto.beermat.hotspot.HotSpot;
import com.docfacto.beermat.ui.BeermatAWTCanvas;
import com.docfacto.beermat.utils.BeermatUtils;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.core.utils.SWTUtils;

/**
 * An Input Listener listens to all mouse events and key events.
 * 
 * This is an empty implementation, so sub classes need to override the methods required.
 * 
 * @author dhudson - created 25 Jun 2013
 * @since 2.4
 */
public abstract class AbstractBeermatAWTInputListener extends MouseAdapter
implements KeyListener,BeermatEventListener {

    private final BeermatController theController;
    private final BeermatAWTCanvas theBeermatCanvas;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param beermatCanvas beermat canvas
     * @since 2.4
     */
    public AbstractBeermatAWTInputListener(BeermatController controller,
    BeermatAWTCanvas beermatCanvas) {
        theController = controller;
        theBeermatCanvas = beermatCanvas;
    }

    /**
     * Return the controller
     * 
     * @return beermat controller
     * @since 2.4
     */
    public BeermatController getController() {
        return theController;
    }

    /**
     * Return the beermat canvas
     * 
     * @return canvas
     * @since 2.4
     */
    public BeermatAWTCanvas getBeermatCanvas() {
        return theBeermatCanvas;
    }

    /**
     * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseClicked(MouseEvent event) {
    }

    /**
     * @see java.awt.event.MouseAdapter#mouseDragged(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseDragged(MouseEvent event) {
    }

    /**
     * @see java.awt.event.MouseAdapter#mouseEntered(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseEntered(MouseEvent event) {
    }

    /**
     * @see java.awt.event.MouseAdapter#mouseExited(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseExited(MouseEvent event) {
    }

    /**
     * @see java.awt.event.MouseAdapter#mouseMoved(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseMoved(MouseEvent event) {
    }

    /**
     * @see java.awt.event.MouseAdapter#mousePressed(java.awt.event.MouseEvent)
     */
    @Override
    public void mousePressed(MouseEvent event) {
    }

    /**
     * @see java.awt.event.MouseAdapter#mouseReleased(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseReleased(MouseEvent event) {
    }

    /**
     * @see java.awt.event.MouseAdapter#mouseWheelMoved(java.awt.event.MouseWheelEvent)
     */
    @Override
    public void mouseWheelMoved(MouseWheelEvent event) {
    }

    /**
     * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
     */
    @Override
    public void keyPressed(KeyEvent event) {
    }

    /**
     * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
     */
    @Override
    public void keyReleased(KeyEvent event) {
    }

    /**
     * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
     */
    @Override
    public void keyTyped(KeyEvent event) {
    }

    /**
     * @see com.docfacto.beermat.listeners.BeermatEventListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    @Override
    public void handleEvent(BeermatEvent event) {
    }

    /**
     * This method is called when an active listener is about to be swapped out.
     * 
     * @since 2.4
     */
    public void swapOut() {
    }

    /**
     * This method is called when the listener is about to become active.
     * 
     * @since 2.4
     */
    public void swapIn() {
    }

    /**
     * Check to see if the mouse event is on the canvas
     * 
     * @param event mouse event
     * @return true if the mouse event is on the canvas
     * @since 2.4
     */
    public boolean isMouseOnCanvas(MouseEvent event) {
        return BeermatUtils.isMouseOnCanvas(event, getController());
    }

    /**
     * Check to see if the mouse position is over a text element
     * 
     * @param event mouse event
     * @return true if the mouse position is over a text element
     * @since 2.4
     */
    public boolean isText(MouseEvent event) {
        Point2D canvasPoint =
            BeermatUtils.getTranformedMousePoint(event, theBeermatCanvas.getViewBoxTransform());
        if (canvasPoint != null) {
            return isText(getBeermatCanvas().getElementAt((int)canvasPoint.getX(), (int)canvasPoint.getY()));
        }
        return false;
    }

    /**
     * Check to see if the element is text
     * 
     * @param element to check
     * @return true if the element is a text type element
     * @since 2.4
     */
    public boolean isText(Element element) {
        return SVGUtils.checkForTextEdit(element);
    }

    /**
     * Open then text entry dialog
     * 
     * @param location mouse event
     * @param element current element, which could be null
     * @param hotspot which can be null, for alignment info
     * @param anchorElement element which the text will be anchored to
     * @since 2.4
     */
    public void openTextDialog(final Point location, final Element element,final HotSpot hotspot,
    final Element anchorElement) {
        SWTUtils.UIThreadExec(new Runnable() {
            /**
             * @see java.lang.Runnable#run()
             */
            @Override
            public void run() {
                AbstractTextEntryDialog dialog = getController().getView().getSVGComposite().getTextDialog();
                dialog.setLocation(location.x, location.y);

                Element parent = element != null ? (Element)element.getParentNode() : null;

                if (parent != null && parent.getNodeName().equals("g") &&
                    parent.getAttribute("class").equals("textGroup")) {
                    dialog.setElement(parent);
                }
                else {
                    dialog.setElement(element);
                }
                dialog.setHotSpot(hotspot);
                dialog.setAnchorElement(anchorElement);

                if (dialog.open() == Window.OK) {
                    dialog.processText(getController().getManager().getDocument());
                }
            }
        });
    }

    /**
     * Return the default cursor the listener.
     * 
     * @return the default cursor
     * @since 2.4
     */
    public abstract int getCursor();

    /**
     * Return the feathering distance from the shapes before they are selected.
     * 
     * @return the amount of surrounding distance in pixels
     * @since 2.4
     */
    public abstract int getSelectionFeathering();

    /**
     * Fired when a mouse is over an element
     * 
     * @param event containing all of the information
     * @since 2.4
     */
    public abstract void mouseOverElement(SVGElementMouseEvent event);

    /**
     * Fired when the mouse has left an element
     * 
     * @param event containing all of the information
     * @since 2.4
     */
    public abstract void mouseOutElement(SVGElementMouseEvent event);

    /**
     * Check to see if element mouse events are required whilst dragging.
     * 
     * {@docfacto.note default is true }
     * 
     * @return true if mouse over and mouse out element events required when dragging
     * @since 2.4
     */
    public boolean requiresMouseElementEvents() {
        return true;
    }
}
