package com.docfacto.beermat.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import com.docfacto.beermat.ui.toolbar.PaletteItem;
import com.docfacto.beermat.ui.toolbar.PaletteItemGrouper;
import com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler;
import com.docfacto.core.utils.ImageUtils;

/**
 * Abstract PaletteItem Toolbar {@docfacto.todo move this to CORE when it is
 * completely uncoupled from beermat}
 * 
 * @author kporter - created Oct 8, 2013
 * @since 2.4.7
 */
public abstract class AbstractPaletteItemToolbar implements Listener {
    public static final String PALETTE_ITEM_DATA_KEY = "PI";
    public static final String SHAPE_HANDLER_DATA_KEY = "SH";

    private ToolBar theToolBar;

    /**
     * Constructor.
     * @param parent The parent composite
     * @param style The style int
     * @since 2.4.7
     */
    public AbstractPaletteItemToolbar(Composite parent,int style) {
        parent.setLayout(new FillLayout(SWT.VERTICAL));
        theToolBar = new ToolBar(parent,SWT.FLAT|SWT.RIGHT|style);
        theToolBar.setBackground(getBackgroundColor());
    }

    /**
     * Add a palette item to the toolbar
     * 
     * @param paletteItem the PaletteItem to add to this toolbar.
     * @param style The style of the item, SWT.PUSH or SWT.RADIO
     * @return The created ToolItem
     * @since 2.4
     */
    public ToolItem addItem(PaletteItem paletteItem,int style) {
        ToolItem item = new ToolItem(theToolBar,style);
        Image image = paletteItem.getImage();

        if (image!=null) {
            if (image.getImageData().height>getIconSize()||
                image.getImageData().width>getIconSize()) {
                image = ImageUtils.resizeImage(image,getIconSize(),getIconSize());
            }
            item.setImage(image);
        }

        item.setToolTipText(paletteItem.getTooltipText());
        item.addListener(SWT.Selection,this);

        item.setData(PALETTE_ITEM_DATA_KEY,paletteItem);

        if (paletteItem instanceof ShapeHandler) {
            item.setData(SHAPE_HANDLER_DATA_KEY,(ShapeHandler)paletteItem);
        }

        if (paletteItem instanceof PaletteItemGrouper) {
            // set bounds to the palette item so that bounds can be used.
            ((PaletteItemGrouper)paletteItem).setBounds(item.getBounds());
            // Set the ToolItem so that the icon can change
            ((PaletteItemGrouper)paletteItem).setToolItem(item);
        }

        item.setWidth(getIconSize()+4);
        return item;
    }

    /**
     * Returns the ToolBar.
     * 
     * @return the ToolBar
     * @since 2.4.3
     */
    public ToolBar getToolBar() {
        return theToolBar;
    }

    /**
     * Get the icon size this toolbar uses
     * 
     * @return the icon size this toolbar uses
     * @since 2.4.7
     */
    public abstract int getIconSize();

    /**
     * Get the colour to be used, override this to define your own colour
     * 
     * @return The colour to be used as a background
     * @since 2.4.7
     */
    public abstract Color getBackgroundColor();

}