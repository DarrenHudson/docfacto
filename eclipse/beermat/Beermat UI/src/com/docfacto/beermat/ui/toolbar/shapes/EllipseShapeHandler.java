package com.docfacto.beermat.ui.toolbar.shapes;

import static com.docfacto.beermat.plugin.PluginConstants.*;

import org.apache.batik.dom.svg.SVGOMEllipseElement;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.utils.ElementUtils;
import com.docfacto.beermat.utils.PluginManager;

/**
 * An Ellipse handler.
 * 
 * @author kporter - created Jun 21, 2013
 * @since 2.4
 * @docfacto.link key="ellipseTool" uri="${doc-beermat}/drawing/simpleshapes/c_ellipseCircleTool.dita" link-to="doc"
 */
public class EllipseShapeHandler extends AbstractShapeHandler {

    public EllipseShapeHandler(BeermatController controller) {
        super(controller);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        return "Ellipse";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
     */
    @Override
    public Image getImage() {
        return PluginManager.getImage(getImagePath());
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        return ICON_FOLDER+ELLIPSE_ORANGE;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        return "Ellipse Tool (e)";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getShortcutKey()
     */
    @Override
    public char getShortcutKey() {
        return 'e';
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#createShape(org.w3c.dom.svg.SVGDocument,
     * org.eclipse.swt.graphics.Point)
     */
    @Override
    public Element createShape(SVGDocument document,Point startPoint) {
        return ElementUtils.createEllipse(document,startPoint);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#updateShape(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void drag(Element element,ShapeUpdateData updateData) {
        ElementUtils.updateEllipse(element,updateData.getStartToEndDiff());
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#resize(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void resize(Element element,ShapeUpdateData updateData) {
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#move(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void move(Element element,ShapeUpdateData updateData) {
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.AbstractShapeHandler#mouseClick(org.eclipse.swt.graphics.Point,
     * org.w3c.dom.Element)
     */
    @Override
    public Element mouseClick(Point location,Element lastElement) {
        Point size = super.getSize(lastElement,SVGOMEllipseElement.class);
        float radiusX = size.x/2;
        float radiusY = size.y/2;
        return ElementUtils.createEllipse(getController().getManager()
            .getDocument(),location,radiusX,radiusY);
    }

}
