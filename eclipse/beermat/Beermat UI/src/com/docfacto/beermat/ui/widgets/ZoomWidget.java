package com.docfacto.beermat.ui.widgets;

import static com.docfacto.beermat.plugin.PluginConstants.ICON_FOLDER;
import static com.docfacto.beermat.plugin.PluginConstants.ZOOM_IN;
import static com.docfacto.beermat.plugin.PluginConstants.ZOOM_IN_BLUE;
import static com.docfacto.beermat.plugin.PluginConstants.ZOOM_OUT;
import static com.docfacto.beermat.plugin.PluginConstants.ZOOM_OUT_BLUE;

import java.awt.geom.AffineTransform;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Label;
import org.w3c.dom.Element;

import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.events.ZoomChangeEvent;
import com.docfacto.beermat.events.ZoomEvent;
import com.docfacto.beermat.listeners.BeermatEventListener;
import com.docfacto.beermat.ui.AbstractControlToolBar;
import com.docfacto.beermat.utils.PluginManager;
import com.docfacto.common.NameValuePair;
import com.docfacto.core.swt.DefaultMouseListener;
import com.docfacto.core.utils.SWTUtils;

/**
 * Handles the Zoom features for the SVG canvas
 * 
 * Stop zooming when the zoom factor is 0.03125 otherwise Batik will not
 * recover.
 * 
 * @author dhudson - created 18 Jun 2013
 * @since 2.4
 * @docfacto.link key="zoomWidget" uri="/Volumes/Striped/Users/dhudson/development/docfacto/main/doc/dita/docfacto/beermat/canvasbar/c_zoom_widget.dita" version="1.1" link-type="link-to-doc" target-file-type="xml" metadata=""
 * 
 */
public class ZoomWidget extends AbstractControlBarWidget implements
BeermatEventListener {

    private Label theZoomOutButton;
    private Combo theCombo;
    String thePreviousComboValue = "100%";
    private double theCurrentScale = 1;

    private static final String[] thePercentages = {"5%","10%","16%","25%",
        "33%","50%","75%","100%","150%","200%","300%","400%","600%","800%",
        "1200%","1600%","2000%","3000%"};
    private static final Image ZOOMOUT = PluginManager.getImage(ICON_FOLDER+ZOOM_OUT);
    private static final Image ZOOMOUT_COMP = PluginManager.getImage(ICON_FOLDER+ZOOM_OUT_BLUE);
    private static final Image ZOOMIN = PluginManager.getImage(ICON_FOLDER+ZOOM_IN);
    private static final Image ZOOMIN_COMP = PluginManager.getImage(ICON_FOLDER+ZOOM_IN_BLUE);

    /**
     * Constructor.
     * 
     * @param toolBar control toolbar
     * @since 2.4
     */
    public ZoomWidget(AbstractControlToolBar toolBar) {
        super(toolBar);
        toolBar.getController().addListener(this);
    }

    /**
     * @see com.docfacto.beermat.ui.widgets.AbstractControlBarWidget#layoutWidget()
     */
    @Override
    protected void layoutWidget() {

        Label label = createLabel("Zoom :");
        label.setToolTipText("Click to reset zoom level");
        label.setLayoutData(getGridData(true));
        label.addMouseListener(new DefaultMouseListener() {
            /**
             * @see com.docfacto.core.swt.DefaultMouseListener#mouseDown(org.eclipse.swt.events.MouseEvent)
             */
            @Override
            public void mouseDown(MouseEvent e) {
                getToolbar().getController().processEvent(
                    new ZoomEvent(this,ZoomEvent.RESET));
                handleButtonLogic();
            }
        });

        theCombo = new Combo(getParent(),SWT.NONE);

        theCombo.setItems(thePercentages);
        theCombo.setText("100%");
        theCombo.setLayoutData(getGridData(true));
        theCombo.addFocusListener(new FocusListener() {
            /**
             * @see org.eclipse.swt.events.FocusListener#focusLost(org.eclipse.swt.events.FocusEvent)
             */
            @Override
            public void focusLost(FocusEvent e) {
                performComboAction();
            }

            /**
             * @see org.eclipse.swt.events.FocusListener#focusGained(org.eclipse.swt.events.FocusEvent)
             */
            @Override
            public void focusGained(FocusEvent e) {
                thePreviousComboValue = theCombo.getText();
            }
        });

        theCombo.addKeyListener(new KeyAdapter() {
            /**
             * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
             */
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.character==SWT.CR||e.character==SWT.ESC) {
                    getToolbar().setFocus();
                }
            }
        });

        theCombo.addSelectionListener(new SelectionListener() {

            /**
             * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent e) {
                // Just lose focus to call perform the actions
                performComboAction();
            }

            /**
             * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
             */
            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });

        final Label up = new Label(getParent(),SWT.NONE);
        up.setLayoutData(getGridData(true));
        up.setImage(PluginManager.getImage(ICON_FOLDER+ZOOM_IN));
        up.setToolTipText("Zoom in");
        up.addMouseListener(new MouseAdapter() {
            /**
             * @see org.eclipse.swt.events.MouseAdapter#mouseDown(org.eclipse.swt.events.MouseEvent)
             */
            @Override
            public void mouseDown(MouseEvent e) {
                getToolbar().getController().processEvent(new ZoomEvent(this,ZoomEvent.IN));
                handleButtonLogic();
            }

        });
        up.addMouseTrackListener(new MouseTrackAdapter() {

            /**
             * @see org.eclipse.swt.events.MouseTrackAdapter#mouseEnter(org.eclipse.swt.events.MouseEvent)
             */
            @Override
            public void mouseEnter(MouseEvent e) {
                up.setImage(ZOOMIN_COMP);
            }

            /**
             * @see org.eclipse.swt.events.MouseTrackAdapter#mouseExit(org.eclipse.swt.events.MouseEvent)
             */
            @Override
            public void mouseExit(MouseEvent e) {
                up.setImage(ZOOMIN);
            }
        });

        theZoomOutButton = new Label(getParent(),SWT.NONE);
        GridData gd = getGridData(true);
        gd.horizontalIndent = 2;
        theZoomOutButton.setLayoutData(gd);
        theZoomOutButton.setImage(ZOOMOUT);
        theZoomOutButton.setToolTipText("Zoom out");

        theZoomOutButton.addMouseListener(new MouseAdapter() {
            /**
             * @see org.eclipse.swt.events.MouseAdapter#mouseDown(org.eclipse.swt.events.MouseEvent)
             */
            @Override
            public void mouseDown(MouseEvent e) {
                getToolbar().getController().processEvent(new ZoomEvent(this,ZoomEvent.OUT));
                handleButtonLogic();
            }
        });
        
        theZoomOutButton.addMouseTrackListener(new MouseTrackAdapter() {

            /**
             * @see org.eclipse.swt.events.MouseTrackAdapter#mouseEnter(org.eclipse.swt.events.MouseEvent)
             */
            @Override
            public void mouseEnter(MouseEvent e) {
                theZoomOutButton.setImage(ZOOMOUT_COMP);
            }

            /**
             * @see org.eclipse.swt.events.MouseTrackAdapter#mouseExit(org.eclipse.swt.events.MouseEvent)
             */
            @Override
            public void mouseExit(MouseEvent e) {
                theZoomOutButton.setImage(ZOOMOUT);
            }
        });
    }

    /**
     * @see com.docfacto.beermat.ui.widgets.AbstractControlBarWidget#getAttributesForElement(org.w3c.dom.Element)
     */
    @Override
    protected List<NameValuePair> getAttributesForElement(Element element) {
        return EMPTY_LIST;
    }

    /**
     * Enable disable the zoom out button as this breaks the SVGCanvas and
     * transforms
     * 
     * @since 2.4
     */
    public void handleButtonLogic() {

        AffineTransform transform =
            getToolbar().getController().getView().getSVGComposite()
                .getBeermatAWTCanvas().getRenderingTransform();

        updateCombo((transform.getScaleY()*100));

        if (transform.getScaleX()<0.03126) {
            // Disable the zoom out button
            theZoomOutButton.setEnabled(false);
        }
        else {
            theZoomOutButton.setEnabled(true);
        }
    }

    /**
     * Takes a scale as a double and sets the combo to this scale. A % is added
     * after the number before adding to the combo.
     * 
     * @param scale The scale to update the combo with
     * @since 2.4.3
     */
    public void updateCombo(double scale) {
        String text = String.format("%d%%",(int) scale);
        theCombo.setText(text);
    }

    private void performComboAction() {
        String text = theCombo.getText();

        // first replace all %
        text = text.replaceAll("%","");
        try {
            // Try parse the string
            double scale = Double.parseDouble(text);
            if (scale<=0||scale>10000) {
                theCombo.setText(thePreviousComboValue);
            }
            else {
                updateCombo(scale);
                // Here set the scale
                scale /= 100;
                theCurrentScale = scale;
                getToolbar().getController().getBeermatAWTCanvas()
                    .setCanvasScale(scale);
            }
        }
        catch (NumberFormatException e) {
            theCombo.setText(thePreviousComboValue);
        }
    }

    private static String addPercentFromText(String text) {
        if (!text.endsWith("%")) {
            return text += "%";
        }
        else {
            return text;
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.BeermatEventListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    @Override
    public void handleEvent(BeermatEvent event) {
        if (event.isZoomChangeEvent()) {
            ZoomChangeEvent zoomEvent = (ZoomChangeEvent)event;
            final double d = zoomEvent.getScale();
            if (d==theCurrentScale) {
                return;
            }
            else {
                theCurrentScale = d;
                thePreviousComboValue =
                    addPercentFromText(Double.toString(d*100));
                SWTUtils.UIThreadExec(new Runnable() {
                    /**
                     * @see java.lang.Runnable#run()
                     */
                    @Override
                    public void run() {
                        updateCombo(d*100);
                    }
                });
            }
        }
    }
}
