package com.docfacto.beermat.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.ui.widgets.GridWidget;
import com.docfacto.beermat.ui.widgets.MouseStatusWidget;
import com.docfacto.beermat.ui.widgets.SVGSizeWidget;
import com.docfacto.beermat.ui.widgets.ZoomWidget;

/**
 * A Control toolbar for canvas related widgets.
 * 
 * @author kporter - created Aug 22, 2013
 * @since 2.4.3
 */
public class CanvasControlToolbar extends AbstractControlToolBar{

    /**
     * Constructor.
     * @param parent The parent composite 
     * @param view The BeermatView
     * @param controller The BeermatController
     * @since 2.4.3
     */
    public CanvasControlToolbar(Composite parent,
    BeermatController controller,BeermatView view) {
        super(parent,view,controller,SWT.NONE);
        layoutUI();
    }

    /**
     * @see com.docfacto.beermat.ui.AbstractControlToolBar#layoutUI()
     */
    @Override
    protected void layoutUI() {
        new ZoomWidget(this);

        addSeparator();
        
        new GridWidget(this);

        addSeparator();

        new SVGSizeWidget(this);

        addSeparator();

        new MouseStatusWidget(getController(),this);

    }

}
