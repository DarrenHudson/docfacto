package com.docfacto.beermat.ui.toolbar.shapes;

import static com.docfacto.beermat.plugin.PluginConstants.*;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.utils.ElementUtils;
import com.docfacto.beermat.utils.PluginManager;

/**
 * Handles path freedrawing.
 * 
 * @author kporter - created Jun 21, 2013
 * @since 2.4
 * @docfacto.link key="pencilTool"
 * uri="${doc-beermat}/drawing/simpleshapes/c_drawing_pencil.dita" link-to="doc"
 */
public class PathShapeHandler extends AbstractPathHandler {

    /**
     * Constructor.
     * 
     * @param controller
     * @since 2.4
     */
    public PathShapeHandler(BeermatController controller) {
        super(controller,null);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        return "Freehand";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
     */
    @Override
    public Image getImage() {
        return PluginManager.getImage(getImagePath());
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        return ICON_FOLDER+PENCIL_ORANGE;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        return "Pencil Tool (P)";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getShortcutKey()
     */
    @Override
    public char getShortcutKey() {
        return 'p';
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#createShape(org.w3c.dom.svg.SVGDocument,
     * org.eclipse.swt.graphics.Point)
     */
    @Override
    public Element createShape(SVGDocument document,Point startPoint) {
        return ElementUtils.createPath(document,startPoint);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#updateShape(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void drag(Element element,ShapeUpdateData updateData) {
        ElementUtils.updatePath(element,updateData.getDiff());
    }

}
