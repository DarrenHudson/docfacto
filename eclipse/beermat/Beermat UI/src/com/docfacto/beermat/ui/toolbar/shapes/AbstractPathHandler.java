package com.docfacto.beermat.ui.toolbar.shapes;

import static com.docfacto.beermat.plugin.PluginConstants.CUSTOM_ORANGE;
import static com.docfacto.beermat.plugin.PluginConstants.ICON_FOLDER;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;
import org.w3c.dom.svg.SVGRect;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.svg.TransformElementParser;
import com.docfacto.beermat.ui.toolbar.AbstractPaletteItem;
import com.docfacto.beermat.utils.ElementSizer;
import com.docfacto.beermat.utils.PluginManager;

/**
 * Defines a shape that holds a path Custom shapes.
 * 
 * @author kporter - created Jul 3, 2013
 * @since 2.4
 */
public abstract class AbstractPathHandler extends AbstractPaletteItem implements IPathHandler,ShapeHandler {

    private String thePath;

    /**
     * Constructor.
     * 
     * @param controller Beermat controller
     * @param path to create
     */
    public AbstractPathHandler(BeermatController controller,String path) {
        super(controller);
        thePath = path;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#createShape(org.w3c.dom.svg.SVGDocument,
     * org.eclipse.swt.graphics.Point)
     */
    @Override
    public Element createShape(SVGDocument document,Point startPoint) {
        return null;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#drag(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void drag(Element element,ShapeUpdateData updateData) {
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#resize(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void resize(Element element,ShapeUpdateData updateData) {
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#move(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void move(Element element,ShapeUpdateData updateData) {
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.IPathHandler#setPath(java.lang.String)
     */
    public void setPath(String path) {
        thePath = path;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.IPathHandler#getPath()
     */
    public String getPath() {
        return thePath;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        return "Custom";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        return ICON_FOLDER+CUSTOM_ORANGE;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        return "Custom shape";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
     */
    @Override
    public Image getImage() {
        return PluginManager.getImage(getImagePath());
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#mouseClick(org.eclipse.swt.graphics.Point,
     * org.w3c.dom.Element)
     */
    @Override
    public Element mouseClick(Point location,Element lastElement) {
        try {
            Document doc = getController().getManager().getDocument();
            Element newPath = (Element)lastElement.cloneNode(true);
            doc.getDocumentElement().appendChild(newPath);

            SVGRect r = ElementSizer.INSTANCE.getElementBounds(newPath);

            int x = location.x-(int)r.getX();
            int y = location.y-(int)r.getY();

            TransformElementParser transformer = new TransformElementParser(newPath);
            transformer.setTranslate(x,y,true);

            return newPath;

        }
        catch (NullPointerException ex) {
            return null;
        }
    }

}
