package com.docfacto.beermat.ui.views;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.geom.Dimension2D;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.EventObject;

import javax.swing.JRootPane;

import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.swing.JSVGScrollPane;
import org.apache.batik.swing.svg.GVTTreeBuilderAdapter;
import org.apache.batik.swing.svg.GVTTreeBuilderEvent;
import org.eclipse.core.runtime.IPath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.beermat.svg.BeermatUserAgent;
import com.docfacto.beermat.utils.BeermatUtils;
import com.docfacto.common.DocfactoException;

/**
 * A simple view to display a SVG's.
 * 
 * @author kporter - created Feb 4, 2013
 * @since 2.2
 */
public class CanvasView extends Composite {

    private LoadCompleteListener theListener;

    private JSVGCanvas theSVGCanvas;

    /**
     * Creates a new blank BeermatCanvas. The SVG can later be set..
     * 
     * @param parent The composite which this canvas is created in, SWT
     * required.
     * @since 2.2
     */
    public CanvasView(Composite parent) {
        super(parent,SWT.EMBEDDED);

        parent.setLayout(new FillLayout());

        final Frame frame = SWT_AWT.new_Frame(this);

        JRootPane rootPane = new JRootPane();

        BeermatUserAgent userAgent = new BeermatUserAgent(rootPane);
        parent.setLayout(new FillLayout());

        java.awt.Container contentPane = rootPane.getContentPane();
        contentPane.setLayout(new BorderLayout());

        theSVGCanvas = new JSVGCanvas(userAgent,false,false);

        JSVGScrollPane scrollPane = new JSVGScrollPane(theSVGCanvas);

        contentPane.add(scrollPane);

        frame.add(rootPane);

        theSVGCanvas.addGVTTreeBuilderListener(new GVTTreeBuilderAdapter() {
            /**
             * @see org.apache.batik.swing.svg.GVTTreeBuilderAdapter#gvtBuildCompleted(org.apache.batik.swing.svg.GVTTreeBuilderEvent)
             */
            @Override
            public void gvtBuildCompleted(GVTTreeBuilderEvent event) {
                if (theListener!=null) {
                    theListener.loadComplete(new EventObject(this));
                }
            }
        });
    }

    /**
     * Sets the SVG to a given IPath.
     * 
     * @param svgPath The path at which an IResource can be found, which is an
     * SVG.
     * @throws DocfactoException if unable to load or parse the given url
     * @since 2.2
     */
    public void setSVG(IPath svgPath) throws DocfactoException {
        try {
            URL url = svgPath.toFile().toURI().toURL();
            theSVGCanvas.loadSVGDocument(url.toExternalForm());
        }
        catch (MalformedURLException ex) {
            BeermatUIPlugin.logException("SVGComposite setSVGPath ",ex);
            throw new DocfactoException("Unable to parse URL",ex);
        }
    }

    /**
     * Gets the size of the SVG set to this canvas.
     * 
     * This should only be called when the document has finished loading
     * 
     * @return The size of the SVG document.
     * @since 2.2
     */
    public Point getSVGSize() {

        Dimension2D size =
            theSVGCanvas.getSVGDocumentSize();
        return new Point(BeermatUtils.convertToInt(size.getHeight()),
            (BeermatUtils.convertToInt(size.getWidth())));
    }

    /**
     * Add a CompleteListener to this view.
     * 
     * CompleteListener's are used to fire an event when the svg loading is
     * complete.  Setting to null will remove the listener
     * 
     * @param listener The CompleteListener
     * @since 2.2
     */
    public void setEventLoadCompleteListener(LoadCompleteListener listener) {
        theListener = listener;
    }
}
