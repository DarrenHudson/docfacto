package com.docfacto.beermat.ui.toolbar;

import static com.docfacto.beermat.plugin.PluginConstants.CLEAR;
import static com.docfacto.beermat.plugin.PluginConstants.ICON_FOLDER;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.graphics.Image;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.updaters.ClearDocumentUpdater;
import com.docfacto.beermat.utils.PluginManager;

/**
 * Clear the document
 * 
 * @author dhudson - created 21 Jun 2013
 * @since 2.4
 * @docfacto.link uri="${doc-beermat}/palettebar/c_clear_palette_item.dita"  key="clear-action" version="2.4.2" link-to="doc"
 */
public class ClearPaletteItem extends AbstractPaletteItem {

    /**
     * Constructor.
     * 
     * @param controller the beermat controller
     * @since 2.4
     */
    public ClearPaletteItem(BeermatController controller) {
        super(controller);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        return "Clear";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
     */
    @Override
    public Image getImage() {
        return PluginManager.getImage(getImagePath());
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        return "Clear Canvas Tool";
    }

    /**
     * Clear the document
     * 
     * @since 2.4
     */
    @Override
    public void performAction() {

        boolean response =
            MessageDialog.openConfirm(getController().getView()
                .getSVGComposite().getShell(),"Clear SVG",
                "Are you sure you want to clear the SVG");

        if (response) {
            new ClearDocumentUpdater(getController());
        }
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        return ICON_FOLDER+CLEAR;
    }

}
