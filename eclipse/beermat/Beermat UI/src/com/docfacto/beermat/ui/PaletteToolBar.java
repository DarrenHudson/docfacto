package com.docfacto.beermat.ui;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ToolItem;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.events.MarkerChangeEvent;
import com.docfacto.beermat.events.MarkerChangeEvent.Position;
import com.docfacto.beermat.events.PaletteBarChangeEvent;
import com.docfacto.beermat.generated.ElementDef;
import com.docfacto.beermat.generated.Palette;
import com.docfacto.beermat.generated.SvgDef;
import com.docfacto.beermat.generated.Type;
import com.docfacto.beermat.listeners.BeermatEventListener;
import com.docfacto.beermat.ui.BeermatViewConstants.ToolbarPopup;
import com.docfacto.beermat.ui.toolbar.ClearPaletteItem;
import com.docfacto.beermat.ui.toolbar.CursorPaletteItem;
import com.docfacto.beermat.ui.toolbar.InsertCSSPaletteItem;
import com.docfacto.beermat.ui.toolbar.InsertImagePaletteItem;
import com.docfacto.beermat.ui.toolbar.MarkerPaletteItem;
import com.docfacto.beermat.ui.toolbar.PaletteItem;
import com.docfacto.beermat.ui.toolbar.PaletteItemGrouper;
import com.docfacto.beermat.ui.toolbar.shapes.CircleShapeHandler;
import com.docfacto.beermat.ui.toolbar.shapes.CustomShapeHandler;
import com.docfacto.beermat.ui.toolbar.shapes.ElementShapeHandler;
import com.docfacto.beermat.ui.toolbar.shapes.EllipseShapeHandler;
import com.docfacto.beermat.ui.toolbar.shapes.LineShapeHandler;
import com.docfacto.beermat.ui.toolbar.shapes.PathShapeHandler;
import com.docfacto.beermat.ui.toolbar.shapes.RectangleShapeHandler;
import com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler;
import com.docfacto.beermat.ui.toolbar.shapes.SquareShapeHandler;
import com.docfacto.beermat.ui.toolbar.shapes.TextShapeHandler;
import com.docfacto.beermat.utils.PluginManager;
import com.docfacto.beermat.utils.SVGUtils;

/**
 * This class handles all of the events and selections for the palette tool bar
 * 
 * @author dhudson - created 19 Jun 2013
 * @since 2.4
 */
public class PaletteToolBar extends PaletteItemToolBar implements
BeermatEventListener {

    private PaletteItem theSelectedPaletteItem;
    private ShapeHandler theSelectedShapeHandler;

    private HashMap<Integer,PaletteItem> theShortcutKeys;
    
    /**
     * Constructor. {@docfacto.note Only create this inside an empty parent
     * Composite. This toolbar will fill the parent, and therefore the parent's
     * layout needs to be set to a FillLayout() and not have other control's
     * inside of it}
     * 
     * @param controller The BeermatController
     * @param parent The parent Composite
     * @param view The BeermatView
     * @since 2.4
     */
    public PaletteToolBar(BeermatController controller,Composite parent,
    BeermatView view) {
        super(parent,controller,SWT.VERTICAL);
        theShortcutKeys = new HashMap<Integer,PaletteItem>(10);
        
        getController().addListener(this);

        addItems();

        // Set the first item to be selected
        getToolBar().getItem(0).setSelection(true);

        getToolBar().pack();
    }

    /**
     * Dispose of the SWT widgets
     * 
     * @since 2.4
     */
    public void dispose() {
        getToolBar().dispose();
    }

    /**
     * Add items.
     * 
     * This needs to be externalised somehow.
     * 
     * @since 2.4
     */
    private void addItems() {

        // Radio buttons need to be together to form a group
        CursorPaletteItem cursor = new CursorPaletteItem(getController());
        theShortcutKeys.put(new Integer(cursor.getShortcutKey()),cursor);
        addItem(cursor,SWT.RADIO);
        
        PathShapeHandler path = new PathShapeHandler(getController());
        theShortcutKeys.put(new Integer(path.getShortcutKey()),path);
        addItem(path,SWT.RADIO);
        
        LineShapeHandler line = new LineShapeHandler(getController());
        theShortcutKeys.put(new Integer(line.getShortcutKey()),line);
        addItem(line,SWT.RADIO);
        
        RectangleShapeHandler rect = new RectangleShapeHandler(getController());
        theShortcutKeys.put(new Integer(rect.getShortcutKey()),rect);
        addItem(rect,SWT.RADIO);
        
        SquareShapeHandler square = new SquareShapeHandler(getController());
        theShortcutKeys.put(new Integer(square.getShortcutKey()),square);
        addItem(square,SWT.RADIO);
        
        CircleShapeHandler circle = new CircleShapeHandler(getController());
        theShortcutKeys.put(new Integer(circle.getShortcutKey()),circle);
        addItem(circle,SWT.RADIO);
        
        EllipseShapeHandler ellipse = new EllipseShapeHandler(getController());
        theShortcutKeys.put(new Integer(ellipse.getShortcutKey()),ellipse);       
        addItem(ellipse,SWT.RADIO);

        TextShapeHandler text = new TextShapeHandler(getController());
        theShortcutKeys.put(new Integer(text.getShortcutKey()),text);        
        addItem(text,SWT.RADIO);

        addCustomItems();
        // End of group
        new ToolItem(getToolBar(),SWT.SEPARATOR);

        addItem(new ClearPaletteItem(getController()),SWT.PUSH);
        addItem(new InsertImagePaletteItem(getController()),SWT.PUSH);
        addItem(new InsertCSSPaletteItem(getController()),SWT.PUSH);

    }

    /**
     * Return the map of shortcut keys
     * 
     * @return the map of shortcut keys
     * @since 2.5
     */
    public HashMap<Integer,PaletteItem> getShortcutKeys() {
        return theShortcutKeys;
    }
    
    /**
     * From Beermat.xml, go a load the group items
     * 
     * @since 2.4
     */
    private void addCustomItems() {
        List<Palette> palettes = PluginManager.getPaletteType(Type.TOOLBAR);
        for (Palette palette:palettes) {
            List<SvgDef> defs = PluginManager.getSVGDefs(palette);
            PaletteItemGrouper group =
                new PaletteItemGrouper(getController(),palette.getName(),
                    ToolbarPopup.HORIZONTAL);

            for (SvgDef def:defs) {
                group.addItem(
                    new CustomShapeHandler(
                        getController(),group,def));
            }

            List<ElementDef> elementdefs =
                PluginManager.getElementDefs(palette);
            for (ElementDef def:elementdefs) {
                try {
                    Element gElement =
                        SVGUtils.getGroupElementFromElementDef(def);

                    group.addItem(new ElementShapeHandler(getController(),
                        group,gElement,def.getName()));
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }

            // Add the group to the toolbar
            addItem(group,SWT.RADIO);
        }
    }

    /**
     * @see com.docfacto.beermat.listeners.BeermatEventListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    @Override
    public void handleEvent(BeermatEvent event) {

        if (event.isPaletteToolbarChangeEvent()) {
            PaletteBarChangeEvent paletteEvent = (PaletteBarChangeEvent)event;
            theSelectedPaletteItem = paletteEvent.getPaletteItem();
            theSelectedShapeHandler = paletteEvent.getShapeHandler();

            if (!(theSelectedPaletteItem instanceof PathShapeHandler)
                &&!(theSelectedPaletteItem instanceof LineShapeHandler)) {
                getController().processEvent(
                    new MarkerChangeEvent(this,Position.BOTH,
                        MarkerPaletteItem.NONE));
            }

            if (theSelectedShapeHandler!=null) {
                theSelectedPaletteItem.performAction();
            }
        }
    }

    /**
     * Get current selected item
     * 
     * @return the currently selected item
     * @since 2.4
     */
    public PaletteItem getSelectedItem() {
        return theSelectedPaletteItem;
    }

    /**
     * The currently selected shape handler or null
     * 
     * @return the selected shape handler
     * @since 2.4
     */
    public ShapeHandler getSelectedShapeHandler() {
        return theSelectedShapeHandler;
    }

    /**
     * @see com.docfacto.beermat.ui.PaletteItemToolBar#getBackgroundColor()
     */
    @Override
    public Color getBackgroundColor() {
        return BeermatViewConstants.COLOR_TOOLBAR_SECONDARY;
    }

}
