package com.docfacto.beermat.ui.xmleditor;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentListener;
import org.eclipse.jface.text.IDocumentPartitioner;
import org.eclipse.jface.text.rules.FastPartitioner;
import org.eclipse.ui.editors.text.TextFileDocumentProvider;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.SVGDirtyEvent;
import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.common.DocfactoException;
import com.docfacto.core.editors.xmleditor.XMLPartitionScanner;

/**
 * Document Provider
 * 
 * @author dhudson - created 10 Jul 2013
 * @since 2.4
 */
public class XMLDocumentProvider extends TextFileDocumentProvider implements
IDocumentListener {

    private boolean hasChanged = false;
    private final BeermatController theController;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @since 2.4
     */
    public XMLDocumentProvider(BeermatController controller) {
        theController = controller;
    }

    /**
     * @see org.eclipse.ui.editors.text.TextFileDocumentProvider#createFileInfo(java.lang.Object)
     */
    protected FileInfo createFileInfo(Object element) throws CoreException {
        FileInfo info = super.createFileInfo(element);
        if (info==null) {
            info = createEmptyFileInfo();
        }
        IDocument document = info.fTextFileBuffer.getDocument();
        if (document!=null) {
            IDocumentPartitioner partitioner =
                new FastPartitioner(
                    new XMLPartitionScanner(),
                    new String[] {
                        XMLPartitionScanner.XML_TAG,
                        XMLPartitionScanner.XML_COMMENT});
            partitioner.connect(document);
            document.setDocumentPartitioner(partitioner);

            try {
                String contents =
                    theController.getManager().getFormattedDocument();
                if (contents!=null) {
                    document.set(contents);
                }
            }
            catch (DocfactoException ex) {
                BeermatUIPlugin.logException("Unable to format document",ex);
            }

            document.addDocumentListener(this);
        }
        return info;
    }

    /**
     * @see org.eclipse.jface.text.IDocumentListener#documentAboutToBeChanged(org.eclipse.jface.text.DocumentEvent)
     */
    @Override
    public void documentAboutToBeChanged(DocumentEvent event) {
    }

    /**
     * @see org.eclipse.jface.text.IDocumentListener#documentChanged(org.eclipse.jface.text.DocumentEvent)
     */
    @Override
    public void documentChanged(DocumentEvent event) {
        hasChanged = true;
        // Fire a SVG Dirty event
        theController.processEvent(new SVGDirtyEvent(this));
    }

    /**
     * Check to see if the document has changed
     * 
     * @return true if the document has changed since the last
     * {@code resetHasChanged}
     * @since 2.4
     */
    public boolean hasChanged() {
        return hasChanged;
    }

    /**
     * Set the has changed flag back to false
     * 
     * @since 2.4
     */
    public void resetHasChanged() {
        hasChanged = false;
    }
}