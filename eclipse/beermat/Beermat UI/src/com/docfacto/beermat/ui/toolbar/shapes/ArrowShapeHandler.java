package com.docfacto.beermat.ui.toolbar.shapes;

import static com.docfacto.beermat.plugin.PluginConstants.ARROW_ORANGE;
import static com.docfacto.beermat.plugin.PluginConstants.BI_ARROW_ORANGE;
import static com.docfacto.beermat.plugin.PluginConstants.ICON_FOLDER;

import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.utils.ElementUtils;


/**
 * A handler for a single headed arrow shape.
 *
 * @author kporter - created Jun 24, 2013
 * @since 2.4
 */
public class ArrowShapeHandler extends LineShapeHandler  {

    private boolean isBi;
    
    /**
     * Constructor.
     * @param controller
     */
    public ArrowShapeHandler(BeermatController controller, boolean biDirectional){
        super(controller);
        isBi = biDirectional;
    }
    
    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        if(isBi){
            return "bi-arrow";
        }
        else{
            return "arrow";            
        }
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        if(isBi){
            return "Bi-Directional Arrow Tool";
        }
        else{
            return "Arrow Tool";            
        }
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getShortcutKey()
     */
    @Override
    public char getShortcutKey() {
        return 0;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#createShape(org.w3c.dom.svg.SVGDocument, org.eclipse.swt.graphics.Point)
     */
    @Override
    public Element createShape(SVGDocument document,Point startPoint) {
        Element line = super.createShape(document,startPoint);
        ElementUtils.addMarkers(document,line,isBi);
        return line;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        if(isBi){
            return ICON_FOLDER+BI_ARROW_ORANGE;
        }
        else{
            return ICON_FOLDER+ARROW_ORANGE;            
        }
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.LineShapeHandler#mouseClick(org.eclipse.swt.graphics.Point, org.w3c.dom.Element)
     */
    @Override
    public Element mouseClick(Point location,Element lastElement) {
        Element arrow = super.mouseClick(location,lastElement);
        ElementUtils.addMarkers(getController().getManager().getDocument(),arrow,isBi);
        return arrow;
    }
    
    

}
