package com.docfacto.beermat.ui.widgets;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.w3c.dom.Element;

import com.docfacto.beermat.events.ControlToolbarChangeEvent;
import com.docfacto.beermat.ui.AbstractControlToolBar;
import com.docfacto.beermat.ui.BeermatViewConstants;
import com.docfacto.common.NameValuePair;

/**
 * Abstract Control Tool Bar widget.
 * 
 * Takes care of all of the commonality of Control Widgets
 * 
 * This will create a widget composite (parent) and set a row layout as well.
 * 
 * @author dhudson - created 18 Jun 2013
 * @since 2.4
 */
public abstract class AbstractControlBarWidget {

    private final Composite theParent;
    private final AbstractControlToolBar theToolBar;

    /**
     * Static empty list
     */
    static final List<NameValuePair> EMPTY_LIST = new ArrayList<NameValuePair>(
        0);

    /**
     * Constructor.
     * 
     * @param toolbar control tool bar
     * @since 2.4
     */
    public AbstractControlBarWidget(AbstractControlToolBar toolbar) {
        theToolBar = toolbar;
        theParent = new Composite(toolbar,SWT.NONE);
        theParent.setBackground(toolbar.getBackground());

        // setDefaultRowLayout();
        setDefaultGridLayout();
        layoutWidget();
    }

    /**
     * Set a RowLayout to the parent component
     * 
     * @since 2.4
     */
    @SuppressWarnings("unused")
    private void setDefaultRowLayout() {
        RowLayout rowLayout = new RowLayout(SWT.HORIZONTAL);
        rowLayout.marginHeight = 0;
        rowLayout.marginWidth = 1;
        rowLayout.spacing = 2;
        theParent.setLayout(rowLayout);
    }

    /**
     * Set a GridLayout to the parent component
     * 
     * @since 2.4.3
     */
    private void setDefaultGridLayout() {
        GridLayout gridLayout = new GridLayout(1,false);
        gridLayout.marginWidth = 1;
        gridLayout.marginHeight = 0;
        gridLayout.verticalSpacing = 0;
        gridLayout.horizontalSpacing = 2;
        theParent.setLayout(gridLayout);
    }

    /**
     * Gets a default GridData that can then be set to an SWT Control.
     * 
     * @param increaseColumns Whether to increase the columns allowed, true if
     * adding columns.
     * @return A default GridData that can be set to a
     * org.eclipse.swt.widgets.Control.
     * @since 2.4.3
     * @see #increaseGridColumns()
     * @see GridData
     * @see GridLayout
     */
    public GridData getGridData(boolean increaseColumns) {
        if (increaseColumns) {
            increaseGridColumns();
        }
        return new GridData(SWT.CENTER,SWT.CENTER,false,false,1,1);
    }

    /**
     * Increase the columns the layout contains. This is necessary if more
     * columns need to be added to the composite. {@docfacto.system Only call
     * this if the layout set was a grid layout!}
     * 
     * @since 2.4.3
     * @see GridLayout
     */
    public void increaseGridColumns() {
        GridLayout layout = (GridLayout)theParent.getLayout();
        layout.numColumns++;
    }

    /**
     * Create a new label with the parent set
     * 
     * @return a newly created label
     * @since 2.4
     */
    public Label createLabel() {
        Label label = new Label(theParent,SWT.NONE);
        return label;
    }

    /**
     * Create a new label, with the parent set.
     * 
     * Set the foreground colour and set the text.
     * 
     * @param text to set
     * @return a newly created label, with text set
     * @since 2.4
     */
    public Label createLabel(String text) {
        Label label = createLabel();
        label.setText(text);
        label.setForeground(BeermatViewConstants.COLOR_FOREGROUND_BRAND);
        return label;
    }

    /**
     * Create a new label and set its size
     * 
     * @param text to set
     * @param size to set
     * @return a newly created label, with text and a size set
     * @since 2.4
     * @see #createLabel(String)
     * @deprecated Instead of using this, get a layoutdata and set a
     * width/height hint.
     */
    @Deprecated
    public Label createLabel(String text,int size) {
        Label label = createLabel(text);
        // RowData data = new RowData();
        // data.width = size;
        // label.setLayoutData(data);
        return label;
    }

    /**
     * Return the Control toolbar
     * 
     * @return the toolbar
     * @since 2.4
     */
    public AbstractControlToolBar getToolbar() {
        return theToolBar;
    }

    /**
     * Return the widget component
     * 
     * @return the widget component
     * @since 2.4
     */
    public Composite getParent() {
        return theParent;
    }

    /**
     * Helper method to fire a control toolbar change event
     * 
     * @since 2.4
     */
    public void fireControlToolbarChangeEvent() {
        theToolBar.getController().processEvent(
            new ControlToolbarChangeEvent(this));
    }

    /**
     * Called from the super method.
     * 
     * {@docfacto.note this gets called before the sub class constructor is
     * completed}
     * 
     * @since 2.4
     */
    protected abstract void layoutWidget();

    /**
     * Get the attributes set by the widget
     * 
     * @param element to get the attributes for
     * @return List of attributes to set on the element, which could be empty
     * @since 2.3
     */
    protected abstract List<NameValuePair> getAttributesForElement(Element element);
}
