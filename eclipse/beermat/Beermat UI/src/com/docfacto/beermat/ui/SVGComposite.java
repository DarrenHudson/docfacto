package com.docfacto.beermat.ui;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import javax.swing.JRootPane;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.dialogs.AbstractTextEntryDialog;
import com.docfacto.beermat.dialogs.CSSBrowserDialog;
import com.docfacto.beermat.dialogs.ClassAttributeDialog;
import com.docfacto.beermat.dialogs.FilterAttributeDialog;
import com.docfacto.beermat.dialogs.ImageBrowserDialog;
import com.docfacto.beermat.dialogs.SingleTextEntryDialog;
import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.beermat.svg.BeermatUserAgent;
import com.docfacto.common.Platform;

/**
 * A SWT widget that displays a Batik JSVGCanvas
 * <p>
 * This widget creates the necessary SWT to AWT Bridge frame, and sets the Batik
 * JSVGCanvas to the frame.
 * </p>
 * 
 * {@docfacto.media uri="doc-files/SVGComposite.svg"}
 * 
 * @author kporter - created Jan 18, 2013
 * @since 2.1
 * @docfacto.link uri="doc-files/SVGComposite.svg" version="1.1"
 * key="SVGComposite"
 */
public class SVGComposite extends Composite {

    private final BeermatAWTCanvas theSVGCanvas;
    private final Frame theFrame;

    private final AbstractTextEntryDialog theTextDialog;
    private final ImageBrowserDialog theImageDialog;
    private final CSSBrowserDialog theCSSDialog;
    private final ClassAttributeDialog theClassDialog;
    private final FilterAttributeDialog theFilterDialog;
    
    private final AbstractCursorManager theCursorManager;

    /**
     * Constructor to create a new SVGWidget.
     * 
     * @param controller beermat controller
     * @param parent The parent that this composite will be a child of, SWT
     * required.
     * @since 2.1
     */
    public SVGComposite(BeermatController controller,Composite parent) {
        super(parent,SWT.EMBEDDED);

        parent.setLayout(new FillLayout());

        theFrame = SWT_AWT.new_Frame(this);

        theFrame.addWindowListener(new java.awt.event.WindowAdapter() {            
            /**
             * @see java.awt.event.WindowAdapter#windowOpened(java.awt.event.WindowEvent)
             */
            @Override
            public void windowOpened(WindowEvent e) {
                WindowEvent spoof = new WindowEvent(theFrame, WindowEvent.WINDOW_ACTIVATED);
                theFrame.dispatchEvent(spoof);
            }
        });

        JRootPane rootPane = new JRootPane();

        BeermatUserAgent userAgent = new BeermatUserAgent(rootPane);

        theSVGCanvas = new BeermatAWTCanvas(controller,userAgent);

        java.awt.Container contentPane = rootPane.getContentPane();
        contentPane.setLayout(new BorderLayout());

        BeermatScrollPane scrollPane =
            new BeermatScrollPane(controller,theSVGCanvas);

        // scrollPane.setBackground(Color.black);
        scrollPane.setOpaque(false);

        // theSVGCanvas.setRecenterOnResize(true);

        contentPane.add(scrollPane);

        theFrame.add(rootPane);
        theSVGCanvas.requestFocusInWindow();
        
        setEnabled(true);

        // Set up the cursor manager
        if (Platform.isMac()) {
            theCursorManager = new SWTCursorManager(controller,this);
        }
        else {
            theCursorManager = new AWTCursorManager(controller,theSVGCanvas);
        }

        theCursorManager.loadCursors();

        // Start with the hand, as the cursor selector palette item is selected
        theCursorManager.handleCursorChange(AbstractCursorManager.HAND);

        // theTextDialog = new MultiTextEntryDialog(getShell(),controller);
        // TODO: Make this a preference
        theTextDialog = new SingleTextEntryDialog(getShell(),controller);

        theImageDialog = new ImageBrowserDialog(getShell());

        theCSSDialog = new CSSBrowserDialog(getShell());
        
        theClassDialog = new ClassAttributeDialog(getShell());
        
        theFilterDialog = new FilterAttributeDialog(getShell());
        
        // setFocus();
    }

    public void setSvg(URL url) {
        theSVGCanvas.loadSVGDocument(url.toExternalForm());
    }

    public void setURI(URI uri) {
        theSVGCanvas.setURI(uri.toString());
    }

    public void setDocument(SVGDocument svgDoc) {
        theSVGCanvas.setDocument(svgDoc);
    }

    public void setSVGFile(IFile file) {
        theSVGCanvas.loadSVGDocument(file.getLocation().toFile().toURI()
            .toString());
    }

    /**
     * Gets the Document represented by this canvas.
     * 
     * @return The Document shown in this canvas.
     * @since 2.1
     */
    public SVGDocument getSVGDocument() {
        return theSVGCanvas.getSVGDocument();
    }

    /**
     * Return the SVGCanvas
     * 
     * @return the SVG Canvas
     * @since 2.4
     */
    public BeermatAWTCanvas getBeermatAWTCanvas() {
        return theSVGCanvas;
    }

    /**
     * Set the SVGPath to load
     * 
     * @param path to file
     * @since 2.1
     */
    public void setSVGPath(IPath path) {
        try {
            URL url = path.toFile().toURI().toURL();
            setSvg(url);
        }
        catch (MalformedURLException ex) {
            BeermatUIPlugin.logException("SVGComposite setSVGPath ",ex);
        }
    }

    /**
     * @see org.eclipse.swt.widgets.Widget#dispose()
     */
    @Override
    public void dispose() {
        theCursorManager.dispose();
        super.dispose();
    }

    /**
     * Returns a Text Entry Dialog.
     * 
     * @return a Text Entry Dialog
     * @since 2.4
     */
    public AbstractTextEntryDialog getTextDialog() {
        return theTextDialog;
    }

    /**
     * Returns the {@link ImageBrowserDialog}
     * 
     * @return the image browser dialog
     * @since 2.4
     */
    public ImageBrowserDialog getImageDialog() {
        return theImageDialog;
    }

    /**
     * Returns the CSS Browser Dialog
     * 
     * @return the CSS Browser Dialog
     * @since 2.4
     */
    public CSSBrowserDialog getCSSDialog() {
        return theCSSDialog;
    }

    /**
     * Returns the Class Attribute Dialog
     * 
     * @return the class attribute dialog
     * @since 2.5
     */
    public ClassAttributeDialog getClassDialog() {
        return theClassDialog;
    }
    
    /**
     * Returns the Filter Attribute Dialog
     * 
     * @return the filter attribute dialog
     * @since 2.5
     */
    public FilterAttributeDialog getFilterDialog() {
        return theFilterDialog;
    }
}
