package com.docfacto.beermat.ui.toolbar;

import static com.docfacto.beermat.plugin.PluginConstants.*;

import org.eclipse.swt.graphics.Image;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.updaters.LayerUpdater;
import com.docfacto.beermat.utils.ElementMovement;
import com.docfacto.beermat.utils.PluginManager;

/**
 * A palette item to display the bring to front or bring to back item
 * 
 * @author kporter - created Jul 29, 2013
 * @since 2.4.2
 * @docfacto.link uri="${doc-beermat}/controlbar/c_bring_backFront_palette_item.dita" key="layer-action" version="2.4.2"
 */
public class LayerUpDownPaletteItem extends AbstractPaletteItem {

    private final ElementMovement theDirection;

    /**
     * Create a new Layer up or Layer down palette item. A style of
     * 
     * @param controller The controller.
     * @param direction The direction of movement. BACKWARD or FORWARD
     * @see com.docfacto.beermat.utils.ElementMovement.FORWARD
     * @see com.docfacto.beermat.utils.ElementMovement.BACKWARD
     * @since 2.4.2
     */
    public LayerUpDownPaletteItem(BeermatController controller,
    ElementMovement direction) {
        super(controller);
        theDirection = direction;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        switch (theDirection) {
        case BACKWARD:
            return "Bring Backwards";
        case FORWARD:
            return "Bring Forwards";
        default:
            return "missing tool";
        }
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
     */
    @Override
    public Image getImage() {
        return PluginManager.getImage(getImagePath());
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        switch (theDirection) {
        case BACKWARD:
            return ICON_FOLDER+LAYER_UP_ORANGE;
        case FORWARD:
            return ICON_FOLDER+LAYER_DOWN_ORANGE;
        default:
            return ICON_FOLDER+ERROR;
        }
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        switch (theDirection) {
        case BACKWARD:
            return "Bring-to-Back Tool";
        case FORWARD:
            return "Bring-to-Front Tool";
        default:
            return "missing tool";
        }

    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.AbstractPaletteItem#performAction()
     */
    @Override
    public void performAction() {
        new LayerUpdater(getController(),theDirection);
    }

}
