package com.docfacto.beermat.ui.widgets;

import static com.docfacto.beermat.ui.BeermatViewConstants.ICON_SIZE;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.docfacto.beermat.ui.BeermatViewConstants;
import com.docfacto.beermat.ui.BeermatViewConstants.ToolbarPopup;
import com.docfacto.beermat.ui.toolbar.PaletteItem;
import com.docfacto.beermat.ui.toolbar.PaletteItemGrouper;

/**
 * Displays the icons in rows
 * 
 * {@docfacto.todo The open creates a new shell every time and it shouldn't}
 * 
 * @author kporter - created Jul 5, 2013
 * @since 2.4.2
 */
public class GroupedItemWidget {

    private Shell theShell;
    private final Composite theParentToolbar;
    private final PaletteItemGrouper theGrouper;
    private ToolbarPopup thePopupDirection;

    /**
     * Spacing between each icon
     */
    private static final int SPACING = 5;

    private static final int MAX_SHELL_SIZE = (ICON_SIZE+SPACING)*10;

    /**
     * Create the widget
     * 
     * @param parent The parent Composite
     * @param g The PaletteItemGrouper associated with this
     * @since 2.4
     */
    public GroupedItemWidget(Composite parent,PaletteItemGrouper g) {
        theParentToolbar = parent;
        theGrouper = g;
        thePopupDirection = ToolbarPopup.VERTICAL;
    }

    /**
     * Gets the shell
     * 
     * @return The Shell
     * @since 2.4
     */
    public Shell getShell() {
        return theShell;
    }

    /**
     * Opens the shell and displays.
     * 
     * @since 2.4.2
     */
    public void open() {
        theShell = new Shell(theParentToolbar.getDisplay(),SWT.NO_TRIM);
        theShell.setBackground(theParentToolbar.getBackground());

        RowLayout rowLayout = new RowLayout(SWT.HORIZONTAL);
        rowLayout.pack = false;
        rowLayout.fill = true;
        rowLayout.wrap = true;
        rowLayout.marginTop = 3;
        rowLayout.marginBottom = 3;
        rowLayout.marginLeft = 3;
        rowLayout.marginRight = 3;
        rowLayout.spacing = SPACING;
        rowLayout.center = true;

        theShell.setLayout(rowLayout);

        ArrayList<PaletteItem> items = theGrouper.getItems();
        for (final PaletteItem item:items) {
            // Don't show the selected one, as that is the group icon
            if (item!=theGrouper.getSelectedItem()) {
                Label label = new Label(theShell,SWT.NONE);
                label.setBackground(theParentToolbar.getBackground());
                label.setImage(item.getImage());
                label.setToolTipText(item.getTooltipText());
                label.addMouseListener(new MouseAdapter() {
                    /**
                     * @see org.eclipse.swt.events.MouseAdapter#mouseDown(org.eclipse.swt.events.MouseEvent)
                     */
                    @Override
                    public void mouseDown(MouseEvent e) {
                        theGrouper.setActivePaletteItem(item,true);
                        theShell.close();
                    }
                });
            }
        }

        theShell.addFocusListener(new FocusAdapter() {
            /**
             * @see org.eclipse.swt.events.FocusListener#focusLost(org.eclipse.swt.events.FocusEvent)
             */
            @Override
            public void focusLost(FocusEvent arg0) {
                theShell.close();
            }
        });

        theShell.addKeyListener(new KeyAdapter() {
            /**
             * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
             */
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.keyCode==SWT.ESC) {
                    theShell.close();
                }
            }
        });

        // One is displayed in the tool bar
        int size = items.size()-1;
        int iconSize = ICON_SIZE+SPACING;
        int rows = size/10;
        
        if(size % 10 != 0) {
            rows++;
        }
       
        int width =
            items.size()<10 ? (size*iconSize)+1
                : MAX_SHELL_SIZE;
        
        // Top and bottom padding
        int height = rows*iconSize+6;

        theShell.setSize(width,height);

        Rectangle rect = theGrouper.getBounds();

        Point pt =
            theParentToolbar.toDisplay(new Point(rect.x,rect.y));
        if (thePopupDirection==ToolbarPopup.HORIZONTAL) {
            theShell.setLocation(pt.x+(BeermatViewConstants.TOOLBAR_WIDTH),pt.y);
        }
        else {
            theShell.setLocation(pt.x,pt.y+(BeermatViewConstants.TOOLBAR_HEIGHT));
        }
        theShell.setVisible(true);
        theShell.open();
    }

    /**
     * Sets the direction of this popup.
     * 
     * @param toolbarDirection Whether this is a vertical dropping popup or
     * horizontal
     * @since 2.4.5
     */
    public void setPopupDirection(ToolbarPopup toolbarDirection) {
        thePopupDirection = toolbarDirection;
    }

}
