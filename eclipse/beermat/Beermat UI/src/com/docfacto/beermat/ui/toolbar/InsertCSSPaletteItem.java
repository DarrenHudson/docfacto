package com.docfacto.beermat.ui.toolbar;

import static com.docfacto.beermat.plugin.PluginConstants.ICON_FOLDER;

import java.io.File;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.graphics.Image;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.dialogs.CSSBrowserDialog;
import com.docfacto.beermat.updaters.CSSUpdater;
import com.docfacto.beermat.utils.PluginManager;
import com.docfacto.common.IOUtils;

/**
 * Palette Item to insert a CSS
 * 
 * @author dhudson - created 10 Sep 2013
 * @since 2.4
 */
public class InsertCSSPaletteItem extends AbstractPaletteItem {

    /**
     * Constructor.
     * 
     * @param controller The controller
     * @since 2.4.2
     */
    public InsertCSSPaletteItem(BeermatController controller) {
        super(controller);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        return "Insert CSS";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
     */
    @Override
    public Image getImage() {
        return PluginManager.getImage(getImagePath());
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        return ICON_FOLDER+"css.png";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        return "Insert CSS";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.AbstractPaletteItem#performAction()
     */
    @Override
    public void performAction() {

        final CSSBrowserDialog dialog =
            getController().getView().getSVGComposite().getCSSDialog();

        File file = getController().getMultiPageEditor().getFile();
        dialog.setBasePath(file.getParentFile().getAbsolutePath());

        if (dialog.open()==Window.OK) {
            if (dialog.getPath()==null||dialog.getPath().isEmpty()) {
                // Nothing to do
                return;
            }

            String relative =
                IOUtils.getRelativePathForFiles(
                    getController().getMultiPageEditor().getFile()
                        .getAbsolutePath(),dialog.getPath());

            new CSSUpdater(getController(),relative);
        }
    }

}
