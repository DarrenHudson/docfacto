package com.docfacto.beermat.ui.widgets.listeners;

import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

import com.docfacto.beermat.ui.BeermatViewConstants;

/**
 * On hover over a label, highlights it with the complementary colour.
 * 
 * If this is added to a label, create with default constructor.
 * Elsewise create with given label to colour on component hover.
 * 
 * @author kporter - created Oct 15, 2013
 * @since 2.4.7
 */
public class ColourLabelOnMouseOver implements MouseTrackListener {

    private Label theLabel;
    
    private Color mouseOffColor;
    private Color mouseOnColor;
    
    public ColourLabelOnMouseOver(){
        theLabel = null;
        mouseOffColor = BeermatViewConstants.COLOR_FOREGROUND_BRAND;
        mouseOnColor = BeermatViewConstants.COLOR_FOREGROUND_COMPLEMENTARY;
    }
    
    public ColourLabelOnMouseOver(Color mouseOff, Color mouseOn){
        theLabel = null;
        mouseOffColor = mouseOff;
        mouseOnColor = mouseOn;
    }
    /**
     * Constructor.
     * @param label the label that will change colour
     */
    public ColourLabelOnMouseOver(Label label) {
        theLabel = label;
    }
    
    public ColourLabelOnMouseOver(Label label, Color mouseOff, Color mouseOn){
        theLabel = label;
        mouseOffColor = mouseOff;
        mouseOnColor = mouseOn;
    }

    /**
     * @see org.eclipse.swt.events.MouseTrackListener#mouseEnter(org.eclipse.swt.events.MouseEvent)
     */
    @Override
    public void mouseEnter(MouseEvent e) {
        try{
            Label label = theLabel==null ? (Label) e.widget : theLabel;
            label.setForeground(mouseOnColor);            
        }catch(ClassCastException exp){
            for(Control control: ((Composite)e.widget).getChildren()){
                control.setForeground(mouseOnColor);
            }
        }
    }

    /**
     * @see org.eclipse.swt.events.MouseTrackListener#mouseExit(org.eclipse.swt.events.MouseEvent)
     */
    @Override
    public void mouseExit(MouseEvent e) {
        try{
            Label label = theLabel==null ? (Label) e.widget : theLabel;
            label.setForeground(mouseOffColor);            
        }catch(ClassCastException exp){
            for(Control control: ((Composite)e.widget).getChildren()){
                control.setForeground(mouseOffColor);
            }
        }
    }

    /**
     * @see org.eclipse.swt.events.MouseTrackListener#mouseHover(org.eclipse.swt.events.MouseEvent)
     */
    @Override
    public void mouseHover(MouseEvent e) {
    }

}
