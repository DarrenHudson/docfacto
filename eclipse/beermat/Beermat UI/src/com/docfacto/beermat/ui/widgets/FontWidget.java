package com.docfacto.beermat.ui.widgets;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.FontDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.w3c.dom.Element;

import com.docfacto.beermat.events.AttributeChangeEvent;
import com.docfacto.beermat.ui.ControlToolBar;
import com.docfacto.beermat.ui.widgets.listeners.ColourLabelOnMouseOver;
import com.docfacto.common.NameValuePair;
import com.docfacto.core.swt.DefaultMouseListener;

/**
 * Allow for the selection of Text, Style and Size
 * 
 * {@docfacto.todo We can do style as well } {@docfacto.todo Make the dialog fit our style scheme, or make a completely
 * new dialog which is ours} The widget {@docfacto.media uri="doc-files/font-widget.png"}
 * 
 * @author dhudson - created 18 Jun 2013
 * @since 2.4
 * @docfacto.link uri="${doc-beermat}/controlbar/c_font_widget.dita" key="font-widget" version="2.4.2"
 */
public class FontWidget extends AbstractControlBarWidget {
    private static final String FONT_FAMILY_ATTRIBUTE = "font-family";
    private static final String FONT_SIZE_ATTRIBUTE = "font-size";
    private static final String FONT_RGB = "fill";

    private Font theSelectedFont;
    private int theFontSize;
    private RGB theRGB;

    private Label theFontNameLabel;
    private Spinner theSpinner;

    private boolean hasSizeChanged;
    private boolean hasFontChanged;

    /**
     * Constructor.
     * 
     * @param toolbar control toolbar
     * @since 2.4
     */
    public FontWidget(ControlToolBar toolbar) {
        super(toolbar);
    }

    /**
     * @see com.docfacto.beermat.ui.widgets.AbstractControlBarWidget#layoutWidget()
     */
    @Override
    protected void layoutWidget() {
        getParent().addMouseTrackListener(new ColourLabelOnMouseOver());
        getParent().addMouseListener(new FontLabelClick());

        final Label label = createLabel("Font :");
        label.setEnabled(false);
        label.setLayoutData(getGridData(true));
        label.addMouseListener(new FontLabelClick());
        // Set up the default font
        theSelectedFont = label.getFont();
        theFontSize = theSelectedFont.getFontData()[0].getHeight();

        GridData gd_fontName = getGridData(true);
        theFontNameLabel = createLabel("Default"); // getFontName());
        theFontNameLabel.setLayoutData(gd_fontName);
        theFontNameLabel.addMouseListener(new FontLabelClick());
        theFontNameLabel.setEnabled(false);

        theSpinner = new Spinner(getParent(),SWT.NONE);
        theSpinner.setLayoutData(getGridData(true));
        theSpinner.setMinimum(1);
        theSpinner.setMaximum(64);
        theSpinner.setSelection(12); // getFontSize());
        theSpinner.setIncrement(1);
        theSpinner.addSelectionListener(new SelectionListener() {
            /**
             * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
             */
            @Override
            public void widgetDefaultSelected(SelectionEvent event) {
                theFontSize = theSpinner.getSelection();
                hasSizeChanged = true;
                fireFontChangeEvent();
            }

            /**
             * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent event) {
                theFontSize = theSpinner.getSelection();
                hasSizeChanged = true;
                fireFontChangeEvent();
            }
        });

    }

    /**
     * Fire a font change event
     * 
     * @since 2.4
     */
    private void fireFontChangeEvent() {
        getToolbar().getController().processEvent(
            new AttributeChangeEvent(this,true));
    }

    /**
     * Helper class to get the font name
     * 
     * @return the font name
     * @since 2.4
     */
    private String getFontName() {
        return theSelectedFont.getFontData()[0].getName();
    }

    /**
     * Helper class to get the font size
     * 
     * @return the font size
     * @since 2.4
     */
    private int getFontSize() {
        return theFontSize;
    }

    public List<NameValuePair> getAttributesForElement() {
        List<NameValuePair> attrList = new ArrayList<NameValuePair>(5);

        if (hasSizeChanged) {
            attrList.add(new NameValuePair(FONT_SIZE_ATTRIBUTE,Integer.toString(getFontSize())));
        }

        if (hasFontChanged) {
            attrList.add(new NameValuePair(FONT_FAMILY_ATTRIBUTE,getFontName()));

            int style = theSelectedFont.getFontData()[0].getStyle();
            if ((style&SWT.ITALIC)==SWT.ITALIC) {
                attrList.add(new NameValuePair("font-style","italic"));
            }

            if ((style&SWT.BOLD)==SWT.BOLD) {
                attrList.add(new NameValuePair("font-weight","bold"));
            }
        }
        if (theRGB!=null) {
            String colour = String.format("#%02x%02x%02x",theRGB.red,theRGB.green,theRGB.blue);
            attrList.add(new NameValuePair(FONT_RGB,colour));
        }
        return attrList;
    }

    /**
     * @see com.docfacto.beermat.ui.widgets.AbstractControlBarWidget#getAttributesForElement(org.w3c.dom.Element)
     */
    @Override
    public List<NameValuePair> getAttributesForElement(Element element) {
        return getAttributesForElement();
    }

    private class FontLabelClick extends DefaultMouseListener {
        /**
         * @see org.eclipse.swt.events.MouseListener#mouseDown(org.eclipse.swt.events.MouseEvent)
         */
        @Override
        public void mouseDown(MouseEvent event) {
            FontDialog dialog = new FontDialog(getParent().getShell(),SWT.NONE);
            dialog.setEffectsVisible(true);
            dialog.open();

            theRGB = dialog.getRGB();
            theSelectedFont = new Font(getParent().getDisplay(),dialog.getFontList());
            theFontSize = theSelectedFont.getFontData()[0].getHeight();

            // update the name
            theFontNameLabel.setText(getFontName());

            hasFontChanged = true;

            if (theSpinner.getSelection()!=getFontSize()) {
                hasSizeChanged = true;
                // Update the size
                theSpinner.setSelection(getFontSize());
            }

            fireFontChangeEvent();
        }
    }
}
