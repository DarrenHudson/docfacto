package com.docfacto.beermat.ui.widgets;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.dialogs.GridCustomiseDialog;
import com.docfacto.beermat.events.ColourChangeEvent;
import com.docfacto.beermat.events.GridEvent;
import com.docfacto.beermat.events.GridSizeEvent;
import com.docfacto.beermat.ui.AbstractControlToolBar;
import com.docfacto.beermat.ui.widgets.listeners.ColourLabelOnMouseOver;
import com.docfacto.common.NameValuePair;
import com.docfacto.core.swt.DefaultSelectionListener;

/**
 * Grid Widget current handles the displaying of the grid.
 * 
 * In future should allow for changing of grid size as well as snap to grid
 * 
 * @author dhudson - created 24 Jun 2013
 * @since 2.4
 * @docfacto.link uri="${doc-beermat}/controlbar/c_grid_widget.dita"
 * key="grid-widget" version="2.4.2"
 */
public class GridWidget extends AbstractControlBarWidget {

    private GridCustomiseDialog theDialog;
    private BeermatController theController;

    /**
     * Constructor.
     * 
     * @param toolBar control toolbar
     * @since 2.4
     */
    public GridWidget(AbstractControlToolBar toolBar) {
        super(toolBar);
        theController = toolBar.getController();
        theDialog = new GridCustomiseDialog(toolBar.getShell());
    }

    /**
     * @see com.docfacto.beermat.ui.widgets.AbstractControlBarWidget#layoutWidget()
     */
    @Override
    protected void layoutWidget() {

        // Need a separate label as windows doesn't honour the text background
        // colour
        Label label = createLabel("Grid");
        label.setLayoutData(getGridData(true));
        label.addMouseTrackListener(new ColourLabelOnMouseOver());

        label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseDown(MouseEvent e) {
                int size = (int)theController.getBeermatAWTCanvas().getGridSize();
                theDialog.setGridSize(size);
                Color gridColour = theController.getBeermatAWTCanvas().getGridColor();
                theDialog.setColour(gridColour);

                if (theDialog.open()==Dialog.OK) {
                    if (size!=theDialog.getGridSize()) {
                        theController.processEvent(new GridSizeEvent(this,theDialog.getGridSize()));
                    }

                    Color c = theDialog.getColour();
                    // If c is not set, or c is the not same as the grid colour
                    // (unchanged)
                    if (c!=null&&c!=gridColour) {
                        theController.processEvent(new ColourChangeEvent(this,c));
                    }
                }
            }
        });

        final Button button = new Button(getParent(),SWT.CHECK|SWT.FLAT);
        button.setLayoutData(getGridData(true));

        button.addSelectionListener(new DefaultSelectionListener() {
            /**
             * @see com.docfacto.core.swt.DefaultSelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent event) {
                getToolbar().getController().processEvent(
                    new GridEvent(this,button.getSelection()));
            }

        });
    }

    /**
     * @see com.docfacto.beermat.ui.widgets.AbstractControlBarWidget#getAttributesForElement(org.w3c.dom.Element)
     */
    @Override
    protected List<NameValuePair> getAttributesForElement(Element element) {
        return EMPTY_LIST;
    }

}
