package com.docfacto.beermat.ui.toolbar;

import static com.docfacto.beermat.plugin.PluginConstants.ICON_FOLDER;

import java.awt.geom.Rectangle2D;
import java.io.File;

import org.apache.batik.gvt.GraphicsNode;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.dialogs.ImageBrowserDialog;
import com.docfacto.beermat.updaters.AppendElementUpdater;
import com.docfacto.beermat.utils.PluginManager;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.IOUtils;

/**
 * Inserts an image to the canvas
 * 
 * @author kporter - created Jul 22, 2013
 * @since 2.4.2
 */
public class InsertImagePaletteItem extends AbstractPaletteItem {
    /**
     * Constructor.
     * 
     * @param controller The controller
     * @since 2.4.2
     */
    public InsertImagePaletteItem(BeermatController controller) {
        super(controller);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        return "Insert Image";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
     */
    @Override
    public Image getImage() {
        return PluginManager.getImage(getImagePath());
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        return ICON_FOLDER+"frame.png";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        return "Insert an image in to the SVG";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.AbstractPaletteItem#performAction()
     */
    @Override
    public void performAction() {

        final ImageBrowserDialog dialog =
            getController().getView().getSVGComposite().getImageDialog();
        if (getController().getBeermatAWTCanvas().getHighlightManager()
            .getHighlightedElement()!=null) {
            dialog.setElementSelected(true);
        }

        else {
            dialog.setElementSelected(false);
        }

        File file = getController().getMultiPageEditor().getFile();
        dialog.setBasePath(file.getParentFile().getAbsolutePath());

        if (dialog.open()==Window.OK) {
            if (dialog.getPath()==null||dialog.getPath().isEmpty()) {
                // Nothing to do
                return;
            }

            String path = dialog.getPath();
            String extension = path.substring(path.lastIndexOf('.'));

            if (extension.equalsIgnoreCase(".svg")||
                extension.equalsIgnoreCase("svg")) {
                addSVG(path,dialog.getLocation(),dialog.getSize());
            }
            else {
                switch (dialog.getSizeInt()) {
                case ImageBrowserDialog.SIZE_ACTUAL:
                    addImage(dialog.getPath(),dialog.getLocation(),null);
                    break;
                case ImageBrowserDialog.SIZE_FIT:
                    GraphicsNode n =
                        getController()
                            .getBeermatAWTCanvas().getGraphicsNodeFor(
                                getController().getBeermatAWTCanvas()
                                    .getHighlightManager()
                                    .getHighlightedElement()
                                    .getHighlightedElement());
                    Rectangle2D bounds = n.getBounds();

                    addImage(dialog.getPath(),new Point((int)bounds.getX(),
                        (int)bounds.getY()),new Point((int)bounds.getWidth(),
                        (int)bounds.getHeight()));
                    break;
                case ImageBrowserDialog.SIZE_CUSTOM:
                    addImage(dialog.getPath(),dialog.getLocation(),
                        dialog.getSize());
                    break;
                }
            }
        }
    }

    /**
     * Inserts an svg in to the canvas
     * 
     * @param path The path of the SVG
     * @param location The x and y to set
     * @param size The size to set the image
     * @since 2.4.3
     */
    private void addSVG(String path,Point location,Point size) {
        if (size!=null) {
            String relative =
                IOUtils.getRelativePathForFiles(
                    getController().getMultiPageEditor().getFile()
                        .getAbsolutePath(),path);

            final Document document =
                getController().getManager().getDocument();

            final Element element =
                document.createElementNS(SVGUtils.SVG_NAMESPACE,
                    "image");
            element.setAttributeNS("http://www.w3.org/1999/xlink",
                "xlink:href",relative);

            SVGUtils.setAttribute(element,"x",location.x);
            SVGUtils.setAttribute(element,"y",location.y);
            SVGUtils.setAttribute(element,"width",size.x);
            SVGUtils.setAttribute(element,"height",size.y);

            // Add the element via the updater
            new AppendElementUpdater(getController(),element);
        }
    }

    /**
     * Adds an image to the SVG
     * 
     * @param path of the image to add
     * @since 2.4
     */
    private void addImage(String path,Point location,Point size) {
        Image image = new Image(Display.getDefault(),path);
        if (image!=null) {
            String relative =
                IOUtils.getRelativePathForFiles(
                    getController().getMultiPageEditor().getFile()
                        .getAbsolutePath(),path);

            final Document document =
                getController().getManager().getDocument();

            final Element element =
                document.createElementNS(SVGUtils.SVG_NAMESPACE,
                    "image");
            element.setAttributeNS("http://www.w3.org/1999/xlink",
                "xlink:href",relative);
            SVGUtils.setAttribute(element,"x",location.x);
            SVGUtils.setAttribute(element,"y",location.y);
            if (size!=null) {
                element.setAttribute("width",size.x+"");
                element.setAttribute("height",size.y+"");
            }
            else {
                SVGUtils.setAttribute(element,"width",
                    image.getImageData().width);
                SVGUtils.setAttribute(element,"height",
                    image.getImageData().height);
            }

            new AppendElementUpdater(getController(),element);
        }
    }
}
