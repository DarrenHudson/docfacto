package com.docfacto.beermat.ui.toolbar.shapes;

import java.awt.geom.Point2D;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.generated.SvgDef;
import com.docfacto.beermat.svg.TransformElementParser;
import com.docfacto.beermat.ui.toolbar.PaletteItemGrouper;
import com.docfacto.beermat.utils.PluginManager;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.core.utils.MathUtils;

/**
 * A handler for custom path shapes.
 * 
 * @author kporter - created Jun 24, 2013
 * @since 2.4
 */
public class CustomShapeHandler extends AbstractPathHandler implements
ShapeHandler {

    private final PaletteItemGrouper theParent;
    private final SvgDef theDef;

    /**
     * Constructor.
     * 
     * @param controller Beermat Controller
     * @param parent Parent grouper
     * @param def the definition
     */
    public CustomShapeHandler(BeermatController controller,
    PaletteItemGrouper parent,SvgDef def) {
        super(controller,def.getPath().getValue());
        theParent = parent;
        theDef = def;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        return theDef.getName();
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
     */
    @Override
    public Image getImage() {
        return PluginManager.getImage(getImagePath());
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        return theParent.getImagePath()+"/"+theDef.getName()+".png";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        return theDef.getDescription();
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#createShape(org.w3c.dom.svg.SVGDocument,
     * org.eclipse.swt.graphics.Point)
     */
    @Override
    public Element createShape(SVGDocument document,Point startPoint) {
        Element path = document.createElementNS(SVGUtils.SVG_NAMESPACE,"path");
        path.setAttribute("d",getPath());
        document.getDocumentElement().appendChild(path);

        // Set an class attribute for styling
        path.setAttribute("class",getName());
        
        SVGUtils.setTransformTranslate(path,new Point2D.Double(startPoint.x,startPoint.y));
        
        return path;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#updateShape(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void drag(Element element,ShapeUpdateData updateData) {
        // exactly the same logic as a resize
        resize(element,updateData);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#resize(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void resize(Element element,ShapeUpdateData updateData) {
        // Get Rectangle bounds of the shape.
        Rectangle bounds =
            SVGUtils.getBounds(element,getController().getViewBoxTransform());
        // Get the difference in drag, i.e. (1,0)

        float x = updateData.getDiff().x;
        float y = updateData.getDiff().y;

        // retain aspect needed if shift is down
        if (updateData.isShiftDown()) {
            Point2D.Float f =
                MathUtils.retainAspectRatio(x,y,bounds.width,bounds.height);
            x = f.x;
            y = f.y;
        }

        // Divide the drag.x by width to double - result a
        float xScale = x/bounds.width;
        // Divide the drag.y by height to double - result b
        float yScale = y/bounds.height;
        // Get old scale and add the scale difference to the old scale.
        Point2D oldScale = SVGUtils.getElementScale(element);
        xScale += oldScale.getX();
        yScale += oldScale.getY();
        // Set the new scale only if not infinite and not 'nan'.
        if (!Float.isInfinite(yScale)&&!Float.isInfinite(xScale)&&
            !Float.isNaN(yScale)&&!Float.isNaN(xScale)) {
            TransformElementParser transformer =
                new TransformElementParser(element);
            transformer.setScale(xScale,yScale);
        }
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.AbstractPathHandler#mouseClick(org.eclipse.swt.graphics.Point,
     * org.w3c.dom.Element)
     */
    @Override
    public Element mouseClick(Point location,Element lastElement) {

        Element path =
            createShape(getController().getManager().getDocument(),location);

        SVGUtils.retainScale(path,lastElement);

        return path;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return getImagePath();
    }

}
