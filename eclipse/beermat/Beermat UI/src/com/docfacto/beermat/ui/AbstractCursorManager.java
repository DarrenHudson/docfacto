package com.docfacto.beermat.ui;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.events.CursorChangeEvent;
import com.docfacto.beermat.listeners.BeermatEventListener;

/**
 * OSX and Windows / Linux behave differently, so this is the abstract of the
 * platform dependent Cursor Managers
 * 
 * 
 * @author dhudson - created 28 Aug 2013
 * @since 2.4
 * @docfacto.adam ignore
 */
public abstract class AbstractCursorManager implements BeermatEventListener {

    private final BeermatController theController;

    private int theLastCursor = -1;

    public static final int CROSSHAIR = 0;
    public static final int DEFAULT = 1;
    public static final int HAND = 2;
    public static final int WAIT = 3;
    public static final int TEXT = 4;
    public static final int MOVE = 5;
    public static final int E_RESIZE = 6;
    public static final int NE_RESIZE = 7;
    public static final int NW_RESIZE = 8;
    public static final int N_RESIZE = 9;
    public static final int SE_RESIZE = 10;
    public static final int SW_RESIZE = 11;
    public static final int S_RESIZE = 12;
    public static final int W_RESIZE = 13;

    // Custom cursors
    public static final int ROTATE = 20;
    public static final int PENCIL = 21;
    public static final int ADD_TEXT = 22;

    public static final int[] CURSORS = {CROSSHAIR,DEFAULT,HAND,WAIT,TEXT,MOVE,
        E_RESIZE,NE_RESIZE,NW_RESIZE,N_RESIZE,SE_RESIZE,
        SW_RESIZE,S_RESIZE,W_RESIZE,ROTATE,PENCIL,ADD_TEXT};

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @since 2.4
     */
    public AbstractCursorManager(BeermatController controller) {
        theController = controller;
        // Register thy self
        theController.addListener(this);
    }

    /**
     * @see com.docfacto.beermat.listeners.BeermatEventListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    @Override
    public void handleEvent(BeermatEvent event) {
        if (event.isCursorChangeEvent()) {
            final CursorChangeEvent cursorEvent = (CursorChangeEvent)event;
            if (theLastCursor==cursorEvent.getCursorType()) {
                // We are already set, so lets not do any more
                return;
            }

            theLastCursor = cursorEvent.getCursorType();

            handleCursorChange(theLastCursor);
        }
    }

    /**
     * This event is thrown if the cursor needs to change from the current type
     * 
     * @param type of cursor
     * @since 2.4
     */
    public abstract void handleCursorChange(int type);

    /**
     * This is called after construction to load the cursor map
     * 
     * @since 2.4
     */
    public abstract void loadCursors();

    /**
     * Dispose of any cursors
     * 
     * @since 2.4
     */
    public abstract void dispose();
}
