package com.docfacto.beermat.ui.toolbar.shapes;

/**
 * Use when handling a shape that holds a path.
 *
 * @author kporter - created Jul 4, 2013
 * @since 2.4
 */
public interface IPathHandler extends ShapeHandler {
    /**
     * Returns the path
     * @return The path as a string
     * @since 2.4 
     */
    public String getPath();
    
    /**
     * Sets the path.
     * @param path The path
     * @since 2.4
     */
    public void setPath(String path);
}
