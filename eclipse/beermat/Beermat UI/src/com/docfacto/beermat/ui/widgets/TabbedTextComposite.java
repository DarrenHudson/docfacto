package com.docfacto.beermat.ui.widgets;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

import com.docfacto.beermat.ui.BeermatViewConstants;
import com.docfacto.beermat.ui.widgets.data.TabData;
import com.docfacto.common.Platform;
import com.docfacto.common.Utils;
import com.docfacto.core.utils.SWTUtils;

/**
 * A composite holding a text area, with tabs.
 * 
 * @author kporter - created Jun 25, 2013
 * @since 2.4
 */
public class TabbedTextComposite extends Composite {
    // list of listeners?
    private static final String[] LOCALES = Utils.getLocales();
    private CTabFolder tabFolder;
    private List theList;

    /**
     * Constructor.
     * 
     * @param parent The parent Composite
     * @param style The style int
     */
    public TabbedTextComposite(Composite parent,int style) {
        super(parent,style);
        this.setSize(new Point(327,150));
        this.setBackground(BeermatViewConstants.COLOR_BACKGROUND);

        tabFolder = new CTabFolder(this,SWT.BORDER);
        tabFolder.setBounds(0,0,260,123);
        tabFolder
            .setSelectionBackground(BeermatViewConstants.COLOR_BACKGROUND);
        tabFolder
            .setBackground(BeermatViewConstants.COLOR_BACKGROUND);
        tabFolder.setForeground(BeermatViewConstants.COLOR_FOREGROUND_BRAND);
        tabFolder.setSelectionForeground(SWTUtils.getColor(SWT.COLOR_WHITE));

        theList = new List(this,SWT.V_SCROLL|SWT.MULTI);
        theList.setBounds(266,10,54,113);
        theList.setItems(LOCALES);

        Button addButton = new Button(this,SWT.CENTER);
        addButton.setBounds(260,124,67,26);
        addButton.setText("Add");
        addButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                if (theList.getSelection().length>0) {
                    addTab(theList.getSelection());
                }
            }
        });

        Button btnRemove = new Button(
            this,SWT.NONE);
        btnRemove.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                if (tabFolder.getSelectionIndex()!=-1
                    &&tabFolder.getItems().length>1
                    &&!tabFolder.getSelection().getText().equals("default")) {
                    int i = tabFolder.getSelectionIndex();
                    CTabItem c = tabFolder.getItem(i);
                    c.dispose();
                }
            }
        });
        btnRemove.setText("Remove");
        btnRemove.setBounds(182,124,84,26);

        addTab("default",new String[] {""});
    }

    public void addTab(String[] selections) {
        String locale = selections[0];
        if (selections.length>1) {
            for (int i = 0;i<selections.length-1;i++) {
                locale += ","+selections[i+1];
            }
        }
        addTab(locale,new String[] {""});
    }

    public CTabItem addTab(String locale,String text[]) {
        String displayedLocale;
        if (!locale.equals("default")&&locale.length()>5) {
            displayedLocale = locale.substring(0,5);
            displayedLocale += "...";
        }
        else {
            displayedLocale = locale;
        }

        CTabItem tab = new CTabItem(tabFolder,SWT.NONE);

        Text textArea = new Text(tabFolder,SWT.BORDER|SWT.WRAP|SWT.H_SCROLL|
            SWT.V_SCROLL|SWT.CANCEL|SWT.MULTI);

        StringBuilder builder = new StringBuilder();
        for (int i = 0;i<text.length;i++) {
            builder.append(text[i]);
            if (i<text.length-1) {
                builder.append(textArea.getLineDelimiter());
            }
        }
        textArea.setText(builder.toString());
        tab.setControl(textArea);
        tab.setText(locale);
        tab.setToolTipText(locale);
        tab.setData(TabData.LOC_KEY,locale);

        tabFolder.setSelection(tab);
        textArea.setFocus();

        return tab;
    }

    /**
     * Adds a collection of tabs to this tabbed composite.
     * 
     * @param tabs The Collection of tabs to set
     * @since 2.4
     */
    public void setTabs(Collection<TabData> tabs) {
        for (TabData t:tabs) {
            addTab(t.getLocale(),t.getText());
        }
    }

    public ArrayList<TabData> getTabData() {
        ArrayList<TabData> tabs = new ArrayList<TabData>(0);
        CTabItem[] items = tabFolder.getItems();
        for (CTabItem c:items) {
            Text t = (Text)c.getControl();
            String[] strings = t.getText().split(Platform.LINE_SEPARATOR);
            tabs.add(new TabData((String)c.getData(TabData.LOC_KEY),strings));
        }
        return tabs;
    }
}
