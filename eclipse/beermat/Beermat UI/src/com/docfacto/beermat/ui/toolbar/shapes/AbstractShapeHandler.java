package com.docfacto.beermat.ui.toolbar.shapes;

import org.apache.batik.gvt.GraphicsNode;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.ui.toolbar.AbstractPaletteItem;

/**
 * Abstract Shape Handler and Palette Handler
 * 
 * @author dhudson - created 1 Jul 2013
 * @since 2.4
 */
public abstract class AbstractShapeHandler extends AbstractPaletteItem
implements ShapeHandler {

    private Rectangle theLastBounds;

    /**
     * Constructor.
     * 
     * @param controller Beermat Controller
     * @since 2.4
     */
    public AbstractShapeHandler(BeermatController controller) {
        super(controller);
    }

    /**
     * Gets the size of the last drawn element, or a default size if none.
     * <p>
     * If no element has previously been drawn, then the size returned is a
     * default size of (100,100)
     * </p>
     * 
     * @param lastElement The last element drawn, or null if no element has been
     * drawn yet
     * @param type The class type of the element
     * @return The size of the last element inserted of the given type
     * @since 2.4.2
     */
    public Point getSize(Element lastElement,Class<?> type) {
        if (lastElement!=null) {
            GraphicsNode graphicsNode =
                getController().getBeermatAWTCanvas().getGraphicsNodeFor(
                    lastElement);
            if (graphicsNode!=null&&graphicsNode.getBounds()!=null) {
                java.awt.Rectangle r = graphicsNode.getBounds().getBounds();
                return new Point(r.width,r.height);
            }
            else {
                return new Point(100,60);
            }
        }

        return new Point(100,60);
        // if(type.isInstance(lastElement)){
        // Rectangle r =
        // SVGUtils.getBounds(lastElement,getController().getViewBoxTransform());
        // setLastBounds(r);
        // return new Point(r.width,r.height);
        // }
        // else{
        // try{
        // //Else use the bounds from lastBounds in this object.
        // Rectangle r = getLastBounds(); //throw npe if unset.
        // return new Point(r.width,r.height);
        // } catch(NullPointerException e2){
        // //set bounds to 100,100 default, as mouse clicked without rect
        // //and no previous rect has been made
        // return new Point(100,60);
        // }
        // }
    }

    /**
     * Returns lastBounds.
     * 
     * @return the lastBounds
     */
    public Rectangle getLastBounds() {
        return theLastBounds;
    }

    /**
     * Sets lastBounds.
     * 
     * @param lastBounds the lastBounds value
     */
    public void setLastBounds(Rectangle lastBounds) {
        theLastBounds = lastBounds;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#createShape(org.w3c.dom.svg.SVGDocument,
     * org.eclipse.swt.graphics.Point)
     */
    @Override
    public Element createShape(SVGDocument document,Point startPoint) {
        return null;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#drag(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void drag(Element element,ShapeUpdateData updateData) {
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#resize(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void resize(Element element,ShapeUpdateData updateData) {
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#move(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void move(Element element,ShapeUpdateData updateData) {
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#mouseClick(org.eclipse.swt.graphics.Point,
     * Element)
     */
    @Override
    public Element mouseClick(Point location,Element lastElement) {
        return null;
    }

}
