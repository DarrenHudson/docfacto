package com.docfacto.beermat.ui.toolbar.shapes;

import static com.docfacto.beermat.plugin.PluginConstants.ICON_FOLDER;
import static com.docfacto.beermat.plugin.PluginConstants.SQUARE_ORANGE;

import org.apache.batik.dom.svg.SVGOMRectElement;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.utils.ElementUtils;
import com.docfacto.beermat.utils.PluginManager;

/**
 * A handler for a Square (rect element constrained)
 * 
 * @author kporter - created Jun 21, 2013
 * @since 2.4
 * @docfacto.link key="squareHandler"
 * uri="${doc-beermat}/drawing/simpleshapes/c_rectSquareTool.dita" link-to="doc"
 */
public class SquareShapeHandler extends AbstractShapeHandler {

    /**
     * Constructor.
     * @param controller beermat controller
     * @since 2.4
     */
    public SquareShapeHandler(BeermatController controller) {
        super(controller);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        return "Square";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
     */
    @Override
    public Image getImage() {
        return PluginManager.getImage(getImagePath());
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        return ICON_FOLDER+SQUARE_ORANGE;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        return "Square tool (s)";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.AbstractPaletteItem#getShortcutKey()
     */
    @Override
    public char getShortcutKey() {
        return 's';
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.AbstractShapeHandler#createShape(org.w3c.dom.svg.SVGDocument,
     * org.eclipse.swt.graphics.Point)
     */
    @Override
    public Element createShape(SVGDocument document,Point startPoint) {
        return ElementUtils.createRectangle(document,startPoint);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.AbstractShapeHandler#drag(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void drag(Element element,ShapeUpdateData updateData) {
        ElementUtils.updateRect(element,updateData.getStart(),updateData.getLast(),true);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.AbstractShapeHandler#mouseClick(org.eclipse.swt.graphics.Point,
     * org.w3c.dom.Element)
     */
    @Override
    public Element mouseClick(Point location,Element lastElement) {
        Point size = super.getSize(lastElement,SVGOMRectElement.class);
        if (size.x!=size.y) {
            int largest = size.x>size.y ? size.x : size.y;
            size = new Point(largest,largest);
        }
        return ElementUtils.createRectangle(getController().getManager().getDocument(),location,size.x,size.y);
    }

}
