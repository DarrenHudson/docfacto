package com.docfacto.beermat.ui.toolbar;

import org.eclipse.swt.graphics.Image;

/**
 * Interface for the basic shape
 * 
 * This is wrong, its not a shape, its a toolbar item...
 * 
 * @author dhudson - created 18 Jun 2013
 * @since 2.4
 */
public interface PaletteItem {
    
    /**
     * Return the name of the shape
     * 
     * @return name of the shape
     * @since 2.4
     */
    public String getName();

    /**
     * Return the toolbar image for the shape
     * 
     * @return image for the shape
     * @since 2.4
     */
    public Image getImage();
    
    /**
     * Gets the path of the image.
     * @return The path of the image
     * @since 2.4.2
     */
    String getImagePath();

    /**
     * Return the toolbar tip text for the shape
     * 
     * @return tooltip for the shape
     * @since 2.4
     */
    public String getTooltipText();

    /**
     * Return the shortcut key
     * 
     * @return the shortcut key
     * @since 2.4
     */
    public char getShortcutKey();
    
    /**
     * An action to perform when selected
     * 
     * @since 2.4
     */
    public void performAction();
    
}
