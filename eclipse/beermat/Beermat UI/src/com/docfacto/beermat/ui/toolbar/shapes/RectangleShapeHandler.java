package com.docfacto.beermat.ui.toolbar.shapes;
import static com.docfacto.beermat.plugin.PluginConstants.*;

import org.apache.batik.dom.svg.SVGOMRectElement;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.utils.ElementUtils;
import com.docfacto.beermat.utils.PluginManager;

/**
 * A Handler for the rect shape.
 * @author kporter - created Jun 28, 2013
 * @since 2.4
 * @docfacto.link key="rectTool" uri="${doc-beermat}/drawing/simpleshapes/c_rectSquareTool.dita" link-to="doc"
 */
public class RectangleShapeHandler extends AbstractShapeHandler {

    /**
     * Constructor.
     * @param controller Berrmat Controller
     */
    public RectangleShapeHandler(BeermatController controller) {
        super(controller);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        return "Rectangle";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
     */
    @Override
    public Image getImage() {
        return PluginManager.getImage(getImagePath());
    }
    
    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        return ICON_FOLDER+RECT_ORANGE;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        return "Rectangle Tool (R)";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.AbstractPaletteItem#getShortcutKey()
     */
    @Override
    public char getShortcutKey() {
        return 'r';
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#createShape(org.w3c.dom.svg.SVGDocument, org.eclipse.swt.graphics.Point)
     */
    @Override
    public Element createShape(SVGDocument document,Point startPoint) {
        return ElementUtils.createRectangle(document,startPoint);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#drag(org.w3c.dom.Element, com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void drag(Element element,ShapeUpdateData data) {
        ElementUtils.updateRect(element,data.getStart(),data.getLast(),false);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.AbstractShapeHandler#mouseClick(org.eclipse.swt.graphics.Point, org.w3c.dom.Element)
     */
    @Override
    public Element mouseClick(Point location,Element lastElement) {        
        Point size = super.getSize(lastElement,SVGOMRectElement.class);
        return ElementUtils.createRectangle(getController().getManager().getDocument(),location,size.x,size.y);
    }
}
