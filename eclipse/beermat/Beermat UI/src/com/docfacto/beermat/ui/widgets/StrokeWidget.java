package com.docfacto.beermat.ui.widgets;

import static com.docfacto.beermat.plugin.PluginConstants.ICON_FOLDER;
import static com.docfacto.beermat.plugin.PluginConstants.SHORTCUT_ARROW;
import static com.docfacto.beermat.plugin.PluginConstants.SHORTCUT_LINE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.batik.dom.svg.SVGOMLineElement;
import org.apache.batik.dom.svg.SVGOMMarkerElement;
import org.apache.batik.dom.svg.SVGOMPathElement;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.AttributeChangeEvent;
import com.docfacto.beermat.generated.ElementDef;
import com.docfacto.beermat.generated.Palette;
import com.docfacto.beermat.generated.Type;
import com.docfacto.beermat.ui.BeermatViewConstants;
import com.docfacto.beermat.ui.BeermatViewConstants.ToolbarPopup;
import com.docfacto.beermat.ui.ControlToolBar;
import com.docfacto.beermat.ui.PaletteItemToolBar;
import com.docfacto.beermat.ui.toolbar.MarkerPaletteItem;
import com.docfacto.beermat.ui.toolbar.PaletteItem;
import com.docfacto.beermat.ui.toolbar.PaletteItemGrouper;
import com.docfacto.beermat.utils.ElementUtils;
import com.docfacto.beermat.utils.PluginManager;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.NameValuePair;
import com.docfacto.core.utils.SWTUtils;
import com.docfacto.core.widgets.listeners.NumericVerifyListener;

/**
 * Stroke widget handles the stroke thickness, line type and colour
 * 
 * @author dhudson - created 18 Jun 2013
 * @since 2.4
 * @docfacto.link uri="${doc-beermat}/controlbar/c_stroke_widget.dita" key="stroke-widget" version="2.4.2"
 */
public class StrokeWidget extends AbstractControlBarWidget {

    private final static String[] STROKE_DASH_OPTIONS = {"solid","dashed",
        "dotted"};

    private final static String[] STROKE_WIDTH_OPTIONS = {"0","0.5","1","1.5",
        "2","2.5",
        "3","4","5","6","7","8","9","10","15","20"};

    /**
     * @docfacto.link uri="${doc-beermat}/controlbar/c_stroke_widget.dita" key="colour-picker" version="2.4.2"
     */
    private ColourPickerWidget theColourWidget;

    /**
     * @docfacto.link uri="${doc-beermat}/controlbar/c_stroke_widget.dita" key="stroke-width" version="2.4.2"
     */
    private Combo theStrokeWidth;

    /**
     * @docfacto.link uri="${doc-beermat}/controlbar/c_stroke_widget.dita" key="stroke-type" version="2.4.2"
     */
    private Combo theStrokeDash;

    private PaletteItemGrouper theStartMarker;

    // private PaletteItemGrouper theMidMarker;

    private PaletteItemGrouper theEndMarker;

    /**
     * Create a widget that has handles all aspects of stroke.
     * 
     * @param toolbar control toolbar
     * @since 2.4
     */
    public StrokeWidget(ControlToolBar toolbar) {
        super(toolbar);
    }

    /**
     * @see com.docfacto.beermat.ui.widgets.AbstractControlBarWidget#layoutWidget()
     * @docfacto.link key="arrowShortcut" uri="${doc-beermat}/drawing/arrows/c_drawing_with_arrowheads.dita"
     * link-to="doc"
     */
    @Override
    protected void layoutWidget() {
        createLabel("Stroke :");
        final BeermatController controller = getToolbar().getController();

        // Create a selection listener which will fire attribute changed events
        SelectionListener listener = new SelectionListener() {
            /**
             * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent e) {
                controller.processEvent(new AttributeChangeEvent(this));
            }

            /**
             * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
             */
            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                controller.processEvent(new AttributeChangeEvent(this));
            }
        };

        theStrokeWidth = new Combo(getParent(),SWT.NONE);
        theStrokeWidth.setFont(BeermatViewConstants.DEFAULT_FONT);
        theStrokeWidth.setItems(STROKE_WIDTH_OPTIONS);
        theStrokeWidth.select(2);
        theStrokeWidth.addSelectionListener(listener);
        theStrokeWidth.setLayoutData(getGridData(true));
        theStrokeWidth.addVerifyListener(new NumericVerifyListener());

        theStrokeDash = new Combo(getParent(),SWT.READ_ONLY);
        theStrokeDash.setItems(STROKE_DASH_OPTIONS);
        theStrokeDash.setFont(BeermatViewConstants.DEFAULT_FONT);
        theStrokeDash.select(0);
        theStrokeDash.addSelectionListener(listener);
        GridData strokeData = getGridData(true);
        strokeData.widthHint = 66;
        theStrokeDash.setLayoutData(strokeData);

        // Disable the none option
        theColourWidget =
            new ColourPickerWidget(getToolbar().getController(),getParent(),
                SWTUtils.getColor(SWT.COLOR_BLACK));
        theColourWidget.getComposite().setLayoutData(getGridData(true));

        Composite markerToolbarComposite = new Composite(getParent(),SWT.NONE);
        markerToolbarComposite.setLayoutData(getGridData(true));
        try {
            createMarkerToolbar(markerToolbarComposite);
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }

        Composite shortcutBar = new Composite(getParent(),SWT.NONE);
        shortcutBar.setLayout(new FillLayout(SWT.VERTICAL));
        shortcutBar.setLayoutData(getGridData(true));
        Label lineShortcut = new Label(shortcutBar,SWT.NONE);
        lineShortcut.setImage(PluginManager.getImage(ICON_FOLDER+SHORTCUT_LINE));
        // Label sep = new Label(shortcutBar, SWT.NONE);
        // sep.setSize(2,2);
        Label arrowShortcut = new Label(shortcutBar,SWT.NONE);
        arrowShortcut.setImage(PluginManager.getImage(ICON_FOLDER+SHORTCUT_ARROW));

        lineShortcut.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseDown(MouseEvent e) {
                setMarkerStart("none");
                setMarkerEnd("none");
            }
        });

        arrowShortcut.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseDown(MouseEvent e) {
                setMarkerStart(MarkerPaletteItem.NONE);
                setMarkerEnd(MarkerPaletteItem.DEFAULT_ARROW);
            }
        });

    }

    /**
     * Creates the marker ToolBar
     * 
     * @throws IOException
     * @since 2.4.5
     * @docfacto.link key="markerList" uri="${doc-beermat}/drawing/arrows/c_drawing_with_arrowheads.dita" link-to="doc"
     */
    private void createMarkerToolbar(Composite toolbarComposite) throws IOException {
        BeermatController controller = getToolbar().getController();
        Palette markerPalette =
            PluginManager.getPaletteType(Type.MARKER).get(0);
        List<ElementDef> elementdefs =
            PluginManager.getElementDefs(markerPalette);
        Document originalDocument =
            getToolbar().getController().getManager().getDocument();
        ArrayList<MarkerPaletteItem> markerItems =
            new ArrayList<MarkerPaletteItem>(10);
        ArrayList<MarkerPaletteItem> reverseMarkers =
            new ArrayList<MarkerPaletteItem>(10);

        for (ElementDef def:elementdefs) {
            Document svg = SVGUtils.getSVGFromElementDef(def);

            NodeList children = svg.getDocumentElement().getChildNodes();

            for (int i = 0;i<children.getLength();i++) {
                if (children.item(i) instanceof SVGOMMarkerElement) {
                    Element oldmarker = (Element)children.item(i);
                    Element element =
                        (Element)originalDocument.importNode(oldmarker,true);

                    markerItems.add(new MarkerPaletteItem(getToolbar()
                        .getController(),element,element.getAttribute("id"),false));

                    reverseMarkers.add(new MarkerPaletteItem(getToolbar()
                        .getController(),element,element.getAttribute("id"),true));
                }
            }
        }

        // Composite toolbarComposite = new Composite(parent,SWT.NONE);
        toolbarComposite.setLayout(new FillLayout(SWT.HORIZONTAL));

        PaletteItemToolBar markerToolbar =
            new PaletteItemToolBar(toolbarComposite,controller,SWT.HORIZONTAL);
        theStartMarker = new PaletteItemGrouper(controller,"marker-start",ToolbarPopup.VERTICAL);
        theEndMarker = new PaletteItemGrouper(controller,"marker-end",ToolbarPopup.VERTICAL);

        theStartMarker.setToolbar(toolbarComposite);
        theEndMarker.setToolbar(toolbarComposite);

        MarkerPaletteItem noneMarker =
            new MarkerPaletteItem(getToolbar().getController(),null,MarkerPaletteItem.NONE,false);
        noneMarker.setNoneMarker(true);
        theStartMarker.addItem(noneMarker);
        theEndMarker.addItem(noneMarker);

        for (MarkerPaletteItem marker:markerItems) {
            theEndMarker.addItem(marker);
        }
        for (MarkerPaletteItem marker:reverseMarkers) {
            theStartMarker.addItem(marker);
        }

        markerToolbar.addItem(theStartMarker,SWT.PUSH);
        markerToolbar.addItem(theEndMarker,SWT.PUSH);
    }

    /**
     * @see com.docfacto.beermat.ui.widgets.AbstractControlBarWidget#getAttributesForElement(org.w3c.dom.Element)
     */
    @Override
    public List<NameValuePair> getAttributesForElement(final Element element) {
        List<NameValuePair> attrList = new ArrayList<NameValuePair>(3);

        attrList.add(new NameValuePair("stroke-width",theStrokeWidth.getText()));
        attrList.add(new NameValuePair("stroke",theColourWidget.getCurrentSelectedColour()));

        final NameValuePair dash = new NameValuePair("stroke-dasharray");
        // Set the dash array
        switch (theStrokeDash.getSelectionIndex()) {
        case 0:
            dash.setValue("none");
            break;
        case 1:
            dash.setValue("12,12");
            break;
        case 2:
            dash.setValue("2,5");
            break;
        }

        attrList.add(dash);

        if (element instanceof SVGOMPathElement||element instanceof SVGOMLineElement) {
            Document document = getToolbar().getController().getManager().getDocument();

            MarkerPaletteItem start = (MarkerPaletteItem)theStartMarker.getSelectedItem();
            MarkerPaletteItem end = (MarkerPaletteItem)theEndMarker.getSelectedItem();

            if (!start.isNoneMarker()) {
                String id = ElementUtils.addMarker(document,start.getMarker(),start.getName(),true);
                attrList.add(new NameValuePair("marker-start","url(#"+id+")"));
            }
            else {
                attrList.add(new NameValuePair("marker-start",""));
            }

            if (!end.isNoneMarker()) {
                String id = ElementUtils.addMarker(document,end.getMarker(),end.getName(),false);
                attrList.add(new NameValuePair("marker-end","url(#"+id+")"));
            }
            else {
                attrList.add(new NameValuePair("marker-end",""));
            }
        }

        return attrList;
    }

    /**
     * Sets the colour in the colour widget to a hex value
     * 
     * @param hexColour The hex colour
     * @since 2.4.4
     */
    public void setSelectedColour(String hexColour) {
        theColourWidget.setColour(SWTUtils.convertHexToColor(hexColour),true);
    }

    /**
     * Sets the width value in the combo
     * 
     * @param strokeWidth The stroke width
     * @since 2.4.4
     */
    public void setSelectedStrokeWidth(String strokeWidth) {
        theStrokeWidth.setText(strokeWidth);
    }

    /**
     * Sets the stroke-dasharray value to the combo
     * 
     * @param dash The dash value to set
     * @since 2.4.4
     */
    public void setSelectedStrokeDash(String dash) {
        theStrokeDash.setText(dash);
    }

    /**
     * Sets the start marker on this widget
     * 
     * @param markerStart The start marker id
     * @since 2.4.5
     */
    public void setMarkerStart(String markerStart) {
        if (markerStart==null||markerStart.isEmpty()) {
            markerStart = "none";
        }
        for (final PaletteItem p:theStartMarker.getAllItems()) {
            if (p.getName().equals(markerStart)) {
                SWTUtils.UIThreadExec(new Runnable() {
                    @Override
                    public void run() {
                        theStartMarker.setActivePaletteItem(p,false);
                    }
                });
            }
        }
    }

    /**
     * Change the end marker shown on the this widget
     * 
     * @param markerEnd The end marker id
     * @since 2.4.5
     */
    public void setMarkerEnd(String markerEnd) {
        if (markerEnd==null||markerEnd.isEmpty()) {
            markerEnd = "none";
        }
        for (final PaletteItem p:theEndMarker.getAllItems()) {
            ;
            if (p.getName().equals(markerEnd)) {
                SWTUtils.UIThreadExec(new Runnable() {
                    @Override
                    public void run() {
                        theEndMarker.setActivePaletteItem(p,false);
                    }
                });
            }
        }
    }

}
