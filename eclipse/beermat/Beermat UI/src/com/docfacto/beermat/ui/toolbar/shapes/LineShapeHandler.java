package com.docfacto.beermat.ui.toolbar.shapes;

import static com.docfacto.beermat.plugin.PluginConstants.ICON_FOLDER;
import static com.docfacto.beermat.plugin.PluginConstants.LINE_ORANGE;

import java.awt.Rectangle;

import org.apache.batik.gvt.GraphicsNode;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.utils.ElementUtils;
import com.docfacto.beermat.utils.PluginManager;

/**
 * A Handler for the Line element.
 * 
 * @author dhudson - created 27 Jun 2013
 * @since 2.4
 * @docfacto.link key="lineTool"
 * uri="${doc-beermat}/drawing/simpleshapes/c_lineTool.dita" link-to="doc"
 */
public class LineShapeHandler extends AbstractShapeHandler {

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @since 2.4
     */
    public LineShapeHandler(BeermatController controller) {
        super(controller);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        return "Line";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
     */
    @Override
    public Image getImage() {
        return PluginManager.getImage(getImagePath());
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        return ICON_FOLDER+LINE_ORANGE;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        return "Line tool (L)";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.AbstractPaletteItem#getShortcutKey()
     */
    @Override
    public char getShortcutKey() {
        return 'l';
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.AbstractShapeHandler#createShape(org.w3c.dom.svg.SVGDocument,
     * org.eclipse.swt.graphics.Point)
     */
    @Override
    public Element createShape(SVGDocument document,Point startPoint) {
        return ElementUtils.createLine(document,startPoint);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.AbstractShapeHandler#drag(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void drag(Element element,ShapeUpdateData data) {
        Point point = data.getLast();

        if (data.isShiftDown()) {
            GraphicsNode graphicsNode =
                getController().getBeermatAWTCanvas().getGraphicsNodeFor(
                    element);
            if (graphicsNode!=null&&graphicsNode.getBounds()!=null) {
                Rectangle bounds = graphicsNode.getBounds().getBounds();

                if (bounds.height>bounds.width) {
                    // Vertical line
                    point.y = (int)Float.parseFloat(element.getAttribute("y1"));
                }
                else {
                    // Horizontal line
                    point.x = (int)Float.parseFloat(element.getAttribute("x1"));
                }
            }
        }

        ElementUtils.updateLine(element,data.getLast());
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#resize(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void resize(Element element,ShapeUpdateData updateData) {
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#move(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void move(Element element,ShapeUpdateData updateData) {
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.AbstractShapeHandler#mouseClick(org.eclipse.swt.graphics.Point,
     * org.w3c.dom.Element)
     */
    @Override
    public Element mouseClick(Point location,Element lastElement) {
        try {
            int x1 = (int)Float.parseFloat(lastElement.getAttribute("x1"));
            int y1 = (int)Float.parseFloat(lastElement.getAttribute("y1"));
            int y2 = (int)Float.parseFloat(lastElement.getAttribute("y2"));
            int x2 = (int)Float.parseFloat(lastElement.getAttribute("x2"));

            int diffX = x2-x1;
            int diffY = y2-y1;

            Element e =
                ElementUtils.createLine(getController().getManager()
                    .getDocument(),location);
            ElementUtils.updateLine(e,new Point(location.x+diffX,location.y+
                diffY));
            return e;

        }
        catch (NullPointerException ex) {
            // Do nothing, there was no last element
            return null;
        }
    }

}
