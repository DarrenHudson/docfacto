package com.docfacto.beermat.ui.toolbar.shapes;

import static com.docfacto.beermat.plugin.PluginConstants.CIRCLE_ORANGE;
import static com.docfacto.beermat.plugin.PluginConstants.ICON_FOLDER;

import org.apache.batik.dom.svg.SVGOMCircleElement;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.utils.ElementUtils;
import com.docfacto.beermat.utils.PluginManager;

/**
 * A circle handler.
 *
 * @author kporter - created Jun 21, 2013
 * @since 2.4
 * @docfacto.link key="circleTool" uri="${doc-beermat}/drawing/simpleshapes/c_ellipseCircleTool.dita" link-to="doc"
 */
public class CircleShapeHandler extends AbstractShapeHandler {
	
    public CircleShapeHandler(BeermatController controller) {
        super(controller);
    }

    /**
	 * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
	 */
	@Override
	public String getName() {
		return "Circle";
	}

	/**
	 * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
	 */
	@Override
	public Image getImage() {
		return PluginManager.getImage(getImagePath());
	}

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        return ICON_FOLDER+CIRCLE_ORANGE;
    }
	
	/**
	 * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
	 */
	@Override
	public String getTooltipText() {
		return "Circle tool (C)";
	}

	/**
	 * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getShortcutKey()
	 */
	@Override
	public char getShortcutKey() {
		return 'c';
	}

	/**
	 * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#createShape(org.w3c.dom.svg.SVGDocument, org.eclipse.swt.graphics.Point)
	 */
	@Override
	public Element createShape(SVGDocument document,Point startPoint) {
		return ElementUtils.createCircle(document, startPoint);
	}

	/**
	 * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#updateShape(org.w3c.dom.Element, com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
	 */
	@Override
	public void drag(Element element,ShapeUpdateData updateData) {
	    ElementUtils.updateCircle(element,updateData.getStartToEndDiff());
	}

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#resize(org.w3c.dom.Element, com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void resize(Element element,ShapeUpdateData updateData) {
        // TODO Auto-generated method stub
        
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#move(org.w3c.dom.Element, com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void move(Element element,ShapeUpdateData updateData) {
        // TODO Auto-generated method stub
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.AbstractShapeHandler#mouseClick(org.eclipse.swt.graphics.Point, org.w3c.dom.Element)
     */
    @Override
    public Element mouseClick(Point location,Element lastElement) {
        Point size = super.getSize(lastElement,SVGOMCircleElement.class);
        float radius = size.x / 2;
        return ElementUtils.createCircle(getController().getManager().getDocument(),location,radius);
    }
    

}
