package com.docfacto.beermat.ui.widgets;

import java.util.List;

import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Label;
import org.osgi.framework.Version;
import org.w3c.dom.Element;

import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.events.PaletteBarChangeEvent;
import com.docfacto.beermat.listeners.BeermatEventListener;
import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.beermat.ui.ControlToolBar;
import com.docfacto.common.NameValuePair;
import com.docfacto.core.swt.DefaultMouseListener;
import com.docfacto.core.utils.SWTUtils;

/**
 * Selector widget, display what is currently selected from the tool bar.
 * 
 * A hover control is implemented to provide the current version number
 * 
 * @author dhudson - created 18 Jun 2013
 * @since 2.4
 */
public class SelectorWidget extends AbstractControlBarWidget implements
BeermatEventListener {

    private Label theLabel;

    /**
     * Constructor.
     * 
     * @param toolbar control toolbar
     * @since 2.4
     */
    public SelectorWidget(ControlToolBar toolbar) {
        super(toolbar);
    }

    /**
     * @see com.docfacto.beermat.ui.widgets.AbstractControlBarWidget#layoutWidget()
     */
    @Override
    protected void layoutWidget() {
        theLabel = createLabel("Beeermat");

        // Lets set the tooltip to be the version
        Version version = BeermatUIPlugin.getDefault().getBundle().getVersion();
        theLabel.setToolTipText(version.toString());

        // Add a click listener to the label to force a canvas repaint
        theLabel.addMouseListener(new DefaultMouseListener() {
            /**
             * @see com.docfacto.core.swt.DefaultMouseListener#mouseDown(org.eclipse.swt.events.MouseEvent)
             */
            public void mouseDown(MouseEvent e) {
                getToolbar().getController().getBeermatAWTCanvas()
                    .immediateRepaint();
            }
        });

        // Register for Beermat events
        getToolbar().getController().addListener(this);
    }

    /**
     * @see com.docfacto.beermat.ui.widgets.AbstractControlBarWidget#getAttributesForElement(org.w3c.dom.Element)
     */
    @Override
    public List<NameValuePair> getAttributesForElement(Element element) {
        return EMPTY_LIST;
    }

    /**
     * @see com.docfacto.beermat.listeners.BeermatEventListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    @Override
    public void handleEvent(BeermatEvent event) {
        if (event.isPaletteToolbarChangeEvent()) {
            // Its the only event that we are interested in
            final PaletteBarChangeEvent pbEvent = (PaletteBarChangeEvent)event;
            SWTUtils.UIThreadExec(new Runnable() {
                /**
                 * @see java.lang.Runnable#run()
                 */
                public void run() {
                    theLabel.setText(pbEvent.getPaletteItem().getName());
                }
            });
        }
    }
}
