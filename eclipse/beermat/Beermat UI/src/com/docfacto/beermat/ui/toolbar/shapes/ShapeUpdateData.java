package com.docfacto.beermat.ui.toolbar.shapes;

import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;

import org.eclipse.swt.graphics.Point;

import com.docfacto.beermat.plugin.BeermatUIPlugin;

/**
 * Simple object wrapper to handle updates to an element
 * 
 * @author dhudson - created 20 Jun 2013
 * @since 2.4
 */
public class ShapeUpdateData {

    private Point theStart;
    private Point theLast;
    private Point theLastDiff;
    private Point theStartToEndDiff;
    
    private boolean theShiftDown;

    private String theUpdateText;

    private final AffineTransform theTransform;

    /**
     * Constructor.
     * 
     * @param transform current view transform
     * @since 2.4
     */
    public ShapeUpdateData(AffineTransform transform) {
        theTransform = transform;
        theShiftDown = false;
    }

    /**
     * Return the start point
     * 
     * @return the transformed start point
     * @since 2.4
     */
    public Point getStart() {
        return theStart;
    }

    /**
     * Set the mouse start point of the drag
     * 
     * @param start mouse point
     * @since 2.4
     */
    public void setStart(Point start) {
        theStart = createPointFrom(start);
    }

    /**
     * Return the transformed last mouse point
     * 
     * @return the transformed last mouse point
     * @since 2.4
     */
    public Point getLast() {
        return theLast;
    }

    /**
     * Set the last mouse point
     * 
     * @param last mouse point
     * @since 2.4
     */
    public void setLast(Point last) {
        theLast = createPointFrom(last);
    }

    /**
     * Set the last diff
     * 
     * @param diff
     * @since 2.4
     */
    public void setDiff(Point diff) {
        theLastDiff = diff;
    }

    /**
     * Return the diff
     * 
     * @return the diff with the scale taken into account
     * @since 2.4
     */
    public Point2D.Float getDiff() {
//        Point2D.Float point =
//            new Point((int)Math.round(theLastDiff.x/theTransform.getScaleX()),
//                (int)Math.round((theLastDiff.y/theTransform.getScaleY())));
        
        Point2D.Float point = new Point2D.Float((float)(theLastDiff.x/theTransform.getScaleX()),
            (float)(theLastDiff.y/theTransform.getScaleY()));
        return point;
    }

    /**
     * Return the update text
     * 
     * @return update text
     * @since 2.4
     */
    public String getUpdateText() {
        return theUpdateText;
    }

    /**
     * Set update text
     * 
     * @param updateText
     * @since 2.4
     */
    public void setUpdateText(String updateText) {
        theUpdateText = updateText;
    }

    /**
     * Return the start to end diff
     * 
     * @return the start to end diff
     * @since 2.4
     */
    public Point getStartToEndDiff() {
        return theStartToEndDiff;
    }

    /**
     * Set start to end diff
     * 
     * @param startToEndPoint
     * @since 2.4
     */
    public void setStartToEndDiff(Point startToEndPoint) {
        theStartToEndDiff = startToEndPoint;
    }

    /**
     * Calculate the transformed point
     * 
     * @param point to transform,
     * @return the transformed point
     * @since 2.4
     */
    private Point createPointFrom(Point point) {

        double aX = point.x;
        double aY = point.y;

        Point2D newPoint = new Point2D.Double(aX,aY);

        try {
            Point2D aPoint = theTransform.inverseTransform(newPoint,null);
            return new Point((int)Math.round(aPoint.getX()),
                (int)Math.round(aPoint.getY()));
        }
        catch (NoninvertibleTransformException ex) {
            BeermatUIPlugin.logException("NoninvertableTransform",ex);
        }

        return point;
    }

    /**
     * Set whether the alt modifier is pressed
     * @param isDown Whether the alt is pressed
     * @since 2.4.5
     */
    public void setShiftDown(boolean isDown) {
        theShiftDown = isDown;
    }
    
    /**
     * Returns true if the alt key is pressed
     * @return Whether the alt key is pressed
     * @since 2.4.5
     */
    public boolean isShiftDown(){
        return theShiftDown;
    }
    

}
