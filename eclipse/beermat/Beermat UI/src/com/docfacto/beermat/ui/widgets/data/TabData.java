package com.docfacto.beermat.ui.widgets.data;
/**
 * Simple object to hold a text and locale.
 * @author kporter - created Jun 26, 2013
 * @since 2.4
 */
public class TabData implements Comparable<TabData>{
    public final static String KEY = "beermat.td";
    public final static String LOC_KEY = "beermat.td.loc";
    private String theLocale;
    private String theText[];

    public TabData(String loc, String[] text) {
        theLocale = loc;
        theText = text;
    }
    

    public String[] getText() {
        return theText;
    }

    public String getLocale() {
        return theLocale ;
    }

    public void setLocale(String loc) {
        theLocale = loc;
    }
    public void setText(String text[]){
        theText = text;
    }

    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(TabData t) {
        return theLocale.compareTo(t.getLocale());
    }

}
