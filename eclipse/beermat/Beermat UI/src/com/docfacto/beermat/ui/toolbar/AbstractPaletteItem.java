package com.docfacto.beermat.ui.toolbar;

import com.docfacto.beermat.controller.BeermatController;

/**
 * Abstract Palette Item.
 * 
 * {@docfacto.note getShortcutKey and performAction need to be over-ridden if
 * required }
 * 
 * @author dhudson - created 27 Jun 2013
 * @since 2.4
 */
public abstract class AbstractPaletteItem implements PaletteItem {

    private final BeermatController theController;

    /**
     * Constructor.
     * 
     * @param controller The BeermatController
     * @since 2.4
     */
    public AbstractPaletteItem(BeermatController controller) {
        theController = controller;
    }

    /**
     * Return the controller
     * 
     * @return beermat controller
     * @since 2.4
     */
    public BeermatController getController() {
        return theController;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getShortcutKey()
     */
    @Override
    public char getShortcutKey() {
        return 0;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#performAction()
     */
    @Override
    public void performAction() {
    }

}
