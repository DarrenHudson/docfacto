package com.docfacto.beermat.ui.widgets;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

import com.docfacto.beermat.ui.widgets.listeners.ColourLabelOnMouseOver;
import com.docfacto.core.utils.ImageUtils;

/**
 * A button to hide/show components.
 * 
 * Create a HiderButton when wanting to hide components from a widget.
 * Components that need to be hidden should be added to the HiderButton. Upon
 * clicking the HiderButton composite, it will show the components that were
 * hidden.
 * 
 * 
 * @author kporter - created Aug 14, 2013
 * @since 2.4.3
 */
public class HiderButton extends Composite {
    private static final int SPACE = 2;

    private boolean theIsShown = false;
    private Label theArrow;
    private Color thePreColor;
    private Image theImage;
    private ArrayList<Control> theHidden;

    private Label theText;

    /**
     * Create a new hiderbutton
     * 
     * @param parent Parent Composite
     * @param style the style int
     * @param text The text to use
     * @param image an image to display on this  
     * @since 2.4.2
     */
    public HiderButton(Composite parent,int style,String text,Image image) {
        super(parent,style);
        setLayout(new FormLayout());
        theImage = image;
        theHidden = new ArrayList<Control>(2);

        theArrow = new Label(this,SWT.NONE);
        drawArrow(0);
        theArrow.setEnabled(false);
        // Layout the arrow
        FormData fd_label = new FormData();
        fd_label.top = new FormAttachment(0,5);
        fd_label.left = new FormAttachment(0,2);
        fd_label.right = new FormAttachment(0,10);
        theArrow.setLayoutData(fd_label);

        theText = new Label(this,SWT.NONE);
        theText.setText(text);
        theText.setEnabled(false);
        this.addMouseTrackListener(new ColourLabelOnMouseOver(theText));
        // Layout the text
        FormData fd_lblText = new FormData();
        fd_lblText.top = new FormAttachment(0,SPACE);
        fd_lblText.left = new FormAttachment(theArrow,SPACE);
        fd_lblText.right = new FormAttachment(100,-SPACE);
        fd_lblText.bottom = new FormAttachment(100,-SPACE);
        theText.setLayoutData(fd_lblText);

        // Size this composite down or up according to its components sizes.
        this.addMouseListener(new Click());
        this.pack();
    }

    /**
     * Lightens the background colour. This sets the background colour to the
     * previously set background colour (if darkened). Else if the background
     * was not darkened, the background is lightened.
     * 
     * @since 2.4.3
     */
    protected void setBackgroundLighter() {
        if (thePreColor!=null) {
            theArrow.setBackground(thePreColor);
        }
        else {
            RGB rgb = getBackground().getRGB();
            theArrow.setBackground(new Color(getDisplay(),rgb.red+10,
                rgb.green+10,rgb.blue+10));
        }
    }

    /**
     * Darkens the current background colour.
     * 
     * @since 2.4.3
     */
    protected void setBackgroundDarker() {
        Color lighter = theArrow.getBackground();
        thePreColor = lighter;
        Color darker =
            new Color(lighter.getDevice(),lighter.getRed()-10,
                lighter.getGreen()-10,lighter.getBlue()-10);
        theArrow.setBackground(darker);
    }

    /**
     * Adds to list and hides it if shown.
     * 
     * @param control The control to hide/show by this.
     * @since 2.4.3
     */
    public void addToHideAndHide(Control control) {
        if (control.getVisible()) {
            control.setVisible(false);
        }
        theHidden.add(control);
    }

    /**
     * Adds to the list of items to hide.
     * 
     * @param control The control to hide/show by this button
     * @since 2.4.3
     */
    public void addToHide(Control control) {
        theHidden.add(control);
    }

    /**
     * Switch the visibility of all the controls added to the list.
     * 
     * @since 2.4.3
     */
    protected void switchVisibility() {
        for (Control c:theHidden) {
            c.setVisible(!c.getVisible());
        }
    }

    /**
     * Draws the image in this composite to the label, Rotated by an angle.
     * 
     * @param angle The angle to rotate the image by
     * @since 2.4.3
     */
    public void drawArrow(final int angle) {
        if (theArrow!=null) {
            theArrow.addPaintListener(ImageUtils.rotateAndDrawImage(theImage,angle));
        }
    }

    /**
     * @see org.eclipse.swt.widgets.Control#getBackground()
     */
    @Override
    public void setBackground(Color color) {
        super.setBackground(color);
        theArrow.setBackground(color);
    }

    /**
     * Gets the label's text
     * @return the text set on the label.
     * @since 2.4.2
     */
    public Label getLabelText() {
        return theText;
    }

    /**
     * The mouse listener for the clicks on this composite
     * 
     * @author kporter - created Aug 15, 2013
     * @since 2.4.2
     */
    private class Click extends MouseAdapter {
        /**
         * @see org.eclipse.swt.events.MouseAdapter#mouseDown(org.eclipse.swt.events.MouseEvent)
         */
        @Override
        public void mouseDown(MouseEvent e) {
            theIsShown = !theIsShown;
            switchVisibility();
            setBackgroundDarker();

            if (theIsShown) {
                drawArrow(90);
            }
            else {
                drawArrow(0);
            }
        }

        /**
         * @see org.eclipse.swt.events.MouseAdapter#mouseUp(org.eclipse.swt.events.MouseEvent)
         */
        @Override
        public void mouseUp(MouseEvent e) {
            setBackgroundLighter();
        }
    }
}
