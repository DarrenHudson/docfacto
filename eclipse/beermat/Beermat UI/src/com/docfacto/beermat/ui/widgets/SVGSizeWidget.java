package com.docfacto.beermat.ui.widgets;

import java.awt.geom.Dimension2D;
import java.util.List;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.w3c.dom.Element;

import com.docfacto.beermat.dialogs.SizeDialog;
import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.events.SVGSizeEvent;
import com.docfacto.beermat.listeners.BeermatEventListener;
import com.docfacto.beermat.svg.SVGSize;
import com.docfacto.beermat.ui.AbstractControlToolBar;
import com.docfacto.beermat.ui.BeermatViewConstants;
import com.docfacto.beermat.ui.widgets.listeners.ColourLabelOnMouseOver;
import com.docfacto.beermat.updaters.SVGSizeUpdater;
import com.docfacto.common.DocfactoException;
import com.docfacto.common.NameValuePair;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.core.utils.SWTUtils;

/**
 * Simple Widget to display and alter the size.
 * 
 * @author dhudson - created 18 Jun 2013
 * @since 2.4
 */
public class SVGSizeWidget extends AbstractControlBarWidget implements
BeermatEventListener {

    private Label theSizeLabel;
    private final Shell theShell;
    private SizeDialog theDialog;

    /**
     * Constructor.
     * 
     * @param toolbar control toolbar
     * @since 2.4
     */
    public SVGSizeWidget(AbstractControlToolBar toolbar) {
        super(toolbar);
        theShell = new Shell(toolbar.getDisplay(),SWT.ON_TOP);
        theShell.setBackground(BeermatViewConstants.COLOR_BACKGROUND);
        theShell.setLayout(new FillLayout());
        theShell.pack();
        theDialog = new SizeDialog(toolbar.getDisplay().getActiveShell());
    }

    /**
     * @see com.docfacto.beermat.ui.widgets.AbstractControlBarWidget#layoutWidget()
     */
    @Override
    protected void layoutWidget() {

        MouseAdapter adapter = new MouseAdapter() {
            /**
             * @see org.eclipse.swt.events.MouseAdapter#mouseDown(org.eclipse.swt.events.MouseEvent)
             */
            @Override
            public void mouseDown(MouseEvent e) {
                Dimension2D previous = getToolbar().getController().getBeermatAWTCanvas()
                    .getSVGDocumentSize();

                theDialog.setSVGSize(new Point((int)previous.getWidth(),(int)previous.getHeight()));
                if (theDialog.open()==Window.OK) {
                    try {
                        final Point size = theDialog.getSize();
                        new SVGSizeUpdater(getToolbar().getController(),size);
                        
                        // Updated label
                        setSizeLabel(size.y+"",size.x+"");
                    }
                    catch (DocfactoException ex) {
                        DocfactoCorePlugin.logException(
                            "Invalid Size. Enter a value greater than 0",null);
                    }
                }
            }
        };

        getParent().addMouseListener(adapter);
        getParent().addMouseTrackListener(new ColourLabelOnMouseOver());

        Label lblSize = createLabel("Size :");
        lblSize.setLayoutData(getGridData(true));
        lblSize.setEnabled(false);

        theSizeLabel = createLabel("");
        GridData gd_sizeLabel = getGridData(true);
        gd_sizeLabel.widthHint = 90;
        theSizeLabel.setLayoutData(gd_sizeLabel);
        theSizeLabel.setEnabled(false);
        // theSizeLabel.addMouseListener(adapter);

        // Register for beermat events
        getToolbar().getController().addListener(this);
    }

    /**
     * @see com.docfacto.beermat.ui.widgets.AbstractControlBarWidget#getAttributesForElement(org.w3c.dom.Element)
     */
    @Override
    protected List<NameValuePair> getAttributesForElement(Element element) {
        return EMPTY_LIST;
    }

    /**
     * Sets the size widget in the UI thread
     * 
     * @param height
     * @param width
     * @since 2.4
     */
    private void setSizeLabel(final String height,final String width) {
        SWTUtils.UIThreadExec(new Runnable() {
            /**
             * @see java.lang.Runnable#run()
             */
            public void run() {
                theSizeLabel.setText(width+
                    " x "+height);
            }
        });
    }

    /**
     * @see com.docfacto.beermat.listeners.BeermatEventListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    @Override
    public void handleEvent(BeermatEvent event) {
        if (event.isSVGSizeEvent()) {
            SVGSizeEvent sizeEvent = (SVGSizeEvent)event;
            final SVGSize svgSize = sizeEvent.getSize();
            setSizeLabel(Integer.toString(svgSize.getHeight()),Integer.toString(svgSize.getWidth()));
        }
    }
}
