package com.docfacto.beermat.ui.toolbar.shapes;

import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

/**
 * Shape Handler interface.
 * 
 * Handle the functions from the canvas for the palette item
 *
 * @author dhudson - created 1 Jul 2013
 * @since 2.4
 */
public interface ShapeHandler {

    /**
     * Creates a certain Element in the SVGDocument.
     * @param document The Document that the element will be created in.
     * @param startPoint The starting x,y coordinates of the Element (THe only required coordinates).
     * @return The element that was created.
     * @since 2.4
     */
    public Element createShape(SVGDocument document, Point startPoint);

    /**
     * Calls the relevant updates on an Element when the mouse has been dragged.
     * {@docfacto.note differs from resize as only point is ever changing??}
     * @param element to update
     * @param updateData update data
     * @since 2.4
     */
    public void drag(Element element, ShapeUpdateData updateData);

    /**
     * Resizes an element from any corner.
     * @param element The element to resize
     * @param updateData The data required from resizing.
     * @since 2.4
     */
    public void resize(Element element, ShapeUpdateData updateData);

    /**
     * Moves an element to a different position on the canvas.
     * This only requires the new x,y position.
     * @param element The element to move.
     * @param updateData The data required to move.
     * @since 2.4
     */
    public void move(Element element, ShapeUpdateData updateData);
    
    /**
     * Handle mouse click event
     * 
     * @param location location of the mouse click
     * @param lastElement the bounds of the last object, may be null
     * @return newly created element, or null
     * @since 2.4
     */
    public Element mouseClick(Point location, Element lastElement);

}
