package com.docfacto.beermat.ui.widgets;

import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Label;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.events.CanvasMouseMoveEvent;
import com.docfacto.beermat.listeners.BeermatEventListener;
import com.docfacto.beermat.ui.AbstractControlToolBar;
import com.docfacto.common.NameValuePair;
import com.docfacto.core.utils.SWTUtils;

/**
 * Simple widget to display the position of the mouse over the SVGCanvas
 * 
 * It transpires that updating a label quickly slows everything down.
 * 
 * The pattern here is that we will get the latest set from the controller, but
 * only update it every 100 MS
 * 
 * @author dhudson - created 18 Jun 2013
 * @since 2.4
 * @docfacto.link uri="${doc-beermat}/controlbar/c_mouse_widget.dita"
 * key="mouse-widget" version="2.4.2"
 */
public class MouseStatusWidget extends AbstractControlBarWidget implements
BeermatEventListener,Runnable {

    private Label theCoords;
    private String theLatest;

    private ScheduledThreadPoolExecutor theExecuter;
    private ScheduledFuture<?> theFuture;

    private boolean hasChanged = false;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param toolbar control toolbar
     * @since 2.4
     */
    public MouseStatusWidget(BeermatController controller,AbstractControlToolBar toolbar) {
        super(toolbar);
        controller.addListener(this);
        theExecuter = new ScheduledThreadPoolExecutor(2);
        theFuture =
            theExecuter.scheduleAtFixedRate(this,100,100,TimeUnit.MILLISECONDS);
    }

    /**
     * @see com.docfacto.beermat.ui.widgets.AbstractControlBarWidget#layoutWidget()
     */
    @Override
    protected void layoutWidget() {
        // Create a label with a correct size
        theLatest = "0 : 0";
        theCoords = createLabel(theLatest);
        GridData gd_coords = getGridData(true);
        gd_coords.widthHint = 70;
        theCoords.setLayoutData(gd_coords);
        theCoords.setToolTipText("The location of the mouse");
    }

    /**
     * @see com.docfacto.beermat.ui.widgets.AbstractControlBarWidget#getAttributesForElement(org.w3c.dom.Element)
     */
    @Override
    protected List<NameValuePair> getAttributesForElement(Element element) {
        return EMPTY_LIST;
    }

    /**
     * @see com.docfacto.beermat.listeners.BeermatEventListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    @Override
    public void handleEvent(BeermatEvent event) {
        if (event.isCanvasMouseMoveEvent()) {
            final CanvasMouseMoveEvent mouseEvent = (CanvasMouseMoveEvent)event;

            // Build the string as doing this on the SWT UI thread causes lag
            final StringBuilder builder = new StringBuilder(50);
            builder.append(mouseEvent.getSVGX());
            builder.append(" : ");
            builder.append(mouseEvent.getSVGY());

            theLatest = builder.toString();

            hasChanged = true;
        }

        if (event.isUICloseEvent()) {
            if (theFuture!=null) {
                theFuture.cancel(true);
            }
        }
    }

    /**
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        if (hasChanged) {
            SWTUtils.UIThreadExec(new Runnable() {
                /**
                 * @see java.lang.Runnable#run()
                 */
                public void run() {
                    if (getParent().isDisposed()) {
                        theFuture.cancel(true);
                        return;
                    }

                    if (!theCoords.getText().equals(theLatest)) {
                        theCoords.setText(theLatest);
                    }

                    hasChanged = false;
                }
            });
        }
    }

}
