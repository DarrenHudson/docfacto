package com.docfacto.beermat.ui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.apache.batik.css.engine.CSSEngine;
import org.apache.batik.dom.svg.SVGOMDocument;
import org.eclipse.jface.window.Window;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGGElement;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.dialogs.ClassAttributeDialog;
import com.docfacto.beermat.dialogs.FilterAttributeDialog;
import com.docfacto.beermat.highlight.HighlightManager;
import com.docfacto.beermat.plugin.actions.InsertXsdAction;
import com.docfacto.beermat.updaters.CreateConnectorUpdater;
import com.docfacto.beermat.updaters.DeleteHighlightedUpdater;
import com.docfacto.beermat.updaters.GrouperUpdater;
import com.docfacto.beermat.updaters.LayerUpdater;
import com.docfacto.beermat.updaters.MultiElementsToPathUpdater;
import com.docfacto.beermat.updaters.SetAttributesUpdater;
import com.docfacto.beermat.updaters.SnapToGridUpdater;
import com.docfacto.beermat.updaters.ToPathUpdater;
import com.docfacto.beermat.updaters.TrimCanvasUpdater;
import com.docfacto.beermat.utils.BeermatUtils;
import com.docfacto.beermat.utils.ElementMovement;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.common.NameValuePair;
import com.docfacto.core.utils.SWTUtils;

/**
 * Popup menu for the Beermat Canvas
 * 
 * @author dhudson - created 17 Aug 2013
 * @since 2.4
 */
public class CursorPopupMenu extends JPopupMenu {

    /**
     * {@value}
     */
    private static final long serialVersionUID = 1L;

    private final BeermatController theController;
    private HighlightManager theHighlightManager;

    private JMenuItem theClearSelectionItem;
    private JMenuItem theDeleteSelectionItem;
    private JMenuItem theDeleteWithAnchorsItem;
    private JMenuItem theSelectAllItem;
    private JMenuItem theTrimCanvasItem;
    private JMenuItem theBringToFrontItem;
    private JMenuItem theBringForwardsItem;
    private JMenuItem theSendBackwardItem;
    private JMenuItem theSendToBackItem;
    private JMenuItem theGroupItem;
    private JMenuItem theUngroupItem;
    private JMenuItem theToPathItem;
    private JMenuItem theToSinglePathItem;
    private JMenuItem theClassDialogItem;
    private JMenuItem theFilterDialogItem;
    private JMenuItem theInsertXsdItem;
    private JMenuItem theSnapToGridItem;
    private JMenuItem theConnectorItem;
    
    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @since 2.4
     */
    public CursorPopupMenu(BeermatController controller) {
        theController = controller;

        createMenuItems();
    }

    /**
     * Due to timing issues, we need to make sure that everything is created
     * first.
     * 
     * @return the highlight manager
     * @since 2.4
     */
    private HighlightManager getHighlightManager() {
        if (theHighlightManager==null) {
            theHighlightManager =
                theController.getBeermatAWTCanvas().getHighlightManager();
        }

        return theHighlightManager;
    }

    /**
     * @see javax.swing.JPopupMenu#show(java.awt.Component, int, int)
     */
    @Override
    public void show(Component invoker,int x,int y) {
        // Lets enable menus depending on state of highlight manager
        List<Element> elements = getHighlightManager().getHighlightedElements();

        int size = elements.size();

        if (size==0) {
            theClearSelectionItem.setEnabled(false);
            theDeleteSelectionItem.setEnabled(false);
            theDeleteWithAnchorsItem.setEnabled(false);
            theBringToFrontItem.setEnabled(false);
            theBringForwardsItem.setEnabled(false);
            theSendBackwardItem.setEnabled(false);
            theSendToBackItem.setEnabled(false);
            theGroupItem.setEnabled(false);
            theUngroupItem.setEnabled(false);
            theToPathItem.setEnabled(false);
            theToSinglePathItem.setEnabled(false);
            theClassDialogItem.setEnabled(false);
            theFilterDialogItem.setEnabled(false);
            theConnectorItem.setEnabled(false);
            remove(theUngroupItem);
            remove(theGroupItem);
        }
        else if (size==1) {
            theClearSelectionItem.setEnabled(true);
            theDeleteSelectionItem.setEnabled(true);
            theDeleteWithAnchorsItem.setEnabled(true);
            theBringToFrontItem.setEnabled(true);
            theBringForwardsItem.setEnabled(true);
            theSendBackwardItem.setEnabled(true);
            theSendToBackItem.setEnabled(true);
            theToPathItem.setEnabled(true);
            theToSinglePathItem.setEnabled(false);
            theClassDialogItem.setEnabled(true);
            theFilterDialogItem.setEnabled(true);
            theConnectorItem.setEnabled(false);
            Element element = elements.get(0);
            if (element instanceof SVGGElement) {
                add(theUngroupItem);
                remove(theGroupItem);
                theUngroupItem.setEnabled(true);
            }
            else {
                remove(theUngroupItem);
                remove(theGroupItem);
            }
        }
        else if(size == 2){
            theConnectorItem.setEnabled(true);
        }else {
            theClearSelectionItem.setEnabled(true);
            theDeleteSelectionItem.setEnabled(true);
            theDeleteWithAnchorsItem.setEnabled(true);
            theBringToFrontItem.setEnabled(true);
            theBringForwardsItem.setEnabled(true);
            theSendBackwardItem.setEnabled(true);
            theSendToBackItem.setEnabled(true);
            theToPathItem.setEnabled(true);
            theToSinglePathItem.setEnabled(true);
            theClassDialogItem.setEnabled(true);
            theFilterDialogItem.setEnabled(true);
            theConnectorItem.setEnabled(false);
            add(theGroupItem);
            remove(theUngroupItem);
            theGroupItem.setEnabled(true);
        }
        super.show(invoker,x,y);
    }

    /**
     * Create the menu items
     * 
     * @since 2.4
     */
    private void createMenuItems() {
        theClearSelectionItem = new JMenuItem("Clear selection");
        add(theClearSelectionItem);
        theClearSelectionItem.addActionListener(new ActionListener() {
            /**
             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                theHighlightManager.removeHighlights();
            }
        });

        theDeleteSelectionItem = new JMenuItem("Delete selection");
        add(theDeleteSelectionItem);

        theDeleteSelectionItem.addActionListener(new ActionListener() {
            /**
             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteSelectedItems(false);
            }
        });

        theDeleteWithAnchorsItem =
            new JMenuItem("Delete selection and anchors");
        add(theDeleteWithAnchorsItem);

        theDeleteWithAnchorsItem.addActionListener(new ActionListener() {

            /**
             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
             */
            @Override
            public void actionPerformed(ActionEvent arg0) {
                deleteSelectedItems(true);
            }
        });
        
        theSelectAllItem = new JMenuItem("Select all");
        add(theSelectAllItem);
        theSelectAllItem.addActionListener(new ActionListener() {
            /**
             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                selectAllAction();
            }

        });

        theToPathItem = new JMenuItem("Convert to path");
        add(theToPathItem);
        theToPathItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ToPathUpdater(theController);
            }
        });

        theToSinglePathItem = new JMenuItem("Convert to single path");
        add(theToSinglePathItem);
        theToSinglePathItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new MultiElementsToPathUpdater(theController);
            }
        });

        addSeparator();

        createArrangeMenu();

        addSeparator();

        theConnectorItem = new JMenuItem("Create Connector");
        add(theConnectorItem);
        theConnectorItem.addActionListener(new ActionListener() {
            /**
             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                //new MultiElementsToPathUpdater(theController);
                new CreateConnectorUpdater(theController);
            }
        });
        
        addSeparator();
        
        theTrimCanvasItem = new JMenuItem("Trim Canvas");

        add(theTrimCanvasItem);
        theTrimCanvasItem.addActionListener(new ActionListener() {
            /**
             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                trimCanvas();
            }
        });

        theSnapToGridItem = new JMenuItem("Snap to grid");
        add(theSnapToGridItem);
        theSnapToGridItem.addActionListener(new ActionListener() {

            /**
             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                snapToGrid();
            }

        });
        addSeparator();

        theClassDialogItem = new JMenuItem("Assign Class");
        add(theClassDialogItem);
        theClassDialogItem.addActionListener(new ActionListener() {

            /**
             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
             */
            @Override
            public void actionPerformed(ActionEvent event) {
                showClassDialog();
            }
        });

        theFilterDialogItem = new JMenuItem("Assign Filter");
        add(theFilterDialogItem);

        theFilterDialogItem.addActionListener(new ActionListener() {

            /**
             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                showFilterDialog();
            }
        });

        addSeparator();

        theInsertXsdItem = new JMenuItem("Insert XSD");
        add(theInsertXsdItem);

        theInsertXsdItem.addActionListener(new ActionListener() {
            /**
             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                new InsertXsdAction(theController).run();
            }
        });

        createGroupingMenu();
    }

    /**
     * Create the grouping menu
     * 
     * @since 2.4
     */
    private void createGroupingMenu() {
        ActionListener groupListener = new ActionListener() {
            /**
             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                new GrouperUpdater(theController);
            }
        };

        // JMenu group = new JMenu("Grouping");
        theGroupItem = new JMenuItem("Group");
        theGroupItem.addActionListener(groupListener);
        // group.add(theGroupItem);

        theUngroupItem = new JMenuItem("Ungroup");
        theUngroupItem.addActionListener(groupListener);
        // group.add(theUngroupItem);
        // add(group);
    }

    /**
     * Create the arrange menu
     * 
     * @since 2.4
     */
    private void createArrangeMenu() {
        JMenu arrange = new JMenu("Arrange");

        theBringToFrontItem = new JMenuItem("Bring to front");
        theBringToFrontItem.addActionListener(new ActionListener() {
            /**
             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                new LayerUpdater(theController,ElementMovement.FRONT);
            }
        });

        theSendToBackItem = new JMenuItem("Send to back");
        theSendToBackItem.addActionListener(new ActionListener() {
            /**
             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                new LayerUpdater(theController,ElementMovement.BACK);
            }
        });

        theBringForwardsItem = new JMenuItem("Bring forward");
        theBringForwardsItem.addActionListener(new ActionListener() {
            /**
             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                new LayerUpdater(theController,ElementMovement.FORWARD);
            }
        });

        theSendBackwardItem = new JMenuItem("Send backward");
        theSendBackwardItem.addActionListener(new ActionListener() {
            /**
             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                new LayerUpdater(theController,ElementMovement.BACKWARD);
            }
        });

        arrange.add(theSendToBackItem);
        arrange.add(theSendBackwardItem);
        arrange.add(theBringForwardsItem);
        arrange.add(theBringToFrontItem);

        add(arrange);
    }

    /**
     * Calculate the bounds of the root graphics node and then set the document
     * size accordingly
     * 
     * @since 2.4
     */
    private void trimCanvas() {
        new TrimCanvasUpdater(theController);
    }

    /**
     * Align the root level elements to grid locations.
     * 
     * @since 2.5
     */
    private void snapToGrid() {
        new SnapToGridUpdater(theController);
    }

    /**
     * select all of the items on the canvas
     * 
     * @since 2.4
     */
    private void selectAllAction() {
        getHighlightManager().removeHighlights();
        theHighlightManager.add(SVGUtils.getSVGElements(theController
            .getManager().getDocument()));
    }

    /**
     * Delete the selected SVG items
     * 
     * @param deleteAnchors true if anchored elements are also to be deleted.
     * @since 2.4
     */
    public void deleteSelectedItems(boolean deleteAnchors) {
        if (getHighlightManager().hasSelectedElements()) {
            new DeleteHighlightedUpdater(theController,deleteAnchors);
        }
    }

    /**
     * Show the class attribute dialog and set values if required.
     * 
     * @since 2.5
     */
    private void showClassDialog() {
        SWTUtils.UIThreadExec(new Runnable() {
            /**
             * @see java.lang.Runnable#run()
             */
            @Override
            public void run() {
                ClassAttributeDialog dialog =
                    theController.getView().getSVGComposite().getClassDialog();

                if (getHighlightManager().getHighlightedElements().size()==1) {
                    // Set current class attribute if present
                    Element element =
                        getHighlightManager().getHighlightedElements().get(0);
                    dialog.setAttributeValue(element.getAttribute("class"));
                }
                else {
                    dialog.setAttributeValue("");
                }

                // Lets generate this list of possible classes
                SVGOMDocument doc =
                    (SVGOMDocument)theController.getManager().getDocument();
                CSSEngine engine = doc.getCSSEngine();
                dialog.setValues(BeermatUtils.getStyleSheetNodes(engine));

                if (dialog.open()==Window.OK) {
                    NameValuePair classAttribute =
                        new NameValuePair("class",dialog.getAttributeValue());
                    new SetAttributesUpdater(theController,
                        createAttributeList(classAttribute),
                        getHighlightManager().getHighlightedElements());
                }
            }
        });
    }

    /**
     * Show the Filter dialog and set attributes if required
     * 
     * @since 2.5
     */
    private void showFilterDialog() {
        SWTUtils.UIThreadExec(new Runnable() {
            /**
             * @see java.lang.Runnable#run()
             */
            @Override
            public void run() {
                FilterAttributeDialog dialog =
                    theController.getView().getSVGComposite().getFilterDialog();
                if (getHighlightManager().getHighlightedElements().size()==1) {
                    Element element =
                        getHighlightManager().getHighlightedElements().get(0);
                    dialog.setAttributeValue(element.getAttribute("filter"));
                }
                else {
                    dialog.setAttributeValue("");
                }

                dialog.setValues(BeermatUtils.getFilters(theController
                    .getManager().getDocument()));
                // http://tutorials.jenkov.com/svg/filters.html
                if (dialog.open()==Window.OK) {
                    NameValuePair filterAttribute =
                        new NameValuePair("filter");

                    String attrValue = dialog.getAttributeValue();
                    if (attrValue.startsWith("url")) {
                        filterAttribute.setValue(attrValue);
                    }
                    else {
                        filterAttribute.setValue("url(#"+attrValue+")");
                    }

                    new SetAttributesUpdater(theController,
                        createAttributeList(filterAttribute),
                        getHighlightManager().getHighlightedElements());
                }
            }
        });
    }

    /**
     * Create a list of one attribute
     * 
     * @param attr to set
     * @return a list of one name value pair
     * @since 2.5
     */
    private List<NameValuePair> createAttributeList(NameValuePair attr) {
        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>(1);
        list.add(attr);
        return list;
    }
}
