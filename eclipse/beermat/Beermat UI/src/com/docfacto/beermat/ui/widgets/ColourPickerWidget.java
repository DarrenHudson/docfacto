package com.docfacto.beermat.ui.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.AttributeChangeEvent;
import com.docfacto.beermat.ui.BeermatViewConstants;
import com.docfacto.beermat.utils.PluginManager;
import com.docfacto.core.utils.SWTUtils;
import com.docfacto.core.widgets.AbstractColorPicker;

/**
 * A widget that allows for the selection of a colour.
 * 
 * The widget has a label, which shows the selected colour. When the label is
 * clicked, a shell will open showing a png of colours.
 * 
 * Using Robot, the selected colour is chosen. The shell also has a close
 * button, which will set the background to the original colour.
 * 
 * The widget has a concept of no colour, this means that no colour is selected
 * and doesn't have a default colour.
 * 
 * {@docfacto.media uri="doc-files/Colour-Picker.png"}
 * 
 * @author dhudson - created 18 Jun 2013
 * @since 2.4
 * @docfacto.link uri="${doc-beermat}/controlbar/c_stroke_widget.dita"
 * key="colour-picker" version="2.4.2"
 * 
 */
public class ColourPickerWidget extends AbstractColorPicker {
    
    private static Color BACKGROUND = BeermatViewConstants.COLOR_BACKGROUND;
    
    private static Color FOREGROUND = BeermatViewConstants.COLOR_FOREGROUND_BRAND;

    private final BeermatController theController;

    private RowData theColourLabelData;

    /**
     * Using this constructor will disable the option of having no colour.
     * 
     * @param controller beermat controller
     * @param toolbar control toolbar
     * @param color to set the default
     * @since 2.4.4
     */
    public ColourPickerWidget(BeermatController controller,Composite toolbar,
    Color color) {
        this(controller,toolbar);
        setColour(color,true);
    }

    /**
     * Using this constructor will enable the none colour and set it to the
     * default
     * 
     * @param controller beermat controller
     * @param toolbar The composite in which this is created in
     * @since 2.4.4
     */
    public ColourPickerWidget(BeermatController controller,Composite toolbar) {
        super(toolbar, true);
        theController = controller;
    }

    /**
     * @see com.docfacto.core.widgets.AbstractColorPicker#createCompositePart(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Composite createCompositePart(Composite parent) {
        // Create a container for the widget
        Composite composite = new Composite(parent,SWT.NONE);
        // Same colour as the toolbar
        composite.setBackground(parent.getBackground());

        RowLayout rowLayout = new RowLayout(SWT.HORIZONTAL);
        rowLayout.marginHeight = 0;
        rowLayout.marginBottom = 0;
        rowLayout.marginTop = 0;
        rowLayout.marginLeft = 3;
        rowLayout.marginRight = 1;
        // rowLayout.justify = true;
        composite.setLayout(rowLayout);
        return composite;
    }

    /**
     * @see com.docfacto.core.widgets.AbstractColorPicker#createColourLabel(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Label createColourLabel(Composite parent) {
        Label label = new Label(parent,SWT.NONE);
        theColourLabelData = new RowData();
        theColourLabelData.width = 18;
        theColourLabelData.height = 16;
        label.setLayoutData(theColourLabelData);
        return label;
    }

    /**
     * @see com.docfacto.core.widgets.AbstractColorPicker#setColourLabelToColour(org.eclipse.swt.graphics.Color)
     */
    @Override
    protected void setColourLabelToColour(Color color) {
        Label theColourLabel = super.getColorLabel();
        theColourLabel.setImage(null);
        theColourLabel.setLayoutData(theColourLabelData);
        theColourLabel.setBackground(color);
    }

    /**
     * @see com.docfacto.core.widgets.AbstractColorPicker#setColorLabelToNone()
     */
    @Override
    protected void setColorLabelToNone() {
        Label theColourLabel = super.getColorLabel();
        theColourLabel.setImage(PluginManager.getImage("icons/close_view.gif"));
        theColourLabel.setBackground(SWTUtils.getColor(SWT.COLOR_WHITE));
    }

    /**
     * @see com.docfacto.core.widgets.AbstractColorPicker#setVisible(boolean)
     */
    @Override
    protected void setVisible(boolean vis) {
        Shell shell = getShell();
        Label theColourLabel = super.getColorLabel();
        if (vis) {
            Rectangle rect = theColourLabel.getBounds();
            Point pt = theColourLabel.getParent().toDisplay(new Point(rect.x,rect.y));
            // Removed the setting of bounds, size, everything should be
            // calculated relatively
            shell.setLocation(pt.x,(pt.y+rect.height));
            // pack to work out the size based on all of the children
            // components.
            shell.pack();
            shell.setVisible(true);
            setOriginalColour(theColourLabel.getBackground());
            shell.setFocus();
        }
        else {
            theController.processEvent(new AttributeChangeEvent(this));
            shell.setVisible(false);
        }
    }

    /**
     * @see com.docfacto.core.widgets.AbstractColorPicker#getForegroundColour()
     */
    @Override
    public Color getForegroundColour() {
        return FOREGROUND;
    }

    /**
     * @see com.docfacto.core.widgets.AbstractColorPicker#getBackgroundColour()
     */
    @Override
    public Color getBackgroundColour() {
        return BACKGROUND;
    }
}
