package com.docfacto.beermat.ui.xmleditor;

import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.texteditor.ITextEditorActionConstants;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.plugin.IBeermatEditor;
import com.docfacto.core.editors.xmleditor.XMLConfiguration;

/**
 * XML Editor
 * 
 * @author dhudson - created 10 Jul 2013
 * @since 2.4
 */
public class XMLEditor extends TextEditor implements IBeermatEditor {

    private final XMLDocumentProvider theDocumentProvider;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @since 2.4
     */
    public XMLEditor(BeermatController controller) {
        setSourceViewerConfiguration(new XMLConfiguration());
        theDocumentProvider = new XMLDocumentProvider(controller);
        setDocumentProvider(theDocumentProvider);
    }

    /**
     * @see org.eclipse.ui.editors.text.TextEditor#dispose()
     */
    public void dispose() {
        // ColorManager.dispose();
        super.dispose();
    }

    /**
     * @see com.docfacto.core.editors.xmleditor.XMLDocumentProvider#hasChanged()
     */
    public boolean hasChanged() {
        return theDocumentProvider.hasChanged();
    }

    /**
     * @see com.docfacto.core.editors.xmleditor.XMLDocumentProvider#resetHasChanged()
     */
    public void resetHasChanged() {
        theDocumentProvider.resetHasChanged();
    }

    /**
     * Returns the text in the editor
     * 
     * @return the text in the editor
     * @since 2.4
     */
    public String getEditorContents() {
        IDocument textdoc = theDocumentProvider.getDocument(getEditorInput());
        return textdoc.get();
    }

    /**
     * Disables the editing functionality from the editor, use for unlicensed copies of beermat.
     * 
     * @since 2.5
     */
    public void disableCopy() {
        setAction(ITextEditorActionConstants.CUT, null);
        setAction(ITextEditorActionConstants.COPY, null);
        setAction(ITextEditorActionConstants.GROUP_COPY, null);

        this.getSourceViewer().getTextWidget().setEditable(false);
    }
}
