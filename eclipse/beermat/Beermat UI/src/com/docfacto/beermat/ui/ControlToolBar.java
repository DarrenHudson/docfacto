package com.docfacto.beermat.ui;

import static com.docfacto.beermat.plugin.PluginConstants.COPY_ATTRIBUTES;
import static com.docfacto.beermat.plugin.PluginConstants.ICON_FOLDER;

import java.util.ArrayList;
import java.util.List;

import org.apache.batik.dom.svg.SVGOMGElement;
import org.apache.batik.dom.svg.SVGOMLineElement;
import org.apache.batik.dom.svg.SVGOMPathElement;
import org.apache.batik.dom.svg.SVGOMTextElement;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ToolItem;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.events.ElementSelectChangeEvent;
import com.docfacto.beermat.events.MarkerChangeEvent;
import com.docfacto.beermat.listeners.BeermatEventListener;
import com.docfacto.beermat.ui.toolbar.GrouperPaletteItem;
import com.docfacto.beermat.ui.toolbar.LayerUpDownPaletteItem;
import com.docfacto.beermat.ui.widgets.FillWidget;
import com.docfacto.beermat.ui.widgets.FontWidget;
import com.docfacto.beermat.ui.widgets.SelectorWidget;
import com.docfacto.beermat.ui.widgets.StrokeWidget;
import com.docfacto.beermat.updaters.SetAttributesUpdater;
import com.docfacto.beermat.utils.ElementAttributes;
import com.docfacto.beermat.utils.ElementMovement;
import com.docfacto.beermat.utils.ElementUtils;
import com.docfacto.beermat.utils.PluginManager;
import com.docfacto.common.NameValuePair;
import com.docfacto.core.utils.SWTUtils;

/**
 * The control tool bar allows for setting of fill colour, line style and width etc..
 * 
 * @author dhudson - created 17 Jun 2013
 * @since 2.4
 * @docfacto.link key="controlToolbar" uri="${doc-beermat}/controlbar/c_control_bar.dita" link-to="doc"
 */
public class ControlToolBar extends AbstractControlToolBar implements
BeermatEventListener {

    private StrokeWidget theStrokeWidget;
    private FillWidget theFillWidget;
    private FontWidget theFontWidget;

    private ElementAttributes theCopiedAttributes;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param parent parent composite
     * @param view Beermat view
     * @since 2.4
     */
    public ControlToolBar(BeermatController controller,Composite parent,BeermatView view) {
        super(parent,view,controller,SWT.FILL);
        layoutUI();
        controller.addListener(this);
    }

    /**
     * Layout the toolbar
     * 
     * @since 2.4
     */
    protected void layoutUI() {
        new SelectorWidget(this);

        addSeparator();

        theStrokeWidget = new StrokeWidget(this);

        addSeparator();

        theFillWidget = new FillWidget(this);

        addSeparator();

        theFontWidget = new FontWidget(this);

        addSeparator();

        createToolbar();
    }

    /**
     * Creates the PaletteItemToolBar with items pertaining to element changes.
     * 
     * @since 2.4.5
     * @docfacto.link key="groupElementToolItem" uri="${doc-beermat}/controlbar/c_group_tool_palette_item.dita"
     * link-to="doc"
     * @docfacto.link key="copyAttributeToolItem" uri="${doc-beermat}/controlbar/c_copy_attribute_item.dita"
     * link-to="doc"
     */
    private void createToolbar() {
        Composite toolbar = new Composite(this,SWT.NONE);
        PaletteItemToolBar palette = new PaletteItemToolBar(toolbar,getController(),SWT.HORIZONTAL);

        final ToolItem copier = new ToolItem(palette.getToolBar(),SWT.CHECK);
        copier.setImage(PluginManager.getImage(ICON_FOLDER+COPY_ATTRIBUTES));
        copier.setToolTipText("Copy attributes from selected element");

        palette.addItem(new GrouperPaletteItem(getController()),SWT.PUSH);
        palette.addItem(new LayerUpDownPaletteItem(getController(),ElementMovement.FORWARD),SWT.PUSH);
        palette.addItem(new LayerUpDownPaletteItem(getController(),ElementMovement.BACKWARD),SWT.PUSH);

        copier.setSelection(false);
        copier.addSelectionListener(new SelectionAdapter() {
            /**
             * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent e) {
                boolean select = checkIfCanEnableCopier(copier.getSelection());
                copier.setSelection(select);
            }

        });
    }

    public ArrayList<NameValuePair> getElementAttributes(final Element element) {
        final ArrayList<NameValuePair> attrList = new ArrayList<NameValuePair>(5);
        // To collect information from the widgets this needs to be in the SWT UI thread
        SWTUtils.UIThreadExecAndWait(new Runnable() {
            @Override
            public void run() {
                attrList.addAll(theStrokeWidget.getAttributesForElement(element));
                attrList.addAll(theFillWidget.getAttributesForElement(element));
            }
        });
        return attrList;
    }

    public ArrayList<NameValuePair> getFontAttributes() {
        final ArrayList<NameValuePair> attrList = new ArrayList<NameValuePair>(5);
        SWTUtils.UIThreadExecAndWait(new Runnable() {
            /**
             * @see java.lang.Runnable#run()
             */
            @Override
            public void run() {
                attrList.addAll(theFontWidget.getAttributesForElement());
            }
        });
        return attrList;
    }

    /**
     * Return the font widget
     * 
     * @return the font widget
     * @since 2.4
     */
    public FontWidget getFontWidget() {
        return theFontWidget;
    }

    /**
     * Sets whether the copier is on or not
     * 
     * @param selected the theCopierSelected value
     */
    boolean checkIfCanEnableCopier(boolean selected) {
        if (selected) {
            List<Element> elements =
                getController().getBeermatAWTCanvas().getHighlightManager().getHighlightedElements();
            if (elements.size()==1&&!(elements.get(0) instanceof SVGOMGElement)) {
                theCopiedAttributes = ElementUtils.getElementAttributes(elements.get(0));
                return true;
            }
        }
        theCopiedAttributes = null;
        return false;
    }

    /**
     * @see com.docfacto.beermat.listeners.BeermatEventListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    @Override
    public void handleEvent(BeermatEvent event) {
        if (event.isMarkerChangeEvent()) {
            MarkerChangeEvent markerevent = (MarkerChangeEvent)event;
            switch (markerevent.getPosition()) {
            case BOTH:
                theStrokeWidget.setMarkerEnd(markerevent.getID());
                theStrokeWidget.setMarkerStart(markerevent.getID());
                break;
            case START:
                theStrokeWidget.setMarkerStart(markerevent.getID());
                break;
            case END:
                theStrokeWidget.setMarkerEnd(markerevent.getID());
                break;
            }
        }
        else if (event.isElementSelectChangeEvent()) {
            ElementSelectChangeEvent changeEvent = (ElementSelectChangeEvent)event;
            Element element = changeEvent.getElement();

            if (theCopiedAttributes!=null) {
                List<Element> allElements = ElementUtils.getElementChildren(element);
                List<Class<?>> removals = new ArrayList<Class<?>>(2);
                removals.add(SVGOMGElement.class);
                removals.add(SVGOMTextElement.class);
                final List<Element> children = ElementUtils.filterElementType(allElements,removals);

                List<NameValuePair> attrs = new ArrayList<NameValuePair>(4);
                attrs.add(new NameValuePair("stroke",theCopiedAttributes.getStrokeColour()));
                attrs.add(new NameValuePair("stroke-width",theCopiedAttributes.getStrokeWidth()));
                attrs.add(new NameValuePair("fill",theCopiedAttributes.getFill()));
                attrs.add(new NameValuePair("stroke-dasharray",theCopiedAttributes.getDash()));

                new SetAttributesUpdater(getController(),attrs,children);

            }
            else {
                // currently nothing to handle text element selections
                if (element instanceof SVGOMTextElement) {
                    return;
                }
                // Do something, like set the colour to a '?'
                else if (element instanceof SVGOMGElement) {
                }
                else {
                    final ElementAttributes attr = ElementUtils.getElementAttributes(element);
                    if (element instanceof SVGOMLineElement||element instanceof SVGOMPathElement) {
                        if (element.hasAttribute("marker-start")) {
                            String id = element.getAttribute("marker-start").replace("url(#","").replace(")","");
                            attr.setMarkerStart(id);
                        }
                        if (element.hasAttribute("marker-end")) {
                            String id = element.getAttribute("marker-end").replace("url(#","").replace(")","");
                            attr.setMarkerEnd(id);
                        }
                    }
                    SWTUtils.UIThreadExec(new Runnable() {
                        @Override
                        public void run() {
                            theStrokeWidget.setSelectedColour(attr.getStrokeColour());
                            theStrokeWidget.setSelectedStrokeDash(attr.getDash());
                            theStrokeWidget.setSelectedStrokeWidth(attr.getStrokeWidth());
                            theStrokeWidget.setMarkerStart(attr.getMarkerStart());
                            theStrokeWidget.setMarkerEnd(attr.getMarkerEnd());
                            theFillWidget.setSelectedColour(attr.getFill());
                        }
                    });
                }
            }
        }
    }
}
