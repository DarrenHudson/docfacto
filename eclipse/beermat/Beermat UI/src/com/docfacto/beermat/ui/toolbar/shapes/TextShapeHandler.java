package com.docfacto.beermat.ui.toolbar.shapes;

import static com.docfacto.beermat.plugin.PluginConstants.ICON_FOLDER;
import static com.docfacto.beermat.plugin.PluginConstants.TEXT_ORANGE;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.dialogs.AbstractTextEntryDialog;
import com.docfacto.beermat.utils.PluginManager;
import com.docfacto.core.utils.SWTUtils;

/**
 * Handle a text entry.
 * 
 * @author kporter - created Jun 25, 2013
 * @since 2.4
 */
public class TextShapeHandler extends AbstractShapeHandler {

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @since 2.4
     */
    public TextShapeHandler(BeermatController controller) {
        super(controller);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        return "Text";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
     */
    @Override
    public Image getImage() {
        return PluginManager.getImage(getImagePath());
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        return ICON_FOLDER+TEXT_ORANGE;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        return "Text Tool (T)";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.AbstractPaletteItem#getShortcutKey()
     */
    @Override
    public char getShortcutKey() {
        return 't';
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.AbstractShapeHandler#mouseClick(org.eclipse.swt.graphics.Point,
     * Element)
     */
    @Override
    public Element mouseClick(final Point location,final Element lastElement) {
        SWTUtils.UIThreadExec(new Runnable() {
            /**
             * @see java.lang.Runnable#run()
             */
            @Override
            public void run() {

                AbstractTextEntryDialog dialog =
                    getController().getView().getSVGComposite().getTextDialog();

                // Set the location
                dialog.setLocation(location.x,location.y);

                // We are not editing, this is done as its a shared widget
                dialog.setElement(null);

                if (dialog.open()==Window.OK) {
                    if (dialog.processText(getController().getManager()
                        .getDocument())) {
                    }
                }
            }
        });
        return null;
    }

}
