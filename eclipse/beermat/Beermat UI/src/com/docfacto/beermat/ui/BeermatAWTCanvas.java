package com.docfacto.beermat.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import org.apache.batik.gvt.AbstractGraphicsNode;
import org.apache.batik.gvt.CompositeGraphicsNode;
import org.apache.batik.gvt.CompositeShapePainter;
import org.apache.batik.gvt.FillShapePainter;
import org.apache.batik.gvt.GraphicsNode;
import org.apache.batik.gvt.StrokeShapePainter;
import org.apache.batik.gvt.TextNode;
import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.swing.gvt.GVTTreeRendererAdapter;
import org.apache.batik.swing.gvt.GVTTreeRendererEvent;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.events.CanvasAction;
import com.docfacto.beermat.events.ColourChangeEvent;
import com.docfacto.beermat.events.GridEvent;
import com.docfacto.beermat.events.GridSizeEvent;
import com.docfacto.beermat.events.SVGSizeEvent;
import com.docfacto.beermat.events.ZoomChangeEvent;
import com.docfacto.beermat.highlight.HighlightManager;
import com.docfacto.beermat.listeners.BeermatEventListener;
import com.docfacto.beermat.listeners.BeermatInputListenerDelegate;
import com.docfacto.beermat.overlay.BeermatOverlay;
import com.docfacto.beermat.svg.BeermatUserAgent;
import com.docfacto.beermat.utils.BeermatUtils;
import com.docfacto.beermat.utils.SVGUtils;
import com.docfacto.core.utils.SWTUtils;

/**
 * Wrapper class round {@code JSVGCanvas }
 * 
 * @author dhudson - created 24 Jun 2013
 * @since 2.4
 */
public class BeermatAWTCanvas extends JSVGCanvas implements
BeermatEventListener {

    private static final long serialVersionUID = 1L;
    private final BeermatController theController;

    private BeermatInputListenerDelegate theInputListener;

    private org.eclipse.swt.graphics.Color theGridColor =
        BeermatViewConstants.BEERMAT_GRID_COLOR;

    private double theGridSize = 20D;

    private boolean isGridRequired = false;

    private final HighlightManager theHighlightManager;

    private final List<BeermatOverlay> theOverlays;

    private boolean hasCanvasChanged;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param userAgent user agent
     * @since 2.4
     */
    public BeermatAWTCanvas(BeermatController controller,
    BeermatUserAgent userAgent) {
        // user agent, events enabled, selectable Text
        super(userAgent,false,false);

        theController = controller;
        theOverlays = new ArrayList<BeermatOverlay>(5);

        setDocumentState(JSVGCanvas.ALWAYS_DYNAMIC);
        // setRecenterOnResize(true);
        setDoubleBufferedRendering(true);
        setDoubleBuffered(true);

        // Make transparent
        setBackground(new Color(0,0,0,0));
        setOpaque(false);

        // Create the highlight manager
        theHighlightManager = new HighlightManager(controller,this);

        addEventListeners();

        // Turn the rotate feature off
        setEnableRotateInteractor(false);

        // Turn off pan interactor
        setEnablePanInteractor(false);
    }

    /**
     * Returns the highlight manager
     * 
     * @return the highlight manager
     * @since 2.4
     */
    public HighlightManager getHighlightManager() {
        return theHighlightManager;
    }

    /**
     * Add required event listeners for the AWT Canvas
     * 
     * @since 2.4
     */
    private void addEventListeners() {

        // Register for beermat events
        theController.addListener(this);

        theInputListener =
            new BeermatInputListenerDelegate(theController,this);

        // Register the delegate for all events
        addKeyListener(theInputListener);
        addMouseListener(theInputListener);
        addMouseMotionListener(theInputListener);
        addMouseWheelListener(theInputListener);

        addGVTTreeRendererListener(new GVTTreeRendererAdapter() {
            /**
             * @see org.apache.batik.swing.gvt.GVTTreeRendererAdapter#gvtRenderingCompleted(org.apache.batik.swing.gvt.GVTTreeRendererEvent)
             */
            @Override
            public void gvtRenderingCompleted(GVTTreeRendererEvent e) {
                // Lets redraw the image so it fits to right
                getActionMap().get(JSVGCanvas.RESET_TRANSFORM_ACTION)
                    .actionPerformed(null);
                requestFocusInWindow();
                theController.processEvent(new SVGSizeEvent(this,
                    BeermatAWTCanvas.this));
                requestFocus();
            }
        });
    }

    /**
     * Add overlay
     * 
     * @param overlay to add
     * @since 2.4
     */
    public void addOverlay(BeermatOverlay overlay) {
        theOverlays.add(overlay);
    }

    /**
     * Remove an overlay
     * 
     * {@docfacto.note Not thread safe }
     * 
     * @param overlay to remove
     * @since 2.4
     */
    public void removeOverlay(BeermatOverlay overlay) {
        theOverlays.remove(overlay);
    }
    
    /**
     * Returns the input delegate
     * 
     * @return the input delegate
     * @since 2.4
     */
    public BeermatInputListenerDelegate getInputDelegate() {
        return theInputListener;
    }

    /**
     * @see org.apache.batik.swing.JSVGCanvas#installSVGDocument(org.w3c.dom.svg.SVGDocument)
     */
    @Override
    protected void installSVGDocument(SVGDocument doc) {
        // Add the docfacto namespace..
        SVGUtils.injectDocfactoNamespace(doc);

        super.installSVGDocument(doc);
    }

    /**
     * At a given X, Y location return a graphics node.
     * 
     * This will take into account any feathering if required
     * 
     * @param x location
     * @param y location
     * @param feather the area of sensitivity around the shape to take into
     * account
     * @return A graphics node at that point or null
     * @since 2.4
     */
    public GraphicsNode getGraphicsNodeAt(int x,int y,int feather) {

        if (bridgeContext==null||gvtRoot==null) {
            // Nothing to do yet
            return null;
        }

        return getGraphicsNodeAt(new Point2D.Double((double)x,(double)y),
            feather,getCanvasGraphicsNode(),null);
    }

    /**
     * Return an element at a given location or null
     * 
     * @param x location
     * @param y location
     * @return An element at a given location or null
     * @since 2.4
     */
    public Element getElementAt(int x,int y) {
        return getElementFor(getGraphicsNodeAt(x,y,0));
    }

    /**
     * Given a SVG element, return the graphics node
     * 
     * @param element SVG element
     * @return the graphics node that represents the SVG element
     * @since 2.4
     */
    public GraphicsNode getGraphicsNodeFor(Element element) {
        if (element==null) {
            return null;
        }

        if (bridgeContext!=null) {
            return bridgeContext.getGraphicsNode(element);
        }

        return null;
    }

    /**
     * Return a graphics node for a given element
     * 
     * @param graphicsNode to check
     * @return a graphics node for a given element or null
     * @since 2.4
     */
    public Element getElementFor(GraphicsNode graphicsNode) {
        if (bridgeContext!=null) {
            return bridgeContext.getElement(graphicsNode);
        }

        return null;
    }

    /**
     * Return a list of possible nodes at one point
     * 
     * @param location to check
     * @param feather amount of feathering
     * @param element to ignore
     * @return a list of nodes that share the same spot
     * @since 2.4
     */
    public List<GraphicsNode> getGraphicsNodesAt(Point2D location,int feather,
    Element element) {
        ArrayList<GraphicsNode> graphicsNodes = new ArrayList<GraphicsNode>(3);
        getGraphicsNodesAt(location,feather,getCanvasGraphicsNode(),element,
            graphicsNodes);
        return graphicsNodes;
    }

    /**
     * A recursive method to add elements to a list
     * 
     * @param location to check
     * @param feather amount of feathering
     * @param root to traverse the children of
     * @param element to ignore
     * @param nodes list to append to
     * @since 2.4
     */
    private void getGraphicsNodesAt(Point2D location,int feather,
    CompositeGraphicsNode root,Element element,List<GraphicsNode> nodes) {
        Rectangle2D bounds = root.getSensitiveBounds();

        if (bounds==null) {
            // Nothing has been drawn
            return;
        }

        // Need to include the feathering here ..
        bounds = BeermatUtils.outset(bounds.getBounds(),feather);

        if (!bounds.contains(location)) {
            // The mouse location is not near the bounds of the image
            return;
        }

        @SuppressWarnings("unchecked")
        List<GraphicsNode> list = root.getChildren();

        // Need to do this in reverse order as this is the order of painting
        for (int i = list.size()-1;i>=0;--i) {

            GraphicsNode child = list.get(i);

            AffineTransform t = child.getInverseTransform();
            Point2D pt = null;
            Point2D cp = null; // Propagated to children

            if (t!=null) {
                pt = t.transform(location,pt);
                cp = pt;
            }
            else {
                cp = location;
            }

            if (child instanceof CompositeGraphicsNode) {
                getGraphicsNodesAt(cp,feather,
                    (CompositeGraphicsNode)child,element,nodes);

                continue;
            }

            AbstractGraphicsNode shapeNode = (AbstractGraphicsNode)child;

            if (getGraphicsNodeFor(element)==shapeNode) {
                // We don't want this one..
                continue;
            }

            if (shapeNode instanceof TextNode) {
                // Want to return the bounds of a text box rather than its
                // outline
                if (shapeNode.contains(cp)) {
                    nodes.add(child);
                }
            }
            else {
                // Create a shape painter
                CompositeShapePainter compositePainter =
                    new CompositeShapePainter(shapeNode.getOutline());

                // Set the stroke to the feather size, so that as we get close
                // the the shape
                // we get a hit
                StrokeShapePainter painter =
                    new StrokeShapePainter(shapeNode.getOutline());
                painter.setStroke(new BasicStroke((float)feather));
                painter.setPaint(Color.black);

                // Add the painter
                compositePainter.addShapePainter(painter);

                // Create a fill, so that empty shapes get hit as well
                FillShapePainter fillPainter =
                    new FillShapePainter(shapeNode.getOutline());
                fillPainter.setPaint(Color.black);

                // Add the painter
                compositePainter.addShapePainter(fillPainter);

                // Is the location a hit?
                if (compositePainter.inPaintedArea(cp)) {
                    nodes.add(child);
                }
            }
        }
    }

    /**
     * Empty boxes are hard to select because GraphicsNode.contains, will not
     * return true if the shape has an empty fill.
     * 
     * {@docfacto.note The old way of doing this was rootNode.nodeHitAt(). The
     * problem is it doesn't handle empty shapes. Text is done by its bounds
     * rather than its shape }
     * 
     * {@docfacto.note This also detects group text nodes, if the group only
     * contains text, then the group will be returned. The reason of this was
     * because if there is a group of text, you don't want the gaps between the
     * text to return null}
     * 
     * @param location of the mouse
     * @param feather the extra bounds to increase the search by
     * @param root node to search
     * @param element to ignore
     * @return a graphics node is the bounds contain the point
     * @since 2.4
     */
    public GraphicsNode getGraphicsNodeAt(Point2D location,int feather,
    CompositeGraphicsNode root,Element element) {

        Rectangle2D bounds = root.getSensitiveBounds();

        if (bounds==null) {
            // Nothing has been drawn
            return null;
        }

        // Need to include the feathering here ..
        bounds = BeermatUtils.outset(bounds.getBounds(),feather);

        if (!bounds.contains(location)) {
            // The mouse location is not near the bounds of the image
            return null;
        }

        @SuppressWarnings("unchecked")
        List<GraphicsNode> list = root.getChildren();

        // Need to do this in reverse order as this is the order of painting
        for (int i = list.size()-1;i>=0;--i) {

            GraphicsNode child = list.get(i);

            AffineTransform t = child.getInverseTransform();
            Point2D pt = null;
            Point2D cp = null; // Propagated to children

            if (t!=null) {
                pt = t.transform(location,pt);
                cp = pt;
            }
            else {
                cp = location;
            }

            if (child instanceof CompositeGraphicsNode) {

                // If the composite just contains text nodes, then its group
                // text
                CompositeGraphicsNode text = (CompositeGraphicsNode)child;
                boolean isGroupText = true;

                for (int j = 0;j<text.getChildren().size();j++) {
                    if (!(text.getChildren().get(j) instanceof TextNode)) {
                        isGroupText = false;
                    }
                }

                if (isGroupText) {
                    Rectangle2D groupBounds = text.getSensitiveBounds();
                    if (groupBounds != null && groupBounds.contains(cp)) {
                        return child;
                    }
                }

                // Recurse
                GraphicsNode hit =
                    getGraphicsNodeAt(cp,feather,
                        (CompositeGraphicsNode)child,element);
                if (hit!=null) {
                    return hit;
                }

                continue;
            }

            AbstractGraphicsNode shapeNode = (AbstractGraphicsNode)child;

            if (getGraphicsNodeFor(element)==shapeNode) {
                // We don't want this one..
                continue;
            }

            if (shapeNode instanceof TextNode) {

                // Want to return the bounds of a text box rather than its
                // outline
                if (shapeNode.contains(cp)) {
                    return child;
                }
            }
            else {
                // Create a shape painter
                CompositeShapePainter compositePainter =
                    new CompositeShapePainter(shapeNode.getOutline());

                // Set the stroke to the feather size, so that as we get close
                // the the shape
                // we get a hit
                StrokeShapePainter painter =
                    new StrokeShapePainter(shapeNode.getOutline());
                painter.setStroke(new BasicStroke((float)feather));
                painter.setPaint(Color.black);

                // Add the painter
                compositePainter.addShapePainter(painter);

                // Create a fill, so that empty shapes get hit as well
                FillShapePainter fillPainter =
                    new FillShapePainter(shapeNode.getOutline());
                fillPainter.setPaint(Color.black);

                // Add the painter
                compositePainter.addShapePainter(fillPainter);

                // Is the location a hit?
                if (compositePainter.inPaintedArea(cp)) {
                    return child;
                }
            }
        }

        return null;
    }

    /**
     * @see com.docfacto.beermat.listeners.BeermatEventListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    @Override
    public void handleEvent(BeermatEvent event) {

        switch (event.getAction()) {
        case BeermatEvent.ZOOM_ACTION:
            CanvasAction canvasAction = (CanvasAction)event;
            canvasAction.performAction(getActionMap());
            break;

        case BeermatEvent.GRID_ACTION:
            GridEvent gridEvent = (GridEvent)event;
            isGridRequired = gridEvent.isGridRequired();
            repaint();
            break;

        case BeermatEvent.COLOUR_CHANGE_ACTION:
            ColourChangeEvent colourChange = (ColourChangeEvent)event;
            theGridColor = colourChange.getColor();
            repaint();
            break;

        case BeermatEvent.GRID_SIZE_EVENT_ACTION:
            GridSizeEvent gridSizeEvent = (GridSizeEvent)event;
            theGridSize = gridSizeEvent.getSize();
            repaint();
            break;
        }
    }

    /**
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     */
    @Override
    public void paintComponent(Graphics g) {
        if (getSVGDocumentSize()==null) {
            return;
        }

        // Draw the white canvas
        drawBackground(g);

        if (isGridRequired) {
            paintGrid(g);
        }

        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
        
        for (BeermatOverlay overlay:theOverlays) {
            overlay.paint(g2d,hasCanvasChanged);
        }

        hasCanvasChanged = false;
    }

    /**
     * Get Beermat Graphics
     * 
     * @return Beermat graphics with Anti Alias on
     * @since 2.5
     */
    public Graphics2D getOverlayGraphics() {
        Graphics2D g2d = (Graphics2D)getGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
        return g2d;
    }

    /**
     * Return the bounds of the SVG canvas
     * 
     * @return the bounds of the SVG canvas
     * @since 2.4
     */
    private Rectangle getCanvasBounds() {
        AffineTransform transform =
            theController.getViewBoxTransform();

        Rectangle svgCanvas =
            new Rectangle2D.Double(0,0,getSVGDocumentSize().getWidth(),
                getSVGDocumentSize().getHeight()).getBounds();

        svgCanvas.x = BeermatUtils.convertToInt(transform.getTranslateX());
        svgCanvas.y = BeermatUtils.convertToInt(transform.getTranslateY());

        double width = svgCanvas.getWidth()*transform.getScaleX();
        double height = svgCanvas.getHeight()*transform.getScaleY();

        svgCanvas.width = BeermatUtils.convertToInt(width);
        svgCanvas.height = BeermatUtils.convertToInt(height);
        return svgCanvas;
    }

    /**
     * Paint the white background of the canvas
     * 
     * @param g graphics
     * @since 2.4
     */
    private void drawBackground(Graphics g) {
        Rectangle svgCanvas = getCanvasBounds();

        // Lets draw a white rect to represent the background of the
        // BeermatCanvas
        g.setColor(Color.white);
        g.fillRect(svgCanvas.x,svgCanvas.y,svgCanvas.width,svgCanvas.height);
    }

    /**
     * Paint the grid
     * 
     * @param g graphics
     * @since 2.4
     */
    private void paintGrid(Graphics g) {
        AffineTransform transform =
            theController.getViewBoxTransform();

        Rectangle svgCanvas = getCanvasBounds();

        double gridSizeDouble = theGridSize*transform.getScaleX();
        int gridSize = BeermatUtils.convertToInt(gridSizeDouble);

        g.setColor(SWTUtils.toAWTColor(theGridColor));

        // Draw horiz lines
        for (int y = svgCanvas.y;y<=(svgCanvas.y+svgCanvas.height);y +=
            gridSize) {
            g.drawLine(svgCanvas.x,y,(svgCanvas.width+svgCanvas.x),y);
        }

        // Draw vert lines
        for (int x = svgCanvas.x;x<=(svgCanvas.x+svgCanvas.width);x +=
            gridSize) {
            g.drawLine(x,svgCanvas.y,x,(svgCanvas.height+svgCanvas.y));
        }
    }

    /**
     * Returns gridColor.
     * 
     * @return the gridColor
     * @since 2.4
     */
    public org.eclipse.swt.graphics.Color getGridColor() {
        return theGridColor;
    }

    /**
     * Sets gridColor.
     * 
     * @param gridColor the gridColor value
     * @since 2.4
     */
    public void setGridColor(org.eclipse.swt.graphics.Color gridColor) {
        theGridColor = gridColor;
    }

    /**
     * Returns theGridSize.
     * 
     * @return the theGridSize
     * @since 2.4
     */
    public double getGridSize() {
        return theGridSize;
    }

    /**
     * Sets theGridSize.
     * 
     * @param gridSize the grid size value
     * @since 2.4
     */
    public void setGridSize(double gridSize) {
        theGridSize = gridSize;
    }

    /**
     * Sets the scale of the rendering transform
     * 
     * @param scale to set
     * @since 2.4
     */
    public void setCanvasScale(double scale) {
        AffineTransform transform = getRenderingTransform();
        transform.setToScale(scale,scale);
        setRenderingTransform(transform);
        repaint();
    }

    /**
     * Sets the rendering transform to the canvas As well as setting the
     * transform, the method notifies the controller of the changes.
     * 
     * @see org.apache.batik.swing.gvt.JGVTComponent#setRenderingTransform(java.awt.geom.AffineTransform)
     */
    @Override
    public void setRenderingTransform(AffineTransform transform) {
        hasCanvasChanged = true;

        super.setRenderingTransform(transform);
        theController.processEvent(new ZoomChangeEvent(this,transform
            .getScaleX()));
    }

    /**
     * @see org.apache.batik.swing.gvt.JGVTComponent#setPaintingTransform(java.awt.geom.AffineTransform)
     */
    @Override
    public void setPaintingTransform(AffineTransform transform) {
        super.setPaintingTransform(transform);
        hasCanvasChanged = true;
    }

    /**
     * @see javax.swing.JComponent#getToolTipLocation(java.awt.event.MouseEvent)
     */
    @Override
    public Point getToolTipLocation(MouseEvent event) {
        Point location = event.getPoint();
        location.y += 80;
        location.x += 20;
        return location;
    }

}
