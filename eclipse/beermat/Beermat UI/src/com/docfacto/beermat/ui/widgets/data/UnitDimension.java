package com.docfacto.beermat.ui.widgets.data;

import static com.docfacto.beermat.ui.widgets.data.UnitConstants.*;

public class UnitDimension{
    private final static float CM_IN_INCHES = 2.54f;
    private final static float MM_IN_INCHES = CM_IN_INCHES*10;

    private int thePPI;
    private float thePixel;
    
    public UnitDimension(int res){
        thePPI = res;
    }

    public void setUnit(String unit,String value) {
        float fValue = Float.parseFloat(value);
        if(unit.equals(PIXELS) || unit.equals(POINTS)){
            setPixels(fValue);
        }
        else if(unit.equals(CM)){
            setCm(fValue);
        }
        else if(unit.equals(MM)){
            setMm(fValue);
        }
        else if(unit.equals(INCH)){
            setInch(fValue);
        }
    }
    
    public float getUnit(String unit){
        if(unit.equals(PIXELS) || unit.equals(POINTS)){
                return thePixel;
        }
        else if(unit.equals(CM)){
                return getCm();
        }
        else if(unit.equals(MM)){
                return getMm();
        }
        else if(unit.equals(INCH)){
                return getInch();
        }
        return -1f;
    }
    

    
    public void setResolution(int res){
        thePPI = res;
    }
    

    public float getPC(){
        return 1f;
    }
    public void setPC(){
        
    }
    
    public float getPixels(){
        return thePixel;
    }
    
    public void setPixels(float px){
        thePixel = px;
    }
    
    public float getPT(){
        return getPixels();
    }
    public void setPT(float px){
        setPixels(px);
    }
    
    public float getCm(){
        return pixelsToCM(thePixel, thePPI);            
    }
    
    public float getInch(){
        return pixelsToInches(thePixel, thePPI);            
    }
    
    public float getMm(){
        return pixelsToMM(thePixel, thePPI);            
    }
    
    public void setCm(float cm){
        thePixel = cmToPixels(cm, thePPI);
    }
    
    public void setInch(float inches){
        thePixel = calculateINtoPX(inches, thePPI);
    }
    
    public void setMm(float mm){
        thePixel = mmToPixels(mm, thePPI);
    }
    
    public static float pixelsToCM(float px, int ppi){
        return (px * CM_IN_INCHES) / ppi; 
    }
    
    public static float pixelsToMM(float px, int ppi){
        return (px * MM_IN_INCHES) / ppi;
    }
    
    public static float pixelsToInches(float px, int ppi){
        return px / ppi;
    }
    
    public static float cmToPixels(float cm, int ppi){
        return (cm * ppi) / CM_IN_INCHES;
    }
    
    public static float mmToPixels(float mm, int ppi){
        return (mm * ppi) / MM_IN_INCHES;
    }
    
    public static float calculateINtoPX(float inches, int ppi){
        return inches * ppi;
    }
    

}
