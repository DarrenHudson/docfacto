package com.docfacto.beermat.ui.views;

import java.util.EventListener;
import java.util.EventObject;

/**
 * Load Complete Interface
 * 
 * @author kporter - created Apr 8, 2013
 * @since 2.1
 */
public interface LoadCompleteListener extends EventListener {
    /**
     * This event will be fired when the SVG document has loaded.
     * 
     * To get at the CanvasView, use {@code (CanvasView) e.getSource()}
     * 
     * @param e event
     * @since 2.1
     */
    public void loadComplete(EventObject e);
}
