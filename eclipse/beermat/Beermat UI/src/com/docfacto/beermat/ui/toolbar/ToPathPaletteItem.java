package com.docfacto.beermat.ui.toolbar;

import static com.docfacto.beermat.plugin.PluginConstants.ICON_FOLDER;

import org.eclipse.swt.graphics.Image;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.utils.PluginManager;

/**
 * Converts a selected element to a path.
 * 
 * @author kporter - created Jun 24, 2013
 * @since 2.4
 */
public class ToPathPaletteItem extends AbstractPaletteItem {

    public ToPathPaletteItem(BeermatController controller) {
        super(controller);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        return "ToPath";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
     */
    @Override
    public Image getImage() {
        return PluginManager.getImage(getImagePath());
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        return "Convert to Path Tool";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#performAction(com.docfacto.beermat.controller.BeermatController)
     */
    @Override
    public void performAction() {
   
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        return ICON_FOLDER+"batik.gif";
    }
}
