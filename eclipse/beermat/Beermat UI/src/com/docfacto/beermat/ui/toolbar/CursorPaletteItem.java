package com.docfacto.beermat.ui.toolbar;
import static com.docfacto.beermat.plugin.PluginConstants.ICON_FOLDER;

import org.eclipse.swt.graphics.Image;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.utils.PluginManager;

/**
 * Cursor selection item
 *
 * @author dhudson - created 21 Jun 2013
 * @since 2.4
 */
public class CursorPaletteItem extends AbstractPaletteItem {

    /**
     * Constructor.
     * @param controller
     */
    public CursorPaletteItem(BeermatController controller) {
        super(controller);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        return "Cursor";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
     */
    @Override
    public Image getImage() {
        return PluginManager.getImage(getImagePath());
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        return "Cursor Tool (Q)";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        return ICON_FOLDER+"cursor.png";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.AbstractPaletteItem#getShortcutKey()
     */
    @Override
    public char getShortcutKey() {
        return 'q';
    }
    
    

}
