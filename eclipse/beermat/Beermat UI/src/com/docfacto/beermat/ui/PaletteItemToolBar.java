package com.docfacto.beermat.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.ToolItem;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.PaletteBarChangeEvent;
import com.docfacto.beermat.ui.BeermatViewConstants.ToolbarPopup;
import com.docfacto.beermat.ui.toolbar.PaletteItem;
import com.docfacto.beermat.ui.toolbar.PaletteItemGrouper;
import com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler;

/**
 * Holds a SWT ToolBar that has PaletteItem's
 * 
 * @author kporter - created Aug 23, 2013
 * @since 2.4.3
 */
public class PaletteItemToolBar extends AbstractPaletteItemToolbar {

    final static Color BACKGROUND = BeermatViewConstants.COLOR_TOOLBAR_PRIMARY;
    final static int DEFAULT_ICON_SIZE = BeermatViewConstants.ICON_SIZE;

    private final BeermatController theController;

    /**
     * Constructor. {@docfacto.note a style of SWT.HORIZONTAL or SWT.VERTICAL
     * should be given} {@docfacto.note Must be contained in a composite with
     * fillayout set}
     * 
     * @param style The style int
     * @param parent The parent composite
     * @param controller the BeermatController
     * @since 2.4.3
     * @see org.eclipse.swt.SWT.HORIZONTAL
     * @see org.eclipse.swt.SWT.VERTICAL
     */
    public PaletteItemToolBar(Composite parent,BeermatController controller,int style) {
        super(parent,style);
        theController = controller;
    }

    /**
     * @see org.eclipse.swt.widgets.Listener#handleEvent(org.eclipse.swt.widgets.Event)
     */
    @Override
    public void handleEvent(Event event) {

        ToolItem item = (ToolItem)event.widget;

        PaletteItem paletteItem =
            (PaletteItem)item.getData(PALETTE_ITEM_DATA_KEY);
        ShapeHandler shapeHandler = null;

        if (item.getStyle()==SWT.RADIO) {
            if (!item.getSelection()) {
                // The item has be un-selected, so nothing to do
                return;
            }
            if (paletteItem instanceof PaletteItemGrouper) {
                if (!paletteGrouperOpened(item,paletteItem)) {
                    shapeHandler =
                        (ShapeHandler)((PaletteItemGrouper)paletteItem)
                            .getSelectedItem();
                }
            }
            else {
                shapeHandler =
                    (ShapeHandler)item.getData(SHAPE_HANDLER_DATA_KEY);
            }

            getController().processEvent(new PaletteBarChangeEvent(this,
                paletteItem,shapeHandler));
        }
        else {
            // Handle clear and insert image
            if (paletteItem!=null) {
                if (paletteItem instanceof PaletteItemGrouper) {
                    paletteGrouperOpened(item,paletteItem);
                    ((PaletteItemGrouper)paletteItem).getSelectedItem().performAction();
                }
                else {
                    // Go do the thing if required..
                    paletteItem.performAction();
                }
            }
        }
    }

    /**
     * Opens the grouper
     * 
     * @param item The ToolItem
     * @param paletteItem The paletteitemgrouper
     * @return true if the grouper was opened
     * @since 2.4.5
     */
    private boolean paletteGrouperOpened(ToolItem item,PaletteItem paletteItem) {
        PaletteItemGrouper grouper = (PaletteItemGrouper)paletteItem;

        // Lets see if we can calculate the hotspot
        Point cursorLocation = item.getDisplay().getCursorLocation();

        Rectangle bounds = item.getBounds();
        Point translatedPoint = getToolBar().toDisplay(bounds.x,bounds.y);

        // 12 to take into account padding..
        Rectangle hotSpot;
        int hsSize = 12;
        if (grouper.getPopupDirection()==ToolbarPopup.HORIZONTAL) {
            hotSpot = new Rectangle((translatedPoint.x+bounds.width)-hsSize,translatedPoint.y,hsSize,hsSize);
        }
        else {
            hotSpot = new Rectangle((translatedPoint.x+bounds.width)-hsSize,
                (translatedPoint.y+bounds.height)-hsSize,hsSize,hsSize);
        }

        if (hotSpot.contains(cursorLocation)) {
            grouper.open();
            return true;
        }
        return false;
    }

    /**
     * Get the BeermatController
     * 
     * @return The controller
     * @since 2.4.3
     */
    protected BeermatController getController() {
        return theController;
    }

    /**
     * @see com.docfacto.beermat.ui.AbstractPaletteItemToolbar#getIconSize()
     */
    @Override
    public int getIconSize() {
        return DEFAULT_ICON_SIZE;
    }

    /**
     * @see com.docfacto.beermat.ui.AbstractPaletteItemToolbar#getBackgroundColor()
     */
    @Override
    public Color getBackgroundColor() {
        return BACKGROUND;
    }

}
