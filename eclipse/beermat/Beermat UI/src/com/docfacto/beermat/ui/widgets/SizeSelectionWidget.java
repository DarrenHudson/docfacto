package com.docfacto.beermat.ui.widgets;

import static com.docfacto.beermat.ui.widgets.data.UnitConstants.MM;
import static com.docfacto.beermat.ui.widgets.data.UnitConstants.PIXELS;
import static com.docfacto.beermat.ui.widgets.data.UnitConstants.RESOLUTIONS;
import static com.docfacto.beermat.ui.widgets.data.UnitConstants.UNITS;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.docfacto.beermat.ui.widgets.data.PresetData;
import com.docfacto.beermat.ui.widgets.data.UnitDimension;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.core.widgets.listeners.NumericVerifyListener;

/**
 * A widget for selecting size.
 * 
 * @author kporter - created Jul 17, 2013
 * @since 2.4.2
 */
public class SizeSelectionWidget extends Composite {
    private final static int DEFAULT_RES = 90;

    private Text theWidth;
    private Text theHeight;

    private Combo thePresets;
    private Combo theWidthUnit;
    private Combo theHeightUnit;
    private Combo theResolution;

    private UnitDimension theHeightData;
    private UnitDimension theWidthData;

    private ArrayList<Control> theColourables;
    private NumericVerifyListener theVerify;

    /**
     * Create the composite.
     * 
     * @param parent
     * @param style
     */
    public SizeSelectionWidget(Composite parent,int style) {
        super(parent,style);
        final ArrayList<PresetData> presets = getPresets();
        theColourables = new ArrayList<Control>(5);
        theVerify = new NumericVerifyListener();

        thePresets = new Combo(this,SWT.READ_ONLY);
        thePresets.setBounds(56,6,175,22);
        for (PresetData p:presets) {
            thePresets.add(p.getName());
        }
        thePresets.add("Custom");
        thePresets.select(thePresets.getItemCount()-1);
        thePresets.addSelectionListener(new SelectionAdapter() {
            /**
             * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent e) {
                if (thePresets.getSelectionIndex()<thePresets.getItemCount()-1) {
                    PresetData pd = presets.get(thePresets.getSelectionIndex());
                    theWidthData.setPixels(pd.getWidth());

                    theHeightData.setPixels(pd.getHeight());

                    theResolution.setText(pd.getResolution()+"");
                    theHeightData.setResolution(pd.getResolution());
                    theWidthData.setResolution(pd.getResolution());
                    
                    setSVGSize(new Point((int)(pd.getWidth()),(int)(pd.getHeight())));
                }
            }
        });

        Label lblPreset = new Label(this,SWT.NONE);
        lblPreset.setBounds(10,10,59,14);
        lblPreset.setText("Preset:");
        theColourables.add(lblPreset);

        Label lblWidth = new Label(this,SWT.NONE);
        lblWidth.setBounds(46,38,38,14);
        lblWidth.setText("Width:");
        theColourables.add(lblWidth);

        Label lblHeight = new Label(this,SWT.NONE);
        lblHeight.setBounds(41,62,43,14);
        lblHeight.setText("Height:");
        theColourables.add(lblHeight);

        Label lblResolution = new Label(this,SWT.NONE);
        lblResolution.setText("Resolution:");
        lblResolution.setBounds(20,103,64,14);
        theColourables.add(lblResolution);

        theHeightData = new UnitDimension(DEFAULT_RES);
        theWidthData = new UnitDimension(DEFAULT_RES);

        theWidth = new Text(this,SWT.BORDER);
        theWidth.setBounds(90,36,64,19);
        theWidth.addModifyListener(new ModifyListener() {
            /**
             * @see org.eclipse.swt.events.ModifyListener#modifyText(org.eclipse.swt.events.ModifyEvent)
             */
            @Override
            public void modifyText(ModifyEvent e) {
                if (!theWidth.getText().isEmpty()) {
                    theWidthData.setUnit(theWidthUnit.getText(),
                        theWidth.getText());
                }
            }
        });

        theHeight = new Text(this,SWT.BORDER);
        theHeight.setBounds(90,60,64,19);
        theHeight.addModifyListener(new ModifyListener() {
            /**
             * @see org.eclipse.swt.events.ModifyListener#modifyText(org.eclipse.swt.events.ModifyEvent)
             */
            @Override
            public void modifyText(ModifyEvent e) {
                if (!theHeight.getText().isEmpty()) {
                    theHeightData.setUnit(theHeightUnit.getText(),
                        theHeight.getText());
                }
            }
        });

        theWidthUnit = new Combo(this,SWT.READ_ONLY);
        theWidthUnit.setBounds(160,35,90,22);
        theWidthUnit.setItems(UNITS);
        theWidthUnit.select(0);
        theWidthUnit.addSelectionListener(new ComboSelectionListener());

        theHeightUnit = new Combo(this,SWT.READ_ONLY);
        theHeightUnit.setBounds(160,59,90,22);
        theHeightUnit.setItems(UNITS);
        theHeightUnit.select(0);
        theHeightUnit.addSelectionListener(new ComboSelectionListener());

        theResolution = new Combo(this,SWT.READ_ONLY);
        theResolution.setBounds(90,100,64,22);
        theResolution.setItems(RESOLUTIONS);
        theResolution.select(1);
        theResolution.addSelectionListener(new SelectionAdapter() {
            /**
             * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent e) {
                setResolution();
            }
        });

        Label lblPpi = new Label(this,SWT.NONE);
        lblPpi.setBounds(160,103,59,14);
        lblPpi.setText("PPI");
        theColourables.add(lblPpi);
        
        // Add verify listeners to only allow numeric
        theWidth.addVerifyListener(theVerify);
        theHeight.addVerifyListener(theVerify);
    }

    /**
     * Gets the height entered by the user.
     * 
     * @return The height input as a string
     * @since 2.4.2
     */
    public String getHeightValue() {
        return theHeight.getText();
    }

    /**
     * Gets the width entered by the user.
     * 
     * @return The width input as a string
     * @since 2.4.2
     */
    public String getWidthValue() {
        return theWidth.getText();
    }

    /**
     * Gets the unit selected.
     * 
     * @return The units
     * @since 2.4.2
     */
    public String getHeightUnit() {
        return theHeightUnit.getText();
    }

    /**
     * Gets the unit selected.
     * 
     * @return The units
     * @since 2.4.2
     */
    public String getWidthUnits() {
        return theWidthUnit.getText();
    }

    public int getWidthAsPixels() {
        return (int)theWidthData.getPixels();
    }

    public int getHeightAsPixels() {
        return (int)theHeightData.getPixels();
    }

    protected void setResolution() {
        theHeightData.setResolution(Integer.parseInt(theResolution.getText()));
        theWidthData.setResolution(Integer.parseInt(theResolution.getText()));
        if (!theWidth.getText().isEmpty()&&!theHeight.getText().isEmpty()) {
            theWidth.setText(theWidthData.getUnit(theWidthUnit.getText())+"");
            theHeight
                .setText(theHeightData.getUnit(theHeightUnit.getText())+"");
        }
    }

    protected void setUnits() {
        theWidth.setText(theWidthData.getUnit(theWidthUnit.getText())+"");
        theHeight.setText(theHeightData.getUnit(theHeightUnit.getText())+"");
    }

    class ComboSelectionListener implements SelectionListener {
        /**
         * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
         */
        @Override
        public void widgetSelected(SelectionEvent e) {
            Combo combo = (Combo)e.widget;
            theHeightUnit.select(combo.getSelectionIndex());
            theWidthUnit.select(combo.getSelectionIndex());

            // If both boxes are empty, do nothing.
            // If one of the boxes is empty, and the unit is changing, then copy
            // the value to the empty box.
            if (theWidth.getText().isEmpty()&&theHeight.getText().isEmpty()) {
                return;
            }
            else if (theWidth.getText().isEmpty()&&
                !theHeight.getText().isEmpty()) {
                theWidth.setText("1");
                theWidthData.setPixels(1);
                DocfactoCorePlugin.logException(
                    "Size unit must be a value above 1",null);
            }
            else if (!theWidth.getText().isEmpty()&&
                theHeight.getText().isEmpty()) {
                theHeight.setText("1");
                theWidthData.setPixels(1);
                DocfactoCorePlugin.logException(
                    "Size unit must be a value above 1",null);
            }
            setUnits();
        }

        /**
         * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
         */
        @Override
        public void widgetDefaultSelected(SelectionEvent e) {
        }

    }

    public static ArrayList<PresetData> getPresets() {
        ArrayList<PresetData> presets = new ArrayList<PresetData>();
        presets.add(new PresetData("Full HD: 1920 x 1080",1920,1080,72,PIXELS));
        presets.add(new PresetData("Screen: 1024 x 768",1024,768,72,PIXELS));
        presets.add(new PresetData("Screen: 800 x 600",800,600,72,PIXELS));
        presets.add(new PresetData("Mobile: 960 x 640",960,640,72,PIXELS));
        presets.add(new PresetData("A4: Portrait",2480,3508,300,MM));
        presets.add(new PresetData("A4: Landscape",3508,2480,300,MM));
        return presets;
    }

    public void setColourToLabels(Color color) {
        for (Control c:theColourables) {
            c.setForeground(color);
        }
    }

    /**
     * Set a size to the text boxes.
     * 
     * @param size The size to the text boxes
     * @since 2.4.3
     */
    public void setSVGSize(Point size) {
        try {
            theHeight.setText(Integer.toString(size.y));
            theWidth.setText(Integer.toString(size.x));
        }
        catch (NullPointerException ignore) {
        }
    }
}
