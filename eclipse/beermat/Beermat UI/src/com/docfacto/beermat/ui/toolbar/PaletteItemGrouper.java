package com.docfacto.beermat.ui.toolbar;

import static com.docfacto.beermat.plugin.PluginConstants.ERROR;
import static com.docfacto.beermat.plugin.PluginConstants.ICON_FOLDER;
import static com.docfacto.beermat.plugin.PluginConstants.MORE_BLUE;
import static com.docfacto.beermat.plugin.PluginConstants.MORE_BLUE_ROT;

import java.util.ArrayList;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ToolItem;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.PaletteBarChangeEvent;
import com.docfacto.beermat.ui.BeermatViewConstants.ToolbarPopup;
import com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler;
import com.docfacto.beermat.ui.widgets.GroupedItemWidget;
import com.docfacto.beermat.utils.PluginManager;
import com.docfacto.core.utils.ImageUtils;

/**
 * A palette item that holds a group of palette items.
 * 
 * @author kporter - created Jul 8, 2013
 * @since 2.4.2
 */
public class PaletteItemGrouper extends AbstractPaletteItem {
    private Rectangle theBounds;

    private static Image DEFAULT_IMAGE = PluginManager.getImage(ICON_FOLDER+ERROR);

    private ArrayList<PaletteItem> theItems;

    private GroupedItemWidget theWidget;
    private PaletteItem thePaletteItem;

    private final String thePaletteName;
    
    private ToolbarPopup thePopupDirection;

    private ToolItem theToolItem;

    private static final Image theDecoratorImage = PluginManager.getImage(ICON_FOLDER+MORE_BLUE);
    private static final Image theDecoratorRotated = PluginManager.getImage(ICON_FOLDER+MORE_BLUE_ROT);

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param paletteName name of the palette
     * @param direction The direction the popup will appear.
     * @since 2.4
     */
    public PaletteItemGrouper(BeermatController controller,String paletteName, ToolbarPopup direction) {
        super(controller);
        thePaletteName = paletteName;
        theItems = new ArrayList<PaletteItem>();
        thePopupDirection = direction;
    }

    /**
     * The GroupItemWidget
     * 
     * @return the widget
     * @since 2.4
     */
    public GroupedItemWidget getWidget() {
        return theWidget;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        return thePaletteName;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
     */
    @Override
    public Image getImage() {
        if (thePaletteItem!=null) {
            
            Image decorator = theDecoratorImage;;
            int direction = ImageUtils.TOP_RIGHT;
            
            if(thePopupDirection == ToolbarPopup.VERTICAL){
                decorator = theDecoratorRotated;
                direction = ImageUtils.BOTTOM_RIGHT;                
            }
            
            return ImageUtils.decorateImage(thePaletteItem.getImage(),
                decorator,direction);
        }
        return DEFAULT_IMAGE;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        if (thePaletteItem!=null) {
            return thePaletteItem.getTooltipText();
        }
        return thePaletteName;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#performAction()
     */
    @Override
    public void performAction() {
    }

    /**
     * Create the widget if required, and open the shell
     * 
     * @since 2.4
     */
    public void open() {
        if (theWidget==null) {
            createWidget();
        }
        theWidget.open();
    }

    /**
     * Create the widget
     * 
     * @since 2.4
     */
    private void createWidget() {
        theWidget =
            new GroupedItemWidget(getToolbar(),this);
        theWidget.setPopupDirection(thePopupDirection);
    }
    
    private Composite theToolbar = null;
    
    /**
     * Sets the toolbar composite.
     * @param toolbar The Composite
     * @since 2.4
     */
    public void setToolbar(Composite toolbar){
        theToolbar = toolbar;
    }
    
    /**
     * Gets the toolbar composite
     * @return The Toolbar composite
     * @since 2.4
     */
    public Composite getToolbar(){
        if(theToolbar==null){
            //assume this is the default one
            theToolbar =  getController().getView().getPaletteToolBar()
            .getToolBar();
        }
        return theToolbar;
    }

    /**
     * Sets the active item held by this grouper.
     * 
     * @param item the selected palette item
     * @param notifyEvents Whether to fire events from this change.
     * @since 2.4.2
     */
    public void setActivePaletteItem(PaletteItem item, boolean notifyEvents) {
        thePaletteItem = item;
        // Swap the image
        
        theToolItem.setImage(getImage());

        theToolItem.setToolTipText(item.getTooltipText());

        if(notifyEvents){
            // Fire events notifying controller that there is a shape handler
            if(thePaletteItem instanceof MarkerPaletteItem){
                thePaletteItem.performAction();
            }else{
                getController().processEvent( new PaletteBarChangeEvent(this,thePaletteItem,
                        (ShapeHandler)thePaletteItem));            
            }            
        }
    }

    /**
     * Add items to the group
     * 
     * @param item to add to the group
     * @since 2.4
     */
    public void addItem(PaletteItem item) {
        theItems.add(item);
        if (thePaletteItem==null) {
            thePaletteItem = item;
        }
    }

    /**
     * Return the selected item
     * 
     * @return the selected item
     * @since 2.4
     */
    public PaletteItem getSelectedItem() {
        return thePaletteItem;
    }

    /**
     * Get the PaletteItem.
     * 
     * @return The items held by this grouper.
     * @since 2.4.2
     */
    public ArrayList<PaletteItem> getItems() {
        return theItems;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        return ICON_FOLDER+"palettes"+"/"+thePaletteName;
    }

    /**
     * Returns theBounds.
     * 
     * @return the theBounds
     * @since 2.4
     */
    public Rectangle getBounds() {
        return theBounds;
    }

    /**
     * Sets theBounds.
     * 
     * @param bounds the bounds value
     * @since 2.4
     */
    public void setBounds(Rectangle bounds) {
        theBounds = bounds;
    }

    /**
     * Set the toolbar item so that the icon can be changed to the selected
     * shape
     * 
     * @param item the toolbar item
     * @since 2.4
     */
    public void setToolItem(ToolItem item) {
        theToolItem = item;
    }
    
    /**
     * Sets thePopupDirection.
     * @param toolbarDirection Whether this is vertical dropping toolbar or horizontal 
     * @since 2.4.5
     */
    public void setPopupDirection(ToolbarPopup toolbarDirection){
        thePopupDirection = toolbarDirection;
    }
    

    /**
     * Returns thePopupDirection.
     *
     * @return the thePopupDirection
     * @since 2.4.5
     */
    public ToolbarPopup getPopupDirection() {
        return thePopupDirection;
    }

    /**
     * Gets all the palette items this grouper holds.
     * 
     * @return All the palette items held
     * @since 2.4.5
     */
    public ArrayList<PaletteItem> getAllItems() {
        return theItems;
    }
}
