package com.docfacto.beermat.ui;

import java.util.ArrayList;

import org.apache.batik.dom.svg.SVGOMGElement;
import org.apache.batik.dom.svg.SVGOMTextElement;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.beermat.plugin.PluginConstants;
import com.docfacto.beermat.ui.toolbar.PaletteItem;
import com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler;
import com.docfacto.beermat.updaters.SetAttributesUpdater;
import com.docfacto.beermat.utils.PluginManager;
import com.docfacto.common.NameValuePair;

/**
 * The View that contains the toolbars and canvas.
 * 
 * @author kporter - created Jan 15, 2013
 * @since 2.2
 */
public class BeermatView {

    private SVGComposite theSvgCanvas;
    private final BeermatController theController;

    private Composite theParent;
    private Composite theCanvasArea;
    private ControlToolBar theControlToolBar;
    private PaletteToolBar thePaletteToolBar;
    private CanvasControlToolbar theCanvasControlToolbar;

    // private DropTarget theCanvasDropTarget;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param parent composite parent
     * @since 2
     */
    public BeermatView(BeermatController controller,Composite parent) {
        theController = controller;
        theParent = new Composite(parent,SWT.FILL);
        theParent.setBackground(BeermatViewConstants.COLOR_BACKGROUND);

        GridLayout grid = new GridLayout(3,false);
        grid.horizontalSpacing = 0;
        grid.verticalSpacing = 0;
        grid.marginWidth = 0;
        grid.marginHeight = 0;
        theParent.setLayout(grid);

        theControlToolBar = new ControlToolBar(theController,theParent,this);
        GridData gdTop = new GridData(SWT.FILL,SWT.CENTER,true,false,3,1);
        gdTop.minimumHeight = BeermatViewConstants.TOOLBAR_HEIGHT;
        gdTop.heightHint = 32;
        theControlToolBar.setLayoutData(gdTop);

        Label colouredSpacer = new Label(theParent,SWT.NONE);
        colouredSpacer.setBackground(BeermatViewConstants.COLOR_FOREGROUND_COMPLEMENTARY);
        GridData gdSpacer = new GridData(SWT.FILL,SWT.CENTER,false,false,3,1);
        gdSpacer.heightHint = 1;
        colouredSpacer.setLayoutData(gdSpacer);

        Composite leftBar = new Composite(theParent,SWT.NONE);
        leftBar.setBackground(BeermatViewConstants.COLOR_TOOLBAR_SECONDARY);
        GridData gdLeft = new GridData(SWT.LEFT,SWT.FILL,false,false,1,1);
        gdLeft.widthHint = BeermatViewConstants.TOOLBAR_WIDTH;
        leftBar.setLayoutData(gdLeft);
        thePaletteToolBar = new PaletteToolBar(theController,leftBar,this);

        createCanvasArea();

        Composite rightBar = new Composite(theParent,SWT.NONE);
        rightBar.setBackground(BeermatViewConstants.COLOR_TOOLBAR_SECONDARY);
        GridData gdRight = new GridData(SWT.RIGHT,SWT.FILL,false,false,1,1);
        gdRight.widthHint = BeermatViewConstants.TOOLBAR_WIDTH;
        rightBar.setLayoutData(gdRight);

        Label graySpacer = new Label(theParent,SWT.NONE);
        graySpacer.setBackground(BeermatViewConstants.COLOR_SEPARATOR);
        gdSpacer = new GridData(SWT.FILL,SWT.CENTER,false,false,3,1);
        gdSpacer.heightHint = 1;
        graySpacer.setLayoutData(gdSpacer);

        theCanvasControlToolbar = new CanvasControlToolbar(theParent,controller,this);
        GridData gdBottom = new GridData(SWT.FILL,SWT.CENTER,false,false,3,1);
        gdBottom.heightHint = BeermatViewConstants.TOOLBAR_HEIGHT;
        theCanvasControlToolbar.setLayoutData(gdBottom);
    }

    /**
     * Return the selected palette item
     * 
     * @return the current selected item
     * @since 2.4
     */
    public PaletteItem getSelectedPaletteItem() {
        return thePaletteToolBar.getSelectedItem();
    }

    /**
     * The selected shape handler, may be null
     * 
     * @return the current shape handler
     * @since 2.4
     */
    public ShapeHandler getShapeHandler() {
        return thePaletteToolBar.getSelectedShapeHandler();
    }

    /**
     * Get the control toolbar
     * 
     * @return the control toolbar
     * @since 2.4
     */
    public ControlToolBar getControlToolBar() {
        return theControlToolBar;
    }

    /**
     * Get the palette item toolbar.
     * 
     * @return The palette toolbar.
     * @since 2.4
     */
    public PaletteToolBar getPaletteToolBar() {
        return thePaletteToolBar;
    }

    /**
     * @since 2.1
     */
    private void createCanvasArea() {
        theCanvasArea = new Composite(theParent,SWT.NONE);
        theSvgCanvas = new SVGComposite(theController,theCanvasArea);
        theCanvasArea.setLayout(new FillLayout(SWT.HORIZONTAL));
        theCanvasArea.setEnabled(true);

        GridData canvas = new GridData(SWT.FILL,SWT.FILL,true,true,1,1);
        theCanvasArea.setLayoutData(canvas);

    }

    /**
     * Dispose of the SWT widgets
     * 
     * @since 2.4
     */
    public void dispose() {
        theCanvasArea.dispose();
        theSvgCanvas.dispose();
        // theCanvasDropTarget.dispose();
        thePaletteToolBar.dispose();
        theControlToolBar.dispose();
    }

    /**
     * Gets the component on which drag events are to listened
     * 
     * @return The control
     * @since 2.2
     */
    public Control getCanvasDragTarget() {
        return theSvgCanvas;
    }

    /**
     * Get the SVG Composite
     * 
     * @return the SVGComposite
     * @since 2.2
     */
    public SVGComposite getSVGComposite() {
        return theSvgCanvas;
    }

    /**
     * Gets the BridgeContext from the JSVGCanvas
     * 
     * @return A BridgeContext
     * @since 2.2
     */
    public org.apache.batik.bridge.BridgeContext getBridgeContext() {
        return theSvgCanvas.getBeermatAWTCanvas().getUpdateManager().getBridgeContext();
    }

    /**
     * Sets the relevent attributes for the given elementbabe.
     * 
     * @param element to set the attribute on
     * @since 2.4
     */
    public void setAttributesOnElement(Element element) {
        if (element==null) {
            return;
        }
        else if (element instanceof SVGOMTextElement) {
            setFontAttributesOnElement(element);
        }
        else if (element instanceof SVGOMGElement) {
            NodeList children = element.getChildNodes();
            for (int i = 0;i<children.getLength();i++) {
                if (children.item(i).getNodeType()==Element.ELEMENT_NODE) {
                    // Recurse for children
                    setAttributesOnElement((Element)children.item(i));
                }
            }
        }

        else {
            ArrayList<NameValuePair> attrList = theControlToolBar.getElementAttributes(element);
            new SetAttributesUpdater(theController,attrList,element);
        }
    }

    /**
     * Set all of the select font options on the given element
     * 
     * @param element
     * @since 2.4
     */
    public void setFontAttributesOnElement(Element element) {
        ArrayList<NameValuePair> attrList = theControlToolBar.getFontAttributes();
        new SetAttributesUpdater(theController,attrList,element);
    }

    @SuppressWarnings("unused")
    private Composite getDoodleComposite(Composite parent) {
        int imagesize = 20;
        Composite composite = new Composite(parent,SWT.NONE);
        composite.setBackground(BeermatViewConstants.COLOR_BACKGROUND);
        composite.setLayout(new RowLayout());
        Label doodle = new Label(composite,SWT.NONE);

        Image image = PluginManager.getImage(PluginConstants.ICON_FOLDER+BeermatUIPlugin.BEERMAT_LOGO_20);
        doodle.setImage(image);
        doodle.setLayoutData(new RowData(imagesize,imagesize));
        return composite;
    }

}
