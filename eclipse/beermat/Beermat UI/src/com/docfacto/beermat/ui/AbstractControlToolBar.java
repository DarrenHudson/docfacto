package com.docfacto.beermat.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.docfacto.beermat.controller.BeermatController;

/**
 * A horizontal Toolbar like Composite that contains widgets
 * 
 * @author kporter - created Aug 22, 2013
 * @since 2.4.3
 */
public abstract class AbstractControlToolBar extends Composite {

    private BeermatController theController;
    private BeermatView theView;

    /**
     * Constructor
     * 
     * @param parent The parent of this composite
     * @param style The style int to set to this Composite
     * @param view The BeermatView
     * @param controller The BeermatController
     * @since 2.4
     */
    public AbstractControlToolBar(Composite parent,BeermatView view,BeermatController controller,int style) {
        super(parent,style);
        theController = controller;
        theView = view;
        layoutDefault();
    }

    void layoutDefault() {
        setBackground(BeermatViewConstants.COLOR_TOOLBAR_PRIMARY);

        RowLayout row = new RowLayout(SWT.HORIZONTAL);
        row.center = true;
        row.fill = false;
        row.marginTop = 3;
        row.spacing = 5;
        row.marginBottom = 2;
        setLayout(row);
    }

    /**
     * Returns the BeermatView.
     * 
     * {@docfacto.system Not giving access to the view, from here, to everyone}
     * 
     * @return the theView
     */
    BeermatView getView() {
        return theView;
    }

    /**
     * Helper method to get the canvas
     * 
     * {@docfacto.todo remove if not needed}
     * 
     * @return the SVG Canvas
     * @since 2.4
     */
    SVGComposite getSVGCanvas() {
        return theView.getSVGComposite();
    }

    /**
     * Call this in the constructor to layout the interface of this control bar.
     * 
     * @since 2.4.3
     */
    protected abstract void layoutUI();

    /**
     * Helper method to add a separator.
     * 
     * @since 2.4
     */
    protected void addSeparator() {
        Label label = new Label(this,SWT.NONE);
        label.setBackground(BeermatViewConstants.COLOR_SEPARATOR);
        RowData d = new RowData(1,-1);
        label.setLayoutData(d);
    }

    /**
     * Returns the BeermatController.
     * 
     * @return the BeermatController
     * @since 2.4.3
     */
    public BeermatController getController() {
        return theController;
    }

}