package com.docfacto.beermat.ui.toolbar.shapes;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.hotspot.HotSpot;
import com.docfacto.beermat.svg.ShapeShifter;
import com.docfacto.beermat.svg.TransformElementParser;
import com.docfacto.beermat.ui.toolbar.PaletteItemGrouper;
import com.docfacto.beermat.utils.PluginManager;
import com.docfacto.beermat.utils.SVGUtils;

/**
 * Handle Shapes other than just paths.
 * 
 * @author kporter - created Sep 24, 2013
 * @since 2.4.5
 */
public class ElementShapeHandler extends AbstractShapeHandler {

    private Element theElement;
    private PaletteItemGrouper theParent;
    private String theName;

    /**
     * Constructor. {@docfacto.note Constructing this handler for the given
     * element, imports the element to the current Document}
     * 
     * @param controller The BeermatController
     * @param parent
     * @param element
     * @param name
     * @since
     */
    public ElementShapeHandler(BeermatController controller,
    PaletteItemGrouper parent,Element element,String name) {
        super(controller);
        theElement = element;
        theName = name;
        theParent = parent;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        return theName;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
     */
    @Override
    public Image getImage() {
        return PluginManager.getImage(getImagePath());
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        return theParent.getImagePath()+"/"+getName()+".png";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        return getName();
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#createShape(org.w3c.dom.svg.SVGDocument,
     * org.eclipse.swt.graphics.Point)
     */
    @Override
    public Element createShape(SVGDocument document,Point startPoint) {

        theElement = (Element)getController().getManager().getDocument()
            .importNode(theElement,true);

        Element element = (Element)theElement.cloneNode(true);
        document.getDocumentElement().appendChild(element);
        TransformElementParser transformer =
            new TransformElementParser(element);
        transformer.translate(startPoint.x,startPoint.y,true);
        return element;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#drag(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void drag(Element element,ShapeUpdateData updateData) {
        resize(element,updateData);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#resize(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void resize(Element element,ShapeUpdateData updateData) {
        ShapeShifter.dragAll(element,updateData.getDiff().x,
            updateData.getDiff().y,HotSpot.SOUTH_EAST,updateData.isShiftDown(),
            getController().getViewBoxTransform());
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#move(org.w3c.dom.Element,
     * com.docfacto.beermat.ui.toolbar.shapes.ShapeUpdateData)
     */
    @Override
    public void move(Element element,ShapeUpdateData updateData) {
        // unused
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.shapes.ShapeHandler#mouseClick(org.eclipse.swt.graphics.Point,
     * org.w3c.dom.Element)
     */
    @Override
    public Element mouseClick(Point location,Element lastElement) {

        Element element =
            createShape(getController().getManager().getDocument(),location);

        SVGUtils.retainScale(element,lastElement);

        return element;
    }

}
