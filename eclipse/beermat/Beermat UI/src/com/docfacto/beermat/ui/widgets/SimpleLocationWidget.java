package com.docfacto.beermat.ui.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.docfacto.core.utils.SWTUtils;
import com.docfacto.core.widgets.listeners.NumericVerifyListener;

/**
 * A simple x and y widget.
 * 
 * @author kporter - created Aug 1, 2013
 * @since 2.4.3
 */
public class SimpleLocationWidget extends Composite {

    private Text theTextX;
    private Text theTextY;

    /**
     * Constructs a location widget, the labels are set to x and y
     * {@docfacto.note if no style is given, then the widget is assumed to be HORIZONTAL layout} 
     * 
     * @param parent The parent composite
     * @param style The style of this widget, REQUIRED
     * @since 2.4
     */
    public SimpleLocationWidget(Composite parent,int style) {
        super(parent,SWT.NONE);
        createContents(style, "x","y");
    }

    /**
     * Constructs a simple location widget, but with custom text on the labels.
     * 
     * {@docfacto.note if no style is given, then the widget is assumed to be HORIZONTAL layout}
     * 
     * @param parent The parent composite
     * @param style The style int, <em>required</em> to specify orientation.
     * @param label1 The String to set the first label to
     * @param label2 The String to set the second label to.
     * @since 2.4.3
     * @See SWT.VERTICAL
     */
    public SimpleLocationWidget(Composite parent,int style, String label1,
    String label2) {
        super(parent,SWT.NONE);
        createContents(style, label1, label2);
    }

    private void createContents(int style, String label1,String label2) {
        int space = (5-label2.length())*4;
        
        /**
         * if you want to edit this widget in windowbuilder, for it
         * to display properly, comment out one or the other in the if/else
         */
        if(style == SWT.VERTICAL){
            GridLayout layout =  new GridLayout(2,false);
            layout.verticalSpacing = 10;
            layout.horizontalSpacing = 10;
            layout.marginHeight = 10;
            layout.marginWidth = 10;
            setLayout(layout);
        }
        else{
            GridLayout layout =  new GridLayout(5,false);
            layout.marginHeight = 10;
            layout.marginWidth = 10;
            setLayout(layout);            
        }

        _xLabel = new Label(this,SWT.NONE);
        _xLabel.setText(label1);

        theTextX = new Text(this,SWT.BORDER);
        theTextX.addVerifyListener(new NumericVerifyListener());

        if(style == SWT.HORIZONTAL){
            Label lblSpacer = new Label(this,SWT.NONE);
            lblSpacer.setLayoutData(new GridData(15+space, SWT.DEFAULT));            
        }

        _yLabel = new Label(this,SWT.NONE);
        _yLabel.setText(label2);

        theTextY = new Text(this,SWT.BORDER);
//        new Label(this, SWT.NONE);
        theTextY.addVerifyListener(new NumericVerifyListener());
    }

    /**
     * Get the X value the user has typed, does no parsing or checking.
     * 
     * @return The X value as string
     * @since 2.4.3
     */
    public String getX() {
        return theTextX.getText();
    }

    /**
     * Get the Y value the user has typed, does no parsing or checking.
     * 
     * @return The Y value as string
     * @since 2.4.3
     */
    public String getY() {
        return theTextY.getText();
    }

    /**
     * Sets the foreground colour to every label in this widget
     * 
     * @param color The colour to set
     * @since 2.4.3
     */
    public void setLabelColour(Color color) {
        _xLabel.setForeground(color);
        _yLabel.setForeground(color);
    }

    public void setEnabledText(boolean enabled) {
        theTextX.setEnabled(enabled);
        theTextY.setEnabled(enabled);
        if (enabled) {
            theTextX.setBackground(SWTUtils.getColor(SWT.COLOR_WHITE));
            theTextY.setBackground(SWTUtils.getColor(SWT.COLOR_WHITE));
        }
        else {
            theTextX.setBackground(SWTUtils
                .getColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND));
            theTextY.setBackground(SWTUtils
                .getColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND));
        }
    }

    private Label _xLabel;
    private Label _yLabel;

}
