package com.docfacto.beermat.ui.widgets.data;
public final class UnitConstants {
    public final static String UNIT_KEY = "unit.key";
    
    public final static String PIXELS = "px";
    public final static String POINTS = "pt";
    public final static String PICAS = "pc";
    public final static String MM = "mm";
    public final static String CM = "cm";
    public final static String INCH = "in";
    public final static String[] UNITS = {PIXELS,POINTS,MM,CM,INCH}; 
    public final static String[] RESOLUTIONS = {"300","150","90","72"};
    
    public final static int HEIGHT = 1;
    public final static int WIDTH = 2;
}
