package com.docfacto.beermat.ui.toolbar;

import static com.docfacto.beermat.plugin.PluginConstants.ICON_FOLDER;
import static com.docfacto.beermat.plugin.PluginConstants.LINE_ORANGE;

import org.eclipse.swt.graphics.Image;
import org.w3c.dom.Element;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.AttributeChangeEvent;
import com.docfacto.beermat.events.MarkerChangeEvent;
import com.docfacto.beermat.utils.PluginManager;

/**
 * A palette item to represent a marker
 * 
 * @author kporter - created Sep 18, 2013
 * @since 2.4.4
 */
public class MarkerPaletteItem extends AbstractPaletteItem {

    /**
     * ID on no marker {@value}
     */
    public static String NONE = "none";

    public static String DEFAULT_ARROW = "beermat.marker.sync";
    public static String DEFAULT_ARROW_REVERSE = DEFAULT_ARROW+".reverse";

    private Element theMarker;
    private String theID;
    private boolean theNoneMarker;
    private String theToolTipText;

    private boolean isStartMarker;

    /**
     * Constructs a new MarkerPaletteItem
     * 
     * The boolean reverse affects the id of this marker, and the tooltip.
     * 
     * @param controller The BeermatController
     * @param marker The marker element
     * @param id The id of the marker
     * @param reverse Whether this marker is a reverse direction marker
     */
    public MarkerPaletteItem(BeermatController controller,Element marker,
    String id,boolean reverse) {
        super(controller);
        theMarker = marker;

        if (marker==null) {
            theNoneMarker = true;
        }
        else {
            theNoneMarker = false;
        }

        if (reverse) {
            id = id+".reverse";
            theToolTipText = "marker-start";
            isStartMarker = true;
        }
        else {
            theToolTipText = "marker-end";
        }

        theID = id;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        return theID;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
     */
    @Override
    public Image getImage() {
        if (isNoneMarker()) {
            return PluginManager.getImage(ICON_FOLDER+LINE_ORANGE);
        }
        return PluginManager.getImage(ICON_FOLDER+"palettes/markers/"+theID+
            ".png");
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        if (isNoneMarker()) {
            return ICON_FOLDER+LINE_ORANGE;
        }
        return ICON_FOLDER+"palettes/markers/"+theID+".png";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        return "Set "+theToolTipText+": "+getName();
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#performAction()
     */
    @Override
    public void performAction() {
        getController().processEvent(new AttributeChangeEvent(this));

        if (isStartMarker) {
            getController().processEvent(
                new MarkerChangeEvent(this,MarkerChangeEvent.Position.START,
                    theID));
        }
        else {
            getController()
                .processEvent(
                    new MarkerChangeEvent(this,MarkerChangeEvent.Position.END,
                        theID));
        }
    }

    /**
     * Sets whether this marker is a none marker.
     * 
     * @param isNoneMarker Whether this marker item is a none marker
     * @since 2.4.4
     */
    public void setNoneMarker(boolean isNoneMarker) {
        theNoneMarker = isNoneMarker;
    }

    /**
     * Check whether this is a none-marker.
     * 
     * @return Whether this is a none marker
     * @since 2.4.5
     */
    public boolean isNoneMarker() {
        return theNoneMarker;
    }

    /**
     * Gets the marker from this palette item.
     * 
     * @return The marker element
     * @since 2.4.5
     */
    public Element getMarker() {
        return (Element)getController().getManager().getDocument()
            .importNode(theMarker,true);
        // return theMarker;
    }

}
