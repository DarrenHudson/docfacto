package com.docfacto.beermat.ui;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.util.HashMap;

import org.apache.batik.bridge.CursorManager;

import com.docfacto.beermat.controller.BeermatController;

/**
 * Cursor manager for Linux and Windows
 * 
 * @author dhudson - created 28 Aug 2013
 * @since 2.4
 */
public class AWTCursorManager extends AbstractCursorManager {

    private final BeermatAWTCanvas theCanvas;

    private HashMap<Integer,Cursor> theCursorCache;
    
    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param canvas to change the cursor on
     * @since 2.4
     */
    public AWTCursorManager(BeermatController controller,
    BeermatAWTCanvas canvas) {
        super(controller);
        theCanvas = canvas;
    }

    /**
     * @see com.docfacto.beermat.ui.AbstractCursorManager#loadCursors()
     */
    @Override
    public void loadCursors() {
        theCursorCache = new HashMap<Integer,Cursor>(CURSORS.length);
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        
        for (int cursorType:CURSORS) {
            Cursor cursor = null;
            switch (cursorType) {
            case CROSSHAIR:
                cursor = Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);
                break;
            case DEFAULT:
                cursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
                break;
            case HAND:
                cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
                break;
            case WAIT:
                cursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
                break;
            case TEXT:
                cursor = Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR);
                break;
            case MOVE:
                cursor = Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR);
                break;
            case E_RESIZE:
                cursor = Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR);
                break;
            case NE_RESIZE:
                cursor = Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR);
                break;
            case NW_RESIZE:
                cursor =  Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR);
                break;
            case N_RESIZE:
                cursor = Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR);
                break;
            case SE_RESIZE:
                cursor =  Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR);
                break;
            case SW_RESIZE:
                cursor = Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR);
                break;
            case S_RESIZE:
                cursor = Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR);
                break;
            case W_RESIZE:
                cursor = Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR);
                break;
            case ROTATE:
                try {
                    Image img = toolkit.createImage
                        (CursorManager.class.getResource("/icons/rotate.gif"));
                    cursor = toolkit.createCustomCursor
                        (img, new Point(0, 0), "rotate");
                } catch (Exception ex) {
                }                
                break;
            case PENCIL:
                try {
                    Image img = toolkit.createImage
                        (CursorManager.class.getResource("/icons/freehand.png"));
                    cursor = toolkit.createCustomCursor
                        (img, new Point(2, 16), "pencil");
                } catch (Exception ex) {
                }                 
                break;
            case ADD_TEXT:
                try {
                    Image img = toolkit.createImage
                        (CursorManager.class.getResource("/icons/ibeam.png"));
                    cursor = toolkit.createCustomCursor
                        (img, new Point(8, 8), "add text");
                } catch (Exception ex) {
                } 
                break;
            }
        
            if(cursor != null) {
                theCursorCache.put(cursorType,cursor);
            }
        }
    }

    /**
     * @see com.docfacto.beermat.ui.AbstractCursorManager#handleCursorChange(int)
     */
    @Override
    public void handleCursorChange(int type) {
        theCanvas.setCursor(theCursorCache.get(type));
    }

    /**
     * @see com.docfacto.beermat.ui.AbstractCursorManager#dispose()
     */
    @Override
    public void dispose() {

    }

}
