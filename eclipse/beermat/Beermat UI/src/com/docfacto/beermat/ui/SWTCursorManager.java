package com.docfacto.beermat.ui;

import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.widgets.Display;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.utils.PluginManager;
import com.docfacto.core.utils.SWTUtils;

/**
 * OSX Cursor management
 * 
 * {@docfacto.system For OSX it seems to be that cursors will only work at the
 * SWT level}
 * 
 * @author dhudson - created 28 Aug 2013
 * @since 2.4
 */
public class SWTCursorManager extends AbstractCursorManager {

    // Cursors are handled by this composite and not at the AWT level for OSX
    private HashMap<Integer,Cursor> theCursorCache;

    private final Display theDisplay;
    private final SVGComposite theComposite;

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @param canvas SWT composite
     * @since 2.4
     */
    public SWTCursorManager(BeermatController controller,SVGComposite canvas) {
        super(controller);
        theComposite = canvas;
        theDisplay = canvas.getDisplay();
    }

    /**
     * @see com.docfacto.beermat.ui.AbstractCursorManager#handleCursorChange(int)
     */
    @Override
    public void handleCursorChange(final int type) {
        SWTUtils.UIThreadExec(new Runnable() {
            public void run() {
                theComposite.setCursor(theCursorCache.get(type));
            }
        });
    }

    /**
     * @see com.docfacto.beermat.ui.AbstractCursorManager#loadCursors()
     */
    @Override
    public void loadCursors() {
        theCursorCache = new HashMap<Integer,Cursor>(CURSORS.length);

        for (int cursorType:CURSORS) {
            Cursor cursor = null;
            switch (cursorType) {
            case CROSSHAIR:
                cursor = new Cursor(theDisplay,SWT.CURSOR_CROSS);
                break;
            case DEFAULT:
                cursor = new Cursor(theDisplay,SWT.CURSOR_ARROW);
                break;
            case HAND:
                cursor = new Cursor(theDisplay,SWT.CURSOR_HAND);
                break;
            case WAIT:
                cursor = new Cursor(theDisplay,SWT.CURSOR_WAIT);
                break;
            case TEXT:
                cursor = new Cursor(theDisplay,SWT.CURSOR_IBEAM);
                break;
            case MOVE:
                cursor = new Cursor(theDisplay,SWT.CURSOR_SIZEALL);
                break;
            case E_RESIZE:
                cursor = new Cursor(theDisplay,SWT.CURSOR_SIZEE);
                break;
            case NE_RESIZE:
                cursor = new Cursor(theDisplay,SWT.CURSOR_SIZENE);
                break;
            case NW_RESIZE:
                cursor = new Cursor(theDisplay,SWT.CURSOR_SIZENW);
                break;
            case N_RESIZE:
                cursor = new Cursor(theDisplay,SWT.CURSOR_SIZEN);
                break;
            case SE_RESIZE:
                cursor = new Cursor(theDisplay,SWT.CURSOR_SIZESE);
                break;
            case SW_RESIZE:
                cursor = new Cursor(theDisplay,SWT.CURSOR_SIZESW);
                break;
            case S_RESIZE:
                cursor = new Cursor(theDisplay,SWT.CURSOR_SIZES);
                break;
            case W_RESIZE:
                cursor = new Cursor(theDisplay,SWT.CURSOR_SIZEW);
                break;
            case ROTATE:
                cursor =
                    new Cursor(theDisplay,PluginManager.getImage(
                        "/icons/rotate.gif").getImageData(),0,0);
                break;
            case PENCIL:
                cursor =
                    new Cursor(theDisplay,PluginManager.getImage(
                        "/icons/freehand.png")
                        .getImageData(),2,16);
                break;
            case ADD_TEXT:
                cursor =
                    new Cursor(theDisplay,PluginManager.getImage(
                        "/icons/ibeam.png")
                        .getImageData(),8,8);
                break;
            }
            theCursorCache.put(cursorType,cursor);
        }
    }

    /**
     * @see com.docfacto.beermat.ui.AbstractCursorManager#dispose()
     */
    public void dispose() {
        // Empty the cursors
        for (Cursor cursor:theCursorCache.values()) {
            cursor.dispose();
        }
    }
}
