package com.docfacto.beermat.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

import javax.swing.ActionMap;
import javax.swing.BoundedRangeModel;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.batik.bridge.ViewBox;
import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.util.SVGConstants;
import org.w3c.dom.svg.SVGDocument;
import org.w3c.dom.svg.SVGSVGElement;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.events.BeermatEvent;
import com.docfacto.beermat.events.CanvasAction;
import com.docfacto.beermat.listeners.BeermatEventListener;
import com.docfacto.beermat.listeners.ScrollListener;

/**
 * A Swing component that consists of a JSVGCanvas with optional scroll bars.
 * <p>
 * Re-implementation, rather than implementing the Scrollable interface,
 * provides several advantages. The main advantage is the ability to control
 * more precisely ScrollBar events; fewer JSVGCanvas updates are required when
 * scrolling. This creates a significant performance (reflected by an increase
 * in scroll speed) advantage compared to implementing the Scrollable interface.
 * </p>
 * <p>
 * As DOM events are turned off, listen to beermat events instead, also enable
 * the mouse wheel listener for panning
 * </p>
 * 
 * @author dhudson
 * @since 2.4
 */
public class BeermatScrollPane extends JPanel implements BeermatEventListener {

    /**
     * {@value}
     */
    private static final long serialVersionUID = 1L;

    private final BeermatAWTCanvas theCanvas;
    private final BeermatController theController;

    protected JPanel horizontalPanel;
    protected JScrollBar vertical;
    protected JScrollBar horizontal;
    protected Component cornerBox;
    protected boolean scrollbarsAlwaysVisible = false;

    protected SBListener hsbListener;
    protected SBListener vsbListener;

    protected Rectangle2D theViewBox = null; // SVG Root element viewbox
    protected boolean ignoreScrollChange = false;

    /**
     * Creates a JSVGScrollPane, which will scroll an JSVGCanvas.
     * 
     * @since 2.4
     */
    public BeermatScrollPane(BeermatController controller,
    BeermatAWTCanvas canvas) {
        setBackground(Color.RED);

        theController = controller;
        theCanvas = canvas;
        // theCanvas.setRecenterOnResize(false);

        // create components
        vertical = new JScrollBar(JScrollBar.VERTICAL,0,0,0,0);
        horizontal = new JScrollBar(JScrollBar.HORIZONTAL,0,0,0,0);

        // create a spacer next to the horizontal bar
        horizontalPanel = new JPanel(new BorderLayout());
        horizontalPanel.add(horizontal,BorderLayout.CENTER);
        cornerBox = Box.createRigidArea
            (new Dimension(vertical.getPreferredSize().width,
                horizontal.getPreferredSize().height));
        horizontalPanel.add(cornerBox,BorderLayout.EAST);

        // listeners
        hsbListener = new SBListener(false);
        horizontal.getModel().addChangeListener(hsbListener);

        vsbListener = new SBListener(true);
        vertical.getModel().addChangeListener(vsbListener);

        // by default, scrollbars are not needed
        updateScrollbarState(false,false);

        // Listen for mouse wheel events
        canvas.getInputDelegate().setScrollPane(this);

        // layout
        setLayout(new BorderLayout());
        add(canvas,BorderLayout.CENTER);
        add(vertical,BorderLayout.EAST);
        add(horizontalPanel,BorderLayout.SOUTH);

        // canvas listeners
        ScrollListener xlistener = new ScrollListener(this);
        addComponentListener(xlistener);
        canvas.addGVTTreeRendererListener(xlistener);
        canvas.addJGVTComponentListener(xlistener);
        canvas.addGVTTreeBuilderListener(xlistener);
        canvas.addUpdateManagerListener(xlistener);

        // Install new scroll action events..
        installScrollActions();

        controller.addListener(this);
    }

    private void installScrollActions() {
        ActionMap actionMap = theCanvas.getActionMap();

        // Installing our own scroll actions will remove the standard ones
        actionMap.put(JSVGCanvas.SCROLL_RIGHT_ACTION,new CanvasScrollAction(
            this,theCanvas,-10,0));
        actionMap.put(JSVGCanvas.SCROLL_LEFT_ACTION,new CanvasScrollAction(
            this,theCanvas,10,0));
        actionMap.put(JSVGCanvas.SCROLL_UP_ACTION,new CanvasScrollAction(this,
            theCanvas,0,10));
        actionMap.put(JSVGCanvas.SCROLL_DOWN_ACTION,new CanvasScrollAction(
            this,theCanvas,0,-10));
    }

    /**
     * Sets the translation portion of the transform based upon the current
     * scroll bar position
     */
    protected void setScrollPosition() {
        checkAndSetViewBoxRect();

        if (theViewBox==null) {
            return;
        }

        AffineTransform crt = theCanvas.getRenderingTransform();
        AffineTransform vbt = theCanvas.getViewBoxTransform();
        if (crt==null) {
            crt = new AffineTransform();
        }
        if (vbt==null) {
            vbt = new AffineTransform();
        }

        Rectangle r2d = vbt.createTransformedShape(theViewBox).getBounds();
        // System.err.println("Pre : " + r2d);
        int tx = 0, ty = 0;
        if (r2d.x<0) {
            tx -= r2d.x;
        }
        if (r2d.y<0) {
            ty -= r2d.y;
        }

        int deltaX = horizontal.getValue()-tx;
        int deltaY = vertical.getValue()-ty;

        // System.err.println("tx = "+tx+"; ty = "+ty);
        // System.err.println("dx = "+deltaX+"; dy = "+deltaY);
        // System.err.println("Pre CRT: " + crt);

        crt.preConcatenate
            (AffineTransform.getTranslateInstance(-deltaX,-deltaY));
        theCanvas.setRenderingTransform(crt);
    }

    /**
     * MouseWheel listener.
     * 
     * Provides mouse wheel support. Works with vertical and horizontal scrolls
     * 
     */
    public void mouseWheelMoved(MouseWheelEvent e) {

        JScrollBar sb = null;
        if (e.getModifiers()==1) {
            // Horizontal scroll
            if (horizontal.isVisible()) {
                sb = horizontal;
            }
        }
        else {
            if (vertical.isVisible()) {
                sb = vertical;
            }
        }

        if (sb==null) {
            // Nothing to do
            return;
        }

        if (e.getScrollType()==MouseWheelEvent.WHEEL_UNIT_SCROLL) {
            final int amt = e.getUnitsToScroll()*sb.getUnitIncrement();
            sb.setValue(sb.getValue()+amt);
        }
        else if (e.getScrollType()==MouseWheelEvent.WHEEL_BLOCK_SCROLL) {
            final int amt = e.getWheelRotation();
            sb.getBlockIncrement();
            sb.setValue(sb.getValue()+amt);
        }
    }

    /**
     * Advanced JScrollBar listener.
     * <p>
     * <b>A separate listener must be attached to each scrollbar, since we keep
     * track of mouse state for each scrollbar separately!</b>
     * </p>
     */
    protected class SBListener implements ChangeListener
    {
        protected int startValue;

        protected boolean isVertical;

        public SBListener(boolean vertical)
        {
            isVertical = vertical;
        }

        public synchronized void stateChanged(ChangeEvent e)
        {
            // only respond to changes if we are NOT being dragged
            // and ignoreScrollChange is not set
            if (ignoreScrollChange) {
                return;
            }

            Object src = e.getSource();
            if (!(src instanceof BoundedRangeModel))
                return;

            int val = ((isVertical) ? vertical.getValue() :
                horizontal.getValue());

            BoundedRangeModel brm = (BoundedRangeModel)src;
            if (brm.getValueIsAdjusting()) {
                AffineTransform at;
                if (isVertical) {
                    at = AffineTransform.getTranslateInstance
                        (0,startValue-val);
                }
                else {
                    at = AffineTransform.getTranslateInstance
                        (startValue-val,0);
                }
                theCanvas.setPaintingTransform(at);
            }
            else {
                setScrollPosition();
            }
        }
    }

    /**
     * Compute the scrollbar extents, and determine if scrollbars should be
     * visible.
     * 
     */
    public void resizeScrollBars()
    {
        ignoreScrollChange = true;

        checkAndSetViewBoxRect();
        if (theViewBox==null) {
            return;
        }

        AffineTransform vbt = theCanvas.getViewBoxTransform();
        if (vbt==null) {
            vbt = new AffineTransform();
        }

        Rectangle r2d = vbt.createTransformedShape(theViewBox).getBounds();

        // compute translation
        int maxW = r2d.width;
        int maxH = r2d.height;
        int tx = 0, ty = 0;
        if (r2d.x>0)
            maxW += r2d.x;
        else
            tx -= r2d.x;
        if (r2d.y>0)
            maxH += r2d.y;
        else
            ty -= r2d.y;

        // System.err.println("   maxW = "+maxW+"; maxH = "+maxH+
        // " tx = "+tx+"; ty = "+ty);

        // Changing scrollbar visibility may change the
        // canvas's dimensions so get the end result.
        Dimension vpSize = updateScrollbarVisibility(tx,ty,maxW,maxH);

        // set scroll params
        vertical.setValues(ty,vpSize.height,0,maxH);
        horizontal.setValues(tx,vpSize.width,0,maxW);

        // set block scroll; this should be equal to a full 'page',
        // minus a small amount to keep a portion in view
        // that small amount is 10%.
        vertical.setBlockIncrement((int)(0.9f*vpSize.height));
        horizontal.setBlockIncrement((int)(0.9f*vpSize.width));

        // set unit scroll. This is arbitrary, but we define
        // it to be 20% of the current viewport.
        vertical.setUnitIncrement((int)(0.2f*vpSize.height));
        horizontal.setUnitIncrement((int)(0.2f*vpSize.width));

        validate();
        horizontalPanel.validate();
        horizontal.validate();
        vertical.validate();

        theCanvas.repaint();

        ignoreScrollChange = false;
    }

    protected Dimension updateScrollbarVisibility(int tx,int ty,
    int maxW,int maxH) {
        // display scrollbars, if appropriate
        // (if scaled document size is larger than viewport size)
        // The tricky bit is ensuring that you properly track
        // the effects of making one scroll bar visible on the
        // need for the other scroll bar.

        Dimension vpSize = theCanvas.getSize();

        // maxVPW/H is the viewport W/H without scrollbars.
        // minVPW/H is the viewport W/H with scrollbars.
        int maxVPW = vpSize.width;
        int minVPW = vpSize.width;
        int maxVPH = vpSize.height;
        int minVPH = vpSize.height;

        if (vertical.isVisible()) {
            maxVPW += vertical.getPreferredSize().width;
        }
        else {
            minVPW -= vertical.getPreferredSize().width;
        }
        if (horizontalPanel.isVisible()) {
            maxVPH += horizontal.getPreferredSize().height;
        }
        else {
            minVPH -= horizontal.getPreferredSize().height;
        }

        // System.err.println("W: [" + minVPW + "," + maxVPW + "] " +
        // "H: [" + minVPH + "," + maxVPH + "]");
        // System.err.println("MAX: [" + maxW + "," + maxH + "]");

        // Fist check if we need either scrollbar (given maxVPW/H).
        boolean hNeeded, vNeeded;
        Dimension ret = new Dimension();

        if (scrollbarsAlwaysVisible) {
            hNeeded = (maxW>minVPW);
            vNeeded = (maxH>minVPH);
            ret.width = minVPW;
            ret.height = minVPH;
        }
        else {
            hNeeded = (maxW>maxVPW)||(tx!=0);
            vNeeded = (maxH>maxVPH)||(ty!=0);
            // System.err.println("Vis flags: " + hNeeded +", " + vNeeded);

            // This makes sure that if one scrollbar is visible
            // we 'recheck' the other scroll bar with the minVPW/H
            // since making one visible makes the room for displaying content
            // in the other dimension smaller. (This also makes the
            // 'corner box' visible if both scroll bars are visible).
            if (vNeeded&&!hNeeded)
                hNeeded = (maxW>minVPW);
            else if (hNeeded&&!vNeeded)
                vNeeded = (maxH>minVPH);

            ret.width = (hNeeded) ? minVPW : maxVPW;
            ret.height = (vNeeded) ? minVPH : maxVPH;
        }

        updateScrollbarState(hNeeded,vNeeded);

        // Return the new size of the canvas.
        return ret;
    }

    public void updateScrollbarState(boolean hNeeded,boolean vNeeded) {
        horizontal.setEnabled(hNeeded);
        vertical.setEnabled(vNeeded);

        if (scrollbarsAlwaysVisible) {
            horizontalPanel.setVisible(true);
            vertical.setVisible(true);
            cornerBox.setVisible(true);
        }
        else {
            horizontalPanel.setVisible(hNeeded);
            vertical.setVisible(vNeeded);
            cornerBox.setVisible(hNeeded&&vNeeded);
        }
    }

    /**
     * Derives the SVG Viewbox from the SVG root element. Caches it. Assumes
     * that it will not change.
     * 
     */
    private void checkAndSetViewBoxRect() {
        if (theViewBox!=null) {
            return;
        }

        theViewBox = getViewBoxRect();
    }

    /**
     * Reset the viewbox so that it is recalculated
     * 
     * @since 2.4
     */
    public void resetViewbox() {
        theViewBox = null;
    }

    /**
     * Return the viewbox
     * 
     * @return the SVG viewbox
     * @since 2.4
     */
    public Rectangle2D getViewbox() {
        return theViewBox;
    }

    /**
     * If a viewbox element is present, then return the viewbox bounds, else
     * return the document bounds.
     * 
     * @return the viewbox rect
     * @since 2.4
     */
    public Rectangle2D getViewBoxRect() {

        SVGDocument doc = theCanvas.getSVGDocument();
        if (doc==null) {
            return null;
        }

        SVGSVGElement el = doc.getRootElement();
        if (el==null) {
            return null;
        }

        // If the SVG has an viewbox attribute, then honour it
        String viewBoxStr = el.getAttributeNS
            (null,SVGConstants.SVG_VIEW_BOX_ATTRIBUTE);
        if (viewBoxStr.length()!=0) {
            float[] rect = ViewBox.parseViewBoxAttribute(el,viewBoxStr,null);
            return new Rectangle2D.Float(rect[0],rect[1],
                rect[2],rect[3]);
        }
        
        Rectangle2D rect =
            new Rectangle2D.Double(0,0,theCanvas.getSVGDocumentSize()
                .getWidth(),
                theCanvas.getSVGDocumentSize().getHeight());
        
        return rect;
    }

    /**
     * Set the new viewbox
     * 
     * @param newview to set
     * @since 2.4
     */
    public void setViewbox(Rectangle2D newview) {
        theViewBox = newview;
    }

    /**
     * @see com.docfacto.beermat.listeners.BeermatEventListener#handleEvent(com.docfacto.beermat.events.BeermatEvent)
     */
    @Override
    public void handleEvent(BeermatEvent event) {
        switch (event.getAction()) {
        case BeermatEvent.SCROLL_ACTION:
            CanvasAction canvasAction = (CanvasAction)event;
            canvasAction.performAction(theCanvas.getActionMap());
            break;
        }
    }

    /**
     * Returns the size of the viewport
     * 
     * @return the viewport of the scrollpane
     * @since 2.5
     */
    public Dimension getViewport() {
        // Use 10 for the component padding
        Dimension dim = new Dimension();
        dim.setSize(getSize().width-vertical.getSize().width-10,
            getSize().height-
                horizontal.getSize().height-10);
        return dim;
    }
}
