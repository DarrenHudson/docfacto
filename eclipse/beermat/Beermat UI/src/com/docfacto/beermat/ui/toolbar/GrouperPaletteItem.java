package com.docfacto.beermat.ui.toolbar;

import static com.docfacto.beermat.plugin.PluginConstants.*;

import org.eclipse.swt.graphics.Image;

import com.docfacto.beermat.controller.BeermatController;
import com.docfacto.beermat.updaters.GrouperUpdater;
import com.docfacto.beermat.utils.PluginManager;

/**
 * Allow for elements to be group together
 * 
 * @author kporter - created Jul 30, 2013
 * @since 2.4
 * @docfacto.link uri="${doc-beermat}/controlbar/c_group_tool_palette_item.dita" key="group-action" version="2.4.2" link-to="doc"
 */
public class GrouperPaletteItem extends AbstractPaletteItem {

    /**
     * Constructor.
     * 
     * @param controller beermat controller
     * @since 2.4
     */
    public GrouperPaletteItem(BeermatController controller) {
        super(controller);
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getName()
     */
    @Override
    public String getName() {
        return "Group Elements";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImage()
     */
    @Override
    public Image getImage() {
        return PluginManager.getImage(getImagePath());
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getImagePath()
     */
    @Override
    public String getImagePath() {
        return ICON_FOLDER+GROUPER_ORANGE;
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.PaletteItem#getTooltipText()
     */
    @Override
    public String getTooltipText() {
        return "Group Selected Elements";
    }

    /**
     * @see com.docfacto.beermat.ui.toolbar.AbstractPaletteItem#performAction()
     */
    @Override
    public void performAction() {
        // Let the updater handle this
        new GrouperUpdater(getController());
    }

}
