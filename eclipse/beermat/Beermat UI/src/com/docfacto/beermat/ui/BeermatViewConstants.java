package com.docfacto.beermat.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.RGB;

import com.docfacto.core.utils.SWTUtils;

/**
 * Constants shared between the View and the Widgets
 * 
 * @author dhudson - created 18 Jun 2013
 * @since 2.4
 * @docfacto.adam ignore
 */
public class BeermatViewConstants {

    public final static int TOOLBAR_HEIGHT = 30;
    public final static int TOOLBAR_WIDTH = 30;
    public static final int ICON_SIZE = 18;

    /**
     * {@docfacto.system if need to change
     * <ul>
     * <li>brand colour: RGB(230,165,47)</li>
     * <li>1 Right on monochromatic scale: RGB(233,175,70)</li>
     * <li>2 right on monochromatic scale: RGB(235,185,93)</li> * </ul>}
     */
    final static RGB BEERMAT_BRAND_RGB = new RGB(225,154,27);

    public final static Color COLOR_BACKGROUND = SWTUtils.getColor(235,235,240);

    /**
     * Colour to use for separators
     */
    public final static Color COLOR_SEPARATOR = SWTUtils.getColor(190,190,190);

    /**
     * The lightest background colour, used for main/primary toolbars.
     */
    public final static Color COLOR_TOOLBAR_PRIMARY = SWTUtils.getColor(245,245,245);

    public final static Color COLOR_TOOLBAR_SECONDARY = SWTUtils.getColor(239,239,239);

    /**
     * The brand colour, hex #e9af46
     */
    public final static Color COLOR_FOREGROUND_BRAND = SWTUtils.getColor(BEERMAT_BRAND_RGB);

    /**
     * The complementary colour to the beermat brand colour
     */
    public final static Color COLOR_FOREGROUND_COMPLEMENTARY = SWTUtils.getColor(70,128,233);

    public final static Font DEFAULT_FONT = SWTUtils.getFont("Lucida Grande",9,SWT.BOLD);

    final static RGB BEERMAT_HIGHLIGHTED_RGB = new RGB(20,70,130);
    public final static Color BEERMAT_HIGHLIGHTED_COLOUR = SWTUtils.getColor(BEERMAT_HIGHLIGHTED_RGB);

    final static RGB BEERMAT_HOVER_RGB = new RGB(35,130,242);
    public final static Color BEERMAT_HOVER_COLOUR = SWTUtils.getColor(BEERMAT_HOVER_RGB);

    public final static Color BEERMAT_GRID_COLOR = SWTUtils.getColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND);

    public enum ToolbarPopup {
        HORIZONTAL,
        VERTICAL
    }
}
