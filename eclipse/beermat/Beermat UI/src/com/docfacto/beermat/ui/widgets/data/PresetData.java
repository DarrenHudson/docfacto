package com.docfacto.beermat.ui.widgets.data;

/**
 * Presets for the size widget
 *
 * @author kporter - created Jul 30, 2013
 * @since 2.4.2
 */
public class PresetData {
    private String name;
    private float width;
    private float height;
    private int resolution;
    private String thePreferredUnit;

    public PresetData(String name,float width,float height,int res) {
        this.name = name;
        this.width = width;
        this.height = height;
        this.resolution = res;
    }
    public PresetData(String name,float width,float height,int res, String preferredUnit) {
        this(name,width,height,res);
        setPreferredUnit(preferredUnit);
    }

    public String getName() {
        return name;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public int getResolution() {
        return resolution;
    }

    /**
     * Returns preferredUnit.
     * @return the preferredUnit
     */
    public String getPreferredUnit() {
        return thePreferredUnit;
    }

    /**
     * Sets preferredUnit.
     * @param preferredUnit the preferredUnit value
     */
    public void setPreferredUnit(String preferredUnit) {
        thePreferredUnit = preferredUnit;
    }
    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return getName();
    }
    
    
}
