package com.docfacto.beermat.ui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.geom.AffineTransform;

import javax.swing.AbstractAction;

/**
 * Scroll Action which stops scrolling beyond the bounds of the SVG Canvas
 * 
 * @author dhudson - created 12 Dec 2013
 * @since 2.5
 */
public class CanvasScrollAction extends AbstractAction {

    /**
     * @serial
     */
    private static final long serialVersionUID = 1L;

    private final BeermatAWTCanvas theCanvas;
    private final AffineTransform theTransform;
    private final BeermatScrollPane theScrollPane;

    CanvasScrollAction(BeermatScrollPane scrollPane,BeermatAWTCanvas canvas,
    double tx,double ty) {
        theCanvas = canvas;
        theTransform = AffineTransform.getTranslateInstance(tx,ty);
        theScrollPane = scrollPane;
    }

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        if (theCanvas.getCanvasGraphicsNode()==null) {
            return;
        }

        AffineTransform rat = theCanvas.getRenderingTransform();
        if (theTransform!=null) {
            Dimension dim = theCanvas.getSize();
            int x = dim.width/2;
            int y = dim.height/2;
            AffineTransform t = AffineTransform.getTranslateInstance(x,y);
            t.concatenate(theTransform);
            t.translate(-x,-y);
            t.concatenate(rat);

            double tY = t.getTranslateY();
            double tX = t.getTranslateX();
            
            if (tY>0||tX>0) {
                // Don't scroll
                return;
            }

            Dimension viewPort = theScrollPane.getViewport();
            
            if (tY<0) {
                int offset = (int)Math.abs(tY);
                offset += viewPort.height;
                
                if (offset>theCanvas.getSVGDocumentSize().getHeight()) {
                    return;
                }
            }

            if (tX<0) {
                int offset = (int)Math.abs(tX);
                offset += viewPort.width;

                if (offset>theCanvas.getSVGDocumentSize().getWidth()) {
                    return;
                }
            }

            theCanvas.setRenderingTransform(t);
        }
    }

}
