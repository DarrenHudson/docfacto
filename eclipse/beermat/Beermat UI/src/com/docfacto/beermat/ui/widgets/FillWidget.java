package com.docfacto.beermat.ui.widgets;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;

import com.docfacto.beermat.ui.ControlToolBar;
import com.docfacto.common.NameValuePair;
import com.docfacto.core.utils.SWTUtils;

/**
 * A widget for attributes pertaining to fill.
 * 
 * 
 * This small widget only contains the colour picker for fill. {@docfacto.media
 * uri="doc-files/fill-widget.png"}
 * 
 * @author dhudson - created 18 Jun 2013
 * @since 2.4
 * @docfacto.link uri="${doc-beermat}/controlbar/c_fill_widget.dita"
 * key="fill-widget" version="2.4.2"
 */
public class FillWidget extends AbstractControlBarWidget {

    private ColourPickerWidget theColourWidget;

    /**
     * Constructor.
     * 
     * @param toolbar parent
     * @since 2.4
     */
    public FillWidget(ControlToolBar toolbar) {
        super(toolbar);
    }

    /**
     * @see com.docfacto.beermat.ui.widgets.AbstractControlBarWidget#layoutWidget()
     */
    @Override
    protected void layoutWidget() {
        createLabel("Fill :");
        theColourWidget =
            new ColourPickerWidget(getToolbar().getController(),getParent());
        theColourWidget.getComposite().setLayoutData(getGridData(true));
    }

    /**
     * @see com.docfacto.beermat.ui.widgets.AbstractControlBarWidget#getAttributesForElement(org.w3c.dom.Element)
     */
    @Override
    public List<NameValuePair> getAttributesForElement(Element element) {
        List<NameValuePair> attrList = new ArrayList<NameValuePair>(1);
        NameValuePair fill = new NameValuePair("fill");
        attrList.add(fill);

        if (theColourWidget.getCurrentSelectedColour()==null) {
            fill.setValue("none");
        }
        else {
            fill.setValue(theColourWidget.getCurrentSelectedColour());
        }

        return attrList;
    }

    /**
     * Sets the colour in the colour widget to a hex value
     * 
     * @param hexColour The hex colour
     * @since 2.4.4
     */
    public void setSelectedColour(String hexColour) {
        theColourWidget.setColour(SWTUtils.convertHexToColor(hexColour),true);
    }
}
