package com.docfacto.beermat.ui.palette.items;

/**
 * A textual item that has no graphical representation.
 * 
 * May be used when a palette is not graphically shown or cannot be rendered,
 * therefore there is no parent to create GraphicItem's.
 * 
 * @author kporter - created Jan 16, 2013
 * @since 2.1
 */
public class TextItem<T> implements IPaletteItem<T> {

	private String theUniqueID;
	private T theSVGData;

	/**
	 * Constructor.
	 * 
	 * @param theURI
	 */
	public TextItem(T svgData) {
		theSVGData = svgData;
	}

	public TextItem(T svgData,String id) {
		theSVGData = svgData;
		theUniqueID = id;
	}

	/*
	 * (Non-Javadoc)
	 * 
	 * @see com.docfacto.beermat.ui.palette.items.Item#getSVG()
	 */
	@Override
	public T getSVGData() {
		return theSVGData;
	}

	/*
	 * (Non-Javadoc)
	 * 
	 * @see com.docfacto.beermat.ui.palette.items.Item#setSVG(java.lang.Object)
	 */
	@Override
	public void setSVGData(T svgData) {
		theSVGData = svgData;
	}

	/*
	 * (Non-Javadoc)
	 * 
	 * @see com.docfacto.beermat.ui.palette.items.Item#getUniqueID()
	 */
	@Override
	public String getUniqueID() {
		return theUniqueID;
	}

	/*
	 * (Non-Javadoc)
	 * 
	 * @see
	 * com.docfacto.beermat.ui.palette.items.Item#setUniqueID(java.lang.String)
	 */
	@Override
	public void setUniqueID(String name) {
	}

}
