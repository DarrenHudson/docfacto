package com.docfacto.beermat.ui.palette;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;

import com.docfacto.beermat.generated.Palette;
import com.docfacto.beermat.generated.SvgDef;
import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.beermat.ui.palette.items.CustomSVGItem;
import com.docfacto.beermat.ui.palette.items.IPaletteItem;
import com.docfacto.beermat.utils.PluginManager;

/**
 * Palette code removed from Beermat view.  Needs to go back at some point
 *
 * @author dhudson - created 19 Jun 2013
 * @since 2.4
 */
public class BeermatViewPaletteCode {

    public final static int DEFAULT_PALETTE_WIDTH = 165;
    
    private ArrayList<IPalette<? extends IPaletteItem<?>>> theCustomPalettes;
    private CTabFolder thePaletteFolder;
    
    private BeermatViewPaletteCode() {
        theCustomPalettes =
        new ArrayList<IPalette<? extends IPaletteItem<?>>>();
    }
    
    /**
     * A method to set the palette are given a Collection of Palette's
     * 
     * @param customPalettes
     * @since 2.1
     */
    public void
    addCustomPalettes(List<Palette> customPalettes,boolean internal) {
        if (thePaletteFolder!=null) {
            for (Palette p:customPalettes) {
                if (p!=null) {
                    List<SvgDef> svgDefs = PluginManager.getSVGDefs(p);
                    if (svgDefs!=null) {
                        WidgetPalette<CustomSVGItem> svgPalette =
                            new CustomSVGPalette(thePaletteFolder,SWT.NONE);
                        for (SvgDef s:svgDefs) {
                            CustomSVGItem c;
                            if (internal) {
                                if (s.getUri().startsWith("http://")) {
                                    c =
                                        new CustomSVGItem(svgPalette,SWT.NONE,
                                            s.getUri());
                                }
                                else {
                                    String uri =
                                        BeermatUIPlugin.getDefault()
                                            .getBundle()
                                            .getEntry(s.getUri())
                                            .toExternalForm();
                                    c =
                                        new CustomSVGItem(svgPalette,SWT.NONE,
                                            uri);
                                }
                            }
                            else {
                                c =
                                    new CustomSVGItem(svgPalette,SWT.NONE,
                                        s.getUri());
                            }
                            svgPalette.add(c);
                        }
                        addCustomTab(svgPalette,svgPalette.getPaletteName());
                    }
                }
            }
            thePaletteFolder.setSelection(0);
        }
        else
            return;
    }

    /**
     * Adds a palette tab
     * 
     * @param palette The Palette
     * @param name Name of the tab
     * @since 2.1
     */
    private void addCustomTab(
    WidgetPalette<? extends IPaletteItem<?>> palette,String name) {
        theCustomPalettes.add(palette);
        CTabItem item = new CTabItem(thePaletteFolder,SWT.NONE);
        item.setControl(palette);
        item.setText(name);
    }
    
    public void createCustomToolBar(List<Palette> palettes) {
//        for (com.docfacto.beermat.generated.Palette p:palettes) {
//            List<TextItem<?>> items = new ArrayList<TextItem<?>>(0);
//            List<SvgDef> svgdefs = PluginManager.getSVGDefs(p);
//
//            for (SvgDef s:svgdefs) {
//                String name = s.getName();
//                if (s.getUri()!=null
//                    &&!s.getUri().isEmpty()) {
//                    try {
//                        URI uri = new URI(s.getUri());
//                        items.add(new TextItem<URI>(uri,name));
//                    }
//                    catch (Exception e) {
//                    }
//                }
//                else if (s.getPath()!=null) {
//                    items.add(new TextItem<String>(s.getPath(),name));
//                }
//            }
//
//            ToolItem tool = new ToolItem(theSideToolbar,SWT.RADIO);
//            Menu m = new Menu(tool.getParent());
//            ToolbarPalette tempPal =
//                new ToolbarPalette(m,tool,theSelection,null);
//            String first = "";
//            boolean firstIsSet = false;
//            for (TextItem<?> t:items) {
//                if (!firstIsSet) {
//                    first = t.getUniqueID();
//                    firstIsSet = true;
//                }
//                tempPal.add(t);
//            }
//            tool.setText(first);
//            tool.addSelectionListener(tempPal);
//        }
    }
    
    /**
     * A method that creates a listviewer of palettes, each palette is displayed
     * as a list. change, or remove
     * 
     * @since 2.0.0 TODO: Remove?
     */
    @SuppressWarnings("unused")
    @Deprecated
    private void createListView() {
        Composite temp = new Composite(thePaletteFolder,SWT.NONE);
        temp.setLayout(new FormLayout());
        CTabItem item = new CTabItem(thePaletteFolder,0);
        item.setControl(temp);

        final ListViewer bottomListViewer =
            new ListViewer(temp,SWT.BORDER
                |SWT.V_SCROLL);
        final ListViewer topListViewer =
            new ListViewer(temp,SWT.BORDER
                |SWT.V_SCROLL);

        org.eclipse.swt.widgets.List list = bottomListViewer.getList();
        org.eclipse.swt.widgets.List topList = topListViewer.getList();

        FormData fd_list_1 = new FormData();
        fd_list_1.top = new FormAttachment(0,10);
        fd_list_1.bottom = new FormAttachment(0,212);
        fd_list_1.right = new FormAttachment(100,0);
        fd_list_1.left = new FormAttachment(0);
        topList.setLayoutData(fd_list_1);

        FormData fd_list = new FormData();
        fd_list.top = new FormAttachment(0,239);
        fd_list.bottom = new FormAttachment(100);
        fd_list.left = new FormAttachment(0);
        fd_list.right = new FormAttachment(100);
        list.setLayoutData(fd_list);

        topListViewer.setContentProvider(new ArrayContentProvider());
        bottomListViewer.setContentProvider(new ArrayContentProvider());

        item.setText("Blobbys");
        topListViewer.setLabelProvider(new LabelProvider() {
            @Override
            public String getText(Object element) {
                return ((IPalette<?>)element).getPaletteName();
            }
        });
        topListViewer.setInput(theCustomPalettes);
        bottomListViewer.setInput(((IPalette<?>)topListViewer.getElementAt(0))
            .getItems());
        bottomListViewer.setLabelProvider(new ItemNameLabelProvider());
        // topListViewer.setSelection((ISelection)topListViewer.getElementAt(0));
        topListViewer
            .addSelectionChangedListener(new ISelectionChangedListener() {
                @Override
                public void selectionChanged(SelectionChangedEvent event) {
                    StructuredSelection s =
                        (StructuredSelection)event.getSelection();
                    IPalette<?> p = (IPalette<?>)s.getFirstElement();
                    bottomListViewer.setInput(p.getItems());
                }
            });
    }
}
