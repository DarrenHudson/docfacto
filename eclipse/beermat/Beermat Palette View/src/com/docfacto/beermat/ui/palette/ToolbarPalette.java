package com.docfacto.beermat.ui.palette;

import java.util.HashMap;

import org.eclipse.swt.widgets.MenuItem;

import com.docfacto.beermat.ui.palette.items.TextItem;

/**
 * TODO - Title.
 * <p>
 * TODO - Description.
 * </p>
 * 
 * @author kporter - created Feb 22, 2013
 * @since n.n
 */
public abstract class ToolbarPalette extends ToolMenuSelectionListener implements
IPalette<TextItem<?>> {

	private HashMap<TextItem<?>,MenuItem> theItems;
	private String theUniqueName;

//	public ToolbarPalette(Menu menu,ToolItem item,final Selection select,
//	IToolSelectionController controller) {
//		super(menu,item,select,controller);
//		theItems = new HashMap<TextItem<?>,MenuItem>(0);
//	}
//
//	public ToolbarPalette(Menu menu,ToolItem item,final Selection select,
//	IToolSelectionController controller,String paletteName) {
//		this(menu,item,select,controller);
//		theUniqueName = paletteName;
//	}
//
//	public MenuItem add(final TextItem<?> item,boolean changed) {
//		MenuItem menuItem = super.add(item.getUniqueID(),Selection.CUSTOM);
//		theItems.put(item,menuItem);
//		return menuItem;
//	}

	/* (Non-Javadoc)
	 * @see com.docfacto.beermat.ui.palette.IPalette#add(java.lang.Object)
	 */
//	@Override
//	public void add(final TextItem<?> item) {
//		MenuItem menuItem = super.add(item.getUniqueID(),Selection.CUSTOM);
//		theItems.put(item,menuItem);
//	}

	/*
	 * (Non-Javadoc)
	 * @see com.docfacto.beermat.ui.palette.Palette#getItems()
	 */
//	@Override
//	public Collection<TextItem<?>> getItems() {
//		return theItems.keySet();
//	}

	/*
	 * (Non-Javadoc)
	 * @see com.docfacto.beermat.ui.palette.Palette#remove(java.lang.Object)
	 */
	@Override
	public void remove(TextItem<?> item) {
		//TODO: remove from GUI and then arraylist
	}

	/*
	 * (Non-Javadoc)
	 * @see com.docfacto.beermat.ui.palette.Palette#clear()
	 */
	@Override
	public void clear() {
		//TODO: clear all the GUI items and arraylist
	}

	/*
	 * (Non-Javadoc)
	 * @see com.docfacto.beermat.ui.palette.Palette#getPaletteName()
	 */
	@Override
	public String getPaletteName() {
		if (theUniqueName==null) {
			return DEFAULT_PALETTE_NAME;
		}
		return theUniqueName;
	}

}
