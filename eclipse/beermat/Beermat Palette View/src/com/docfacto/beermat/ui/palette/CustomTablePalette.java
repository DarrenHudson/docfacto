package com.docfacto.beermat.ui.palette;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import com.docfacto.beermat.ui.palette.items.TextItem;

/**
 * TODO - Title.
 * <p>
 * TODO - Description.
 * </p>
 * 
 * @author kporter - created Jan 16, 2013
 * @since n.n
 */
public class CustomTablePalette extends WidgetPalette<TextItem<?>> {

	private ListViewer viewer;

	/**
	 * Constructor.
	 * 
	 * @param parent
	 * @param style
	 */
	public CustomTablePalette(Composite parent,int style) {
		super(parent,style);
		setLayout(new FillLayout());
		appendBlank();

		viewer = new ListViewer(this,SWT.BORDER|SWT.V_SCROLL);
		viewer.setLabelProvider(new ItemNameLabelProvider());
		viewer.setContentProvider(new ArrayContentProvider());
		viewer.setInput(theItems);

	}

	// Remove after using this for testing
	public void appendBlank() {
		// URI uu = null;
		// try {
		// uu = new URL(
		// "platform:/plugin/com.docfacto.beermat.ui/resources/square.svg")
		// .toURI();
		// TextItem temp = new TextItem(uu);
		// theItems.add(temp);
		// }
		// catch (Exception e) {
		// e.printStackTrace();
		//
		// }
		// theItems.add(new TextItem(uu));
		// theItems.get(theItems.size()-1).setUniqueID(
		// "Blob - "+ (int)(Math.random()*100) );
	}

	/**
	 * TODO - Method Title
	 * <p>
	 * TODO - Method Description
	 * </p>
	 * 
	 * @param index
	 * @since n.n
	 */
	public void removeItem(int index) {
		theItems.remove(index);
	}

	/*
	 * (Non-Javadoc)
	 * 
	 * @see com.docfacto.beermat.ui.palette.Palette#remove(java.lang.Object)
	 */
	@Override
	public void remove(TextItem<?> item) {
		theItems.remove(item);
	}

	/*
	 * (Non-Javadoc)
	 * 
	 * @see com.docfacto.beermat.ui.Palette#clear()
	 */
	@Override
	public void clear() {
		theItems.clear();
	}

	/*
	 * (Non-Javadoc)
	 * 
	 * @see com.docfacto.beermat.ui.Palette#getPaletteName()
	 */
	@Override
	public String getPaletteName() {
		return "Table";
	}

	/*
	 * (Non-Javadoc)
	 * 
	 * @see com.docfacto.beermat.ui.palette.Palette#add(java.lang.Object)
	 */
	@Override
	public void add(TextItem<?> item) {
		// TODO Auto-generated method stub

	}
}
