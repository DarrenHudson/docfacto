package com.docfacto.beermat.ui.palette;

import java.util.Collection;

/**
 * An interface for Palette's.
 * 
 * @author kporter
 * @param <T>
 * @since 2.1
 * 
 */
public interface IPalette<T> {
	/**
	 * The default name for a palette if one is not set.
	 */
	final public static String DEFAULT_PALETTE_NAME = "Palette";

	/**
	 * Returns all the IPaletteItem's
	 * 
	 * @return returns the List of IPaletteItem's held by this palette.
	 * @since 2.1
	 */
	Collection<T> getItems();

	/**
	 * Adds an IPaletteItem to the Collection.
	 * 
	 * Should handle the IPaletteItem graphically if necessary.
	 * 
	 * @param item
	 * @since n.n
	 */
	void add(T item);

	/**
	 * Removes an IPaletteItem from the Collection.
	 * <p>
	 * Can be handled graphically or non graphically.
	 * </p>
	 * 
	 * @param IPaletteItem The item to remove
	 * @since 2.1
	 */
	void remove(T item);

	/**
	 * Removes all IPaletteItems from the palette.
	 * 
	 * @since 2.1
	 */
	void clear();

	/**
	 * Get the palette name
	 * 
	 * Returns the name of this palette, to identify and display.
	 * 
	 * @return The name of the palette
	 * @since 2.1
	 */
	String getPaletteName();
}
