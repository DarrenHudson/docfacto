package com.docfacto.beermat.ui.palette;

import org.eclipse.jface.viewers.LabelProvider;

import com.docfacto.beermat.ui.palette.items.IPaletteItem;

/**
 * Provides the Unique ID from palette items.
 * @author kporter - created Jan 17, 2013
 * @since n.n
 */
public class ItemNameLabelProvider extends LabelProvider {
	/* (Non-Javadoc)
	 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object element) {
		IPaletteItem<?> i = (IPaletteItem<?>)element;
		return i.getUniqueID();
	}
}
