package com.docfacto.beermat.ui.palette;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.docfacto.core.utils.SWTUtils;

/**
 * TODO - Title.
 * <p>
 * TODO - Description.
 * </p>
 * 
 * @author kporter - created Jan 16, 2013
 * @param <T>
 * @since n.n
 */
public abstract class WidgetPalette<T> extends Composite implements IPalette<T> {

	/**
	 * TODO - Field Description
	 */
	public static int DEFAULT_PALETTE_COLOR = SWT.COLOR_WHITE;

	/**
	 * TODO - Field Description
	 */
	private String thePaletteName = null;

	/**
	 * TODO - Field Description
	 */
	List<T> theItems;

	/**
	 * Constructor.
	 * 
	 * @param parent
	 * @param style
	 */
	public WidgetPalette(Composite parent,int style) {
		super(parent,style);
		theItems = new ArrayList<T>(0);
		this.setBackground(SWTUtils.getColor(DEFAULT_PALETTE_COLOR));
	}

	/*
	 * (Non-Javadoc)
	 * 
	 * @see com.docfacto.beermat.ui.Palette#getItems()
	 */
	@Override
	public List<T> getItems() {
		return theItems;
	}

	/**
	 * Returns thePaletteName held by this palette.
	 * 
	 * @return the thePaletteName
	 */
	public String getThePaletteName() {
		if (thePaletteName!=null) {
			return thePaletteName;
		}

		return DEFAULT_PALETTE_NAME;
	}

	/**
	 * Sets thePaletteName for this palette.
	 * 
	 * @param name the name to set to this palette.
	 */
	public void setThePaletteName(String name) {
		thePaletteName = name;
	}

}
