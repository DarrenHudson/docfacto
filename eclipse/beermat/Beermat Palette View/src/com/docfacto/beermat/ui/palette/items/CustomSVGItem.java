package com.docfacto.beermat.ui.palette.items;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.docfacto.beermat.plugin.BeermatUIPlugin;
import com.docfacto.beermat.ui.SVGComposite;

/**
 * An item that holds an SVG converted to a raster.
 * <P>
 * This Item when rendered converts an SVG to a raster PNG/GIF. The Image is
 * added to a label/button, held by this composite.
 * </P>
 * <p>
 * The Item should only be rendered once, upon initialisation, or when it's held
 * SVG is changed. Rendering every time this Item is displayed will be
 * inefficient, and slow to run.
 * </p>
 * 
 * @author kporter - created Jan 10, 2013
 * @since 2.1
 */
public class CustomSVGItem extends Composite implements IPaletteItem<URI> {
	/**
	 * TODO - Field Description
	 */
	URI theURI;

	/**
	 * TODO - Field Description
	 */
	String theName = "SVG";
	/**
	 * TODO - Field Description
	 */
	private SVGComposite theSVG;
	private final static String MISSING_SVG_PATH = "resources/svg/docfacto/missing.svg";
	
	private static URI getMissingImageURI(){
		try {
			return BeermatUIPlugin.getDefault().getBundle().getEntry(MISSING_SVG_PATH).toURI();
		}
		catch (URISyntaxException e) {
			return null;
		}
	}

	/**
	 * Constructor.
	 * 
	 * @param parent
	 * @param style
	 * @param uri
	 * @throws URISyntaxException 
	 */
	public CustomSVGItem(Composite parent,int style,String stringURI){		
		this(parent,style);
		try {
			theURI = new URI(stringURI);
		}
		catch (URISyntaxException e) {
			theURI = getMissingImageURI();
		}
		theSVG.setURI(theURI);
		addDragDropListener();
	}
	
	public CustomSVGItem(Composite parent, int style, URI uri){
		this(parent,style);
		theURI = uri;
		theSVG.setURI(theURI);
		addDragDropListener();
	}
	 
	private CustomSVGItem(Composite parent,int style){
		super(parent,style|SWT.EMBEDDED);
//		theSVG = new SVGComposite(this);
//		theSVG.setSize(GraphicItem.DEFAULT_WIDTH,getSize().y);
//		setSize(GraphicItem.DEFAULT_WIDTH,85);
//		theSVG.setEnabled(false);
	}

	/**
	 * TODO - Method Title
	 * <p>
	 * TODO - Method Description
	 * </p>
	 * 
	 * @param uri
	 * @since n.n
	 */
	@SuppressWarnings("unused")
	private void setItemName() {
		if (theURI!=null) {
			File f = new File(theURI);
			setUniqueID(f.getName());
		}
	}

	/*
	 * (Non-Javadoc)
	 * 
	 * @see com.docfacto.beermat.ui.Item#getSVGLocation()
	 */
	@Override
	public URI getSVGData() {
		return theURI;
	}

	/**
	 * TODO - Method Title
	 * <p>
	 * TODO - Method Description
	 * </p>
	 * 
	 * @since 2.1
	 */
	private void addDragDropListener() {
		final DragSource source =
			new DragSource(this,DND.DROP_MOVE|DND.DROP_COPY|DND.DROP_LINK);
		Transfer[] types = new Transfer[] {TextTransfer.getInstance()};
		source.setTransfer(types);
		source.addDragListener(new DragSourceAdapter() {
			@Override
			public void dragStart(DragSourceEvent dragSourceEvent) {
				Composite composite =
					(Composite)((DragSource)dragSourceEvent.getSource())
						.getControl();
				Point compositeSize = composite.getSize();
				GC gc = new GC(composite);

				Image image =
					new Image(Display.getCurrent(),compositeSize.x,
						compositeSize.y);
				gc.copyArea(image,0,0);
				dragSourceEvent.image = image;
			}

			@Override
			public void dragSetData(DragSourceEvent event) {
				event.data = theURI.toString();
			}

			@Override
			public void dragFinished(DragSourceEvent event) {
				event.doit = false;
			}
		});
	}

	/*
	 * (Non-Javadoc)
	 * 
	 * @see
	 * com.docfacto.beermat.ui.palette.items.Item#setSVGData(java.lang.Object)
	 */
	@Override
	public void setSVGData(URI svgData) {
		theURI = svgData;
	}

	/*
	 * (Non-Javadoc)
	 * 
	 * @see com.docfacto.beermat.ui.palette.items.Item#getUniqueID()
	 */
	@Override
	public String getUniqueID() {
		return theName;
	}

	/*
	 * (Non-Javadoc)
	 * 
	 * @see
	 * com.docfacto.beermat.ui.palette.items.Item#setUniqueID(java.lang.String)
	 */
	@Override
	public void setUniqueID(String id) {
		theName = id;
	}
}
