package com.docfacto.beermat.ui.palette.items;

/**
 * An item that holds an SVG converted to a raster.
 * <P>
 * This Item when rendered converts an SVG to a raster PNG/GIF. The Image is
 * added to a label/button, held by this composite.
 * </P>
 * <p>
 * The Item should only be rendered once, upon initialisation, or when it's held
 * SVG is changed. Rendering every time this Item is displayed will be
 * inefficient, and slow to run.
 * </p>
 * 
 * @author kporter - created Jan 10, 2013
 * @since 2.1
 */
public abstract class CustomRasterItem<T> implements IPaletteItem<T> {

}
