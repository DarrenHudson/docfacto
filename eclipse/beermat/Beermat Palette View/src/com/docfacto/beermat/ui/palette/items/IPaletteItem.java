package com.docfacto.beermat.ui.palette.items;

/**
 * An IPaletteItem is a way to hold a piece of data pertaining to a SVG
 * 
 * Implementations of this interface may hold a URI which points to an SVG file.
 * It may hold a Document, or a String with element data.
 * 
 * @author kporter - created Jan 18, 2013
 * @since 2.1
 */
public interface IPaletteItem<T> {

	/**
	 * A default name for IPaletteItem's, use if none is set.
	 */
	final public static String DEFAULT_ITEM_NAME = "SVG";

	/**
	 * This method returns the generic svg data held by this Item.
	 * <p>
	 * The SVG Data held. For example a Document, may be returned and elements
	 * can be extracted directly. Or if holding a URI then a Document can be
	 * created from this.
	 * </p>
	 * <p>
	 * The other understood data is a the string of a path.
	 * </p>
	 * 
	 * @return The piece of data held by the item, for example a String or URI.
	 * @since 2.1
	 */
	public T getSVGData();

	/**
	 * Sets the data.
	 * 
	 * @param svgData The svg data to set.
	 * @since 2.1
	 */
	void setSVGData(T svgData);

	/**
	 * A method to return a unique string pertaining to this item.
	 * 
	 * @return The unique ID.
	 * @since 2.1
	 */
	public String getUniqueID();

	/**
	 * A method to return the unique id.
	 * 
	 * @param id The String holding the unique id.
	 * @since 2.1
	 */
	public void setUniqueID(String id);

}
