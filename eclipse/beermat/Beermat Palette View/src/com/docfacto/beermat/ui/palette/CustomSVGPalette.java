package com.docfacto.beermat.ui.palette;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;

import com.docfacto.beermat.ui.palette.items.CustomSVGItem;

/**
 * TODO - Title.
 * <p>
 * TODO - Description.
 * </p>
 * 
 * @author kporter - created Jan 10, 2013
 * @since 2.1
 */
public class CustomSVGPalette extends WidgetPalette<CustomSVGItem> {

	/**
	 * Constructor.
	 * 
	 * @param parent
	 * @param style
	 */
	public CustomSVGPalette(Composite parent,int style) {
		super(parent,SWT.V_SCROLL);

		RowLayout rowLayout = new RowLayout(SWT.VERTICAL);
		rowLayout.pack = true;
		rowLayout.center = true;
		rowLayout.spacing = 5;
		setLayout(rowLayout);
		setSize(165,900);
	}

	/*
	 * (Non-Javadoc)
	 * 
	 * @see com.docfacto.beermat.ui.Palette#clear()
	 */
	@Override
	public void clear() {
		// TODO Auto-generated method stub
	}

	/*
	 * (Non-Javadoc)
	 * 
	 * @see com.docfacto.beermat.ui.Palette#getPaletteName()
	 */
	@Override
	public String getPaletteName() {
		return DEFAULT_PALETTE_NAME;
	}

	/*
	 * (Non-Javadoc)
	 * 
	 * @see com.docfacto.beermat.ui.palette.Palette#add(java.lang.Object)
	 */
	@Override
	public void add(CustomSVGItem item) {
		// TODO Auto-generated method stub
	}

	/*
	 * (Non-Javadoc)
	 * 
	 * @see com.docfacto.beermat.ui.palette.Palette#remove(java.lang.Object)
	 */
	@Override
	public void remove(CustomSVGItem item) {
		// TODO Auto-generated method stub
	}

}
