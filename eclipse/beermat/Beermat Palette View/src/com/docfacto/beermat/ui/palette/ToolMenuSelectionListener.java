package com.docfacto.beermat.ui.palette;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.ToolItem;

/**
 * A ToolItem that also holds a menu
 * 
 * @author kporter - created Mar 1, 2013
 * @since 2.2
 */
public class ToolMenuSelectionListener implements SelectionListener {
	private Menu theMenu;
	private ToolItem theParentItem;
//	private final Selection theType;
	//private final IToolSelectionController theController;

//	public ToolMenuSelectionListener(Menu menu,ToolItem tool,final Selection selection,
//	IToolSelectionController controller) {
//		theMenu = menu;
//		theParentItem = tool;
//		theType = selection;
//		theController = controller;
//	}

//	public MenuItem add(String altText, final Selection selection){
//		MenuItem menuItem = new MenuItem(theMenu,SWT.NONE);
//		menuItem.setText(altText);
//		return add(menuItem,selection);
//	}
	
//	public MenuItem add(Image image, final Selection selection){
//		MenuItem menuItem = new MenuItem(theMenu,SWT.NONE);
//		menuItem.setImage(image);
//		return add(menuItem, selection);
//	}
	
//	private MenuItem add(final MenuItem menuItem, final Selection selection) {
//		menuItem.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent event) {
//				MenuItem selected = (MenuItem)event.widget;
//				if (selected.getImage()!=null) {
//					getTheParentItem().setImage(selected.getImage());
//					getTheParentItem().setText("");
//				}
//				else {
//					getTheParentItem().setImage(null);
//					getTheParentItem().setText(selected.getText());
//				}
//				theController.setSelection(selection);
//			}
//		});
//		return menuItem;
//	}

	/*
	 * (Non-Javadoc)
	 * @see
	 * org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt
	 * .events.SelectionEvent)
	 */
	@Override
	public void widgetSelected(SelectionEvent e) {
//		theController.setSelection(theType);
//		ToolItem item = (ToolItem)e.widget;
//		Rectangle rect = item.getBounds();
//		Point pt = item.getParent().toDisplay(new Point(rect.x,rect.y));
//		if (theController.getSelection()==theType) {
//			theMenu.setLocation(pt.x,pt.y+rect.height);
//			theMenu.setVisible(true);
//		}
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		//Do nothing
	}

	public Menu getTheMenu() {
		return theMenu;
	}

	public ToolItem getTheParentItem() {
		return theParentItem;
	}

}
