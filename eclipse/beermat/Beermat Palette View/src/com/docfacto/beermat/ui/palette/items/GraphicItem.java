package com.docfacto.beermat.ui.palette.items;

import org.eclipse.swt.graphics.Point;

import com.docfacto.beermat.ui.palette.BeermatViewPaletteCode;

/**
 * A GraphicItem is an object representing a piece of SVG data, graphically
 * represented.
 * 
 * GraphicItem's have an actual size and are renderable.
 * 
 * @author kporter - created Jan 10, 2013
 * @since 2.1
 */
public interface GraphicItem<T> extends IPaletteItem<T> {
	/**
	 * The default width of a GraphicItem.
	 * Set to 165
	 */
	public static int DEFAULT_WIDTH = BeermatViewPaletteCode.DEFAULT_PALETTE_WIDTH;

	/**
	 * The default height of a GraphicItem, use if none is set.
	 */
	public static int DEFAULT_HEIGHT = 75;

	/**
	 * Sets the size
	 * 
	 * Sets the size of this using a Point to get a width and height.
	 * 
	 * @param size The size to set this GraphicItem, as a Point.
	 * @since 2.1
	 */
	void setSize(Point size);

	/**
	 * Get the height
	 * 
	 * A method to return the height of the item.
	 * 
	 * @return the height int.
	 * @since 2.1
	 */
	int getHeight();

	/**
	 * Get the width
	 * 
	 * A method to return the width of the item.
	 * 
	 * @return the width int.
	 * @since 2.1
	 */
	int getWidth();

	/**
	 * A method to render this item
	 * 
	 * As graphically this Item could be represented by an JSVGCanvas or by an
	 * Image (rastered), each implementing interface should render their item
	 * accordingly.
	 * 
	 * @since 2.1
	 */
	abstract void render();
}
