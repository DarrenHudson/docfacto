package com.docfacto.taglets.plugin;

import org.eclipse.core.runtime.Status;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.licensor.Products;
import com.docfacto.taglets.TagletLoader;

/**
 * The activator class controls the plug-in life cycle
 */
public class DocfactoTagletsPlugin extends AbstractUIPlugin {

    // The plug-in ID
    public static final String PLUGIN_ID = "com.docfacto.taglets"; //$NON-NLS-1$

    // The shared instance
    private static DocfactoTagletsPlugin thePlugin;

    private static TagletProcessor theTagletProcessor;

    // Only need one of these to be shared across the plugin
    private static TagletLoader theTagletLoader;

    /**
     * The constructor
     */
    public DocfactoTagletsPlugin() {
        // Never put anything in the constructor, can be called more than once.
    }

    /**
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
     */
    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);

        thePlugin = this;

        theTagletLoader = new TagletLoader();

        theTagletProcessor = new TagletProcessor(theTagletLoader);

        // Lets register the plugin
        DocfactoCorePlugin.registerPlugin("Taglets",Products.TAGLETS);
        
        logMessage("Docfacto Taglets Plugin loaded");
    }

    /**
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
     */
    @Override
    public void stop(BundleContext context) throws Exception {
        thePlugin = null;
        super.stop(context);
    }

    /**
     * Returns the shared instance
     * 
     * @return the shared instance
     * @since 1.1
     */
    public static DocfactoTagletsPlugin getDefault() {
        return thePlugin;
    }

    /**
     * Log and exception to the console
     * 
     * @param message to display
     * @param ex cause, can be null
     * @since 1.1
     */
    public static void logException(String message,Exception ex) {
        thePlugin.getLog().log(
            new Status(Status.ERROR,PLUGIN_ID,Status.ERROR,message,ex));
    }

    /**
     * Log a message to the console
     * 
     * @param message to display
     * @since 2.4
     */
    public static void logMessage(String message) {
        thePlugin.getLog().log(new Status(Status.INFO,PLUGIN_ID,Status.INFO,
            message,null));
    }
    
    /**
     * Have once instance of the taglet loader for all to share as its read only
     * 
     * @return the taglet loader
     * @since 2.2
     */
    public static TagletLoader getTagletLoader() {
        return theTagletLoader;
    }

    /**
     * Get the Taglet processor
     * 
     * @return the taglet processor
     * @since 2.2
     */
    public static TagletProcessor getTagletProcessor() {
        return theTagletProcessor;
    }
}
