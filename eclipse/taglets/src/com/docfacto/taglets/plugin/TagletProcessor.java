package com.docfacto.taglets.plugin;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jdt.core.dom.TextElement;

import com.docfacto.config.XmlConfig;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.javadoc.TagParser;
import com.docfacto.taglets.TagletLoader;
import com.docfacto.taglets.eclipse.AbstractEclipseTaglet;

/**
 * JavadocContentAccess3 is basically a renamed JavadocContentAccess2 from the
 * Eclipse source, but I needed a way to override the taglet behaviour for
 * taglets that it doesn't understand, like the Docfacto Taglets.
 * 
 * 
 * 
 * @author dhudson - created 19 Feb 2013
 * @since 2.2
 */
@SuppressWarnings("restriction")
public class TagletProcessor {

    private HashMap<String,AbstractEclipseTaglet> theTagletMap;

    /**
     * Constructor.
     * 
     * @since 2.2
     */
    public TagletProcessor(TagletLoader loader) {

        theTagletMap = new HashMap<String,AbstractEclipseTaglet>();

        XmlConfig xmlConfig =
            DocfactoCorePlugin.getXmlConfig();

        if (xmlConfig!=null) {
            for (AbstractEclipseTaglet taglet:loader.getEclipseTaglets()) {
                theTagletMap.put(taglet.getName(),taglet);
            }
        }
    }

    /**
     * Check to see if we know about the taglet
     * 
     * @param tagName to process
     * @param isInline true if an inline tag
     * @return true if we can handle the taglet
     * @since 2.2
     */
    public boolean willHandle(String tagName,boolean isInline) {
        if (tagName==null) {
            return false;
        }

        // Remove the @
        AbstractEclipseTaglet taglet = theTagletMap.get(tagName.substring(1));

        if (taglet==null) {
            return false;
        }

        if (isInline) {
            return taglet.isInlineTag();
        }

        return true;
    }

    /**
     * Handle the taglet
     * 
     * @param tag to process
     * @param buffer the {@code StringBuffer} to append to
     * @since 2.2
     */
    public void handle(TagElement tag,StringBuffer buffer) {
        final String tagName = tag.getTagName().substring(1);
        AbstractEclipseTaglet taglet = theTagletMap.get(tagName);

        if (taglet==null) {
            // Shouldn't happen
            return;
        }

        StringBuilder builder = new StringBuilder(10);

        List fragments = tag.fragments();

        for (Iterator iter = fragments.iterator();iter.hasNext();) {
            ASTNode child = (ASTNode)iter.next();

            if (child.getNodeType()==ASTNode.TEXT_ELEMENT) {
                TextElement el = (TextElement)child;
                builder.append(el.getText());
            }

            if (child.getNodeType()==ASTNode.TAG_ELEMENT) {
                // Its has a inline tag with it ..
                // TODO: this probably needs to be a call back
            }
        }

        String tagText = builder.toString().trim();
        TagParser parser = new TagParser(tagText);

        buffer.append(taglet.getEclipseBlockOutput(parser));
    }

    /**
     * Process the inline tag, populating the provided buffer
     * 
     * @param tag to process
     * @param parser to process
     * @param buffer to populate
     * @since 2.2
     */
    public void
    handleInline(TagElement tag,TagParser parser,StringBuffer buffer) {
        final String tagName = tag.getTagName().substring(1);
        AbstractEclipseTaglet taglet = theTagletMap.get(tagName);

        if (taglet==null) {
            // Shouldn't happen
            return;
        }
        
        buffer.append(taglet.getEclipseInlineOutput(parser));
    }
}
