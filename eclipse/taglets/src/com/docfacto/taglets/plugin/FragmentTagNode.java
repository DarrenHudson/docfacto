package com.docfacto.taglets.plugin;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.TagElement;

/**
 * TagElement Fragment
 * 
 * @author dhudson - created 20 Mar 2013
 * @since 2.2
 */
public class FragmentTagNode extends FragmentTreeNode {

    private List<FragmentTagNode> theChildren;
    private StringBuilder theResult;
    private FragmentTagNode theParent = null;
    int theLastNodeProcessed = 0;
    private boolean isComplete = false;
    
    /**
     * Constructor.
     * 
     * @param node
     */
    public FragmentTagNode(ASTNode node) {
        super(node);
        theChildren = new ArrayList<FragmentTagNode>(3);
        theResult = new StringBuilder(100);
    }

    /**
     * @see com.docfacto.taglets.plugin.FragmentTreeNode#isTagNode()
     */
    @Override
    public boolean isTagNode() {
        return true;
    }

    /**
     * Check to see if the tag has nested inline tags
     * 
     * @return true if this tag element has a nested tag element
     * @since 2.2
     */
    public boolean hasChildren() {
        return !theChildren.isEmpty();
    }

    /**
     * Add a child
     * 
     * @param node
     * @since 2.2
     */
    public void addNode(FragmentTagNode node) {
        theChildren.add(node);
        node.setParent(this);
    }

    /**
     * Fetch the Tag Element
     * 
     * @return the tag element
     * @since 2.2
     */
    public TagElement getTagElement() {
        return (TagElement)getASTNode();
    }

    /**
     * Append the result to the existing result, if there is any
     * 
     * @param result
     * @since 2.2
     */
    public void appendResult(String result) {
        theResult.append(result);
    }

    /**
     * Return the result of running the inline tag
     * 
     * @return the output of the inline tag
     * @since 2.2
     */
    public String getResult() {
        return theResult.toString();
    }

    public void setResult(String result) {
        theResult = new StringBuilder(result.length());
        theResult.append(result);
    }
    
    /**
     * Parent node
     * 
     * @return the parent node, or null if its the root node
     * @since 2.2
     */
    public FragmentTagNode getParent() {
        return theParent;
    }

    /**
     * Set the parent node
     * 
     * @param node parent
     * @since 2.2
     */
    public void setParent(FragmentTagNode node) {
        theParent = node;
    }
    
    /**
     * Check to see if the node has a parent
     * 
     * @return true if the node has a parent
     * @since 2.2
     */
    public boolean hasParent() {
        if (theParent==null) {
            return false;
        }

        return true;
    }

    /**
     * Set the last node processed on the root node
     * 
     * @param index of the last node
     * @since 2.2
     */
    public void setLastNodeProcessed(int index) {
//       // Get the root node;
//       FragmentTagNode root = this;
//       while(root.hasParent()) {
//           root = root.getParent();
//       }
       
       theLastNodeProcessed = index;
    }
    
    /**
     * The last node if the list of nodes that was processed
     * 
     * @return the index of the last node processed in the list
     * @since 2.2
     */
    public int getLastProcessedNode() {
        return theLastNodeProcessed;
    }
    
    public boolean isComplete() {
        return isComplete;
    }
    
    public void setComplete() {
        isComplete = true;
    }
    
    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return getTagElement().getTagName()+" : "+theResult;
    }
}
