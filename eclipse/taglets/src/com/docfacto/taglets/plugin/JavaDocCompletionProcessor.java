/*
 * @author dhudson -
 * Created 16 Jan 2013 : 16:12:06
 */

package com.docfacto.taglets.plugin;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.ui.text.java.ContentAssistInvocationContext;
import org.eclipse.jdt.ui.text.java.IJavaCompletionProposalComputer;
import org.eclipse.jdt.ui.text.java.JavaContentAssistInvocationContext;
import org.eclipse.jface.text.contentassist.CompletionProposal;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.swt.graphics.Point;

import com.docfacto.taglets.TagletLoader;
import com.docfacto.taglets.generated.Attribute;
import com.docfacto.taglets.generated.InternalTagletDefinition;

/**
 * Content proposal computer
 * 
 * @author dhudson - created 16 Jan 2013
 * @since 1.1
 */
public class JavaDocCompletionProcessor implements
IJavaCompletionProposalComputer {

    private static final List<ICompletionProposal> NO_PROPOSALS =
        new ArrayList<ICompletionProposal>(0);

    private static final List<IContextInformation> NO_CONTEXTS =
        new ArrayList<IContextInformation>(0);

    private final List<InternalTagletDefinition> theInternalTaglets;

    /**
     * Constructor.
     */
    public JavaDocCompletionProcessor() {
        // Load the taglets from Docfacto-x.x.x.jar
        TagletLoader loader = DocfactoTagletsPlugin.getTagletLoader();

        theInternalTaglets = new ArrayList<InternalTagletDefinition>(loader.getCustomTaglets().size());
        
        for (final InternalTagletDefinition tagDef:loader
            .getCustomTaglets()) {
            theInternalTaglets.add(tagDef);
            // Change the description to smart text and include attribute
            // information
            tagDef.setDescription(getDescription(tagDef));
        }
    }

    /**
     * @see org.eclipse.jdt.ui.text.java.IJavaCompletionProposalComputer#computeCompletionProposals(org.eclipse.jdt.ui.text.java.ContentAssistInvocationContext,
     * org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public List<ICompletionProposal> computeCompletionProposals(
    ContentAssistInvocationContext context,IProgressMonitor monitor) {

        if (context instanceof JavaContentAssistInvocationContext) {
            final JavaContentAssistInvocationContext jcontext =
                (JavaContentAssistInvocationContext)context;

            Point point = context.getViewer().getSelectedRange();
            if (point.y>0) {
                // // Do something here is the text is selected..
                // try {
                // String text = context.getDocument().get(point.x,point.y);
                // }
                // catch (BadLocationException ex) {
                // DocfactoTagletsPlugin.logException(
                // "Unable to compute completion proposals",ex);
                // return NO_PROPOSALS;
                // }
                return NO_PROPOSALS;
            }

            List<ICompletionProposal> proposals =
                new ArrayList<ICompletionProposal>(3);

            TagQualifier qualifier =
                new TagQualifier(jcontext.getDocument(),
                    jcontext.getInvocationOffset());

            if (qualifier.isAttributeAssist()) {
                InternalTagletDefinition tagDef =
                    getInternalTaglet(qualifier.getTagletName());
                if (tagDef==null) {
                    return NO_PROPOSALS;
                }

                if (tagDef.getAttributes()==null||
                    tagDef.getAttributes().getAttributes()==null) {
                    return NO_PROPOSALS;
                }

                for (Attribute attribute:tagDef.getAttributes().getAttributes()) {

                    if (qualifier.hasAttribute(attribute.getName())) {
                        continue;
                    }
                    String propText;
                    int cursorPosition = 0;
                    if (attribute.isHasValue()) {
                        propText = attribute.getName()+"=\"\"";
                        // Position the cursor in the quotes
                        cursorPosition = propText.length()-1;
                    }
                    else {
                        propText = attribute.getName();
                        cursorPosition = propText.length();
                    }

                    proposals.add(new CompletionProposal(propText,jcontext
                        .getInvocationOffset(),0,cursorPosition,null,
                        attribute.getName(),null,attribute.getDescription()));
                }
            }
            else {

                for (InternalTagletDefinition tagDef:theInternalTaglets) {
                    if (tagDef.getName().startsWith(qualifier.getPrefix())) {

                        // Seems that the Eclipse one will always list them
                        // if (qualifier.isBlockOnly()) {
                        // if (!tagDef.isBlock()) {
                        // continue;
                        // }
                        // }

                        int cursor = tagDef.getName().length();
                        int prefixLength = qualifier.getPrefix().length();

                        proposals.add(new CompletionProposal(tagDef.getName(),
                            jcontext.getInvocationOffset()-prefixLength,
                            prefixLength,cursor,null,"@"+tagDef.getName(),null,
                            tagDef.getDescription()));
                    }
                }
            }

            return proposals;
        }

        return NO_PROPOSALS;
    }

    /**
     * Build the description with attribute information
     * 
     * @param tagDef to process
     * @return the description, as well as any attributes
     * @since 2.2
     */
    private String getDescription(InternalTagletDefinition tagDef) {

        if (tagDef.getAttributes()==null||
            tagDef.getAttributes().getAttributes()==null) {
            return "";
        }

        StringBuilder builder = new StringBuilder(50);
        builder.append(tagDef.getDescription());

        if (tagDef.getAttributes().getAttributes().size()>0) {
            builder.append("<br></br><p>Attributes: <dl>");
            for (Attribute attr:tagDef.getAttributes().getAttributes()) {
                builder.append("<dt>");
                if (attr.isRequired()) {
                    builder.append("<i>");
                }
                builder.append(attr.getName());
                if (attr.isRequired()) {
                    builder.append("</i>");
                }
                builder.append("</dt><dd>");
                builder.append(attr.getDescription());
                builder.append("</dd>");
            }
            builder.append("</dl></p>");
        }

        return builder.toString();
    }

    /**
     * Return the internal taglet definition.
     * 
     * @param tagletName to find
     * @return Internal Taglet Definition or null
     * @since 2.2
     */
    private InternalTagletDefinition getInternalTaglet(String tagletName) {
        if (tagletName==null) {
            return null;
        }

        for (InternalTagletDefinition tagDef:theInternalTaglets) {
            if (tagDef.getName().equals(tagletName)) {
                return tagDef;
            }
        }

        return null;
    }

    /**
     * @see org.eclipse.jdt.ui.text.java.IJavaCompletionProposalComputer#computeContextInformation(org.eclipse.jdt.ui.text.java.ContentAssistInvocationContext,
     * org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public List<IContextInformation> computeContextInformation(
    ContentAssistInvocationContext context,IProgressMonitor monitor) {
        return NO_CONTEXTS;
    }

    /**
     * @see org.eclipse.jdt.ui.text.java.IJavaCompletionProposalComputer#getErrorMessage()
     */
    @Override
    public String getErrorMessage() {
        return null;
    }

    /**
     * @see org.eclipse.jdt.ui.text.java.IJavaCompletionProposalComputer#sessionEnded()
     */
    @Override
    public void sessionEnded() {
    }

    /**
     * @see org.eclipse.jdt.ui.text.java.IJavaCompletionProposalComputer#sessionStarted()
     */
    @Override
    public void sessionStarted() {
    }

}
