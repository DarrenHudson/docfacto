package com.docfacto.taglets.plugin;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;

import com.docfacto.javadoc.TagParser;

/**
 * This class is used to determine what proposals to but forward.
 * 
 * @author dhudson - created 14 Feb 2013
 * @since 2.2
 */
public class TagQualifier {

    private final IDocument theDocument;
    private final int theOffset;

    private boolean isBlockOnly = false;
    private String thePrefix;
    private boolean isAttributeAssist;

    private String theTagletName;
    private int theTagOffset;

    private TagParser theTagParser;

    /**
     * Constructor.
     * 
     * @param context
     * @since 2.2
     */
    public TagQualifier(IDocument document,int offset) {
        theDocument = document;
        theOffset = offset;

        parse();
    }

    /**
     * Parse the document to find out what we are doing
     *
     * @since 2.2
     */
    private void parse() {

        int documentOffset = theOffset;

        // Use string buffer to collect characters
        StringBuffer buffer = new StringBuffer(20);
        while (true) {
            try {
                // Read character backwards
                char c = theDocument.getChar(--documentOffset);

                // Start of tag. Return qualifier
                if (c=='@') {
                    if (theDocument.getChar(documentOffset-1)!='{') {
                        isBlockOnly = true;
                    }

                    thePrefix = buffer.reverse().toString();
                    theTagOffset = documentOffset;
                    // If has space, then it will not just be a tag list
                    int spaceIndex = thePrefix.indexOf(' ');

                    if (spaceIndex>0) {
                        isAttributeAssist = true;
                        theTagletName = thePrefix.substring(0,spaceIndex);
                        theTagParser = new TagParser(getTagletText());
                    }

                    return;
                }

                // Collect character
                buffer.append(c);
            }
            catch (BadLocationException e) {
                thePrefix = "";
                return;
            }
        }
    }

    /**
     * Get the taglet text
     * 
     * @return the text for the taglet, if possible, otherwise return
     * {@code null}
     * @since 2.2
     */
    public String getTagletText() {

        if (theTagletName==null) {
            // Can't process this.
            return null;
        }

        int startIndex = theTagOffset+theTagletName.length()+1;
        int tagletOffset = startIndex;

        while (true) {
            try {
                char c = theDocument.getChar(tagletOffset);
                if (c=='*') {
                    if (theDocument.getChar(tagletOffset+1)=='/') {
                        // Its the end of the comment..
                        return theDocument.get(startIndex,
                            (tagletOffset-startIndex));
                    }
                }

                if (c=='\n') {
                    if (isBlockOnly()) {
                        return theDocument.get(startIndex,
                            (tagletOffset-startIndex));
                    }
                }

                if (c=='}') {
                    return theDocument
                        .get(startIndex,(tagletOffset-startIndex));
                }

                tagletOffset++;
            }
            catch (BadLocationException e) {
                return null;
            }
        }
    }

    /**
     * Return true of the comment has the attribute
     * 
     * @param attributeName to locate
     * @return true if the attribute exists
     * @since 2.2
     */
    public boolean hasAttribute(String attributeName) {
        if (theTagParser==null) {
            return false;
        }

        return theTagParser.hasAttribute(attributeName);
    }

    /**
     * Check to see if the prefix shows that the tag is block only
     * 
     * @return true if its block only taglet
     * @since 2.2
     */
    public boolean isBlockOnly() {
        return isBlockOnly;
    }

    /**
     * Get the preceding characters to help with the assist
     * 
     * @return the context of the help
     * @since 2.2
     */
    public String getPrefix() {
        return thePrefix;
    }

    /**
     * Check to see if the assist is attribute of a tag
     * 
     * @return true if its not help with a tag name
     * @since 2.2
     */
    public boolean isAttributeAssist() {
        return isAttributeAssist;
    }

    /**
     * The taglet name if in attribute assist
     * 
     * @return the taglet name, or null
     * @since 2.2
     */
    public String getTagletName() {
        return theTagletName;
    }

    /**
     * Return the document offset of the tag
     * 
     * @return the document offset of the tag
     * @since 2.2
     */
    public int theTagOffset() {
        return theTagOffset;
    }

    /**
     * Return if tag parser if created or null. This will only be created in
     * code assist entries
     * 
     * @return the parser or null
     * @since 2.2
     */
    public TagParser getTagParser() {
        return theTagParser;
    }
}
