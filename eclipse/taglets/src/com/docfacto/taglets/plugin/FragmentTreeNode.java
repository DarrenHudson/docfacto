package com.docfacto.taglets.plugin;

import org.eclipse.jdt.core.dom.ASTNode;

/**
 * AST can't handle nested inline tags, it just lists them in order, so what
 * this does is to make a tree out of it.
 * 
 * @author dhudson - created 19 Mar 2013
 * @since 2.2
 */
public abstract class FragmentTreeNode {
;

    private final ASTNode theNode;

    /**
     * Constructor.
     * @param node
     */
    public FragmentTreeNode(ASTNode node) {
        theNode = node;
    }

    /**
     * Return the AST node fragment
     * 
     * @return the fragment
     * @since 2.2
     */
    public ASTNode getASTNode() {
        return theNode;
    }
    
    /**
     * Check to see if its a text node
     * 
     * @return true if its a {@code TextElement} node
     * @since 2.2
     */
    public boolean isTextNode() {
        return false;
    }
    
    /**
     * Check to see if its a tag node
     * 
     * @return true if the node is a {@code TagElement} node
     * @since 2.2
     */
    public boolean isTagNode() {
        return false;
    }
}
