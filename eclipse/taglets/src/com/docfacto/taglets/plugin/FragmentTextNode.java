package com.docfacto.taglets.plugin;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.TextElement;

/**
 * Text Node Fragment
 *
 * @author dhudson - created 20 Mar 2013
 * @since 2.2
 */
public class FragmentTextNode extends FragmentTreeNode {

    /**
     * Constructor.
     * @param node
     */
    public FragmentTextNode(TextElement node) {
        super((ASTNode) node);
    }

    /**
     * Get the AST node as a {@code TextElement} 
     * 
     * @return Text Node
     * @since 2.2
     */
    public TextElement getTextElement() {
        return (TextElement) getASTNode();
    }

    /**
     * @see com.docfacto.taglets.plugin.FragmentTreeNode#isTextNode()
     */
    @Override
    public boolean isTextNode() {
        return true;
    }

}
