package com.docfacto.taglets.plugin.hover;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.internal.text.html.HTMLPrinter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;

import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.core.IDocfactoCorePlugin;
import com.docfacto.core.hovers.DualAbstractInformationControl;
import com.docfacto.core.hovers.LaunchAction;
import com.docfacto.core.hovers.TagletInput;
import com.docfacto.core.utils.ImageUtils;
import com.docfacto.javadoc.TagParser;
import com.docfacto.taglets.plugin.DocfactoTagletsPlugin;
import com.docfacto.taglets.plugin.TagletProcessor;

/**
 * Display an image in a browser.
 * 
 * This is different to BrowserInformationControl as it has a launch action and
 * it sets the size according to the image
 * 
 * @author dhudson - created 23 Sep 2013
 * @since 2.4
 */
public class ImageMediaInformationControl extends
DualAbstractInformationControl {

    private TagParser theTagParser;
    private Browser theBrowser;
    private TagletProcessor theTagletProcessor =
        DocfactoTagletsPlugin.getTagletProcessor();
    private LaunchAction theLaunchAction;

    private Point theSizeHint;
    private static final int MAX_SIZE = 1200;
    
    /**
     * Constructor.
     * 
     * @param parentShell parent shell
     * @since 2.4
     */
    public ImageMediaInformationControl(Shell parentShell) {
        super(parentShell);

        theLaunchAction = new LaunchAction(DocfactoCorePlugin
            .getImageDescriptor(IDocfactoCorePlugin.GRABIT_ICON_16),
            "Open in Grabit",this);

        create();
    }

    /**
     * Return the launch action
     * 
     * @return the Grabit launch action
     * @since 2.4
     */
    @Override
    public List<Action> getToolBarActions() {
        List<Action> actions = new ArrayList<Action>(1);
        actions.add(theLaunchAction);

        return actions;
    }

    /**
     * @see org.eclipse.jface.text.IInformationControlExtension#hasContents()
     */
    @Override
    public boolean hasContents() {
        return true;
    }

    /**
     * @see DualAbstractInformationControl.createContent
     */
    @Override
    protected void createContent(Composite parent) {
        theBrowser = new Browser(parent,SWT.NONE);
        theBrowser.setMenu(new Menu(getShell(),SWT.NONE));
    }

    /**
     * @see com.docfacto.core.hovers.DualAbstractInformationControl#setInput(java.lang.Object)
     */
    @Override
    public void setInput(Object input) {
        TagletInput tagletInput = (TagletInput)input;
        theTagParser = tagletInput.getParser();

        theLaunchAction.setFileToLaunch(theTagParser
            .getAttributeValue(TagParser.URI_ATTRIBUTE));

        Image image;
        try {

            image = ImageUtils.getImageFromFile(theTagParser
                .getAttributeValue(TagParser.URI_ATTRIBUTE));

            if (image!=null) {
                
                theSizeHint =
                    new Point(image.getBounds().width+10,
                        image.getBounds().height + (computeTrim().height*2));
                
                theSizeHint.x = Math.min(MAX_SIZE,theSizeHint.x);
                theSizeHint.y = Math.min(MAX_SIZE,theSizeHint.y);
                
                image.dispose();
            }
        }
        catch (IOException ignore) {
        }

        // Generate the contents for the browser
        StringBuffer buffer = new StringBuffer(200);

        TagElement tag = (TagElement)theTagParser.getAttachment();

        if (theTagParser.isInLine()) {
            theTagletProcessor.handleInline(tag,theTagParser,buffer);
        }
        else {
            theTagletProcessor.handle(tag,buffer);
        }

        HTMLPrinter.addPageEpilog(buffer);

        theBrowser.setText(buffer.toString());
        theBrowser.setVisible(true);
    }

    /**
     * @see com.docfacto.core.hovers.DualAbstractInformationControl#computeSizeHint()
     */
    @Override
    public Point computeSizeHint() {
        System.out.println("computeSize ...");
        
        if (theSizeHint!=null) {

            return theSizeHint;
        }
        return super.computeSizeHint();
    }

}
