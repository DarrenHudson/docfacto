package com.docfacto.taglets.plugin.hover;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.internal.text.html.HTML2TextReader;
import org.eclipse.jface.internal.text.html.HTMLPrinter;
import org.eclipse.jface.text.TextPresentation;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.graphics.TextLayout;
import org.eclipse.swt.widgets.Shell;

import com.docfacto.core.hovers.AbstractBrowserInformationControl;
import com.docfacto.core.hovers.TagletInput;
import com.docfacto.javadoc.TagParser;
import com.docfacto.taglets.plugin.DocfactoTagletsPlugin;
import com.docfacto.taglets.plugin.TagletProcessor;

/**
 * Simple Browser information control.
 * 
 * @author dhudson - created 23 Sep 2013
 * @since 2.4
 */
public class BrowserInformationControl extends AbstractBrowserInformationControl {

    private TagParser theTagParser;

    private TagletProcessor theTagletProcessor =
        DocfactoTagletsPlugin.getTagletProcessor();
    private StringBuffer theContent;
    // Used for bounds information
    private TextLayout theTextLayout;

    private static final int MIN_WIDTH = 80;
    private static final int MIN_HEIGHT = 80;

    /**
     * Constructor.
     * 
     * @param parentShell parent shell
     * 
     * @since 2.4
     */
    public BrowserInformationControl(Shell parentShell) {
        super(parentShell);
        theTextLayout = new TextLayout(parentShell.getDisplay());
        create();
    }

    /**
     * @see com.docfacto.core.hovers.DualAbstractInformationControl#setInput(java.lang.Object)
     */
    public void setInput(Object input) {
        TagletInput tagletInput = (TagletInput)input;
        theTagParser = tagletInput.getParser();

        // Generate the contents for the browser
        theContent = new StringBuffer(600);

        HTMLPrinter.insertPageProlog(theContent,0,
            TagletStyleSheetLoader.getStyleSheet());

        TagElement tag = (TagElement)theTagParser.getAttachment();

        if (theTagParser.isInLine()) {
            theTagletProcessor.handleInline(tag,theTagParser,theContent);
        }
        else {
            theTagletProcessor.handle(tag,theContent);
        }

        HTMLPrinter.addPageEpilog(theContent);

        getBrowser().setText(theContent.toString());
        getBrowser().setVisible(true);

    }

    /**
     * @see org.eclipse.jface.text.IInformationControlExtension#hasContents()
     */
    @Override
    public boolean hasContents() {
        return true;
    }

    /**
     * @see com.docfacto.core.hovers.DualAbstractInformationControl#getToolBarActions()
     */
    @Override
    public List<Action> getToolBarActions() {
        return null;
    }

    /**
     * @see com.docfacto.core.hovers.DualAbstractInformationControl#computeSizeHint()
     */
    @Override
    public Point computeSizeHint() {
        if ("docfacto.example".equals(theTagParser.getTagName())) {
            // Lets give a bigger size here ..
            return new Point(400,400);
        }

        TextPresentation presentation = new TextPresentation();
        HTML2TextReader reader =
            new HTML2TextReader(new StringReader(theContent.toString()),
                presentation);
        String text;
        try {
            text = reader.getString();
        }
        catch (IOException e) {
            text = ""; //$NON-NLS-1$
        }

        theTextLayout.setText(text);

        // Iterator iter = presentation.getAllStyleRangeIterator();
        // while (iter.hasNext()) {
        // StyleRange sr = (StyleRange) iter.next();
        // if (sr.fontStyle == SWT.BOLD)
        // fTextLayout.setStyle(fBoldStyle, sr.start, sr.start + sr.length - 1);
        // }
        Rectangle bounds = theTextLayout.getBounds();
        int width = bounds.width;
        int height = bounds.height;

        // Add padding
        width += 15;
        height += 25;

        width = Math.max(MIN_WIDTH,width);
        height = Math.max(MIN_HEIGHT,height);

        return new Point(width,height);
    }
}
