package com.docfacto.taglets.plugin.hover;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.dom.Comment;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;

import com.docfacto.core.hovers.TagletHover;
import com.docfacto.core.hovers.TagletInput;
import com.docfacto.core.utils.PluginUtils;
import com.docfacto.javadoc.TagParser;

/**
 * Listens to Hover activation and marshals the request to
 * 
 * @author dhudson - created 20 Sep 2013
 * @since 2.4
 */
public class TagletsHoverMarshaller extends TagletHover {

    private TagParser theTagParser;

    // Note that links is handled by links and not this plugin
    private static final String[] HANDLING_TAGLETS = {"docfacto.media",
        "docfacto.system","docfacto.note","docfacto.todo","docfacto.example"};

    /**
     * @see com.docfacto.core.hovers.TagletHover#getHoverInfo(org.eclipse.jface.text.ITextViewer,
     * org.eclipse.jface.text.IRegion, com.docfacto.javadoc.TagParser)
     */
    @Override
    public Object getHoverInfo(ITextViewer viewer,IRegion region,
    TagParser parser) {

        String tagName = parser.getTagName();
        boolean found = false;

        for (String tagletName:HANDLING_TAGLETS) {
            if (tagName.equals(tagletName)) {
                found = true;
                break;
            }
        }

        if (!found) {
            // Its not for me ..
            return null;
        }

        if ("docfacto.example".equals(tagName)) {
            if (!resetExampleURI(parser)) {
                // This didn't work so remove the URI..
                parser.removeAttribute(TagParser.URI_ATTRIBUTE);
            }
        }
        else {
            // Process the URI attribute, using the resource as the base
            // directory
            if (parser.hasAttribute(TagParser.URI_ATTRIBUTE)) {
                IResource resource = getResource();

                IPath path = resource.getLocation();

                if (path.segmentCount()>2) {
                    path = path.removeLastSegments(1);
                }
                path =
                    path.append(parser
                        .getAttributeValue(TagParser.URI_ATTRIBUTE));

                // Set the whole path
                parser.setAttribute(TagParser.URI_ATTRIBUTE,
                    path.toString());
            }
        }
        theTagParser = parser;

        return new TagletInput(parser);
    }

    /**
     * @see com.docfacto.core.hovers.AbstractTextHover#getHoverControlCreator()
     */
    @Override
    public IInformationControlCreator getHoverControlCreator() {
        return new TagletControlCreatorMarshaller(theTagParser);
    }

    /**
     * Look up the class name and create a file URI so that the taglet engine
     * can load it.
     * 
     * @param parser to process
     * @since 2.2
     */
    private boolean resetExampleURI(TagParser parser) {
        String resourceName = null;

        // If the attribute is set, lets take it, otherwise just use the
        // text
        if (parser.hasAttribute(TagParser.URI_ATTRIBUTE)) {
            resourceName =
                parser.getAttributeValue(TagParser.URI_ATTRIBUTE);
        }
        else {
            // Nothing to do as there is no uri attribute
            return false;
        }

        IJavaElement javaElement = PluginUtils.locateJavaElement(resourceName);

        if (javaElement!=null&&javaElement.getResource()!=null) {

            URL url;
            try {
                url = javaElement.getResource()
                    .getLocationURI()
                    .toURL();

                // Right, lets set the new URI to be the
                // location of the file on disk
                parser.setAttribute(TagParser.URI_ATTRIBUTE,
                    url.toString());

                // We have finished ..
                return true;
            }
            catch (MalformedURLException e) {
                return false;
            }
        }

        return false;
    }

    /**
     * Only interested in Javadoc comments
     * 
     * @see com.docfacto.core.hovers.TagletHover#requiresComments()
     */
    @Override
    public boolean requiresComments() {
        return false;
    }

    /**
     * @see com.docfacto.core.hovers.TagletHover#getHoverInfo(org.eclipse.jface.text.ITextViewer,
     * org.eclipse.jface.text.IRegion, org.eclipse.jdt.core.dom.Comment)
     */
    @Override
    public Object getHoverInfo(ITextViewer viewer,IRegion region,Comment node) {
        return null;
    }
}
