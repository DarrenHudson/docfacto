package com.docfacto.taglets.plugin.hover;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

import org.eclipse.core.runtime.Path;
import org.eclipse.jface.action.Action;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import com.docfacto.beermat.ui.views.CanvasView;
import com.docfacto.beermat.ui.views.LoadCompleteListener;
import com.docfacto.common.DocfactoException;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.core.IDocfactoCorePlugin;
import com.docfacto.core.hovers.DualAbstractInformationControl;
import com.docfacto.core.hovers.LaunchAction;
import com.docfacto.core.hovers.TagletInput;
import com.docfacto.javadoc.TagParser;
import com.docfacto.taglets.plugin.DocfactoTagletsPlugin;

/**
 * An information control which will display SVG files
 * 
 * @author dhudson - created 20 Sep 2013
 * @since 2.4
 */
public class SVGMediaInformationControl extends DualAbstractInformationControl
implements LoadCompleteListener {

    private TagParser theTagParser;
    private CanvasView theCanvasView;
    private LaunchAction theLaunchAction;

    private Composite theParent;

    /**
     * Constructor used to provide the hover, with the F2 to select text
     * 
     * @param parent shell
     * @since 2.4
     */
    public SVGMediaInformationControl(Shell parent) {
        super(parent);

        theLaunchAction =
            new LaunchAction(
                DocfactoCorePlugin
                    .getImageDescriptor(IDocfactoCorePlugin.BEERMAT_ICON_16),
                "Open in Beermat",this);

        // Create will call getToolbarActions
        create();
    }

    /**
     * @see org.eclipse.jface.text.IInformationControlExtension#hasContents()
     */
    @Override
    public boolean hasContents() {
        if (theTagParser==null) {
            return false;
        }
        return true;
    }

    /**
     * @see org.eclipse.jface.text.AbstractInformationControl#createContent(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void createContent(Composite parent) {
        theParent = parent;
    }

    /**
     * {@docfacto.system The view is created here and not create content, as it seems to hang less. }
     * 
     * @see org.eclipse.jface.text.IInformationControlExtension2#setInput(java.lang.Object)
     */
    @Override
    public void setInput(Object input) {

        TagletInput tagletInput = (TagletInput)input;
        theTagParser = tagletInput.getParser();

        theLaunchAction.setFileToLaunch(theTagParser
            .getAttributeValue(TagParser.URI_ATTRIBUTE));

        // Lets load the SVG
        try {
            theCanvasView = new CanvasView(theParent);
            theCanvasView.setEventLoadCompleteListener(this);
            theCanvasView.setSVG(new Path(theTagParser
                .getAttributeValue(TagParser.URI_ATTRIBUTE)));
        }
        catch (DocfactoException ex) {
            DocfactoTagletsPlugin
                .logException("Unable to load beermat view",ex);
        }
    }

    /**
     * @see com.docfacto.beermat.ui.views.LoadCompleteListener#loadComplete(java.util.EventObject)
     */
    @Override
    public void loadComplete(EventObject event) {
        // Do so something with the size here
    }

    /**
     * @see com.docfacto.core.hovers.DualAbstractInformationControl#getToolBarActions()
     */
    public List<Action> getToolBarActions() {
        List<Action> actions = new ArrayList<Action>(1);
        actions.add(theLaunchAction);

        return actions;
    }

}
