package com.docfacto.taglets.plugin.hover;

import org.eclipse.jface.text.AbstractReusableInformationControlCreator;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.swt.widgets.Shell;

import com.docfacto.javadoc.TagParser;
import com.docfacto.taglets.MediaTagletHelper;

/**
 * Depending on the tag name and type in the parser will depend on what control
 * gets created
 * 
 * @author dhudson - created 20 Sep 2013
 * @since 2.4
 */
public class TagletControlCreatorMarshaller extends
AbstractReusableInformationControlCreator {

    private final TagParser theParser;

    /**
     * Constructor.
     * 
     * @param parser the taglet parser
     * @since 2.4
     */
    public TagletControlCreatorMarshaller(TagParser parser) {
        theParser = parser;
    }

    /**
     * @see org.eclipse.jface.text.AbstractReusableInformationControlCreator#doCreateInformationControl(org.eclipse.swt.widgets.Shell)
     */
    @Override
    protected IInformationControl doCreateInformationControl(Shell parent) {

        String tagName = theParser.getTagName();

        if ("docfacto.media".equals(tagName)) {
            if (MediaTagletHelper.isSVG(theParser)) {
                return new SVGMediaInformationControl(parent);
            }
            // I can see this in the future being a separate control
            else if (MediaTagletHelper.isMP4(theParser)) {
                // Its an image..
                return new BrowserInformationControl(parent);
            }
            
            // Its an image
            return new ImageMediaInformationControl(parent);
        }

        // All else its the default Browser Control, including video
        return new BrowserInformationControl(parent);
    }

    /**
     * @see org.eclipse.jface.text.AbstractReusableInformationControlCreator#canReplace(org.eclipse.jface.text.IInformationControlCreator)
     */
    @Override
    public boolean canReplace(IInformationControlCreator creator) {
        return false;
    }

    /**
     * @see org.eclipse.jface.text.AbstractReusableInformationControlCreator#canReuse(org.eclipse.jface.text.IInformationControl)
     */
    @Override
    public boolean canReuse(IInformationControl control) {
        // if (!super.canReuse(control))
        // return false;
        //
        // // Set the text back again
        // if (control instanceof IInformationControlExtension4) {
        // ((IInformationControlExtension4)control)
        // .setStatusText(EditorsUI
        // .getTooltipAffordanceString());
        // }
        //
        // return true;
        return false;
    }

}
