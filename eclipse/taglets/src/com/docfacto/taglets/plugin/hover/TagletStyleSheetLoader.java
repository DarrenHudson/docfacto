package com.docfacto.taglets.plugin.hover;

import java.io.IOException;

import org.eclipse.jdt.internal.ui.JavaPlugin;
import org.eclipse.jdt.ui.PreferenceConstants;
import org.eclipse.jface.internal.text.html.HTMLPrinter;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.graphics.FontData;

import com.docfacto.common.DocfactoException;
import com.docfacto.core.utils.PluginUtils;
import com.docfacto.taglets.plugin.DocfactoTagletsPlugin;

public class TagletStyleSheetLoader {

    private static String theStyleSheet;

    private TagletStyleSheetLoader() {

    }

    public static String getStyleSheet() {
        if (theStyleSheet==null) {
            theStyleSheet = getStyleSheetImpl();
        }

        return theStyleSheet;
    }

    private static String getStyleSheetImpl() {

        StringBuilder merged = new StringBuilder(3000);
        try {
            String cssContents = PluginUtils.loadResource(
                DocfactoTagletsPlugin.PLUGIN_ID,"/resources/taglet.css");

            String path = "";
            try {
                path = "file:"
                    +PluginUtils.getFileLocationOf(
                        DocfactoTagletsPlugin.PLUGIN_ID,"/resources");

                if (path.endsWith("/")) {
                    path = path.substring(0,path.length()-1);
                }

            }
            catch (IOException ex) {
                DocfactoTagletsPlugin.logException(
                    "Unable to load icons for css",ex);
            }

            String newContents =
                cssContents.replaceAll("/resources/icons.png",
                    path+"/icons.png").replaceAll(
                    "/resources/icons-white.png",
                    path+"/icons-white.png");

            merged.append(PluginUtils.loadResource(
                JavaPlugin.getPluginId(),
                "/JavadocHoverStyleSheet.css"));
            merged.append(newContents);

            FontData fontData =
                JFaceResources.getFontRegistry().getFontData(
                    PreferenceConstants.APPEARANCE_JAVADOC_FONT)[0];
            return HTMLPrinter.convertTopLevelFont(merged.toString(),fontData);

        }
        catch (DocfactoException ex) {
            DocfactoTagletsPlugin.logException(
                "Unable to load css for taglets",ex);
            return "";
        }
    }
}
