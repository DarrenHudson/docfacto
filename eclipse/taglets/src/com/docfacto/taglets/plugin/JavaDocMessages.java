package com.docfacto.taglets.plugin;

import org.eclipse.osgi.util.NLS;

/**
 * Helper class to get NLSed messages.
 */
final class JavaDocMessages extends NLS {

    private static final String BUNDLE_NAME= JavaDocMessages.class.getName();

    private JavaDocMessages() {
        // Do not instantiate
    }

    public static String JavaDoc2HTMLTextReader_parameters_section;
    public static String JavaDoc2HTMLTextReader_returns_section;
    public static String JavaDoc2HTMLTextReader_throws_section;
    public static String JavaDoc2HTMLTextReader_author_section;
    public static String JavaDoc2HTMLTextReader_deprecated_section;
    public static String JavaDoc2HTMLTextReader_method_in_type;
    public static String JavaDoc2HTMLTextReader_overrides_section;
    public static String JavaDoc2HTMLTextReader_see_section;
    public static String JavaDoc2HTMLTextReader_since_section;
    public static String JavaDoc2HTMLTextReader_specified_by_section;
    public static String JavaDoc2HTMLTextReader_version_section;

    static {
        NLS.initializeMessages(BUNDLE_NAME, JavaDocMessages.class);
    }
}