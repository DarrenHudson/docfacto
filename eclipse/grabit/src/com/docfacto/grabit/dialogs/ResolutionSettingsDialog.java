package com.docfacto.grabit.dialogs;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.layout.GridData;

import com.docfacto.grabit.DocfactoGrabitPlugin;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

/**
 * A Dialog for setting image resolution
 * 
 * @author pemi 
 * @since 2.5
 *
 */
public class ResolutionSettingsDialog extends Dialog {

	private String[] resolutionTypes = new String[] { "pixels/inch",
			"pixels/cm" };
	private int resValue;
	private String resType;
	private IEclipsePreferences prefs;
	private Spinner spinner;
	private Combo combo;
	int pxval;

	/**
	 * Create the  resolution dialog.
	 * 
	 * @param parentShell
	 * @since 2.5
	 */
	public ResolutionSettingsDialog(Shell parentShell) {
		super(parentShell);
		prefs = InstanceScope.INSTANCE.getNode(DocfactoGrabitPlugin.PLUGIN_ID);
		resType = prefs.get(DocfactoGrabitPlugin.STORE_PROP_IMAGE_RES,
				resolutionTypes[0]);
	}
	
    protected void configureShell(Shell shell) {
        super.configureShell(shell);
        shell.setText("Image Settings");
    }

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 * @since 2.5
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout gridLayout = (GridLayout) container.getLayout();
		gridLayout.numColumns = 3;

		Label lblImafeResolution = new Label(container, SWT.NONE);
		lblImafeResolution.setText("Resolution:");

		spinner = new Spinner(container, SWT.BORDER);
		spinner.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (combo.getText().equals("pixels/inch")) {
					setResValue(spinner.getSelection());
				} else {

					setResValue((int) (spinner.getSelection()
							/ Math.pow(10, spinner.getDigits()) * 2.54));

				}

			}
		});
		spinner.setMaximum(50000);
		spinner.setSelection(getResValue());

		combo = new Combo(container, SWT.READ_ONLY);
		combo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateValues();

			}
		});
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
				1));
		combo.setItems(resolutionTypes);
		combo.select(combo.indexOf(getResType()));
		updateValues();

		return container;
	}

	private void updateValues() {
		// TODO Auto-generated method stub
		if (combo.getText().equals("pixels/inch")) {
			spinner.setSelection(getResValue());
			spinner.setDigits(0);
		} else {
			spinner.setSelection(getPixPerCM());
			spinner.setDigits(1);

		}

	}

	/**
	 * Create contents of the button bar.
	 * @param parent
	 * @since 2.5
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(300, 150);
	}

	/**
	 * @return
	 */
	public int getResValue() {
		return resValue;
	}

	/**
	 * @return
	 */
	public int getPixPerCM() {
		return (int) (getResValue() / 0.254);
	}

	/**
	 * @param resValue
	 */
	public void setResValue(int resValue) {
		this.resValue = resValue;
	}

	/**
	 * @return
	 */
	public String getResType() {
		return resType;
	}

	/**
	 * @param resType
	 */
	public void setResType(String resType) {
		this.resType = resType;
	}

}
