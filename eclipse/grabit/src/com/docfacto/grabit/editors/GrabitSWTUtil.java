package com.docfacto.grabit.editors;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DirectColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;

import com.docfacto.grabit.DocfactoGrabitPlugin;

/**
 * UTIL class for SWT
 * 
 * @author pemi - created Sep 11, 2013
 * @since 2.4
 */
public class GrabitSWTUtil {

    /**
     * A constant used by property change listener to store the state of the
     * canvas as dirty
     */
    public static final int EDITOR_IS_DIRTY = 1;

    /**
     * A constant used by property change listener to store the state of the
     * canvas as not dirty
     */
    public static final int EDITOR_IS_NOT_DIRTY = 0;

    /**
     * A constant used by property change listener to store the state of the
     * canvas as dirty. This is different from {@code #EDITOR_IS_DIRTY}. It does
     * not create an undoable operation
     */
    public static final int EDITOR_DIRTY_NO_UPDATE = 2;
    
    /**
     * A constant used by property change listener to store the state of the
     * canvas as newly loaded.
     */
    public static final int EDITOR_NEWLY_LOADED = 3;

    /**
     * Given a file extension as {@code String}, this method gets the 2nd
     * parameter for use in {@code ImageLoader.save(parameter1, parameter2)}
     * 
     * @see org.eclipse.swt.graphics.ImageLoader#save(String , int)
     * @param extension - file extension
     * @return int value for file extension (if none available for specified
     * extension parameter; method would return int value of SWT.IMAGE_PNG)
     * 
     * @since 2.4
     */
    public static int getIntValueOfFileExtension(String extension) {

        if (extension.equals("png"))
            return SWT.IMAGE_PNG;

        if (extension.equals("bmp"))
            return SWT.IMAGE_BMP;

        if (extension.equals("ico"))
            return SWT.IMAGE_ICO;

        if (extension.equals("jpeg")||
            extension.equals("jpg"))
            return SWT.IMAGE_JPEG;

        if (extension.equals("gif"))
            return SWT.IMAGE_GIF;

        if (extension.equals("tiff"))
            return SWT.IMAGE_TIFF;

        // else
        return SWT.IMAGE_PNG;

    }

    /**
     * Given an arbitrary rectangle, get the rectangle with the given transform.
     * The result rectangle is positive width and positive height.
     * 
     * @param af AffineTransform
     * @param src source rectangle
     * @return rectangle after transform with positive width and height
     * @since 2.4
     */
    public static Rectangle transformRect(AffineTransform af,Rectangle src) {
        Rectangle dest = new Rectangle(0,0,0,0);
        src = absRect(src);
        Point p1 = new Point(src.x,src.y);
        p1 = transformPoint(af,p1);
        dest.x = p1.x;
        dest.y = p1.y;
        dest.width = (int)(src.width*af.getScaleX());
        dest.height = (int)(src.height*af.getScaleY());
        return dest;
    }

    /**
     * Given an arbitrary rectangle, get the rectangle with the inverse given
     * transform. The result rectangle is positive width and positive height.
     * 
     * @param af AffineTransform
     * @param src source rectangle
     * @return rectangle after transform with positive width and height
     * @since 2.4
     */
    public static Rectangle inverseTransformRect(AffineTransform af,
    Rectangle src) {
        Rectangle dest = new Rectangle(0,0,0,0);
        src = absRect(src);
        Point p1 = new Point(src.x,src.y);
        p1 = inverseTransformPoint(af,p1);
        dest.x = p1.x;
        dest.y = p1.y;
        dest.width = (int)(src.width/af.getScaleX());
        dest.height = (int)(src.height/af.getScaleY());
        return dest;
    }

    /**
     * Given an arbitrary point, get the point with the given transform.
     * 
     * @param af affine transform
     * @param pt point to be transformed
     * @return point after tranform
     * @since 2.4
     */
    public static Point transformPoint(AffineTransform af,Point pt) {
        Point2D src = new Point2D.Float(pt.x,pt.y);
        Point2D dest = af.transform(src,null);
        Point point =
            new Point((int)Math.floor(dest.getX()),(int)Math.floor(dest.getY()));
        return point;
    }

    /**
     * Given an arbitrary point, get the point with the inverse given transform.
     * 
     * @param af AffineTransform
     * @param pt source point
     * @return point after transform
     * @since 2.4
     */
    public static Point inverseTransformPoint(AffineTransform af,Point pt) {
        Point2D src = new Point2D.Float(pt.x,pt.y);
        try {
            Point2D dest = af.inverseTransform(src,null);
            return new Point((int)Math.floor(dest.getX()),(int)Math.floor(dest
                .getY()));
        }
        catch (Exception e) {
            e.printStackTrace();
            return new Point(0,0);
        }
    }

    /**
     * Given arbitrary rectangle, return a rectangle with upper-left start and
     * positive width and height.
     * 
     * @param src source rectangle
     * @return result rectangle with positive width and height
     * @since 2.4
     */
    public static Rectangle absRect(Rectangle src) {
        Rectangle dest = new Rectangle(0,0,0,0);
        if (src.width<0) {
            dest.x = src.x+src.width+1;
            dest.width = -src.width;
        }
        else {
            dest.x = src.x;
            dest.width = src.width;
        }
        if (src.height<0) {
            dest.y = src.y+src.height+1;
            dest.height = -src.height;
        }
        else {
            dest.y = src.y;
            dest.height = src.height;
        }
        return dest;
    }

    /**
     * Method gets {@code InputStream} from a provided {@code BufferedImage}
     * 
     * @param image {@code BufferedImage} to get input stream from
     * @return {@code InputStream} of image
     * @since 2.4
     */
    public static InputStream getImageInputStream(BufferedImage image) {

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            ImageIO.write(image,"png",os);
        }
        catch (IOException e) {
            DocfactoGrabitPlugin.logException(
                "Grabit: unable to get input stream from image"
                    +e.getMessage(),e);
        }
        InputStream is = new ByteArrayInputStream(os.toByteArray());
        return is;

    }

	public static BufferedImage convertToAWT(ImageData data) {
        
        ImageData imageData = data;
        int width = imageData.width;
        int height = imageData.height;
        ImageData maskData = null;
        int alpha[] = new int[1];
        
        if (imageData.alphaData == null)
            maskData = imageData.getTransparencyMask();
        
        // now we should have the image data for the bitmap, decompressed in imageData[0].data.
        // Convert that to a Buffered Image.
        BufferedImage image = new BufferedImage( imageData.width, imageData.height, BufferedImage.TYPE_INT_ARGB );
        
        WritableRaster alphaRaster = image.getAlphaRaster();

        // loop over the imagedata and set each pixel in the BufferedImage to the appropriate color.
        for( int y = 0; y < height; y++ )
        {
            for( int x = 0; x < width; x++ )
            {
                RGB color = imageData.palette.getRGB(imageData.getPixel(x, y));
                image.setRGB( x, y, new java.awt.Color(color.red, color.green, color.blue).getRGB());
        
                // check for alpha channel
                if (alphaRaster != null) {
                    
                    if( imageData.alphaData != null) {
                        alpha[0] = imageData.getAlpha( x, y );
                        alphaRaster.setPixel( x, y, alpha );
                    }
                    else {
                        // check for transparency mask
                        if( maskData != null) {
                            alpha[0] = maskData.getPixel( x, y ) == 0 ? 0 : 255;
                            alphaRaster.setPixel( x, y, alpha );
                        }
                    }
                }
            }
        }

        return image;
    }
	
	   public static Image convert( BufferedImage srcImage) {
	       
	       PaletteData PALETTE_DATA = new PaletteData(0xFF0000, 0xFF00, 0xFF);
	       
	        // We can force bitdepth to be 24 bit because BufferedImage getRGB allows us to always
	        // retrieve 24 bit data regardless of source color depth.
	        ImageData swtImageData =
	            new ImageData(srcImage.getWidth(), srcImage.getHeight(), 24, PALETTE_DATA);

	        // ensure scansize is aligned on 32 bit.
	        int scansize = (((srcImage.getWidth() * 3) + 3) * 4) / 4;
	        
	        WritableRaster alphaRaster = srcImage.getAlphaRaster();
	        byte[] alphaBytes = new byte[srcImage.getWidth()];
	            
	        for (int y=0; y<srcImage.getHeight(); y++) {
	            int[] buff = srcImage.getRGB(0, y, srcImage.getWidth(), 1, null, 0, scansize);
	            swtImageData.setPixels(0, y, srcImage.getWidth(), buff, 0);
	            
	            // check for alpha channel
	            if (alphaRaster != null) {
	                int[] alpha = alphaRaster.getPixels(0, y, srcImage.getWidth(), 1, (int[])null);
	                for (int i=0; i<srcImage.getWidth(); i++)
	                    alphaBytes[i] = (byte)alpha[i];
	                swtImageData.setAlphas(0, y, srcImage.getWidth(), alphaBytes, 0);
	            }
	        }
	    
	        return new Image(Display.getCurrent(), swtImageData);
	    }
	
	public static BufferedImage convertRGBAToIndexed(BufferedImage src) {
	    BufferedImage dest = new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_BYTE_INDEXED);
	    Graphics g = dest.getGraphics();
	    g.setColor(new Color(231,20,189));//fill with an arbitrary color and make it transparent
	    g.fillRect(0, 0, dest.getWidth(), dest.getHeight()); 
	    dest = makeTransparent(dest,0,0);
	    dest.createGraphics().drawImage(src,0,0, null);
	    return dest;
	    }

	    public static BufferedImage makeTransparent(BufferedImage image, int x, int y) {
	    ColorModel cm = image.getColorModel();
	    if (!(cm instanceof IndexColorModel))
	    return image; //sorry...
	    IndexColorModel icm = (IndexColorModel) cm;
	    WritableRaster raster = image.getRaster();
	    int pixel = raster.getSample(x, y, 0); //pixel is offset in ICM's palette
	    int size = icm.getMapSize();
	    byte[] reds = new byte[size];
	    byte[] greens = new byte[size];
	    byte[] blues = new byte[size];
	    icm.getReds(reds);
	    icm.getGreens(greens);
	    icm.getBlues(blues);
	    IndexColorModel icm2 = new IndexColorModel(8, size, reds, greens, blues, pixel);
	    return new BufferedImage(icm2, raster, image.isAlphaPremultiplied(), null);
	    }
}


