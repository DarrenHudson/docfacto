package com.docfacto.grabit.editors;

import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.Stack;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Path;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Text;

import com.docfacto.core.utils.SWTUtils;
import com.docfacto.grabit.DocfactoGrabitPlugin;
import com.docfacto.grabit.editors.tools.BorderCreator;
import com.docfacto.grabit.plugin.panels.GrabitBorder;
import com.docfacto.grabit.plugin.panels.ResizeableSelector;

/**
 * The SWT canvas used by Grabit Image editor for 
 * drawing arbitrary graphics. 
 * The canvas has listeners added to detect mouse and key events.
 * 
 * @author pemi - created Sep 4, 2013
 * @since 2.4.8
 */
public class SWTImageCanvas extends Canvas {

    /**
     * Named value of property change listener
     */
    protected static final String IS_DIRTY = "com.docfact.grabit.isDirty";

    /**
     * boolean value indicating 'mouse down' state
     */
    protected boolean mouseDown;

    final float ZOOMIN_RATE = 1.1f; /* zoomin rate */
    final float ZOOMOUT_RATE = 0.9f; /* zoomout rate */

    private AffineTransform transform = new AffineTransform();
    private Cursor pencilCursor,zoomCursor,zoomOutCursor,pointerCursor,cropCursor;

    private Image sourceImage; /* original image */
    private Image screenImage; /* screen image */
    private Image drawingPane;

    private int originalPos = 0;
    private int editMode = 0;

    private PropertyChangeSupport support;

    private Stack<ImageData> undoStack;
    private Stack<ImageData> redoStack;

    private RGB strokeForegroundColour = new RGB(0,0,0);
    private RGB strokeBackgroundColour = new RGB(255,255,255);
    private Point pointHolder1;
    /** a point relative to the source image bounds */
    private Point mouseDownPoint;
    private Point textPendingPoint;

    private Path path;
    
    private int lineWidth, lineStyle,xFactor, yFactor,fontSize;
    
    private RGB textForegroundColour, textBackgroundColour;
    private String fontType;

    private boolean textUnderline;
    private boolean textBold;
    private boolean textItalic;
    private Text text;

    private Rectangle cropRect;
    private GrabitBorder dragAction;
    private Image screenImage2;


    /**
     * Constructor takes an image file and paints it onto this canvas.
     * 
     * @param parent - the parent composite
     * @param file - file of the main image
     * @since 2.4.8
     */
    public SWTImageCanvas(final Composite parent,File file) {
        this(parent,SWT.NULL);
        sourceImage = getImage(file);
        initCanvas(parent);

    }

    /**
     * Constructor takes an {@code Image} and paints it onto this canvas
     * 
     * @param parent - The parent composite
     * @param canvasImage - The image to paint on canvas
     * @since 2.4.8
     */
    public SWTImageCanvas(Composite parent,Image canvasImage) {
        this(parent,SWT.NULL);
        sourceImage = canvasImage;
        initCanvas(parent);

    }

    /**
     * Constructor for ScrollableCanvas.
     * 
     * @param parent the parent of this control.
     * @param style the style of this control.
     * @since 2.4.8
     */
    public SWTImageCanvas(final Composite parent,int style) {
        super(parent,style|SWT.BORDER|SWT.V_SCROLL|SWT.H_SCROLL
            |SWT.NO_BACKGROUND);
        text = new Text(this,SWT.NONE);
        text.addKeyListener(new KeyAdapter() {

            /**
             * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
             */
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.keyCode==SWT.CR) {
                    writeTextToCanvas(text.getText());
                }

            }
        });
        text.setVisible(false);

        addKeyListener(new KeyAdapter() {

            /**
             * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
             */
            @Override
            public void keyPressed(KeyEvent e) {
                // Action for cropping images
                if (e.keyCode==SWT.CR) {

                    if (getEditMode()==DocfactoGrabitPlugin.IMAGE_CROP_MODE) {
                        if (cropRect!=null&&
                            (cropRect.width>2||cropRect.height>2)) {
                            cropImage(cropRect);
                            if (drawingPane!=null&&!drawingPane.isDisposed()) {
                                drawingPane.dispose();
                                drawingPane = null;
                            }
                        }
                    }
                    cropRect = null;
                    refresh();
                }

                if (e.keyCode==SWT.ESC) {
                    if (getEditMode()==DocfactoGrabitPlugin.IMAGE_CROP_MODE) {
                        if (drawingPane!=null&&!drawingPane.isDisposed()) {
                            drawingPane.dispose();
                            drawingPane = null;
                            cropRect = null;
                            refresh();
                        }
                    }
                }

                if (e.character=='='
                    &&(e.stateMask==SWT.CTRL||e.stateMask==SWT.COMMAND)) {
                    zoomIn();
                }

                if (e.character=='-'
                    &&(e.stateMask==SWT.CTRL||e.stateMask==SWT.COMMAND)) {
                    zoomOut();
                }

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }

        });
        addControlListener(new ControlAdapter() { /* resize listener. */
            @Override
            public void controlResized(ControlEvent event) {
                syncScrollBars();
            }
        });
        addPaintListener(new PaintListener() { /* paint listener. */
            @Override
            public void paintControl(final PaintEvent event) {
                paint(event.gc);
            }
        });

        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseDoubleClick(MouseEvent e) {
                // not used
            }

            @Override
            public void mouseDown(MouseEvent e) {
                mouseDown = true;
                mouseDownPoint = getClientAreaPoint(e.x,e.y);
                pointHolder1 = new Point(e.x,e.y);
                
                switch (getEditMode()) {

                case DocfactoGrabitPlugin.PENCIL_MODE:

                    if (path!=null) {
                        path.dispose();

                    }
                    path = new Path(getSourceImage().getDevice());
                    path.moveTo(mouseDownPoint.x,mouseDownPoint.y);
                    break;


                case DocfactoGrabitPlugin.IMAGE_CROP_MODE:
                    
                    if (cropRect != null) {
                        //check if mouse down on cut rect
                       Point relPoint =  getClientAreaPoint(e.x,e.y);
                        if (cropRect.contains(relPoint)) {
                            
                            xFactor = relPoint.x - cropRect.x;
                            yFactor = relPoint.y - cropRect.y;
                        }
                    }

                    break;

                case DocfactoGrabitPlugin.TEXT_MODE:
                    if (text.getText().length()<1) {
                        textPendingPoint = mouseDownPoint;

                        FontData[] fD = getCanvasFont().getFontData();
                        fD[0].setHeight(fontSize);

                        if(getTextForegroundColour()!=null){
                            text.setForeground(new Color(getDisplay(),
                                getTextForegroundColour()));
                        }

                        text.setFont(new Font(getDisplay(),fD[0]));
                        text.setBounds(pointHolder1.x,
                            pointHolder1.y-(text.getLineHeight()/2),
                            20*fontSize,text.getLineHeight());
                        text.setVisible(true);
                        text.setFocus();

                    
                    }else{

                        writeTextToCanvas(text.getText());

                    }

                    break;

                }

            }

            @Override
            public void mouseUp(MouseEvent e) {
                

                // get rid of the drawing pane for all modes (except crop mode)
                if (getEditMode()!=DocfactoGrabitPlugin.IMAGE_CROP_MODE) {
                    if (drawingPane!=null&&!drawingPane.isDisposed()) {
                        drawingPane.dispose();
                    }
                    drawingPane = null;
                }


                switch (getEditMode()) {

                case DocfactoGrabitPlugin.PENCIL_MODE:
                    if (getStrokeForegroundColour()!=null) {
                        GC gc = new GC(getSourceImage());
                        gc.setForeground(getStrokeForegroundColour());
                        Point gcPoint = getClientAreaPoint(e.x,e.y);
                        if (path!=null&&!path.isDisposed()) {
                            gc.setLineWidth(getLineWidth());
                            gc.setLineStyle(getLineStyle());
                            gc.drawPath(path);
                            path.dispose();

                        }

                        // paint a point if no mouse move
                        if (mouseDownPoint.x-gcPoint.x==0
                        &&mouseDownPoint.y-gcPoint.y==0) {

                            int lw = getLineWidth();
                            if (lw>1) {
                                gc.setBackground(getStrokeForegroundColour());
                                gc.fillOval(gcPoint.x-lw/2,gcPoint.y-lw/2,lw,lw);

                            }
                            else {

                                gc.drawPoint(gcPoint.x,gcPoint.y);
                            }

                        }
                        gc.dispose();
                        // image has changed
                        setDirty();
                        redraw();
                    }
                    break;


                case DocfactoGrabitPlugin.LINE_MODE:

                    if (pointHolder1!=null) {
                        drawShape(pointHolder1.x,pointHolder1.y,e.x,e.y);

                    }

                    break;

                case DocfactoGrabitPlugin.ZOOM_MODE:
                    zoomIn(e.x,e.y);
                    break;

                case DocfactoGrabitPlugin.ZOOM_OUT_MODE:
                    zoomOut();
                    break;

                case DocfactoGrabitPlugin.IMAGE_CROP_MODE:
                    setFocus();
                    dragAction = null;
                    if(cropRect!=null){
                        updateCropRect();
                        
                    }
                    break;

                case DocfactoGrabitPlugin.ERASE_MODE:
                    break;

                case DocfactoGrabitPlugin.CIRCLE_MODE:
                    drawShape(pointHolder1.x,pointHolder1.y,e.x,e.y);
                    break;

                case DocfactoGrabitPlugin.SQUARE_MODE:
                    drawShape(pointHolder1.x,pointHolder1.y,e.x,e.y);
                    break;

                case DocfactoGrabitPlugin.RECTANGLE_MODE:
                    drawShape(pointHolder1.x,pointHolder1.y,e.x,e.y);
                    break;

                case DocfactoGrabitPlugin.OVAL_MODE:
                    drawShape(pointHolder1.x,pointHolder1.y,e.x,e.y);
                    break;

                }
                

                mouseDown = false;

            }

        });

        addMouseTrackListener(new MouseTrackAdapter() {

            private int oldEditMode;

            /**
             * @see org.eclipse.swt.events.MouseTrackAdapter#mouseEnter(org.eclipse.swt.events.MouseEvent)
             */
            @Override
            public void mouseEnter(MouseEvent e) {
                if(oldEditMode != getEditMode()){
                    // mode has changed
                    if (drawingPane!=null&&!drawingPane.isDisposed()) {
                        drawingPane.dispose();
                        drawingPane = null;
                        cropRect = null;
                        
                    }
                    redraw();
                }
                switch (getEditMode()) {
                case DocfactoGrabitPlugin.POINTER_MODE:
                    setCursor(pointerCursor);
                    break;

                case DocfactoGrabitPlugin.PENCIL_MODE:
                    setCursor(pencilCursor);
                    break;

                case DocfactoGrabitPlugin.LINE_MODE:
                    setCursor(new Cursor(getDisplay(),SWT.CURSOR_CROSS));
                    break;

                case DocfactoGrabitPlugin.ZOOM_MODE:
                    setCursor(zoomCursor);
                    break;

                case DocfactoGrabitPlugin.ZOOM_OUT_MODE:
                    setCursor(zoomOutCursor);
                    break;

                case DocfactoGrabitPlugin.IMAGE_CROP_MODE:
                    setCursor(cropCursor);
                    break;

                case DocfactoGrabitPlugin.TEXT_MODE:
                    setCursor(new Cursor(getDisplay(),SWT.CURSOR_IBEAM));
                    break;

                case DocfactoGrabitPlugin.SHAPE_MODE:
                    setCursor(new Cursor(getDisplay(),SWT.CURSOR_CROSS));
                    break;

                default:
                    if (getEditMode()>=9&&getEditMode()<=20) {
                        setCursor(new Cursor(getDisplay(),SWT.CURSOR_CROSS));

                    }
                    else {
                        setCursor(pointerCursor);
                    }

                    break;
                }

                if (getEditMode()!=DocfactoGrabitPlugin.TEXT_MODE) {
                    text.setVisible(false);

                }

            }

            @Override
            public void mouseExit(MouseEvent e) {
                oldEditMode = getEditMode();
            }
        });

        addMouseMoveListener(new MouseMoveListener() {
     

            @Override
            public void mouseMove(MouseEvent e) {
                Point relMouseMove = getClientAreaPoint(e.x, e.y);
                switch (getEditMode()) {
                case DocfactoGrabitPlugin.POINTER_MODE:
                    break;
                    
                    
                case DocfactoGrabitPlugin.IMAGE_CROP_MODE:
                    Point mouseInArea = getClientAreaPoint(e.x, e.y);   
                    if (cropRect!=null) {
                        if(cropRect.contains(mouseInArea)){
                        ResizeableSelector reshapeState = new ResizeableSelector();
                         GrabitBorder dragAction1 = reshapeState.pointCloseToLine(mouseInArea,cropRect,6);
                         if(!mouseDown){
                             dragAction = dragAction1;
                         }
                         if(dragAction != null){
                            //change cursors when user rolls over hotspots
                             if(dragAction.equals(GrabitBorder.CENTRE)){
                                 setCursor(new Cursor(getDisplay(),SWT.CURSOR_HAND));
                             }else if(dragAction.equals(GrabitBorder.NE)){
                                 setCursor(new Cursor(getDisplay(),SWT.CURSOR_SIZENE));
                             }else if(dragAction.equals(GrabitBorder.NW)){
                                 setCursor(new Cursor(getDisplay(),SWT.CURSOR_SIZENW));
                             }else if(dragAction.equals(GrabitBorder.SE)){
                                 setCursor(new Cursor(getDisplay(),SWT.CURSOR_SIZESE));
                             }else if(dragAction.equals(GrabitBorder.SW)){
                                 setCursor(new Cursor(getDisplay(),SWT.CURSOR_SIZESW));
                             }else if(dragAction.equals(GrabitBorder.BOTTOM)){
                                 setCursor(new Cursor(getDisplay(),SWT.CURSOR_SIZES));
                             }else if(dragAction.equals(GrabitBorder.TOP)){
                                 setCursor(new Cursor(getDisplay(),SWT.CURSOR_SIZEN));
                             }else if(dragAction.equals(GrabitBorder.LEFT)){
                                 setCursor(new Cursor(getDisplay(),SWT.CURSOR_SIZEW));
                             }else if(dragAction.equals(GrabitBorder.RIGHT)){
                                 setCursor(new Cursor(getDisplay(),SWT.CURSOR_SIZEE));
                             }else {
                                 setCursor(new Cursor(getDisplay(),SWT.CURSOR_CROSS));
                             }
                         }
                        
                        }else{
                            
                            if(!mouseDown){
                                setCursor(cropCursor);
                                dragAction = null;
                            }
                        }
                        
                    }

                    if (mouseDown&&pointHolder1!=null) {

                        if (dragAction!=null && cropRect!=null) {
                                // cut rectangle is being reshaped/dragged.
                                if (dragAction.equals(GrabitBorder.BOTTOM)) {
                                    drawResizeableRect(new Rectangle(cropRect.x,cropRect.y, cropRect.width, relMouseMove.y-cropRect.y));
    
                                }else if (dragAction.equals(GrabitBorder.TOP)) {

                                    drawResizeableRect(new Rectangle(cropRect.x,relMouseMove.y, cropRect.width,cropRect.height+cropRect.y-relMouseMove.y));
    
                                }else if (dragAction.equals(GrabitBorder.CENTRE)) {
                                    
                                    drawResizeableRect(new Rectangle(relMouseMove.x - Math.abs(xFactor) ,relMouseMove.y - Math.abs(yFactor) , cropRect.width, cropRect.height));
    
                                }else if (dragAction.equals(GrabitBorder.LEFT)) {
                                    drawResizeableRect(new Rectangle(relMouseMove.x,cropRect.y, cropRect.x-relMouseMove.x+cropRect.width, cropRect.height));
    
                                }else if (dragAction.equals(GrabitBorder.RIGHT)) {
                                    drawResizeableRect(new Rectangle(cropRect.x,cropRect.y, relMouseMove.x-cropRect.x, cropRect.height));
    
                                }else if (dragAction.equals(GrabitBorder.NE)) {
                                    drawResizeableRect(new Rectangle(cropRect.x,relMouseMove.y, relMouseMove.x-cropRect.x, cropRect.height+cropRect.y-relMouseMove.y));
    
                                }else if (dragAction.equals(GrabitBorder.NW)) {
                                    drawResizeableRect(new Rectangle(relMouseMove.x, relMouseMove.y, cropRect.x-relMouseMove.x+cropRect.width, cropRect.height+cropRect.y-relMouseMove.y));
    
                                }else if (dragAction.equals(GrabitBorder.SE)) {
                                    drawResizeableRect(new Rectangle(cropRect.x, cropRect.y, relMouseMove.x-cropRect.x, relMouseMove.y-cropRect.y));
    
                                }else if (dragAction.equals(GrabitBorder.SW)) {
                                    drawResizeableRect(new Rectangle(relMouseMove.x,cropRect.y, cropRect.x-relMouseMove.x+cropRect.width, relMouseMove.y-cropRect.y));
    
                                }      
                            }

                        else{
                            drawResizeableRect(new Rectangle(mouseDownPoint.x,mouseDownPoint.y, relMouseMove.x-mouseDownPoint.x, relMouseMove.y-mouseDownPoint.y));
                            
                        }
                    }
                    break;
                    
                    
                case DocfactoGrabitPlugin.PENCIL_MODE:
                    if (mouseDown && pointHolder1!=null) {
                        
                        path.lineTo(relMouseMove.x,relMouseMove.y);
                        GC gc = getDrawingGC();
                        gc.drawPath(path);
                        redraw();
                        gc.dispose();
                    }

                    break;

                case DocfactoGrabitPlugin.LINE_MODE:

                    if (mouseDown&&pointHolder1!=null) {
                        if (pointHolder1!=null) {

                            GC gc = getDrawingGC();
                            gc.drawLine(mouseDownPoint.x,mouseDownPoint.y,relMouseMove.x,relMouseMove.y);
                            redraw();
                            gc.dispose();

                        }
                    }
                    break;

                case DocfactoGrabitPlugin.TRIANGLE_MODE:
                    if (mouseDown&&pointHolder1!=null) {

                        GC gc = getDrawingGC();
                        int signX = (int)Math.signum(relMouseMove.x-mouseDownPoint.x);
                        int signY = (int)Math.signum(relMouseMove.y-mouseDownPoint.y);
                        int xy = 0;
                        if (relMouseMove.x-mouseDownPoint.x>relMouseMove.y-mouseDownPoint.y) {
                            xy = Math.abs(relMouseMove.x-mouseDownPoint.x);

                        }
                        else {
                            xy = Math.abs(relMouseMove.y-mouseDownPoint.y);
                        }
                        gc.drawRectangle(mouseDownPoint.x,mouseDownPoint.y,
                            signX*xy,signY*xy);
                        redraw();
                        gc.dispose();

                    }
                    break;

                case DocfactoGrabitPlugin.CIRCLE_MODE:
                    if (mouseDown&&pointHolder1!=null) {

                        GC gc = getDrawingGC();
                        int signX = (int)Math.signum(relMouseMove.x-mouseDownPoint.x);
                        int signY = (int)Math.signum(relMouseMove.y-mouseDownPoint.y);
                        int xy = 0;
                        if (relMouseMove.x-mouseDownPoint.x>relMouseMove.y-mouseDownPoint.y) {
                            xy = Math.abs(relMouseMove.x-mouseDownPoint.x);

                        }
                        else {
                            xy = Math.abs(relMouseMove.y-mouseDownPoint.y);
                        }
                        gc.drawOval(mouseDownPoint.x,mouseDownPoint.y,signX*xy,
                            signY*xy);
                        redraw();
                        gc.dispose();

                    }
                    break;

                case DocfactoGrabitPlugin.OVAL_MODE:
                    if (mouseDown&&pointHolder1!=null) {

                        GC gc = getDrawingGC();
                        gc.drawOval(mouseDownPoint.x,mouseDownPoint.y,relMouseMove.x-
                            mouseDownPoint.x,relMouseMove.y-mouseDownPoint.y);
                        redraw();
                        gc.dispose();

                    }
                    break;

                case DocfactoGrabitPlugin.SQUARE_MODE:
                    if (mouseDown&&pointHolder1!=null) {

                        GC gc = getDrawingGC();
                        int signX = (int)Math.signum(relMouseMove.x-mouseDownPoint.x);
                        int signY = (int)Math.signum(relMouseMove.y-mouseDownPoint.y);
                        int xy = 0;
                        if (relMouseMove.x-mouseDownPoint.x>relMouseMove.y-mouseDownPoint.y) {
                            xy = Math.abs(relMouseMove.x-mouseDownPoint.x);

                        }
                        else {
                            xy = Math.abs(relMouseMove.y-mouseDownPoint.y);
                        }
                        gc.drawRectangle(mouseDownPoint.x,mouseDownPoint.y,
                            signX*xy,signY*xy);
                        redraw();
                        gc.dispose();

                    }
                    break;
                case DocfactoGrabitPlugin.RECTANGLE_MODE:
                    if (mouseDown&&pointHolder1!=null) {


                        GC gc = getDrawingGC();
                        gc.drawRectangle(mouseDownPoint.x,mouseDownPoint.y,relMouseMove.x-
                            mouseDownPoint.x,relMouseMove.y-mouseDownPoint.y);
                        redraw();
                        gc.dispose();

                    }
                    break;

                case DocfactoGrabitPlugin.ARC_MODE:
                    if (mouseDown&&pointHolder1!=null) {


                        GC gc = getDrawingGC();
                        int signX = (int)Math.signum(relMouseMove.x-mouseDownPoint.x);
                        int signY = (int)Math.signum(relMouseMove.y-mouseDownPoint.y);
                        int xy = 0;
                        if (relMouseMove.x-mouseDownPoint.x>relMouseMove.y-mouseDownPoint.y) {
                            xy = Math.abs(relMouseMove.x-mouseDownPoint.x);

                        }
                        else {
                            xy = Math.abs(relMouseMove.y-mouseDownPoint.y);
                        }
                        gc.drawRectangle(mouseDownPoint.x,mouseDownPoint.y,
                            signX*xy,signY*xy);
                        redraw();
                        gc.dispose();

                    }
                    break;

                }

            }
        });

        initScrollBars();
    }

    private void drawResizeableRect(Rectangle rectangle) {
        int hotSpotSize = 6;
        int hotSpotWidth = (int) Math.signum(rectangle.width)*hotSpotSize;
        int hotSpotHeight = (int) Math.signum(rectangle.height)*hotSpotSize;
        
        GC gc = getDrawingGC();
        
        cropRect = rectangle;
        
        // draw the boundary
        gc.setLineStyle(SWT.LINE_DASH);
        gc.setForeground(SWTUtils.getColor(SWT.COLOR_BLACK));
        gc.drawRectangle(cropRect.x,cropRect.y,cropRect.width,cropRect.height);
        //draw hotspots
        gc.setBackground(SWTUtils.getColor(SWT.COLOR_BLACK));        
        gc.fillRectangle(new Rectangle(cropRect.x,cropRect.y, hotSpotWidth, hotSpotHeight));
        gc.fillRectangle(new Rectangle(cropRect.x,cropRect.y-hotSpotHeight+cropRect.height, hotSpotWidth, hotSpotHeight));
        gc.fillRectangle(new Rectangle(cropRect.x,cropRect.y-hotSpotHeight/2+cropRect.height/2, hotSpotWidth, hotSpotHeight));
        gc.fillRectangle(new Rectangle(cropRect.x+cropRect.width-hotSpotWidth,cropRect.y, hotSpotWidth, hotSpotHeight));
        gc.fillRectangle(new Rectangle(cropRect.x-hotSpotWidth+cropRect.width,cropRect.y-hotSpotHeight+cropRect.height, hotSpotWidth, hotSpotHeight));
        gc.fillRectangle(new Rectangle(cropRect.x-hotSpotWidth+cropRect.width,cropRect.y-hotSpotHeight/2+cropRect.height/2, hotSpotWidth, hotSpotHeight));
        gc.fillRectangle(new Rectangle(cropRect.x-hotSpotWidth/2+cropRect.width/2,cropRect.y, hotSpotWidth, hotSpotHeight));
        gc.fillRectangle(new Rectangle(cropRect.x-hotSpotWidth/2+cropRect.width/2,cropRect.y-hotSpotHeight/2+cropRect.height/2, hotSpotWidth, hotSpotHeight));
        gc.fillRectangle(new Rectangle(cropRect.x-hotSpotWidth/2+cropRect.width/2,cropRect.y-hotSpotHeight+cropRect.height, hotSpotWidth, hotSpotHeight));
        gc.dispose();     
        redraw();

    }


    /**
     * Sets the X, Y, width & Height of the cropRect
     * taking into consideration negative values of width & height
     * 
     * @since 2.5
     */
    private void updateCropRect() {
        // set absolute values for height and width
        int cropWidth = Math.abs(cropRect.width);
        int cropHeight = Math.abs(cropRect.height);

        //make sure rectangles with negative width/height do not mess up with your x and y points.
        int rectX =cropRect.width>=0? cropRect.x: cropRect.x - Math.abs(cropRect.width);
        int rectY =cropRect.height>=0? cropRect.y: cropRect.y - Math.abs(cropRect.height);  

        cropRect = new Rectangle(rectX, rectY, cropWidth, cropHeight);
    
    }

    /**
     * Creates a drawing pane
     * 
     * @since 2.4.8
     */
    protected void createDrawingPane() {
        if(drawingPane!=null){
            drawingPane.dispose();
        }

        drawingPane = new Image(getDisplay(),
            sourceImage.getImageData());
    }

    /**
     * Method adds the property change listener to support class
     * 
     * @param l - the listener.
     * @since 2.4.8
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        support.addPropertyChangeListener(l);
    }

    /**
     * Perform a zooming operation centered on the given point (dx, dy) and
     * using the given scale factor. The given AffineTransform instance is
     * pre-concatenated.
     * 
     * @param dx center x
     * @param dy center y
     * @param scale zoom rate
     * @param af original affinetransform
     * @since 2.4.8
     */
    public void centerZoom(double dx,double dy,double scale,
    AffineTransform af) {
        af.preConcatenate(AffineTransform.getTranslateInstance(-dx,-dy));
        af.preConcatenate(AffineTransform.getScaleInstance(scale,scale));
        af.preConcatenate(AffineTransform.getTranslateInstance(dx,dy));
        transform = af;
        syncScrollBars();
    }

    /**
     * Crop image action.
     * <p>
     * Sets the bounds of the image on canvas to a given rectangular bounds
     * </p>
     * 
     * @param cropArea - {@code Rectangle} representing the intended crop area
     * @since 2.4.8
     */
    protected void cropImage(Rectangle cropArea) {
        Rectangle cropArea2 = cropArea;
        Rectangle cropRectangle2 = cropArea2.intersection(getSourceImage().getBounds()); 
        Image croppedImage = new Image(Display.getCurrent(), cropRectangle2.width, cropRectangle2.height);
        GC gc = new GC(getSourceImage());
        gc.copyArea(croppedImage, cropRectangle2.x, cropRectangle2.y);
        gc.dispose();
        BorderCreator borderMaker = new BorderCreator(16);
        IEclipsePreferences prefs = InstanceScope.INSTANCE.getNode(DocfactoGrabitPlugin.PLUGIN_ID);
        
        boolean top = prefs.getBoolean(DocfactoGrabitPlugin.STORE_CROP_STYLE_TOP,true);
        boolean bottom = prefs.getBoolean(DocfactoGrabitPlugin.STORE_CROP_STYLE_BOTTOM,true);
        boolean right = prefs.getBoolean(DocfactoGrabitPlugin.STORE_CROP_STYLE_RIGHT,true);
        boolean left = prefs.getBoolean(DocfactoGrabitPlugin.STORE_CROP_STYLE_LEFT,true);
        
        
        ImageData anImageData = borderMaker.addCrinkleBorder(croppedImage.getImageData(), top, right, bottom, left);
        
        
//        anImageData = borderMaker.dropShadow(anImageData,getBackground(),8,8,50);
        if (anImageData!=null) {
            setImageData(anImageData);
            syncScrollBars();
        }
        
        refresh();
        setDirty();

    }

    /**
     * @see org.eclipse.swt.widgets.Widget#dispose()
     */
    @Override
    public void dispose() {
        if (sourceImage!=null&&!sourceImage.isDisposed()) {
            sourceImage.dispose();
        }
        if (screenImage!=null&&!screenImage.isDisposed()) {
            screenImage.dispose();
        }
    }

    /**
     * Draw a shape using the provided values
     * 
     * @param x1 - the x coordinate of shape location
     * @param y1 - the y coordinate of shape location
     * @param x2 - the width or x coordinate of x2
     * @param y2 - the height or y coordinate of y2
     * @since 2.4.8
     */
    protected void drawShape(int x1,int y1,int x2,int y2) {
        if (getStrokeForegroundColour()!=null||
        getStrokeBackgroundColour()!=null) {

            /*
             * a shape style is one of OUTLINE, BORDERED, BORDERLESS OUTLINE:
             * getStrokeBackgroundColour()==null BORDERED:
             * getStrokeForegroundColour()!=null &&
             * getStrokeBackgroundColour()!=null BORDERLESS:
             * getStrokeForegroundColour()==null
             */

            text.setVisible(false);

            Point point1 = getClientAreaPoint(x1,y1);
            Point point2 = getClientAreaPoint(x2,y2);

            // ...x2 and y2 are width and height respectively
            int signX = (int)Math.signum(point2.x-point1.x);
            int signY = (int)Math.signum(point2.y-point1.y);
            int xy = 0;
            if (point2.x-point1.x>point2.y-point1.y) {
                xy = Math.abs(point2.x-point1.x);

            }
            else {
                xy = Math.abs(point2.y-point1.y);
            }

            GC gc = new GC(getSourceImage());
            gc.setLineWidth(getLineWidth());
            gc.setLineStyle(getLineStyle());
            if (getStrokeForegroundColour()!=null) {
                gc.setForeground(getStrokeForegroundColour());
            }
            if (getStrokeBackgroundColour()!=null) {
                gc.setBackground(getStrokeBackgroundColour());
            }

            switch (getEditMode()) {
            case DocfactoGrabitPlugin.LINE_MODE:
                if (getStrokeForegroundColour()!=null) {
                    gc.drawLine(point1.x,point1.y,point2.x,point2.y);
                }

                break;
            case DocfactoGrabitPlugin.RECTANGLE_MODE:
                if (getStrokeForegroundColour()==null) {
                    gc.fillRectangle(point1.x,point1.y,point2.x-point1.x,
                        point2.y-point1.y);

                }
                else if (getStrokeBackgroundColour()==null) {
                    gc.drawRectangle(point1.x,point1.y,point2.x-point1.x,
                        point2.y-point1.y);

                }
                else {
                    gc.fillRectangle(point1.x,point1.y,point2.x-point1.x,
                        point2.y-point1.y);
                    gc.drawRectangle(point1.x,point1.y,point2.x-point1.x,
                        point2.y-point1.y);

                }

                break;

            case DocfactoGrabitPlugin.SQUARE_MODE:

                if (getStrokeForegroundColour()==null) {
                    gc.fillRectangle(point1.x,point1.y,signX*xy,signY*xy);

                }
                else if (getStrokeBackgroundColour()==null) {
                    gc.drawRectangle(point1.x,point1.y,signX*xy,signY*xy);

                }
                else {
                    gc.fillRectangle(point1.x,point1.y,signX*xy,signY*xy);
                    gc.drawRectangle(point1.x,point1.y,signX*xy,signY*xy);

                }

                break;

            case DocfactoGrabitPlugin.OVAL_MODE:
                if (getStrokeForegroundColour()==null) {
                    gc.fillOval(point1.x,point1.y,point2.x-point1.x,point2.y-
                        point1.y);

                }
                else if (getStrokeBackgroundColour()==null) {
                    gc.drawOval(point1.x,point1.y,point2.x-point1.x,point2.y-
                        point1.y);

                }
                else {
                    gc.fillOval(point1.x,point1.y,point2.x-point1.x,point2.y-
                        point1.y);
                    gc.drawOval(point1.x,point1.y,point2.x-point1.x,point2.y-
                        point1.y);

                }
                break;


            case DocfactoGrabitPlugin.CIRCLE_MODE:

                if (getStrokeForegroundColour()==null) {
                    gc.fillOval(point1.x,point1.y,signX*xy,signY*xy);

                }
                else if (getStrokeBackgroundColour()==null) {
                    gc.drawOval(point1.x,point1.y,signX*xy,signY*xy);

                }
                else {
                    gc.fillOval(point1.x,point1.y,signX*xy,signY*xy);
                    gc.drawOval(point1.x,point1.y,signX*xy,signY*xy);

                }
                break;
            }

            gc.dispose();
            setDirty();
            redraw();
            pointHolder1 = null;

        }

    }

    /**
     * Fit the image onto the canvas
     * 
     * @since 2.4.8
     */
    public void fitCanvas() {
        if (sourceImage==null) {
            return;
        }
        Rectangle imageBound = sourceImage.getBounds();
        Rectangle destRect = getClientArea();
        double sx = (double)destRect.width/(double)imageBound.width;
        double sy = (double)destRect.height/(double)imageBound.height;
        double s = Math.min(sx,sy);
        double dx = 0.5*destRect.width;
        double dy = 0.5*destRect.height;
        centerZoom(dx,dy,s,new AffineTransform());
    }

    /**
     * The initialization process.
     * <p>
     * Method must be called from the constructor. It creates the cursor images
     * and puts the canvas image in a stack for undo/redo purposes
     * </p>
     * 
     * @since 2.4.8
     */
    private void initCanvas(Composite parent) {

        undoStack = new Stack<ImageData>();
        redoStack = new Stack<ImageData>();

        support = new PropertyChangeSupport(this);
        // get images for the cursors
        Image pencilIcon = DocfactoGrabitPlugin.getModeImage(DocfactoGrabitPlugin.PENCIL_MODE);
        Image zoomIcon = DocfactoGrabitPlugin.getModeImage(DocfactoGrabitPlugin.ZOOM_MODE);;
        Image zoomOutIcon = DocfactoGrabitPlugin.getModeImage(DocfactoGrabitPlugin.ZOOM_OUT_MODE);
        Image cropIcon = DocfactoGrabitPlugin.getModeImage(DocfactoGrabitPlugin.IMAGE_CROP_MODE);
        // set cursor images
        pencilCursor = new Cursor(parent.getDisplay(),
            pencilIcon.getImageData(),1,pencilIcon.getBounds().height-1);
        zoomCursor = new Cursor(parent.getDisplay(),zoomIcon.getImageData(),
            0,0);
        zoomOutCursor = new Cursor(parent.getDisplay(),
            zoomOutIcon.getImageData(),0,0);
        cropCursor = new Cursor(parent.getDisplay(),cropIcon.getImageData(),
            2,6);

        if (getImageData()!=null) {
            undoStack.push(getImageData());
            this.originalPos = undoStack.size();
        }

    }

    /* Initalize the scrollbar and register listeners. */
    private void initScrollBars() {
        ScrollBar horizontal = getHorizontalBar();
        horizontal.setEnabled(false);
        horizontal.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                scrollHorizontally((ScrollBar)event.widget);
            }
        });
        ScrollBar vertical = getVerticalBar();
        vertical.setEnabled(false);
        vertical.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                scrollVertically((ScrollBar)event.widget);
            }
        });
    }

    /**
     * Returns textBold.
     * 
     * @return the textBold
     * @since 2.4.8
     */
    public boolean isTextBold() {
        return textBold;
    }

    /**
     * Returns textItalic.
     * 
     * @return the textItalic
     * @since 2.4.8
     */
    public boolean isTextItalic() {
        return textItalic;
    }

    /**
     * Returns textUnderline.
     * 
     * @return the textUnderline
     * @since 2.4.8
     */
    public boolean isTextUnderline() {
        return textUnderline;
    }

    /* Paint function */
    private void paint(GC gc) {
        Rectangle clientRect = getClientArea(); /* Canvas' painting area */
        if (sourceImage!=null) {
            Rectangle imageRect =
                GrabitSWTUtil.inverseTransformRect(transform,clientRect);
            int gap = 2; /* find a better start point to render */
            imageRect.x -= gap;
            imageRect.y -= gap;
            imageRect.width += 2*gap;
            imageRect.height += 2*gap;

            Rectangle imageBound = sourceImage.getBounds();
            imageRect = imageRect.intersection(imageBound);
            Rectangle destRect =
                GrabitSWTUtil.transformRect(transform,imageRect);
            if (screenImage!=null) {
                screenImage.dispose();
            }
            screenImage =
                new Image(getDisplay(),clientRect.width,clientRect.height);
            GC newGC = new GC(screenImage);
            newGC.setClipping(clientRect);
            newGC.setBackground(gc.getBackground());
            newGC.fillRectangle(clientRect);
            newGC.drawImage(
                sourceImage,
                imageRect.x,
                imageRect.y,
                imageRect.width,
                imageRect.height,
                destRect.x,
                destRect.y,
                destRect.width,
                destRect.height);

            newGC.dispose();

            if (drawingPane!=null&&!drawingPane.isDisposed()) {
                if (screenImage2!=null) {
                    screenImage2.dispose();
                }
                screenImage2 =
                    new Image(getDisplay(),clientRect.width,clientRect.height);
                GC newGC2 = new GC(screenImage2);
                newGC2.setClipping(clientRect);
                newGC2.setBackground(gc.getBackground());
                newGC2.fillRectangle(clientRect);
                newGC2.drawImage(
                    drawingPane,
                    imageRect.x,
                    imageRect.y,
                    imageRect.width,
                    imageRect.height,
                    destRect.x,
                    destRect.y,
                    destRect.width,
                    destRect.height);

                newGC2.dispose();
                gc.drawImage(screenImage2,0,0);
            }
            else {
                gc.drawImage(screenImage,0,0);
            }

        }
        else {
            gc.setClipping(clientRect);
            gc.fillRectangle(clientRect);
            initScrollBars();
        }
    }

    /**
     * Show the image with the original size
     * 
     * @since 2.4.8
     */
    public void showOriginal() {
        if (sourceImage==null) {
            return;
        }
        transform = new AffineTransform();
        syncScrollBars();
    }

    /**
     * Synchronize the scrollbar with the image. If the transform is out of
     * range, it will correct it. This function considers only following factors
     * :<b> transform, image size, client area</b>.
     * 
     * @since 2.4.8
     */
    public void syncScrollBars() {
        if (sourceImage==null) {
            redraw();
            return;
        }

        AffineTransform af = transform;
        double sx = af.getScaleX(), sy = af.getScaleY();
        double tx = af.getTranslateX(), ty = af.getTranslateY();
        if (tx>0) {
            tx = 0;
        }
        if (ty>0) {
            ty = 0;
        }

        ScrollBar horizontal = getHorizontalBar();
        horizontal.setIncrement(getClientArea().width/100);
        horizontal.setPageIncrement(getClientArea().width);
        Rectangle imageBound = sourceImage.getBounds();
        int cw = getClientArea().width, ch = getClientArea().height;
        if (imageBound.width*sx>cw) { /* image is wider than client area */
            horizontal.setMaximum((int)(imageBound.width*sx));
            horizontal.setEnabled(true);
            if (((int)-tx)>horizontal.getMaximum()-cw) {
                tx = -horizontal.getMaximum()+cw;
            }
        }
        else { /* image is narrower than client area */
            horizontal.setEnabled(false);
            tx = (cw-imageBound.width*sx)/2; // center if too small.
        }
        horizontal.setSelection((int)(-tx));
        horizontal.setThumb((getClientArea().width));

        ScrollBar vertical = getVerticalBar();
        vertical.setIncrement(getClientArea().height/100);
        vertical.setPageIncrement((getClientArea().height));
        if (imageBound.height*sy>ch) { /* image is higher than client area */
            vertical.setMaximum((int)(imageBound.height*sy));
            vertical.setEnabled(true);
            if (((int)-ty)>vertical.getMaximum()-ch) {
                ty = -vertical.getMaximum()+ch;
            }
        }
        else { /* image is less higher than client area */
            vertical.setEnabled(false);
            ty = (ch-imageBound.height*sy)/2; // center if too small.
        }
        vertical.setSelection((int)(-ty));
        vertical.setThumb((getClientArea().height));

        /* update transform. */
        af = AffineTransform.getScaleInstance(sx,sy);
        af.preConcatenate(AffineTransform.getTranslateInstance(tx,ty));
        transform = af;

        redraw();
    }

    /**
     * Redo operation
     * <p>
     * Method restores a previously undone operation
     * </p>
     * 
     * @since 2.4.8
     */
    public void redo() {
        if (!redoStack.isEmpty()) {
            ImageData redoImage = redoStack.pop();
            undoStack.add(redoImage);
            setImageData(redoImage);
            refresh();
            if (undoStack.size()==originalPos) {
                // not dirty, input is same as original
                support.firePropertyChange(IS_DIRTY,null,
                    GrabitSWTUtil.EDITOR_IS_NOT_DIRTY);

            }
            else {
                // set dirty
                support.firePropertyChange(IS_DIRTY,null,
                    GrabitSWTUtil.EDITOR_DIRTY_NO_UPDATE);

            }

        }

    }


    /**
     * Undo operation
     * <p>
     * Method restores the image data to a previous {@code ImageData}
     * </p>
     * 
     * @since 2.4.8
     */
    public void undo() {

        if (!undoStack.isEmpty()) {

            redoStack.add(undoStack.pop());
            if (!undoStack.isEmpty()) {
                ImageData undoImage = undoStack.peek();
                if (undoImage!=null) {
                    setImageData(undoImage);
                    refresh();

                }

                if (undoStack.size()==originalPos) {
                    support.firePropertyChange(IS_DIRTY,null,
                        GrabitSWTUtil.EDITOR_IS_NOT_DIRTY);

                }
                else {
                    support.firePropertyChange(IS_DIRTY,null,
                        GrabitSWTUtil.EDITOR_IS_DIRTY);

                }
            }
        }

    }

    /**
     * Zoom in around the center of client Area.
     * 
     * @since 2.4.8
     */
    public void zoomIn() {
        if (sourceImage==null) {
            return;
        }
        Rectangle rect = getClientArea();
        int w = rect.width, h = rect.height;
        double dx = ((double)w)/2;
        double dy = ((double)h)/2;
        centerZoom(dx,dy,ZOOMIN_RATE,transform);
    }

    /**
     * Zoom in around the specified coordinates.
     * 
     * @param dx - the x value of coordinate
     * @param dy - the y value of coordinate
     * @since 2.4.8
     */
    public void zoomIn(int dx,int dy) {
        if (sourceImage==null) {
            return;
        }

        centerZoom(dx,dy,ZOOMIN_RATE,transform);
    }

    /**
     * Zoom out around the center of client Area.
     * 
     * @since 2.4.8
     */
    public void zoomOut() {
        if (sourceImage==null) {
            return;
        }
        Rectangle rect = getClientArea();
        int w = rect.width, h = rect.height;
        double dx = ((double)w)/2;
        double dy = ((double)h)/2;
        centerZoom(dx,dy,ZOOMOUT_RATE,transform);
    }    

    /**
     * Write the given text to canvas
     * 
     * @param text2 - The string to write
     * @since 2.4.8
     */
    protected void writeTextToCanvas(String text2) {

        text.setVisible(false);

        if (getTextForegroundColour() != null ) {

            GC gc = new GC(getSourceImage());
            gc.setFont(getCanvasFont());
            gc.setForeground(new Color(getDisplay(),getTextForegroundColour()));
            gc.drawString(text2,textPendingPoint.x,textPendingPoint.y-
                (fontSize/2),
                true);
            if (textUnderline) {
                Point stringLength = gc.textExtent(text2);
                gc.drawLine(textPendingPoint.x-1,textPendingPoint.y-
                    (fontSize/2)+
                    stringLength.y,textPendingPoint.x+stringLength.x-1,
                    textPendingPoint.y-(fontSize/2)+stringLength.y);

            }
            gc.dispose();
            redraw();
            // image has changed
            setDirty();

        }
        text.setText("");

    }

    /**
     * Sets "saved image" position in undo stack.
     * 
     * @param value - if false, the dirtiness of the editor will not be evaluated
     * during undo/redo operations
     * 
     * @since 2.4.8
     */
    public void resetStackPos(boolean value) {
        if (value) {
            this.originalPos = undoStack.size();

        }
        else {
            this.originalPos = 0;
        }

    }

    private void refresh() {
        syncScrollBars();
        redraw();
    }

    /**
     * Method removes the property change listener from the support class
     * 
     * @param l - the listener
     * @since 2.4.8
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        support.removePropertyChangeListener(l);
    }

    /* Scroll horizontally */
    private void scrollHorizontally(ScrollBar scrollBar) {
        if (sourceImage==null) {
            return;
        }

        AffineTransform af = transform;
        double tx = af.getTranslateX();
        double select = -scrollBar.getSelection();
        af.preConcatenate(AffineTransform.getTranslateInstance(select-tx,0));
        transform = af;
        syncScrollBars();
    }

    /* Scroll vertically */
    private void scrollVertically(ScrollBar scrollBar) {
        if (sourceImage==null) {
            return;
        }

        AffineTransform af = transform;
        double ty = af.getTranslateY();
        double select = -scrollBar.getSelection();
        af.preConcatenate(AffineTransform.getTranslateInstance(0,select-ty));
        transform = af;
        syncScrollBars();
    }

    // fire an event when image state changes
    /**
     * Sets the editor's dirty state
     * <p>
     * Method notifies the editor of canvas state by firing a property change
     * listener of name {@code IS_DIRTY}
     * </p>
     * 
     * @since 2.4.8
     */
    protected void setDirty() {
        redoStack.clear();
        // save image to stack
        ImageData imgData = getImageData();
        
        if (imgData!=null) {
            undoStack.push(imgData);

            // aaargh! neatness of code is not an issue here
            // ..just don't want to hold on to too much image data :(
            // shit list: make undo stack a fixed capacity
            if (undoStack.size()>20) {
                undoStack.remove(undoStack.size()-1);
                resetStackPos(false);

            }

        }

        support.firePropertyChange(IS_DIRTY,null,GrabitSWTUtil.EDITOR_IS_DIRTY);

    }

    /**
     * Method to get image
     * <p>
     * Get image from the provided file path
     * </p>
     * 
     * @return an {@code Image} file
     * @since 2.4.8
     */
    private Image getImage(File filepath) {
        return new Image(getDisplay(),filepath.getAbsolutePath());
    }



    private Font getCanvasFont() {

        Font font =
        new Font(getDisplay(),getFontType(),getFontSize(),SWT.NORMAL);
        if (textBold&&textItalic) {
            font =
            new Font(getDisplay(),getFontType(),getFontSize(),SWT.BOLD|
                SWT.ITALIC);
        }
        else if (textItalic) {
            font =
            new Font(getDisplay(),getFontType(),getFontSize(),SWT.ITALIC);
        }
        else if (textBold) {
            font = new Font(getDisplay(),getFontType(),getFontSize(),SWT.BOLD);
        }

        return font;

    }

    private Point getClientAreaPoint(int xA,int yA) {

        Point mouseXY = null;
        try {
            mouseXY = GrabitSWTUtil
            .transformPoint(transform.createInverse(),new Point(xA,yA));
        }
        catch (NoninvertibleTransformException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return mouseXY;
    }

    private GC getDrawingGC() {
        createDrawingPane();
        GC gc = new GC(drawingPane);
        gc.setForeground(SWTUtils.getColor(SWT.COLOR_BLUE));
        gc.setLineStyle(SWT.LINE_DOT);
        return gc;
    }

    /**
     * Reset the image data and update the image
     * 
     * @param data image data to be set
     * @since 2.4.8
     * 
     */
    public void setImageData(ImageData data) {
        if (sourceImage!=null) {
            sourceImage.dispose();
        }
        if (data!=null) {
            sourceImage = new Image(getDisplay(),data);
        }
        syncScrollBars();
    }

    /**
     * Set edit mode
     * <p>
     * Method is called from the editor to set this canvas's mode
     * </p>
     * 
     * 
     * @param i - the edit mode is normally one of
     * {@code DocfactoGrabitPlugin.*_MODE}
     * @since 2.4.8
     */
    public void setEditMode(int i) {
        this.editMode = i;
    }

    /**
     * Sets fontSize.
     * 
     * @param fontSize the fontSize value
     * @since 2.4.8
     */
    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    /**
     * Sets fontType.
     * 
     * @param fontType the fontType value
     * @since 2.4.8
     */
    public void setFontType(String fontType) {
        this.fontType = fontType;
    }

    /**
     * Sets lineStyle.
     * 
     * @param lineStyle the lineStyle value
     * @since 2.4.8
     */
    public void setLineStyle(int lineStyle) {
        this.lineStyle = lineStyle;
    }

    /**
     * Sets lineWidth.
     * 
     * @param lineWidth the lineWidth value
     * @since 2.4.8
     */
    public void setLineWidth(int lineWidth) {
        this.lineWidth = lineWidth;
    }

    /**
     * Source image setter.
     * 
     * @param newSource - The canvas's new image
     * @since 2.4.8
     * 
     */
    public void setSourceImage(Image newSource) {
        this.sourceImage = newSource;
    }

    /**
     * Sets strokeBackgroundColour.
     * 
     * @param strokeBackgroundColour the strokeBackgroundColour value
     * @since 2.4.8
     */
    public void setStrokeBackgroundColour(RGB strokeBackgroundColour) {
        this.strokeBackgroundColour = strokeBackgroundColour;
    }

    /**
     * Sets strokeForegroundColour.
     * 
     * @param strokeForegroundColour the strokeForegroundColour value
     * @since 2.4.8
     */
    public void setStrokeForegroundColour(RGB strokeForegroundColour) {
        this.strokeForegroundColour = strokeForegroundColour;
    }

    /**
     * Sets textBackgroundColour.
     * 
     * @param textBackgroundColour the textBackgroundColour value
     * @since 2.4.8
     */
    public void setTextBackgroundColour(RGB textBackgroundColour) {
        this.textBackgroundColour = textBackgroundColour;
    }

    /**
     * Sets textBold.
     * 
     * @param textBold the textBold value
     * @since 2.4.8
     */
    public void setTextBold(boolean textBold) {
        this.textBold = textBold;
    }

    /**
     * Sets textForegroundColour.
     * 
     * @param textForegroundColour the textForegroundColour value
     * @since 2.4.8
     */
    public void setTextForegroundColour(RGB textForegroundColour) {
        this.textForegroundColour = textForegroundColour;
    }

    /**
     * Sets textItalic.
     * 
     * @param textItalic the textItalic value
     * @since 2.4.8
     */
    public void setTextItalic(boolean textItalic) {
        this.textItalic = textItalic;
    }

    /**
     * Sets textUnderline.
     * 
     * @param textUnderline the textUnderline value
     * @since 2.4.8
     */
    public void setTextUnderline(boolean textUnderline) {
        this.textUnderline = textUnderline;
    }

    /**
     * Method returns this method's edit mode
     * 
     * @return - the edit mode
     * @since 2.4.8
     */
    public int getEditMode() {
        return editMode;
    }

    /**
     * Returns fontSize.
     * 
     * @return the fontSize
     * @since 2.4.8
     */
    public int getFontSize() {
        return fontSize;
    }

    /**
     * Returns fontType.
     * 
     * @return the fontType
     * @since 2.4.8
     */
    public String getFontType() {
        return fontType;
    }

    /**
     * Get the image data
     * 
     * @return image data of canvas
     * @since 2.4.8
     */
    public ImageData getImageData() {
        return sourceImage.getImageData();
    }

    /**
     * Returns lineStyle.
     * 
     * @return the lineStyle
     * @since 2.4.8
     */
    public int getLineStyle() {
        return this.lineStyle;
    }

    /**
     * Returns lineWidth.
     * 
     * @return the lineWidth
     * @since 2.4.8
     */
    public int getLineWidth() {
        return this.lineWidth;
    }

    /**
     * Source image getter.
     * 
     * @return sourceImage.
     * @since 2.4.8
     */
    public Image getSourceImage() {
        return sourceImage;
    }

    /**
     * Returns backgroundColour.
     * 
     * @return the backgroundColour
     * @since 2.4.8
     */
    public Color getStrokeBackgroundColour() {


        if (strokeBackgroundColour==null) {
            return null;
        }
        return new Color(getDisplay(),strokeBackgroundColour);
    }

    /**
     * Returns foregroundColour.
     * 
     * @return the foregroundColour
     * @since 2.4.8
     */
    public Color getStrokeForegroundColour() {

        if (strokeForegroundColour==null) {
            return null;
        }
        return new Color(getDisplay(),strokeForegroundColour);
    }

    /**
     * Returns backgroundColour.
     * 
     * @return the backgroundColour
     * @since 2.4.8
     */
    public RGB getTextBackgroundColour() {


        return this.textBackgroundColour;
    }

    /**
     * Returns foregroundColour.
     * 
     * @return the foregroundColour
     * @since 2.4.8
     */
    public RGB getTextForegroundColour() {

        return this.textForegroundColour;
    }

    public void resetStack() {
        undoStack.clear();
        undoStack.add(getImageData());
        
    }

}