package com.docfacto.grabit.editors;

import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.IUndoContext;
import org.eclipse.core.commands.operations.IUndoableOperation;
import org.eclipse.core.commands.operations.ObjectUndoContext;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IPathEditorInput;
import org.eclipse.ui.IStorageEditorInput;
import org.eclipse.ui.IURIEditorInput;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.SaveAsDialog;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.operations.UndoRedoActionGroup;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.part.FileEditorInput;
import org.osgi.service.prefs.BackingStoreException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.docfacto.common.IOUtils;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.core.IDocfactoCorePlugin;
import com.docfacto.core.utils.PluginUtils;
import com.docfacto.grabit.DocfactoGrabitPlugin;
import com.docfacto.grabit.dialogs.ResolutionSettingsDialog;
import com.docfacto.grabit.operations.GrabitOperation;
import com.docfacto.grabit.plugin.panels.PropertyPanel;

/**
 * Creates an editor part for images
 * <p>
 * Class has a tool bar and a status bar at the bottom. Editor can be used to
 * view and edit image files.
 * </p>
 * 
 * @author pemi - created Jul 29, 2013
 * @since 2.4
 */
public class ImageEditor extends EditorPart {

    /** Variable used for getting the save state of file */
    protected boolean isDirty = false;
    private ImageLoader loader;
    private SWTImageCanvas imageCanvas;
    private Image canvasImage;
    private IUndoContext myUndoContext;
    private UndoRedoActionGroup historyActionGroup;
    private IFile theIFile;
    private File theFile;
    private Composite composite;
    private PropertyPanel shapeProp;
    private String dpiValue;

    /**
     * default constructor
     * 
     * @since 2.4
     */
    public ImageEditor() {
        // nothing here yet
    }

    /**
     * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public void doSave(IProgressMonitor monitor) {

        // check licence validity
        if(!DocfactoGrabitPlugin.getDefault().isLicenceValid()){
            licenceDialog();
            return;
        }

        // file is null if image is an unsaved screenshot.
        if (isDirty() && theFile != null) {
            OutputStream out = null;

            // save the image file
            ImageLoader loader = new ImageLoader();
            loader.data = new ImageData[] { imageCanvas.getSourceImage()
                .getImageData() };

            String extension = IOUtils.extension(theFile);
            int depth = imageCanvas.getSourceImage()
            .getImageData().depth;
            // add metadata to file
            addMetaData(theFile);

            if(!(depth == 1 || depth == 4 || depth == 8) && extension.equalsIgnoreCase("gif")){

                // get bufferedimage from imagedata
                BufferedImage bufferedImage = GrabitSWTUtil.convertToAWT(imageCanvas.getImageData());

                // write the bufferedImage to the outputFile
                try {
                    ImageIO.write(GrabitSWTUtil.convertRGBAToIndexed(bufferedImage), "gif", theFile);
                }
                catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }else{
                try {
                    out = new BufferedOutputStream(new FileOutputStream(theFile));
                }
                catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if(out == null){
                    loader.save(theFile.getAbsolutePath(),
                        GrabitSWTUtil.getIntValueOfFileExtension(extension));

                }else{
                    loader.save(out,GrabitSWTUtil.getIntValueOfFileExtension(extension));
                }

            }

            setDirty(false, GrabitSWTUtil.EDITOR_IS_NOT_DIRTY);
        }

        // save as new; change editor input to the newly saved
        else {

            SaveAsDialog dialog = new SaveAsDialog(getSite().getShell());
            dialog.setOriginalName(getPartName());
            saveDialog(dialog);

        }

    }

    /**
     * @see org.eclipse.ui.part.EditorPart#doSaveAs()
     */
    @Override
    public void doSaveAs() {

        // open a saveas dialog
        SaveAsDialog dialog = new SaveAsDialog(getSite().getShell());
        dialog.setOriginalFile(theIFile);
        saveDialog(dialog);

    }

    /**
     * Method for saving files
     * <p>
     * Methods creates files from a given a {@code SaveAsDialog}
     * </p>
     * 
     * @param dialog
     *            whose results would be used to create new file
     * @since 2.4
     */
    private IFile saveDialog(SaveAsDialog dialog) {
        IFile saveasFile = null;
        if (dialog.open() == SaveAsDialog.OK) {
            try {
                // create IFile
                saveasFile = ResourcesPlugin.getWorkspace().getRoot()
                .getFile(dialog.getResult());

                File saveFile = PluginUtils.getFileFromIFile(saveasFile);
                addMetaData(saveFile);
                OutputStream out = new BufferedOutputStream(new FileOutputStream(saveFile));

                ImageLoader loader = new ImageLoader();
                loader.data = new ImageData[] { imageCanvas.getSourceImage()
                    .getImageData() };
                String extension = saveasFile.getFileExtension();

                // ...unfortunately, ImageLoader#save() does not handle some gif files properly
                int depth = imageCanvas.getImageData().depth;
                if(!(depth == 1 || depth == 4 || depth == 8) && extension.equalsIgnoreCase("gif")){
                    // get bufferedImage from imgagedata
                    BufferedImage bufferedImage = GrabitSWTUtil.convertToAWT(imageCanvas.getImageData());

                    // write the bufferedImage to an outputFile
                    ImageIO.write(GrabitSWTUtil.convertRGBAToIndexed(bufferedImage), "gif", saveFile);

                }else{

                    loader.save(out, GrabitSWTUtil
                        .getIntValueOfFileExtension(extension));
                }


                IProject project = ResourcesPlugin.getWorkspace().getRoot()
                .getProject(dialog.getResult().segment(0));
                if (project.exists()) {

                    // launch saved file
                    saveasFile.refreshLocal(IResource.DEPTH_ONE, null);

                    try {
                        ImageEditor openedEditor = (ImageEditor) getSite()
                        .getPage().openEditor(
                            new FileEditorInput(saveasFile),
                            DocfactoGrabitPlugin.IMAGE_EDITOR_ID);
                        
                        
                        // set the new image (needed if this file is already opened)
                        //get imageData from file
                        Image image = new Image(Display.getCurrent(),
                            saveFile.getAbsolutePath());
                        
                        openedEditor.getImageCanvas().setImageData(image.getImageData());
                        //						openedEditor.getImageCanvas().setImageData(
                        //                            imageCanvas.getSourceImage().getImageData());
                        openedEditor.setDirty(false,GrabitSWTUtil.EDITOR_NEWLY_LOADED);
                        

                        // ..and close current editor
                        getSite().getPage().closeEditor(this, false);

                    } catch (PartInitException e) {
                        DocfactoGrabitPlugin.logException(
                            "unable to launch image editor", e);
                    }

                }

            } catch (SWTException ex) {
                DocfactoGrabitPlugin.logException("Unable to save As", ex);
            } catch (CoreException e) {
                DocfactoGrabitPlugin.logException("Grabit Error", e);
            }
            catch (FileNotFoundException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return saveasFile;

    }

    /**
     * @see org.eclipse.ui.part.EditorPart#init(org.eclipse.ui.IEditorSite,
     *      org.eclipse.ui.IEditorInput)
     */
    @Override
    public void init(IEditorSite site, IEditorInput input)
    throws PartInitException {

        setPartName(input.getName());

        if (input instanceof IFileEditorInput) {
            theIFile = ((IFileEditorInput) input).getFile();
            // Convert to file
            theFile = PluginUtils.getFileFromIFile(theIFile);
        } else if (input instanceof IPathEditorInput) {
            // FInd the file in the work space
            IWorkspace ws = ResourcesPlugin.getWorkspace();
            IWorkspaceRoot wsr = ((IResource) ws).getWorkspace().getRoot();
            IPath location = ((IPathEditorInput) input).getPath();
            theIFile = wsr.getFileForLocation(location);
            theFile = wsr.getFileForLocation(location).getRawLocation()
            .makeAbsolute().toFile();
        } else if (input instanceof IURIEditorInput) {
            URI uri = ((IURIEditorInput) input).getURI();
            theFile = new File(uri);
        }

        loader = new ImageLoader();

        // load the image from storage or file
        if (input instanceof IStorageEditorInput) {
            setSite(site);
            setInput(input);
            try {
                InputStream screenImage = ((IStorageEditorInput) input)
                .getStorage().getContents();
                canvasImage = new Image(this.getSite().getShell().getDisplay(),
                    screenImage);

            } catch (CoreException e) {
                e.printStackTrace();
            }

        } else {

            try {
                ImageData[] imgData = loader.load(new FileInputStream(theFile));
                if (imgData != null) {
                    setSite(site);
                    setInput(input);
                } else {
                    throw new PartInitException("Unsupported file type");
                }
            } catch (IOException e) {
                DocfactoGrabitPlugin.logException(
                    "an I/O exception of some sort has occurred", e);
            }
        }

        myUndoContext = new ObjectUndoContext(this);
        historyActionGroup = new UndoRedoActionGroup(getSite(), myUndoContext,
            false);
        historyActionGroup.fillActionBars(getEditorSite().getActionBars());
        dpiValue = getMetadataDPI();

        setLicenceStatusBar();

    }

    private void setLicenceStatusBar() {
        if(!DocfactoGrabitPlugin.getDefault().isLicenceValid()){

            PluginUtils.displayStatusLineMessage(this, "Update your Dofacto Grabit licence.");
        }

    }

    /**
     * @see org.eclipse.ui.part.EditorPart#isDirty()
     */
    @Override
    public boolean isDirty() {
        return isDirty;
    }

    /**
     * Sets the state of the file
     * 
     * @param isDirty
     *            - {@code true} if dirty
     * @param type
     *            - the dirtiness type (one of GrabitSWTUtil.EDITOR_*)
     * @since 2.4
     */
    public void setDirty(boolean isDirty, int type) {
        this.isDirty = isDirty;
        firePropertyChange(PROP_DIRTY);

        if (isDirty) {
            if (type == GrabitSWTUtil.EDITOR_IS_DIRTY) {
                IUndoableOperation operation = new GrabitOperation("grabit");
                operation.addContext(myUndoContext);
                try {
                    OperationHistoryFactory.getOperationHistory().execute(
                        operation, null, null);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        } else {
            // image is saved
            if(type==GrabitSWTUtil.EDITOR_NEWLY_LOADED){
                imageCanvas.resetStack();
            }
            imageCanvas.resetStackPos(true);
        }

    }

    /**
     * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
     */
    @Override
    public boolean isSaveAsAllowed() {
        return DocfactoGrabitPlugin.getDefault().isLicenceValid();
    }

    /**
     * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createPartControl(final Composite parent) {

        FillLayout fillLayout = (FillLayout) parent.getLayout();
        fillLayout.type = SWT.VERTICAL;

        composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(2, false));

        Composite composite_2 = new Composite(composite, SWT.NONE);
        composite_2.setLayout(new FillLayout(SWT.VERTICAL));
        GridData gd_composite_2 = new GridData(SWT.LEFT, SWT.TOP, false, false,
            1, 1);
        gd_composite_2.widthHint = 32;
        composite_2.setLayoutData(gd_composite_2);

        // get the SWTImageCanvas
        createImageCanvas();

        final ToolBar toolBar = new ToolBar(composite_2, SWT.FLAT | SWT.RIGHT
            | SWT.VERTICAL);

        ToolItem pencilTlItem = new ToolItem(toolBar, SWT.RADIO);
        pencilTlItem.setToolTipText("Pencil tool");
        pencilTlItem.setImage(DocfactoGrabitPlugin
            .getModeImage(DocfactoGrabitPlugin.PENCIL_MODE));
        pencilTlItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                imageCanvas.setEditMode(DocfactoGrabitPlugin.PENCIL_MODE);
                clearPropertiesPanel();
                shapeProp.setPanelMode(DocfactoGrabitPlugin.PENCIL_MODE);
                shapeProp.setVisible(true);

            }
        });

        final ToolItem straightLItem = new ToolItem(toolBar, SWT.RADIO);
        straightLItem.setToolTipText("Line tool");
        straightLItem.setImage(DocfactoGrabitPlugin
            .getModeImage(DocfactoGrabitPlugin.LINE_MODE));
        straightLItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                imageCanvas.setEditMode(DocfactoGrabitPlugin.LINE_MODE);
                clearPropertiesPanel();
                shapeProp.setPanelMode(DocfactoGrabitPlugin.LINE_MODE);
                shapeProp.setVisible(true);
            }
        });

        ToolItem rectItem = new ToolItem(toolBar, SWT.RADIO);
        rectItem.setToolTipText("Rectangle tool");
        rectItem.setImage(DocfactoGrabitPlugin
            .getModeImage(DocfactoGrabitPlugin.RECTANGLE_MODE));
        rectItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                shapeProp.setPanelMode(DocfactoGrabitPlugin.RECTANGLE_MODE);
                imageCanvas.setEditMode(shapeProp.getEditMode());
                clearPropertiesPanel();
                shapeProp.setVisible(true);

            }
        });

        ToolItem squareItem = new ToolItem(toolBar, SWT.RADIO);
        squareItem.setToolTipText("Square tool");
        squareItem.setImage(DocfactoGrabitPlugin
            .getModeImage(DocfactoGrabitPlugin.SQUARE_MODE));
        squareItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                shapeProp.setPanelMode(DocfactoGrabitPlugin.SQUARE_MODE);
                imageCanvas.setEditMode(shapeProp.getEditMode());
                clearPropertiesPanel();
                shapeProp.setVisible(true);

            }
        });

        ToolItem ovalItem = new ToolItem(toolBar, SWT.RADIO);
        ovalItem.setToolTipText("Oval tool");
        ovalItem.setImage(DocfactoGrabitPlugin
            .getModeImage(DocfactoGrabitPlugin.OVAL_MODE));
        ovalItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                shapeProp.setPanelMode(DocfactoGrabitPlugin.OVAL_MODE);
                imageCanvas.setEditMode(shapeProp.getEditMode());
                clearPropertiesPanel();
                shapeProp.setVisible(true);

            }
        });

        ToolItem circleItem = new ToolItem(toolBar, SWT.RADIO);
        circleItem.setToolTipText("Circle tool");
        circleItem.setImage(DocfactoGrabitPlugin
            .getModeImage(DocfactoGrabitPlugin.CIRCLE_MODE));
        circleItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                shapeProp.setPanelMode(DocfactoGrabitPlugin.CIRCLE_MODE);
                imageCanvas.setEditMode(shapeProp.getEditMode());
                clearPropertiesPanel();
                shapeProp.setVisible(true);

            }
        });

        ToolItem textTlItem = new ToolItem(toolBar, SWT.RADIO);
        textTlItem.setToolTipText("Text tool");
        textTlItem.setImage(DocfactoGrabitPlugin
            .getModeImage(DocfactoGrabitPlugin.TEXT_MODE));
        textTlItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                imageCanvas.setEditMode(DocfactoGrabitPlugin.TEXT_MODE);
                clearPropertiesPanel();
                shapeProp.setPanelMode(DocfactoGrabitPlugin.TEXT_MODE);

                shapeProp.redraw();
                shapeProp.setVisible(true);

            }
        });

        ToolItem rectTlItem = new ToolItem(toolBar, SWT.RADIO);
        rectTlItem.setToolTipText("Crop tool");
        rectTlItem.setImage(DocfactoGrabitPlugin
            .getModeImage(DocfactoGrabitPlugin.IMAGE_CROP_MODE));
        rectTlItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                imageCanvas.setEditMode(DocfactoGrabitPlugin.IMAGE_CROP_MODE);
                clearPropertiesPanel();
                shapeProp.setPanelMode(DocfactoGrabitPlugin.IMAGE_CROP_MODE);
                shapeProp.redraw();
                shapeProp.setVisible(true);
            }
        });

        ToolItem zoomTlItem = new ToolItem(toolBar, SWT.RADIO);
        zoomTlItem.setToolTipText("Zoom in tool");
        zoomTlItem.setImage(DocfactoGrabitPlugin
            .getModeImage(DocfactoGrabitPlugin.ZOOM_MODE));
        zoomTlItem.addSelectionListener(new SelectionAdapter() {

            /**
             * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent e) {
                imageCanvas.setEditMode(DocfactoGrabitPlugin.ZOOM_MODE);
                clearPropertiesPanel();
            }
        });

        ToolItem zoomoutTlItem = new ToolItem(toolBar, SWT.RADIO);
        zoomoutTlItem.setToolTipText("Zoom out tool");
        zoomoutTlItem.setImage(DocfactoGrabitPlugin
            .getModeImage(DocfactoGrabitPlugin.ZOOM_OUT_MODE));
        zoomoutTlItem.addSelectionListener(new SelectionAdapter() {

            @Override
            public void widgetSelected(SelectionEvent e) {

                imageCanvas.setEditMode(DocfactoGrabitPlugin.ZOOM_OUT_MODE);
                clearPropertiesPanel();

            }
        });

        ToolItem toolItem = new ToolItem(toolBar, SWT.SEPARATOR);

        ToolItem tltmSvg = new ToolItem(toolBar, SWT.PUSH);
        tltmSvg.setToolTipText("Export image to SVG Editor");
        tltmSvg.setImage(DocfactoCorePlugin
            .getImage(IDocfactoCorePlugin.BEERMAT_ICON_16));
        tltmSvg.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {

                try {

                    if (isDirty) {

                        doSave(null);

                    }

                    FileDialog svgDialog = new FileDialog(getSite().getShell(),
                        SWT.SAVE);

                    String[] filterNames = new String[] { "SVG (Scalable Vector Graphics)" };
                    String[] filterExtensions = new String[] { "*.svg" };

                    svgDialog.setFilterNames(filterNames);
                    svgDialog.setFilterExtensions(filterExtensions);
                    svgDialog.setOverwrite(true);
                    svgDialog.setFilterPath(theFile.getParent());

                    svgDialog.open();
                    if (!svgDialog.getFileName().isEmpty()) {
                        File svgFile = new File(svgDialog.getFilterPath(),
                            svgDialog.getFileName());
                        IWorkbench wb = PlatformUI.getWorkbench();
                        IWorkbenchPage page = wb.getActiveWorkbenchWindow()
                        .getActivePage();

                        FileWriter fw = new FileWriter(svgFile
                            .getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        String content = "<svg width=\""
                        + imageCanvas.getImageData().width
                        + "\" height=\""
                        + imageCanvas.getImageData().height
                        + "\" \n"
                        + "  xmlns=\"http://www.w3.org/2000/svg\" >"
                        + "<image height=\""
                        + imageCanvas.getImageData().height
                        + "\" width=\""
                        + imageCanvas.getImageData().width
                        + "\" x=\"0\" xlink:href=\""
                        + theFile.getAbsolutePath()
                        + "\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" y=\"0\"/>"
                        + "</svg>";
                        bw.write(content);
                        bw.close();

                        IFileStore fileStore = EFS.getLocalFileSystem()
                        .getStore(svgFile.toURI());
                        IDE.openEditorOnFileStore(page, fileStore);
                    }
                } catch (PartInitException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            }
        });

        ToolItem tltmSettings = new ToolItem(toolBar, SWT.PUSH);
        tltmSettings.setToolTipText("Image settings");
        tltmSettings.addSelectionListener(new SelectionAdapter() {

            @Override
            public void widgetSelected(SelectionEvent e) {

                ResolutionSettingsDialog settingsDialog = new ResolutionSettingsDialog(
                    getSite().getShell());

                // set dialog values
                settingsDialog.setResValue(Integer.valueOf(dpiValue));

                Double oldResVal = Double.valueOf(dpiValue);

                if (settingsDialog.open() == Window.OK) {
                    // update image settings
                    dpiValue = String.valueOf(settingsDialog.getResValue());

                    // set dirty if value has changed significantly
                    if (Math.abs(oldResVal - (Double.valueOf(dpiValue))) > 0) {
                        setDirty(true, GrabitSWTUtil.EDITOR_IS_DIRTY);
                    }

                    // save selection pref to preference store
                    try {

                        InstanceScope.INSTANCE.getNode(
                            DocfactoGrabitPlugin.PLUGIN_ID).flush();
                    } catch (BackingStoreException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }

                }

            }

        });
        tltmSettings.setImage(DocfactoGrabitPlugin
            .getImage("/icons/settings.png"));

        imageCanvas.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true,
            1, 1));
        imageCanvas.setSize(594, 235);
        new Label(composite, SWT.NONE);
        Composite composite_1 = new Composite(composite, SWT.NONE);
        GridData gd_composite_1 = new GridData(SWT.LEFT, SWT.CENTER, false,
            false, 1, 1);
        gd_composite_1.widthHint = 521;
        composite_1.setLayoutData(gd_composite_1);
        composite_1.setLayout(new FormLayout());

        shapeProp = new PropertyPanel(composite_1, SWT.NONE);
        shapeProp.setLayout(new FormLayout());
        shapeProp.setLayoutData(new FormData());
        shapeProp.setBackground(new Color(parent.getDisplay(), new RGB(232,
            232, 232)));
        shapeProp.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals(PropertyPanel.LINE_WIDTH)) {
                    imageCanvas.setLineWidth(shapeProp.getLineWidth());

                } else if (evt.getPropertyName().equals(
                    PropertyPanel.LINE_STYLE)) {
                    imageCanvas.setLineStyle(shapeProp.getLineStyle());

                } else if (evt.getPropertyName().equals(PropertyPanel.BG_COLOR)) {
                    imageCanvas.setStrokeBackgroundColour(shapeProp
                        .getTheBackgroundColour());

                } else if (evt.getPropertyName().equals(PropertyPanel.FG_COLOR)) {
                    imageCanvas.setStrokeForegroundColour(shapeProp
                        .getTheForegroundColour());

                } else if (evt.getPropertyName()
                .equals(PropertyPanel.FONT_TYPE)) {
                    imageCanvas.setFontType(shapeProp.getFontType());

                } else if (evt.getPropertyName()
                .equals(PropertyPanel.TEXT_SIZE)) {
                    imageCanvas.setFontSize(shapeProp.getTextSize());

                } else if (evt.getPropertyName()
                .equals(PropertyPanel.TEXT_BOLD)) {
                    imageCanvas.setTextBold(shapeProp.isTextBold());

                } else if (evt.getPropertyName().equals(
                    PropertyPanel.TEXT_FG_COLOR)) {
                    imageCanvas.setTextForegroundColour(shapeProp
                        .getTheTextFGColour());

                } else if (evt.getPropertyName().equals(
                    PropertyPanel.TEXT_UNDERLINE)) {
                    imageCanvas.setTextUnderline(shapeProp.isTextUnderline());

                } else if (evt.getPropertyName().equals(
                    PropertyPanel.TEXT_ITALIC)) {
                    imageCanvas.setTextItalic(shapeProp.isTextItalic());

                }

            }

        });

        // default select the pencil tool item
        pencilTlItem.setSelection(true);
        imageCanvas.setEditMode(DocfactoGrabitPlugin.PENCIL_MODE);
        shapeProp.setPanelMode(DocfactoGrabitPlugin.PENCIL_MODE);
        shapeProp.setVisible(true);

        updateSWTImageCanvas();

    }

    /**
     * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
     */
    @Override
    public void setFocus() {
        historyActionGroup.fillActionBars(getEditorSite().getActionBars());
        imageCanvas.setFocus();
    }

    /**
     * @see org.eclipse.ui.part.EditorPart#isSaveOnCloseNeeded()
     */
    @Override
    public boolean isSaveOnCloseNeeded() {

        // this code needs review
        if (theFile == null) {

            // remove from workbench if closing
            IWorkbench wb = PlatformUI.getWorkbench();
            if (wb.isClosing()) {
                IWorkbenchPage page = wb.getActiveWorkbenchWindow()
                .getActivePage();
                page.closeEditor(this, false);
            }

            return false;
        }

        return super.isSaveOnCloseNeeded();
    }

    /**
     * Method performs an undo operation on the canvas
     * 
     * @since 2.4
     */
    public void grabitUndo() {
        imageCanvas.undo();
    }

    /**
     * Method performs a redo operation on the canvas
     * 
     * @since 2.4
     */
    public void grabitRedo() {
        imageCanvas.redo();
    }

    /**
     * Returns imageCanvas.
     * 
     * @return the imageCanvas
     * @since 2.4
     */
    public SWTImageCanvas getImageCanvas() {

        if (imageCanvas == null) {
            createImageCanvas();
        }

        return imageCanvas;
    }

    /**
     * Method creates an image canvas
     * <p>
     * TODO - Method Description
     * </p>
     * 
     * @since n.n
     */
    private void createImageCanvas() {
        if (theFile != null) {
            imageCanvas = new SWTImageCanvas(composite, theFile);
        } else if (canvasImage != null) {
            setDirty(true, GrabitSWTUtil.EDITOR_IS_NOT_DIRTY);
            imageCanvas = new SWTImageCanvas(composite, canvasImage);

        } else {
            canvasImage = new Image(this.getSite().getShell().getDisplay(),
                new Rectangle(0, 0, 500, 500));
            imageCanvas = new SWTImageCanvas(composite, canvasImage);

        }
        imageCanvas.addPropertyChangeListener(new PropertyChangeListener() {
            /**
             * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
             */
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals(SWTImageCanvas.IS_DIRTY)) {

                    if (evt.getNewValue().equals(
                        GrabitSWTUtil.EDITOR_IS_NOT_DIRTY)) {
                        setDirty(false, GrabitSWTUtil.EDITOR_IS_NOT_DIRTY);
                    } else if (evt.getNewValue().equals(
                        GrabitSWTUtil.EDITOR_IS_DIRTY)) {

                        setDirty(true, GrabitSWTUtil.EDITOR_IS_DIRTY);
                    } else if (evt.getNewValue().equals(
                        GrabitSWTUtil.EDITOR_DIRTY_NO_UPDATE)) {
                        // just set dirty, don't call the #setDirty() method
                        setDirty(true, GrabitSWTUtil.EDITOR_DIRTY_NO_UPDATE);

                    }

                }

            }
        });

    }

    /**
     * This method sets the drawing properties
     * of this canvas.
     * 
     * @since 2.4
     */
    private void updateSWTImageCanvas() {
        imageCanvas.setStrokeBackgroundColour(shapeProp
            .getTheBackgroundColour());
        imageCanvas.setStrokeForegroundColour(shapeProp
            .getTheForegroundColour());
        imageCanvas.setLineStyle(shapeProp.getLineStyle());
        imageCanvas.setLineWidth(shapeProp.getLineWidth());
        imageCanvas.setTextForegroundColour(shapeProp.getTheTextFGColour());
        imageCanvas.setFontSize(shapeProp.getTextSize());
        imageCanvas.setFontType(shapeProp.getFontType());
        imageCanvas.setTextBold(shapeProp.isTextBold());
        imageCanvas.setTextItalic(shapeProp.isTextItalic());
        imageCanvas.setTextUnderline(shapeProp.isTextUnderline());

    }

    private void clearPropertiesPanel() {
        shapeProp.setVisible(false);

    }

    private void addMetaData(File file) {


        if (file != null) {


            if(!file.exists()){
                try {
                    file.createNewFile();
                }
                catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }


            // I will review this section later.
            ImageWriter writer = null;
            for (Iterator<ImageWriter> iw = ImageIO
            .getImageWritersByFormatName(IOUtils.extension(file));iw
            .hasNext();) {
                writer = iw.next();
                ImageWriteParam writeParam = writer.getDefaultWriteParam();
                ImageTypeSpecifier typeSpecifier = ImageTypeSpecifier
                .createFromBufferedImageType(BufferedImage.TYPE_INT_RGB);
                IIOMetadata metadata = writer.getDefaultImageMetadata(
                    typeSpecifier,writeParam);
                if (metadata.isReadOnly()
                ||!metadata.isStandardMetadataFormatSupported()) {
                    continue;
                }

                break;
            }
            ImageWriteParam writeParam = writer.getDefaultWriteParam();
            ImageTypeSpecifier typeSpecifier = ImageTypeSpecifier
            .createFromBufferedImageType(BufferedImage.TYPE_INT_RGB);
            IIOMetadata metadata =
            writer.getDefaultImageMetadata(typeSpecifier,
                writeParam);
            try {
                setDPI(metadata,IOUtils.extension(file));
                final ImageOutputStream stream = ImageIO
                .createImageOutputStream(file);
                try {
                    writer.setOutput(stream);
                    //                    BufferedImage image =
                    //                        GrabitSWTUtil.convertToAWT(imageCanvas
                    //                            .getImageData());
                    //
                    //                    writer.write(metadata,new IIOImage(image,null,metadata),
                    //                        writeParam);
                }
                finally {
                    stream.close();

                }
                if(theIFile != null){
                    theIFile.refreshLocal(IResource.DEPTH_ZERO,null);
                }
            }
            catch (IIOInvalidTreeException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (CoreException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    /**
     * Setting image DPI
     * <p>
     * This method adds a dimension property to the metadata of image file
     * </p>
     * 
     * @param metadata
     *            {@code IIOMetadata} of file
     * @throws IIOInvalidTreeException
     *             when unable to parse the associated metadata
     * @since n.n
     */
    private void setDPI(IIOMetadata metadata, String formatName)
    throws IIOInvalidTreeException {

        double dotsPerMilli = Double.valueOf(dpiValue) / 25.4;

        IIOMetadataNode horiz = new IIOMetadataNode("HorizontalPixelSize");
        horiz.setAttribute("value", Double.toString(dotsPerMilli));

        IIOMetadataNode vert = new IIOMetadataNode("VerticalPixelSize");
        vert.setAttribute("value", Double.toString(dotsPerMilli));

        IIOMetadataNode dim = new IIOMetadataNode("Dimension");
        dim.appendChild(horiz);
        dim.appendChild(vert);

        IIOMetadataNode root = new IIOMetadataNode("javax_imageio_1.0");
        root.appendChild(dim);

        metadata.mergeTree("javax_imageio_1.0", root);
    }


    private void licenceDialog() {
        MessageDialog.openWarning(this.getEditorSite().getShell(),"Invalid Grabit Licence",
        "Unable to save file: No valid licence found for Docfacto Grabit.");
    }


    /**
     * A null value is returned if no DPI value is found for file
     * 
     * @return DPI value of the image file.
     */
    private String getMetadataDPI() {
        String val = null;

        // return display DPI if no actual image file exist
        if (theFile != null) {
            try {

                ImageInputStream iis = ImageIO.createImageInputStream(theFile);
                Iterator<ImageReader> readers = ImageIO.getImageReaders(iis);

                if (readers.hasNext()) {

                    // pick the first available ImageReader
                    ImageReader reader = readers.next();

                    // attach source to the reader
                    reader.setInput(iis, true);
                    double mmToInch = 25.4;

                    NodeList nodeList;
                    Element node = (Element) reader.getImageMetadata(0).getAsTree(
                    "javax_imageio_1.0");
                    nodeList = node.getElementsByTagName("HorizontalPixelSize");
                    if (nodeList != null && nodeList.getLength() == 1){
                        val = String.valueOf(Math.round(mmToInch
                            / Float.parseFloat(((Element) nodeList.item(0))
                                .getAttribute("value"))));
                    }
                }


            } catch (IOException e) {

                e.printStackTrace();
            }

        }

        if(val == null){
            val = String.valueOf(Display.getCurrent().getDPI().x);
        }
        return val;

    }

    @Override
    public void dispose() {
        getImageCanvas().dispose();
        super.dispose();
    }
}