package com.docfacto.grabit.editors.tools;

import java.util.Random;

import org.eclipse.swt.graphics.ImageData;

/**
 * Border Creator
 *<P>
 * A class to handle the look and feel of image edge.
 * 
 *
 * @author pemi - created 25 Jan 2014
 * @since 2.5
 */
public class BorderCreator {

    int size;
    public BorderCreator(int size) {
        super();
        this.size = size;
    }

    
    
    /**
     * Method adds a crinkle effect to the given edges
     * 
     * @param imageData - The image data
     * @param top - true if effect is desired
     * @param right - true if effect is desired
     * @param bottom - true if effect is desired
     * @param left - true if effect is desired
     * @return ImageData with edges modified
     * @since 2.5
     */
    public ImageData addCrinkleBorder(ImageData imageData,boolean top, boolean right, boolean bottom, boolean left) {

        ImageData ideaImageData = imageData;
        int width = ideaImageData.width;
        int height = ideaImageData.height;
        int leftSide = size;
        int rightSide = width-size;
        int topSide = size;
        int bottomSide = height-size;
        
        int leftDirection =0;
        int rightDirection = 0; 
        int topDirection = 0;
        int bottomDirection = 0;
        int topSideArray[] = new int[width]; 
        int bottomSideArray[] = new int[width];
        
        // create top and bottom line limits for crinkle
        for(int x = 0; x<ideaImageData.width;x++){
            topDirection = addStepAndDirection(size);
            bottomDirection = addStepAndDirection(size);
            topSide +=
            topSide+topDirection>size||topSide+topDirection<0
                ? topDirection*-1 : topDirection;
            
            bottomSide +=
            bottomSide+bottomDirection>height||
                bottomSide+bottomDirection<height-size ? bottomDirection*-1
                : bottomDirection;
            topSideArray[x] = topSide;
            bottomSideArray[x] = bottomSide;
            
        }

        int[] lineData = new int[ideaImageData.width];
        for (int y = 0;y<ideaImageData.height;y++) {

            leftDirection = addStepAndDirection(size);
            rightDirection = addStepAndDirection(size);
            
            // make sure the border is not out of bounds
            leftSide +=
                leftSide+leftDirection>size||leftSide+leftDirection<0
                    ? leftDirection*-1 : leftDirection;
            rightSide +=
                rightSide+rightDirection>width||
                    rightSide+rightDirection<width-size ? rightDirection*-1
                    : rightDirection;

            ideaImageData.getPixels(0,y,width,lineData,0);
            // Analyze each pixel value in the line
            for (int x = 0;x<lineData.length;x++) {
                if (((x<leftSide&&left)
                ||(x>rightSide&&right))
                ||((y<topSideArray[x]&&top)
                ||(y>bottomSideArray[x])&&bottom)) {
                    //ideaImageData.setPixel(x,y,0x000000);
                    ideaImageData.setAlpha(x,y,0);
                    
                    
                    
                }
                else {
                    ideaImageData.setAlpha(x,y,255);
                    if(((x<leftSide+size/3 && left)
                    ||(x>rightSide-size/3) && right)
                    ||((y<topSideArray[x]-size/3 && top)
                    ||((y>bottomSideArray[x]-size/3)&& bottom))){
                        ideaImageData.setPixel(x,y,0xFFFFFF);
                            ideaImageData.setAlpha(x,y,120);
                        }
                    
                }
            }
        }
        return ideaImageData;
    }

    private int addStepAndDirection(int size) {
        int nextStep = getNextGaussianicStep(size);
        // change integer sign
        Random rnd = new Random();
        if (rnd.nextBoolean()) {
            nextStep *= -1;
        }
        return nextStep;
    }

    public int getNextGaussianicStep(int size) {
        int variation = 0;
        Random arand = new Random();
        int desiredVariation = (int)Math.abs(Math.rint(arand.nextGaussian()));
        variation =
            (int)(desiredVariation>size ? Math.random()*size : desiredVariation);
        return variation;
    }
    

}
