package com.docfacto.grabit.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.PlatformUI;

import com.docfacto.grabit.editors.ImageEditor;

/**
 * Grabit undoable operation. 
 * <p>
 * Sets the operation for undo/redo actions which in turn 
 * calls the respective method in the editor.
 * 
 * </p>
 *
 * @author pemi - created Sep 12, 2013
 * @since 2.4
 */
public class GrabitOperation extends AbstractOperation {

    /**
     * Constructor sets operation with specified label
     * @param label - string displayed in undo/redo menu
     * @since 2.4
     */
    public GrabitOperation(String label) {
        super(label);
    }

    /**
     * @see org.eclipse.core.commands.operations.AbstractOperation#execute(org.eclipse.core.runtime.IProgressMonitor, org.eclipse.core.runtime.IAdaptable)
     */
    @Override
    public IStatus execute(IProgressMonitor monitor, IAdaptable info)
            throws ExecutionException {
        return Status.OK_STATUS;
    }

    /**
     * @see org.eclipse.core.commands.operations.AbstractOperation#redo(org.eclipse.core.runtime.IProgressMonitor, org.eclipse.core.runtime.IAdaptable)
     */
    @Override
    public IStatus redo(IProgressMonitor monitor, IAdaptable info)
            throws ExecutionException {
        Object obj = (ImageEditor) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
        if(obj instanceof ImageEditor){
            ImageEditor activeEditor = (ImageEditor)obj;
            activeEditor.grabitRedo();
        }
        return Status.OK_STATUS;
    }

    /**
     * @see org.eclipse.core.commands.operations.AbstractOperation#undo(org.eclipse.core.runtime.IProgressMonitor, org.eclipse.core.runtime.IAdaptable)
     */
    @Override
    public IStatus undo(IProgressMonitor monitor, IAdaptable info)
            throws ExecutionException {
        Object obj = (ImageEditor) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
        if(obj instanceof ImageEditor){
            ImageEditor activeEditor = (ImageEditor)obj;
            activeEditor.grabitUndo();
        }
        return Status.OK_STATUS;
    }

}
