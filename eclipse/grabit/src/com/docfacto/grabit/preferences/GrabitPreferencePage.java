package com.docfacto.grabit.preferences;

import java.awt.AWTException;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashSet;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.part.IPageSite;
import org.eclipse.ui.part.Page;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.FormAttachment;

import com.docfacto.grabit.DocfactoGrabitPlugin;
import com.docfacto.grabit.editors.GrabitSWTUtil;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.osgi.service.prefs.BackingStoreException;

/**
 * Grabit preference page
 * 
 * <p>
 * Page displays option for users to select all screens or choose from a list of available screens. 
 * Display IDs are stored concatenated in a single string using a delimiter.
 * </p>
 * 
 * @author pemi - created Sep 23, 2013
 * @since 2.4
 */
public class GrabitPreferencePage extends PreferencePage implements
IWorkbenchPreferencePage {
    /**
     * The delimiter used in storing display IDs
     */
    public final static String STORE_KEY = "-grab-";

    private final HashSet<String> screenID = new HashSet<String>();
    private HashSet<String> tempCollection = new HashSet<String>();
    private Group grpTakeShotOf;
    private Button takeSelectedScreens;
    private Button takeAllScreens;
    private IEclipsePreferences prefs;

    /**
     * GrabitPreferencePage constructor
     * 
     * @since 2.4
     */
    public GrabitPreferencePage() {
    }

    /**
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    @Override
    public void init(IWorkbench workbench) {
        // nothing here yet

    }

    /**
     * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createContents(Composite parent) {
        prefs = InstanceScope.INSTANCE.getNode(DocfactoGrabitPlugin.PLUGIN_ID);

        Composite container = new Composite(parent,SWT.NULL);
        container.setLayout(new FormLayout());

        takeAllScreens = new Button(container,SWT.RADIO);
        takeAllScreens.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {

                enableGroup(grpTakeShotOf,!takeAllScreens.getSelection());

            }
        });

        FormData fd_btnTakeShotOf = new FormData();
        fd_btnTakeShotOf.bottom = new FormAttachment(0,29);
        fd_btnTakeShotOf.right = new FormAttachment(0,299);
        fd_btnTakeShotOf.top = new FormAttachment(0);
        fd_btnTakeShotOf.left = new FormAttachment(0);
        takeAllScreens.setLayoutData(fd_btnTakeShotOf);
        takeAllScreens.setText("Take shot of all available screens");

        takeSelectedScreens = new Button(container,SWT.RADIO);
        takeSelectedScreens.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                enableGroup(grpTakeShotOf,!takeAllScreens.getSelection());

            }
        });
        FormData fd_btnTakeShotOf_1 = new FormData();
        fd_btnTakeShotOf_1.top = new FormAttachment(takeAllScreens,6);
        fd_btnTakeShotOf_1.left = new FormAttachment(takeAllScreens,0,SWT.LEFT);
        takeSelectedScreens.setLayoutData(fd_btnTakeShotOf_1);
        takeSelectedScreens.setText("Take shot of selected screens");

        GraphicsEnvironment env =
            GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] devices = env.getScreenDevices();
        grpTakeShotOf = new Group(container,SWT.NONE);
        grpTakeShotOf.setLayout(new GridLayout(devices.length,false));
        FormData fd_grpTakeShotOf = new FormData();
        fd_grpTakeShotOf.right = new FormAttachment(takeAllScreens,224);
        fd_grpTakeShotOf.left = new FormAttachment(takeAllScreens,0,SWT.LEFT);
        fd_grpTakeShotOf.bottom = new FormAttachment(100,-206);
        fd_grpTakeShotOf.top = new FormAttachment(takeSelectedScreens,1);
        grpTakeShotOf.setLayoutData(fd_grpTakeShotOf);
        String screenIDStore = prefs.get(DocfactoGrabitPlugin.SCREEN_IDS,"");
        ;
        String[] screenIDStoreArray = screenIDStore.split(STORE_KEY);

        for (String id:screenIDStoreArray) {
            screenID.add(id);
        }

        // Composite displayWindow = new Composite(grpTakeShotOf, SWT.NONE);
        // GridData gd_displayWindow = new GridData(SWT.LEFT, SWT.CENTER, false,
        // false, 1, 1);
        // gd_displayWindow.widthHint = 131;
        // displayWindow.setLayoutData(gd_displayWindow);
        // displayWindow.setBackground(SWTResourceManager.getColor(SWT.COLOR_BLUE));
        //
        //
        // Composite displayWindow2 = new Composite(grpTakeShotOf, SWT.NONE);
        // GridData gd_displayWindow2 = new GridData(SWT.LEFT, SWT.CENTER,
        // false, false, 1, 1);
        // gd_displayWindow2.heightHint = 71;
        // displayWindow2.setLayoutData(gd_displayWindow2);
        // displayWindow2.setBackground(SWTResourceManager.getColor(SWT.COLOR_BLUE));

        int scaleFactor = 40;

        // create a composite for each available screen
        for (final GraphicsDevice screenDevice:devices) {
            GraphicsConfiguration[] screenConfigurations =
                screenDevice.getConfigurations();

            Composite displayWindow = new Composite(grpTakeShotOf,SWT.NONE);
            GridData gd_displayWindow =
                new GridData(SWT.LEFT,SWT.CENTER,false,false,1,1);

            displayWindow.setBackground(parent.getBackground());

            // iterate through this screen's configuration to get specific
            // details
            for (GraphicsConfiguration screenConfig:screenConfigurations) {

                Rectangle rect = screenConfig.getBounds();
                BufferedImage image = null;
                try {
                    image = new Robot().createScreenCapture(rect);
                }
                catch (AWTException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (image!=null) {
                    AffineTransform af = new AffineTransform();
                    af.scale(0.05,0.05);
                    BufferedImage scaled =
                        new BufferedImage(rect.width,rect.height,
                            BufferedImage.TYPE_INT_ARGB);

                    AffineTransformOp scaleOp =
                        new AffineTransformOp(af,
                            AffineTransformOp.TYPE_BILINEAR);
                    scaled = scaleOp.filter(image,scaled);
                    ImageData data =
                        new ImageData(GrabitSWTUtil.getImageInputStream(scaled));
                    final Image swtImage = new Image(parent.getDisplay(),data);
                    double xVal = image.getWidth()*af.getScaleX();
                    double yVal = image.getHeight()*af.getScaleY();

                    gd_displayWindow.widthHint = (int)xVal;
                    gd_displayWindow.heightHint = (int)yVal;

                    final Button btnCheckButton =
                        new Button(displayWindow,SWT.CHECK);
                    btnCheckButton.setBounds(0,0,(int)xVal,(int)yVal);
                    btnCheckButton.setSelection(screenID.contains(screenDevice
                        .getIDstring()));
                    if (screenID.contains(screenDevice.getIDstring())) {
                        tempCollection.add(screenDevice.getIDstring());

                    }

                    btnCheckButton.addSelectionListener(new SelectionAdapter() {
                        @Override
                        public void widgetSelected(SelectionEvent e) {
                            if (btnCheckButton.getSelection()) {
                                tempCollection.add(screenDevice.getIDstring());
                            }
                            else {
                                tempCollection.remove(screenDevice
                                    .getIDstring());

                            }

                        }
                    });

                    displayWindow.setLayoutData(gd_displayWindow);
                    displayWindow.setBackgroundImage(swtImage);

                }

            }

        }
        // set boolean value to "true" if none can be found in the store
        String takeAll =
            prefs.get(DocfactoGrabitPlugin.GRAB_ALL_SCREENS,"true");

        if (takeAll.equals("true")) {
            takeAllScreens.setSelection(true);

        }
        else {
            takeSelectedScreens.setSelection(true);

        }
        enableGroup(grpTakeShotOf,!takeAllScreens.getSelection());
        grpTakeShotOf.pack();

        return container;

    }

    /**
     * Method recursively looks at a given composite and sets its children state
     * based on the given value
     * 
     * @param aprent - the parent composite
     * @param b - enables children if true
     * @since 2.4
     */
    private void enableGroup(Composite aprent,boolean b) {

        Control[] children = aprent.getChildren();
        for (Control child:children)
        {
            if (child instanceof Composite)
            {
                enableGroup((Composite)child,b);
            }
            else {
                child.setEnabled(b);
            }
        }

        aprent.setEnabled(b);

    }

    /**
     * @see org.eclipse.jface.preference.PreferencePage#performOk()
     */
    @Override
    public boolean performOk() {

        savePluginSettings();

        return true;
    }

    /**
     * Method saves IDs to a string
     * 
     * @param screenID2
     * @return
     * @since 2.4
     */
    private String saveIDsToStore(HashSet<String> screenID2) {
        String storeIDs = "";
        for (String s:screenID2)
        {
            if (s.length()>0) {
                storeIDs += s+STORE_KEY;
            }

        }

        return storeIDs;

    }

    private void savePluginSettings() {

        String screenIDStore = saveIDsToStore(tempCollection);

        // saves plugin preferences at the workspace level
        prefs.put(DocfactoGrabitPlugin.SCREEN_IDS,screenIDStore);
        prefs.put(DocfactoGrabitPlugin.GRAB_ALL_SCREENS,
            Boolean.toString(takeAllScreens.getSelection()));

    }
}
