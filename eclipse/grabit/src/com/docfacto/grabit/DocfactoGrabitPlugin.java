package com.docfacto.grabit;

import java.net.URL;
import java.util.GregorianCalendar;

import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import com.docfacto.config.XmlConfig;
import com.docfacto.config.generated.Product;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.core.utils.PluginUtils;
import com.docfacto.licensor.Products;

/**
 * Grabit activator
 * <p>
 * The activator class controls the plug-in life cycle. It also has an image
 * utility method for getting {@code ImageDescriptor}s
 * </p>
 * 
 * @author pemi - created Jul 24, 2013
 * @since 2.4
 */
public class DocfactoGrabitPlugin extends AbstractUIPlugin {

    /**
     * The plug-in ID
     */
    public static final String PLUGIN_ID = "com.docfacto.grabit"; //$NON-NLS-1$

    /**
     * Prefix of frame position store name
     */
    public static final String FRAME_POSITION = "Frame.Position";

    /**
     * Default image extension
     */
    public static final String DEFAULT_EXTENSION = ".png";

    // Edit modes
    /**
     * Default mode
     */
    public static final int POINTER_MODE = 0;
    /**
     * Value in {@code int} indicating the pencil edit mode
     */
    public static final int PENCIL_MODE = 1;
    /**
     * Value in {@code int} indicating the zoom-in edit mode
     */
    public static final int ZOOM_MODE = 2;
    /**
     * Value in {@code int} indicating the rectangle-select edit mode
     */
    public static final int IMAGE_CROP_MODE = 3;
    /**
     * Value in {@code int} indicating the eraser edit mode
     */
    public static final int ERASE_MODE = 4;
    /**
     * Value in {@code int} indicating the shapes edit mode
     */
    public static final int SHAPE_MODE = 5;
    /**
     * Value in {@code int} indicating the zoom-out edit mode
     */
    public static final int ZOOM_OUT_MODE = 6;

    /**
     * Image editor ID
     */
    public static final String IMAGE_EDITOR_ID =
        "com.docfacto.grabit.editors.ImageEditor";

    /**
     * Store name of screen IDs
     */
    public static final String SCREEN_IDS = "com.docfacto.grabit.sreen.ids";

    /**
     * Store name of line width properties
     */
    public static final String STORE_PROP_LINE_WIDTH = "grabit.line.width";

    /**
     * Store name of colour RGB foreground colour
     */
    public static final String STORE_PROP_FG_COLOUR =
        "grabit.foreground.colour";

    /**
     * Store name of colour RGB background colour
     */
    public static final String STORE_PROP_BG_COLOUR =
        "grabit.background.colour";

    /**
     * Store name of colour RGB foreground colour
     */
    public static final String STORE_PROP_TEXT_FG_COLOUR =
        "grabit.text.foreground.colour";

    /**
     * Store name of colour RGB background colour
     */
    public static final String STORE_PROP_TEXT_BG_COLOUR =
        "grabit.text.background.colour";

    /**
     * Store name of text font size
     */
    public static final String STORE_PROP_FONT_SIZE = "grabit.font.size";

    /**
     * Store name of text font type
     */
    public static final String STORE_PROP_FONT_TYPE = "grabit.font.type";

    /**
     * Store name of text BOLD properties
     */
    public static final String STORE_PROP_TEXT_IS_BOLD = "grabit.text.bold";

    /**
     * Store name of text ITALIC properties
     */
    public static final String STORE_PROP_TEXT_IS_ITALIC = "grabit.text.italic";

    /**
     * Store name text UNDERLINE properties
     */
    public static final String STORE_PROP_TEXT_IS_UNDERLINE =
        "grabit.text.underline";

    /**
     * Store name of line style properties
     */
    public static final String STORE_PROP_LINE_STYLE = "grabit.line.style";

    /**
     * Value in {@code int} indicating the straight line mode
     */
    public static final int LINE_MODE = 7;

    /**
     * Value for screen grab option
     */
    public static final String GRAB_ALL_SCREENS = "grabit.allScreens";

    /**
     * Value in {@code int} indicating the text edit mode
     */
    public static final int TEXT_MODE = 8;

    /**
     * Value in {@code int} indicating the shape drawing mode
     */
    public static final int SHAPES_MODE = 9;

    /**
     * Value in {@code int} indicating the circle drawing mode
     */
    public static final int CIRCLE_MODE = 10;

    /**
     * Value in {@code int} indicating the oval drawing mode
     */
    public static final int OVAL_MODE = 11;

    /**
     * Value in {@code int} indicating the square drawing mode
     */
    public static final int SQUARE_MODE = 12;

    /**
     * Value in {@code int} indicating the rectangle drawing mode
     */
    public static final int RECTANGLE_MODE = 13;

    /**
     * Value in {@code int} indicating the triangle drawing mode
     */
    public static final int TRIANGLE_MODE = 14;

    /**
     * Value in {@code int} indicating the arc drawing mode
     */
    public static final int ARC_MODE = 15;

    /**
     * String name of property change event
     */
    public static final String SET_EDIT_MODE = "canvas edit event";

	public static final String STORE_PROP_IMAGE_RES = "grabit_resolution";
    public static final String STORE_PROP_CROP_STYLE = "grabit_crop_style";
    public static final String STORE_CROP_STYLE_TOP = "grabit_crop_top";
    public static final String STORE_CROP_STYLE_LEFT = "grabit_crop_left";
    public static final String STORE_CROP_STYLE_BOTTOM = "grabit_crop_bottom";
    public static final String STORE_CROP_STYLE_RIGHT = "grabit_crop_right";

    // The shared instance
    private static DocfactoGrabitPlugin thePlugin;

    private static Image pencilIcon;

    private static Image straightLine;

    private static Image rectIcon;

    private static Image zoomIcon;

    private static Image zoomOutIcon;

    private static Image boldIcon;

    private static Image underlineIcon;

    private static Image italicIcon;

    private static Image textIcon;

    private static Image shapesIcon;

    private static Image circleIcon;

    private static Image ovalIcon;

    private static Image squareIcon;

    private static Image rectangleIcon;

    private static Image triangleIcon;

    private static Image arcIcon;

    private boolean imagesLoaded = false;

    /**
     * The default constructor
     * 
     * @since 2.4
     */
    public DocfactoGrabitPlugin() {
    }

    /**
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
     */
    public void start(BundleContext context) throws Exception {
        super.start(context);

        thePlugin = this;

        // Lets register the plugin
        DocfactoCorePlugin.registerPlugin("Grabit",Products.GRABIT);
    }

    /**
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
     */
    public void stop(BundleContext context) throws Exception {
        thePlugin = null;
        super.stop(context);
    }

    /**
     * Returns the shared instance
     * 
     * @return the shared instance
     * @since 2.4
     */
    public static DocfactoGrabitPlugin getDefault() {
        return thePlugin;
    }

    /**
     * Returns an image descriptor for the image file at the given plug-in
     * relative path
     * 
     * @param path the path
     * @return the image descriptor
     * @since 2.4
     */
    public static ImageDescriptor getImageDescriptor(String path) {
        return imageDescriptorFromPlugin(PLUGIN_ID,path);
    }

    /**
     * Method gets image from provided key value
     * <p>
     * An image is requested from the image registry with the given key value; A
     * new image is created and stored in the registry if no image is associated
     * with the given key.
     * </p>
     * <p>
     * Note: This method would store your images. Restrict usage to frequently
     * occurring images
     * </p>
     * 
     * @param key - String in form of a relative file path
     * @return Image represented by the key value provided
     * @since 2.4
     */
    public static Image getImage(String key) {
        Image image = getDefault().createImageRegistry().get(key);
        if (image==null) {
            URL url = getDefault().getBundle().getResource(key);
            image = (ImageDescriptor.createFromURL(url)).createImage();
            getDefault().createImageRegistry().put(key,image);
        }

        return image;

    }

    /**
     * Log and exception to the console
     * 
     * @param message to display
     * @param ex cause, can be null
     * @since 1.1
     */
    public static void logException(String message,Throwable ex) {
        thePlugin.getLog().log(
            new Status(Status.ERROR,PLUGIN_ID,Status.ERROR,message,ex));
    }

    /**
     * Log message.
     * 
     * This will only log a message if debugging is switched on.
     * 
     * @param message to log
     * @since 2.4
     */
    public static void logMessage(String message) {
        thePlugin.getLog().log(new Status(Status.INFO,PLUGIN_ID,Status.INFO,
            message,null));
    }

    /**
     * TODO - Method Title
     * <p>
     * TODO - Method Description
     * </p>
     * 
     * @param editMode
     * @return
     * @since n.n
     */
    public static Image getModeImage(int editMode) {

        Image modeImage = null;

        switch (editMode) {

        case DocfactoGrabitPlugin.ARC_MODE:
            modeImage = getImage("/icons/arc.png");
            break;

        case DocfactoGrabitPlugin.CIRCLE_MODE:
            modeImage = getImage("/icons/circle.png");
            break;

        case DocfactoGrabitPlugin.LINE_MODE:
            modeImage = getImage("icons/straight_line.png");
            break;

        case DocfactoGrabitPlugin.OVAL_MODE:
            modeImage = getImage("/icons/oval.png");
            break;

        case DocfactoGrabitPlugin.PENCIL_MODE:
            modeImage = getImage("/icons/pencil_icon.png");
            break;

        case DocfactoGrabitPlugin.IMAGE_CROP_MODE:
            modeImage = getImage("/icons/select_icon.png");
            break;

        case DocfactoGrabitPlugin.RECTANGLE_MODE:
            modeImage = getImage("/icons/rectangle.png");
            break;

        case DocfactoGrabitPlugin.SHAPE_MODE:
            modeImage = getImage("/icons/shapes_icon.png");
            break;

        case DocfactoGrabitPlugin.SHAPES_MODE:
            modeImage = getImage("/icons/shapes_icon.png");
            break;

        case DocfactoGrabitPlugin.SQUARE_MODE:
            modeImage = getImage("/icons/square.png");
            break;

        case DocfactoGrabitPlugin.TEXT_MODE:
            modeImage = getImage("/icons/text_icon.png");
            break;

        case DocfactoGrabitPlugin.ZOOM_MODE:
            modeImage = getImage("/icons/zoom_icon.png");
            break;

        case DocfactoGrabitPlugin.TRIANGLE_MODE:
            modeImage = getImage("/icons/triangle.png");
            break;

        case DocfactoGrabitPlugin.ZOOM_OUT_MODE:
            modeImage = getImage("/icons/zoomout_icon.png");
            break;
        }

        return modeImage;
    }
    
    /**
     * Method checks if Grabit is properly licensed
     * 
     * @return <code>true</code> if license valid
     * @since 2.5
     */
    public boolean isLicenceValid() {

        XmlConfig xmlConfig = DocfactoCorePlugin.getXmlConfig();
        if (xmlConfig!=null) {
            Product grabitProduct = xmlConfig.getProduct(Products.GRABIT);
            boolean valid = GregorianCalendar.getInstance().before(grabitProduct.getExpiryDate().toGregorianCalendar());
            if (grabitProduct!=null && valid) {
                // License is valid
                return true;
            }
        }

        return false;

    }
}
