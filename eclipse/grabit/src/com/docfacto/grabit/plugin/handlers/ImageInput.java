package com.docfacto.grabit.plugin.handlers;

import org.eclipse.core.resources.IStorage;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.IStorageEditorInput;

/**
 * Editor input class for images
 *
 * @author pemi - created Sep 12, 2013
 * @since 2.4
 */
public class ImageInput implements IStorageEditorInput {
    private IStorage storage;
    private String name;
    
    
    /**
     * Constructs an image input from the provided {@code IStorage}.
     * @param storage
     */
    ImageInput(IStorage storage){
        this.storage = storage;
        this.name = storage.getName();
    }

    /**
     * @see org.eclipse.ui.IEditorInput#exists()
     */
    @Override
    public boolean exists() {
        return false;
    }

    /**
     * @see org.eclipse.ui.IEditorInput#getImageDescriptor()
     */
    @Override
    public ImageDescriptor getImageDescriptor() {
        return null;
    }

    /**
     * @see org.eclipse.ui.IEditorInput#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @see org.eclipse.ui.IEditorInput#getPersistable()
     */
    @Override
    public IPersistableElement getPersistable() {
        return null;
    }

    /**
     * @see org.eclipse.ui.IEditorInput#getToolTipText()
     */
    @Override
    public String getToolTipText() {
        return "Screenshot: "+ storage.getName();
    }

    /**
     * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
     */
    @Override
    public Object getAdapter(Class adapter) {
        return null;
    }

    /**
     * @see org.eclipse.ui.IStorageEditorInput#getStorage()
     */
    @Override
    public IStorage getStorage() throws CoreException {
        return storage;
    }
    
    /**
     * Sets the name of editor input
     * @param name sets the input name
     * @since 2.4
     */
    public void setName(String name) {
        this.name = name;
    }

}
