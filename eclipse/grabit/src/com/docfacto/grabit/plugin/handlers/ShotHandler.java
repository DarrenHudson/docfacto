package com.docfacto.grabit.plugin.handlers;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.ArrayList;

import javax.swing.JFrame;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IStorage;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;

import com.docfacto.grabit.DocfactoGrabitPlugin;
import com.docfacto.grabit.editors.GrabitSWTUtil;
import com.docfacto.grabit.editors.ImageEditor;
import com.docfacto.grabit.plugin.panels.Countdown;
import com.docfacto.grabit.preferences.GrabitPreferencePage;

/**
 * This handler extends AbstractHandler, an IHandler base class.
 * <p>
 * When a user clicks on the grabit tool, dDepending on the state of the menu
 * (timed/normal), the image is taken after a wait time(1sec or 7secs
 * respectively). A screen shot is taken and the image is displayed in the
 * editor. The countdown shell is moveable JFrame.
 * </p>
 * 
 * @see org.eclipse.core.commands.AbstractHandler
 * @author pemi - created July 24, 2013
 * @since 2.4
 */
public class ShotHandler extends AbstractHandler {

    private Shell shell;
    private int waitTime = 0;
    private Job countdown;
    private int screenCount = 0;
    private boolean takeAll = true;

    /**
     * The constructor.
     * 
     * @since 2.4
     */
    public ShotHandler() {

    }

    /**
     * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
     */
    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {

        shell = Display.getCurrent().getActiveShell();

        // check default radio settings and set value for wait time
        ICommandService service = (ICommandService)PlatformUI.getWorkbench()
            .getService(ICommandService.class);
        Command defaultCom = service.getCommand("grabit2.command4");
        String defState = (String)defaultCom
            .getState("org.eclipse.ui.commands.radioState").getValue();
        if (defState.equals("Timed")) {
            waitTime = 7000;

        }
        else {
            waitTime = 1000;
        }
        // get a location for the progress bar for camera timer
        final int cX = shell.getLocation().x+10;
        final int cY = shell.getLocation().y+10;
        // create timer if wait time is more than 1 second
        if (waitTime>1000) {
            countdown = new Job("Start countdown") {
                protected IStatus run(IProgressMonitor monitor) {

                    final JFrame countDownFrame = new JFrame();
                    countDownFrame.setUndecorated(true);
                    countDownFrame.setAlwaysOnTop(true);
                    countDownFrame.setBackground(new Color(0,0,0,0));
                    countDownFrame.setBounds(cX,cY,40,150);
                    countDownFrame.add(new Countdown(waitTime));
                    countDownFrame.pack();
                    countDownFrame.setVisible(true);

                    try {
                        Thread.sleep(waitTime-1000);
                        countDownFrame.dispose();
                    }
                    catch (InterruptedException e) {
                        DocfactoGrabitPlugin
                            .logException("Grabit: Timer iterupt error "
                                +e.getMessage(),e);
                    }
                    return Status.OK_STATUS;
                }
            };

            countdown.schedule();
        }
        // the screen capture job
        Job job = new Job("screenshot") {
            ArrayList<String> aList = new ArrayList<String>();

            // find the graphic devices and process screen shots
            protected IStatus run(IProgressMonitor monitor) {

                // get a list of relevant screen IDs from the store

                // take all screens if there is no preference
                IEclipsePreferences prefs = InstanceScope.INSTANCE
                    .getNode(DocfactoGrabitPlugin.PLUGIN_ID);
                String takeAll2 =
                    prefs.get(DocfactoGrabitPlugin.GRAB_ALL_SCREENS,"true");

                String screenIDStore =
                    prefs.get(DocfactoGrabitPlugin.SCREEN_IDS,"");

                if (screenIDStore.length()>0) {
                    String[] screenIDStoreArray = screenIDStore
                        .split(GrabitPreferencePage.STORE_KEY);

                    for (String id:screenIDStoreArray) {
                        aList.add(id);
                    }
                }

                try {

                    GraphicsEnvironment ge = GraphicsEnvironment
                        .getLocalGraphicsEnvironment();
                    GraphicsDevice[] screens = ge.getScreenDevices();
                    if (!takeAll2.equals("true")) {
                        for (GraphicsDevice screen:screens) {

                            if (aList.contains(screen.getIDstring())) {
                                takeAll = false;
                                break;
                            }

                        }
                    }

                    screenCount = 0;
                    for (GraphicsDevice screen:screens) {

                        if (takeAll||aList.contains(screen.getIDstring())) {
                            GraphicsConfiguration[] bounds = screen
                                .getConfigurations();
                            for (GraphicsConfiguration cg:bounds) {
                                screenCount++;
                                final Rectangle rect = cg.getBounds();
                                final BufferedImage image;
                                image = new Robot().createScreenCapture(rect);
                                InputStream is = GrabitSWTUtil
                                    .getImageInputStream(image);
                                IStorage storage = new ImageStorage(is,
                                    screenCount);
                                final ImageInput input =
                                    new ImageInput(storage);

                                Display.getDefault().syncExec(new Runnable() {
                                    public void run() {
                                        IWorkbenchPage page = PlatformUI
                                            .getWorkbench()
                                            .getActiveWorkbenchWindow()
                                            .getActivePage();

                                        try {
                                            // if a file of the same name is
                                            // opened, rename the input label
                                            IEditorReference[] editorRefs =
                                                page.getEditorReferences();
                                            ArrayList<String> existingNames =
                                                new ArrayList<String>();
                                            for (IEditorReference ref:editorRefs) {
                                                if (ref.getEditorInput() instanceof ImageInput) {

                                                    existingNames.add(ref
                                                        .getEditorInput()
                                                        .getName());
                                                }

                                            }
                                            int nameCount = 0;
                                            while (existingNames.contains(input
                                                .getName())) {
                                                nameCount++;
                                                // create a new name.
                                                String extension =
                                                    DocfactoGrabitPlugin.DEFAULT_EXTENSION;
                                                String prefix =
                                                    input
                                                        .getStorage()
                                                        .getName()
                                                        .substring(
                                                            0,
                                                            input
                                                                .getStorage()
                                                                .getName()
                                                                .lastIndexOf(
                                                                    extension));
                                                input.setName(String
                                                    .format(prefix+"%s"
                                                        +extension,"_"
                                                        +nameCount));
                                            }
                                            ImageEditor openedEditor =
                                                (ImageEditor)page
                                                    .openEditor(
                                                        input,
                                                        DocfactoGrabitPlugin.IMAGE_EDITOR_ID);
                                            openedEditor.getImageCanvas()
                                                .fitCanvas();
                                            openedEditor.getImageCanvas()
                                                .resetStackPos(false);
                                        }
                                        catch (PartInitException e1) {
                                            DocfactoGrabitPlugin
                                                .logException(
                                                    "Grabit: Unable to launch the image viewer "
                                                        +e1.getMessage(),e1);
                                        }
                                        catch (CoreException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                        }
                    }
                }

                catch (AWTException e) {
                    DocfactoGrabitPlugin
                        .logException("Grabit: Camera AWT error "
                            +e.getMessage(),e);
                }

                return Status.OK_STATUS;
            }
        };
        job.schedule(waitTime);

        return null;
    }

}