package com.docfacto.grabit.plugin.handlers;

import java.io.InputStream;

import org.eclipse.core.resources.IStorage;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;

import com.docfacto.grabit.DocfactoGrabitPlugin;

/**
 * Class holds data to be used by the editor input
 * 
 * @author pemi - created Sep 12, 2013
 * @since 2.4
 */
public class ImageStorage implements IStorage {

    private InputStream is;
    private int scrn;

    ImageStorage(InputStream is,int screenNumber) {
        this.is = is;
        this.scrn = screenNumber;
    }

    /**
     * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
     */
    @Override
    public Object getAdapter(Class adapter) {
        return null;
    }

    /**
     * @see org.eclipse.core.resources.IStorage#getContents()
     */
    @Override
    public InputStream getContents() throws CoreException {
        return is;
    }

    /**
     * @see org.eclipse.core.resources.IStorage#getFullPath()
     */
    @Override
    public IPath getFullPath() {

        return null;
    }

    /**
     * @see org.eclipse.core.resources.IStorage#getName()
     */
    @Override
    public String getName() {
        return "Display"+scrn+DocfactoGrabitPlugin.DEFAULT_EXTENSION;
    }

    /**
     * @see org.eclipse.core.resources.IStorage#isReadOnly()
     */
    @Override
    public boolean isReadOnly() {
        return false;
    }

}
