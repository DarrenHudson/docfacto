package com.docfacto.grabit.plugin.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.handlers.RadioState;

/**
 * 
 * RadioHandler processes execution of the grabit dropdown.
 * 
 * @author pemi - created Jul 24, 2013
 * @since 2.4
 */
public class RadioHandler extends AbstractHandler {

    public Object execute(ExecutionEvent event) throws ExecutionException {

        if (HandlerUtil.matchesRadioState(event))
            return null; // do nothing

        String currentState = event.getParameter(RadioState.PARAMETER_ID);

        // update the current state
        HandlerUtil.updateRadioState(event.getCommand(),currentState);

        return null;
    }

}