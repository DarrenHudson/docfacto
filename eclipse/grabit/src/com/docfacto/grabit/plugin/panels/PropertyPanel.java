package com.docfacto.grabit.plugin.panels;

import java.awt.GraphicsEnvironment;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.osgi.service.prefs.BackingStoreException;

import com.docfacto.grabit.DocfactoGrabitPlugin;

//...in simple terms, this class is a Bean in UI form.

/**
 * A {@code org.eclipse.swt.widgets.Composite} with the observable properties.
 * 
 * <p>
 * The UI is updated when the edit mode is set and a property event is fired
 * when changes are made to its state/attributes properties.
 * </p>
 * 
 * @author pemi - created Oct 15, 2013
 * @since 2.4.8
 */
public class PropertyPanel extends Composite {

    private PropertyChangeSupport notifyParent;
    private Composite composite_1;
    private ToolItem tltmNewItem;
    private int editMode;
    private ColourPicker foregroundColourPicker;
    private IEclipsePreferences prefs;
    private int lineWidth;
    private int textSize;
    private String fontType;
    private int lineStyle;
    private boolean textBold;
    private boolean textItalic;
    private boolean textUnderline;
    private RGB theForegroundColour;
    private RGB theBackgroundColour;
    private RGB theTextFGColour;
    private RGB theTextBGColour;
    private ColourPicker backgroundColorPicker;
    private Composite backgroundColorComposite;
    private Composite shape_composite;
    private Composite crop_composite;
    private Composite text_composite;
    private StackLayout stackLayout;

    /** The name of the property change event - text size */
    public static final String TEXT_SIZE = "grabitTextSize";

    /** The name of the property change event - font type */
    public static final String FONT_TYPE = "grabitTextType";

    /** The name of the property change event - line size */
    public static final String LINE_STYLE = "grabitLineStyle";

    /** The name of the property change event - line width */
    public static final String LINE_WIDTH = "grabitLineWidth";

    /** The name of the property change event - text is bold */
    public static final String TEXT_BOLD = "grabitBold";

    /** The name of the property change event - text is italic */
    public static final String TEXT_ITALIC = "grabitItalic";

    /** The name of the property change event - text is underlined */
    public static final String TEXT_UNDERLINE = "grabitUnderline";

    /** The name of the property change event - foreground colour */
    public static final String FG_COLOR = "grabitFGColor";

    /** The name of the property change event - background colour */
    public static final String BG_COLOR = "grabitBGColor";

    /** The name of the property change event - text colour */
    public static final String TEXT_FG_COLOR = "grabitTextFGColor";

    /** The name of the property change event - text background colour */
    public static final String TEXT_BG_COLOR = "grabitTextBGColor";
    

    /**
     * Create the composite.
     * 
     * @param parent - The parent composite
     * @param style - Style to use
     * @since 2.4.8
     */
    public PropertyPanel(Composite parent,int style) {
        super(parent,style);
        setLayout(new FormLayout());
        initValues();
        createParts();        

    }

    /**
     * Creates the parts that make up the property panel
     * 
     * @since 2.4.8
     */
    private void createParts() {

        notifyParent = new PropertyChangeSupport(this);

        Composite composite = new Composite(this,SWT.NONE);
        FormData fd_composite = new FormData();
        fd_composite.top = new FormAttachment(0);
        fd_composite.left = new FormAttachment(0);
        fd_composite.bottom = new FormAttachment(0,33);
        fd_composite.right = new FormAttachment(0,538);
        composite.setLayoutData(fd_composite);
        RowLayout rl_composite = new RowLayout(SWT.HORIZONTAL);
        rl_composite.center = true;
        composite.setLayout(rl_composite);

        Composite composite_2 = new Composite(composite,SWT.NONE);
        composite_2.setLayoutData(new RowData(SWT.DEFAULT,26));
        RowLayout rl_composite_2 = new RowLayout(SWT.HORIZONTAL);
        rl_composite_2.center = true;
        composite_2.setLayout(rl_composite_2);

        Label lblProperties = new Label(composite_2,SWT.HORIZONTAL|SWT.CENTER);
        lblProperties.setLayoutData(new RowData(SWT.DEFAULT,15));
        lblProperties.setText("Properties");

        ToolBar toolBar =
            new ToolBar(composite_2,SWT.FLAT|SWT.RIGHT|SWT.NO_FOCUS);

        tltmNewItem = new ToolItem(toolBar,SWT.NONE);
        tltmNewItem.setImage(DocfactoGrabitPlugin
            .getImage("/icons/pencil_icon.png"));

        Label label = new Label(composite,SWT.SEPARATOR|SWT.VERTICAL);
        label.setLayoutData(new RowData(4,26));

        composite_1 = new Composite(composite,SWT.NONE);
        stackLayout = new StackLayout();
        composite_1.setLayout(stackLayout);

        composite_1.setLayoutData(new RowData(356,24));
        text_composite = new Composite(composite_1,SWT.NONE);
        shape_composite = new Composite(composite_1,SWT.NONE);
        crop_composite = new Composite(composite_1,SWT.NONE);

        composite_2.pack();
        
        Label cropStyleLabel = new Label(crop_composite,SWT.NONE);
        cropStyleLabel.setBounds(0,4,30,19);
        cropStyleLabel.setText("Style");
        
        final Combo cropStyleCombo = new Combo(crop_composite,SWT.READ_ONLY);
        cropStyleCombo.setBounds(30,0,89,24);

        String[] cropStyle = {"normal"
            ,"paper tear"};
        cropStyleCombo.setItems(cropStyle);
        
        Button btnedit = new Button(crop_composite, SWT.NONE);
        btnedit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
//                ImageEdgeDialog edgeDialog = new ImageEdgeDialog(crop_composite.getShell());
//                if(edgeDialog.open() == Window.OK){
//                    prefs.putBoolean(DocfactoGrabitPlugin.STORE_CROP_STYLE_TOP,edgeDialog.isTop());
//                    prefs.putBoolean(DocfactoGrabitPlugin.STORE_CROP_STYLE_BOTTOM,edgeDialog.isBottom());
//                    prefs.putBoolean(DocfactoGrabitPlugin.STORE_CROP_STYLE_RIGHT,edgeDialog.isRight());
//                    prefs.putBoolean(DocfactoGrabitPlugin.STORE_CROP_STYLE_LEFT,edgeDialog.isLeft());
//                    try {
//                        prefs.flush();
//                    }
//                    catch (BackingStoreException e1) {
//                        // TODO Auto-generated catch block
//                        e1.printStackTrace();
//                    }
//                }
            }
        });
        btnedit.setBounds(120, 0, 60, 25);
        btnedit.setText("..edit");
        
        cropStyleCombo.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                // set the value for the style
                String value = cropStyleCombo.getItem(cropStyleCombo.getSelectionIndex());
                

                prefs.put(DocfactoGrabitPlugin.STORE_PROP_CROP_STYLE,
                    value);

                try {
                    prefs.flush();
                }
                catch (BackingStoreException e1) {
                    e1.printStackTrace();
                }

            }
        });
        cropStyleCombo.setText(prefs.get(DocfactoGrabitPlugin.STORE_PROP_CROP_STYLE,cropStyle[0]));
        
        
        
        
        
        Label lineLblStyle = new Label(shape_composite,SWT.NONE);
        lineLblStyle.setBounds(0,4,30,19);
        lineLblStyle.setText("Style");

        final Combo shapeStyleCombo = new Combo(shape_composite,SWT.READ_ONLY);
        shapeStyleCombo.setBounds(30,0,89,24);

        String[] items = {"\u23af\u23afsolid\u23af\u23af"
            ,"\u2015 \u2015 \u2015 \u2015"
            ,"\u00b7 \u00b7 \u00b7 \u00b7 \u00b7 \u00b7 \u00b7 \u00b7 \u00b7"
            ,"\u2015 \u00b7 \u2015 \u00b7 \u2015 \u00b7"
            ,"\u2015 \u00b7  \u00b7 \u2015 \u00b7  \u00b7"};

        shapeStyleCombo.setItems(items);
        shapeStyleCombo.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                // set the value for the style
                lineStyle = shapeStyleCombo.getSelectionIndex()+1;
                Combo lineStyleCombo = new Combo(shape_composite,SWT.READ_ONLY);
                ;
                lineStyleCombo.select(shapeStyleCombo.getSelectionIndex());

                prefs.putInt(DocfactoGrabitPlugin.STORE_PROP_LINE_STYLE,
                    lineStyle);
                notifyParent.firePropertyChange(LINE_STYLE,null,lineStyle);
                setLineStyle(lineStyle);
                try {
                    prefs.flush();
                }
                catch (BackingStoreException e1) {
                    e1.printStackTrace();
                }

            }
        });
        GridData gd_combo1 = new GridData(SWT.LEFT,SWT.CENTER,false,false,1,1);
        gd_combo1.heightHint = 20;
        gd_combo1.widthHint = 84;

        shapeStyleCombo.setLayoutData(gd_combo1);
        shapeStyleCombo.setText(items[lineStyle-1]);

        Label sizeLabel = new Label(shape_composite,SWT.RIGHT);
        sizeLabel.setBounds(110,4,50,19);
        sizeLabel.setText("Size");

        final Spinner line2WidthPicker = new Spinner(shape_composite,SWT.BORDER);
        line2WidthPicker.setBounds(162,0,45,21);
        GridData gd_line2WidthPicker =
            new GridData(SWT.LEFT,SWT.CENTER,false,false,1,1);
        gd_line2WidthPicker.heightHint = 17;
        line2WidthPicker.setLayoutData(gd_line2WidthPicker);
        line2WidthPicker.setMinimum(1);
        line2WidthPicker.setMaximum(100);
        line2WidthPicker.setSelection(lineWidth);

        line2WidthPicker.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                int selection = line2WidthPicker.getSelection();
                prefs.putInt(DocfactoGrabitPlugin.STORE_PROP_LINE_WIDTH,
                    selection);
                setLineWidth(selection);
                notifyParent.firePropertyChange(LINE_WIDTH,null,selection);
                try {
                    prefs.flush();
                }
                catch (BackingStoreException e1) {
                    e1.printStackTrace();
                }
            }
        });

        Composite foregroundColorComposite =
            new Composite(shape_composite,SWT.NONE);
        foregroundColorComposite.setBounds(213,0,63,23);
        foregroundColorComposite.setLayout(new RowLayout(SWT.HORIZONTAL));
        foregroundColorComposite.setLayoutData(new RowData(66,20));

        Label lblColor = new Label(foregroundColorComposite,SWT.RIGHT);
        lblColor.setText("Color:");

        foregroundColourPicker =
            new ColourPicker(foregroundColorComposite, true);
        if (theForegroundColour!=null) {
            Color aColour = new Color(Display.getCurrent(),theForegroundColour);
            foregroundColourPicker.setColour(aColour,true);

        }
        else {
            foregroundColourPicker.setColorLabelToNone();
        }
        
       // add a property change listener to foreground colour widget
        foregroundColourPicker
            .addPropertyChangeListener(new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    String stringRGB = "";
                    if (evt.getPropertyName().equals(ColourPicker.PICKER_COLOR)) {

                        if (evt.getNewValue()!=null) {
                            setTheForegroundColour((RGB)evt.getNewValue());
                            notifyParent.firePropertyChange(FG_COLOR,null,
                                (RGB)evt.getNewValue());
                            stringRGB =
                                ""+theForegroundColour.red+","+
                                    theForegroundColour.green+","+
                                    theForegroundColour.blue;

                        }

                        try {

                            if (foregroundColourPicker.getHasNoColour()||
                                evt.getNewValue()==null) {
                                setTheForegroundColour(null);
                                stringRGB = "null";
                                notifyParent.firePropertyChange(FG_COLOR,null,
                                    null);

                            }
                            prefs.put(
                                DocfactoGrabitPlugin.STORE_PROP_FG_COLOUR,
                                stringRGB);
                            prefs.flush();
                            ;
                        }
                        catch (BackingStoreException e1) {
                            e1.printStackTrace();
                        }

                    }

                }

            });

        backgroundColorComposite = new Composite(shape_composite,SWT.NONE);
        backgroundColorComposite.setBounds(282,0,63,23);
        backgroundColorComposite.setLayout(new RowLayout(SWT.HORIZONTAL));
        backgroundColorComposite.setLayoutData(new RowData(61,20));

        Label lblFill = new Label(backgroundColorComposite,SWT.RIGHT);
        lblFill.setLayoutData(new RowData(32,SWT.DEFAULT));
        lblFill.setText("Fill:");

        backgroundColorPicker =
            new ColourPicker(backgroundColorComposite, true);
        if (theBackgroundColour!=null) {
            Color aColour = new Color(Display.getCurrent(),theBackgroundColour);
            backgroundColorPicker.setColour(aColour,true);

        }
        else {
            backgroundColorPicker.setColorLabelToNone();
        }

        // add a property change listener to background colour widget
        backgroundColorPicker
            .addPropertyChangeListener(new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    String stringRGB = "";
                    if (evt.getPropertyName().equals(ColourPicker.PICKER_COLOR)) {

                        if (evt.getNewValue()!=null) {
                            setTheBackgroundColour((RGB)evt.getNewValue());
                            notifyParent.firePropertyChange(BG_COLOR,null,
                                (RGB)evt.getNewValue());
                            stringRGB =
                                ""+theBackgroundColour.red+","+
                                    theBackgroundColour.green+","+
                                    theBackgroundColour.blue;

                        }

                        try {

                            if (backgroundColorPicker.getHasNoColour()||
                                evt.getNewValue()==null) {
                                setTheBackgroundColour(null);
                                stringRGB = "null";
                                notifyParent.firePropertyChange(BG_COLOR,null,
                                    null);

                            }
                            prefs.put(
                                DocfactoGrabitPlugin.STORE_PROP_BG_COLOUR,
                                stringRGB);
                            prefs.flush();
                            ;
                        }
                        catch (BackingStoreException e1) {
                            e1.printStackTrace();
                        }

                    }

                }

            });

        composite_2.pack();
        Label textStyleLabel = new Label(text_composite,SWT.NONE);
        textStyleLabel.setBounds(0,4,30,19);
        textStyleLabel.setText("Style");

        String[] fontArray =
            GraphicsEnvironment.getLocalGraphicsEnvironment()
                .getAvailableFontFamilyNames();

        final Combo fontCombo = new Combo(text_composite,SWT.READ_ONLY);
        fontCombo.setBounds(28,0,88,23);
        fontCombo.setItems(fontArray);
        fontCombo.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                // set the value for the style

                fontType = fontCombo.getItem(fontCombo.getSelectionIndex());
                notifyParent.firePropertyChange(FONT_TYPE,null,fontType);
                prefs.put(DocfactoGrabitPlugin.STORE_PROP_FONT_TYPE,fontType);
                try {
                    prefs.flush();
                }
                catch (BackingStoreException e1) {
                    e1.printStackTrace();
                }

            }
        });

        fontCombo.setText(fontType);
        final Spinner textSizeSpinner = new Spinner(text_composite,SWT.BORDER);
        textSizeSpinner.setBounds(154,0,42,21);
        GridData gd_TextSizePicker =
            new GridData(SWT.LEFT,SWT.CENTER,false,false,1,1);
        gd_TextSizePicker.heightHint = 17;
        textSizeSpinner.setLayoutData(gd_TextSizePicker);
        textSizeSpinner.setMinimum(6);
        textSizeSpinner.setMaximum(100);
        textSizeSpinner.setSelection(textSize);
        textSizeSpinner.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                // set the value for the style

                textSize = textSizeSpinner.getSelection();
                prefs
                    .putInt(DocfactoGrabitPlugin.STORE_PROP_FONT_SIZE,textSize);
                notifyParent.firePropertyChange(TEXT_SIZE,null,textSize);

                try {
                    prefs.flush();
                }
                catch (BackingStoreException e1) {
                    e1.printStackTrace();
                }

            }
        });

        ToolBar text_fontToolbar =
            new ToolBar(text_composite,SWT.FLAT|SWT.RIGHT);

        text_fontToolbar.setBounds(202,0,66,24);
        final ToolItem tltmB = new ToolItem(text_fontToolbar,SWT.CHECK);
        tltmB.setSelection(textBold);
        tltmB.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                textBold = tltmB.getSelection();
                prefs.putBoolean(DocfactoGrabitPlugin.STORE_PROP_TEXT_IS_BOLD,
                    textBold);
                notifyParent.firePropertyChange(TEXT_BOLD,null,textBold);
                try {
                    prefs.flush();
                }
                catch (BackingStoreException e1) {
                    e1.printStackTrace();
                }
            }
        });
        Image boldIcon = DocfactoGrabitPlugin.getImage("/icons/bold.png");
        Image underlineIcon =
            DocfactoGrabitPlugin.getImage("/icons/underline.png");

        Image italicIcon = DocfactoGrabitPlugin.getImage("/icons/italic.png");
        tltmB.setImage(boldIcon);

        final ToolItem tltmI = new ToolItem(text_fontToolbar,SWT.CHECK);
        tltmI.setSelection(textItalic);
        tltmI.setImage(italicIcon);
        tltmI.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                textItalic = tltmI.getSelection();
                prefs.putBoolean(
                    DocfactoGrabitPlugin.STORE_PROP_TEXT_IS_ITALIC,textItalic);
                notifyParent.firePropertyChange(TEXT_ITALIC,null,textItalic);
                try {
                    prefs.flush();
                }
                catch (BackingStoreException e1) {
                    e1.printStackTrace();
                }
            }
        });

        final ToolItem tltmU = new ToolItem(text_fontToolbar,SWT.CHECK);
        tltmU.setSelection(textUnderline);
        tltmU.setImage(underlineIcon);
        Label lblSize = new Label(text_composite,SWT.NONE);
        lblSize.setBounds(127,4,26,14);
        lblSize.setText("Size");
        tltmU.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {

                textUnderline = tltmU.getSelection();
                prefs.putBoolean(
                    DocfactoGrabitPlugin.STORE_PROP_TEXT_IS_UNDERLINE,
                    textUnderline);
                notifyParent.firePropertyChange(TEXT_UNDERLINE,null,
                    textUnderline);
                try {
                    prefs.flush();
                }
                catch (BackingStoreException e1) {
                    e1.printStackTrace();
                }
            }
        });

        Composite textForegroundColorComposite =
            new Composite(text_composite,SWT.NONE);
        textForegroundColorComposite.setBounds(274,0,72,23);
        textForegroundColorComposite.setLayout(new RowLayout(SWT.HORIZONTAL));
        textForegroundColorComposite.setLayoutData(new RowData(66,20));

        Label textlblColor = new Label(textForegroundColorComposite,SWT.RIGHT);
        textlblColor.setText("Color:");
        final ColourPicker textForegroundColourPicker =
            new ColourPicker(textForegroundColorComposite, false);
        if (theTextFGColour!=null) {
            Color aColour = new Color(Display.getCurrent(),theTextFGColour);
            textForegroundColourPicker.setColour(aColour,true);

        }
        else {
            textForegroundColourPicker.setColorLabelToNone();

        }

     // add a property change listener to text colour widget
        textForegroundColourPicker
            .addPropertyChangeListener(new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    String stringRGB = "";
                    if (evt.getPropertyName().equals(ColourPicker.PICKER_COLOR)) {

                        if (evt.getNewValue()!=null) {
                            setTheTextFGColour((RGB)evt.getNewValue());
                            notifyParent.firePropertyChange(TEXT_FG_COLOR,null,
                                (RGB)evt.getNewValue());
                            stringRGB =
                                ""+theTextFGColour.red+","+
                                    theTextFGColour.green+","+
                                    theTextFGColour.blue;

                        }
                        try {
                            if (textForegroundColourPicker.getHasNoColour()||
                                evt.getNewValue()==null) {
                                setTheTextFGColour(null);
                                stringRGB = "null";
                            }

                            prefs.put(
                                DocfactoGrabitPlugin.STORE_PROP_TEXT_FG_COLOUR,
                                stringRGB);
                            prefs.flush();
                        }
                        catch (BackingStoreException e1) {
                            e1.printStackTrace();
                        }

                    }

                }

            });
        
    }

    /**
     * Gets property values from the preference store
     * 
     * @since 2.4.8
     */
    private void initValues() {
        prefs = InstanceScope.INSTANCE.getNode(DocfactoGrabitPlugin.PLUGIN_ID);
        lineWidth = prefs.getInt(DocfactoGrabitPlugin.STORE_PROP_LINE_WIDTH,1);
        textSize = prefs.getInt(DocfactoGrabitPlugin.STORE_PROP_FONT_SIZE,8);

        fontType = prefs.get(DocfactoGrabitPlugin.STORE_PROP_FONT_TYPE,"Arial");
        lineStyle =
            prefs.getInt(DocfactoGrabitPlugin.STORE_PROP_LINE_STYLE,
                SWT.LINE_SOLID);
        textBold =
            prefs
                .getBoolean(DocfactoGrabitPlugin.STORE_PROP_TEXT_IS_BOLD,false);
        textItalic =
            prefs.getBoolean(DocfactoGrabitPlugin.STORE_PROP_TEXT_IS_ITALIC,
                false);
        textUnderline =
            prefs.getBoolean(DocfactoGrabitPlugin.STORE_PROP_TEXT_IS_UNDERLINE,
                false);

        // get the colours
        String[] fcl =
            prefs.get(DocfactoGrabitPlugin.STORE_PROP_FG_COLOUR,"0,0,0").split(
                ",");
        String[] bcl =
            prefs.get(DocfactoGrabitPlugin.STORE_PROP_BG_COLOUR,"255,255,255")
                .split(",");
        String[] tfcl =
            prefs.get(DocfactoGrabitPlugin.STORE_PROP_TEXT_FG_COLOUR,"0,0,0")
                .split(",");
        String[] tbcl =
            prefs.get(DocfactoGrabitPlugin.STORE_PROP_TEXT_BG_COLOUR,
                "255,255,255").split(",");

        if (fcl.length==3) {
            theForegroundColour =
                new RGB(Integer.valueOf(fcl[0]),Integer.valueOf(fcl[1]),
                    Integer.valueOf(fcl[2]));
        }
        else if (fcl.equals("null")||fcl.length!=3) {
            theForegroundColour = null;

        }
        if (bcl.length==3) {
            theBackgroundColour =
                new RGB(Integer.valueOf(bcl[0]),Integer.valueOf(bcl[1]),
                    Integer.valueOf(bcl[2]));
        }
        else if (bcl.equals("null")||bcl.length!=3) {
            theBackgroundColour = null;

        }
        if (tfcl.length==3) {
            theTextFGColour =
                new RGB(Integer.valueOf(tfcl[0]),Integer.valueOf(tfcl[1]),
                    Integer.valueOf(tfcl[2]));
        }
        else if (tfcl.equals("null")||tfcl.length!=3) {
            theTextFGColour = null;

        }
        if (tbcl.length==3) {
            theTextBGColour =
                new RGB(Integer.valueOf(tbcl[0]),Integer.valueOf(tbcl[1]),
                    Integer.valueOf(tbcl[2]));
        }
        else if (tbcl.equals("null")||tbcl.length!=3) {
            theTextBGColour = null;

        }

    }

    /**
     * Method sets the edit mode value and updates the UI to reflect its mode
     * 
     * @param editMode - The edit mode
     * @since 2.4.8
     */
    public void setPanelMode(int editMode) {
        if (editMode==DocfactoGrabitPlugin.TEXT_MODE) {
            stackLayout.topControl = text_composite;
            text_composite.setVisible(true);
            shape_composite.setVisible(false);
            crop_composite.setVisible(false);

        }else if (editMode==DocfactoGrabitPlugin.IMAGE_CROP_MODE) {
            stackLayout.topControl = crop_composite;
            text_composite.setVisible(false);
            shape_composite.setVisible(false);
            crop_composite.setVisible(true);

        }
        else {
            stackLayout.topControl = shape_composite;
            shape_composite.setVisible(true);
            text_composite.setVisible(false);
            crop_composite.setVisible(false);
        }

        tltmNewItem.setImage(DocfactoGrabitPlugin.getModeImage(editMode));
        if (editMode==DocfactoGrabitPlugin.LINE_MODE||
            editMode==DocfactoGrabitPlugin.PENCIL_MODE) {
            backgroundColorComposite.setVisible(false);

        }
        else {
            backgroundColorComposite.setVisible(true);

        }

        this.editMode = editMode;

    }

    /**
     * @see org.eclipse.swt.widgets.Composite#checkSubclass()
     */
    @Override
    protected void checkSubclass() {
        // Disable the check that prevents subclassing of SWT components
    }

    /**
     * Method for adding the property change listener
     * 
     * @param l - the property change listener to be removed
     * @since 2.4.8
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        notifyParent.addPropertyChangeListener(l);
    }

    /**
     * Method for removing the property change listener
     * 
     * @param l - the property change listener to be removed
     * @since 2.4.8
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        notifyParent.removePropertyChangeListener(l);

    }

    /**
     * Returns editMode.
     * 
     * @return the editMode
     * @since 2.4.8
     */
    public int getEditMode() {
        return editMode;
    }

    /**
     * Returns lineWidth.
     * 
     * @return the lineWidth
     * @since 2.4.8
     */
    public int getLineWidth() {
        return lineWidth;
    }

    /**
     * Sets lineWidth.
     * 
     * @param lineWidth the lineWidth value
     * @since 2.4.8
     */
    public void setLineWidth(int lineWidth) {
        this.lineWidth = lineWidth;
    }

    /**
     * Returns textSize.
     * 
     * @return the textSize
     * @since 2.4.8
     */
    public int getTextSize() {
        return textSize;
    }

    /**
     * Sets textSize.
     * 
     * @param textSize the textSize value
     * @since 2.4.8
     */
    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }

    /**
     * Returns fontType.
     * 
     * @return the fontType
     * @since 2.4.8
     */
    public String getFontType() {
        return fontType;
    }

    /**
     * Sets fontType.
     * 
     * @param fontType the fontType value
     * @since 2.4.8
     */
    public void setFontType(String fontType) {
        this.fontType = fontType;
    }

    /**
     * Returns lineStyle.
     * 
     * @return the lineStyle
     * @since 2.4.8
     */
    public int getLineStyle() {
        return lineStyle;
    }

    /**
     * Sets lineStyle.
     * 
     * @param lineStyle the lineStyle value
     * @since 2.4.8
     */
    public void setLineStyle(int lineStyle) {
        this.lineStyle = lineStyle;
    }

    /**
     * Returns textBold.
     * 
     * @return the textBold
     * @since 2.4.8
     */
    public boolean isTextBold() {
        return textBold;
    }

    /**
     * Sets textBold.
     * 
     * @param textBold the textBold value
     * @since 2.4.8
     */
    public void setTextBold(boolean textBold) {
        this.textBold = textBold;
    }

    /**
     * Returns textItalic.
     * 
     * @return the textItalic
     * @since 2.4.8
     */
    public boolean isTextItalic() {
        return textItalic;
    }

    /**
     * Sets textItalic.
     * 
     * @param textItalic the textItalic value
     * @since 2.4.8
     */
    public void setTextItalic(boolean textItalic) {
        this.textItalic = textItalic;
    }

    /**
     * Returns textUnderline.
     * 
     * @return the textUnderline
     * @since 2.4.8
     */
    public boolean isTextUnderline() {
        return textUnderline;
    }

    /**
     * Sets textUnderline.
     * 
     * @param textUnderline the textUnderline value
     * @since 2.4.8
     */
    public void setTextUnderline(boolean textUnderline) {
        this.textUnderline = textUnderline;
    }

    /**
     * Returns theForegroundColour.
     * 
     * @return the theForegroundColour
     * @since 2.4.8
     */
    public RGB getTheForegroundColour() {
        return theForegroundColour;
    }

    /**
     * Sets theForegroundColour.
     * 
     * @param theForegroundColour the theForegroundColour value
     * @since 2.4.8
     */
    public void setTheForegroundColour(RGB theForegroundColour) {
        this.theForegroundColour = theForegroundColour;
    }

    /**
     * Returns theBackgroundColour.
     * 
     * @return the theBackgroundColour
     * @since 2.4.8
     */
    public RGB getTheBackgroundColour() {
        return theBackgroundColour;
    }

    /**
     * Sets theBackgroundColour.
     * 
     * @param theBackgroundColour the theBackgroundColour value
     * @since 2.4.8
     */
    public void setTheBackgroundColour(RGB theBackgroundColour) {
        this.theBackgroundColour = theBackgroundColour;
    }

    /**
     * Returns theTextFGColour.
     * 
     * @return the theTextFGColour
     * @since 2.4.8
     */
    public RGB getTheTextFGColour() {
        return theTextFGColour;
    }

    /**
     * Sets theTextFGColour.
     * 
     * @param theTextFGColour the theTextFGColour value
     * @since 2.4.8
     */
    public void setTheTextFGColour(RGB theTextFGColour) {
        this.theTextFGColour = theTextFGColour;
    }

    /**
     * Returns theTextBGColour.
     * 
     * @return the theTextBGColour
     * @since 2.4.8
     */
    public RGB getTheTextBGColour() {
        return theTextBGColour;
    }

    /**
     * Sets theTextBGColour.
     * 
     * @param theTextBGColour the theTextBGColour value
     * @since 2.4.8
     */
    public void setTheTextBGColour(RGB theTextBGColour) {
        this.theTextBGColour = theTextBGColour;
    }

    /**
     * Sets editMode.
     * 
     * @param editMode the editMode value
     * @since 2.4.8
     */
    public void setEditMode(int editMode) {
        this.editMode = editMode;
    }
}
