package com.docfacto.grabit.plugin.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.Timer;

/**
 * Countdown panel
 * <p>
 * The panel creates a vertical {@code JProgressBar} which counts down per
 * second
 * </p>
 * 
 * @author pemi - created Jul 23, 2013
 * @since 2.4
 */
public class Countdown extends JPanel {
    /*
     * Default serialization
     */
    private static final long serialVersionUID = 1L;
    JProgressBar progressBar;
    Timer timer;

    /**
     * Constructor takes an int representing the wait time in milliseconds
     * 
     * @param waitTime - count time (in milliseconds)
     */
    public Countdown(final int waitTime) {

        progressBar = new JProgressBar(JProgressBar.VERTICAL,0,waitTime);
        progressBar.setValue(waitTime);
        ActionListener listener = new ActionListener() {
            int counter = waitTime;

            public void actionPerformed(ActionEvent ae) {
                counter = counter-1000;
                progressBar.setValue(counter);

                if (counter<1000) {
                    timer.stop();
                }
            }
        };
        timer = new Timer(1000,listener);
        timer.start();
        add("counting down",progressBar);
    }

}
