package com.docfacto.grabit.plugin.panels;

/**
 * 
 * @since 2.5
 */
public enum GrabitBorder {
    TOP,
    BOTTOM,
    LEFT,
    RIGHT,
    NE,
    NW,
    SW,
    SE,
    CENTRE,
    MOUSE_DOWN, OUT_OF_BOUNDS
}