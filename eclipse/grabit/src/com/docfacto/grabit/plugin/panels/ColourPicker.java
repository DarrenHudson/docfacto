package com.docfacto.grabit.plugin.panels;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import com.docfacto.core.utils.SWTUtils;
import com.docfacto.core.widgets.AbstractColorPicker;

/**
 * A widget for setting colour properties.
 * <p>
 * This widget can be used to create a colour picker whose composite can be set to a specified colour.
 * In the GUI, during user interaction, a popup is displayed showing the a grid of colours.
 * On colour selection, the popup is disposed and the widget's colour is set.
 * 
 * Colour selection causes the widget to fire a property change event.
 * Any class/composite willing to use this widget should handle the fired event.
 * </p>
 * @see com.docfacto.core.widgets.AbstractColorPicker
 * 
 * @author pemi - created Oct 25, 2013
 * @since 2.4.8
 */
public class ColourPicker extends AbstractColorPicker {
    
    /** The name of the property change event */
    public static final String PICKER_COLOR = "color-picker-foreground";
    
    /** The display to use */
    public final Display display;
    
    private Color pickerForeground;
    private Color pickerBackground;
    private RowData theColourLabelData;
    private PropertyChangeSupport notifyParent;
    
    

    /**
     * Constructor for adding composite to a given parent composite
     * 
     * @param parent - The parent composite 
     * @param showNone - allow the 'no colour' option
     * @since 2.4.8
     */
    public ColourPicker(Composite parent, boolean showNone) {
        super(parent, showNone);
        display = parent.getDisplay();
        notifyParent = new PropertyChangeSupport(this);
    }

    /**
     * @see com.docfacto.core.widgets.AbstractColorPicker#createCompositePart(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Composite createCompositePart(Composite parent) {
        // Create a container for the widget
        Composite composite = new Composite(parent,SWT.NONE);
        // Same colour as the toolbar
        composite.setBackground(parent.getBackground());

        RowLayout rowLayout = new RowLayout(SWT.HORIZONTAL);
        rowLayout.marginHeight = 0;
        rowLayout.marginBottom = 0;
        rowLayout.marginTop = 0;
        rowLayout.marginLeft = 0;
        rowLayout.marginRight = 0;
        composite.setLayout(rowLayout);

        return composite;
    }

    /**
     * @see com.docfacto.core.widgets.AbstractColorPicker#createColourLabel(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Label createColourLabel(Composite parent) {
        Label label = new Label(parent,SWT.NONE);
        theColourLabelData = new RowData();
        theColourLabelData.width = 18;
        theColourLabelData.height = 16;
        label.setLayoutData(theColourLabelData);
        return label;
    }

    /**
     * @see com.docfacto.core.widgets.AbstractColorPicker#setVisible(boolean)
     */
    @Override
    protected void setVisible(boolean visible) {
        final Shell shell = getShell();
        final Label theColourLabel = super.getColorLabel();
        if (visible) {
            Rectangle rect = theColourLabel.getBounds();
            Point pt =
                theColourLabel.getParent().toDisplay(new Point(rect.x,rect.y));

            // pack to work out the size based on all of the children
            // components.
            shell.pack();
            // Removed the setting of bounds, size, everything should be
            // calculated relatively
            shell.setLocation(pt.x-300,pt.y-220);
            shell.setVisible(true);
            setOriginalColour(theColourLabel.getBackground());

        }
        else {
            shell.setVisible(false);
            RGB newRGB =
                this.getForegroundColour()!=null ? getForegroundColour()
                    .getRGB() : null;

            notifyParent.firePropertyChange(
                PICKER_COLOR,null,newRGB);
        }

    }

    /**
     * @see com.docfacto.core.widgets.AbstractColorPicker#setColorLabelToNone()
     */
    @Override
    protected void setColorLabelToNone() {
        Label theColourLabel = super.getColorLabel();
        theColourLabel.setBackground(SWTUtils.getColor(SWT.COLOR_WHITE));
        theColourLabel.setImage(PlatformUI.getWorkbench().getSharedImages().
            getImageDescriptor(ISharedImages.IMG_TOOL_DELETE).createImage());
        
        // variable 'notifyParent' is null until after initialization
        if(notifyParent!=null ){
            notifyParent.firePropertyChange(
                PICKER_COLOR,getOriginalColour(),null);
        }

    }

    /**
     * @see com.docfacto.core.widgets.AbstractColorPicker#setColourLabelToColour(org.eclipse.swt.graphics.Color)
     */
    @Override
    protected void setColourLabelToColour(Color color) {
        Label theColourLabel = super.getColorLabel();
        theColourLabel.setImage(null);
        theColourLabel.setLayoutData(theColourLabelData);
        theColourLabel.setBackground(color);
        pickerForeground = color;

    }

    /**
     * @see com.docfacto.core.widgets.AbstractColorPicker#getForegroundColour()
     */
    @Override
    public Color getForegroundColour() {
        // TODO Auto-generated method stub
        return pickerForeground;
    }

    /**
     * @see com.docfacto.core.widgets.AbstractColorPicker#getBackgroundColour()
     */
    @Override
    public Color getBackgroundColour() {
        // TODO Auto-generated method stub
        return this.pickerBackground;
    }

    /**
     * Method for adding property change listener
     * @param l - the property change listener to be added
     * @since 2.4.8
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        notifyParent.addPropertyChangeListener(l);
    }

    /**
     * Method for removing the property change listener
     * @param l - the property change listener to be removed
     * @since 2.4.8
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        notifyParent.removePropertyChangeListener(l);

    }

}
