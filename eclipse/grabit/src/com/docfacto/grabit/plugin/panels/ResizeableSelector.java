package com.docfacto.grabit.plugin.panels;

import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;

public class ResizeableSelector {

    GrabitBorder resizeAction;


    public ResizeableSelector(){
//        resizeAction = pointCloseToLine(mousePoint,6);

    }

    
    
    private boolean pointCloseToPoint(Point a, Point b, int nearness){
        
        if((Math.abs(a.x - b.x)<nearness)&&(Math.abs(a.y - b.y)<nearness)){
          return true;  
        }
         
        return false;
    }
    
    public GrabitBorder pointCloseToLine(Point a, Rectangle rectangle, int nearness){
        
        int x1 = rectangle.x;
        int y1 = rectangle.y;
        int x2 = x1+rectangle.width;
        int y2 = y1+rectangle.height;
        
        Point cornerNE = new Point(x2, y1);
        Point cornerNW = new Point(x1, y1);
        Point cornerSE = new Point(x2, y2);
        Point cornerSW = new Point(x1, y2);
        
        if(pointCloseToPoint(cornerSW, a, nearness)){
            return GrabitBorder.SW;   
        }else if(pointCloseToPoint(cornerSE, a, nearness)){
            return GrabitBorder.SE;   
        }else if(pointCloseToPoint(cornerNW, a, nearness)){
            return GrabitBorder.NW;   
        }else if(pointCloseToPoint(cornerNE, a, nearness)){
            return GrabitBorder.NE;   
        }else if(a.x-x1<nearness) {
            return GrabitBorder.LEFT;
        }else if(Math.abs(a.y-y2)<nearness) {
            return GrabitBorder.BOTTOM;
        }else if(Math.abs(a.x-x2)<nearness) {
            return GrabitBorder.RIGHT;
        }else if(a.y-y1<nearness) {
            return GrabitBorder.TOP;
        }else if(rectangle.contains(a)) {
            return GrabitBorder.CENTRE;
        }
         
        return null;
    }
    
   // private void setDefaultCursor() {setCursor(new Cursor(getDisplay(),SWT.CURSOR_HAND));
//    
//    
//    }


    
    
    
    
    
    

}
