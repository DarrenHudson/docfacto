package com.docfacto.links.plugin.visitors;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.TagElement;

import com.docfacto.core.utils.ASTUtils;
import com.docfacto.core.utils.EclipseTagletUtils;
import com.docfacto.javadoc.TagParser;
import com.docfacto.links.plugin.DocfactoLinksPlugin;
import com.docfacto.links.plugin.markers.LinksMarker;

/**
 * If Something has changed in the java development environment, then lets check
 * it
 * 
 * @author dhudson - created 28 Feb 2013
 * @since 2.2
 */
public class LinksASTVisitor extends ASTVisitor {

    private final CompilationUnit theCompilationUnit;

    /**
     * Constructor.
     * 
     * @param cu to process
     */
    public LinksASTVisitor(ICompilationUnit cu) {

        theCompilationUnit = ASTUtils.getCompilationUnitFor(cu);

        theCompilationUnit.accept(this);
        // TODO: Use SharedASTProvider..

        // SharedASTProvider.getAST(cu.getType(arg0).getTypeRoot(),
        // SharedASTProvider.WAIT_ACTIVE_ONLY,null);

        try {
            cu.getResource().deleteMarkers(LinksMarker.LINKS_MARKER_ID, true, IResource.DEPTH_INFINITE);
        }
        catch (CoreException ex) {
            DocfactoLinksPlugin.logException("Unable to clear markers",ex);
        }
    }

    /**
     * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.TagElement)
     */
    @Override
    public boolean visit(TagElement node) {
        if (DocfactoLinksPlugin.DOCFACTO_LINK_TAG_NAME.equals(node
            .getTagName())) {
            processLinksTag(node);
        }
        return true;
    }

    /**
     * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.Javadoc)
     */
    @Override
    public boolean visit(Javadoc node) {
        // If you don't have this, then Tag Elements are not visited, default
        // {@code ASTVisitor} must have this method set to false
        return true;
    }

    /**
     * Process the link tag..
     * 
     * @param tagElement
     * @since 2.2
     */
    private void processLinksTag(TagElement tagElement) {
        // REMOVE:
        System.out.println("LinksASTVisitor Process Link Tag "+
            EclipseTagletUtils.createTagParser(tagElement));

        TagParser parser = EclipseTagletUtils.createTagParser(tagElement);

        if (!parser.hasAttribute(TagParser.URI_ATTRIBUTE)) {
            // Add marker for missing uri attribute
            new LinksMarker(theCompilationUnit,tagElement,
                "Missing URI attribute");
            return;
        }

        // We have a URI so lets process it..
        // 1. Does it exist
        // 2. Does the file have a link back
    }
}
