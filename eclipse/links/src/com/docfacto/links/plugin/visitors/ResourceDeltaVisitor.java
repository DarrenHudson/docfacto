package com.docfacto.links.plugin.visitors;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.runtime.CoreException;

/**
 * Delta resource listener, fire the correct events to the links engine 
 *
 * @author dhudson - created 27 Feb 2013
 * @since 2.2
 */
public class ResourceDeltaVisitor implements IResourceDeltaVisitor {

    /**
     * @see org.eclipse.core.resources.IResourceDeltaVisitor#visit(org.eclipse.core.resources.IResourceDelta)
     */
    @Override
    public boolean visit(IResourceDelta delta) throws CoreException {
        //only interested in changed resources (not added or removed)
        if (delta.getKind() != IResourceDelta.CHANGED) {
           return true;
        }
        
        //only interested in content changes
        if ((delta.getFlags() & IResourceDelta.CONTENT) == 0) {
           return true;
        }
        
        IResource resource = delta.getResource();
        
        if(resource.getType() == IResource.FILE) {
            // Fire File change ..
            //REMOVE:
            System.out.println("File has changed " + resource);
        }
        
        if(resource.getType() == IResource.FOLDER) {
            // Fire folder change
            System.out.println("Folder has changed " + resource);
        }
        
        //only interested in files with the "txt" extension
//        if (resource.getType() == IResource.FILE && 
//         "txt".equalsIgnoreCase(resource.getFileExtension())) {
//           changed.add(resource);
//        }
        return true;
    }

}
