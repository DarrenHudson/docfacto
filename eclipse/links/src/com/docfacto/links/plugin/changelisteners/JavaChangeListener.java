package com.docfacto.links.plugin.changelisteners;

import org.eclipse.jdt.core.ElementChangedEvent;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IElementChangedListener;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaElementDelta;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaModelException;

import com.docfacto.links.plugin.DocfactoLinksPlugin;
import com.docfacto.links.plugin.visitors.LinksASTVisitor;

/**
 * Listen to Java Changes ...
 * 
 * <p>
 * {@docfacto.system <a href="http://svn.objectteams.org/ot/tags/LAST_CVS_VERSION/topprax/src/org.eclipse.jdt.ui_2_1_1/ui/org/eclipse/jdt/ui/JavaElementContentProvider.java"></a> }
 * </p>
 * 
 * @author dhudson - created 27 Feb 2013
 * @since 2.2
 */
public class JavaChangeListener implements IElementChangedListener {

    /**
     * @see org.eclipse.jdt.core.IElementChangedListener#elementChanged(org.eclipse.jdt.core.ElementChangedEvent)
     */
    @Override
    public void elementChanged(ElementChangedEvent event) {
        try {
            processDelta(event.getDelta());
        }
        catch (JavaModelException ex) {
            DocfactoLinksPlugin.logException("Unable to process delta change",
                ex);
        }
    }

    /**
     * Look for changes and notify the correct engines if required.
     * 
     * @param delta event to process
     * @since 2.2
     */
    private void processDelta(IJavaElementDelta delta)
    throws JavaModelException {
        
        int kind = delta.getKind();
        IJavaElement element = delta.getElement();

        if (element!=null&&
            element.getElementType()==IJavaElement.COMPILATION_UNIT&&
            !isOnClassPath((ICompilationUnit)element)) {
            // Its not for us
            return;
        }
        
        if( kind == IJavaElementDelta.REMOVED) {
            // Class file removed, so is there a documentation link to it?
            // File a Links Event ... element.getCorrespondingResource();
            return;
        }
        
        if(kind == IJavaElementDelta.CHANGED) {
            System.out.println(" --> " +element.getClass().getName());
            
            if(element instanceof ICompilationUnit) {
               // File the Links event ((ICompilationUnit)element).
                //REMOVE:
                System.out.println("JavaChangeListener : " +delta);
                new LinksASTVisitor((ICompilationUnit)element);
            }
        }
    }

    /**
     * Check to see if the thing that has changed is in an open project and on
     * the classpath
     * 
     * @param element to look for
     * @return true, if class on the class path
     * @throws JavaModelException
     * @since 2.2
     */
    private boolean isOnClassPath(ICompilationUnit element)
    throws JavaModelException {
        IJavaProject project = element.getJavaProject();
        if (project==null||!project.exists()) {
            return false;
        }
        return project.isOnClasspath(element);
    }
}
