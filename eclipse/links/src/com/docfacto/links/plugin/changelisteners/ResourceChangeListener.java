package com.docfacto.links.plugin.changelisteners;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

/**
 * Links resource change listener. Listen to events and create the correct Links
 * events
 * 
 * <p>
 * {@docfacto.system <a href=
 * "http://www.eclipse.org/articles/Article-Resource-deltas/resource-deltas.html"
 * ></a> }
 * </p>
 * 
 * @author dhudson - created 27 Feb 2013
 * @since 2.2
 */
public class ResourceChangeListener implements IResourceChangeListener {

    // Remove at some point, list listen for this folder
    private static final IPath DOC_PATH = new Path("Links Plugin/doc");

    /**
     * @see org.eclipse.core.resources.IResourceChangeListener#resourceChanged(org.eclipse.core.resources.IResourceChangeEvent)
     */
    @Override
    public void resourceChanged(IResourceChangeEvent event) {

        if (event.getType()!=IResourceChangeEvent.POST_CHANGE) {
            return;
        }

        IResourceDelta rootDelta = event.getDelta();

        IResourceDelta docDelta = rootDelta.findMember(DOC_PATH);
        if (docDelta==null) {
            // REMOVE:
            System.out.println("ResourceChangeListener "+event);
            return;
        }
    }

}
