/*
 * @author dhudson -
 * Created 22 Jan 2013 : 11:11:22
 */

package com.docfacto.links.plugin.markers;

import java.util.HashMap;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.TagElement;

import com.docfacto.links.plugin.DocfactoLinksPlugin;

/**
 * Create a market for a links issue.
 * 
 * @author dhudson - created 22 Jan 2013
 * @since 2.2
 */
public class LinksMarker {

    public static final String MARKER_ID = "com.docfacto.links.marker";
    public static final String LINKS_MARKER_KEY = "docfacto.links";
    public static final String LINKS_MARKER_ID =
        "org.eclipse.viatra2.loaders.linksmarker";

    /**
     * Constructor.
     * 
     * @param cu being processed
     * @param element which is in error
     * @param message to display
     */
    public LinksMarker(CompilationUnit cu,TagElement element,String message) {
        
        //REMOVE:
        System.out.println("New Links Marker..");
        
        IResource resource = cu.getJavaElement().getResource();

        try {
            IMarker marker = resource.createMarker(LINKS_MARKER_ID);

            final HashMap<String,Object> attributes =
                new HashMap<String,Object>(7);
            attributes.put(IMarker.PRIORITY,IMarker.PRIORITY_NORMAL);
            attributes.put(IMarker.CHAR_START,element.getStartPosition());
            attributes.put(IMarker.CHAR_END,element.getLength());
            attributes.put(IMarker.LINE_NUMBER,
                cu.getLineNumber(element.getStartPosition()));
            attributes.put(IMarker.MESSAGE,message);
            attributes.put(IMarker.USER_EDITABLE,new Boolean(false));
            attributes.put(LINKS_MARKER_KEY,new Boolean(true));
            attributes.put(IMarker.SEVERITY,IMarker.SEVERITY_WARNING);

            marker.setAttributes(attributes);
           
            //REMOVE:
            System.out.println("start " + element.getStartPosition() + " " + element.getLength() + " " +cu.getLineNumber(element.getStartPosition()));
        }
        catch (CoreException ex) {
            DocfactoLinksPlugin.logException("Unable to create marker",ex);
        }
    }
}
