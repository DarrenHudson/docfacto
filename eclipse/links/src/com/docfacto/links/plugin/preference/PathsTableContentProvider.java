package com.docfacto.links.plugin.preference;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import com.docfacto.config.generated.Path;

public class PathsTableContentProvider implements IStructuredContentProvider {

    public PathsTableContentProvider() {
    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void inputChanged(Viewer viewer,Object oldInput,Object newInput) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Object[] getElements(Object inputElement) {
        return ((List<Path>)inputElement).toArray();
    }
}
