package com.docfacto.links.plugin.preference;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class FileAssociationsTableContentProvider implements IStructuredContentProvider {
    public FileAssociationsTableContentProvider() {
    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void inputChanged(Viewer viewer,Object oldInput,Object newInput) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Object[] getElements(Object inputElement) {
        return ((List<FileAssociationPair>)inputElement).toArray();
    }
}
