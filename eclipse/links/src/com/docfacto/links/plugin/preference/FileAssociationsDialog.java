package com.docfacto.links.plugin.preference;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.docfacto.links.ui.LinksDialog;

public class FileAssociationsDialog extends LinksDialog {

	private final String[] SUPPORTED_FILE_TYPES = {"java", "javascript","html", "xml", "markdown"};
	
    private String fileType;
    private String extension;
    
    private Combo fileTypeCombo;
    private Text extensionText;
    
    private boolean extensionEntered = false;
    
    private Button okButton;
    
    private boolean editing;
    
    public FileAssociationsDialog(Shell parent) {
        super(parent,"File Association Selection");
        fileType = null;
        extension = null;
    }
    
    public FileAssociationsDialog(Shell parent, String pathName, String pathValue, boolean editingAnExistingPath) {
        super(parent,"File Association Selection");
        this.fileType = pathName;
        this.extension = pathValue.trim();
        this.editing = editingAnExistingPath;
    }
    
    @Override
    protected Control createDialogArea(Composite parent) {
        if(editing) {
            getShell().setText("Editing an existing file association");
        }else{
            getShell().setText("Create a new file association");
        }
        
        Composite container = (Composite) super.createDialogArea(parent);
        container.setLayout(new GridLayout(2, false));
        
        // create 'File Type' Field
        new Label(container, SWT.NONE).setText("File Type: ");

        fileTypeCombo = new Combo(container, SWT.READ_ONLY); // SWT.READ_ONLY means the combo isn't a textbox too
        fileTypeCombo.setItems(SUPPORTED_FILE_TYPES);
        fileTypeCombo.select(0);
        fileType = fileTypeCombo.getText();
        fileTypeCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
        
        if (editing)
        	fileTypeCombo.setEnabled(false);
        
        fileTypeCombo.addModifyListener(new ModifyListener() {
        	@Override
        	public void modifyText(ModifyEvent e) {
        		fileType = fileTypeCombo.getText();
        	}
        });
        
        // create 'Extension' Field
        new Label(container, SWT.NONE).setText("Extension: ");
        
        extensionText = new Text(container, SWT.BORDER | SWT.SINGLE);
        extensionText.setText(extension);
        extensionText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
        
        extensionText.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                extension = extensionText.getText();
                refreshExtensionEntered();
            }
        });
        
        return container;
    }
    
    private void refreshExtensionEntered() {
        if (extension.trim().length() > 0) {
            extensionEntered = true;
            checkPageComplete();
        }
        else
            extensionEntered = false;
    }
    
    private void checkPageComplete() {
        okButton.setEnabled(extensionEntered);
    }
    
    @Override
    protected void createButtonsForButtonBar(Composite parent) {
        okButton = createButton(parent, IDialogConstants.OK_ID,
                IDialogConstants.OK_LABEL, true);

        createButton(parent, IDialogConstants.CANCEL_ID,
                IDialogConstants.CANCEL_LABEL, false);
        
        checkPageComplete();
    }

    public String getFileType() {
        return this.fileType;
    }
    
    public String getExtension() {
        return this.extension;
    }
}
