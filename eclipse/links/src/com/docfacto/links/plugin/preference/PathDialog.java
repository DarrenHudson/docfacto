package com.docfacto.links.plugin.preference;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.docfacto.core.widgets.DirectoryBrowserWidget;
import com.docfacto.links.ui.LinksDialog;

public class PathDialog extends LinksDialog {

    private String pathName;
    private String pathValue;
    
    private Text pathNameText;
    private DirectoryBrowserWidget pathBrowser;
    
    private boolean nameEntered = false;
    private boolean pathValueEntered = false;
    
    private Button okButton;
    
    private boolean editing;
    
    public PathDialog(Shell parent) {
        super(parent,"Path selection");
        pathName = null;
        pathValue = null;
    }
    
    public PathDialog(Shell parent, String pathName, String pathValue, boolean editingAnExistingPath) {
        super(parent,"Path selection");
        this.pathName = pathName;
        this.pathValue = pathValue.trim();
        this.editing = editingAnExistingPath;
    }
    
    @Override
    protected Control createDialogArea(Composite parent) {
        if(editing) {
            getShell().setText("Editing an existing path");
            nameEntered = true;
        }else{
            getShell().setText("Create a new path");
        }
        
        Composite container = (Composite) super.createDialogArea(parent);
        container.setLayout(new GridLayout(2, false));
        
        // create 'Name' Field
        new Label(container, SWT.NONE).setText("Name:");

        pathNameText = new Text(container, SWT.BORDER | SWT.SINGLE);
        pathNameText.setText(pathName);
        
        if (editing)
            pathNameText.setEditable(false);

        // enable OK button if all three fields are filled
        pathNameText.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                pathName = pathNameText.getText();
                refreshPathNameEntered();
            }
        });
        pathNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
        
        pathBrowser = new DirectoryBrowserWidget(container,SWT.NONE,"Path:");
        pathBrowser.setFilePath(pathValue);
        
        GridData pathBrowserGridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        pathBrowserGridData.horizontalSpan = 2;
        pathBrowser.setLayoutData(pathBrowserGridData);
        
        pathBrowser.registerListener(new Listener() {
            @Override
            public void handleEvent(Event event) {
                pathValue = pathBrowser.getSelectedPath();
                refreshPathValueEntered();
            }  
        });
        return container;
    }
    
    private void refreshPathNameEntered() {
        if (pathNameText.getText().trim().length() > 0) {
            nameEntered = true;
            checkPageComplete();
        } else
            nameEntered = false;
    }
    
    private void refreshPathValueEntered() {
        if (pathValue.trim().length() > 0) {
            pathValueEntered = true;
            checkPageComplete();
        }
        else
            pathValueEntered = false;
    }
    
    private void checkPageComplete() {
        okButton.setEnabled(nameEntered && pathValueEntered);
    }
    
    @Override
    protected void createButtonsForButtonBar(Composite parent) {
        okButton = createButton(parent, IDialogConstants.OK_ID,
                IDialogConstants.OK_LABEL, true);

        createButton(parent, IDialogConstants.CANCEL_ID,
                IDialogConstants.CANCEL_LABEL, false);
        
        checkPageComplete();
    }

    public String getPathName() {
        return this.pathName;
    }
    
    public String getPathValue() {
        return this.pathValue;
    }
}
