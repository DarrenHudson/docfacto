package com.docfacto.links.plugin.preference;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import com.docfacto.config.generated.Path;

public class PathsTableLabelProvider implements ITableLabelProvider {

    @Override
    public void addListener(ILabelProviderListener listener) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean isLabelProperty(Object element,String property) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void removeListener(ILabelProviderListener listener) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Image getColumnImage(Object element,int columnIndex) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getColumnText(Object element,int columnIndex) {
        Path path = (Path)element;
        
        switch(columnIndex) {
            case 0:
                return path.getName();
            case 1:
                return path.getValue();
            default:
                return "unknown";
        }
    }

}
