package com.docfacto.links.plugin.preference;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.config.XmlConfig;
import com.docfacto.config.generated.Association;
import com.docfacto.config.generated.FileAssociations;
import com.docfacto.config.generated.Links;
import com.docfacto.config.generated.Path;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.core.utils.PluginUtils;
import com.docfacto.links.LinksPathsManager;
import com.docfacto.links.plugin.DocfactoLinksPlugin;

public class LinksXMLConfigEditor {

	private IEclipsePreferences theLinksPrefs;
	
    private String xmlConfigLocation;
    private XmlConfig xmlConfig;
    private Links linksConfig;
    
    private String tempBaseDir;
    private List<Path> tempPaths;
    private FileAssociations tempFileAssociations;
    

    public LinksXMLConfigEditor(String xmlConfigPath) {
    	theLinksPrefs = InstanceScope.INSTANCE.getNode(DocfactoLinksPlugin.PLUGIN_ID);
    	
        xmlConfigLocation = xmlConfigPath;
        xmlConfig = DocfactoCorePlugin.getXmlConfig();
        linksConfig = xmlConfig.getLinksConfig();
        
        setUpTempConfigData(linksConfig);
    }

    private void setUpTempConfigData(Links linksConfig) {
    	tempBaseDir = theLinksPrefs.get(DocfactoLinksPlugin.BASE_DIR_KEY, linksConfig.getBasedir().trim());
		tempPaths = new ArrayList<Path>(linksConfig.getPaths().getPaths());
        tempFileAssociations = getCopyOfFileAssociations(linksConfig.getFileAssociations());
    }
    
    private FileAssociations getCopyOfFileAssociations(FileAssociations fileAssociations) {
		FileAssociations newFileAssociations = new FileAssociations();
		List<Association> newAssociationsList = new ArrayList<Association>();
		
		for (Association association: fileAssociations.getAssociations()) {
			Association newAssociation = new Association();
			newAssociation.setFileType(association.getFileType());
			
			for (String extension : association.getFileExtensions()) {
				newAssociation.getFileExtensions().add(extension);
			}
			
			newAssociationsList.add(newAssociation);
		}
		
		newFileAssociations.getAssociations().addAll(newAssociationsList);
		
		return newFileAssociations;
	}

	public void setBaseDir(String newBaseDir) {
        tempBaseDir = newBaseDir;
    }

    public String getBaseDir() {
        return tempBaseDir;
    }

    public void updateXML() throws LinksPreferenceException {
    	
    	boolean canWrite = xmlConfigIsWritable();
    	
    	if (!canWrite)
    		throw new LinksPreferenceException("Do not have permission to write to xml configuration file");
    	
    	// It seems saving to the xml is possible so go ahead
    	saveTempConfigDataToRealConfig();
    	
        try {
            xmlConfig.save();
        }
        catch (DocfactoException e) {
            throw new LinksPreferenceException("Could not save the xml config: "+e.getMessage());
        }
    }
    
    public boolean xmlConfigIsWritable() {
    	/*
    	 * Try and see if it is possible to save before transferring the temp data to actual xml config object
    	 * and overwriting the original settings
    	 */
    	
    	boolean canWrite = true;
    	boolean checkedOutXmlConfig = true;
    	
    	
		IFile xmlConfigIFile = PluginUtils.findIFileInWorkspace(xmlConfigLocation);
    	
    	if (xmlConfigIFile != null) {
    		// Try to check out the file
    		checkedOutXmlConfig = PluginUtils.checkoutFile(xmlConfigIFile);
    	}
    	
    	// Try to see if the file is writable
    	File file = new File(xmlConfigLocation);
    	canWrite = file.canWrite();
    	
    	return canWrite && checkedOutXmlConfig;
    }

    private void saveTempConfigDataToRealConfig() {
		linksConfig.setBasedir(tempBaseDir);
		linksConfig.getPaths().getPaths().clear();
		linksConfig.getPaths().getPaths().addAll(tempPaths);
		linksConfig.setFileAssociations(tempFileAssociations);
	}

	public List<Path> getPaths() {
        return tempPaths;
    }

    public void addNewPath(String newPathName,String newPathValue) throws LinksPreferenceException {
        List<Path> paths = getPaths();
		
		String resolvedPathValue = LinksPathsManager.INSTANCE.getShortenedPathFromAbsolutePath(newPathValue);

        if (pathsListAlreadyHasPathWithName(paths, newPathName))
            throw new LinksPreferenceException("A path of name: "+newPathName+" already exists");
        
        Path newPath = new Path();
        newPath.setName(newPathName);
        newPath.setValue(resolvedPathValue);

        paths.add(newPath);
    }

    private String getRelativePath(String selectedPath,String baseDirectory) {
        String relativePathString = IOUtils.getRelativePath(baseDirectory, selectedPath);

        if (relativePathString.trim().isEmpty())
            return ".";

        return relativePathString;
    }

    private boolean pathsListAlreadyHasPathWithName(List<Path> paths,String newPathName) {

        for (Path path:paths) {
            if (path.getName().equals(newPathName))
                return true;
        }

        return false;
    }

    public void changePathValue(String pathName,String newPathValue) {
        List<Path> paths = getPaths();
        for (Path path:paths) {
            if (path.getName().equals(pathName)) {
                String resolvedPathValue = LinksPathsManager.INSTANCE.getShortenedPathFromAbsolutePath(newPathValue);
                path.setValue(resolvedPathValue);
            }
        }
    }

    public void deletePath(String pathName) {
        List<Path> paths = getPaths();
        for (Path path:paths) {
            if (path.getName().equals(pathName)) {
                paths.remove(path);
                return;
            }
        }
    }

    public FileAssociations getFileAssociations() {
        return tempFileAssociations;
    }

    public void addNewFileAssociation(String newFileAssociationFileType,String newFileAssociationExtension)
    throws LinksPreferenceException {

    	if (tempFileAssociations == null) {
    		tempFileAssociations = new FileAssociations();
    	}
    	
        for (Association association:tempFileAssociations.getAssociations()) {
            String fileType = association.getFileType();

            if (fileType.equals(newFileAssociationFileType)) {

                if (extensionAlreadyExistsInAssociation(association, newFileAssociationExtension)) {
                    throw new LinksPreferenceException("The extensions: "+newFileAssociationExtension+
                        " is already associated with a file type");
                }

                association.getFileExtensions().add(newFileAssociationExtension);
                return;
            }
        }

        // An existing association for the file type to be added does not exist, so make and add a new association
        Association newAssociation = new Association();
        newAssociation.setFileType(newFileAssociationFileType);
        newAssociation.getFileExtensions().add(newFileAssociationExtension);
        tempFileAssociations.getAssociations().add(newAssociation);
    }

    private boolean extensionAlreadyExistsInAssociation(Association association,String targetExtension) {
        for (String extension:association.getFileExtensions()) {
            if (extension.equals(targetExtension))
                return true;
        }

        return false;
    }

    public void deleteFileAssociation(String targetExtension) {
        String targetExtensionTrimmed = targetExtension.trim();
        List<Association> associations = tempFileAssociations.getAssociations();

        for (Association association:associations) {
            for (String extension:association.getFileExtensions()) {
                if (extension.trim().equals(targetExtensionTrimmed)) {
                    association.getFileExtensions().remove(targetExtension);

                    if (association.getFileExtensions().isEmpty())
                        associations.remove(association);

                    if (associations.isEmpty())
                        tempFileAssociations = null;

                    return;
                }
            }
        }
    }

	public void restoreTempValuesBackToXmlConfigValues() {
		setUpTempConfigData(this.linksConfig);
	}
}
