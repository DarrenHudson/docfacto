package com.docfacto.links.plugin.preference;

public class FileAssociationPair {
	private final String fileType;
	private final String fileExtension;
	
	public FileAssociationPair(String fileType, String fileExtension) {
		this.fileType = fileType;
		this.fileExtension = fileExtension;
	}

	/**
	 * @return the fileType
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * @return the fileExtension
	 */
	public String getFileExtension() {
		return fileExtension;
	}
}
