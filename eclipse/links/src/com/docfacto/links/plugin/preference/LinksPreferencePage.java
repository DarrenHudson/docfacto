/*
 * @author dhudson -
 * Created 22 Jan 2013 : 09:41:45
 */

package com.docfacto.links.plugin.preference;

import java.io.File;
import java.util.List;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IScope;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.dialogs.PreferencesUtil;
import org.osgi.service.prefs.BackingStoreException;

import com.docfacto.common.IOUtils;
import com.docfacto.config.generated.FileAssociations;
import com.docfacto.config.generated.Path;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.core.XmlChangeListener;
import com.docfacto.core.widgets.DirectoryBrowserWidget;
import com.docfacto.links.plugin.DocfactoLinksPlugin;

/**
 * The preference page for Links.
 * <p>
 * The preference page for links allows the user to change the base directory, the user defined paths and file associations 
 * defined in the Links configuration.
 * </p>
 * @author dhudson - created 22 Jan 2013
 * @since 2.1
 */
public class LinksPreferencePage extends PreferencePage implements IWorkbenchPreferencePage,IScope,XmlChangeListener {

    private IEclipsePreferences theLinksPrefs;
    private String xmlConfigLocation;
    private LinksXMLConfigEditor configEditor;

    private DirectoryBrowserWidget theBaseDirBrowserWidget;
    private String selectedBaseDir;

    private PathsTableContentProvider pathsTableContentProvider;
    private TableViewer pathsTableViewer;
    private Table pathsTable;
    private Button addNewPathButton;
    private Button editPathButton;
    private Button deletePathButton;

    private String selectedPathNameInTable;
    private String selectedPathValueInTable;

    private FileAssociationsTableContentProvider fileAssociationsTableContentProvider;
    private FileAssociationPairsProvider fileAssociationPairsProvider;
    private TableViewer fileAssociationsTableViewer;
    private Table fileAssociationsTable;
    private Button addNewFileAssociationButton;
    private Button deleteFileAssociationButton;

    private String selectedFileAssociationTypeInTable;
    private String selectedFileAssociationExtensionInTable;

    private Label warningLabel;
    
    private boolean coreAvailable;

    private boolean isDirty;
    
    private boolean canEditXMLConfig;

    private Composite composite;

    /**
     * Create a new instance of <code>LinksPreferencePage</code>.
     */
    public LinksPreferencePage() {
        setTitle("Links Configuration");
        isDirty = false;
        canEditXMLConfig = false;
        DocfactoCorePlugin.getDefault().addXmlChangeListener(this);
    }

	/**
     * @see com.docfacto.core.XmlChangeListener#xmlChanged()
     */
    @Override
    public void xmlChanged() {
        // TODO Auto-generated method stub
    }

    /**
     * @see com.docfacto.core.XmlChangeListener#xmlReloaded()
     */
    @Override
    public void xmlReloaded() {
        if (composite!=null) {
            Composite grandParent = composite.getParent().getParent();
            super.createControl(grandParent);
        }
    }

    /**
     * @see org.eclipse.jface.dialogs.DialogPage#dispose()
     */
    @Override
    public void dispose() {
        DocfactoCorePlugin.getDefault().removeXmlChangeListener(this);
        super.dispose();
    }

    /**
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    @Override
    public void init(IWorkbench workbench) {
        setDescription("Set the base dir and add paths for links to use");
    }

    /**
     * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createContents(final Composite parent) {
        theLinksPrefs = InstanceScope.INSTANCE.getNode(DocfactoLinksPlugin.PLUGIN_ID);
        xmlConfigLocation = DocfactoCorePlugin.getDefault().getPreferenceStore().getString(DocfactoCorePlugin.DOCFACTO_KEY);
        configEditor = new LinksXMLConfigEditor(xmlConfigLocation);
        canEditXMLConfig = configEditor.xmlConfigIsWritable();
        pathsTableContentProvider = new PathsTableContentProvider();
        fileAssociationsTableContentProvider = new FileAssociationsTableContentProvider();
        fileAssociationPairsProvider = new FileAssociationPairsProvider();

        // Set the currently selected base dir as the one from the config first
        selectedBaseDir = configEditor.getBaseDir();
        updatePreferencesWithValuesFromConfig();

        composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(2, true));

        // Check if internal XML is being used
        if (DocfactoCorePlugin.getDefault().isUsingInternalXML()) {
            // display error message above page
            setMessage("Using default configuration settings", WARNING);

            Link link = new Link(composite, SWT.NONE);
            link.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    PreferencesUtil
                        .createPreferenceDialogOn(null, "com.docfacto.core.preference.Docfacto", null, null)
                        .open();
                }
            });

            link.setText("<a>Set your configuration file</a>");

            return composite;
        }
        // Check if 'Docfacto.xml' has been validated
        else if (!DocfactoCorePlugin.getDefault().getPreferenceStore().getBoolean(DocfactoCorePlugin.KEY_VALIDATED)) {
            // display error message above page
            setErrorMessage("A valid docfacto configuration file is required");

            Label lblTagsInSelected = new Label(composite, SWT.WRAP
                |SWT.CENTER);
            lblTagsInSelected.setBounds(5, 5, 300, 150);
            lblTagsInSelected
                .setText(DocfactoCorePlugin.getDefault()
                    .getPreferenceStore()
                    .getString(DocfactoCorePlugin.VALIDATION_ERROR)
                    +"\n\nYou will have to relaunch this window before your changes take effect");

            return composite;
        }
        else {
            coreAvailable = true;
        }

        GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
        gridData.horizontalSpan = 2;
        gridData.grabExcessVerticalSpace = false;
        composite.setLayoutData(gridData);

        setUpBaseDirPreferenceContent(composite);
        setUpPathsPreferenceContent(composite);
        setUpFileAssociationsPreferenceContent(composite);
        setUpWarningContent(composite);
        

    	if (!canEditXMLConfig) {
    		theBaseDirBrowserWidget.setEnabled(false);
    		
    		addNewPathButton.setEnabled(false);
    		editPathButton.setEnabled(false);
    		deletePathButton.setEnabled(false);
    		
    		addNewFileAssociationButton.setEnabled(false);
    		deleteFileAssociationButton.setEnabled(false);
    		
    		warningLabel.setText("Do not have permission to edit XML configuration file");
    	}
        
        return composite;
    }

    private void setIsDirty(boolean isDirty) {
        this.isDirty = isDirty;
        
        if (isDirty)
        	warningLabel.setText("Changes have not been applied to the xml yet");
        else
        	warningLabel.setText("");
    }
    
    private void setUpWarningContent(Composite parent) {
    	Composite warningSectionComposite = new Composite(parent, SWT.NONE);
        GridLayout warningSectionGridLayout = new GridLayout(2, false);
        warningSectionGridLayout.marginWidth = 0;
        warningSectionComposite.setLayout(warningSectionGridLayout);

        GridData warningSectionGridData = new GridData(GridData.FILL_BOTH);
        warningSectionGridData.horizontalSpan = 2;
        warningSectionGridData.grabExcessVerticalSpace = true;
        warningSectionComposite.setLayoutData(warningSectionGridData);

        warningLabel = new Label(warningSectionComposite, SWT.NONE);
        warningLabel.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_RED));
        warningLabel.setText("");
        GridData warningLabelGridData = new GridData(GridData.FILL_HORIZONTAL);
        warningLabelGridData.horizontalSpan = 2;
        warningLabelGridData.grabExcessVerticalSpace = false;
        warningLabel.setLayoutData(warningLabelGridData);
    }

    private void setUpBaseDirPreferenceContent(Composite parent) {
        Composite baseDirComposite = new Composite(parent, SWT.NONE);
        GridLayout baseDirectoryGridLayout = new GridLayout(2, false);
        baseDirComposite.setLayout(baseDirectoryGridLayout);

        GridData baseDirGridData = new GridData(GridData.FILL_HORIZONTAL);
        baseDirGridData.horizontalSpan = 2;
        baseDirGridData.verticalSpan = 1;
        baseDirGridData.grabExcessVerticalSpace = false;
        baseDirComposite.setLayoutData(baseDirGridData);

        final String containingFolderOfXmlConfig = new File(xmlConfigLocation).getParent().toString();
        // TODO: Find out why this messes up the layout
        // Label baseDirDescriptionLabel = new Label(baseDirComposite, SWT.NONE);
        // baseDirDescriptionLabel.setLayoutData(baseDirGridData);
        // baseDirDescriptionLabel.setText("Set the base dir (relative to the Docfacto.xml location) which all other links paths are relative to");

        theBaseDirBrowserWidget = new DirectoryBrowserWidget(baseDirComposite, SWT.NONE, "Base Directory Location");
        theBaseDirBrowserWidget.setLayoutData(baseDirGridData);
        theBaseDirBrowserWidget.setFilePath(selectedBaseDir);

        theBaseDirBrowserWidget.registerListener(new Listener() {
            @Override
            public void handleEvent(Event event) {
                String previouslySelectedBaseDir = selectedBaseDir;
                String selectedPath = theBaseDirBrowserWidget.getSelectedPath();

                if (selectedPath.startsWith(".")) {
                    // Path is already relative
                    selectedBaseDir = selectedPath;
                }
                else {
                    String baseDirRelativeToTheLocationOfTheXmlConfig =
                        getRelativePath(selectedPath, containingFolderOfXmlConfig);
                    if (baseDirRelativeToTheLocationOfTheXmlConfig!=null) {
                        selectedBaseDir = baseDirRelativeToTheLocationOfTheXmlConfig;
                        theBaseDirBrowserWidget.setFilePath(selectedBaseDir);
                    }
                }

                if (!previouslySelectedBaseDir.equals(selectedBaseDir)) {
                    setIsDirty(true);
                }
            }
        });
    }

    private void setUpPathsPreferenceContent(Composite parent) {
        Composite pathsSectionComposite = new Composite(parent, SWT.NONE);
        GridLayout pathsSectionGridLayout = new GridLayout(2, false);
        pathsSectionGridLayout.marginWidth = 0;
        pathsSectionComposite.setLayout(pathsSectionGridLayout);

        GridData pathsSectionGridData = new GridData(GridData.FILL_BOTH);
        pathsSectionGridData.horizontalSpan = 2;
        pathsSectionGridData.grabExcessVerticalSpace = true;
        pathsSectionComposite.setLayoutData(pathsSectionGridData);

        Label pathsTableLabel = new Label(pathsSectionComposite, SWT.NONE);
        pathsTableLabel.setText("Paths: ");
        GridData pathsLabelGridData = new GridData(GridData.FILL_HORIZONTAL);
        pathsLabelGridData.horizontalSpan = 2;
        pathsLabelGridData.grabExcessVerticalSpace = false;
        pathsTableLabel.setLayoutData(pathsLabelGridData);

        // A composite for the paths table and buttons
        Composite pathsTableSectionComposite = new Composite(pathsSectionComposite, SWT.NONE);

        // A composite for just the paths table
        Composite pathsTableComposite = new Composite(pathsTableSectionComposite, SWT.NONE);

        pathsTableViewer = new TableViewer(pathsTableComposite, SWT.BORDER|SWT.FULL_SELECTION);

        pathsTable = pathsTableViewer.getTable();
        pathsTable.setLinesVisible(true);
        pathsTable.setHeaderVisible(true);

        pathsTableViewer.setContentProvider(pathsTableContentProvider);
        pathsTableViewer.setLabelProvider(new PathsTableLabelProvider());

        // Disable delete and edit buttons when no item is selected
        pathsTableViewer.addPostSelectionChangedListener(new ISelectionChangedListener() {
            @Override
            public void selectionChanged(SelectionChangedEvent event) {
                if (pathsTableViewer.getSelection().isEmpty()) {
                    deletePathButton.setEnabled(false);
                    editPathButton.setEnabled(false);
                }
                else {
                	if (canEditXMLConfig) {
	                    deletePathButton.setEnabled(true);
	                    editPathButton.setEnabled(true);
                	}
                }
            }
        });

        pathsTableViewer.addSelectionChangedListener(new ISelectionChangedListener() {
            @Override
            public void selectionChanged(SelectionChangedEvent event) {
                IStructuredSelection selection = (IStructuredSelection)event.getSelection();
                Path path = (Path)selection.getFirstElement();

                if (path!=null) {
                    selectedPathNameInTable = path.getName();
                    selectedPathValueInTable = path.getValue();
                }
            }
        });

        TableColumn pathNameColumn = new TableColumn(pathsTable, SWT.NONE);
        pathNameColumn.setText("Name");

        TableColumn pathValueColumn = new TableColumn(pathsTable, SWT.NONE);
        pathValueColumn.setText("Path");

        TableColumnLayout columnsLayout = new TableColumnLayout();
        pathsTableComposite.setLayout(columnsLayout);
        columnsLayout.setColumnData(pathNameColumn, new ColumnWeightData(20));
        columnsLayout.setColumnData(pathValueColumn, new ColumnWeightData(80));

        refreshPathsTable();

        addNewPathButton = new Button(pathsTableSectionComposite, SWT.PUSH);
        addNewPathButton.setText("New");
        addNewPathButton.setEnabled(true);

        editPathButton = new Button(pathsTableSectionComposite, SWT.PUSH);
        editPathButton.setText("Edit");
        editPathButton.setEnabled(false);

        deletePathButton = new Button(pathsTableSectionComposite, SWT.PUSH);
        deletePathButton.setText("Delete");
        deletePathButton.setEnabled(false);

        GridData pathsTableSectionGridData = new GridData(GridData.FILL_BOTH);
        pathsTableSectionGridData.horizontalAlignment = SWT.FILL;
        pathsTableSectionGridData.verticalAlignment = SWT.FILL;
        pathsTableSectionGridData.grabExcessHorizontalSpace = true;
        pathsTableSectionGridData.grabExcessVerticalSpace = true;
        pathsTableSectionGridData.verticalSpan = 6;

        pathsTableComposite.setLayoutData(pathsTableSectionGridData);

        GridData pathsTableButtonsGridData = new GridData();
        pathsTableButtonsGridData.verticalAlignment = SWT.BEGINNING;

        addNewPathButton.setLayoutData(pathsTableButtonsGridData);
        editPathButton.setLayoutData(pathsTableButtonsGridData);
        deletePathButton.setLayoutData(pathsTableButtonsGridData);

        addNewPathButton.addSelectionListener(new SelectionAdapter() {
            // New Path Button: on click
            @Override
            public void widgetSelected(final SelectionEvent e) {
                final PathDialog dialog = new PathDialog(getShell(), "", "", false);
                if (dialog.open()==Window.OK) {
                    String newPathName = dialog.getPathName();
                    String newPathValue = dialog.getPathValue();
                    Path newPath = new Path();
                    newPath.setName(newPathName);
                    newPath.setValue(newPathValue);
                    try {
                        configEditor.addNewPath(newPathName, newPathValue);
                        setIsDirty(true);
                    }
                    catch (LinksPreferenceException e1) {
                        showErrorDialog(e1.getMessage());
                    }
                    refreshPathsTable();
                }
            }
        });

        editPathButton.addSelectionListener(new SelectionAdapter() {
            // Edit Path Button: on click
            @Override
            public void widgetSelected(final SelectionEvent e) {
                final PathDialog dialog =
                    new PathDialog(getShell(), selectedPathNameInTable, selectedPathValueInTable, true);
                if (dialog.open()==Window.OK) {
                    String newPathValue = dialog.getPathValue();
                    configEditor.changePathValue(selectedPathNameInTable, newPathValue);
                    setIsDirty(true);
                    refreshPathsTable();
                }
            }
        });

        deletePathButton.addSelectionListener(new SelectionAdapter() {
            // Edit Path Button: on click
            @Override
            public void widgetSelected(final SelectionEvent e) {
                configEditor.deletePath(selectedPathNameInTable);
                setIsDirty(true);
                refreshPathsTable();
            }
        });

        GridLayout pathsTableSectionGridLayout = new GridLayout(2, false);
        pathsTableSectionGridLayout.marginWidth = 0;
        pathsTableSectionComposite.setLayout(pathsTableSectionGridLayout);
        pathsTableSectionComposite.setLayoutData(pathsTableSectionGridData);
    }

    private void setUpFileAssociationsPreferenceContent(Composite parent) {
        Composite fileAssociationsSectionComposite = new Composite(parent, SWT.NONE);
        GridLayout fileAssociationsSectionGridLayout = new GridLayout(2, false);
        fileAssociationsSectionGridLayout.marginWidth = 0;
        fileAssociationsSectionComposite.setLayout(fileAssociationsSectionGridLayout);

        GridData fileAssociationsSectionGridData = new GridData(GridData.FILL_BOTH);
        fileAssociationsSectionGridData.horizontalSpan = 2;
        fileAssociationsSectionGridData.grabExcessVerticalSpace = true;
        fileAssociationsSectionComposite.setLayoutData(fileAssociationsSectionGridData);

        Label fileAssociationsTableLabel = new Label(fileAssociationsSectionComposite, SWT.NONE);
        fileAssociationsTableLabel.setText("File Associations: ");
        GridData fileAssociationsLabelGridData = new GridData(GridData.FILL_HORIZONTAL);
        fileAssociationsLabelGridData.horizontalSpan = 2;
        fileAssociationsLabelGridData.grabExcessVerticalSpace = false;
        fileAssociationsTableLabel.setLayoutData(fileAssociationsLabelGridData);

        // A composite for the paths table and buttons
        Composite fileAssociationsTableSectionComposite = new Composite(fileAssociationsSectionComposite, SWT.NONE);

        // A composite for just the paths table
        Composite fileAssociationsTableComposite = new Composite(fileAssociationsTableSectionComposite, SWT.NONE);

        fileAssociationsTableViewer = new TableViewer(fileAssociationsTableComposite, SWT.BORDER|SWT.FULL_SELECTION);

        fileAssociationsTable = fileAssociationsTableViewer.getTable();
        fileAssociationsTable.setLinesVisible(true);
        fileAssociationsTable.setHeaderVisible(true);

        fileAssociationsTableViewer.setContentProvider(fileAssociationsTableContentProvider);
        fileAssociationsTableViewer.setLabelProvider(new FileAssociationsTableLabelProvider());

        // Disable delete and edit buttons when no item is selected
        fileAssociationsTableViewer.addPostSelectionChangedListener(new ISelectionChangedListener() {
            @Override
            public void selectionChanged(SelectionChangedEvent event) {
                if (fileAssociationsTableViewer.getSelection().isEmpty()) {
                    deleteFileAssociationButton.setEnabled(false);
                }
                else {
                	if (canEditXMLConfig)
                		deleteFileAssociationButton.setEnabled(true);
                }
            }
        });

        fileAssociationsTableViewer.addSelectionChangedListener(new ISelectionChangedListener() {
            @Override
            public void selectionChanged(SelectionChangedEvent event) {
                IStructuredSelection selection = (IStructuredSelection)event.getSelection();
                FileAssociationPair fileAssociationPair = (FileAssociationPair)selection.getFirstElement();

                if (fileAssociationPair!=null) {
                    selectedFileAssociationTypeInTable = fileAssociationPair.getFileType();
                    selectedFileAssociationExtensionInTable = fileAssociationPair.getFileExtension();
                }
            }
        });

        TableColumn fileAssociationsNameColumn = new TableColumn(fileAssociationsTable, SWT.NONE);
        fileAssociationsNameColumn.setText("File Type");

        TableColumn fileAssociationsValueColumn = new TableColumn(fileAssociationsTable, SWT.NONE);
        fileAssociationsValueColumn.setText("Extension");

        TableColumnLayout columnsLayout = new TableColumnLayout();
        fileAssociationsTableComposite.setLayout(columnsLayout);
        columnsLayout.setColumnData(fileAssociationsNameColumn, new ColumnWeightData(20));
        columnsLayout.setColumnData(fileAssociationsValueColumn, new ColumnWeightData(80));

        refreshFileAssociationsTable();

        addNewFileAssociationButton = new Button(fileAssociationsTableSectionComposite, SWT.PUSH);
        addNewFileAssociationButton.setText("New");
        addNewFileAssociationButton.setEnabled(true);

        deleteFileAssociationButton = new Button(fileAssociationsTableSectionComposite, SWT.PUSH);
        deleteFileAssociationButton.setText("Delete");
        deleteFileAssociationButton.setEnabled(false);

        GridData fileAssociationsTableSectionGridData = new GridData(GridData.FILL_BOTH);
        fileAssociationsTableSectionGridData.horizontalAlignment = SWT.FILL;
        fileAssociationsTableSectionGridData.verticalAlignment = SWT.FILL;
        fileAssociationsTableSectionGridData.grabExcessHorizontalSpace = true;
        fileAssociationsTableSectionGridData.grabExcessVerticalSpace = true;
        fileAssociationsTableSectionGridData.verticalSpan = 6;

        fileAssociationsTableComposite.setLayoutData(fileAssociationsTableSectionGridData);

        GridData fileAssociationsTableButtonsGridData = new GridData();
        fileAssociationsTableButtonsGridData.verticalAlignment = SWT.BEGINNING;

        addNewFileAssociationButton.setLayoutData(fileAssociationsTableButtonsGridData);
        deleteFileAssociationButton.setLayoutData(fileAssociationsTableButtonsGridData);

        addNewFileAssociationButton.addSelectionListener(new SelectionAdapter() {
            // New Path Button: on click
            @Override
            public void widgetSelected(final SelectionEvent e) {
                final FileAssociationsDialog dialog = new FileAssociationsDialog(getShell(), "", "", false);
                if (dialog.open()==Window.OK) {
                    String newFileAssociationFileType = dialog.getFileType();
                    String newFileAssociationExtension = dialog.getExtension();

                    String fixedNewFileAssociationExtension = fixExtensionIfNeeded(newFileAssociationExtension);

                    try {
                        configEditor
                            .addNewFileAssociation(newFileAssociationFileType, fixedNewFileAssociationExtension);
                        setIsDirty(true);
                    }
                    catch (LinksPreferenceException e1) {
                        showErrorDialog(e1.getMessage());
                    }
                    refreshFileAssociationsTable();
                }
            }

            private String fixExtensionIfNeeded(String newFileAssociationExtension) {
                String fixedExtension = newFileAssociationExtension;

                if (!fixedExtension.contains("."))
                    fixedExtension = "."+newFileAssociationExtension;

                if (!fixedExtension.startsWith(".")) {
                    int lastIndexOfDot = fixedExtension.lastIndexOf(".");
                    fixedExtension = newFileAssociationExtension.substring(lastIndexOfDot);
                }

                return fixedExtension;
            }
        });

        deleteFileAssociationButton.addSelectionListener(new SelectionAdapter() {
            // Edit Path Button: on click
            @Override
            public void widgetSelected(final SelectionEvent e) {
                configEditor.deleteFileAssociation(selectedFileAssociationExtensionInTable);
                setIsDirty(true);
                refreshFileAssociationsTable();
            }
        });

        GridLayout fileAssociationsTableSectionGridLayout = new GridLayout(2, false);
        fileAssociationsTableSectionGridLayout.marginWidth = 0;
        fileAssociationsTableSectionComposite.setLayout(fileAssociationsTableSectionGridLayout);
        fileAssociationsTableSectionComposite.setLayoutData(fileAssociationsTableSectionGridData);
    }

    private void showErrorDialog(String errorMessage) {
        ErrorDialog errorDialog = new ErrorDialog(getShell(), errorMessage);
        if (errorDialog.open()==Window.OK) {
            // Do Nothing
        }
    }

    private void refreshPathsTable() {
        pathsTableViewer.setInput(configEditor.getPaths());
        pathsTableViewer.refresh();

        for (int i = 0;i<pathsTable.getColumnCount();i++) {
            pathsTable.getColumn(i).pack();
        }
    }

    private void refreshFileAssociationsTable() {
        FileAssociations fileAssociations = configEditor.getFileAssociations();
        List<FileAssociationPair> fileAssociationPairs =
            fileAssociationPairsProvider.getFileAssociationPairs(fileAssociations);
        fileAssociationsTableViewer.setInput(fileAssociationPairs);

        fileAssociationsTableViewer.refresh();

        for (int i = 0;i<fileAssociationsTable.getColumnCount();i++) {
            fileAssociationsTable.getColumn(i).pack();
        }
    }

    /**
     * @see org.eclipse.core.runtime.preferences.IScope#create(org.eclipse.core.runtime.preferences.IEclipsePreferences,
     * java.lang.String)
     */
    @Override
    public IEclipsePreferences create(IEclipsePreferences parent,String name) {
        return null;
    }

    private String getRelativePath(String selectedPath,String baseDirectory) {
        String relativePathString = IOUtils.getRelativePath(baseDirectory, selectedPath);

        if (relativePathString.trim().isEmpty())
            return ".";

        return relativePathString;
    }

    @Override
    public boolean performOk() {
        if (coreAvailable&&isDirty) {
            configEditor.setBaseDir(selectedBaseDir);

            try {
                configEditor.updateXML();
            }
            catch (LinksPreferenceException e1) {
            	DocfactoLinksPlugin.logException(e1.getMessage(), e1);
                showErrorDialog(e1.getMessage());
                return false;
            }
            // save base dir to preferences
            theLinksPrefs.put(DocfactoLinksPlugin.BASE_DIR_KEY, selectedBaseDir);

            try {
                theLinksPrefs.flush();
            }
            catch (BackingStoreException e) {
            	DocfactoLinksPlugin.logException(e.getMessage(), e);
                showErrorDialog(e.getMessage());
            }
            
            setIsDirty(false);
        }
        
        return true;
    }
    
    @Override
    public void performDefaults() {
    	configEditor.restoreTempValuesBackToXmlConfigValues();
    	
    	theBaseDirBrowserWidget.setFilePath(configEditor.getBaseDir());
    	refreshPathsTable();
    	refreshFileAssociationsTable();
    	setIsDirty(false);
    	super.performDefaults();
    }

    private void updatePreferencesWithValuesFromConfig() {
        theLinksPrefs.put(DocfactoLinksPlugin.BASE_DIR_KEY, configEditor.getBaseDir());
        try {
            theLinksPrefs.flush();
        }
        catch (BackingStoreException e) {
        	DocfactoLinksPlugin.logException(e.getMessage(), e);
            showErrorDialog(e.getMessage());
        }
    }

}
