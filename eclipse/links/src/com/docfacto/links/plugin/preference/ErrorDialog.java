package com.docfacto.links.plugin.preference;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.docfacto.links.ui.LinksDialog;

public class ErrorDialog extends LinksDialog {

    private final String errorMessage;

    public ErrorDialog(Shell parentShell,String errorMessage) {
        super(parentShell,"Error occurred");
        this.errorMessage = errorMessage;
    }

    @Override
    protected Control createDialogArea(Composite parent) {
        Composite container = (Composite)super.createDialogArea(parent);
        GridData gd = new GridData();
        gd.widthHint = 400;
        container.setLayoutData(gd);
        
//        FillLayout fillLayout = new FillLayout();
//        fillLayout.type = SWT.VERTICAL;

        Label errorLabel = new Label(container,SWT.WRAP);
        GridData gd_errorLabel = new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1);
        gd_errorLabel.widthHint = 400;
        errorLabel.setLayoutData(gd_errorLabel);
        errorLabel.setText(errorMessage);

        Display display = Display.getCurrent();
        Color red = display.getSystemColor(SWT.COLOR_RED);
        errorLabel.setForeground(red);

        return container;
    }

    @Override
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent,IDialogConstants.OK_ID,IDialogConstants.OK_LABEL,true);
    }
}
