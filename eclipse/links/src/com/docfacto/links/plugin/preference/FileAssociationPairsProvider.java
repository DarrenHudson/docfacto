package com.docfacto.links.plugin.preference;

import java.util.ArrayList;
import java.util.List;

import com.docfacto.config.generated.Association;
import com.docfacto.config.generated.FileAssociations;

public class FileAssociationPairsProvider {
	
	public List<FileAssociationPair> getFileAssociationPairs(FileAssociations fileAssociations) {
		List<FileAssociationPair> pairsList = new ArrayList<FileAssociationPair>();
		
		if (fileAssociations == null)
			return pairsList;
		
		for (Association association : fileAssociations.getAssociations()) {
			String fileType = association.getFileType();
			
			for (String extension : association.getFileExtensions()) {
				FileAssociationPair pair = new FileAssociationPair(fileType, extension);
				pairsList.add(pair);
			}
		}
		
		return pairsList;
	}
}
