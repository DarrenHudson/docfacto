package com.docfacto.links.plugin.preference;

public class LinksPreferenceException extends Exception {

    public LinksPreferenceException(String message) {
        super(message);
    }
}
