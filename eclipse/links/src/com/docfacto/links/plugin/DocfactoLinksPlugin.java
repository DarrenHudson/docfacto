package com.docfacto.links.plugin;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import com.docfacto.common.DocfactoException;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.links.LinksConfiguration;
import com.docfacto.links.LinksPathsManager;

/**
 * The activator class controls the plug-in life cycle
 * 
 * @author dhudson
 * @since 2.4
 */
public class DocfactoLinksPlugin extends AbstractUIPlugin {

    // The plug-in ID
    public static final String PLUGIN_ID = "com.docfacto.links";

    // The shared instance
    private static DocfactoLinksPlugin thePlugin;

    public static final String BASE_DIR_KEY = "basedir";

    public static final String DOCFACTO_LINK_TAG_NAME = "@docfacto.link";

    public static final String LINKS_LOGO_64 = "links_logo-64";

    private LinksConfiguration theLinksConfiguration;

    /**
     * {@value}
     */
    public static final String LINKS_LOGO_16 = "links-logo-16";

    /**
     * The constructor
     */
    public DocfactoLinksPlugin() {
    }

    /**
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
     */
    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);
        
        setUpLinksConfiguration();
        
        thePlugin = this;
        
        logMessage("Docfacto Links Plugin loaded");
    }

    private void setUpLinksConfiguration() throws DocfactoException {
        theLinksConfiguration = new LinksConfiguration();
        String xmlConfigPath = DocfactoCorePlugin.getDefault().getPreferenceStore().getString(DocfactoCorePlugin.DOCFACTO_KEY);
        theLinksConfiguration.setConfigPath(xmlConfigPath);
        theLinksConfiguration.validate(false, false);
	}
    
    public LinksConfiguration getLinksConfiguration() {
    	return theLinksConfiguration;
    }

	/**
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
     */
    @Override
    public void stop(BundleContext context) throws Exception {
        thePlugin = null;
        super.stop(context);
    }

    /**
     * Returns the shared instance
     * 
     * @return the shared instance
     */
    public static DocfactoLinksPlugin getDefault() {
        return thePlugin;
    }

    /**
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#initializeImageRegistry(org.eclipse.jface.resource.ImageRegistry)
     */
    @Override
    protected void initializeImageRegistry(ImageRegistry registry) {
        super.initializeImageRegistry(registry);

        final Bundle bundle = Platform.getBundle(PLUGIN_ID);

        registry.put(LINKS_LOGO_64,ImageDescriptor.createFromURL(FileLocator
            .find(bundle,new Path("icons/links64x64.png"),null)));

        registry.put(LINKS_LOGO_16,ImageDescriptor.createFromURL(FileLocator
            .find(bundle,new Path("icons/links16x16.png"),null)));
    }

    /**
     * Return a Docfacto image
     * 
     * @param key of the Image
     * @return the cached image, or null if not found
     * @since 2.1
     */
    public static Image getImage(String key) {
        final ImageRegistry imageRegistry = thePlugin.getImageRegistry();
        return imageRegistry.get(key);
    }

    /**
     * Log and exception to the console
     * 
     * @param message to display
     * @param t throwable cause, can be null
     * @since 2.1
     */
    public static void logException(String message,Throwable t) {
        thePlugin.getLog().log(
            new Status(Status.ERROR,PLUGIN_ID,Status.ERROR,message,t));
    }

    /**
     * Log an message.
     * 
     * This will only log a message if debugging is switched on.
     * 
     * @param message to log
     * @since 2.4
     */
    public static void logMessage(String message) {
        thePlugin.getLog().log(
            new Status(Status.INFO,PLUGIN_ID,Status.INFO,message,null));
    }
}
