package com.docfacto.links.hover;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.ITextHoverExtension;
import org.eclipse.jface.text.ITextHoverExtension2;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Region;
import org.eclipse.wst.sse.ui.internal.StructuredTextViewer;

import com.docfacto.common.XMLUtils;
import com.docfacto.core.hovers.TagletInput;
import com.docfacto.javadoc.TagParser;
import com.docfacto.links.plugin.DocfactoLinksPlugin;

/**
 * Comment Hover. This needs to be installed getTextHover
 * 
 * @see org.eclipse.wst.sse.ui.StructuredTextViewerConfiguration#getTextHover(org.eclipse.jface.text.source.ISourceViewer,
 * java.lang.String, int)
 * @author dhudson - created 25 Nov 2013
 * @since 2.5
 */
@SuppressWarnings("restriction")
public class CommentHover implements ITextHover,ITextHoverExtension,
ITextHoverExtension2 {

    private TagParser theTagParser;

    /**
     * Constructor.
     */
    public CommentHover() {

    }

    /**
     * We have a region, but we need to calculate the start and end of the
     * comment
     * 
     * @see org.eclipse.jface.text.ITextHoverExtension2#getHoverInfo2(org.eclipse.jface.text.ITextViewer,
     * org.eclipse.jface.text.IRegion)
     */
    @Override
    public Object getHoverInfo2(ITextViewer textViewer,IRegion hoverRegion) {
        try {

            IDocument document = getDocument(textViewer);

            // To handle broken comment lines et al scan backwards and forwards
            // to find the begin and end of the comment
            int offset =
                getCommentStart(document,
                    hoverRegion.getOffset()+hoverRegion.getLength());
            int endOffset = getCommentEnd(document,hoverRegion.getOffset());

            TagParser parser =
                processComment(document,new Region(offset,endOffset-offset));

            if (parser!=null) {
                return new TagletInput(parser);
            }
        }
        catch (BadLocationException ignore) {
            // Can happen if the doc is incomplete
        }

        return null;
    }

    /**
     * Return the offset of the end of the comment
     * 
     * @param document to process
     * @param offset to start from
     * @return the offset of the end of the comment
     * @throws BadLocationException
     * @since 2.5
     */
    private int getCommentEnd(IDocument document,int offset)
    throws BadLocationException {
        boolean found = false;
        int index = offset;
        while (!found) {
            char character = document.getChar(index);
            if (character=='-') {
                String part = document.get(index,XMLUtils.END_COMMENT.length());
                if (XMLUtils.END_COMMENT.equals(part)) {
                    found = true;
                }
            }
            index++;
        }
        return index+3;
    }

    /**
     * Get the offset of the start of the comment
     * 
     * @param document to process
     * @param offset to start from
     * @return the offset of the start of the comment
     * @throws BadLocationException
     * @since 2.5
     */
    private int getCommentStart(IDocument document,int offset)
    throws BadLocationException {
        int index = offset;
        while (true) {
            char character = document.getChar(index);
            if (character=='<') {
                String part =
                    document.get(index,XMLUtils.START_COMMENT.length());
                if (XMLUtils.START_COMMENT.equals(part)) {
                    return index;
                }
            }
            index--;
        }
    }

    /**
     * Remove spaces and white space from the comment
     * 
     * @param document current editable
     * @param region of the comment
     * @since 2.5
     */
    private TagParser processComment(IDocument document,IRegion region) {
        try {
            // Remove the cr / lf's
            String comment =
                document.get(region.getOffset(),region.getLength()).replaceAll(
                    "(?:\\n|\\r)","");

            comment = comment.substring(XMLUtils.START_COMMENT.length());
            comment =
                comment.substring(0,
                    comment.length()-XMLUtils.END_COMMENT.length());
            comment = comment.trim();

            if (comment.startsWith(DocfactoLinksPlugin.DOCFACTO_LINK_TAG_NAME)) {
                theTagParser =
                    new TagParser(
                        "docfacto.link",
                        comment
                            .substring(DocfactoLinksPlugin.DOCFACTO_LINK_TAG_NAME
                                .length()));
                return theTagParser;
            }
        }
        catch (BadLocationException e) {
            // Can't happen as getCommentEnd and getCommentStart will have
            // caught these
        }

        return null;
    }

    /**
     * @see org.eclipse.jface.text.ITextHoverExtension#getHoverControlCreator()
     */
    @Override
    public IInformationControlCreator getHoverControlCreator() {
        return new HoverControlCreatorMarshaller(theTagParser);
    }

    /**
     * @see org.eclipse.jface.text.ITextHover#getHoverInfo(org.eclipse.jface.text.ITextViewer,
     * org.eclipse.jface.text.IRegion)
     */
    @Override
    public String getHoverInfo(ITextViewer textViewer,IRegion hoverRegion) {
        // Will not happen as getHoverInfo2 is implemented
        return null;
    }

    /**
     * @see org.eclipse.jface.text.ITextHover#getHoverRegion(org.eclipse.jface.text.ITextViewer,
     * int)
     */
    @Override
    public IRegion getHoverRegion(ITextViewer textViewer,int offset) {
        try {
            return getDocument(textViewer).getLineInformationOfOffset(offset);
        }
        catch (BadLocationException ignore) {
            // Nothing to do here
        }

        return null;
    }

    /**
     * Return the document for the viewer
     * 
     * @param textViewer to process
     * @return the document for the viewer
     * @since 2.4
     */
    private IDocument getDocument(ITextViewer textViewer) {
        StructuredTextViewer viewer = (StructuredTextViewer)textViewer;
        return viewer.getDocument();
    }
}
