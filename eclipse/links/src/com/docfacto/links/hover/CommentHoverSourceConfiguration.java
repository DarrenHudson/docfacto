package com.docfacto.links.hover;

import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.wst.xml.ui.StructuredTextViewerConfigurationXML;


/**
 * Add Links Hovers to XML documents
 *
 * @author dhudson - created 26 Nov 2013
 * @since 2.5
 */
public class CommentHoverSourceConfiguration extends StructuredTextViewerConfigurationXML {

    /**
     * @see org.eclipse.wst.sse.ui.StructuredTextViewerConfiguration#getTextHover(org.eclipse.jface.text.source.ISourceViewer, java.lang.String, int)
     */
    @Override
    public ITextHover getTextHover(ISourceViewer sourceViewer,
    String contentType,int stateMask) {
        
        //REMOVE:
        System.out.println("CommentHoverSourceConf...");
        
        // Install new comment hover
        if(contentType.equals("org.eclipse.wst.xml.XML_COMMENT") && stateMask == 0) {
            return new CommentHover();
        }
        
        return super.getTextHover(sourceViewer,contentType,stateMask);
    }
}
