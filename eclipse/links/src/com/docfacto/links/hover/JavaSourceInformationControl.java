package com.docfacto.links.hover;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.ui.ISharedImages;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.action.Action;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.common.StringUtils;
import com.docfacto.core.hovers.DualAbstractInformationControl;
import com.docfacto.core.hovers.LaunchAction;
import com.docfacto.core.hovers.TagletInput;
import com.docfacto.core.widgets.viewer.JavaFilePreview;
import com.docfacto.javadoc.CommentParser;
import com.docfacto.javadoc.TagParser;
import com.docfacto.links.LinksPathsManager;
import com.docfacto.links.plugin.DocfactoLinksPlugin;
import com.docfacto.links.scanners.JavaLinkScanner;
import com.docfacto.links.scanners.LinkScannerWrapper;
import com.docfacto.links.scanners.StandardUriHandler;
import com.docfacto.taglets.LinkTagletConstants;

/**
 * Java Source Hover
 * 
 * @author dhudson - created 13 Nov 2013
 * @since 2.5
 */
public class JavaSourceInformationControl extends
DualAbstractInformationControl {

    private JavaFilePreview theViewer;
    private LaunchAction theLaunchAction;
    
    /**
     * Constructor.
     * 
     * @param parent shell
     * @since 2.5
     */
    public JavaSourceInformationControl(Shell parent) {
        super(parent);
        ISharedImages images = (ISharedImages) JavaUI.getSharedImages();
        theLaunchAction =
        new LaunchAction(images.getImageDescriptor(ISharedImages.IMG_OBJS_CUNIT),
            "Open in Editor",this);
        create();
    }

    /**
     * @see org.eclipse.jface.text.IInformationControlExtension#hasContents()
     */
    @Override
    public boolean hasContents() {
        return true;
    }

    /**
     * @see com.docfacto.core.hovers.DualAbstractInformationControl#createContent(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void createContent(Composite parent) {
        theViewer = new JavaFilePreview(parent);
    }

    /**
     * @see com.docfacto.core.hovers.DualAbstractInformationControl#getToolBarActions()
     */
    @Override
    public List<Action> getToolBarActions() {
        List<Action> actions = new ArrayList<Action>(1);
        actions.add(theLaunchAction);

        return actions;
    }

    /**
     * @see com.docfacto.core.hovers.DualAbstractInformationControl#setInput(java.lang.Object)
     */
    @Override
    public void setInput(Object input) {
        TagletInput tagletInput = (TagletInput)input;
        TagParser parser = tagletInput.getParser();
        String uri = parser.getAttributeValue(TagParser.URI_ATTRIBUTE);

        try {
            String resolvedPath = LinksPathsManager.INSTANCE.getAbsolutePathFromShortenedPath(uri);

            File resolvedFile = new File(resolvedPath);

            // Add to the launch button
            theLaunchAction.setFileToLaunch(resolvedPath);
            
            String content = IOUtils.readFileAsString(resolvedFile);
            theViewer.setContent(content);
            theViewer.format();

            String docKey =
                parser.getAttributeValue(TagParser.LINK_KEY_ATTRIBUTE);

            if (!StringUtils.nullOrEmpty(docKey)) {
                // Lets position the source file to the correct link
                JavaLinkScanner scanner = new JavaLinkScanner();
                LinkScannerWrapper scannerWrapper = new LinkScannerWrapper(resolvedPath, "", new StandardUriHandler(), scanner);
                
                try {
                    scannerWrapper.scan();
                    // Search for key match
                    for (CommentParser commentParser:scanner.getComments()) {
                        for (final TagParser tagParser:commentParser
                            .getTags(LinkTagletConstants.LINK_TAGLET_NAME)) {
                            if (parser
                                .hasAttribute(TagParser.LINK_KEY_ATTRIBUTE)) {
                                if (docKey
                                    .equals(tagParser
                                        .getAttributeValue(TagParser.LINK_KEY_ATTRIBUTE))) {
                                    // Have found the line number, so lets set
                                    // the position
                                    theViewer.setLineNumber(tagParser
                                        .getLineNumber());
                                    // All done
                                    return;
                                }
                            }
                        }
                    }
                }
                catch (IOException ex) {
                    DocfactoLinksPlugin.logException(
                        "Unable to position hover ["+ex.getMessage()+"]",ex);
                }
            }
        }
        catch (DocfactoException ex) {
            DocfactoLinksPlugin.logException(
                "Unable to show hover ["+ex.getMessage()+"]",ex);
        }
    }

    /**
     * @see com.docfacto.core.hovers.DualAbstractInformationControl#computeSizeHint()
     */
    public Point computeSizeHint() {
        return new Point(800,800);
    }
}
