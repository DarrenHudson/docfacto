package com.docfacto.links.hover;

import java.io.File;

import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Shell;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.core.hovers.AbstractBrowserInformationControl;
import com.docfacto.core.hovers.TagletInput;
import com.docfacto.javadoc.TagParser;
import com.docfacto.links.LinksPathsManager;
import com.docfacto.links.plugin.DocfactoLinksPlugin;

/**
 * Basic links hover
 * 
 * @author dhudson - created 13 Nov 2013
 * @since 2.5
 */
public class BrowserInformationControl extends
AbstractBrowserInformationControl {

    /**
     * Constructor.
     * 
     * @param parentShell shell
     * @since 2.5
     */
    public BrowserInformationControl(Shell parentShell) {
        super(parentShell);
        create();
    }

    /**
     * @see org.eclipse.jface.text.IInformationControlExtension#hasContents()
     */
    @Override
    public boolean hasContents() {
        return true;
    }

    /**
     * @see com.docfacto.core.hovers.DualAbstractInformationControl#setInput(java.lang.Object)
     */
    @Override
    public void setInput(Object input) {
        TagletInput tagletInput = (TagletInput)input;
        TagParser parser = tagletInput.getParser();
        String uri = parser.getAttributeValue(TagParser.URI_ATTRIBUTE);

        try {
            String resolvedPath = LinksPathsManager.INSTANCE.getShortenedPathFromAbsolutePath(uri);

            String content = IOUtils.readFileAsString(new File(resolvedPath));
            getBrowser().setText(content);
            getBrowser().setVisible(true);
        }
        catch (DocfactoException ex) {
            DocfactoLinksPlugin.logException(
                "Unable to show hover ["+ex.getMessage()+"]",ex);
        }
    }

    /**
     * @see com.docfacto.core.hovers.DualAbstractInformationControl#computeSizeHint()
     */
    public Point computeSizeHint() {
        return new Point(800,800);
    }
}
