package com.docfacto.links.hover;

import java.util.List;

import org.eclipse.jdt.core.dom.Comment;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;

import com.docfacto.core.hovers.TagletHover;
import com.docfacto.core.hovers.TagletInput;
import com.docfacto.javadoc.TagParser;
import com.docfacto.links.links.LinksCommentParser;

/**
 * Hover capability for links
 * 
 * @author dhudson - created 20 Sep 2013
 * @since 2.4
 */
public class LinksTagletHover extends TagletHover {

    private TagParser theTagParser;

    /**
     * @see com.docfacto.core.hovers.TagletHover#getHoverInfo(org.eclipse.jface.text.ITextViewer,
     * org.eclipse.jface.text.IRegion, com.docfacto.javadoc.TagParser)
     */
    @Override
    public Object getHoverInfo(ITextViewer viewer,IRegion region,
    TagParser parser) {

        if ("docfacto.links".equals(parser.getTagName())) {
            // I have something to do
            theTagParser = parser;
            return new TagletInput(parser);
        }

        // Its not for me
        return null;
    }

    /**
     * @see com.docfacto.core.hovers.TagletHover#getHoverInfo(org.eclipse.jface.text.ITextViewer,
     * org.eclipse.jface.text.IRegion, org.eclipse.jdt.core.dom.Comment)
     */
    @Override
    public Object getHoverInfo(ITextViewer viewer,IRegion region,Comment node) {
        try {
            String comment =
                viewer.getDocument().get(node.getStartPosition(),
                    node.getLength());

            LinksCommentParser parser = new LinksCommentParser(1);
            parser.setCommentOffset(1);
            parser.parse(comment);

            List<TagParser> list = parser.getTags();

            if (list.size()>0) {
                // I have some information ...
                theTagParser = list.get(0);
                return new TagletInput(theTagParser);
            }
        }
        catch (BadLocationException e) {
            return null;
        }
        return null;
    }

    /**
     * @see com.docfacto.core.hovers.AbstractTextHover#getHoverControlCreator()
     */
    @Override
    public IInformationControlCreator getHoverControlCreator() {
        return new HoverControlCreatorMarshaller(theTagParser);
    }

    /**
     * @see com.docfacto.core.hovers.TagletHover#requiresComments()
     */
    @Override
    public boolean requiresComments() {
        return true;
    }
}
