package com.docfacto.links.hover;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.common.StringUtils;
import com.docfacto.core.hovers.DualAbstractInformationControl;
import com.docfacto.core.hovers.LaunchAction;
import com.docfacto.core.hovers.TagletInput;
import com.docfacto.core.widgets.viewer.SimpleXmlViewer;
import com.docfacto.javadoc.TagParser;
import com.docfacto.links.LinksPathsManager;
import com.docfacto.links.links.ILink;
import com.docfacto.links.plugin.DocfactoLinksPlugin;
import com.docfacto.links.scanners.LinkScannerWrapper;
import com.docfacto.links.scanners.StandardUriHandler;
import com.docfacto.links.scanners.XMLLinkScanner;

/**
 * XML Hover widget
 * 
 * @author dhudson - created 13 Nov 2013
 * @since 2.5
 */
public class XMLInformationControl extends
DualAbstractInformationControl {

    private SimpleXmlViewer theViewer;
    private LaunchAction theLaunchAction;

    /**
     * Constructor.
     * 
     * @param parent shell
     * @since 2.5
     */
    public XMLInformationControl(Shell parent) {
        super(parent);

        theLaunchAction = new LaunchAction(this);

        create();
    }

    /**
     * @see org.eclipse.jface.text.IInformationControlExtension#hasContents()
     */
    @Override
    public boolean hasContents() {
        return true;
    }

    /**
     * @see com.docfacto.core.hovers.DualAbstractInformationControl#createContent(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void createContent(Composite parent) {
        theViewer = new SimpleXmlViewer(parent);
        theViewer.getText().setBackground(
            parent.getDisplay().getSystemColor(SWT.COLOR_INFO_BACKGROUND));
    }

    /**
     * @see com.docfacto.core.hovers.DualAbstractInformationControl#getToolBarActions()
     */
    @Override
    public List<Action> getToolBarActions() {
        List<Action> actions = new ArrayList<Action>(1);
        actions.add(theLaunchAction);

        return actions;
    }

    /**
     * @see com.docfacto.core.hovers.DualAbstractInformationControl#setInput(java.lang.Object)
     */
    @Override
    public void setInput(Object input) {
        TagletInput tagletInput = (TagletInput)input;
        TagParser parser = tagletInput.getParser();
        String uri = parser.getAttributeValue(TagParser.URI_ATTRIBUTE);

        try {
            String resolvedPath = LinksPathsManager.INSTANCE.getAbsolutePathFromShortenedPath(uri);

            theLaunchAction.setFileToLaunch(resolvedPath);
            theLaunchAction.changeImage();

            String content = IOUtils.readFileAsString(new File(resolvedPath));
            theViewer.setContent(content);
            theViewer.format();

            String key = parser.getAttributeValue(TagParser.LINK_KEY_ATTRIBUTE);
            if (!StringUtils.nullOrEmpty(key)) {
                // Lets position the text
                XMLLinkScanner scanner = new XMLLinkScanner();
                LinkScannerWrapper scannerWrapper = new LinkScannerWrapper(resolvedPath, "", new StandardUriHandler(), scanner);
                try {
                    scannerWrapper.scan();
                    for (ILink link:scannerWrapper.getLinks()) {
                        if (key.equals(link.getKey())) {
                            theViewer.setLineNumber(link.getLineNumber());
                            break;
                        }
                    }
                }
                catch (IOException ignore) {
                }
            }
            // theViewer.setVisible(true);
        }
        catch (DocfactoException ex) {
            DocfactoLinksPlugin.logException(
                "Unable to show hover ["+ex.getMessage()+"]",ex);
        }
    }

    /**
     * @see com.docfacto.core.hovers.DualAbstractInformationControl#computeSizeHint()
     */
    public Point computeSizeHint() {
        return new Point(800,800);
    }
}
