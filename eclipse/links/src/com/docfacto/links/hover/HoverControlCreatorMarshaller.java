package com.docfacto.links.hover;

import org.eclipse.jface.text.AbstractReusableInformationControlCreator;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.swt.widgets.Shell;

import com.docfacto.common.StringUtils;
import com.docfacto.javadoc.TagParser;
import com.docfacto.links.FileTypeProvider;
import com.docfacto.links.UnknownFileTypeException;

/**
 * Depending on the link, display the correct hover
 * 
 * @author dhudson - created 13 Nov 2013
 * @since 2.5
 */
public class HoverControlCreatorMarshaller extends
AbstractReusableInformationControlCreator {

    private final TagParser theParser;

    /**
     * Constructor.
     * 
     * @param parser containing the link information
     * @since 2.5
     */
    public HoverControlCreatorMarshaller(TagParser parser) {
        theParser = parser;
    }

    /**
     * @see org.eclipse.jface.text.AbstractReusableInformationControlCreator#doCreateInformationControl(org.eclipse.swt.widgets.Shell)
     */
    @Override
    protected IInformationControl doCreateInformationControl(Shell parent) {

        String path = theParser.getAttributeValue(TagParser.URI_ATTRIBUTE);
        
        try {
            String fileType = FileTypeProvider.INSTANCE.getTypeForFile(path);
            
            if (fileType.equals("html") || fileType.equals("javascript")) {
            	return new BrowserInformationControl(parent);
            }
            else if (fileType.equals("java")) {
                return new JavaSourceInformationControl(parent);
            }
            else {
                return new XMLInformationControl(parent);
            }
        }
        catch (UnknownFileTypeException e) {
            String extension = StringUtils.getExtension(path);

            if (extension.equals("java")) {
                return new JavaSourceInformationControl(parent);
            }
            else if (extension.equals("dita")) {
                return new XMLInformationControl(parent);
            }
            else if (extension.equals("xml")) {
                return new XMLInformationControl(parent);
            }

        }
        // Can't return null at this point, must return something
        return new BrowserInformationControl(parent);
    }

    /**
     * @see org.eclipse.jface.text.AbstractReusableInformationControlCreator#canReuse(org.eclipse.jface.text.IInformationControl)
     */
    @Override
    public boolean canReuse(IInformationControl control) {
        return false;
    }

    /**
     * @see org.eclipse.jface.text.AbstractReusableInformationControlCreator#canReplace(org.eclipse.jface.text.IInformationControlCreator)
     */
    @Override
    public boolean canReplace(IInformationControlCreator creator) {
        return false;
    }

}
