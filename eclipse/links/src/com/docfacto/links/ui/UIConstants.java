package com.docfacto.links.ui;

import org.eclipse.swt.graphics.Color;

import com.docfacto.core.utils.SWTUtils;

public class UIConstants {
	private UIConstants() {
	}

	public static Color COLOR_BRAND = SWTUtils.getColor(0, 154, 184);
}
