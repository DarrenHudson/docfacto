package com.docfacto.links.ui;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Shell;

import com.docfacto.core.widgets.AbstractDocfactoDialog;

/**
 * For links popups
 * @author kporter - created Nov 13, 2013
 * @since 2.4.8
 */
public class LinksDialog extends AbstractDocfactoDialog {

    String theTitle;
    
    /**
     * Constructor for a Links Dialog
     * @param shell The parent shell
     * @param title The title text
     */
    public LinksDialog(Shell shell, String title) {
        super(shell);
        theTitle = title;
    }

    /**
     * @see com.docfacto.core.widgets.AbstractDocfactoDialog#getDialogTitle()
     */
    @Override
    protected String getDialogTitle() {
        return theTitle;
    }

    /**
     * @see com.docfacto.core.widgets.AbstractDocfactoDialog#getBrandColor()
     */
    @Override
    public Color getBrandColor() {
        return UIConstants.COLOR_BRAND;
    }

    /**
     * @see com.docfacto.core.widgets.AbstractDocfactoDialog#getBrandComplementColor()
     */
    @Override
    public Color getBrandComplementColor() {
        return UIConstants.COLOR_BRAND;
    }

}
