package com.docfacto.links.wizard;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.part.FileEditorInput;

import com.docfacto.config.XmlConfig;
import com.docfacto.core.DocfactoCorePlugin;
import com.docfacto.links.FileTypeProvider;
import com.docfacto.links.UnknownFileTypeException;
import com.docfacto.links.plugin.preference.ErrorDialog;

/**
 * Wizard Driver for creating Links.
 * 
 * @author dhudson - created 15 Jul 2013
 * @since 2.4
 */
public class LinksWizard extends Wizard implements INewWizard {

	private final LinksWizardData theWizardData;
	
	private final LinksWizardPage linksWizardPage;
	private final LinksPreviewPage linksPreviewPage;
	
	private IWorkbench workbench;
	private IWorkbenchWindow window;
	private IWorkbenchPage page;
	private IEditorPart activeEditor;
	private IFile file;
	private IPath path;
	
	/**
	 * Constructor
	 * @since 2.4.8
	 */
    public LinksWizard() {
    	theWizardData = new LinksWizardData();
        linksWizardPage = new LinksWizardPage(theWizardData);
        linksPreviewPage = new LinksPreviewPage(theWizardData);
    }
    
    /**
     * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench, org.eclipse.jface.viewers.IStructuredSelection)
     */
    @Override
    public void init(IWorkbench workbench, IStructuredSelection selection) {
    	this.workbench = workbench;
    	this.window = workbench.getActiveWorkbenchWindow();
    	this.page = window.getActivePage();
    	this.activeEditor = page.getActiveEditor();
    	this.file = ((FileEditorInput)activeEditor.getEditorInput()).getFile();
    	this.path = ((FileEditorInput)activeEditor.getEditorInput()).getPath();
    	
    	theWizardData.currentFileUri = path.toString();    	
    	theWizardData.activeEditor = activeEditor;
    	theWizardData.baseDir = DocfactoCorePlugin.getXmlConfig().getLinksConfig().getBasedir();
    	linksWizardPage.setCurrentFileUri(path.toString());
    	
        addPage(linksWizardPage);
        addPage(linksPreviewPage);
    }

    /**
     * Inserts the links to the necessary files.
     * 
     * @return true when finished
     * @since 2.4.8
     */
    public boolean performFinish() {
    	
    	/* 
    	 * Try to insert into the target file first because it is more likely inserting into the target file will cause problems
    	 * since there are more possibilities whilst the current file is definitely the one in the active editor.
    	 * 
    	 * If inserting into the target file failed, then return and don't continue inserting into the current file.
    	 */
        ILinkInserter targetFileLinkInserter = getLinkInserterForFile(linksWizardPage.getTargetFileUri());
    	
    	String newLinkAttributesForTargetFile = linksWizardPage.getNewLinkAttributesForTargetFile();
    	String targetFileUri = linksWizardPage.getTargetFileUri();
    	try {
			targetFileLinkInserter.insertLinkToFile(targetFileUri, newLinkAttributesForTargetFile,theWizardData.targetLineToInsertLink, theWizardData.targetCharOffset);
		} catch (LinkInsertException e) {
			showLinksInsertExceptionErrorDialog(e);
			return false;
		}
    	
    	ILinkInserter currentFileLinkInserter = getLinkInserterForFile(linksWizardPage.getCurrentFileUri());
    	
    	String newLinkAttributesForCurrentFile = linksWizardPage.getNewLinkAttributesForCurrentFile();
    	String currentFileUri = linksWizardPage.getCurrentFileUri();
    	try {
			currentFileLinkInserter.insertLinkToFile(currentFileUri, newLinkAttributesForCurrentFile, 0, 0);
		} catch (LinkInsertException e) {
			showLinksInsertExceptionErrorDialog(e);
		}
    	
        return true;
    }
    
    private void showLinksInsertExceptionErrorDialog(LinkInsertException exception) {
    	// Only show the error dialog if this exception was intended to be exposed to the user.
    	if (exception.isExposedToUser())
    		showErrorDialog("Failed to insert Link: " + exception.getMessage());
    }
    
    private void showErrorDialog(final String errorMessage) {
        ErrorDialog errorDialog = new ErrorDialog(getShell(), errorMessage);
        if (errorDialog.open()==Window.OK) {
            // Do Nothing
        }
    }

    private ILinkInserter getLinkInserterForFile(String file) {
		String fileTypeForFile;
    	try {
			fileTypeForFile =  FileTypeProvider.INSTANCE.getTypeForFile(file);
		} catch (UnknownFileTypeException e) {
			fileTypeForFile = FileTypeProvider.INSTANCE.DEFAULT_FILE_TYPE;
		}
    	
    	return getLinkInserterForFileType(fileTypeForFile);
    }
    
	private ILinkInserter getLinkInserterForFileType(String fileType) {
		if (fileType.equals("java")) {
			return new JavaLinkInserter(workbench);
		} else if (fileType.equals("javascript")) {
			return new JavaScriptLinkInserter(workbench, file, path);
		} else if (fileType.equals("markdown")) {
			return new MarkdownLinkInserter(workbench, file, path);
		} else if (fileType.equals("xml")) {
			return new XMLLinkInserter(workbench, file, path);
		} else if (fileType.equals("html")) {
			return new XMLLinkInserter(workbench, file, path);
		} else {
			return new XMLLinkInserter(workbench, file, path);
		}
	}
}
