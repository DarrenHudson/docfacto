package com.docfacto.links.wizard;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.MultiPageEditorPart;
import org.eclipse.ui.texteditor.ITextEditor;

import com.docfacto.common.DocfactoException;
import com.docfacto.core.utils.EclipseLineInsertException;
import com.docfacto.core.utils.EclipseLineInserter;
import com.docfacto.core.utils.PluginUtils;
import com.docfacto.core.utils.WorkbenchUtils;
import com.docfacto.internal.LineInserter;
import com.docfacto.links.plugin.DocfactoLinksPlugin;

/**
 * A link inserter for javascript files.
 * 
 * @author damonli - created Oct 31, 2013
 * @since 2.4.8
 */
public class XMLLinkInserter implements ILinkInserter {

    private IWorkbench theWorkbench;
    private IWorkbenchWindow theWindow;
    private IWorkbenchPage thePage;
    private IEditorPart theActiveEditor;
    private IFile theActiveFile;
    private IPath theActivePath;

    private final EclipseLineInserter lineInserter;
    
    /**
     * Constructor.
     * 
     * @param workbench the workbench with the active file
     * @param file the active file
     * @param path the active file path
     * @since 2.4.8
     */
    public XMLLinkInserter(IWorkbench workbench,IFile file,IPath path) {
        theWorkbench = workbench;
        theWindow = workbench.getActiveWorkbenchWindow();
        thePage = theWindow.getActivePage();
        theActiveEditor = thePage.getActiveEditor();
        theActiveFile = file;
        theActivePath = path;
        this.lineInserter = new EclipseLineInserter(workbench);
    }

    /**
     * @throws LinkInsertException 
     * @see com.docfacto.links.wizard.ILinkInserter#insertLinkToFile(java.lang.String, java.lang.String, int)
     */
    @Override
    public void insertLinkToFile(String fileUri,String linkAttributes,int lineNumber,int charOffset) throws LinkInsertException {

        try {
            insertLinkToActiveEditorFile(fileUri,linkAttributes);
        }
        catch (FileNotInActiveEditorException e) {
            try {
                addLinkToFileInWorkbench(fileUri,linkAttributes,lineNumber);
            }
            catch (FileNotInWorkbenchException e2) {
                try {
                    addLinkToFileInWorkspace(fileUri,linkAttributes,lineNumber);
                }
                catch (FileNotInWorkspaceException e3) {
                    addLinkToExternalFile(fileUri,linkAttributes,lineNumber);
                }
            }
        }
    }

    private void insertLinkToActiveEditorFile(String targetFileUri, String linkAttributes) throws FileNotInActiveEditorException, LinkInsertException {
        if (!theActivePath.toOSString().equals(targetFileUri))
            throw new FileNotInActiveEditorException();

        if (theActiveEditor instanceof ITextEditor) {
            ITextEditor activeTextEditor = (ITextEditor)theActiveEditor;
            ISelectionProvider selectionProvider = activeTextEditor.getSelectionProvider();
            ITextSelection selection = (ITextSelection)selectionProvider.getSelection();
            int selectedLine = selection.getStartLine()+1;
            addLinkToActiveFileAtLine(linkAttributes,selectedLine);
        }
        else if (theActiveEditor instanceof MultiPageEditorPart) {
            // If the editor is the xml editor, this will happen
            ITextSelection selection = (ITextSelection)theActiveEditor.getEditorSite().getSelectionProvider().getSelection();
            int selectedLine = selection.getStartLine()+1;
            addLinkToActiveFileAtLine(linkAttributes,selectedLine);
        }
        else {
            addLinkToActiveFileAtLine(linkAttributes,1);
        }
    }

    private void addLinkToFileInWorkspace(String fileUri,String linkAttributes, int lineNumber) throws FileNotInWorkspaceException, LinkInsertException {
        IPath targetPath = new Path(fileUri);
        IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
        IFile targetFile = workspaceRoot.getFileForLocation(targetPath);

        if (targetFile==null) {
            throw new FileNotInWorkspaceException();
        }
        else {
            try {
                String xmlLink = getLink(linkAttributes);
                insertLineIntoFileAtLineNumber(targetFile,lineNumber,xmlLink);
            }
            catch (IOException ex) {
                DocfactoLinksPlugin.logException("XMLLinkInserter: addLinkToFileInWorkspace "+ ex.getMessage(),ex);
            }
        }
    }

    private void addLinkToExternalFile(String fileUri,String linkAttributes,int lineNumber) {
        String xmlLink = getLink(linkAttributes);
        File file = new File(fileUri);
        LineInserter lineInserter = new LineInserter();
        try {
            lineInserter.insertLineIntoFileAtLineNumber(file,lineNumber,xmlLink);
        }
        catch (IOException ex) {
            DocfactoLinksPlugin.logException("XMLLinkInserter: addLinkToExternalFile "+ex.getMessage(),ex);
        }
    }

    private void addLinkToFileInWorkbench(String fileUri,String linkAttributes, int lineNumber) throws FileNotInWorkbenchException, LinkInsertException {
        List<IEditorPart> editors = WorkbenchUtils.getEditorsInWorkbench(theWorkbench);

        for (IEditorPart editor:editors) {
            IEditorInput editorInput = editor.getEditorInput();

            if (editorInput instanceof FileEditorInput) {
                IPath targetPath = ((FileEditorInput)editorInput).getPath();

                if (targetPath.toString().equals(fileUri)) {
                	
                	//Special beermat case
                	if (editor.getEditorSite().getId().equals("com.docfacto.beermat.plugin.BeermatMultiPage")) {
                		
                		boolean confirm = MessageDialog.openConfirm(theWorkbench.getActiveWorkbenchWindow().getShell(), 
                									"Link to SVG", 
                									"Adding a link to an SVG file currently opened by Beermat editor will close your Beermat editor. Is this ok?");
                		
                		if (confirm) {
	                		IFile iFile = ((FileEditorInput)editor.getEditorInput()).getFile();
	                		IWorkbenchPage page = editor.getEditorSite().getPage();
	                		boolean editorClosed = editor.getEditorSite().getPage().closeEditor(editor, true);
	                		try {
								addLinkToFileInWorkspace(fileUri, linkAttributes, lineNumber);
							} catch (FileNotInWorkspaceException e1) {
								//This shouldn't happen
								addLinkToExternalFile(fileUri, linkAttributes, lineNumber);
							}
                		}
                		else {
                			throw new LinkInsertException("Did not insert link into open Beermat editor", false);
                		}
                		
                		return;
                	}
                	
                	String xmlLink = getLink(linkAttributes);
                	try {
						lineInserter.insertLineIntoFileInEditor(editor, lineNumber, xmlLink);
						return;
					} catch (BadLocationException e) {
						DocfactoLinksPlugin.logMessage(e.getMessage());
			            throw new LinkInsertException(e.getMessage());
	                	
					} catch (EclipseLineInsertException e) {
						/* Could not insert into that particular editor so treat it as if the file could not
						 * be found in the editor so the link inserter will continue to try inserting it in other ways.
						 */
						throw new FileNotInWorkbenchException();
					}
                }
            }
        }

        throw new FileNotInWorkbenchException();
    }

    private void addLinkToActiveFileAtLine(String linkAttributes,int lineNumber) throws LinkInsertException {
        String xmlLink = getLink(linkAttributes);

        // First try to checkout the file
        boolean checkedOut = PluginUtils.checkoutFile(theActiveFile);
        
        if (!checkedOut) {
        	throw new LinkInsertException("Do not have permission to insert link into file: [" + theActiveFile.getFullPath().toString() + "]");
        }
        
        if (checkedOut) {
			insertLineIntoActiveFileAtLineNumber(lineNumber,xmlLink);
        }
    }

    private void insertLineIntoActiveFileAtLineNumber(int lineNumber, String lineToInsert) throws LinkInsertException {
        try {
			lineInserter.insertLineIntoFileInActiveEditor(lineNumber, lineToInsert);
		} catch (BadLocationException e) {
			DocfactoLinksPlugin.logMessage(e.getMessage());
            throw new LinkInsertException(e.getMessage());
		}
    }
    
    private void insertLineIntoFileAtLineNumber(IFile file,int lineNumber,String lineToInsert) throws IOException, LinkInsertException {
        try {
        	lineInserter.insertLineIntoFileInWorkspace(file, lineNumber, lineToInsert);
        }
        catch (CoreException ex) {
            DocfactoLinksPlugin.logException(
            		"XMLLinkInserter: insertLineIntoFileAtLineNumber"+
                    ex.getMessage(),ex);
        } catch (DocfactoException e) {
        	DocfactoLinksPlugin.logMessage(e.getMessage());
            throw new LinkInsertException(e.getMessage());
		} catch (BadLocationException e) {
			DocfactoLinksPlugin.logMessage(e.getMessage());
            throw new LinkInsertException(e.getMessage());
		}
    }

    /**
     * Get the link for the given link attributes
     * 
     * @param linkAttributes string of attributes
     * @return the concatenated string
     * @since 2.4.8
     */
    protected String getLink(String linkAttributes) {
        return "<!-- @docfacto.link "+linkAttributes+" -->";
    }
}
