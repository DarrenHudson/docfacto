package com.docfacto.links.wizard;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.ui.IWorkbench;

public class MarkdownLinkInserter extends XMLLinkInserter {

	/**
	 * Constructor.
	 * @param workbench the workbench which has the active editor
	 * @param file the active file
	 * @param path the active file path
	 * @since 2.4.8
	 */
	public MarkdownLinkInserter(IWorkbench workbench, IFile file, IPath path) {
		super(workbench, file, path);
	}
	
	@Override
	protected String getLink(String linkAttributes) {
		return "<!--- @docfacto.link " + linkAttributes + " -->";
	}
}
