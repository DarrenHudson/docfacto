package com.docfacto.links.wizard;

import java.io.File;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import com.docfacto.core.widgets.FileBrowserWidget;
import com.docfacto.core.widgets.listeners.FileTypeSelectionValidator;
import com.docfacto.javadoc.TagParser;
import com.docfacto.links.FileTypeProvider;
import com.docfacto.links.LinksPathsManager;
import com.docfacto.links.UnknownFileTypeException;
import com.docfacto.links.links.LinkType;
import com.docfacto.links.plugin.DocfactoLinksPlugin;
import com.docfacto.links.ui.UIConstants;

public class LinksWizardPage extends WizardPage implements KeyListener,SelectionListener {

    private Composite theContainer;
    private Text theVersion;
    private Text theKey;
    private Text theMetadata;
    private Combo theLinkType;
    protected Label theCurrentFileLabel;
    protected Label theCurrentFile;
    protected FileBrowserWidget theTargetFileBrowserWidget;

    private String currentFileUri;
    private String newLinkAttributesForTargetFile;
    private String newLinkAttributesForCurrentFile;

    private LinksWizardData theData;

    /**
     * Constructor.
     * 
     * @since 2.4.8
     */
    protected LinksWizardPage(LinksWizardData data) {
        super("Links Wizard");
        setDescription("Create a link.");
        setImageDescriptor(ImageDescriptor.createFromImage(DocfactoLinksPlugin
            .getImage(DocfactoLinksPlugin.LINKS_LOGO_64)));

        theData = data;
    }

    /**
     * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createControl(Composite parent) {
        theContainer = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        layout.numColumns = 2;

        theContainer.setLayout(layout);

        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 2;
        gd.grabExcessVerticalSpace = false;
        gd.heightHint = 30;

        // Create a listener for the file dialog widgets
        Listener fileDialogListener = new Listener() {
            /**
             * @see org.eclipse.swt.widgets.Listener#handleEvent(org.eclipse.swt.widgets.Event)
             */
            @Override
            public void handleEvent(Event arg0) {
                checkPageComplete();
            }
        };

        theCurrentFileLabel = new Label(theContainer, SWT.NONE);
        theCurrentFileLabel.setText("Linking file with:");

        theCurrentFile = new Label(theContainer, SWT.NONE);
        theCurrentFile.setText(getFileNameFromFilePath(getCurrentFileUri()));

        Label label = new Label(theContainer, SWT.BORDER | SWT.SINGLE);

        theTargetFileBrowserWidget = createFileWidget(theContainer);
        theTargetFileBrowserWidget.registerListener(fileDialogListener);
        theTargetFileBrowserWidget.setLayoutData(gd);
        theTargetFileBrowserWidget.setInternalValidator(new FileTypeSelectionValidator("Valid", "Choose a valid file"));
        theTargetFileBrowserWidget.setBrandColor(UIConstants.COLOR_BRAND);

        theTargetFileBrowserWidget.registerListener(new Listener() {
			@Override
			public void handleEvent(Event event) {
				String currentTargetFileType;
				
				try {
					currentTargetFileType = FileTypeProvider.INSTANCE.getTypeForFile(theTargetFileBrowserWidget.getSelectedPath());
				} catch (UnknownFileTypeException e) {
					currentTargetFileType = FileTypeProvider.INSTANCE.DEFAULT_FILE_TYPE;
				}
				
				tryToGuessLinkType(currentTargetFileType);
			}
        });
        
        label = new Label(theContainer, SWT.NONE);
        label.setText("Key");

        theKey = new Text(theContainer, SWT.BORDER | SWT.SINGLE);
        theKey.setText("");
        theKey.setLayoutData(gd);
        theKey.addKeyListener(this);

        label = new Label(theContainer, SWT.NONE);
        label.setText("Version");

        theVersion = new Text(theContainer, SWT.BORDER | SWT.SINGLE);
        theVersion.setText("");
        theVersion.addKeyListener(this);
        theVersion.setLayoutData(gd);

        label = new Label(theContainer, SWT.BORDER | SWT.SINGLE);
        label.setText("Linking To");

        theLinkType = new Combo(theContainer, SWT.READ_ONLY | SWT.DROP_DOWN);
        theLinkType.setItems(LinkType.getAttributeValues());
        theLinkType.addKeyListener(this);
        theLinkType.addSelectionListener(this);
        theLinkType.select(0);
        theLinkType.setLayoutData(gd);

        label = new Label(theContainer, SWT.BORDER | SWT.SINGLE);
        label.setText("Metadata");

        theMetadata = new Text(theContainer, SWT.BORDER | SWT.SINGLE);
        theMetadata.setText("");
        theMetadata.setLayoutData(gd);

        // Required to avoid an error in the system
        setControl(theContainer);
        setPageComplete(false);
    }
    
    private void tryToGuessLinkType(String fileType) {
    	if (fileType.equals("java") || fileType.equals("javascript") || fileType.equals("html"))
			setLinkType(LinkType.SOURCE_LINK);
		else
			setLinkType(LinkType.DOC_LINK);
    }
    
    private void setLinkType(LinkType linkType) {
    	for (int i=0; i < theLinkType.getItemCount(); i++) {
    		if (theLinkType.getItem(i).equals(linkType.getAttributeValue()))
    			theLinkType.select(i);
    	}
    }

    @SuppressWarnings("unused")
    private String getFileExtension(String fileName) {
        int positionOfLastDot = fileName.lastIndexOf('.');

        if (positionOfLastDot >= 0)
            return fileName.substring(positionOfLastDot + 1);
        else
            return fileName;
    }

    private String getFileNameFromFilePath(String filePath) {
        File file = new File(filePath);
        return file.getName();
    }

    /**
     * If the page is complete allow to move to the next page
     * 
     * @since 2.4
     */
    private void checkPageComplete() {
        boolean targetFileHasBeenSet = validateTargetFileWidget();
        boolean keyHasBeenSet = !getKeyText().trim().isEmpty();
        boolean linkTypeHasBeenSet = !getLinkTypeText().trim().isEmpty();

        if (targetFileHasBeenSet && keyHasBeenSet && linkTypeHasBeenSet) {
        	String targetFileLinkType = LinkType.getCorrespondingAttributeValue(getLinkTypeText());
            setTargetFileNewLinkAttributes(getKeyText(), getCurrentFileUri(), getVersionText(), targetFileLinkType, getMetadataText());
            setCurrentFileNewLinkAttributes(getKeyText(), getTargetFileUri(), getVersionText(), getLinkTypeText(), getMetadataText());

            theData.targetFileUri = getTargetFileUri();
            try {
                theData.targetFileType = FileTypeProvider.INSTANCE.getTypeForFile(getTargetFileUri());
            }
            catch (UnknownFileTypeException e) {
                theData.targetFileType = FileTypeProvider.INSTANCE.DEFAULT_FILE_TYPE;
            }
            // If user presses finish now, assume that link is at top, so offset and line number are 1
            theData.targetLineToInsertLink = 1;
            theData.targetCharOffset = 1;

            setPageComplete(true);
            return;
        }

        setPageComplete(false);
    }

    private boolean validateTargetFileWidget() {
        return theTargetFileBrowserWidget.validate();
    }

    protected String getTargetFileUri() {
        return theTargetFileBrowserWidget.getSelectedPath();
    }

    public void setTargetFileNewLinkAttributes(String key,String currentFileUri,String version,String linkType, String metadata) {
        String currentFileUriResolved = LinksPathsManager.INSTANCE.getShortenedPathFromAbsolutePath(currentFileUri);

        String newLinkAttributes = getLinkAttributesString(key, currentFileUriResolved, version, linkType, metadata);

        this.setNewLinkAttributesForTargetFile(newLinkAttributes);
    }
    
    private String getLinkAttributesString(String key, String uri, String version, String linkType, String metadata) {
        String newLink = "";
        
        newLink += TagParser.LINK_KEY_ATTRIBUTE + "=\"" + key + "\"";
        
        newLink += " " + TagParser.URI_ATTRIBUTE + "=\"" + uri+ "\"";
        
        if (version != null && !version.isEmpty())
        	newLink += " " + TagParser.VERSION_ATTRIBUTE + "=\"" + version + "\"";
        
        newLink += " " + TagParser.LINK_TO_ATTRIBUTE + "=\"" + linkType + "\"";
        
        if (metadata != null && !metadata.isEmpty())
        	newLink += " " + TagParser.METADATA_ATTRIBUTE + "=\"" + metadata + "\"";
        
        return newLink;
    }

    public void setCurrentFileNewLinkAttributes(String key,String targetFileUri,String version,String linkType,String metadata) {
        String targetFileUriResolved = LinksPathsManager.INSTANCE.getShortenedPathFromAbsolutePath(targetFileUri);
        
        String newLink = getLinkAttributesString(key, targetFileUriResolved, version, linkType, metadata);

        this.setNewLinkAttributesForCurrentFile(newLink);
    }

    /**
     * @see org.eclipse.swt.events.KeyListener#keyPressed(org.eclipse.swt.events.KeyEvent)
     */
    @Override
    public void keyPressed(KeyEvent event) {
    }

    /**
     * @see org.eclipse.swt.events.KeyListener#keyReleased(org.eclipse.swt.events.KeyEvent)
     */
    @Override
    public void keyReleased(KeyEvent event) {
        // Check all of the text and see if its complete
        checkPageComplete();
    }

    /**
     * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
     */
    @Override
    public void widgetSelected(SelectionEvent e) {
        checkPageComplete();
    }

    /**
     * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
     */
    @Override
    public void widgetDefaultSelected(SelectionEvent e) {
        checkPageComplete();
    }

    /**
     * Return the version, must have a version
     * 
     * @return the version
     * @since 2.4
     */
    public String getVersionText() {
        return theVersion.getText();
    }

    /**
     * Return the key value, must have a key
     * 
     * @return the key
     * @since 2.4
     */
    public String getKeyText() {
        return theKey.getText();
    }

    /**
     * Return the link type, must have a link type
     * 
     * @return the link type
     * @since 2.4.8
     */
    public String getLinkTypeText() {
        return theLinkType.getText();
    }

    /**
     * Return the metadata
     * 
     * <ul>
     * <li>This is a list item</li>
     * <li>This is another list item</li>
     * </ul>
     * 
     * @return the metadata if any
     * @since 2.4
     */
    public String getMetadataText() {
        return theMetadata.getText();
    }

    /**
     * Create a doc or source file browser
     * 
     * @param parent composite
     * @return a File Browser
     * @since 2.4
     */
    private FileBrowserWidget createFileWidget(Composite parent) {
        return new FileBrowserWidget(parent, SWT.NONE, "Target file path: ", theData.baseDir);
    }

    public String getCurrentFileUri() {
        return currentFileUri;
    }

    public void setCurrentFileUri(String currentFileUri) {
        this.currentFileUri = currentFileUri;
    }

    public String getNewLinkAttributesForTargetFile() {
        return newLinkAttributesForTargetFile;
    }

    public void setNewLinkAttributesForTargetFile(String newLinkAttributesForTargetFile) {
        this.newLinkAttributesForTargetFile = newLinkAttributesForTargetFile;
    }

    public String getNewLinkAttributesForCurrentFile() {
        return newLinkAttributesForCurrentFile;
    }

    public void setNewLinkAttributesForCurrentFile(String newLinkAttributesForCurrentFile) {
        this.newLinkAttributesForCurrentFile = newLinkAttributesForCurrentFile;
    }

}
