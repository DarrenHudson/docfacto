package com.docfacto.links.wizard;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.eclipse.core.filebuffers.FileBuffers;
import org.eclipse.core.filebuffers.ITextFileBuffer;
import org.eclipse.core.filebuffers.ITextFileBufferManager;
import org.eclipse.core.filebuffers.LocationKind;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jdt.core.dom.TextElement;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.TextEdit;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.texteditor.ITextEditor;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.core.utils.ASTUtils;
import com.docfacto.core.utils.CompleteNodeFinder;
import com.docfacto.core.utils.EclipseLineInsertException;
import com.docfacto.core.utils.EclipseLineInserter;
import com.docfacto.core.utils.PluginUtils;
import com.docfacto.core.utils.WorkbenchUtils;
import com.docfacto.links.plugin.DocfactoLinksPlugin;

/**
 * A link inserter for java files.
 * 
 * @author damonli - created Oct 31, 2013
 * @since 2.4.8
 */
public class JavaLinkInserter implements ILinkInserter {

    private final IWorkbench workbench;
    private IWorkbenchWindow window;
    private IWorkbenchPage page;
    private IEditorPart activeEditor;
    private IFile activeFile;
    private IPath activePath;

    private final EclipseLineInserter lineInserter;
    /**
     * Constructor.
     * 
     * @param workbench the current workbench which has the active editor
     * @since 2.4.8
     */
    public JavaLinkInserter(IWorkbench workbench) {
        this.workbench = workbench;
        this.window = workbench.getActiveWorkbenchWindow();
        this.page = window.getActivePage();
        this.activeEditor = page.getActiveEditor();
        this.activeFile = ((FileEditorInput)activeEditor.getEditorInput()).getFile();
        this.activePath = ((FileEditorInput)activeEditor.getEditorInput()).getPath();
        this.lineInserter = new EclipseLineInserter(workbench);
    }

    /**
     * @see com.docfacto.links.wizard.ILinkInserter#insertLinkToFile(java.lang.String, java.lang.String, int, int)
     */
    @Override
    public void insertLinkToFile(String fileUri,String linkAttributes,int lineNumber,int charOffset) throws LinkInsertException {
    	
    	// This is necessary because compilation units can only be made from files with '.java'
    	if(!fileUri.endsWith(".java")) {
    		try {
    			File selectedFile = new File(fileUri);
				File newTempJavaFile = File.createTempFile("tempJavaFile", ".java");
				IOUtils.copyFile(selectedFile, newTempJavaFile);
				insertLinkToFile(newTempJavaFile.getAbsolutePath(), linkAttributes, lineNumber, charOffset);
				IOUtils.copyFile(newTempJavaFile, selectedFile);
				newTempJavaFile.delete();
				return;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DocfactoException e) {
				throw new LinkInsertException(e.getMessage(), e);
			}
    	}
    	
        try {
            insertLinkToActiveEditorFile(fileUri,linkAttributes);
        }
        catch (FileNotInActiveEditorException e) {
            try {
                addLinkToFileInWorkbench(fileUri,linkAttributes,charOffset);
            }
            catch (FileNotInWorkbenchException e2) {
                try {
                    addLinkToFileInWorkspace(fileUri,linkAttributes,charOffset);
                }
                catch (FileNotInWorkspaceException e3) {
                    addLinkToExternalFile(fileUri,linkAttributes,charOffset);
                }
            }
        }
    }

    private void insertLinkToActiveEditorFile(String targetFileUri,String linkAttributes) throws FileNotInActiveEditorException, LinkInsertException {

        if (!this.activePath.toOSString().equals(targetFileUri))
            throw new FileNotInActiveEditorException();

        boolean checkedOut = PluginUtils.checkoutFile(activeFile,activeEditor.getSite().getShell());
        
    	if (!checkedOut) {
        	throw new LinkInsertException("Do not have permission to insert link into file: [" + activeFile.getFullPath().toString() + "]");
        }
    	else {
            ICompilationUnit iCompilationUnit = JavaCore.createCompilationUnitFrom(activeFile);
            CompilationUnit compilationUnit = ASTUtils.getCompilationUnitFor(iCompilationUnit);
            compilationUnit.recordModifications();

            ITextFileBufferManager bufferManager = FileBuffers.getTextFileBufferManager();
            ITextFileBuffer textFileBuffer = bufferManager.getTextFileBuffer(activePath,LocationKind.NORMALIZE);
            IDocument document = textFileBuffer.getDocument();

            AST ast = compilationUnit.getAST();
            ASTRewrite rewriter = ASTRewrite.create(ast);

            ITextEditor activeTextEditor = (ITextEditor)activeEditor;
            ITextSelection selection = (ITextSelection)activeTextEditor.getSelectionProvider().getSelection();
            int charOffset = selection.getOffset();
            ASTNode selectedNode = CompleteNodeFinder.perform(compilationUnit, charOffset, selection.getLength());

            if (selectedNode.getNodeType() == ASTNode.LINE_COMMENT || selectedNode.getNodeType() == ASTNode.BLOCK_COMMENT) {
            	int lineNumber = compilationUnit.getLineNumber(charOffset);
            	String javaLink = getSingleLineJavaLink(linkAttributes);
				insertLinkIntoEditor(activeEditor, lineNumber, javaLink);
            }
            else {
	            addLinkToNode(rewriter,compilationUnit,selectedNode,linkAttributes);
	            applyCompilationUnitChangesToDocument(compilationUnit,document,iCompilationUnit);
            }
    	}
    }

	/**
     * This method adds a link to a given node. <br />
     * 
     * If the node is a javadoc node, then the link will be added directly to the javadoc node. Otherwise go through the
     * parents of the node to get the BodyDeclaration node and then its javadoc node (if there is no javadoc node then
     * create and add one to the body declaration) to add the link to. <br />
     * 
     * If a parent is null, the chances are it is outside the class or it is a comment node. If it is a comment node,
     * add the link to it, otherwise, do not add the link as it is unknown what should be done to the node. (Do not add
     * links outside a class) <br />
     * 
     * If a parent found is a block, it means the node was a statement within a method so the link can be added to that
     * statement.
     * 
     * @param rewriter the active editor's AST rewriter
     * @param compilationUnit the compilation unit of the node
     * @param node the node to add the link to.
     * @param linkAttributes the linkAttributes of the link to add to the node.
     */
    private void addLinkToNode(ASTRewrite rewriter, CompilationUnit compilationUnit,ASTNode node,String linkAttributes) {
        // If node is javadoc, just add the link to the javadoc
        if (node instanceof Javadoc) {
            addLinkAsJavadocTagToNode((Javadoc)node,linkAttributes);
        }
        else {
            // Go through the parents to find the BodyDeclaration node which has
            // the javadoc
            while (!(node instanceof BodyDeclaration)) {
                ASTNode parentNode = node.getParent();

                if (parentNode==null) {
                    System.out.println("Don't know where to add this node, it has a null parent");
                    return;
                }

                node = parentNode;
            }

            // Get the javadoc node of the body declaration
            BodyDeclaration decl = (BodyDeclaration)node;
            Javadoc javadocNode = decl.getJavadoc();

            if (javadocNode==null) {
                // No javadoc exists for this node, so create a javadoc node and
                // assign it to the body declaration
                javadocNode = compilationUnit.getAST().newJavadoc();
                decl.setJavadoc(javadocNode);
            }

            addLinkAsJavadocTagToNode(javadocNode,linkAttributes);
        }
    }

    private void addLinkToFileInWorkspace(String targetFileUri,String linkAttributes,int charOffset) throws FileNotInWorkspaceException, LinkInsertException {
        IPath targetPath = new Path(targetFileUri);
        IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
        IFile targetFile = workspaceRoot.getFileForLocation(targetPath);

        if (targetFile==null) {
            throw new FileNotInWorkspaceException();
        }
        else {
            addLinkToFileWithinEclipse(targetFile, linkAttributes, charOffset);
        }
    }
    
    private void addLinkToFileWithinEclipse(IFile targetFile, String linkAttributes, int charOffset) throws LinkInsertException {
    	// Checkout file first
        boolean checkedOut = PluginUtils.checkoutFile(targetFile);
        
        if (!checkedOut) {
        	throw new LinkInsertException("Do not have permission to insert link into file: [" + targetFile.getFullPath().toString() + "]");
        }
        
        IPath targetPath = targetFile.getFullPath();

        ICompilationUnit iCompilationUnit = JavaCore.createCompilationUnitFrom(targetFile);

        CompilationUnit compilationUnit = ASTUtils.getCompilationUnitFor(iCompilationUnit);
        compilationUnit.recordModifications();

        AST ast = compilationUnit.getAST();
        ASTRewrite rewriter = ASTRewrite.create(ast);

        ASTNode selectedNode = CompleteNodeFinder.perform(compilationUnit,charOffset,1);

        if (selectedNode.getNodeType() == ASTNode.LINE_COMMENT || selectedNode.getNodeType() == ASTNode.BLOCK_COMMENT) {
        	int lineNumber = compilationUnit.getLineNumber(charOffset);
        	insertLinkAsBlockCommentIntoWorkspaceFile(targetFile, lineNumber, linkAttributes);
        }
        else {
        	addLinkToNode(rewriter,compilationUnit,selectedNode,linkAttributes);
        
            ITextFileBufferManager bufferManager = FileBuffers.getTextFileBufferManager();
            try {
                bufferManager.connect(targetPath,LocationKind.NORMALIZE,null);
            }
            catch (CoreException e) {
            	throwAndLogLinkInsertException(e.getMessage(), e);
            }
            
            ITextFileBuffer textFileBuffer = bufferManager.getTextFileBuffer(targetPath,LocationKind.NORMALIZE);
            IDocument document = textFileBuffer.getDocument();
            
            applyCompilationUnitChangesToDocument(compilationUnit, document, iCompilationUnit);
            
            try {
				textFileBuffer.commit(null, true);
				bufferManager.disconnect(targetPath, LocationKind.NORMALIZE, null);
                targetFile.refreshLocal(IResource.DEPTH_ZERO,null);
            }
            catch (CoreException e) {
            	throwAndLogLinkInsertException(e.getMessage(), e);
            }
        }
	}

	private void addLinkToFileInWorkbench(String targetFileUri,String linkAttributes,int charOffset) throws FileNotInWorkbenchException, LinkInsertException {
		List<IEditorPart> editors = WorkbenchUtils.getEditorsInWorkbench(workbench);
    	
    	for (IEditorPart editor:editors) {
    		IEditorInput editorInput = editor.getEditorInput();
    		if (editorInput instanceof FileEditorInput) {
    			IPath targetPath = ((FileEditorInput)editorInput).getPath();

                if (targetPath.toString().equals(targetFileUri)) {
    				addLinkToEditor(editor, linkAttributes, charOffset, false);
    				return;
    			}
    		}
    	}
    	
    	// No file in the workbench matched the target file
    	throw new FileNotInWorkbenchException();
    }
    
    private void addLinkToEditor(IEditorPart editor, String linkAttributes,int charOffset, boolean b) throws LinkInsertException {
    	
    	IFile targetFile = ((FileEditorInput)editor.getEditorInput()).getFile();
        IPath targetPath = ((FileEditorInput)editor.getEditorInput()).getPath();
    
    	// Checkout file first
        boolean checkedOut = PluginUtils.checkoutFile(targetFile);
        
        if (!checkedOut) {
        	throw new LinkInsertException("Do not have permission to insert link into file: [" + targetFile.getFullPath().toString() + "]");
        }

        ICompilationUnit iCompilationUnit = JavaCore.createCompilationUnitFrom(targetFile);

        CompilationUnit compilationUnit = ASTUtils.getCompilationUnitFor(iCompilationUnit);
        compilationUnit.recordModifications();

        AST ast = compilationUnit.getAST();
        ASTRewrite rewriter = ASTRewrite.create(ast);
        
        ITextFileBufferManager bufferManager = FileBuffers.getTextFileBufferManager();
        ITextFileBuffer textFileBuffer = bufferManager.getTextFileBuffer(targetPath,LocationKind.NORMALIZE);
        IDocument document = textFileBuffer.getDocument();

        ASTNode selectedNode = CompleteNodeFinder.perform(compilationUnit,charOffset,1);

        if (selectedNode == null) {
        	int lineNumber = compilationUnit.getLineNumber(charOffset);
        	
        	if (lineNumber <= 0) {
        		lineNumber = 1;
        	}
        	String singleLineJavaLink = getSingleLineJavaLink(linkAttributes);
        	insertLinkIntoEditor(editor, lineNumber, singleLineJavaLink);
        	return;
        }
        
        if (selectedNode.getNodeType() == ASTNode.LINE_COMMENT || selectedNode.getNodeType() == ASTNode.BLOCK_COMMENT) {
        	int lineNumber = compilationUnit.getLineNumber(charOffset);
        	String singleLineJavaLink = getSingleLineJavaLink(linkAttributes);
        	insertLinkIntoEditor(editor, lineNumber, singleLineJavaLink);
	    }
	    else {
	        addLinkToNode(rewriter,compilationUnit,selectedNode,linkAttributes);
	        applyCompilationUnitChangesToDocument(compilationUnit,document,iCompilationUnit);
	    }
	}
    
    private void insertLinkIntoEditor(IEditorPart editor, int lineNumber, String linkToInsert) throws LinkInsertException {
    	try {
			lineInserter.insertLineIntoFileInEditor(editor, lineNumber, linkToInsert);
		} catch (BadLocationException e) {
			throwAndLogLinkInsertException(e.getMessage(), e);
		} catch (EclipseLineInsertException e) {
			throwAndLogLinkInsertException(e.getMessage(), e);
		}
    }

	private void insertLinkAsBlockCommentIntoWorkspaceFile(IFile file,int lineNumber, String linkAttributes) throws LinkInsertException {
		String javaLink = getSingleLineJavaLink(linkAttributes);
		try {
			lineInserter.insertLineIntoFileInWorkspace(file, lineNumber, javaLink);
		} catch (IOException e) {
			throwAndLogLinkInsertException(e.getMessage(), e);
		} catch (CoreException e) {
			throwAndLogLinkInsertException(e.getMessage(), e);
		} catch (DocfactoException e) {
			throwAndLogLinkInsertException(e.getMessage(), e);
		} catch (BadLocationException e) {
			throwAndLogLinkInsertException(e.getMessage(), e);
		}
	}
    
    private String getSingleLineJavaLink(String linkAttributes) {
    	return "/* @docfacto.link " + linkAttributes + " */";
    }
    
    private void addLinkToExternalFile(String targetFileUri, String linkAttributes, int lineNumber) throws LinkInsertException {
        File file = new File(targetFileUri);
    	//Use the current time in milliseconds as a way to create a unique temp file name
        String newTempFileName = "" + System.currentTimeMillis() + ".java";
    	IFile tempIFile = activeFile.getProject().getFile(newTempFileName);
    	
    	if (!tempIFile.exists()) {
			try {
				// Create a temporary file in the workspace
				tempIFile.create(new FileInputStream(file), true, null);
				String newFilePath = PluginUtils.getFileFromIFile(tempIFile).getAbsolutePath();
				// Add the link to the temporary file
				addLinkToFileInWorkspace(newFilePath, linkAttributes, lineNumber);
				
				File tempFile = new File(newFilePath);
				// Write the contents of the temporary file back to the original file
				IOUtils.copyFile(tempFile, file);
				tempFile.delete();
				tempIFile.delete(true, null);
				
			} catch (FileNotFoundException e) {
				throwAndLogLinkInsertException(e.getMessage(), e);
			} catch (CoreException e) {
				throwAndLogLinkInsertException(e.getMessage(), e);
			} catch (FileNotInWorkspaceException e) {
				throwAndLogLinkInsertException(e.getMessage(), e);
			} catch (IOException e) {
				// Problem when overwriting file with new text
				throwAndLogLinkInsertException(e.getMessage(), e);
			} catch (DocfactoException e) {
				// PRoblem with copying the contents of the temp file to the actual file
				throwAndLogLinkInsertException(e.getMessage(), e);
			}
    	}
    }

    private void addLinkAsJavadocTagToNode(Javadoc javadocNode, String linkAttributes) {
        TagElement newTagElement = javadocNode.getAST().newTagElement();
        newTagElement.setTagName("@docfacto.link");

        TextElement textElement = javadocNode.getAST().newTextElement();
        textElement.setText(linkAttributes);
        newTagElement.fragments().add(textElement);
        javadocNode.tags().add(newTagElement);
    }

    private void applyCompilationUnitChangesToDocument(CompilationUnit compilationUnit, IDocument document, ICompilationUnit iCompilationUnit) throws LinkInsertException {
    	TextEdit edit = compilationUnit.rewrite(document,iCompilationUnit.getJavaProject().getOptions(true));
        
        try {
            edit.apply(document);
        }
        catch (MalformedTreeException e) {
        	throwAndLogLinkInsertException("Unable to apply java changes to document", e);
        }
        catch (BadLocationException e) {
        	throwAndLogLinkInsertException("Unable to apply java changes to document", e);
        }
    }
    
    private void throwAndLogLinkInsertException(String message, Throwable e) throws LinkInsertException {
    	if (e == null) {
    		DocfactoLinksPlugin.logMessage(message);
            throw new LinkInsertException(message);
    	}
    	else {
	    	DocfactoLinksPlugin.logException(message,e);
	        throw new LinkInsertException(message, e);
    	}
    }
}
