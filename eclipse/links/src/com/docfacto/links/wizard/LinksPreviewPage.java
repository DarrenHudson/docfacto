package com.docfacto.links.wizard;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CaretEvent;
import org.eclipse.swt.custom.CaretListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

import com.docfacto.common.DocfactoException;
import com.docfacto.common.IOUtils;
import com.docfacto.core.widgets.viewer.IPreviewer;
import com.docfacto.core.widgets.viewer.JavaFilePreview;
import com.docfacto.core.widgets.viewer.LineHighlighter;
import com.docfacto.core.widgets.viewer.SimpleXmlViewer;
import com.docfacto.links.ui.UIConstants;

/**
 * A wizard page that previews the target file and chooses a link insertion line.
 * 
 * @author kporter - created Nov 8, 2013
 * @since 2.4.8
 */
public class LinksPreviewPage extends WizardPage {

    private LinksWizardData theData;
    private IPreviewer theTargetPreview;
    private Composite targetContainer;
    private Composite currentContainer;

    Composite container;

    /**
     * Create the wizard.
     */
    public LinksPreviewPage(LinksWizardData data) {
        super("Links Wizard");
        // Choose which line in the target file to insert the link
        setDescription("Choose location in the target file to insert the link");
        // setImageDescriptor(ImageDescriptor.createFromImage(DocfactoLinksPlugin.getImage(DocfactoLinksPlugin.LINKS_LOGO_64)));
        theData = data;
    }

    /**
     * Create contents of the wizard.
     * 
     * @param parent
     */
    public void createControl(Composite parent) {
        container = new Composite(parent, SWT.NULL);

        GridLayout gl_container = new GridLayout();
        gl_container.makeColumnsEqualWidth = false;
        gl_container.numColumns = 3;
        container.setLayout(gl_container);

        Composite topArea = new Composite(container, SWT.NONE);
        topArea.setLayout(new RowLayout(SWT.HORIZONTAL));
        topArea.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 3, 1));
        Label descrip = new Label(topArea, SWT.NONE);
        descrip.setText("Choose which line in the target file in which to insert the link");

        Label arrow = new Label(container, SWT.NONE);
        arrow.setText("Choose line ->");

//        SourceViewer textViewer = new SourceViewer(container, null, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
//        StyledText styledText = textViewer.getTextWidget();
//        styledText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
//        textViewer.setDocument(new Document(content));
        setControl(container);
    }

    /**
     * 
     */
    public void createViewerArea() {
        cleanContainer();
        currentContainer = new Composite(container, SWT.NONE);
        currentContainer.setLayout(new FillLayout());
        currentContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));

        Label arrow = new Label(container, SWT.NONE);
        arrow.setText("Choose line ->");

        targetContainer = new Composite(container, SWT.NONE);
        targetContainer.setLayout(new FillLayout(SWT.HORIZONTAL));
        targetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

        try {
        	String content = IOUtils.readFileAsString(new File(theData.targetFileUri));
            // ITextEditor activeTextEditor = (ITextEditor)theData.activeEditor;
            // ITextSelection selection =(ITextSelection)activeTextEditor.getSelectionProvider().getSelection();

            String targetFileType = theData.targetFileType;
            
            if (targetFileType.equals("java") || targetFileType.equals("javascript")) {
            	theTargetPreview = new JavaFilePreview(targetContainer, UIConstants.COLOR_BRAND);
            }
            else {
                theTargetPreview = new SimpleXmlViewer(targetContainer);
                ((SimpleXmlViewer)theTargetPreview).setHighlightColor(UIConstants.COLOR_BRAND);
            }

            theTargetPreview.format(content);
            theTargetPreview.getText().addCaretListener(new LineHighlighter(UIConstants.COLOR_BRAND));

            theTargetPreview.getText().addCaretListener(new CaretListener() {
                @Override
                public void caretMoved(CaretEvent event) {
                    theData.targetLineToInsertLink = theTargetPreview.getLine() + 1;
                    theData.targetCharOffset = event.caretOffset;
                    setPageComplete(true);
                }
            });
        }
        catch (DocfactoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    private void cleanContainer() {
        for (Control control:container.getChildren()) {
            control.dispose();
        }
    }

    @Override
    public void setVisible(boolean visible) {
        if (visible) {
            createViewerArea();
        }
        super.setVisible(visible);
    }

//    private static String testComment = "\n" +
//        "/** \n"
//        + "* Hello \n"
//        + "* @docfacto.link {@note blah} uri=" + '"' + '{' + "some uri" + '}' + "/something@/something" + '"' +
//        " blah blah \n"
//        + "* @docfacto.link blah blah @since 2.0 @docfacto.link blah blah \n"
//        + "* blah blah \n"
//        + "*/\n";
//
//    private static String content = testComment + testComment + testComment + testComment + testComment;

}
