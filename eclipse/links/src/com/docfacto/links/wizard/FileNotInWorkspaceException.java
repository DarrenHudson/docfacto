package com.docfacto.links.wizard;

/**
 * An exception for when a file is not in the eclipse workspace
 *
 * @author damonli - created Oct 31, 2013
 * @since 2.4.8
 */
public class FileNotInWorkspaceException extends Exception {

}
