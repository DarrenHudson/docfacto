package com.docfacto.links.wizard;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.ui.IWorkbench;

/**
 * A link inserter for javascript files.
 *
 * @author damonli - created Oct 31, 2013
 * @since 2.4.8
 */
public class JavaScriptLinkInserter extends XMLLinkInserter {

	/**
	 * Constructor.
	 * @param workbench the workbench which has the active editor
	 * @param file the active file
	 * @param path the active file path
	 * @since 2.4.8
	 */
	public JavaScriptLinkInserter(IWorkbench workbench, IFile file, IPath path) {
		super(workbench, file, path);
	}

	/**
	 * @see com.docfacto.links.wizard.XMLLinkInserter#getLink(java.lang.String)
	 */
	@Override
	protected String getLink(String linkAttributes) {
		return "// @docfacto.link " + linkAttributes;
	}
}
