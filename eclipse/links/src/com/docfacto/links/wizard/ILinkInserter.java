package com.docfacto.links.wizard;

/**
 * An interface for classes which insert links to files.
 *
 * @author damonli - created Oct 31, 2013
 * @since 2.4.8
 */
public interface ILinkInserter {

	/**
	 * Inserts link into a file for the given attributes of the link.
	 * 
	 * @param fileUri the file uri
	 * @param linkAttributes the attributes of the link to be inserted
	 * @param lineNumber the line to insert the link into
	 * @param charOffset takes the offset if needed
	 * @throws LinkInsertException when there was a problem inserting the link
	 * @since 2.4.8
	 */
	public void insertLinkToFile(String fileUri, String linkAttributes, int lineNumber, int charOffset) throws LinkInsertException;
}
