package com.docfacto.links.wizard;

import com.docfacto.common.DocfactoException;

/**
 * An exception to be thrown when there is was a problem inserting a link.
 * 
 * @author damonli
 * @since 2.5.0
 */
public class LinkInsertException extends DocfactoException {

	/**
	 * Whether or not this exception should be exposed the user (logged and/or show an error dialog)
	 */
	private boolean exposeToUser;
	
    /**
     * Constructor
     * 
     * @param message for the exception
     * @since 2.5.0
     */
	public LinkInsertException(String message) {
		super(message);
		this.exposeToUser = true;
	}

    /**
     * Constructor
     * 
     * @param message for the exception
     * @param ex what caused the exception
     * @since 2.5.0
     */
	public LinkInsertException(String message, Throwable ex) {
		super(message, ex);
		this.exposeToUser = true;
	}
	
	/**
     * Constructor
     * 
     * @param message for the exception
     * @param exposeToUser whether this exception will be exposed to the user or not
     * @since 2.5.0
     */
	public LinkInsertException(String message, boolean exposeToUser) {
		super(message);
		this.exposeToUser = exposeToUser;
	}

	/**
	 * Get whether this exception is exposed to the user or not.
	 * @return whether this exception is exposed to the user or not.
	 */
	public boolean isExposedToUser() {
		return exposeToUser;
	}
}
