package com.docfacto.links.wizard;

import org.eclipse.ui.INewWizard;

import com.docfacto.core.swt.AbstractWizardHandler;

/**
 * Open a links wizard.
 * 
 * @author dhudson - created 4 Nov 2013
 * @since 2.4
 */
public class LinksWizardCommand extends AbstractWizardHandler {

    /**
     * @see com.docfacto.core.swt.AbstractWizardHandler#getWizard()
     */
    @Override
    public INewWizard getWizard() {
        return new LinksWizard();
    }
}
