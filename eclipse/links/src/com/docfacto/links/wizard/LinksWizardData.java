package com.docfacto.links.wizard;

import org.eclipse.ui.IEditorPart;

public class LinksWizardData {

	public String currentFileUri;
	public String targetFileUri;
    public String targetFileType;
    public int targetLineToInsertLink;
    public IEditorPart activeEditor;
    public int targetCharOffset;
    public String baseDir;
}
