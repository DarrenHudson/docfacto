package com.docfacto.links.wizard;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

/**
 * Show the Doc file, and allow user to select location
 *
 * @author dhudson - created 19 Jul 2013
 * @since 2.4
 */
public class LinksWizardDocPage extends WizardPage {

    private Composite theContainer;
    
    /**
     * Constructor.
     * @param pageName
     */
    protected LinksWizardDocPage(String pageName) {
        super(pageName);
    }

    /**
     * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createControl(Composite parent) {

        theContainer = new Composite(parent, SWT.NONE);
        
        setControl(theContainer);
        setPageComplete(false);
    }

}
