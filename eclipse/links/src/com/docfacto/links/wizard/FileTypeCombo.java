package com.docfacto.links.wizard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;

/**
 * This FileTypeCombo is a wrapper of a Combo instead of subclassing it becuase Combo is final and should not be subclassed.
 * @author damonli
 * @since 2.4.8
 */
public class FileTypeCombo {
	private Combo combo;
	private List<String> fileTypes;
	private Map<String, String> extensionToTypeMap;
	
	/**
	 * Constructor.
	 * @param container the container of the combo.
	 */
	public FileTypeCombo(Composite container) {
		combo = new Combo(container, SWT.READ_ONLY | SWT.DROP_DOWN);
		fileTypes = new ArrayList<String>();
		extensionToTypeMap = new HashMap<String,String>();
		setUpFileTypes();
		setUpExtensionToTypeMap();
	}
	
	private void setUpFileTypes() {
		fileTypes.add("java");
		fileTypes.add("javascript");
		fileTypes.add("html");
		fileTypes.add("xml");

		for (String fileType : fileTypes)
			combo.add(fileType);
	}
	
	private void setUpExtensionToTypeMap() {
		extensionToTypeMap.put("java", "java");
		extensionToTypeMap.put("js", "javascript");
		extensionToTypeMap.put("html", "html");
		extensionToTypeMap.put("xml", "xml");
	}

	/**
	 * Sets the current file type of the combo for a file extension.
	 * <p>
	 * This method sets the currently selected file type for the combo for a given file extension.
	 * 
	 * For example, an extension of "xml" would set the file type to xml.
	 * </p>
	 * @param fileExtension the extension to set the file type for
	 * @since 2.4.8
	 */
	public void setFileTypeAccordingToFileExtension(String fileExtension) {
		if (extensionToTypeMap.containsKey(fileExtension))
			combo.select(fileTypes.indexOf(extensionToTypeMap.get(fileExtension)));
		else
			combo.select(fileTypes.indexOf("xml"));
	}

	/**
	 * Add a key listener for this combo.
	 * 
	 * @param listener the listener
	 * @since 2.4.8
	 */
	public void addKeyListener(KeyListener listener) {
		combo.addKeyListener(listener);
	}

	/**
	 * Adds a selection listener for this combo.
	 * 
	 * @param listener the listener
	 * @since 2.4.8
	 */
	public void addSelectionListener(SelectionListener listener) {
		combo.addSelectionListener(listener);
	}

	public void select(int index) {
		combo.select(index);
	}

	/**
	 * Sets the layout grid data for this combo.
	 * 
	 * @param gridData the grid data
	 * @since 2.4.8
	 */
	public void setLayoutData(GridData gridData) {
		combo.setLayoutData(gridData);;
	}

	/**
	 * Get the text being shown on the combo.
	 * 
	 * @return the combo text
	 * @since 2.4.8
	 */
	public String getText() {
		return combo.getText();
	}
}
