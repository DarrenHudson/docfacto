package com.docfacto.links.wizard;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

public class LinksWizardSourcePage extends WizardPage {

    private Composite theContainer;
    
    /**
     * Constructor.
     */
    protected LinksWizardSourcePage() {
        super("Source Link");
    }

    /**
     * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createControl(Composite parent) {
        theContainer = new Composite(parent, SWT.NONE);
    
        setControl(theContainer);
        setPageComplete(false);
    }

}
