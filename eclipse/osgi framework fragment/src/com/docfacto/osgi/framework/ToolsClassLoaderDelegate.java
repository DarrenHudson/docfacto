package com.docfacto.osgi.framework;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;

import org.eclipse.osgi.baseadaptor.HookConfigurator;
import org.eclipse.osgi.baseadaptor.HookRegistry;
import org.eclipse.osgi.framework.adaptor.BundleClassLoader;
import org.eclipse.osgi.framework.adaptor.BundleData;
import org.eclipse.osgi.framework.adaptor.ClassLoaderDelegateHook;

/**
 * Create a class loading delegate so that we can locate and load tools.jar
 * 
 * @author dhudson - created 9 May 2013
 * @since 2.3
 */
public class ToolsClassLoaderDelegate implements ClassLoaderDelegateHook,
HookConfigurator {
    private boolean flag;

    private URLClassLoader theToolsLoader;

    /**
     * Constructor.
     */
    public ToolsClassLoaderDelegate() {
        theToolsLoader = createURLClassloader();
    }

    /**
     * @see org.eclipse.osgi.framework.adaptor.ClassLoaderDelegateHook#postFindClass(java.lang.String,
     * org.eclipse.osgi.framework.adaptor.BundleClassLoader,
     * org.eclipse.osgi.framework.adaptor.BundleData)
     */
    @Override
    public Class<?> postFindClass(String name,BundleClassLoader classLoader,
    BundleData data) throws ClassNotFoundException {

        // This can be core or adam...
        if (data.getSymbolicName().startsWith("com.docfacto")) {
            if (theToolsLoader!=null&&!flag) {
                if (name.startsWith("com.sun.javadoc")||
                    name.startsWith("com.sun.tools")) {
                    try {
                        flag = true;
                        return theToolsLoader.loadClass(name);
                    }
                    catch (Throwable t) {
                        t.printStackTrace();
                    }
                    finally {
                        flag = false;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Locate tools.jar and then create a URL class loader from it.
     * 
     * @return a URL class loader that handles classes from tools.jar
     * @since 2.3
     */
    private URLClassLoader createURLClassloader() {

        try {
            File javaHome;
            try {
                javaHome =
                    new File(System.getProperty("java.home"))
                        .getCanonicalFile();
            }
            catch (IOException e) {
                throw new IllegalStateException(
                    "Unable to locate java home",e);
            }
            if (!javaHome.exists()) {
                throw new IllegalStateException("The java home '"+
                    javaHome.getAbsolutePath()+"' does not exits");
            }

            // Find the tools
            File jarFile = new File(javaHome.getParent(),"lib/tools.jar");

            if (jarFile.exists()) {
                URL url = jarFile.getCanonicalFile().toURI().toURL();
                return new URLClassLoader(new URL[] {url},null);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * @see org.eclipse.osgi.framework.adaptor.ClassLoaderDelegateHook#preFindClass(java.lang.String,
     * org.eclipse.osgi.framework.adaptor.BundleClassLoader,
     * org.eclipse.osgi.framework.adaptor.BundleData)
     */
    @Override
    public Class<?> preFindClass(String name,BundleClassLoader classLoader,
    BundleData data) throws ClassNotFoundException {
        return null;
    }

    /**
     * @see org.eclipse.osgi.framework.adaptor.ClassLoaderDelegateHook#preFindResource(java.lang.String,
     * org.eclipse.osgi.framework.adaptor.BundleClassLoader,
     * org.eclipse.osgi.framework.adaptor.BundleData)
     */
    @Override
    public URL preFindResource(String name,BundleClassLoader classLoader,
    BundleData data) throws FileNotFoundException {
        return null;
    }

    /**
     * @see org.eclipse.osgi.framework.adaptor.ClassLoaderDelegateHook#postFindResource(java.lang.String,
     * org.eclipse.osgi.framework.adaptor.BundleClassLoader,
     * org.eclipse.osgi.framework.adaptor.BundleData)
     */
    @Override
    public URL postFindResource(String name,BundleClassLoader classLoader,
    BundleData data) throws FileNotFoundException {
        return null;
    }

    /**
     * @see org.eclipse.osgi.framework.adaptor.ClassLoaderDelegateHook#preFindResources(java.lang.String,
     * org.eclipse.osgi.framework.adaptor.BundleClassLoader,
     * org.eclipse.osgi.framework.adaptor.BundleData)
     */
    @Override
    public Enumeration<URL> preFindResources(String name,
    BundleClassLoader classLoader,BundleData data) throws FileNotFoundException {
        return null;
    }

    /**
     * @see org.eclipse.osgi.framework.adaptor.ClassLoaderDelegateHook#postFindResources(java.lang.String,
     * org.eclipse.osgi.framework.adaptor.BundleClassLoader,
     * org.eclipse.osgi.framework.adaptor.BundleData)
     */
    @Override
    public Enumeration<URL> postFindResources(String name,
    BundleClassLoader classLoader,BundleData data) throws FileNotFoundException {
        return null;
    }

    /**
     * @see org.eclipse.osgi.framework.adaptor.ClassLoaderDelegateHook#preFindLibrary(java.lang.String,
     * org.eclipse.osgi.framework.adaptor.BundleClassLoader,
     * org.eclipse.osgi.framework.adaptor.BundleData)
     */
    @Override
    public String preFindLibrary(String name,BundleClassLoader classLoader,
    BundleData data) throws FileNotFoundException {
        return null;
    }

    /**
     * @see org.eclipse.osgi.framework.adaptor.ClassLoaderDelegateHook#postFindLibrary(java.lang.String,
     * org.eclipse.osgi.framework.adaptor.BundleClassLoader,
     * org.eclipse.osgi.framework.adaptor.BundleData)
     */
    @Override
    public String postFindLibrary(String name,BundleClassLoader classLoader,
    BundleData data) {
        return null;
    }

    /**
     * @see org.eclipse.osgi.baseadaptor.HookConfigurator#addHooks(org.eclipse.osgi.baseadaptor.HookRegistry)
     */
    @Override
    public void addHooks(HookRegistry hookRegistry) {
        hookRegistry.addClassLoaderDelegateHook(this);
    }
}
