
      var tree;
      
      function treeInit() {
      tree = new YAHOO.widget.TreeView("treeDiv1");
      var root = tree.getRoot();
    
	

	
	

	var objd4e269 = { label: "Introduction", href:"introduction/c_introduction_new.html", target:"contentwin" };
    var d4e269 = new YAHOO.widget.TextNode(objd4e269, root, false);var objd4e279 = { label: "The coolest tools ever!", href:"introduction/c_cool_tools.html", target:"contentwin" };
    var d4e279 = new YAHOO.widget.TextNode(objd4e279, d4e269, false);var objd4e289 = { label: "How docfacto tools work together", href:"introduction/c_tools_work_together.html", target:"contentwin" };
    var d4e289 = new YAHOO.widget.TextNode(objd4e289, d4e269, false);var objd4e299 = { label: "How does your documentation rate?", href:"introduction/c_documentation_rate.html", target:"contentwin" };
    var d4e299 = new YAHOO.widget.TextNode(objd4e299, d4e269, false);var objd4e309 = { label: "Impact of documentation debt", href:"introduction/c_impact_doc_debt.html", target:"contentwin" };
    var d4e309 = new YAHOO.widget.TextNode(objd4e309, d4e269, false);var objd4e319 = { label: "Docfacto toolkit for java", href:"introduction/c_what_docfacto.html", target:"contentwin" };
    var d4e319 = new YAHOO.widget.TextNode(objd4e319, d4e269, false);var objd4e336 = { label: "Docfacto Installation Overview", href:"installation/c_installation_overview.html", target:"contentwin" };
    var d4e336 = new YAHOO.widget.TextNode(objd4e336, root, false);var objd4e346 = { label: "Installing Docfacto runtime engine", href:"installation/c_installing_docfacto.html", target:"contentwin" };
    var d4e346 = new YAHOO.widget.TextNode(objd4e346, d4e336, false);var objd4e364 = { label: "Installing Docfacto plugins for Eclipse", href:"installation/c_installing_plugin.html", target:"contentwin" };
    var d4e364 = new YAHOO.widget.TextNode(objd4e364, d4e336, false);var objd4e380 = { label: "Installing the Sonar plugins", href:"installation/c_installing_sonar.html", target:"contentwin" };
    var d4e380 = new YAHOO.widget.TextNode(objd4e380, d4e336, false);var objd4e390 = { label: "Installing the Sonar Adam Plugin", href:"installation/c_installing_sonar_adam.html", target:"contentwin" };
    var d4e390 = new YAHOO.widget.TextNode(objd4e390, d4e380, false);var objd4e400 = { label: "System Requirements", href:"installation/r_system_requirements.html", target:"contentwin" };
    var d4e400 = new YAHOO.widget.TextNode(objd4e400, d4e336, false);var objd4e410 = { label: "Links Overview", href:"links/c_links_overview.html", target:"contentwin" };
    var d4e410 = new YAHOO.widget.TextNode(objd4e410, root, false);var objd4e420 = { label: "Links Introduction", href:"links/c_introduction_to_links.html", target:"contentwin" };
    var d4e420 = new YAHOO.widget.TextNode(objd4e420, d4e410, false);var objd4e433 = { label: "Installing Links", href:"links/installation/c_installing_links.html", target:"contentwin" };
    var d4e433 = new YAHOO.widget.TextNode(objd4e433, d4e410, false);var objd4e448 = { label: "Installing Links Eclipse Plugin", href:"links/installation/c_installing_plugin.html", target:"contentwin" };
    var d4e448 = new YAHOO.widget.TextNode(objd4e448, d4e410, false);var objd4e464 = { label: "Installing the Docfacto Links Servlet", href:"links/installation/c_installing_docfacto_links_servlet.html", target:"contentwin" };
    var d4e464 = new YAHOO.widget.TextNode(objd4e464, d4e410, false);var objd4e474 = { label: "Installing the Sonar plugins", href:"installation/c_installing_sonar.html", target:"contentwin" };
    var d4e474 = new YAHOO.widget.TextNode(objd4e474, d4e410, false);var objd4e484 = { label: "Installing the Sonar Links Plugin", href:"links/installation/c_installing_sonar_links.html", target:"contentwin" };
    var d4e484 = new YAHOO.widget.TextNode(objd4e484, d4e474, false);var objd4e494 = { label: "Links Format", href:"links/c_links_format.html", target:"contentwin" };
    var d4e494 = new YAHOO.widget.TextNode(objd4e494, d4e410, false);var objd4e504 = { label: "Links File Associations", href:"links/config/c_links_config.html", target:"contentwin" };
    var d4e504 = new YAHOO.widget.TextNode(objd4e504, d4e410, false);var objd4e514 = { label: "Links File Associations", href:"links/config/c_links_file_associations.html", target:"contentwin" };
    var d4e514 = new YAHOO.widget.TextNode(objd4e514, d4e410, false);var objd4e524 = { label: "Link Paths", href:"links/config/c_links_paths_config.html", target:"contentwin" };
    var d4e524 = new YAHOO.widget.TextNode(objd4e524, d4e410, false);var objd4e534 = { label: "Running Links", href:"links/c_running_links.html", target:"contentwin" };
    var d4e534 = new YAHOO.widget.TextNode(objd4e534, d4e410, false);var objd4e549 = { label: "Running Links from Ant", href:"links/c_running_links_ant.html", target:"contentwin" };
    var d4e549 = new YAHOO.widget.TextNode(objd4e549, d4e534, false);var objd4e566 = { label: "Running Links from Maven", href:"links/c_running_links_maven.html", target:"contentwin" };
    var d4e566 = new YAHOO.widget.TextNode(objd4e566, d4e534, false);var objd4e582 = { label: "\nRunningLinks\nfrom Sonar", href:"links/c_running_links_sonar.html", target:"contentwin" };
    var d4e582 = new YAHOO.widget.TextNode(objd4e582, d4e534, false);var objd4e601 = { label: "\nRunningLinks\nfrom Docfacto Links Servlet", href:"links/c_running_links_servlet.html", target:"contentwin" };
    var d4e601 = new YAHOO.widget.TextNode(objd4e601, d4e534, false);var objd4e621 = { label: "Link - Eclipse Plugin", href:"links/eclipse-plugin/c_links_eclipse_plugin_overview.html", target:"contentwin" };
    var d4e621 = new YAHOO.widget.TextNode(objd4e621, d4e410, false);var objd4e631 = { label: "Link - Using the Links Eclipse Plugin", href:"links/eclipse-plugin/c_using_links_plugin.html", target:"contentwin" };
    var d4e631 = new YAHOO.widget.TextNode(objd4e631, d4e410, false);var objd4e641 = { label: "Link - Hovers", href:"links/eclipse-plugin/c_using_hovers.html", target:"contentwin" };
    var d4e641 = new YAHOO.widget.TextNode(objd4e641, d4e410, false);var objd4e651 = { label: "Links Statistics", href:"links/r_links_statistics.html", target:"contentwin" };
    var d4e651 = new YAHOO.widget.TextNode(objd4e651, d4e410, false);var objd4e664 = { label: "Generating output from Links", href:"links/c_links_output.html", target:"contentwin" };
    var d4e664 = new YAHOO.widget.TextNode(objd4e664, d4e410, false);

	var objd4e893 = { label: "Glossary", href:"glossary.html", target:"contentwin" };
    var d4e893 = new YAHOO.widget.TextNode(objd4e893, root, false);

      tree.draw(); 
      } 
      
      YAHOO.util.Event.addListener(window, "load", treeInit); 
    