<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE concept PUBLIC "-//OASIS//DTD DITA Concept//EN" "concept.dtd">
<concept id="linksservlet">
	<title>
		Running
		<keyword keyref="links" />
		from the Docfacto Links Dashboard
	</title>
	<shortdesc>
		How to use the Docfacto Links Dashboard to run
		<codeph>
			<keyword keyref="links" />
		</codeph>
		.
	</shortdesc>
	<conbody>
		<section>
			<title>Adding a Project</title>
			<p>
				After deploying <keyword keyref="links.servlet.war" /> you can view the links servlet opening screen at for example: 
				
				<codeblock>localhost:8080/docfacto-links-servlet</codeblock>
			</p>
			
			<fig>
				<title>The Links dashboard at the start</title>
				<image href="images/dashboard/LinksDashboardStart.png" width="350" placement="break"></image>
			</fig>

			<p>
				To add a project, you must be logged in (by clicking the 'Log in' button on the top right of the page) then add project 
				by clicking the 'Projects' tab on the left of the page and then clicking 'Add New Project' (only those logged in have permission to do this).
			
				<fig>
					<title>Where to add a new project</title>
					<image href="images/dashboard/LinksDashboardProjectSideMenu.png" width="350" placement="break"></image>
				</fig>
			
				<note>
					Your log in details are set into the config.xml file. 
					(see the <xref href="../installation/c_installing_docfacto_links_servlet.dita#installing_links_servlet">Installing the Docfacto Links Servlet</xref>).
				</note>
			</p>
			
			<p>	
				The 'Add Project' pop up will appear and ask for the following information:
			
				<fig>
					<title>Adding a new project</title>
					<image href="images/dashboard/LinksDashboardAddNewProject.png" width="350" placement="break"></image>
				</fig>
			
				<simpletable relcolwidth="90* 180*">
           			<sthead>
           				<stentry>Field</stentry>
           				<stentry>Description</stentry>
           			</sthead>
           			<strow>
           				<stentry><codeph>Project Name</codeph></stentry>
           				<stentry>The project name you would like to associate with this project.</stentry>
           			</strow>
           			<strow>
           				<stentry><codeph>Source Directory</codeph></stentry>
           				<stentry>The directory of the source files if you have any.</stentry>
           			</strow>
					<strow>
           				<stentry><codeph>Documentation Directory</codeph></stentry>
           				<stentry>The directory of the documentation files if you have any.</stentry>
           			</strow>
           			<strow>
           				<stentry><codeph>XML Configuration Directory</codeph></stentry>
           				<stentry>The path to the Docfacto XML Config which also has the correct base directory and paths configured for this project.</stentry>
           			</strow>
           			<strow>
           				<stentry><codeph>Source File Extensions</codeph></stentry>
           				<stentry>The extensions of the source files you want to analyze (no extensions means any files will be processed).</stentry>
           			</strow>
           			<strow>
           				<stentry><codeph>Doc File Extensions</codeph></stentry>
           				<stentry>The extensions of the document files you want to analyze (no extensions means any files will be processed).</stentry>
           			</strow>
           			<strow>
           				<stentry><codeph>Base Directory</codeph></stentry>
           				<stentry>The base directory for your project (This is optional and overrides the one set in the Docfacto XML config).</stentry>
           			</strow>
				</simpletable>
			
				An example project configuration would be: 
				
				<sl>
					<sli>Project Name: My project</sli>
					<sli>Source Directory: your-home/workspace/project/src</sli>
					<sli>Doc Directory: your-home/workspace/project/doc</sli>
					<sli>XML Config Directory: your-home/workspace/project/Docfacto.xml</sli>
					<sli>Source File Extensions: .java</sli>
					<sli>Doc File Extensions: .dita, .txt, .xml</sli>
					<sli>Base Directory: your-home/workspace/project</sli>
				</sl>
				
				<note>
					Remember to make the links paths in the Docfacto.xml file are set correctly in relation to 
					the working directory and your links tags in your source/doc files to ensure the results will display 
					correctly (see <xref href="../config/c_links_paths_config.dita#links_paths_config">Configuring Links Paths</xref>).
				</note>
			</p>
			
			<p>
				You can change the project or delete it in the 'Project Details' tab by clicking the 'Edit Project' button.
				
				<fig>
					<title>Running the project and changing the project details</title>
					<image href="images/dashboard/LinksDashboardProjectDetailsPage.png" width="450" placement="break"></image>
				</fig>
			</p>
		</section>
		<section>
			<title>Viewing and Analysing Output from Links</title>
			<p>
				Once you have added a project, Links is automatically run on it. When you have made changes to your code/documentation, you can click the 
				'Projects' side tab and click 'Run Links' on your project to run links again. 
			</p>
			
			<p>
				You can see a summary of the statistics by clicking the 'Summary' tab, and view all the rule violations by clicking on the 'Errors' tab. 
			</p>
			
			<fig>
				<title>The Summary page</title>
				<image href="images/dashboard/LinksDashboardSummaryPage.png" width="450" placement="break"></image>
			</fig>
			
			<p>In the Errors tab you can select which packages you want to analyse by selecting the packages you wish to see results for in the 'Packages' section 
				and then clicking the 'Update' button.
			</p>
			
			<fig>
				<title>The Errors Page</title>
				<image href="images/dashboard/LinksDashboardErrorsPage.png" width="450" placement="break"></image>
			</fig>
			
			<p>
				To see the location of a particular violation in the 'Errors' Tab, click on one of the rules which have been violated in the 
				'Results' section's 'Rule' table, then click on a package in the subsequent Package table, then select one of the files in the 'File' table.
			</p>
			
			<p>
				To change the project's settings or even delete the project, click the 'Project Details' tab (only those logged in have permission to do this).
			</p>
		</section>
	</conbody>
</concept>
