![Alt text](./doc/dita/docfacto/images/docfacto_DOODLE.png)
# Docfacto

Docfacto eliminates documentation debt and takes the "too hard" out of documentation for time-pressured developers.

Docfacto project is a tool set for developers that work together from within their IDE and help eliminate documentation debt (the amount of work required to fix all outstanding documentation issues for a software project), by making it easier for them to increase the accuracy of code comments, capture white board designs, and write user or system documentation. The code can then be linked to the documentation to keep both in sync.
This methodology puts the software team at the heart of the documentation process ensuring the code is the source of the truth. 

This way the developers are creating the content that technical authors need for authoring.
Docfacto tools make understanding the level of documentation debt and the impact of code change easier and when used in conjunction with our methodology helps eliminate documentation debt, which will improve performance, efficiencies and lower the barriers to developers completely and accurately commenting code.


The best way for a developer to demonstrate the brilliance of their code is to capture all the smart thinking that went into it. When the developer's clever logic isn't captured nobody ever knows if the code is good or "why" it was done the way it is. Capturing these ideas leads to better documentation. Ultimately, software is only as good as its supporting documentation. Not documenting code creates legacy code as soon as it's written and this is bad for business.

Sonar and Eclipse plugins have been created to highlight documentation debt.

**This project has been donated to the Open Source Community and is licence and warranty free.**

Full toolkit documentation can be found [here](./doc/dita/release/toolkit/pdf/docfacto-dita.pdf)

![Alt text](./doc/dita/docfacto/images/docfacto_ADAM_icon.png)
## Docfacto Adam:
Comments validation and syntax checker that checks Javadoc comments against the code with easily customisable rules that allows you to extend your coding standards to comments, to ensure greater accuracy and improved documentation. Adam is also available for Eclipse and Sonar.


![Alt text](./doc/dita/docfacto/images/docfacto_BEERMAT_icon.png)
## Docfacto Beermat:
A SVG editor for Eclipse that allows developers to easily create and edit SVG diagrams within Eclipse, providing a quick and easy way to doodle ideas and designs and share them with the development community or add them to documentation without leaving the IDE.


![Alt text](./doc/dita/docfacto/images/docfacto_TAGLETS_icon.png)
## Docfacto Taglets:
Customisable rich content taglets that extend the Javadoc framework to capture and explain the why and not just the what.


![Alt text](./doc/dita/docfacto/images/docfacto_LINKS_icon.png)
## Docfacto Links:
A code, developer asset and documentation linking tool that shows the impact of any code change on each other, the scope of documentation coverage and exposes hidden features. It also helps technical authors track what code is responsible for which features and functions.



## Docfacto Grabit:
A simple screen capture and annotate facility within the IDE, which simplifies the process of including screen shots as a development asset.

![Alt text](./doc/dita/docfacto/images/docfacto_DITA_icon.png)
## Docfacto DITA Doclet:
Javadoc to DITA Doclet that handles generics, doesn't require any specialisation and can generate DITA files from Javadoc to compliment your existing DITA files. This Doclet honours the Docfacto Taglets as well.

![Alt text](./doc/dita/docfacto/images/docfacto_XSD_icon.png)
## Docfacto XSD Tools:
XSD documentation tool that extracts the annotations from the XSD, generates system and user documentation from them, making the XSD's self-documenting to describe and visualise what the schemas look like.